
import {Component, ViewChild} from '@angular/core';
import {API_Config} from '../../../common/core/API_Config';
import {BaseTable} from '../../../common/core/classes_us/base-table';
import {TableFunctions} from '../../../common/core/table-functions';
import {BaseService} from '../../../common/core/classes_us/base-service';
import {TableStructure} from '../../../common/directives/directives';
import {CountryService} from '../../../master-data/country/country-service';
import {StateService} from '../../../master-data/state/state-service';
import {AddContact} from './add-contact.component';
import {Alert} from '../../../common/directives/popup-modal_us/alert/alert';
import {Confirm} from '../../../common/directives/popup-modal_us/confirm/confirm';

declare var jQuery: any;

@Component({
    selector: 'client-info',
    directives: [
        TableStructure,
        AddContact,
        Confirm,
        Alert
    ],
    providers: [
        TableFunctions,
        BaseService,
        CountryService,
        StateService
    ],
    templateUrl: 'client-info.component.html',
})

export class ClientInfo extends BaseTable
{
    @ViewChild(AddContact) addContact: AddContact;
    @ViewChild(Confirm) confirm: Confirm;
    @ViewChild(Alert) alert: Alert;

    public urlCustomerInfo = this._api.API_CLIENT_INFO;
    public urlCustomerContacts = this._api.API_CUSTOMER_CONTACTS;

    public tableID = 'contact-info';
    public url = this.urlCustomerContacts + '/get';
    public headerUrlSuffix = 'rep';
    public title = 'Contact Information';
    public permissionPage = 'viewInvoice';

    public headerDef = [
        {
            id: 'ver_table',
            value: 11222
        },
        {
            id: 'delete',
            name: 'DEL',
            width: 48,
            template: '<input type="checkbox" class="selectContacts">'
        },
        {
            id: 'cus_ctt_dft',
            name: 'DFT',
            width: 48,
            unsortable: true
        },
        this.columnList.customerContactDepartment,
        this.columnList.customerContactFirstName,
        this.columnList.customerContactLastName,
        this.columnList.customerContactPosition,
        this.columnList.customerContactEmail,
        this.columnList.customerContactPhone,
        this.columnList.customerContactExtention,
        this.columnList.customerContactMobile,
        this.hiddenColumns.customerContactID
    ];

    public skipPageSize = true;
    public skipExport = true;

    public customSearches = {
        cus_id: ''
    };

    public isVisible: boolean;

    public clientInfo = {
        cus_id: ''
    };

    protected infoDropDowns = {
        countries: {
            service: '_countryService',
            idField: 'sys_country_id',
            nameField: 'sys_country_name'
        },
        states: {
            service: '_stateService',
            idField: 'sys_state_id',
            nameField: 'sys_state_name'
        }
    };

    //**************************************************************************

    constructor (
        public _api: API_Config,
        public _tableFunc: TableFunctions,
        public _tableService: BaseService,
        public _countryService: CountryService,
        public _stateService: StateService
    )
    {
        super();

        this.tableComponent = this;
    }

    //**************************************************************************

    public showClientInfo(cus_id)
    {
        this.customSearches.cus_id = cus_id;
        this.messages = '';
        this.searchParams = this._tableFunc.getSearchParams(this.customSearches);

        this._tableFunc.permissionCall(this);

        let url = this.urlCustomerInfo + '/get?cus_id=' + cus_id;
        // showLoadingOverlay will be set to false in the getInfoDropdowns() method
        this.showLoadingOverlay = true;

        this._tableService.get(url)
            .subscribe(
                data => {

                    this.clientInfo = data;

                    this.getInfoDropdowns();
                },
                err => {
                    this.messages = this._tableFunc.parseError(err);
                    this.showLoadingOverlay = false;
                },
                () => {}
        );

        this.isVisible = true;
    }

    //**************************************************************************

    public hideClientInfo()
    {
        this.isVisible = false;
    }

    //**************************************************************************

    public updateClientInfo()
    {
        let errors = [],
            infoInputs = {
                action: 'deleteContacts',
                cus_code: 'Customer Code',
                cus_type: 'Customer Type',
                cus_name: 'Customer Name',
                bill_to_add_line_1: 'Bill Address Line 1',
                bill_to_add_line_2: 'Bill Address Line 2',
                bill_to_city: 'Bill City',
                bill_to_country_id: 'Bill Country',
                bill_to_state_id: 'Bill State',
                bill_to_zip: 'Bill Zip',
                net_terms: 'PYMNT-Term',
                ship_to_add_line_1: 'Ship Address Line 1',
                ship_to_add_line_2: 'Ship Address Line 2',
                ship_to_city: 'Ship City',
                ship_to_country_id: 'Ship Country',
                ship_to_state_id: 'Ship State',
                ship_to_zip: 'Ship Zip'
            };

        let optional = [
            'bill_to_country',
            'ship_to_country',
            'bill_to_add_line_2',
            'ship_to_add_line_2'
        ];

        for (var key in this.clientInfo) {
            if (infoInputs.hasOwnProperty(key)
             && ! ~jQuery.inArray(key, optional) && ! this.clientInfo[key]) {

                errors.push(infoInputs[key]);
            }
        }

        if (errors.length) {
            this.alert.show('Missing mandatory values:<br>' + errors.join(', '));
        } else {

            let message = 'Customer Billing and Shipping information will be '
                        + 'updated.<br>Proceed?';

            let data = this.cloneObject(this.clientInfo);

            data['action'] = 'updateAddress';

            this.confirm.show(message, data);
        }
    }

    //**************************************************************************

    public onCellClicked($event)
    {
        if ($event.colDef.field != 'cus_ctt_dft') {
            return;
        }

        let index = $event.rowIndex;

        let cus_ctt_id = this.rowData[index].cus_ctt_id,
            cus_id = this.customSearches.cus_id;

        jQuery('#contact-info .defaultRadio').eq(index).prop('checked', true);

        let url = this.urlCustomerContacts + '/setDefault/' + cus_ctt_id + '/'
                + cus_id;

        this.showLoadingOverlay = true;

        this._tableService.post(url)
            .subscribe(
                data => {},
                err => {
                    this.messages = this._tableFunc.parseError(err);
                },
                () => {
                    this.showLoadingOverlay = false;
                }
        );
    }

    //**************************************************************************

    public createRowData(data: any)
    {
        for (let key in data) {
            data[key].cus_ctt_dft = data[key].cus_ctt_dft == 1 ? `
                <input type="radio" name="defaultContact" class="defaultRadio"
                       checked="checked">
            ` : `
                <input type="radio" name="defaultContact" class="defaultRadio">
            `;
        }

        return data;
    }

    //**************************************************************************

    public removeContacts()
    {
        let selectedContacts = [],
            rowData = this.rowData,
            isDefault = false;

        jQuery('#contact-info .selectContacts').each(function(count: number) {
           if (this.checked) {

                selectedContacts.push(rowData[count].cus_ctt_id);

                isDefault = jQuery('#contact-info .defaultRadio').eq(count)
                        .prop('checked');

                if (isDefault) {
                    return false;
                }
            }
        });

        if (isDefault !== false) {

            this._tableFunc.showMessage(this, 'danger',
                    'Default contacts can not be deleted');

            return false;
        }

        if (selectedContacts.length) {

            let message = 'Selected contact information will be deleted.<br>'
                            + 'Proceed?';

            this.confirm.show(message, {
                action: 'deleteContacts',
                contacts: selectedContacts
            });
        } else {
            this.alert.show('Select contacts to delete');
        }
    }

    //**************************************************************************

    public onConfirm($event)
    {
        let confirmData = $event.data;

        let data = this.cloneObject(confirmData),
            action = confirmData.action;

        delete data['action'];

        if ($event.response) {
            switch (action) {
                case 'deleteContacts':
                    this.deleteContacts(data);
                    break;
                case 'updateAddress':
                    this.updateAddress(data);
                    break;
                default:
                    break;
            }
        }
    }

    //**************************************************************************

    private updateAddress(data)
    {
        let url = this.urlCustomerInfo + '/update/' + data.cus_id,
            params = JSON.stringify(data);

        this.showLoadingOverlay = true;

        this._tableService.post(url, params)
            .subscribe(
                data => {
                    this.alert.show('Customer data was successfully updated');
                },
                err => {
                    this.messages = this._tableFunc.parseError(err);
                },
                () => {
                    this.showLoadingOverlay = false;
                }
        );
    }

    //**************************************************************************

    private deleteContacts(data)
    {
        let url = this.urlCustomerContacts + '/remove',
            params = JSON.stringify(data)

        this.showLoadingOverlay = true;

        this._tableService.post(url, params)
            .subscribe(
                data => {
                    this._tableFunc.permissionCall(this);
                },
                err => {
                    this.messages = this._tableFunc.parseError(err);
                },
                () => {
                    this.showLoadingOverlay = false;
                }
        );
    }

    //**************************************************************************

    public addContacts()
    {
        this.addContact.showInfo(this);
    }

    //**************************************************************************

    public customerRates()
    {
        window.open('/#/invoice/invoice-costs/' + this.customSearches.cus_id);
    }

    //**************************************************************************

}
