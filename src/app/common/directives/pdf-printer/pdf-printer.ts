import {Component, Input, Output, EventEmitter} from '@angular/core';

import {Functions} from '../../../common/core/load';

declare var saveAs:any;

@Component({
	selector: 'pdf-printer',
	templateUrl: 'pdf-printer.html',
})

export class PDFPrinter {
	@Input() printerAPIUrl:string = '';
	@Input() fileName:string = '';
	@Input() label:string = 'Print PDF';
	private showLoadingOverlay:any;
	@Output() bindMessages = new EventEmitter();

	constructor(private funs:Functions) {
	}

	private printPDF() {
		const that = this;
		try {
			this.showLoadingOverlay=true;
			let xhr = new XMLHttpRequest();
			xhr.open("GET", that.printerAPIUrl, true);
			xhr.setRequestHeader("Authorization", 'Bearer ' + that.funs.getToken());
			xhr.responseType = 'blob';

			xhr.onreadystatechange = () => {
				if (xhr.readyState == 2) {
					if (xhr.status == 200) {
						xhr.responseType = "blob";
					} else {
						xhr.responseType = "text";
					}
				}

				if (xhr.readyState === 4) {
					if (xhr.status === 200) {
						var blob = new Blob([xhr.response], {type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'});
						saveAs.saveAs(blob, that.fileName + '.pdf');
						this.showLoadingOverlay=false;
					} else {
						let errMsg = '';
						if (xhr.response) {
							try {
								var res = JSON.parse(xhr.response);
								errMsg = res.errors && res.errors.message ? res.errors.message : xhr.statusText;
							}
							catch (err) {
								errMsg = xhr.statusText
							}
						}
						else {
							errMsg = xhr.statusText;
						}
						if (!errMsg) {
							errMsg = that.funs.msg('VR100');
						}
						this.showLoadingOverlay=false;
						this.bindMessages.emit(this.funs.Messages('danger', errMsg));
					}
				}
			};
			xhr.send();
		} catch (err) {
			this.bindMessages.emit(this.funs.Messages('danger', that.funs.msg('VR100')));
		}
	}
}
