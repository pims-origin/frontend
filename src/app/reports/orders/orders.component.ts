
import {Component} from '@angular/core';
import {API_Config} from '../../common/core/API_Config';
import {BaseTable} from '../../common/core/classes_us/base-table';
import {Functions} from '../../common/core/functions';
import {TableFunctions} from '../../common/core/table-functions';
import {DateFunctions} from '../../common/core/functions/date-functions';
import {BaseService} from '../../common/core/classes_us/base-service';
import {CustomersService} from '../../master-data/customers/customers-service';
import {TableStructure} from '../../common/directives/directives';

@Component ({
    selector: 'orders',
    directives: [
        TableStructure
    ],
    templateUrl: '../../common/directives/table-structure/table.component.html',
    providers: [
        BaseService,
        CustomersService,
        TableFunctions,
        DateFunctions
    ]
})

export class OrdersComponent extends BaseTable
{
    public tableID = 'orders';
    public url = this._api.API_ORDER_REPORTS;
    public headerUrlSuffix = 'rep';
    public title = 'Search Orders';
    public permissionPage = 'viewReport';

    public headerDef = [
        {
            id: 'ver_table',
            value: 1 + Math.random() * 10000
        },
        this.columnList.customer,
        this.columnList.userName,
        this.columnList.orderStatus,
        this.columnList.orderType,
        this.columnList.orderNumber,
        this.columnList.customerPO,
        this.columnList.customerOrderNumber,
        this.columnList.shipToName,
        this.columnList.shipToAddress,
        this.columnList.shipToCity,
        this.columnList.shipToState,
        this.columnList.shipToCountry,
        this.columnList.shipToZip,
        this.columnList.warehouse,
        this.columnList.qty,
        this.columnList.pieces,
        this.columnList.orderRequestDate,
        this.columnList.shipByDate,
        this.columnList.cancelByDate,
        this.columnList.carrier,
        this.columnList.cusNotes
    ];

    public unsortableColumns = [
        'qty',
        'pieces'
    ];

    public searches = [
        this.searchList.customer,
        this.searchList.orderType,
        this.searchList.orderNumber,
        this.searchList.customerPO,
        this.searchList.customerOrderNumber,
        this.searchList.orderRequestDate
    ];

    /*
    ****************************************************************************
    */

    constructor (
        public _api: API_Config,
        public _func: Functions,
        public _tableFunc: TableFunctions,
        public _tableService: BaseService,
        public _customersService: CustomersService
    )
    {
        super();

        this._tableFunc.permissionCall(this);
    }

    //**************************************************************************

}