import {Component, Input, Output, EventEmitter} from '@angular/core';

import {WMSPagination, AdvanceTable} from '../directives'

declare var $:any;

@Component({
	selector: 'asn-log-tab-content',
	templateUrl: 'asn-log-tab-content.html',
	directives: [WMSPagination, AdvanceTable]
})

export class ASNLogTabContent {
	private rowDataInput:Array<any> = [];
	private tableIDInput:string = '';
	private headerURLInput:string = '';
	private headerDefInput:any;
	private perPageInput:number;
	private paginDataInput:any;

	private viewData;
	private modalTitle;
	private isTextData;

	@Input('rowData') set rowData(value:any) {
		this.rowDataInput = value;
	}

	@Input('tableID') set tableID(value:any) {
		this.tableIDInput = value;
	}

	@Input('headerURL') set headerURL(value:any) {
		this.headerURLInput = value;
	}

	@Input('headerDef') set headerDef(value:any) {
		this.headerDefInput = value;
	}

	@Input('perPage') set perPage(value:any) {
		this.perPageInput = value;
	}

	@Input('paginData') set paginData(value:any) {
		this.paginDataInput = value;
	}

	@Output() changePage  = new EventEmitter();

	constructor() {
		this.initModalEvent();
	}

	getLogs($event) {
		this.changePage.emit($event);
	}

	private initModalEvent() {
		var that = this;
		$('body').on('click.showDetail', '[data-modal-content]', function(e) {
			if (that.rowDataInput.length === 0) {
				return;
			}

			var elm = $(this),
				modalData = that.rowDataInput[elm.data('index')][elm.data('modal-content')],
				arrData = [];
			try {
				modalData = modalData ? JSON.parse(modalData) : {};
			}
			catch(err) {
			}
			if(modalData instanceof Object && !(modalData instanceof Array)) {
				for(var x in modalData){
					let value = modalData[x];
					if(value instanceof Object && !(value instanceof Array)) {
						value = JSON.stringify(value);
					}
					arrData.push({
						key: x,
						value: value
					})
				}
				that.isTextData = false;
				that.viewData = arrData;
			}
			else {
				that.isTextData = true;
				that.viewData = modalData;
			}
			that.modalTitle = elm.data('title');
			$('#modal-content').modal('show');
		});
	}
}
