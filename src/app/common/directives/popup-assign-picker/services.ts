import {Component, Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {Functions, API_Config} from '../../../common/core/load';

@Injectable()
export class Services {

  public AuthHeader = this._Func.AuthHeader();
  public headerPostJson = this._Func.AuthHeaderPostJson();

  constructor(
    private _Func: Functions, 
    private http: Http, 
    private _api: API_Config) {
  }
  getListCRSByCustomer(url, $whs_id = '') {
    return  this.http.get(url + $whs_id, {headers: this.AuthHeader})
        .map(res => res.json());
  }

  assignPicker(stringData, url) {
    return this.http.put(url, stringData, { headers: this.headerPostJson })
        .map((res: Response) => res.json().data);
  }
}
