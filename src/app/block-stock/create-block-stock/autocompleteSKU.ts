export class AutoCompleteSKUProviders{

    public selectedSKU(skuItem={},item){
        const terms = this.split( skuItem['value'] );
        const itemIds = this.split( skuItem['itemIds'] );
        // remove the current input
        terms.pop();
        if(terms.length < itemIds.length) {
            itemIds.pop();
        }
        // add the selected item
        terms.push( item['sku'] );
        itemIds.push( item['item_id'] );
        skuItem['value'] = terms.join( ", " );
        skuItem['itemIds'] = itemIds.join( ", " );
        return true;
    }

    split( val ) {
        return val.split( /,\s*/ );
    }

    extractLast( term ) {
        return this.split( term ).pop();
    }

}