/**
 * Created by admin on 1/4/2017.
 */
import {Component} from "@angular/core";
import {
    WMSBreadcrumb, WMSMessages, WMSPagination, AdvanceTable
} from '../../common/directives/directives';
import {Functions} from "../../common/core/functions";
import {API_Config} from "../../common/core/API_Config";
import {Http} from '@angular/http';
import {JwtHelper} from "angular2-jwt/angular2-jwt";
import {Router, RouterLink, ROUTER_DIRECTIVES, RouteParams} from '@angular/router-deprecated';
import {UserService} from "../../common/users/users.service";
import {PbServices} from "./pb.service";
declare var jQuery:any;
declare var jsPDF:any;
@Component({
    selector: 'pb-receipt-list',
    directives: [AdvanceTable, WMSMessages, WMSBreadcrumb, WMSPagination],
    templateUrl: 'pb-receipt-list.component.html',
    providers: [PbServices],
})

export class PbReceiptListComponent {
    private tableID = 'return-order-list';
    private headerURL = this._API_Config.API_User_Metas + '/pbo';
    private headerDef = [{id: 'ver_table', value: 1},
        {id: 'ck', name: '', width: 25},
        {id: 'return_sts_name', name: 'Status', width: 90, unsortable: true},
        {id: 'return_num', name: 'Put Back Receipt #', width: 90},
        {id: 'odr_num', name: 'Order Number', width: 90},
        {id: 'cus_name', name: 'Customer', width: 90, unsortable: true},
        {id: 'num_of_sku', name: '# of SKUs', width: 90},
        {id: 'return_qty', name: 'Put Back QTY', width: 90, unsortable: true},
        {id: 'created_at', name: 'Created at', width: 110, unsortable: true},
        {id: 'created_by', name: 'Created by', width: 110, unsortable: true}];
    private pinCols = 5;
    private rowData:any[];
    private getSelectedRow;
    private objSort;
    private searchParams;
    private whs_id:any;
    private Pagination;
    private totalCount = 0;
    private tmpPageCount;
    private pageCount = 0;
    private ItemOnPage = 0;
    private currentPage = 1;
    private perPage = 20;
    private numLinks = 3;
    private messages:any;
    private dataList = [];
    private dataListCustomer = [];
    private asnStatus:any = [];
    private showLoadingOverlay = false;
    private hasEditBtn:any;
    private hasCreateBtn:any;
    private hasPalletAssignment: any;
    // Construct
    constructor(
                private _pbServices:PbServices,
                private _func:Functions,
                private _API_Config:API_Config,
                private _http:Http,
                public jwtHelper:JwtHelper,
                private _router:Router,
                private _user:UserService) {

        this.whs_id = localStorage.getItem('whs_id');
        // Check permission
        this.checkPermission();
        this.getCustomers();
        this.getListReturnOrder(1);
    }

    private checkPermission() {

        this._user.GetPermissionUser().subscribe(
            data => {

                this.hasEditBtn = this._user.RequestPermission(data, 'updatePutback');
                this.hasCreateBtn = this._user.RequestPermission(data, 'createPutbackReceipt');
                this.hasPalletAssignment = this._user.RequestPermission(data, 'assignPalletPutback')
                var flagView = this._user.RequestPermission(data, 'viewPutbackReceipt');

                if (!flagView) {
                    this._router.parent.navigateByUrl('/deny');
                }
            },
            err => {

                this.showMessage('danger', this._func.parseErrorMessageFromServer(err));

            }
        );
    }

    private showMessage(msgStatus, msg) {

        this.messages = {
            'status': msgStatus,
            'txt': msg
        };
        jQuery(window).scrollTop(0);
    }

    private requestPermission(permission, permission_req) {

        if (permission && permission.length) {

            for (var per of permission) {

                if (per.name == permission_req) {

                    return true;
                }
            }

        }

        return false;

    }

    // Format data for ag-grid
    private createRowData(data) {
        var rowData:any[] = [];
        // Check data
        if (typeof data.data != 'undefined') {

            data = this._func.formatData(data.data);
            for (var i = 0; i < data.length; i++) {

                let link = "";
                if(data[i].return_sts != 'NW'){
                    link = '<a href="#/putback-process/pb-receipt/' + data[i].return_id +'">'+ data[i].return_num+'</a>';
                } else {

                    link = data[i].return_num;
                }

                rowData.push({
                    return_sts_name: data[i].return_sts_name,
                    return_num: link,
                    odr_num: data[i].odr_num,
                    cus_name: data[i].cus_name,
                    num_of_sku: data[i].num_of_sku,
                    return_qty: data[i].return_qty,
                    created_at: data[i].created_at,
                    created_by: data[i].created_by,
                    return_id: data[i].return_id,
                    return_sts: data[i].return_sts
                })
            }
        }

        this.rowData = rowData;
    }

    // Get ListReturnOrder
    private getListReturnOrder(page = null) {

        this.showLoadingOverlay = true;
        if (!page) page = 1;
        var params = "?page=" + page + "&limit=" + this.perPage;
        params += "&whs_id=" + this.whs_id;
        if (this.objSort && this.objSort['sort_type'] != 'none') {
            params += '&sort[' + this.objSort['sort_field'] + ']=' + this.objSort['sort_type'];
        }
        else {
            params += '&sort[created_at]=desc';
        }
        if (this.searchParams) {
            params += this.searchParams;
        }
        this._pbServices.getListReturnOrder(params)
            .subscribe(
                data => {
                    
                    this.dataList = data.data;
                    this.initPagination(data);
                    this.createRowData(data);
                    this.showLoadingOverlay = false;
                },
                err => {

                    this.parseError(err);
                    this.showLoadingOverlay = false;
                },
                () => {
                }
            );
    }

    // Get customers
    private getCustomers() {

        var $params = '?limit=10000';
        this._pbServices.getCustomersByWH($params)
            .subscribe(
                data => {

                    this.dataListCustomer = data.data;
                },
                err => {

                    this.parseError(err);
                },
                () => {
                }
            );

    }

    private filterList(pageNumber) {
        this.getListReturnOrder(pageNumber);
    }

    private getPage(pageNumber) {
        let arr = new Array(pageNumber);
        return arr;
    }

    // Set params for pagination
    private initPagination(data) {
        var meta = data.meta;
        this.Pagination = meta['pagination'];
        this.Pagination['numLinks'] = 3;
        this.Pagination['tmpPageCount'] = this._func.Pagination(this.Pagination);

    }

    // Event when user filter form
    private onPageSizeChanged($event) {
        this.perPage = $event.target.value;
        if ((this.Pagination['current_page'] - 1) * this.perPage >= this.Pagination['total']) {
            this.currentPage = 1;
        }
        else {
            this.currentPage = this.Pagination['current_page'];
        }
        this.getListReturnOrder(this.currentPage);
    }

    // Edit
    // Navigation to edit page
    private createCartonAndAssignPalet() {
        this.getSelectedRow = 'createCartonAndAssignPalet';
    }



    afterGetSelectedRow($event) {
        var listSelectedItem = $event.data;
        switch ($event.action) {
            case 'createCartonAndAssignPalet':
                if (listSelectedItem.length > 1) {
                    this.messages = this._func.Messages('danger', this._func.msg('PAS03'));
                }
                else {
                    if (listSelectedItem.length < 1) {
                        this.messages = this._func.Messages('danger', this._func.msg('PAS04'));
                    }
                    else {

                        if (listSelectedItem[0].return_sts != 'NW') {

                            this.messages = this._func.Messages('danger', this._func.msg('PBAP004'));
                        } else {

                            this._router.parent.navigateByUrl('/putback-process/putaway/assign-pallet/' + listSelectedItem[0].return_id);
                        }
                    }
                }
                break;
            case 'delete':
                break;
        }

        this.getSelectedRow = false;
    }

    private doSort(objSort) {
        this.objSort = objSort;
        this.getListReturnOrder(this.Pagination.current_page);
    }

    // Search
    private search() {
        this.searchParams = '';
        let params_arr = jQuery("#form-filter").serializeArray();
        for (let i in params_arr) {
            if (params_arr[i].value == "") continue;
            this.searchParams += "&" + params_arr[i].name.trim() + "=" + encodeURIComponent(params_arr[i].value.trim());
        }
        this.getListReturnOrder(1);
    }

    // Reset form
    private reset() {

        jQuery("#form-filter input, #form-filter select").each(function (index) {

            jQuery(this).val("");
        });
        this.searchParams = '';
        this.getListReturnOrder(1);

    }

    // Show error when server die or else
    private parseError(err) {

        this.messages = {'status': 'danger', 'txt': this._func.parseErrorMessageFromServer(err)};
    }

    private expandTable = false;

    private viewListFullScreen() {
        this.expandTable = true;
        setTimeout(() => {
            this.expandTable = false;
        }, 500);
    }
}