import { Component, Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Functions, API_Config } from '../../../common/core/load';

@Injectable()
export class Services {

  public AuthHeader = this._Func.AuthHeader();
  public headerPostJson = this._Func.AuthHeaderPostJson();

  constructor(
    private _Func: Functions,
    private http: Http,
    private _api: API_Config) {
  }

  saveListCSR(orderId, data) {
    return this.http.post(this._api.API_ORDERS_CORE + '/assign-multi-csr/' + orderId, data, { headers: this.headerPostJson })
      .map((res: Response) => res.json());
  }
}
