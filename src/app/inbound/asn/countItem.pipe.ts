import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'countItem'})

export class CountItemPipe implements PipeTransform {

    transform(arrItem, args): number {

        if (args == "") {

            return 0;
        }
        
        var i = 0;
        var count = 0;
        if (arrItem != undefined) {
;
            for (i = 0; i < arrItem.length; i++ )
            {

                if (arrItem[i][args]) {

                    count ++;
                }
            }
        }
        
        return count;
    }
}