import {Component,Input} from '@angular/core';
import {Router} from '@angular/router-deprecated';
@Component({
    selector: 'goBack',
    template:
    '<a class="btn btn-outline red no-margin-left pull-left" (click)="goBack()"><i class="icon-action-undo "></i>Back</a>'
})

export class GoBackComponent{

    constructor(private _router:Router){}
    private _url;
    @Input('url') set url(value:string) {
        this._url=value;
    }
    public goBack(){
        // if exit url 
        if(this._url){
            this._router.parent.navigateByUrl(this._url);
        }else{
            history.back();
        }
    };

}