
import {Component} from '@angular/core';
import {RouteParams} from '@angular/router-deprecated';
import {API_Config} from '../../../../common/core/API_Config';
import {BaseTable} from '../../../../common/core/classes_us/base-table';
import {Functions} from '../../../../common/core/functions';
import {TableFunctions} from '../../../../common/core/table-functions';
import {DateFunctions} from '../../../../common/core/functions/date-functions';
import {BaseService} from '../../../../common/core/classes_us/base-service';
import {TableStructure} from '../../../../common/directives/directives';

@Component ({
    selector: 'containers',
    directives: [
        TableStructure
    ],
    templateUrl: '../../../../common/directives/table-structure/table.component.html',
    providers: [
        TableFunctions,
        DateFunctions,
        BaseService
    ]
})

export class ReceivingDetailsComponent extends BaseTable
{
    public tableID = 'receiving';
    public headerURL = this._api.API_User_Metas + '/rep';
    public title = 'View Receiving Details';

    public skipPageSize = true;
    public skipPagination = true;

    public headerDef = [
        {
            id: 'ver_table',
            value: 1 + Math.random() * 10000
        },
        {
            id: 'cus_name',
            name: 'Client',
            width: 150,
            unsortable: true
        },
        {
            id: 'ctnr_num',
            name: 'Container Name',
            width: 150,
            unsortable: true
        },
        {
            id: 'asn_num',
            name: 'ASN #',
            width: 170,
            unsortable: true
        },
        {
            id: 'asn_hdr_ref',
            name: 'Ref Code',
            width: 150,
            unsortable: true
        },
        {
            id: 'gr_num',
            name: 'Goods Receipt #',
            width: 170,
            unsortable: true
        },
        {
            id: 'carton',
            name: 'Carton',
            width: 120,
            unsortable: true
        },
        {
            id: 'pallet',
            name: 'Pallet',
            width: 120,
            unsortable: true
        },
        {
            id: 'kg',
            name: 'Weight',
            width: 120,
            unsortable: true
        },
        {
            id: 'created_at',
            name: 'GR Complete Date',
            width: 150,
            unsortable: true
        }
    ];

    public permissionPage = 'viewInvoice';

    //**************************************************************************

    constructor (
        private _routeParams: RouteParams,
        private _api: API_Config,
        private _func: Functions,
        private _tableFunc: TableFunctions,
        private _tableService: BaseService,
        private _dateFunctions: DateFunctions
    )
    {
        super();

        this.tableComponent = this;

        this.rowData = [];

        let url = this.printURL = this._api.API_Invoice_Receiving_Details
                + '?gr_hdr_id=' + this._routeParams.get('gr_hdr_id');

        this._tableService.get(url)
            .subscribe(
                data => {

                    this.rowData = data;
                    this.dataExport = data;

                    this.Pagination = {
                        current_page: 1,
                        total: this.rowData.length
                    };
                },
                err => {},
                () => {
                    this.showLoadingOverlay = false;
                }
            );
    }

    //**************************************************************************

}
