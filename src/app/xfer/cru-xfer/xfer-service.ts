import {Component, Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {API_Config, Functions} from '../../common/core/load';

@Injectable()
export class XferService {

  public AuthHeader = this._Func.AuthHeader();
  public AuthHeaderPost = this._Func.AuthHeaderPost();

  constructor(private _Func: Functions, private _API: API_Config, private http: Http) {

  }

  getCustomersByWH($params) {
    return  this.http.get(this._API.API_CUSTOMER_MASTER + '/customer-warehouses' + $params + '&sort[cus_name]=asc' , {headers: this.AuthHeader})
      .map(res => res.json());
  }

  searchItems(params){
    return this.http.get(this._API.API_WH_XFER+'/item-info'+params,{ headers: this.AuthHeader })
      .map((res: Response) => res.json());
  }

  searchLoc(params){
    return this.http.get(this._API.API_WH_XFER_LOC+params,{ headers: this.AuthHeader })
      .map((res: Response) => res.json());
  }

  createXferRecord(data){
    return this.http.post(this._API.API_WH_XFER+'/create-xfer-record', data, { headers: this._Func.AuthHeaderPostJson() })
      .map((res: Response) => res.json().data);
  }

  updateXferRecord(data){
    return this.http.put(this._API.API_WH_XFER+'/edit-xfer-record',data, { headers: this._Func.AuthHeaderPostJson() })
      .map((res: Response) => res.json().data);
  }

  getListItemLocation($params){
    return this.http.get(this._API.API_WH_XFER+'/list-item-location'+$params,{ headers: this.AuthHeader })
      .map((res: Response) => res.json().data);
  }

  geteXferDetail(id){
  return  this.http.get(this._API.API_WH_XFER+'/detail-xfer?wv_tf_id='+id, {headers: this.AuthHeader})
      .map(res => res.json().data);
    /*return {
      "xfer_hdr_id":232323,
      "cus_id": "42575",
      "request_qty": "455",
      "loc_code": "Z1-NY-F-09",
      "loc_id": 29679,
      "sku": "MB7128201",
      "item_id": 18657,
      "cus_upc": "",
      "lot": null,
      "size": "STD",
      "color": "CLEAR/SHINY",
      "wh_xfer_stt": "ádas",
      "items": [
        {
          "xfer_dtl_id":213232,
          "loc_sts": "CL",
          "loc_sts_name": "CYCLE LOC",
          "loc_id": 16463,
          "loc_code": "101-A-L1-03",
          "ctn_ttl": 10,
          "ttl_pieces": "120",
          "disabled": false,
          "selected": true
        },
        {
          "xfer_dtl_id":213232,
          "loc_sts": "CL",
          "loc_sts_name": "CYCLE LOC",
          "loc_id": 16464,
          "loc_code": "101-A-L1-04",
          "ctn_ttl": 10,
          "ttl_pieces": "120",
          "selected": true,
          "disabled": false
        },
        {
          "xfer_dtl_id":213232,
          "loc_sts": "CL",
          "loc_sts_name": "CYCLE LOC",
          "loc_id": 16438,
          "loc_code": "101-B-L1-02",
          "ctn_ttl": 10,
          "ttl_pieces": "120",
          "disabled": false,
          "selected": true
        },
        {
          "xfer_dtl_id":213232,
          "loc_sts": "CL",
          "loc_sts_name": "CYCLE LOC",
          "loc_id": 26113,
          "loc_code": "LCX3",
          "ctn_ttl": 10,
          "ttl_pieces": "120",
          "disabled": false,
          "selected": true
        }
      ]
    };*/

  }

  getEventracking(XFERNumber){
    return this.http.get(this._API.API_GOODS_RECEIPT_MASTER+'/eventracking?owner='+XFERNumber+'&type=wo', {
      headers: this.AuthHeader
    }).map(res => res.json());
  }




}
