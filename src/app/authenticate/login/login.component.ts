import { Component, OnDestroy } from '@angular/core';
import { Router, RouterLink } from '@angular/router-deprecated';
import {Http, Response} from '@angular/http';
import { FORM_DIRECTIVES,ControlGroup,FormBuilder ,Validators} from '@angular/common';
import {API_Config, Functions} from '../../common/core/load';
import { ValidationService } from '../../common/core/validator';
import {WMSMessages} from '../../common/directives/directives';
import {WarningBoxPopup} from "../../common/popup/warning-box-popup";
import {BoxPopupService} from "../../common/popup/box-popup.service";

declare var jQuery:any;

@Component({
    selector: 'login',
     providers:[ValidationService, FormBuilder, BoxPopupService],
    directives: [RouterLink, FORM_DIRECTIVES ,WMSMessages],
    templateUrl: 'login.component.html'
})

export class LoginComponent implements OnDestroy {

    private AuthHeaderNoTK = this._Func.AuthHeaderNoTK();
    private messages;
    public loading:boolean=false;
    loginForm:ControlGroup;
    private submitForm:boolean=false;
    isFirst: boolean = true;
    AppVersion = process.env.CONFIG['VERSION'];
    intervalCheckVersion = 120000;
    intervalCheck: any;

    constructor(
        public router: Router,
        public http: Http,
        private _Func: Functions,
        private _API: API_Config,
        private _Valid: ValidationService,
        private fb: FormBuilder,
        private _boxPopupService: BoxPopupService
    ) {

      let token = localStorage.getItem('jwt');
      if(token != null){
        this.http.get(this._API.API_User_Token, { headers: this._Func.AuthHeader() })
          .map((res: Response) => res.json().data).subscribe(
          data => {
            this.router.parent.navigateByUrl('/dashboard');
          },
          err => {
            this.loginForm =fb.group({
              username: [null, Validators.compose([Validators.required])],
              password: [null,Validators.required]
            });

          }
        );
      }else{
        this.loginForm =fb.group({
          username: [null, Validators.compose([Validators.required])],
          password: [null,Validators.required]
        });
      }


    }

    ngAfterViewInit() {
      this.checkVersionChange();
      this.intervalCheck = setInterval(() => {
          this.checkVersionChange();
      }, this.intervalCheckVersion);
    }

    ngOnDestroy() {
      clearInterval(this.intervalCheck);
    }

    private checkVersionChange() {
        const versionURL = `${window.location.origin}/assets/version/version.txt`;
        const that = this;
        let xhr = new XMLHttpRequest();
        xhr.onreadystatechange = () => {
            if (xhr.readyState == 4 && xhr.status == 200) {
                const curAppVersion = localStorage.getItem(that.AppVersion);
                const newAppVersion = xhr.responseText.toString().trim();

                if (that.isFirst) {
                  that.isFirst = false;
                  localStorage.setItem(that.AppVersion, newAppVersion);
                  return;
                }

                if (!curAppVersion || newAppVersion !== curAppVersion) {
                    let warningPopup = new WarningBoxPopup();
                    warningPopup.text = that._Func.msg('VERSION_CHANGE');
                    that._boxPopupService.showWarningPopup(warningPopup)
                        .then((isOK) => {
                            if (!isOK) {
                                return;
                            } else {
                                localStorage.setItem(that.AppVersion, newAppVersion);
                                window.location.reload(true);
                            }
                        });
                }
            }
        };
        xhr.open('GET', versionURL);
        xhr.send();
    }

    login(data:any) {
        this.submitForm=true;
        if(this.loginForm.valid){

          this.loading=true;
          var creds = "username="+data['username']+"&password="+encodeURIComponent(data['password']);
          this.http.post(this._API.API_URL_LOGIN, creds, {
              headers: this.AuthHeaderNoTK
            })
            .map((res: Response) => res.json())
            .subscribe(
              data => {
                if(data.code == '405'){
                  this.messages = {
                    status: 'danger',
                    txt: data.message + ". Please click <a id='resetPass' href='#/reset-password?token=" + data.token + "'>here</a> to reset password."
                  }
                }else{
                  localStorage.setItem('jwt', data.data.token);
                  localStorage.setItem('id_token', data.data.token);
                  this.router.parent.navigateByUrl('/dashboard');
                }
                this.loading = false;
                //this.router.parent.navigateByUrl('/dashboard');
              },
              err => {
                this.loading = false;
                this.messages=this._Func.Messages('danger',this._Func.parseErrorMessageFromServer(err));
              },
              () => {
                  
              }
            )
        }

    }
    saveJwt(jwt) {
        if (jwt) {
            localStorage.setItem('token', jwt);
        }
    }
    signup(event) {
        event.preventDefault();
        this.router.parent.navigateByUrl('/signup');
        //this.router.navigate(['Signup']);
    }
}
