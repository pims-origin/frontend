import {Component} from '@angular/core';
import {Router, RouterLink, RouteConfig, ROUTER_DIRECTIVES} from '@angular/router-deprecated';
import {PutawayAssignmentListComponent} from "./putaway-assignment-list/putaway-assignment.component";


@Component ({
  selector: 'putaway',
  directives: [ROUTER_DIRECTIVES],
  template: `<router-outlet></router-outlet>`,
})

@RouteConfig([
  { path: '/', component:PutawayAssignmentListComponent  , name: 'PutAway Assignment', useAsDefault: true},

])
export class PutAwayAssignmentComponent {

}
