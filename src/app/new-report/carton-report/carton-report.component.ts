import {Component} from "@angular/core";
import {Router, ROUTER_DIRECTIVES} from '@angular/router-deprecated';
import {ControlGroup, FormBuilder, Control} from '@angular/common';
import {API_Config, UserService, Functions} from '../../common/core/load';
import {BoxPopupService} from "../../common/popup/box-popup.service";
import {CartonReportService} from './carton-report-service';
import {WMSPagination, AgGridExtent, WMSMessages, ReportFileExporter, ProgressExporter} from '../../common/directives/directives';
import 'ag-grid-enterprise/main';

declare var jQuery: any;
declare const $:any;

@Component({
	selector: 'carton-report',
	providers: [CartonReportService, BoxPopupService],
	directives: [ROUTER_DIRECTIVES, AgGridExtent, WMSMessages, WMSPagination, ReportFileExporter, ProgressExporter],
	templateUrl: 'carton-report.component.html',

})
export class CartonReportComponent {
	private pagination:any;
	private perPage:number = 20;
	private whs_id:string;
	private messages:any;
	private searchForm:ControlGroup;
	private sortData = {fieldname: 'updated_at', sort: 'desc'};
	private showLoadingOverlay:boolean = false;
	private cartonReports:Array<any> = [];
	private headerURL:string = this.apiService.API_User_Metas + '/ctr';
	private columnData:any = {
		cus_code: {
			title: 'Customer Code',
			width: 120,
			pin: true,
			sort: true,
			ver_table: '03/04/19'
		},
		cus_name: {
			title: 'Customer Name',
			width: 120,
			pin: true,
			sort: true
		},
		ctnr_num: {
			title: 'Carrier Num',
			width: 120,
			pin: true,
			sort: true
		},
		gr_hdr_num: {
			title: 'GR Num',
			width: 120,
			pin: true,
			sort: true
		},
		piece_remain: {
			title: 'Pieces Remain',
			width: 50,
			sort: true
		},
		upc: {
			title: 'UPC',
			width: 50,
			sort: true
		},
		sku: {
			title: 'SKU',
			width: 50,
			sort: true
		},
		size: {
			title: 'Size',
			width: 50,
			sort: true
		},
		color: {
			title: 'Color',
			width: 50,
			sort: true
		},
		po: {
			title: 'PO',
			width: 50,
			sort: true
		},
		lot: {
			title: 'Lot',
			width: 50,
			sort: true
		},
		length: {
			title: 'Length',
			width: 50,
			sort: true
		},
		width: {
			title: 'Width',
			width: 50,
			sort: true
		},
		height: {
			title: 'Height',
			width: 50,
			sort: true
		},
		weight: {
			title: 'Weight',
			width: 50,
			sort: true
		},
		volume: {
			title: 'Volume',
			width: 50,
			sort: true
		},
		cube: {
			title: 'Cube',
			width: 50,
			sort: true
		},
		ctn_pack_size: {
			title: 'Pack Size',
			width: 50,
			sort: true
		},
		ctn_num: {
			title: 'Carton Num',
			width: 120,
			sort: true
		},
		rfid: {
			title: 'Carton RFID',
			width: 120,
			sort: true
		},
		plt_num: {
			title: 'Pallet Num',
			width: 120,
			sort: true
		},
		plt_rfid: {
			title: 'Pallet RFID',
			width: 120,
			sort: true
		},
		loc_code: {
			title: 'Location',
			width: 80,
			sort: true
		},
		ctn_sts: {
			title: 'Status',
			width: 50,
			sort: true
		},
		created_at: {
			title: 'Created At',
			width: 50,
			sort: true
		},
		expired_dt: {
			title: 'Expired Date',
			width: 50,
			sort: true
		},
		row: {
			title: 'Row',
			width: 50,
			sort: true
		},
		level: {
			title: 'Level',
			width: 50,
			sort: true
		},
		aisle: {
			title: 'Aisle ',
			width: 50,
			sort: true
		},
	};
	private customers:Array<any> = [];		
	private currentSKU:any = {};

	// Check permission for user using this function page
	private canAccess:boolean = false;

	// Export csv file
	private exportAPIUrl:string = '';
	private fileName:string = 'Carton-Report.csv';

	public isLoading: any[] = [];
	public totalRec = '';
	public statusExport = false;
	public limitRow = 5000;

	constructor(private formBuilder:FormBuilder,
	            private cartonReportService:CartonReportService,
	            private func:Functions,
	            private userService:UserService,
	            private apiService:API_Config,
	            private router:Router) {
		this.buildSearchForm();
		this.whs_id = localStorage.getItem('whs_id');
		this.checkPermission();
		this.initializeCurSKU();
	}

	buildSearchForm() {
		this.searchForm = this.formBuilder.group({
			loc_type_code: [''],
			cus_id: [''],
			ctnr_num: [''],
			ctn_num: [''],
			loc_code: [''],
			rfid: [''],
			plt_rfid: [''],
			sku: [''],
			row: [''],
			level: [''],
			aisle: [''],
			upc: [''],
			ctn_sts: [''],
		});
	}
	
	initializeCurSKU() {
		this.currentSKU = {size: '', color: '', lot: '', item_id: ''};
	}

	private checkPermission() {
		this.showLoadingOverlay = true;
		this.userService.GetPermissionUser().subscribe(
			data => {
				this.showLoadingOverlay = false;
				this.canAccess = this.userService.RequestPermission(data, 'viewReport');
				if (this.canAccess) {
					this.getCustomers();
					this.getLocationType();
					this.getCartonReports();
					this.getCartonStatus();
				} else {
					// redirect to deny page.
					this.router.parent.navigateByUrl('/deny');
				}
			},
			err => {
				this.messages = this.func.Messages('danger', this.func.parseErrorMessageFromServer(err));
				this.showLoadingOverlay = false;
			}
		);
	}

	private changePageSize($event:any) {
		this.perPage = $event.target.value;
		if (this.pagination) {
			if ((this.pagination['current_page'] - 1) * this.perPage >= this.pagination['total']) {
				this.pagination['current_page'] = 1;
			}
			this.getCartonReports(this.pagination['current_page']);
		}

	}

	private getCartonReports(pageNum:number = 1) {
		const sortField = this.sortData['fieldname'] ? this.sortData['fieldname'] : 'updated_at';
		const sortValue = this.sortData['sort'] ? this.sortData['sort'] : 'desc';
		this.searchForm.value['page'] = pageNum;
		this.searchForm.value['limit'] = this.perPage;
		this.searchForm.value['item_id'] = this.currentSKU['item_id'];
		this.searchForm.value['lot'] = this.currentSKU['lot'];
		this.searchForm.value['created_at_from'] = jQuery("#created_at_from").val();
		this.searchForm.value['created_at_to'] = jQuery("#created_at_to").val();

		const params = '?' + $.param(this.searchForm.value) + '&sort['+sortField+']=' + sortValue;
		this.exportAPIUrl = this.cartonReportService.getReportAPIUrl(params);
		this.getTotalRec(params);

		this.showLoadingOverlay = true;
		this.cartonReportService.getReports(params)
			.subscribe(
				data => {
					this.showLoadingOverlay = false;
					this.cartonReports = data.data;

					// paging
					this.pagination = data['meta']['pagination'];
					this.pagination['numLinks'] = 3;
					this.pagination['tmpPageCount'] = this.func.Pagination(this.pagination);
				},
				err => {
					this.messages = this.func.Messages('danger', err.json().errors.message);
					this.showLoadingOverlay = false;
				},
				() => {}
			);
	}

	private getCustomers() {
		const params:string = '?limit=200';
		this.cartonReportService.getCustomers(params).subscribe(
			data => {
				this.customers = data['data'];
			},
			err => {
				this.messages = this.func.Messages('danger', err.json().errors.message);
			},
			() => {
			}
		);
	}

	private locationType = [];
	private getLocationType() {
		const params:string = '?limit=200';
		this.cartonReportService.getLocationType(params).subscribe(
			data => {
				this.locationType = data['data'];
			},
			err => {
				this.messages = this.func.Messages('danger', err.json().errors.message);
			},
			() => {
			}
		);
	}
	
	private cartonStatus = [];
	private getCartonStatus() {
		const params:string = '?limit=200';
		this.cartonReportService.getCartonStatus(params).subscribe(
			data => {
				this.cartonStatus = data['data'];
			},
			err => {
				this.messages = this.func.Messages('danger', err.json().errors.message);
			},
			() => {
			}
		);
	}

	// for Autocomplete Container
	private ctnrNums:Array<any> = [];
	
	loadCTNRNums($event:any) {
		const cusId = this.searchForm.value['cus_id'];
		const ctnrNum = this.searchForm.value['ctnr_num'].trim();
		const params = `?cus_id=${cusId}&ctnr_num=${ctnrNum}&limit=20`;
		const enCodeSearchQuery = this.func.FixEncodeURI(params);

		if (!ctnrNum) {
			return;
		}
		this.isLoading['CTNRNums'] = true;
		this.cartonReportService.getCTNRNums(enCodeSearchQuery).debounceTime(400).distinctUntilChanged().subscribe(
			data => {
				this.isLoading['CTNRNums'] = false;
				this.ctnrNums = data['data'];
			},
			err => {
				this.isLoading['CTNRNums'] = false;
				this.messages = this.func.Messages('danger', err.json().errors.message);
			},
			() => {
			}
		);
	}
	
	selectCTNRNum($event, ctnrNum) {
		(<Control>this.searchForm.controls['ctnr_num']).updateValue(ctnrNum['ctnr_num']);
	}
	// *************************************************************************************
	
	
	// for Autocomplete Carton Number
	private cartonNums:Array<any> = [];
	
	loadCartonNums($event:any) {
		const cusId = this.searchForm.value['cus_id'];
		const ctnrNum = this.searchForm.value['ctnr_num'].trim();
		const ctnNum = this.searchForm.value['ctn_num'].trim();
		const params = `?cus_id=${cusId}&ctnr_num=${ctnrNum}&ctn_num=${ctnNum}&limit=20`;
		const enCodeSearchQuery = this.func.FixEncodeURI(params);

		if (!ctnNum) {
			return;
		}
		this.isLoading['CartonNums'] = true;
		this.cartonReportService.getCartonNums(enCodeSearchQuery).debounceTime(400).distinctUntilChanged().subscribe(
			data => {
				this.isLoading['CartonNums'] = false;
				this.cartonNums = data['data'];
			},
			err => {
				this.isLoading['CartonNums'] = false;
				this.messages = this.func.Messages('danger', err.json().errors.message);
			},
			() => {
			}
		);
	}
	
	selectCartonNum($event, cartonNum) {
		(<Control>this.searchForm.controls['ctn_num']).updateValue(cartonNum['ctn_num']);
	}
	// *************************************************************************************
	
	
	
	// for Autocomplete Location
	private locs:Array<any> = [];
	
	loadLocs($event:any) {
		const cusId = this.searchForm.value['cus_id'];
		const ctnrNum = this.searchForm.value['ctnr_num'].trim();
		const ctnNum = this.searchForm.value['ctn_num'].trim();
		const locCode = this.searchForm.value['loc_code'].trim();
		const params = `?cus_id=${cusId}&ctnr_num=${ctnrNum}&ctn_num=${ctnNum}&loc_code=${locCode}&limit=20`;
		const enCodeSearchQuery = this.func.FixEncodeURI(params);

		if (!locCode) {
			return;
		}
		this.isLoading['Locs'] = true;
		this.cartonReportService.getLocs(enCodeSearchQuery).debounceTime(400).distinctUntilChanged().subscribe(
			data => {
				this.isLoading['Locs'] = false;
				this.locs = data['data'];
			},
			err => {
				this.isLoading['Locs'] = false;
				this.messages = this.func.Messages('danger', err.json().errors.message);
			},
			() => {
			}
		);
	}
	
	selectLoc($event, loc) {
		(<Control>this.searchForm.controls['loc_code']).updateValue(loc['loc_code']);
	}
	// *************************************************************************************
	
	
	
	// for Autocomplete Carton RFID
	private rfids:Array<any> = [];
	
	loadRFIDs($event:any) {
		const cusId = this.searchForm.value['cus_id'];
		const ctnrNum = this.searchForm.value['ctnr_num'].trim();
		const ctnNum = this.searchForm.value['ctn_num'].trim();
		const locCode = this.searchForm.value['loc_code'].trim();
		const ctnRFID = this.searchForm.value['rfid'].trim();
		const params = `?cus_id=${cusId}&ctnr_num=${ctnrNum}&ctn_num=${ctnNum}&loc_code=${locCode}&rfid=${ctnRFID}&limit=20`;
		const enCodeSearchQuery = this.func.FixEncodeURI(params);

		if (!ctnRFID) {
			return;
		}
		this.isLoading['RFIDs'] = true;
		this.cartonReportService.getRFIDs(enCodeSearchQuery).debounceTime(400).distinctUntilChanged().subscribe(
			data => {
				this.isLoading['RFIDs'] = false;
				this.rfids = data['data'];
			},
			err => {
				this.isLoading['RFIDs'] = false;
				this.messages = this.func.Messages('danger', err.json().errors.message);
			},
			() => {
			}
		);
	}
	
	selectRFID($event, rfid) {
		(<Control>this.searchForm.controls['rfid']).updateValue(rfid['rfid']);
	}
	// *************************************************************************************
	
	
	
	// for Autocomplete Pallet RFID
	private plt_rfids:Array<any> = [];
	
	loadPalletRFIDs($event:any) {
		const cusId = this.searchForm.value['cus_id'];
		const ctnrNum = this.searchForm.value['ctnr_num'].trim();
		const ctnNum = this.searchForm.value['ctn_num'].trim();
		const locCode = this.searchForm.value['loc_code'].trim();
		const ctnRFID = this.searchForm.value['rfid'].trim();
		const pltRFID = this.searchForm.value['plt_rfid'].trim();
		const params = `?cus_id=${cusId}&ctnr_num=${ctnrNum}&ctn_num=${ctnNum}&loc_code=${locCode}&rfid=${ctnRFID}&plt_rfid=${pltRFID}&limit=20`;
		const enCodeSearchQuery = this.func.FixEncodeURI(params);

		if (!pltRFID) {
			return;
		}
		this.isLoading['PalletRFIDs'] = true;
		this.cartonReportService.getPalletRFIDs(enCodeSearchQuery).debounceTime(400).distinctUntilChanged().subscribe(
			data => {
				this.isLoading['PalletRFIDs'] = false;
				this.plt_rfids = data['data'];
			},
			err => {
				this.isLoading['PalletRFIDs'] = false;
				this.messages = this.func.Messages('danger', err.json().errors.message);
			},
			() => {
			}
		);
	}
	
	selectPalletRFID($event, plt_rfid) {
		(<Control>this.searchForm.controls['plt_rfid']).updateValue(plt_rfid['rfid']);
	}
	// *************************************************************************************
	
	
	
	// for autocomplete SKU
	private SKUs:Array<any> = [];
	
	loadSKUs($event:any) {
		this.initializeCurSKU();
		if (!this.searchForm.value['sku']) {
			return;
		}

		let querySKUsObj = Object.assign({}, this.searchForm.value);
		querySKUsObj['limit'] = 20;

		this.isLoading['SKUs'] = true;
		this.cartonReportService.getSKUs('?' + $.param(querySKUsObj)).debounceTime(400).distinctUntilChanged().subscribe(
			data => {
				this.isLoading['SKUs'] = false;
				this.SKUs = data['data'];
			},
			err => {
				this.isLoading['SKUs'] = false;
				this.messages = this.func.Messages('danger', err.json().errors.message);
			},
			() => {
			}
		);
	}
	
	selectSKU($event, sku) {
		this.currentSKU = sku;
		(<Control>this.searchForm.controls['sku']).updateValue(sku['sku']);
	}
	// *************************************************************************************

	reset() {
		this.resetSearchForm();
		this.initializeCurSKU();
		this.getCartonReports();
	}

	search() {
		this.getCartonReports();
	}

	resetSearchForm() {
		this.func.ResetForm(this.searchForm);
		jQuery("#created_at_from").val("");
		jQuery("#created_at_to").val("");
	}

	private updateMessages(messages) {
		this.messages = messages;
	}

	// for Autocomplete levels
	private levels: any[] = [];
	
	loadLevels($event:any) {
		const level = this.searchForm.value['level'].trim();
		const params = `/loc-level?page=1&limit=20&level=${level}`;
		const enCodeSearchQuery = this.func.FixEncodeURI(params);

		if (!level) {
			return;
		}
		this.isLoading['level'] = true;
		this.cartonReportService.getAutocompleteLocation(enCodeSearchQuery).debounceTime(400).distinctUntilChanged().subscribe(
			data => {
				this.isLoading['level'] = false;
				this.levels = data['data'];
			},
			err => {
				this.isLoading['level'] = false;
				this.messages = this.func.Messages('danger', err.json().errors.message);
			},
			() => {
			}
		);
	}
	
	selectLevel($event, item) {
		(<Control>this.searchForm.controls['level']).updateValue(item['level']);
	}
	// *************************************************************************************
	// for Autocomplete rows
	private rows: any[] = [];
	
	loadRows($event:any) {
		const row = this.searchForm.value['row'].trim();
		const params = `/loc-row?page=1&limit=20&row=${row}`;
		const enCodeSearchQuery = this.func.FixEncodeURI(params);

		if (!row) {
			return;
		}
		this.isLoading['row'] = true;
		this.cartonReportService.getAutocompleteLocation(enCodeSearchQuery).debounceTime(400).distinctUntilChanged().subscribe(
			data => {
				this.isLoading['row'] = false;
				this.rows = data['data'];
			},
			err => {
				this.isLoading['row'] = false;
				this.messages = this.func.Messages('danger', err.json().errors.message);
			},
			() => {
			}
		);
	}
	
	selectRow($event, item) {
		(<Control>this.searchForm.controls['row']).updateValue(item['row']);
	}
	// *************************************************************************************
	// for Autocomplete aisles
	private aisles: any[] = [];
	
	loadAisles($event:any) {
		const aisle = this.searchForm.value['aisle'].trim();
		const params = `/loc-aisle?page=1&limit=20&aisle=${aisle}`;
		const enCodeSearchQuery = this.func.FixEncodeURI(params);

		if (!aisle) {
			return;
		}
		this.isLoading['aisle'] = true;
		this.cartonReportService.getAutocompleteLocation(enCodeSearchQuery).debounceTime(400).distinctUntilChanged().subscribe(
			data => {
				this.isLoading['aisle'] = false;
				this.aisles = data['data'];
			},
			err => {
				this.isLoading['aisle'] = false;
				this.messages = this.func.Messages('danger', err.json().errors.message);
			},
			() => {
			}
		);
	}
	
	selectAisle($event, item) {
		(<Control>this.searchForm.controls['aisle']).updateValue(item['aisle']);
	}
	// *************************************************************************************

	getTotalRec(param) {
		this.cartonReportService.getTotalRec(this.whs_id, param).subscribe(
			data => {
				this.totalRec = data['data']['total'];
				this.statusExport = data['status'];
			}
		);
	}
}
