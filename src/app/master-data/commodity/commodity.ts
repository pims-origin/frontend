import {Component} from '@angular/core';
import {Router, RouterLink, RouteConfig, ROUTER_DIRECTIVES} from '@angular/router-deprecated';
import {CommodityList} from "./commodity-list/commodity-list";
import {CommodityCRU} from "./commodity-cru/commodity-cru";

@Component ({
    selector: 'commodity-selector',
    directives: [ROUTER_DIRECTIVES],
    template: `<router-outlet></router-outlet>`,
})

@RouteConfig([
    { path: '/', component:CommodityList  , name: 'Customer', useAsDefault: true},
    { path: '/new', component:CommodityCRU  , name: 'Add Commodity' , data :{action:'new'}},
    { path: '/:id', component:CommodityCRU , name: 'View Commodity' , data :{action:'view'}},
    { path: '/:id/edit', component:CommodityCRU , name: 'Edit Commodity', data:{action:'edit'}},
])
export class CommodityComponent {
    
}
