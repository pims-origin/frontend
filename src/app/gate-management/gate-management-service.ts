import {Component, Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {API_Config, Functions} from '../common/core/load';

@Injectable()
export class GateManagementServices {

  public AuthHeader = this._Func.AuthHeader();
  public AuthHeaderPost = this._Func.AuthHeaderPost();

  constructor(private _Func: Functions, private _API: API_Config, private http: Http) {

  }

  getCustomersByWH($params) {
    return  this.http.get(this._API.API_Customer_List_All + $params + '&sort[cus_name]=asc' , {headers: this.AuthHeader})
      .map(res => res.json());
  }

  getListContainer() {
    return  this.http.get(this._API.API_GOODS_RECEIPT + '/containers?limit=1000', {headers: this.AuthHeader})
      .map(res => res.json());
  }

  getGateList() {
    return this.http.get(this._API.API_Container_Plug +'/gate/gates',{ headers: this.AuthHeader })
      .map((res: Response) => res.json());
  }

  addNew(gateId, stringData) {
    return this.http.post(this._API.API_Container_Plug + '/gate/gate-dtls/' + gateId, stringData, { headers: this._Func.AuthHeaderPostJson() })
        .map((res: Response) => res.json());
  }

  update(containerID, stringData) {
    return this.http.put(this._API.API_Container_Plug + '/gate/gate-dtls/' + containerID, stringData, { headers: this._Func.AuthHeaderPostJson() })
        .map((res: Response) => res.json());
  }

  public getContainerByName ($params) {
    return this.http.get(this._API.API_Container + $params, {headers: this.AuthHeader})
        .map(res => res.json().data);
  }
}
