export class ChargeCode {
    constructor(
        public id: number,
        public name: string,
        public chgTypeId: string,
        public uomId: string,
        public des: string
    ) { 
    }

}