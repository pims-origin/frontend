import {isDate} from "rxjs/util/isDate";
export default class RefData {


  static firstNames = ["Nhan", "Huy"];

  static lastNames = ["Truong", "Pham"];

  static addresses = [
    '1197 Thunder Wagon Common, Cataract, RI, 02987-1016, US, (401) 747-0763',
    '3685 Rocky Glade, Showtucket, NU, X1E-9I0, CA, (867) 371-4215'];

  static email=["nganha@logisoft.com"];

  static phone= ["012345678910"];

  static userRole = ["Manager","Vendor"];

  static birthDay=["21/05/1987"];

  static status =["Active","Inactive"];
}

