import { Pipe, PipeTransform } from '@angular/core';
import {DateFormatter} from "@angular/common/src/facade/intl";

@Pipe({name: 'dateFormat'})

export class DateFormat implements PipeTransform {
    transform(value: any, args: string[]): any {
        if (value) {

            var date = value instanceof Date ? value : new Date(parseInt(value) * 1000);
            var str_date = ("0" + (date.getMonth() + 1)).slice(-2) +  "/" + ("0" + date.getDate()).slice(-2) + "/" + date.getFullYear();
            return str_date;

        }
    }
}