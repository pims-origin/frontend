/*
* Only return data
* */
import {Component, Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {API_Config, Functions} from '../../common/core/load';


@Injectable()
export class CustomerWarehouseService {

  private headerGet = this._Func.AuthHeader();
  private urlAPICustomerMaster = this._API.API_CUSTOMER_MASTER;

  constructor(private _Func: Functions, private _API: API_Config, private _http: Http)
  {

  }
  
  // Get customers by WH
    public getCustomersByWH ($params = '') {
//console.log(this._api.API_CUSTOMER_WAREHOUSES_1 + '/customer-warehouses' + $params + '&sort[cus_name]=asc');
//        return this._http.get(this._api.API_CUSTOMER_WAREHOUSES_1 + '/customer-warehouses' + $params + '&sort[cus_name]=asc' , {headers: this.headerGet})
//            .map(res => res.json());


        return this._http.get(this._API.API_CUSTOMER_NAMES, {
            headers: this.headerGet
        }).map(res => res.json());

    }
}

