import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {API_Config, Functions} from '../../common/core/load';

@Injectable()
export class SKUTrackingService {
	public AuthHeader = this._Func.AuthHeader();
	public AuthHeaderPost = this._Func.AuthHeaderPost();

	constructor(private _Func:Functions, private _API:API_Config, private http:Http) {
	}

	getReport(wh_id, params: string) {
		return this.http.get(`${this._API.API_REPORTS_CORE}/sku-tracking/reports/${wh_id}` + params, {headers: this.AuthHeader})
			.map((res:Response) => res.json());
	}

	getReportAPIUrl(wh_id, params: string) {
		return `${this._API.API_REPORTS_CORE}/sku-tracking/reports/${wh_id}${params}&export=1`;
	}

	// getCustomersByWH(params) {
	// 	return this.http.get(`${this._API.API_INVENTORY_NEW_REPORTS}/customers${params}&sort[cus_name]=asc`, {headers: this.AuthHeader})
	// 		.map(res => res.json());
	// }

	getCustomersByUser(params) {
		return this.http.get(`${this._API.API_Customer_User}${params}&sort[cus_name]=asc`, {headers: this.AuthHeader})
			.map(res => res.json());
	}

	getSKUs(params: string) {
		return this.http.get(`${this._API.API_REPORTS_CORE}/sku-tracking/auto-sku-tracking${params}`, {headers: this.AuthHeader})
			.map(res => res.json());

	}

}
