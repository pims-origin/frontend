import { Component } from '@angular/core';
import { Router, ROUTER_DIRECTIVES } from '@angular/router-deprecated';
import { ControlGroup, FormBuilder, Control } from '@angular/common';
import { API_Config, UserService, Functions } from '../../common/core/load';
import { BoxPopupService } from '../../common/popup/box-popup.service';
import { StorageReportService } from './storage-report.service';
import { WMSPagination, AgGridExtent, WMSMessages, ReportFileExporter } from '../../common/directives/directives';
import 'ag-grid-enterprise/main';

declare const $: any;

@Component({
  selector: 'storage-report',
  providers: [StorageReportService, BoxPopupService],
  directives: [ROUTER_DIRECTIVES, AgGridExtent, WMSMessages, WMSPagination, ReportFileExporter],
  templateUrl: 'storage-report.component.html'
})
export class StorageReportComponent {
  pagination: any;
  perPage: number = 20;
  whs_id: string;
  messages: any;
  searchForm: ControlGroup;
  sortData = { fieldname: 'updated_at', sort: 'desc' };
  showLoadingOverlay: boolean = false;
  grReports: Array<any> = [];
  headerURL: string = this.apiService.API_User_Metas + '/srr';
  columnData: any = {
    cus_name: {
      title: 'Customer Name',
      width: 200,
      pin: true,
      ver_table: '2'
    },
    rac: {
      title: 'Rack',
      width: 200,
      pin: true
    },
    bin: {
      title: 'Bin',
      width: 200,
      pin: true
    },
    she: {
      title: 'Shelf',
      width: 200,
      pin: true
    },
    num_of_loc: {
      title: 'Total Locations',
      width: 200,
      pin: true
    }
  };
  customers: any[] = [];
  public isLoading: any[] = [];

  // Check permission for user using this function page
  canAccess: boolean = false;

  // Export csv file
  exportAPIUrl: string = '';
  fileName: string = 'Storage-Report.csv';

  constructor(
    private formBuilder: FormBuilder,
    private service: StorageReportService,
    private func: Functions,
    private userService: UserService,
    private apiService: API_Config,
    private router: Router
  ) {
    this.buildSearchForm();
    this.whs_id = localStorage.getItem('whs_id');
    this.checkPermission();
  }

  buildSearchForm() {
    this.searchForm = this.formBuilder.group({
      cus_id: ['']
    });
  }

  checkPermission() {
    this.showLoadingOverlay = true;
    this.userService.GetPermissionUser().subscribe(
      (data) => {
        this.showLoadingOverlay = false;
        this.canAccess = this.userService.RequestPermission(data, 'viewReport');
        if (this.canAccess) {
          this.getCustomers();
          this.getReports();
        } else {
          // redirect to deny page.
          this.router.parent.navigateByUrl('/deny');
        }
      },
      (err) => {
        this.messages = this.func.Messages('danger', this.func.parseErrorMessageFromServer(err));
        this.showLoadingOverlay = false;
      }
    );
  }

  changePageSize($event: any) {
    this.perPage = $event.target.value;
    if (this.pagination) {
      if ((this.pagination['current_page'] - 1) * this.perPage >= this.pagination['total']) {
        this.pagination['current_page'] = 1;
      }
      this.getReports(this.pagination['current_page']);
    }
  }

  getReports(pageNum: number = 1) {
    const sortField = this.sortData['fieldname'] ? this.sortData['fieldname'] : 'updated_at';
    const sortValue = this.sortData['sort'] ? this.sortData['sort'] : 'desc';
    this.searchForm.value['page'] = pageNum;
    this.searchForm.value['limit'] = this.perPage;
    this.searchForm.value['date_from'] = $('#created_at_from').val();
    3;
    this.searchForm.value['date_to'] = $('#created_at_to').val();

    const params = '?' + $.param(this.searchForm.value) + '&sort[' + sortField + ']=' + sortValue;
    this.exportAPIUrl = this.service.getReportAPIUrl(params);
    this.showLoadingOverlay = true;
    this.service.getReports(params).subscribe(
      (data) => {
        this.showLoadingOverlay = false;
        this.grReports = data.data;

        // paging
        this.pagination = data['meta']['pagination'];
        this.pagination['numLinks'] = 3;
        this.pagination['tmpPageCount'] = this.func.Pagination(this.pagination);
      },
      (err) => {
        this.messages = this.func.Messages('danger', err.json().errors.message);
        this.showLoadingOverlay = false;
      },
      () => {}
    );
  }

  getCustomers() {
    const params: string = '?limit=2000';
    this.service.getCustomers(params).subscribe(
      (data) => {
        this.customers = data['data'];
      },
      (err) => {
        this.messages = this.func.Messages('danger', err.json().errors.message);
      },
      () => {}
    );
  }

  reset() {
    this.resetSearchForm();
    this.getReports();
  }

  search() {
    this.getReports();
  }

  resetSearchForm() {
    this.func.ResetForm(this.searchForm);

    $('#created_at_from').val('');
    $('#created_at_to').val('');
  }

  updateMessages(messages) {
    this.messages = messages;
  }
}

