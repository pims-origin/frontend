import {Component, Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {API_Config,Functions} from '../../../common/core/load';

@Injectable()
export class orderFlowServices {

    public AuthHeader = this._Func.AuthHeader();
    public AuthHeaderPost = this._Func.AuthHeaderPost();

    constructor(private _Func: Functions,
                private _API: API_Config,
                private http: Http) {

    }

    getOrderFlow(param){
        
        return this.http.get( param ,{ headers: this.AuthHeader })
            .map((res: Response) => res.json());

    }
    
}
