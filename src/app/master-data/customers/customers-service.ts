import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {API_Config, Functions} from '../../common/core/load';

@Injectable()

export class CustomersService {

    public AuthHeader = this._Func.AuthHeader();
    public AuthHeaderPost = this._Func.AuthHeaderPost();
    public url = this._API.API_Customers;

    constructor(
        private _Func: Functions,
        private _API: API_Config,
        private _http: Http
    )
    {

    }

    //**************************************************************************

    getDropdown()
    {
        let params = '?sort[cus_name]=asc';

        return this._http.get(this.url + '/dropdown' + params, {
            headers: this.AuthHeader
        })
        .map((res: Response) => res.json());
    }

    //**************************************************************************

}