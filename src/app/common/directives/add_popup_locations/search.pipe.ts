import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name : 'searchPipe',
})
export class SearchPipe implements PipeTransform {
    public transform(value:any, key: string, term: string) {

        if(term){
            term = term.toUpperCase();
        }

        return value.filter((item:any) => {

            if (item.hasOwnProperty(key)) {
                if (term) {
                    let str = item[key];
                    let regExp = new RegExp(term, 'g');
                    return str.match(regExp) ? true : false;
                } else {
                    return true;
                }
            } else {
                return false;
            }
        });
    }
}