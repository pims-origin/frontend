export const DEFAULT_STYLES = `

.table .ag-root .ag-header .ag-header-row .ag-header-cell .ag-header-cell-label
{
  background: url('images/sort_both.png');
  background-repeat: no-repeat;
  background-position: center right;
  cursor: pointer;
}

.ag-grid-ng2-extent
{
position:relative;
}
.btn-expand
{
  position: absolute;
  z-index: 999;
  left: 95px;
  top: -26px;
}

`;
