import { BaseClass } from './base-class';

export class BaseTable extends BaseClass {
    public headerURL = '';
    public pinCols = -1;
    public originalData: any[];
    public rowData: any[];
    public searchParams: string;
    public controlSearch: string;
    public headerExport = <any>[];
    public dataExport: any[];
    public Pagination = <any>{};
    public headerDef = <any>{};
    public currentPage = 1;
    public perPage = 20;
    public numLinks = 3;
    public messages: any;
    public showLoadingOverlay = false;
    public tableComponent: {};
    public printURL = '';
    public currentURL = '';

    public unsortableColumns = [];
    public searchFields: {};

    public columnList = {
        customer: {
            id: 'cus_name',
            name: 'CUST',
            width: 150
        },
        userName: {
            id: 'user_name',
            name: 'CSR',
            width: 150,
        },
        container: {
            id: 'ctnr_num',
            name: 'CNTR',
            width: 140
        },
        receivingNumber: {
            id: 'gr_hdr_num',
            name: 'RCV NUM',
            width: 150
        },
        measurementSystem: {
            id: 'sys_mea_name',
            name: 'Measurement System',
            width: 160
        },
        createdAt: {
            id: 'created_at',
            name: 'Set Date',
            width: 120
        },
        upc: {
            id: 'upc',
            name: 'UPC',
            width: 120
        },
        sku: {
            id: 'sku',
            name: 'SKU',
            width: 150
        },
        length: {
            id: 'length',
            name: 'Length',
            width: 70
        },
        width: {
            id: 'width',
            name: 'Width',
            width: 70
        },
        height: {
            id: 'height',
            name: 'Height',
            width: 70
        },
        volume: {
            id: 'volume',
            name: 'VOL',
            width: 150
        },
        weight: {
            id: 'weight',
            name: 'Weight',
            width: 70
        },
        po: {
            id: 'po',
            name: 'PO',
            width: 70
        },
        lot: {
            id: 'lot',
            name: 'Lot',
            width: 70
        },
        size: {
            id: 'size',
            name: 'Size',
            width: 50
        },
        color: {
            id: 'color',
            name: 'Color',
            width: 140
        },
        cartonPackSize: {
            id: 'ctn_pack_size',
            name: 'Pack Size',
            width: 100
        },
        actualCartons: {
            id: 'actual_cartons',
            name: 'Actual Cartons',
            width: 120
        },
        totalCartons: {
            id: 'total_cartons',
            name: 'Cartons',
            width: 110
        },
        totalPieces: {
            id: 'total_pieces',
            name: 'Pieces',
            width: 110
        },
        pieceRemain: {
            id: 'piece_remain',
            name: 'Pieces Remain',
            width: 120
        },
        cartonNumber: {
            id: 'ctn_num',
            name: 'Carton Number',
            width: 180
        },
        plateNumber: {
            id: 'plt_num',
            name: 'PL',
            width: 180
        },
        location: {
            id: 'loc_code',
            name: 'Location',
            width: 120
        },
        location_name: {
            id: 'loc_name',
            name: 'Location',
            width: 120
        },
        locationAlternativeName: {
            id: 'loc_alternative_name',
            name: 'Alternative Name',
            width: 150
        },
        zoneName: {
            id: 'zone_name',
            name: 'Zone Name',
            width: 120,
        },
        locationType: {
            id: 'loc_type_name',
            name: 'Type',
            width: 130,
        },
        locationStatus: {
            id: 'loc_sts_name',
            name: 'Status',
            width: 80,
        },
        locationAvailableCapacity: {
            id: 'loc_available_capacity',
            name: 'Capacity',
            width: 80
        },
        locationLength: {
            id: 'loc_length',
            name: 'Length',
            width: 70
        },
        locationWidth: {
            id: 'loc_width',
            name: 'Width',
            width: 70
        },
        locationHeight: {
            id: 'loc_height',
            name: 'Height',
            width: 70
        },
        locationMaxWeight: {
            id: 'loc_max_weight',
            name: 'Weight',
            width: 70
        },
        locationWeightCapacity: {
            id: 'loc_weight_capacity',
            name: 'Weight Capacity',
            width: 130
        },
        locationMinCount: {
            id: 'loc_min_count',
            name: 'Min Count',
            width: 90
        },
        locationMaxCount: {
            id: 'loc_max_count',
            name: 'Max Count',
            width: 95
        },
        cartonStatus: {
            id: 'ctn_sts',
            name: 'Status',
            width: 70
        },
        customerStatus: {
            id: 'cus_status',
            name: 'Status',
            width: 80
        },
        customerCode: {
            id: 'cus_code',
            name: 'Code',
            width: 140
        },
        totalCustomer: {
            id:'ttl_cus',
            name: 'TTL_CUS',
            width: 140
        },
        country: {
            id: 'country',
            name: 'Country',
            width: 100
        },
        customerCuntry: {
            id: 'cus_country',
            name: 'Country',
            width: 100
        },
        state: {
            id: 'state',
            name: 'State',
            width: 100
        },
        customerState: {
            id: 'cus_state',
            name: 'State',
            width: 100
        },
        customerCityName: {
            id: 'cus_city_name',
            name: 'City',
            width: 100
        },
        customerPostalCode: {
            id: 'cus_postal_code',
            name: 'ZIP',
            width: 80
        },
        customerBillingAccount: {
            id: 'cus_billing_account',
            name: 'Billing Account',
            width: 140
        },
        customerDescription: {
            id: 'cus_des',
            name: 'Description',
            width: 140
        },
        netTerms: {
            id: 'net_terms',
            name: 'Net Terms',
            width: 140
        },
        customerContactFirstName: {
            id: 'cus_ctt_fname',
            name: 'Contact First Name',
            width: 150
        },
        customerContactLastName: {
            id: 'cus_ctt_lname',
            name: 'Contact Last Name',
            width: 150
        },
        customerContactPosition: {
            id: 'cus_ctt_position',
            name: 'Contact Position',
            width: 150
        },
        customerContactDepartment: {
            id: 'cus_ctt_department',
            name: 'Contact Department',
            width: 160
        },
        customerContactEmail: {
            id: 'cus_ctt_email',
            name: 'Contact Email',
            width: 200
        },
        customerContactPhone: {
            id: 'cus_ctt_phone',
            name: 'Contact Phone',
            width: 120
        },
        customerContactExtention: {
            id: 'cus_ctt_ext',
            name: 'Extention',
            width: 100
        },
        customerContactMobile: {
            id: 'cus_ctt_mobile',
            name: 'Mobile',
            width: 100
        },
        orderType: {
            id: 'odr_type',
            name: 'Order Type',
            width: 110
        },
        orderNumber: {
            id: 'odr_num',
            name: 'Order Number',
            width: 130
        },
        orderStatus: {
            id: 'odr_sts',
            name: 'Order Status',
            width: 110
        },
        customerPO: {
            id: 'cus_po',
            name: 'Customer PO',
            width: 170,
        },
        customerOrderNumber: {
            id: 'cus_odr_num',
            name: 'Customer Order Number',
            width: 190
        },
        shipToName: {
            id: 'ship_to_name',
            name: 'Ship To Name',
            width: 120
        },
        shipToAddress: {
            id: 'ship_to_address',
            name: 'Ship To Address',
            width: 180
        },
        shipToCity: {
            id: 'ship_to_city',
            name: 'Ship To City',
            width: 120
        },
        shipToState: {
            id: 'ship_to_state',
            name: 'Ship To State',
            width: 120
        },
        shipToCountry: {
            id: 'ship_to_country',
            name: 'Ship To Country',
            width: 140
        },
        shipToZip: {
            id: 'ship_to_zip',
            name: 'Ship To Zip',
            width: 100
        },
        productDescription: {
            id: 'product_description',
            name: 'Description',
            width: 120
        },
        warehouse: {
            id: 'whs_name',
            name: 'Warehouse',
            width: 140
        },
        warehouseCity: {
            id: 'cus_ware_city',
            name: 'Warehouse',
            width: 140
        },
        qty: {
            id: 'qty',
            name: 'Qty',
            width: 70
        },
        pieces: {
            id: 'pieces',
            name: 'Pieces',
            width: "*",
        },
        orderRequestDate: {
            id: 'odr_req_dt',
            name: 'Order Date',
            width: 110
        },
        shipByDate: {
            id: 'ship_by_dt',
            name: 'Ship By Date',
            width: 120
        },
        cancelByDate: {
            id: 'cancel_by_dt',
            name: 'Cancel By Date',
            width: 130
        },
        carrier: {
            id: 'carrier',
            name: 'Carrier',
            width: 110
        },
        cusNotes: {
            id: 'cus_notes',
            name: 'Customer Notes',
            width: 150,
        },
        website: {
            id: 'website',
            name: 'Web Site',
            width: 100
        },
        waveNumber: {
            id: 'wv_num',
            name: 'Wave Number',
            width: 190
        },
        customerUPC: {
            id: 'cus_upc',
            name: 'Customer UPC',
            width: 150
        },
        packSize: {
            id: 'pack_size',
            name: 'Pack Size',
            width: 80
        },
        actPackSize: {
            id: 'pack_size',
            name: ' ACT Pack Size',
            width: 80
        },
        expPackSize: {
            id: 'exp_pack_size',
            name: ' EXP Pack Size',
            width: 80
        },
        expCtnTtl: {
            id: 'exp_ctn_ttl',
            name: 'EXP CTN TTL',
            width: 80
        },
        actCtnTtl: {
            id: 'act_ctn_ttl',
            name: 'ACT CTN TTL',
            width: 80
        },
        packDisc: {
            id: 'pack_disc',
            name: 'PACK DISCREPANCY',
            width: 80
        },
        ctnDisc: {
            id: 'ctn_disc',
            name: 'CTN DISCREPANCY',
            width: 80
        },
        cartonQty: {
            id: 'ctn_qty',
            name: 'Carton Qty',
            width: 150,
            unsortable: true
        },
        pieceQty: {
            id: 'piece_qty',
            name: 'Piece Qty',
            width: 120
        },
        actualPieceQty: {
            id: 'act_piece_qty',
            name: 'Actual Piece Qty',
            width: 150
        },
        primaryLocation: {
            id: 'primary_loc',
            name: 'Primary Location',
            width: 150
        },
        buLoc_1: {
            id: 'bu_loc_1',
            name: 'BU_Loc_1',
            width: 110
        },
        buLoc_2: {
            id: 'bu_loc_2',
            name: 'BU_Loc_2',
            width: 110
        },
        buLoc_3: {
            id: 'bu_loc_3',
            name: 'BU_Loc_3',
            width: 110
        },
        buLoc_4: {
            id: 'bu_loc_4',
            name: 'BU_Loc_4',
            width: 110
        },
        buLoc_5: {
            id: 'bu_loc_5',
            name: 'BU_Loc_5',
            width: 110
        },
        actualLocation: {
            id: 'act_loc',
            name: 'Actual Location',
            width: 120
        },
        waveStatus: {
            id: 'wv_dtl_sts',
            name: 'Wave Status',
            width: 110
        },
        logDate: {
            id: 'log_date',
            name: 'LogDate',
            width: 90
        },

        //Leon Thanikal 13/7/2017
        asnFullName: {
            id: 'asn_full_name',
            name: 'Created By',
            width: 90
        },
        asnCreatedAt: {
            id: 'asn_created_at',
            name: 'ASN DT',
            width: 120
        },
        asnttlsku: {
            id: 'asn_ttl_sku',
            name: 'TTL ASN SKU',
            width: 90
        },
        asnttlcarton: {
            id: 'asn_ttl_ctn',
            name: 'TTL ASN CTN',
            width: 90
        },
        asnttlpieces: {
            id: 'asn_ttl_pcs',
            name: 'TTL ASN PCS',
            width: 90
        },
        grFullName: {
            id: 'gr_full_name',
            name: 'Created By',
            width: 90
        },
        grCreatedAt: {
            id: 'gr_created_at',
            name: 'Putaway Date',
            width: 120
        },
        grEptDt: {
            id: 'gr_ept_dt',
            name: 'CTNR ARRIVING',
            width: 120
        },
        dtDiff: {
            id: 'dateDiff',
            name: 'DAYS OLD',
            width: 120
        },
        billingDt: {
            id: 'billing',
            name: 'BILLING',
            width: 120
        },
        grttlsku: {
            id: 'gr_ttl_sku',
            name: 'TTL GR SKU',
            width: 90
        },
        grttlcarton: {
            id: 'gr_ttl_ctn',
            name: 'TTL GR CTNS',
            width: 90
        },
        grttlpieces: {
            id: 'gr_ttl_pcs',
            name: 'TTL GR PCS',
            width: 90
        },
        grttlplt: {
            id: 'gr_ttl_plt',
            name: 'PLT PER CTNR',
            width: 90
        },
        ttlflrplt: {
            id: 'ttl_flr_plt',
            name: 'FLR PLT PER CTNR',
            width: 90
        },
/******************************************************************************/        
        grdtlpalletotal: {
            id: 'gr_dtl_plt_ttl',
            name: 'TTL GR PLT',
            width: 90
        },
        asnNumber:{
            id: 'asn_number',
            name: 'ASN NUM',
            width: 125
        },
         addressType: {
            id: 'cus_bill_ship',
            name: 'AddressType',
            width: 50
        },
        addressLine1: {
            id: 'cus_add_line_1',
            name: 'Address Line 1',
            width: 120
        },
        addressLine2: {
            id: 'cus_add_line_2',
            name: 'Address Line 2',
            width: 120
        },
        addressCity: {
            id: 'cus_add_city_name',
            name: 'Address City',
            width: 120
        },
        addressState: {
            id: 'cus_add_state',
            name: 'Address State',
            width: 120
        },
        addressPostalCode: {
            id: 'cus_add_postal_code',
            name: 'Address Postal Code',
            width: 120
        },
        addressCountry: {
            id: 'cus_add_country',
            name: 'Address Country',
            width: 120
        },
        uom: {
            id: 'UOM',
            name: 'UOM',
            width: 120
        },
        totalcu:{
            id:'total_CU',
            name:'Total Cube',
            widyh:120
        },
        cupercarton:{
            id:'carton_cube',
            name:'Carton Cube',
            width:120
        },
        ctn_cuft: {
            id:'ctn_cuft',
            name:'CTN CUFT',
            width:120
        },
         ctn_qty: {
            id:'ctn_qty',
            name:'CTN QTY',
            width:120
        },
         enterby: {
            id:'enter_by',
            name:'Enter By',
            width:120
        },
         checkoutby: {
            id:'chk_out_by',
            name:'Checkout By',
            width:120
        },
         createdBy: {
            id:'created_by',
            name:'Created By',
            width:120
        },
         eComm: {
            id:'eComm',
            name:'ECOMM',
            width:120
        },
         conditions: {
            id:'conditions',
            name:'Conditions',
            width:120
        },
         daysLeft: {
            id:'daysLeft',
            name:'Days Left',
            width:120
        },
         dtOdrReq: {
            id:'dtOdrReq',
            name:'Date Ord Req',
            width:120
        },
         tmOdrReq: {
            id:'tmOdrReq',
            name:'Time Ord Req',
            width:120
        },
         docoDt: {
            id:'docoDt',
            name:'Dt Ord Chk Out',
            width:120
        },
         dpcoDt: {
            id:'dpcoDt',
            name:'Dtt Pick Chk Out',
            width:120
        },
         dcPerson: {
            id:'dcPerson',
            name:'DC Person',
            width:120
        },
         opcoDt: {
            id:'opcoDt',
            name:'Ord Prc Chk Out',
            width:120
        },
         dsciDt: {
            id:'dsciDt',
            name:'Dt Ship Chk In',
            width:120
        },
         shippedDt: {
            id:'shipped_dt',
            name:'Dt Ord Shipped',
            width:120
        },                    
        cube: {
            id:'cube',
			name: 'Cube',
			width: 90
        },
        ctns: {
            id:'ctns',
			name: 'CTNS',
			width: 90
        },
        grDate: {
            id:'asn_hdr_act_dt',
			name: 'Goods Receipt Date',
			width: 120
        }
    };

    public hiddenColumns = {
        customerContactID: {
            id: 'cus_ctt_id',
            name: '',
            width: 0,
            hide: true
        }
    };

    public searchList = {
        customer: {
            caption: 'Customer',
            name: 'customer',
            field: 'cus_id',
            dropdown: {
                source: [],
                idField: 'cus_id',
                nameField: 'cus_name',
                service: '_customersService'
            }
        },
        container: {
            caption: 'Container #',
            name: 'ctnr_num',
            autocomplete: []
        },
        carton: {
            caption: 'Carton #',
            name: 'ctn_num',
            autocomplete: []
        },
        location: {
            caption: 'Location',
            name: 'loc_code',
            autocomplete: []
        },
        locationType: {
            caption: 'Type',
            name: 'loc_type_name',
            autocomplete:[]
        },
        plateNumber: {
            caption: 'LP',
            name: 'plt_num',
            autocomplete:[]
        },
        totalCartons: {
            caption: 'TTL_Cartons',
            name: 'total_cartons'
        },
        totalPieces:{
            caption: 'TTL_Pieces',
            name: 'total_pieces'
        },
        totalCustomer: {
            caption: 'TTL_CUS',
            name: 'ttl_cus'
        },

        upc: {
            caption: 'UPC',
            name: 'item_code',
            autocomplete: []
        },
        sku: {
            caption: 'SKU',
            name: 'sku',
            // autocomplete: []
        },
        customerCode: {
            caption: 'Code',
            name: 'cus_code'
        },
        customerStatus: {
            caption: 'Status',
            name: 'customer_status',
            field: 'cus_status',
            dropdown: {
                source: [],
                idField: 'cus_sts_code',
                nameField: 'cus_sts_name',
                service: '_customerStatusesService'
            }
        },
        country: {
            caption: 'Country',
            name: 'country_name',
            dropdown: {
                source: [],
                idField: 'sys_country_id',
                nameField: 'sys_country_name',
                service: '_countryService'
            }
        },
        state: {
            caption: 'State',
            name: 'state_name',
            dropdown: {
                source: [],
                idField: 'sys_state_id',
                nameField: 'sys_state_name',
                service: '_stateService'
            }
        },
        orderNumber: {
            caption: 'Order #',
            name: 'odr_num',
            compareOperator: 'like',
            autocomplete: []
        },
        orderType: {
            caption: 'Order Type',
            name: 'odr_type'
        },
        logDate: {
            caption: 'Log Date',
            name: 'log_date',
            datePicker: true
        },
        customerPO: {
            caption: 'Customer PO',
            name: 'cus_po',
            compareOperator: 'like',
            autocomplete: []
        },
        customerOrderNumber: {
            caption: 'Customer Order #',
            name: 'cus_odr_num',
            compareOperator: 'like',
            autocomplete: []
        },
        orderRequestDate: {
            caption: 'Order Date',
            name: 'odr_req_dt',
            datePicker: true
        },
        createdAt: {
            caption: 'Create DT',
            name: 'created_at',
            datePicker: true
        },
        waveNumber: {
            caption: 'Wave #',
            name: 'wv_num',
            autocomplete: []
        },
        lot: {
            caption: 'Lot',
            name: 'lot',
            autocomplete: []
        },
        primaryLocation: {
            caption: 'Primary Location',
            name: 'primary_loc',
            datePicker: true
        },
        from: {
            caption: 'From',
            name: 'from',
            datePicker: true
        },
        to: {
            caption: 'To',
            name : 'to',
            datePicker:true

        },
        
        createdGrStart: {
            caption: 'Start GR Create DT',
            name: 'created_gr_start',
            datePicker: true
        },
        createdGrFinish: {
            caption: 'End GR Create DT',
            name: 'created_gr_finish',
            datePicker: true
        },
        upc_dim: {
            caption: 'UPC',
            name: 'upc',
            autocomplete:[]
        }
        
    };

    //**************************************************************************

    constructor() {
        super();

        this.controlSearch = 'whs_id=' + this.whs_id;
    }

    //**************************************************************************

}
