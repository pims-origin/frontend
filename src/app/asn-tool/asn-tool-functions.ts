/*
* ANSToolFunction
* @tien.nguyen
* */
import {Component} from '@angular/core';
import { ControlGroup, ControlArray, Control} from '@angular/common';
import {ASN_Services} from './asn-services';
import {Functions} from '../common/core/load';
import {FormBuilderFunctions} from '../common/core/formbuilder.functions';
declare var jQuery: any;
@Component ({
    providers: [ASN_Services]
})
export class AsnToolFunctions
{
    constructor(private asnService:ASN_Services,private _globalFunc:Functions,private fbdFunc:FormBuilderFunctions) {}

    /*===============================
     * checkTotalLess9999
     *=============================== */
    public  checkTotalExpCarton(controlArray:ControlArray){

        let expcart=controlArray.value['details'];
        let expcartTotal=0;
        for (let i in expcart){
            expcartTotal=expcartTotal+parseInt(expcart[i]['dtl_ctn_ttl']);
        }
        if(expcartTotal>9999){
            // set error for the dtl_ctn_ttl control on all rows
            for (let i in expcart){
                this.fbdFunc.setErrorsControlItem(controlArray,i,'dtl_ctn_ttl',{'invalidMaxValue':true})
            }
        }else{
            for (let i in expcart){
                this.fbdFunc.setValueControlItem(controlArray,i,'dtl_ctn_ttl',expcart[i]['dtl_ctn_ttl']);
            }
        }

    }

    /*=========================
     * selectedContainer
     * =======================*/
    public selectedContainer(controlGroup:ControlGroup,ctn){
        this.fbdFunc.setValueControl(controlGroup,'ctnr_num',ctn['ctnr_num']);
        this.fbdFunc.setValueControl(controlGroup,'ctnr_id',ctn['ctnr_id']);
    }

    /*=================================
     * Check Exist Container
     * ===============================*/
    public checkExistContainer(controlGroup:ControlGroup,key,dataArr) {

        this.fbdFunc.setNewValue(controlGroup,'ctnr_num','newKey',true);
        for (let i in dataArr){
            let item = dataArr[i];
            if (key == this._globalFunc.trim(item['ctnr_num'])) {
                this.fbdFunc.setValueControl(controlGroup,'ctnr_num',item['ctnr_num']);
                this.fbdFunc.setValueControl(controlGroup,'ctnr_id',item['ctnr_id']);
                this.fbdFunc.setNewValue(controlGroup,'ctnr_num','newKey',false);
                return;
            }
        }
        this.fbdFunc.setValueControl(controlGroup,'ctnr_id','');

    }

    /*
     * Auto select item if match
     * */
    selectedItem(controlArray:ControlArray,row,dataItem)
    {
        controlArray.controls[row]['disabled']=true;
        this.fbdFunc.setValueControlItem(controlArray,row,'dtl_item_id',dataItem['item_id']);
        this.fbdFunc.setValueControlItem(controlArray,row,'dtl_sku',dataItem['sku']);
        this.fbdFunc.setValueControlItem(controlArray,row,'dtl_size',dataItem['size']);
        this.fbdFunc.setValueControlItem(controlArray,row,'dtl_color',dataItem['color']);
        this.fbdFunc.setValueControlItem(controlArray,row,'dtl_lot',dataItem['lot']);
        this.fbdFunc.setValueControlItem(controlArray,row,'dtl_uom_name',dataItem['uom_name'] ? dataItem['uom_name'] : '');
        this.fbdFunc.setValueControlItem(controlArray,row,'dtl_pack',dataItem['pack']);
        this.fbdFunc.setValueControlItem(controlArray,row,'dtl_ctn_ttl',dataItem['uom']);
        this.fbdFunc.setValueControlItem(controlArray,row,'dtl_length',dataItem['uom']);
        this.fbdFunc.setValueControlItem(controlArray,row,'dtl_width',dataItem['uom']);
        this.fbdFunc.setValueControlItem(controlArray,row,'dtl_height',dataItem['uom']);
        this.fbdFunc.setValueControlItem(controlArray,row,'dtl_weight',dataItem['uom']);
        this.fbdFunc.setValueControlItem(controlArray,row,'dtl_crs_doc',dataItem['uom']);
        this.fbdFunc.setValueControlItem(controlArray,row,'dtl_po_date',dataItem['created_at']['date']);

    }

}
