import {Component} from '@angular/core';
import { FORM_DIRECTIVES,ControlGroup,FormBuilder,ControlArray, Control ,Validators} from '@angular/common';
import {API_Config, Functions, UserService} from '../../../common/core/load';
import {RouteParams,Router,RouteData } from '@angular/router-deprecated';
import {  ValidationService } from '../../../common/core/validator';
import {BoxPopupService} from "../../../common/popup/box-popup.service";
import { OrderBy } from "../../../common/pipes/order-by.pipe";
import {WMSBreadcrumb,WMSPagination,WMSMessages,ProGressBarDirective, EventTrackingDirective, PopupAssignMultiCSR, PopupAssignMultiPickers} from '../../../common/directives/directives';
import {GoBackComponent} from "../../../common/component/goBack.component";
import {AssignMultplePickersService} from './assign-multiple-pickers.service';
import {PickingUpdateServices} from '../picking-update/picking-update-service';
import {WarningBoxPopup} from "../../../common/popup/warning-box-popup";
import {PickingUpdateFunctions} from "../picking-update/picking-functions";

declare var jQuery: any;
declare var swal: any;
declare var saveAs: any;
@Component ({
    selector: 'assign-multple-pickers',
    directives: [FORM_DIRECTIVES,GoBackComponent,ProGressBarDirective,WMSBreadcrumb,WMSMessages,WMSPagination, EventTrackingDirective, PopupAssignMultiPickers],
    pipes: [OrderBy],
    providers: [ValidationService,FormBuilder, AssignMultplePickersService,PickingUpdateFunctions,BoxPopupService, PickingUpdateServices, BoxPopupService],
    templateUrl: 'assign-multiple-pickers.component.html',
})

export class AssignMultiplePickersComponent {

  /** list permission need to check **/
  private hasUpdateOrderPickingPermission;
  /* end list permission */
  WavePickPopupForm: ControlGroup
  private messages;
  private showLoadingOverlay = false;

  private dataWavepickItems;
  private waveNumber;
  private orderNumbers;
  private customerName;
  private whs_id;
  private wv_id;
  private action;
  private showAlert = false;
  private listMsgs = {
    'ER1': 'Location is not existed.',
    'ER2': 'Location is in cycle count process',
    'ER3': 'Location is {{loc_status}}'
  }
  private currentItem={};
  private hideModal;
  items:ControlGroup[]=[];
  itemsArray: ControlArray= new ControlArray(this.items);
  private _submitForm;
  private suggestLocation:Array<any>=[];
    private _popup_sts='new';
    private messages_popup;
    private _reCheckLocProgress=0;
    private resetPickedQty=true;
    private total_pic_qty=0;
    private validForm=false;
    private listSelectedItem = [];
    public URLAssign = this._API.API_Wavepick+'/assign-multi-picker';
    public URLget = this._API.API_Authen + '/users/get-picker/';

    private listUserAssign = [];
    public listAssigned = [];
  constructor(
    private _func: Functions,
    private params: RouteParams,
    private _router:Router,
    private _Valid: ValidationService,
    private _user: UserService,
    private _boxPopupService: BoxPopupService,
    private asp_service:AssignMultplePickersService,
    private _pickingUpdateService: PickingUpdateServices,
    _RTAction: RouteData,
    private pickFunc:PickingUpdateFunctions,
    private _API: API_Config,
    private fb: FormBuilder) {

    this.checkPermission();
    this.whs_id = localStorage.getItem('whs_id');
    this.wv_id = this.params.get('id');
    this.action = _RTAction.get('action');
  }

  private showMessage(msgStatus, msg) {
    this.messages = {
      'status': msgStatus,
      'txt': msg
    }
    jQuery(window).scrollTop(0);
  }

  private checkInputNumber (evt) {
   this._Valid.isNumber(evt,true);
  }

  private checkPermission() {
    this.showLoadingOverlay = true;
    this._user.GetPermissionUser().subscribe(
      data => {
        this.hasUpdateOrderPickingPermission = this._user.RequestPermission(data,'updateOrderPicking');

        if(!this.hasUpdateOrderPickingPermission) {
          this._router.parent.navigateByUrl('/deny');
        }
        else {
          this.getWavepickDetail();
          this.formBuilder();
          this.getListPickersAssigned();
        }
      },
      err => {
        this.showMessage('danger', this._func.parseErrorMessageFromServer(err));
        this.showLoadingOverlay = false;
      }
    );
  }

  private WavepickDetail={};
  private getWavepickDetail() {

    this._pickingUpdateService.getWavepickDetail(this.whs_id, this.wv_id)
      .subscribe(
        data => {
          data = data.data;
          this.WavepickDetail=data;
          this.waveNumber = data.wv_num;
          this.customerName = data.cus_name;
          this.orderNumbers = data.odr_num;
          if(this.action=='view') {
            this.dataWavepickItems = data.details.map((item) => {
              if(item.act_loc) {
                let filterItem = item.act_loc.filter((loc) => {
                  return loc.act_loc;
                });
                item.act_loc = filterItem;
              }
              return item;
            });
          }
          else {
            this.dataWavepickItems = data.details;
          }
          this.showLoadingOverlay = false;
          this.__detecteValidForm();
        },
        err => {
          this.showMessage('danger', this._func.parseErrorMessageFromServer(err));
          this.showLoadingOverlay = false;
        }
      );
  }

  checkExistLocation(str:string,arr:Array<any>,fieldname,row)
  {
    for(let i in arr) {
      if ( this._func.trim(str) == this._func.trim(arr[i]['loc_code'])) {// auto select item if match
          //this.setErrorForControlItem(this.items,fieldname,row,null);
          this.selectedItem(fieldname,row,arr[i]);
            return;
      }
    }
    // if not found code -> set invalid
    this.setErrorForControlItem(this.items,fieldname,row,{'invalid':true});
    return;
  }

    private checkDuplicated(){

        let _set = new Set();
        let duplicated = new Set();
        let _row = [];

        for(let i = 0; i < this.items.length; i ++){
            let size = _set.size;
            let item = this.items[i]['controls']['act_loc_id']._value;
            if(item == null){
                item = this.items[i]['controls']['loc_id']._value
            }
            _set.add(item);
            if(_set.size == size){
                duplicated.add(item);
                _row.push(i);
            }
        }

        if(duplicated.size > 0){
            for(let j = 0; j < _row.length; j ++){
                this.setErrorForControlItem(this.items,'act_loc',_row[j],{'duplicated':true});
            }

            return true;
        }
        return false;
    }

  selectedItem(control,row,data)
  {
      this.resetRow(row);
    this.setValueForControlItem(this.items,'act_loc',row,data['loc_code']);
      this.setValueForControlItem(this.items,'act_loc_id',row,data['loc_id']);
      // update avail
      this.updateAvaiQty(this.items[row],row,data);
    // after selected item , call check function
      this.setRequiredPickedQty(data,row);
      setTimeout(()=>{
          this.checkLoc_Status(control,data,row);
      });

  }

  checkLoc_Status(control,loc_item,row)
  {
      // reset value inactived
    if(loc_item['loc_sts_code']=='LK') {
      this.setErrorForControlItem(this.items,control,row,{'loc_status_is_locked':true});
    }
    if(loc_item['loc_sts_code']=='IA') {
      // show popup change stt
        this.setErrorForControlItem(this.items,control,row,{'inactived':true});
    }
    if(loc_item['loc_sts_code']=='CL') {
      this.setErrorForControlItem(this.items,control,row,{'loc_status_is_locked_cycle':true});
    }
  }

  private autoSearchData={};
  private autoComplete(row,key,fieldname){
     if(key) {
         this.items[row]['showLoading'] = true;
         let searchkey = this._func.trim(key);
         let param = '?item_id='+this.currentItem['itm_id']+'&is_ecom='+this.WavepickDetail['is_ecom']+'&loc_code=' + encodeURIComponent(searchkey) + '&lot='+this.currentItem['lot']+'&limit=20';

         if (this.items[row]['subscribe']) {
             this.items[row]['subscribe'].unsubscribe();
         }
         // do Search
         this.items[row]['subscribe'] = this._pickingUpdateService.searchLocation(param).debounceTime(400).distinctUntilChanged().subscribe(
             data => {
                 // disable loading icon
                 this.items[row]['showLoading'] = false;
                 this.autoSearchData[row] = data.data;
                 this.checkExistLocation(key, data.data, fieldname, row);
             },
             err => {
                 this.items[row]['showLoading'] = false;
                 this._func.parseErrorMessageFromServerStandard(err);
             },
             () => {
             }
         );
     }else{
         // if empty key , unsubscribe
         if (this.items[row]['subscribe']) {
             this.items[row]['subscribe'].unsubscribe();
         }
         this.resetRow(row);
     }
  }

  private submitForm(params='') {
      if(this.checkValidSuggestLoction()) {

          this.showLoadingOverlay = true;
          let submitData = this.dataWavepickItems;
          this._pickingUpdateService.updateWavepickItems(this.wv_id + params, JSON.stringify(submitData))
              .subscribe(
                  data => {
                      this.showMessage('success', this._func.msg('VR131').replace('{0}', this.waveNumber));
                      setTimeout(() => {
                          this._router.parent.navigateByUrl('/outbound/picking');
                      }, 600);
                      this.showLoadingOverlay = false;
                  },
                  err => {
                      this.showLoadingOverlay = false;
                      this.showMessage('danger', this._func.parseErrorMessageFromServerStandard(err));
                  },
                  () => {
                      this.showLoadingOverlay = false;
                  }
              );
      }

  }

  private checkValidSuggestLoction(seft=false){
      if(seft){
          let item = this.currentItem;
          if (item['act_loc']) {
              item['suggest_loc_required'] = false;
          } else {
              item['suggest_loc_required'] = true;
          }
      }else {

          let valid = 0;
          this.dataWavepickItems.forEach((item)=> {
              if (item.act_loc) {
                  item['suggest_loc_required'] = false;
                  valid++;
              } else {
                  item['suggest_loc_required'] = true;
              }
          });

          if (valid !== this.dataWavepickItems.length) {
              return false;
          }
      }

      return true;

  }

  private cancel() {
    var that = this;
    this._boxPopupService.showWarningPopup()
      .then(function (ok) {
        if(ok)
          that._router.navigateByUrl('/outbound/picking');
      });
  }

  /*===========================
  * Builder Form Popup
  * ==========================*/
  private formBuilder(){
    this.WavePickPopupForm =this.fb.group({
      data:this.itemsArray
    });
  }

  /*
  * itemsArray
  * */
  private addRowItem(data,autoAddMore=true){

    this.itemsArray.push(
        new ControlGroup({
            'loc_id': new Control(data['loc_id']),
          'loc_code': new Control(data['loc_code']),
          'act_loc': new Control(data['act_loc']),
           'act_loc_id': new Control(data['act_loc_id']),
          'avail_qty': new Control(data['avail_qty'] ? data['avail_qty'] : 0),
          'picked_qty': new Control(data['picked_qty'], Validators.compose([Validators.required, this._Valid.inPutOnlyNumber, this._Valid.isZero]))
        })
    );
      this.setRequiredPickedQty(data,this.items.length-1);
      if(!data['picked_qty']) {
          this.smartAutoPickedQty(data,autoAddMore);
      }

  }

    resetRow(row,reset_picked=true){
        //this.setValueForControlItem(this.items,'act_loc_id',row,'');
        if(this.resetPickedQty){
            this.setValueForControlItem(this.items,'picked_qty',row,'');
            this.setErrorForControlItem(this.items,'picked_qty',row,null);
        }
    }

    /*============
    * disbale required picked_qty
    * ===========*/
    setRequiredPickedQty(item,row){
        // check qty
        let itemVal=item['avail_qty'] ? item['avail_qty'] : item['available_quantity'];
        let pickedQty=this.items[row]['controls']['picked_qty'].value;
        if(!itemVal){
            this.setErrorForControlItem(this.items,'picked_qty',row,null);
        }else{
            // check null picked qty
            if(!pickedQty){
                this.setErrorForControlItem(this.items,'picked_qty',row,{'required':true});
            }
        }
    }

  /*
  *
  * */
  private getSuggestLocation(item,action) {

      this._submitForm=false;
      this.messages = false;
      this._popup_sts=action;
      this.messages_popup=false;
      this._reCheckLocProgress=0;

      this.__resetPopupForm();

      // fix to view
      this.suggestLocation=[];
      this.currentItem=item;
      this.total_pic_qty=this.currentItem['act_piece_qty'];

      // Handle param
      let param='list-suggest-location';
      if(action=='view'){
          param='view-wave-pick-detail';
      }

      // =======
      // If act_loc had updated , we'll use that value
      // ======
      if ( item['act_loc']!== null && typeof item['act_loc'] === 'object' || action=='view'){
          
          if(item['act_loc']){
              item['act_loc'].forEach((itm)=>{
                  this.addRowItem(itm);
              });
              // // recheck loc
              // if(action != 'view') {
              //     this.reCheckLocStatus(false);
              // }

          }else{
              
          }
      }
      else{
          this.showLoadingOverlay = true;

          this._pickingUpdateService.getSuggestLocation(param,item['wv_dtl_id'])
              .subscribe(
                  data => {
                      this.showLoadingOverlay = false;
                      this.suggestLocation=data.data;
                      if(this.suggestLocation.length){

                          this.suggestLocation.forEach((item)=>{

                              if(this.total_pic_qty<this.currentItem['piece_qty']){
                                  this.addRowItem(item,false);
                              }

                          });

                          if(this.total_pic_qty<parseInt(this.currentItem['piece_qty'])){
                              this.addMoreSuggestLoc();
                          }

                      }else{
                          this.addMoreSuggestLoc();
                      }
                  },
                  err => {
                      this.showLoadingOverlay = false;
                  }
              );
      }

  }

    /*=========================================
    * __detecteValidForm
    * ====================================== */

    private __detecteValidForm(){

        let valid = 0;
        this.dataWavepickItems.forEach((item)=> {
            if (item.act_loc) {
                item['suggest_loc_required'] = false;
                valid++;
            } else {
                item['suggest_loc_required'] = true;
            }
        });

        if (valid !== this.dataWavepickItems.length) {
            this.validForm=false;
            return;
        }

        this.validForm=true;
    }


    private addMoreSuggestLoc() {
        this.messages_popup=false;
    this.showLoadingOverlay = true;
    this._reCheckLocProgress=0;
    // get loc Id suggest
    let locsId=this.getLocId();

    let params='?loc_ids='+locsId+'&item_id='+this.currentItem['itm_id']+'&is_ecom='+this.WavepickDetail['is_ecom']+'&lot='+this.currentItem['lot'];
    this._pickingUpdateService.getMoreSuggestLocation(params)
        .subscribe(
            data => {
              this.showLoadingOverlay = false;
              if(data.data){
                this.addRowItem(data.data);
              }
              else{
                  //this.messages_popup=this._func.Messages('danger',this._func.msg('NOT_FOUND'));
              }
            },
            err => {
              this.showLoadingOverlay = false;
            },()=>{
            }
        );

  }

  private updateAvaiQty(item,row,data){
      this.setValueForControlItem(this.items,'avail_qty',row,data['avail_qty'] ? data['avail_qty'] : 0);

      // set error if value is zero
      if(!data['avail_qty']){
          this.setErrorForControlItem(this.items,'avail_qty',row,{'isZero':true});
      }else{
          this.setErrorForControlItem(this.items,'avail_qty',row,null);
      }

  }

    /*============================
     changePickQty
    * =============================*/
    private  changePickQty(item,$event,row) {

        if(parseInt(item.controls['avail_qty'].value)<parseInt($event.target.value)){
            this.setErrorForControlItem(this.items,'picked_qty',row,{'invalidPick':true});
        }else{
            if(!$event.target.value){
                this.setErrorForControlItem(this.items,'picked_qty',row,{'required':true});
            }else{
             //   this.setErrorForControlItem(this.items,'picked_qty',row,null);
            }
        }
        // cal_totalPickQty
        this.cal_totalPickQty();

    }

  /*=========================================
  * Get Loc ID
  * ======================================*/
  private getLocId() {
     let locIds=[];
      this.WavePickPopupForm.value['data'].forEach((item)=>{
         locIds.push(item['loc_id']);
     });
      return locIds;
  }

  /*======================================
  * removeLoc
  * ====================================*/
  private removeLoc(i) {
      this.itemsArray.removeAt(i);
      this._reCheckLocProgress=0;
      // auto get suggest location if array empty
      if(!this.WavePickPopupForm.value['data'].length){
          this.addMoreSuggestLoc();
      }
      // calculator total picked qty
      this.cal_totalPickQty();
  }

  /*===========================================
  * updateSuggestLoc
  *======================================== =*/
 
  private updateSuggestLoc() {


    this.cal_totalPickQty();

    if(this.WavePickPopupForm.valid){

        let item =this.currentItem;
       // reset = 0 to caclulator
        item['act_piece_qty']=0;

        for(let i in this.WavePickPopupForm.value['data']) {
            let value=this.items[i]['controls']['picked_qty'].value;
            if(value){
                item['act_piece_qty']=parseInt(item['act_piece_qty'])+parseInt(value);
            }
        }

        if(this.total_pic_qty<parseInt(this.currentItem['piece_qty'])){

            /*case 1: modify text*/
            let that=this;
            let warningPopup = new WarningBoxPopup();
            warningPopup.text = 'Back Order will be created because Actual QTY < Allocated QTY. Are you sure?';
            this._boxPopupService.showWarningPopup(warningPopup)
                .then(function (dm) {
                    if (!dm) {
                        return;
                    } else {
                        // add popup data to item
                        item['act_loc']=that.WavePickPopupForm.value['data'];
                        // recheck suggest loctions
                        that.checkValidSuggestLoction(true);
                        that.__detecteValidForm();
                        jQuery('#popupWavePick').modal('hide');

                    }
                });


        }else{

            // add popup data to item
            item['act_loc']=this.WavePickPopupForm.value['data'];
            setTimeout(()=>{
                // close popup if form vaild
                jQuery('#popupWavePick').modal('hide');
            });

        }




    }  else{
    }
      // recheck suggest loctions
      this.checkValidSuggestLoction(true);
      this.__detecteValidForm();

  }

    /*=====================
    * Recheck Loc status
    * ====================*/
    private reCheckLocStatusData:Array<any>=[];

    reCheckLocStatus(){
        this._submitForm=true;
        if(this.itemsArray.valid){
            this.pickFunc.copySuggestedLocationToActualLocation(this.itemsArray);
            //
            this.showLoadingOverlay=true;
            let dataCheck=this.pickFunc.creatDataLocToCheck(this.itemsArray,this.currentItem);
            this._pickingUpdateService.checkActualLocations(JSON.stringify(dataCheck)).subscribe(
                data=>{

                    this.reCheckLocStatusData=data;
                    this.pickFunc.pareDataErrors(this.itemsArray,data);
                    if(this.itemsArray.valid){
                        this.updateSuggestLoc();
                    }else{
                        this.messages_popup=this._func.Messages('danger',"Some locations have error. Please check.");
                        jQuery('.modal-body').animate({
                            scrollTop: 0
                        }, 'slow');
                    }

                },
                err=>{this.showLoadingOverlay=false;},
                ()=>{
                    this.showLoadingOverlay=false;
                }
            )
        }else{
            this.messages_popup=this._func.Messages('danger',"Some locations have error. Please check.");
            jQuery('.modal-body').animate({
                scrollTop: 0
            }, 'slow');
        }
        
    }


    /*=============================
    * Reset Popup Form
    *================================ */
    __resetPopupForm(){
        // reset Form every show popup
        while(this.itemsArray.length) {
            this.itemsArray.removeAt(0);
        }
    }

    /*=======================
    * Smart auto set picked qty
    * =====================*/
    smartAutoPickedQty(data,autoAddMore=true){

        if(this.total_pic_qty<parseInt(this.currentItem['piece_qty'])){

            if(data['avail_qty']){

                let total_pic_qty_tm=parseInt(data['avail_qty']) + this.total_pic_qty;
                if(total_pic_qty_tm<=parseInt(this.currentItem['piece_qty'])){
                    this.setValueForControlItem(this.items,'picked_qty',this.items.length-1,data['avail_qty']);
                }else{
                    let picked=parseInt(this.currentItem['piece_qty']) - this.total_pic_qty;
                    this.setValueForControlItem(this.items,'picked_qty',this.items.length-1,picked);
                }

                this.cal_totalPickQty();

                if(autoAddMore){
                    if(this.total_pic_qty<parseInt(this.currentItem['piece_qty'])){
                        this.addMoreSuggestLoc();
                    }
                }

            }

        }

    }

    /*===
     total_pic_qty
    *=== */
    private cal_totalPickQty(){
        this.total_pic_qty=0;
        this.WavePickPopupForm.value['data'].forEach((item)=>{
            let qty=item['picked_qty'] ? item['picked_qty'] : 0;
            this.total_pic_qty=this.total_pic_qty+parseInt(qty);
        });
        if(this.total_pic_qty>parseInt(this.currentItem['piece_qty'])){
            (<ControlGroup>this.WavePickPopupForm).setErrors({'invalid_picked_qty':true});
            this.messages_popup=this._func.Messages('danger',this._func.msg('WAV001'));
        }
    }
  /*======================================
  * Set value for control item
  * ====================================*/
  private setValueForControlItem(FormGroup,Control,Row,Value){
    (<Control>FormGroup[Row]['controls'][Control]).updateValue(Value);
  }
  /*===================================
   * Set value for control item
   *=================================== */
  private setErrorForControlItem(FormGroup,Control,Row,Err){
    (<Control>FormGroup[Row]['controls'][Control]).setErrors(Err);
  }

  /*===================================
   * Export to pdf or excel file
   *=================================== */
    private printWavepick(){
        if(this.WavepickDetail['wv_id']) {
            this.printWavepickTicket(this.WavepickDetail['wv_id'], this.WavepickDetail['wv_num']);
        } else {
            this.showMessage('danger', 'Data not found. Please check.');
        }
    }
    
    private printWavepickTicket(WpId, WpNum){
        let that = this;
        let api = that._API.API_Wavepick_2  + '/wave-pick/print?wv_id=' +  WpId;
        try{
        let xhr = new XMLHttpRequest();
        xhr.open("GET", api , true);
        xhr.setRequestHeader("Authorization", 'Bearer ' +that._func.getToken());
        xhr.responseType = 'blob';

        xhr.onreadystatechange = function () {
            if(xhr.readyState == 2) {
            if(xhr.status == 200) {
                xhr.responseType = "blob";
            } else {
                xhr.responseType = "text";
            }
            }

            if(xhr.readyState === 4) {
            if(xhr.status === 200) {
                //update printed label value

                var today = new Date();
                var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
                "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
                ];
                var day = today.getDate();
                var month = monthNames[today.getMonth()];
                var year = today.getFullYear().toString().substr(2,2);
                // var fileName = 'Wavepick_' + month+ day + year;
                let fileName:string = WpNum;
                var blob = new Blob([this.response], {type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'});
                let fileExt = '.xlsx';
                saveAs.saveAs(blob, fileName + fileExt);
            }else{
                let errMsg = '';
                if(xhr.response) {
                try {
                    var res = JSON.parse(xhr.response);
                    errMsg = res.errors && res.errors.message ? res.errors.message : xhr.statusText;
                }
                catch(err){errMsg = xhr.statusText}
                }
                else {
                errMsg = xhr.statusText;
                }
                if(!errMsg) {
                errMsg = that._func.msg('VR100');
                }
                that.showMessage('danger', errMsg);
            }
            }
        }
        xhr.send();
        }catch (err){
        that.showMessage('danger', that._func.msg('VR100'));
        }

        /*if (f > 0) {

        }else{
        that.showMessage('danger', that._func.msg('CF005'));
        }*/

    }

    checkedItem(){

        this._func.checkedItemData(this.WavepickDetail,'details');
        
    }
        
    private getWavePickSelected(){

        let wv_dtl_ids:Array<any>=[];
        this.dataWavepickItems.forEach((item)=>{
            if(item['selected']){
                wv_dtl_ids.push(item['wv_dtl_id'])
            }
        });
        return wv_dtl_ids;

    }

    assignPicker() {
        this.showLoadingOverlay = true;
        this.asp_service.getListCRSByCustomer(this.whs_id).subscribe(
            data => {
                this.showLoadingOverlay = false;
                this.listUserAssign = data['data'];
                if(this.listUserAssign.length&&this.listAssigned.length) {
                    this.listUserAssign.forEach(element => {
                        if(this.listAssigned.find(x => x.user_name === element.username)) {
                            element['selected'] = true;
                        }
                    });
                }
            },
            ()=>{
                this.showLoadingOverlay = false;
            }
        );
    }
    
    getListPickersAssigned() {
        this.showLoadingOverlay = true;
        this.asp_service.getListPickersAssigned(this.wv_id).subscribe(
            data => {
                this.showLoadingOverlay = false;
                this.listAssigned = data['data'];                
            }, err => {
                this.showMessage('danger', this._func.parseErrorMessageFromServer(err));
            }
        );
    }


}
