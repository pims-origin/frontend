import {Component,Input, Injectable} from '@angular/core';
import {API_Config, Functions} from '../../../common/core/load';
@Component({
  selector: 'dms-in',
  template: `
   <iframe height="350px" *ngIf="uploadDocumentData['api_string']" width="100%" src="{{uploadDocumentData['api_string']}}"></iframe>
    `
})
@Injectable()
export class DmsIn {
  
  @Input() doc_type: string = '';
  @Input() group: string = '';
  private _trans;
  private uploadDocumentData={};

  @Input('trans') set trans(value: string) {
    if(value){
      this.getUploadDoccument(value);
    }
  }

  constructor(
    private _Func: Functions,
    private _API: API_Config){
  }

  getUploadDoccument(trans) {
    let apiString = this._API.SERVICE_DOCUMENTS 
                    +'?doc_type='+this.doc_type
                    +'&group='+this.group
                    +'&transaction='+ trans 
                    +'&token=' + this._Func.getToken();
    this.uploadDocumentData['api_string']=apiString;
  }
}
