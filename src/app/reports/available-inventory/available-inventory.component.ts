
import {Component} from '@angular/core';
import {API_Config} from '../../common/core/API_Config';
import {BaseTable} from '../../common/core/classes_us/base-table';
import {TableFunctions} from '../../common/core/table-functions';
import {DateFunctions} from '../../common/core/functions/date-functions';
import {BaseService} from '../../common/core/classes_us/base-service';
import {CustomersService} from '../../master-data/customers/customers-service';
import {TableStructure} from '../../common/directives/directives';

@Component ({
    selector: 'available-inventory',
    directives: [
        TableStructure
    ],
    templateUrl: '../../common/directives/table-structure/table.component.html',
    providers: [
        BaseService,
        TableFunctions,
        DateFunctions,
        CustomersService
    ]
})

export class AvailableInventoryComponent extends BaseTable
{
    public tableID = 'available-inventory';
    public url = this._api.API_AVAILABLE_INVENTORY_REPORTS;
    public headerUrlSuffix = 'rep';
    public title = 'Available Inventory';
    public permissionPage = 'viewReport';

    public headerDef = [
        {
            id: 'ver_table',
            value: 1 + Math.random() * 10000
        },
        this.columnList.customer,
        this.columnList.container,
        this.columnList.measurementSystem,
        this.columnList.createdAt,
        this.columnList.upc,
        this.columnList.sku,
        this.columnList.color,
        this.columnList.size,
        this.columnList.po,
        this.columnList.lot,
        this.columnList.length,
        this.columnList.width,
        this.columnList.height,
        this.columnList.weight,
        this.columnList.actualCartons,
        this.columnList.pieceRemain
    ];

    public unsortableColumns = [
        'customer',
        'measurementSystem',
        'actualCartons',
        'pieceRemain'
    ];

    public searches = [
        this.searchList.customer,
        this.searchList.container,
        this.searchList.upc,
        this.searchList.sku
    ];

    /*
    ****************************************************************************
    */

    constructor (
        public _api: API_Config,
        public _tableFunc: TableFunctions,
        public _tableService: BaseService,
        public _customersService: CustomersService
    )
    {
        super();

        this._tableFunc.permissionCall(this);
    }

    //**************************************************************************

}
