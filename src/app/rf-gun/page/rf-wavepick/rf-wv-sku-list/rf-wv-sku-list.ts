import {Component,OnInit} from "@angular/core";
import { Router,RouteParams, RouterLink, ROUTER_DIRECTIVES } from '@angular/router-deprecated';
import { FORM_DIRECTIVES,FormBuilder} from '@angular/common';
import {UserService, Functions} from '../../../../common/core/load';
import {WMSBreadcrumb,WMSPagination,PaginationControlsCmp,ColorPickerDirective,WMSMessages,ColorPickerService,PaginationService} from '../../../../common/directives/directives';
import {CustomerServices} from "../../../../system-setup/customer/customer-service";
import {RF_WavePickServices} from "../wavepick.service";
@Component({
  selector: 'rf-gun',
  providers: [CustomerServices,RF_WavePickServices,PaginationService,ColorPickerService,UserService],
  directives: [FORM_DIRECTIVES,WMSPagination,PaginationControlsCmp,ColorPickerDirective,WMSMessages,WMSBreadcrumb],
  templateUrl: 'rf-wv-sku-list.html',
})
export class RF_WavePickSKUListComponent {

  private SKUList:Array<any>=[];
  private allowAccess;
  private showLoadingOverlay;
  private messages;
  private rfGunPermission;
  private whs_id;
  private gr_id;
  private qty;
  private wv_num;

  constructor(private rfGunService:RF_WavePickServices,
              private _router: Router,
              private params: RouteParams,
              private _user:UserService,
              private _Func: Functions)
  {
    this.checkPermission();
  }


  checkPermission(){

    this.showLoadingOverlay = true;
    this._user.GetPermissionUser().subscribe(
      data => {
        this.showLoadingOverlay = false;
        this.rfGunPermission = this._user.RequestPermission(data, 'viewRFGUN');
        if(this.rfGunPermission) {
          this.allowAccess=true;
          this.whs_id = localStorage.getItem('whs_id');
          this.getSKUList();
        }
        else{
          this.redirectDeny();
        }
      },
      err => {
        this.parseError(err);
        this.showLoadingOverlay = false;
      }
    );
  }

  redirectDeny(){
    this._router.parent.navigateByUrl('/deny');
  }


  getSKUList(page='')
  {
    this.showLoadingOverlay=true;
    this.gr_id=this.params.get('id');
    let params='/'+this.gr_id;
    this.rfGunService.RF_GUN_API_GET(this.whs_id,params)
      .subscribe(
        data => {
          this.SKUList=data.data;
          this.qty=this.SKUList[0]['qty'];
          this.wv_num=this.SKUList[0]['wv_num'];
          this.showLoadingOverlay=false;
        },
        err => {
          // this.flagLoading = false;
          // this.flagLoading2 = false;
          this.parseError(err);
          this.showLoadingOverlay = false;
        },
        () => {

          // this.flagLoading = false;
          // this.flagLoading2 = false;
        }
      );

  }

  rediect_updatePutAway(item)
  {
    this._router.parent.navigateByUrl('/rf-gun/wave-pick/rf-wavepick-update/'+item['wv_dtl_id']);
  }

  // Show error when server die or else
  private parseError (err) {
    err = err.json();
    this.messages = this._Func.Messages('danger', err.errors.message);
  }

}
