import { Component, Injectable  } from '@angular/core';
import {Http, Response, Headers, HTTP_BINDINGS } from '@angular/http';
import { contentHeaders } from '../../common/api/headers';
import {API_Config, Functions} from "../../common/core/load";

@Injectable()
@Component({
    providers: [HTTP_BINDINGS],
    template: ``
})
export class MainMenuService {

  public API_Left = this._API.API_LeftMenu;
  public API_FavoriteMenu = this._API.API_FavoriteMenu;
  public loading:boolean=false;
  public AuthHeader = this._Func.AuthHeader();
  public AuthHeaderPost = this._Func.AuthHeaderPost();

  constructor(private _Func: Functions, private _API: API_Config, private http: Http) {}

  /*=====
   Get List main menu
   ======*/
  getLeftMenu() {

    return this.http.get(this.API_Left, { headers: this.AuthHeader })
      .map((res: Response) => res.json().data);
  }
  /*=====
   Get List menu favorite from cookie
   ======*/
  getFavoriteMenu() {
    return this.http.get(this.API_FavoriteMenu, { headers: this.AuthHeader })
      .map((res: Response) => res.json().data);
  }

  /*=====
   Add item to FavoriteMenu
   ======*/
  Add_to_FavoriteMenu(menu_id,position_order) {

    var data = "menu_id="+menu_id+"&display_order="+position_order;

    return this.http.post(this.API_FavoriteMenu, data, { headers: this.AuthHeaderPost })
      .map(res => res.json());

  }

  DeleteItemMenuFavorite(menu_id)
  {
    return this.http.delete(this.API_FavoriteMenu + '/' + menu_id, { headers: this.AuthHeader });
  }

  private handleError(error: any) {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }

  private extractData(res: Response) {
    let body = res.json();
    return body.data || {};
  }


}

class Menu {
  constructor(public id:number, public name:string) {
  }
}
