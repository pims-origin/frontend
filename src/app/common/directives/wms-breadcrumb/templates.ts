export const DEFAULT_TEMPLATES = `

<ol class="breadcrumb">
    <li>
      <a href="#/dashboard">Home</a>
    </li>
    <li *ngFor="let brc of wmsbreadcrumb">
      <a href="#{{brc.patch}}" *ngIf="!brc.current">{{brc.txt}}</a>
      <span *ngIf="brc.current">{{brc.txt}}</span>
    </li>
    <li class="active" *ngIf="currentBrc">
      {{currentBrc}}    
    </li>
</ol>

`;
