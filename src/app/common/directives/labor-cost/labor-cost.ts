import {Component,Input, Output,EventEmitter} from '@angular/core';
import { FORM_DIRECTIVES,ControlGroup,Control,ControlArray,FormBuilder,Validators} from '@angular/common';
import {Router, RouteData, RouteParams } from '@angular/router-deprecated';
import {LaborCostServices} from "./labor-cost-service";
import { ValidationService } from '../../../common/core/validator';
import {UserService, Functions} from '../../../common/core/load';
declare var jQuery: any;
@Component ({
  selector: 'labor-cost-directive',
  directives:[FORM_DIRECTIVES],
  providers: [LaborCostServices,ValidationService],
  templateUrl: 'labor-cost-template.html'
})

export class LaborCostDirective {

  LaborCostForm: ControlGroup;
  private messages;
  private showLoadingOverlay=false;
  private whs_id;
  private submitForm=false;
  private _cus_id = '';
  private loading={};
  private laborCostDeatail;
  items:ControlGroup[]=[];
  itemsArray: ControlArray= new ControlArray(this.items);
  private _environment;
  private _value;
  private params;

  @Input('category') set category(value: string) {
    this._environment=value;
  }

  @Input('value') set value(value: string) {
    this._value=value;
  }


  @Input('cus_id') set cus_id(value: string) {
    this._cus_id=value;
    if(value){
      this.params=this._value+"/"+this._environment+'/'+this.whs_id+'/'+this._cus_id;
      this.getLaborCost(this.params);
    }
  }

  @Output() laborMessages  = new EventEmitter();


  constructor(
              private fb: FormBuilder,
              private _Valid: ValidationService,
              private _Func: Functions,
              private _laborcostSerivce:LaborCostServices) {
    this.whs_id=this._Func.lstGetItem('whs_id');
  }

  private formBuilder(data=[]){

    this.LaborCostForm =this.fb.group({
      data:this.itemsArray,
    });

    if(data.length){

      this.buildItem(data);

    }else{

     let labor=[
       {urgency:'labor'},
       {urgency:'overtime'},
       {urgency:'rush'}
     ]
      this.buildItem(labor);

    }


  }

  private buildItem(data){

    data.forEach((val)=>{

      this.itemsArray.push(
          new ControlGroup({
            'label':new Control(val['label']),
            'expected_amount': new Control(val['expected_amount'], Validators.compose([this._Valid.isLaborCost])),
            'actual_amount': new Control(val['actual_amount'], Validators.compose([this._Valid.isLaborCost])),
            'urgency': new Control(val['urgency'], Validators.compose([Validators.required, this._Valid.validateSpace])),
          })
      )

    })


  }


  private getLaborCost(param){
    this._laborcostSerivce.getLaborCost(param).subscribe(
        data => {
          this.formBuilder(data);
        },
        err => {
          this.parseError(err);
        },
        () => {}
    );
  }




  Save(){

    this.submitForm=true;

    // empty messages
    this.messages=false;
    
    if(this.LaborCostForm.valid){
      let data=this.LaborCostForm.value['data'];
      var isValidData = true;
      for(let i = 0; i < data.length; i++) {
        if((data[i].actual_amount && !data[i].expected_amount) || (!data[i].actual_amount && data[i].expected_amount)) {
          isValidData = false;
        }
      }
      if(isValidData) {
        let data_json = JSON.stringify(data);
        this.Update(data_json);
      }
    }
    else{
    }

  }


  Update(data){

    this.showLoadingOverlay=true;
    this._laborcostSerivce.updateLaborCost(this.params,data).subscribe(
        data => {
          this.showLoadingOverlay=false;
          this.laborMessages.emit(this._Func.Messages('success', this._Func.msg('VR109')));
          // scroll to top
          jQuery('html, body').animate({
            scrollTop: 0
          }, 700);
        },
        err => {
          this.showLoadingOverlay=false;
          this.parseError(err);
          jQuery('html, body').animate({
            scrollTop: 0
          }, 700);
        },
        () => {}
    );

  }

  checkInputNumber(event)
  {
    this._Valid.isNumber(event);
  }

  private parseError(err){
    this.messages={'status' : 'danger', 'txt' : this._Func.parseErrorMessageFromServer(err)};
    this.laborMessages.emit(this.messages);
  }


}
