import {Component} from '@angular/core';
import {Router, RouterLink, RouteConfig, ROUTER_DIRECTIVES} from '@angular/router-deprecated';
import {UserListComponent} from "./user-list/user-list.component";
import {CRUUserComponent} from "./cru-user/cru-user.component";

@Component ({
  selector: 'user-management',
  directives: [ROUTER_DIRECTIVES],
  template: `<router-outlet></router-outlet>`
})

@RouteConfig([
  { path: '/', component: UserListComponent, name: 'User List', useAsDefault: true },
  { path: '/new', component:CRUUserComponent  , name: 'Add  User' , data:{Action:'New'} },
  { path: '/:id/:edit', component:CRUUserComponent  , name: 'Edit  User', data:{Action:'Edit'} },
  { path: '/:id', component:CRUUserComponent  , name: 'View  User' ,data:{Action:'View'}}
])
export class UserManagementComponent {

}
