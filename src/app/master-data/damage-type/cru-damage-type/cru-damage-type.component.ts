import {Component} from '@angular/core';
import { FORM_DIRECTIVES,ControlGroup,FormBuilder, Control ,Validators} from '@angular/common';
import {RouteParams,Router ,RouteData} from '@angular/router-deprecated';
import {API_Config, UserService, Functions} from '../../../common/core/load';
import { ValidationService } from '../../../common/core/validator';
import { Http } from '@angular/http';
import { ControlMessages } from '../../../common/core/control-messages';
import {GoBackComponent} from "../../../common/component/goBack.component";
import {DamageTypeService} from "../damage-type.service";
import {ErrorMessageForControlService} from "../../../common/services/error-message-for-control.service";
import {BoxPopup} from "../../../common/popup/box-popup";
import {WarningBoxPopup} from "../../../common/popup/warning-box-popup";
import {BoxPopupService} from "../../../common/popup/box-popup.service";
import {JwtHelper} from "angular2-jwt/angular2-jwt";
import {WMSBreadcrumb,WMSMessages} from '../../../common/directives/directives';
declare var jQuery: any;

@Component({
    selector: 'cru-damage-type',
    providers: [DamageTypeService,ValidationService,FormBuilder,ErrorMessageForControlService, BoxPopupService],
    directives: [FORM_DIRECTIVES,ControlMessages,GoBackComponent,WMSMessages,WMSBreadcrumb],
    templateUrl: 'cru-damage-type.component.html',

})
export class CruDamageTypeComponent{
    public loading = [];
    private messages;
    private action;
    private idDetail;
    private TitlePage='Damage Type';
    private isView:boolean=false;
    private dmgTypeDetail=[];
    private detail;
    dmgTypeForm: ControlGroup;
    public showLoadingOverlay = false;
    private submitted:boolean = false;

    constructor(
        private _http: Http,
        private _Func: Functions,
        private _user: UserService,
        private params: RouteParams,
        private _Valid: ValidationService,
        private _dmgTypeService: DamageTypeService,
        private _boxPopupService: BoxPopupService,
        public jwtHelper: JwtHelper,
        private fb: FormBuilder,
        private _ErrMsgControlService: ErrorMessageForControlService,
        _RTAction: RouteData,
        private _router: Router) {

        this.action=_RTAction.get('action');

        this.checkPermission();

        this._ErrMsgControlService.errorMsg.invalidSpace = 'is required.';  // set error message for invalid space

        if(this.action=='view')
        {
            this.isView=true;
            this.TitlePage='View Damage Type';
        }
        if(this.action=='edit')
        {
            this.TitlePage='Edit Damage Type';
        }
        if(this.action=='edit'||this.action=='view')
        {
            // edit

            this.idDetail=this.params.get('id');
            this._dmgTypeService.getDamageTypeDetail(this.idDetail).subscribe(
                data => {
                    this.detail=data;
                    this.FormBuilderEdit();
                },
                err => {
                    this._router.parent.navigateByUrl('/system-setup/item-master');
                },
                () => {}
            );

        }
        else{
            // Action new
            if(this.action=='new')
            {
                this.TitlePage='Add Damage Type';
                this.FormBuilder();
            }
            else{

            }
        }
    }

    private checkPermission() {
        this.showLoadingOverlay = true;
        this._user.GetPermissionUser().subscribe(
            data => {
                if((this.action == 'view' && !this._user.RequestPermission(data,'viewDamageType')) || (this.action == 'edit' && !this._user.RequestPermission(data,'editDamageType')) || (this.action == 'new' && !this._user.RequestPermission(data,'createDamageType'))) {
                    this._router.parent.navigateByUrl('/deny');
                }
                this.showLoadingOverlay = false;
            },
            err => {
                this.showMessage('danger', this._Func.parseErrorMessageFromServer(err));
                this.showLoadingOverlay = false;
            }
        );
    }

    FormBuilder()
    {
        //item_code,description,sku,size,color,uom,pack,length,width,height,weight,cus_id,condition

        this.dmgTypeForm =this.fb.group({
            dmg_code:[null, Validators.compose([Validators.required, this._Valid.validateSpace, this._Valid.invalidLength2Or3])],
            dmg_name:[null, Validators.compose([Validators.required, this._Valid.validateSpace, this._Valid.invalidNameStr])],
            dmg_des :[''],

        });
    }

    FormBuilderEdit()
    {
        this.dmgTypeForm =this.fb.group({
            dmg_code:[this.detail['dmg_code'], Validators.compose([Validators.required, this._Valid.validateSpace, this._Valid.invalidLength2Or3])],
            dmg_name :[this.detail['dmg_name'], Validators.compose([Validators.required, this._Valid.validateSpace, this._Valid.invalidNameStr])],
            dmg_des:[this.detail['dmg_des']]
        });
    }

    save(data :Object):void{

        let n=this;
        // this._genErrMsgForMultiControls(this.chgTypeForm, this.errorMsg)
        this._ErrMsgControlService._genErrMsgForMultiControls(this.dmgTypeForm);

        this.submitted = true;
        if(this.dmgTypeForm.valid) {
            // trim input of 2 fields
            this._trimObjProperty(data, ['dmg_code', 'dmg_name']);

            let data_json = JSON.stringify(data);

            if (this.action == 'new') {
                // add new
                this.addNew(data_json);
            }
            else {
                // edit zone type
                this.update(this.idDetail,data_json);
            }

        }
        // else{
        //     this.messages = this._Func.Messages('danger', this._Func.msg('VR108'));
        // }

    }

    update(id,data)
    {
        let n=this;
        this.loading['AddUpdate']=true;
        this._dmgTypeService.updateDamageType(id,data).subscribe(
            data => {
                this.loading['AddUpdate']=false;
                //Reset Form
                this.messages = this._Func.Messages('success', this._Func.msg('VR109'));
                setTimeout(function(){
                    n._router.parent.navigateByUrl('/master-data/damage-type');
                }, 600);
            },
            err => {
                this.loading['AddUpdate']=false;
                this.showMessage('danger', this._Func.parseErrorMessageFromServer(err));
            },
            () => {}
        );
    }

    /*
     *
     * */
    addNew(data)
    {
        let n=this;
        this.loading['AddUpdate']=true;
        this._dmgTypeService.addNewDamageType(data).subscribe(
            data => {
                this.loading['AddUpdate'] = false;
                //Reset Form
                this.messages = this._Func.Messages('success', this._Func.msg('VR107'));
                setTimeout(function(){
                    n._router.parent.navigateByUrl('/master-data/damage-type');
                }, 600);
            },
            err => {
                this.loading['AddUpdate'] = false;
                this.showMessage('danger', this._Func.parseErrorMessageFromServer(err));
            },
            () => {}
        );
    }

    cancel()
    {
        let n = this;
        /*case 1: modify text*/
        // let warningPopup = new WarningBoxPopup();
        // warningPopup.text = "Are you sure to cancel";
        // this._boxPopupService.showWarningPopup(warningPopup)

        /*case 2: default*/
        this._boxPopupService.showWarningPopup()
            .then(function (dm) {
                if(dm)
                    n._router.navigateByUrl('master-data/damage-type');

            });
    }

    /*
     * obj: object
     * properties: keys of object need to trim
     * */
    _trimObjProperty(obj, properties)
    {
        properties.forEach(function(prop) {
            obj[prop] = obj[prop].trim();
        });
    }

    detectControlErr(control) {
        // this._genErrMsgForControl(control, this.errorMsg);
        this._ErrMsgControlService._genErrMsgForControl(control);
    }

    private showMessage(msgStatus, msg) {
        // if(this.timeoutHideMessage) clearTimeout(this.timeoutHideMessage);
        this.messages = {
            'status': msgStatus,
            'txt': msg
        }
        // this.timeoutHideMessage = setTimeout(() => {
        //     this.messages = null;
        // }, 4000);
        jQuery(window).scrollTop(0);
    }
}
