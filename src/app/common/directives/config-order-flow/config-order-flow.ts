import {Component,Input, Output,EventEmitter} from '@angular/core';
import {configOrderFlowServices} from './config-order-flow-service';
import { API_Config,Functions} from '../../../common/core/load';

@Component ({
  selector: 'config-order-flow-directive',
  directives:[],
  providers: [configOrderFlowServices],
  templateUrl: 'config-order-flow.html'
})

export class ConfigOrderFlowDirective {

  private _cusId = '';
  private orderFlowDetail={};
  private _orderId;
  private _disable;
  private _environment;
  private hiddenFlowCodes = ['APK', 'PAK', 'BOL', 'ARS'];

  @Input('orderId') set orderId(value: string) {
    this._orderId=value;
  }

  @Input('disabled') set disabled(value: string) {
    this._disable=value;
  }

  @Input('environment') set environment(value: string) {
    this._environment=value;
  }

  @Input('cusId') set cusId(value: string) {
    this._cusId=value;
    if(value){
      this.getOrderFlow();
    }
  }

  @Output() orderFlowReturnData  = new EventEmitter();
  @Output() messages=  new EventEmitter();
  @Output() showLoadingOverlay=  new EventEmitter();

  constructor(
              private _Func: Functions,
              private _API: API_Config,
              private _ODFSerivce:configOrderFlowServices) {}

  private getOrderFlow(){

    let param=this._cusId;
    // this.showLoadingOverlay.emit(true);

    if(this._environment=='customer'){
      param=this._API.API_Customer+'/order-flow/'+this._cusId;
    }else if(this._environment=='order'){
      if(!this._orderId){
        param=this._API.API_Customer+'/order-flow/'+this._cusId;
      }else{
        param=this._API.API_ORDERS+'/order-flows/'+this._cusId + "?orderid=" + this._orderId;
      }
    }

    this._ODFSerivce.getOrderFlow(param).subscribe(
        data => {
            var newData = data[0].value.map((item, index) => {
              var newObject = {
                odr_flow_id: item.odr_flow_id,
                odr_sts: item.odr_sts,
                type: item.type,
                description: item.description,
                flow_code: item.flow_code,
                dependency: item.dependency,
                name: item.name,
                step: item.step,
              };
              data.forEach((itemData, indexData) => {
                switch (data[indexData].qualifier) {
                  case 'OFF':
                    newObject['bulk'] = {
                      qualifier:data[indexData].qualifier,
                      value: data[indexData].value[index].usage
                    }
                  break;
                  case 'OSF':
                    newObject['usage'] = {
                      qualifier:data[indexData].qualifier,
                      value: data[indexData].value[index].usage
                    }
                  break;
                  case 'OEF':
                    newObject['ecom'] = {
                      qualifier:data[indexData].qualifier,
                      value: data[indexData].value[index].usage
                    }
                  break;

                  default:
                    break;
                }
              });
              return newObject;
            });
            this.orderFlowDetail['value'] = newData;
            this.orderFlowDetail['value'].forEach(odrFlow => {
              if (odrFlow['flow_code'] === 'PAM') {
                this._disabledShippingArea(odrFlow['bulk'].value === 1, 'bulk');
                this._disabledShippingArea(odrFlow['usage'].value === 1, 'usage');
              }
            });
            this._disabledShippingArea(true, 'ecom');
            this.orderFlowReturnData.emit(data);
        },
        err => {
          // this.showLoadingOverlay.emit(false);
          this.parseError(err);
        },
        () => {
          // this.showLoadingOverlay.emit(false);
        }
    );

  }


  /*=========================
  * Return data every changed value
  *======================== */
  private changeCheckedOrderFlow($event, item, typeItem) {
    item[typeItem].value = $event.target.checked ? 1 : 0;

    if (item.flow_code === 'PAM') {
      if (item[typeItem].value === 1) {
        this.orderFlowDetail['value'].forEach(step => {
          if (typeItem == 'bulk') {
            if (step.flow_code === 'ARS') {
              step[typeItem].value = 1;
              step[typeItem].disabled = true;
            }
          }
          if (typeItem == 'usage') {
            if (step.flow_code === 'BOL' || step.flow_code === 'ARS') {
              step[typeItem].value = 1;
              step[typeItem].disabled = true;
            }
          }
        });
      } else {
        this._disabledShippingArea(false, typeItem);
      }
    }

    if (item.flow_code === 'ASS') {
      const isCheck = !this._orderId ? item[typeItem].value === 1 : item.usage === 1;
      const checkValue = isCheck ? 1 : 0;

      const itemARS = this.orderFlowDetail['value'].find((step) => step['flow_code'] === 'ARS');
      if (!this._orderId) {
        itemARS[typeItem].value = checkValue;
      } else {
        itemARS.usage = checkValue;
      }
    }

    const convertData = this.getValueByQualifier();
    this.orderFlowReturnData.emit(convertData);
  }


  getValueByQualifier() {

    let arrayOrderType = ['OSF','OFF', 'OEF'];
    let orderFlowDataConvert: Array<any> = [];
    arrayOrderType.forEach((type)=>{
      orderFlowDataConvert.push({
        qualifier : type,
        value : this.getArrayQualifierByKey(type)
      })

    });

    return orderFlowDataConvert;

  }

  /*
   // fieldname like bulk / ecom / usage
  */
  getArrayQualifierByKey(key:any) {

    let arr = [], fieldQualifier:any;

    this.orderFlowDetail['value'].forEach((item)=>{

      if (item['ecom']['qualifier'] == key){
          fieldQualifier = 'ecom';
      } else if ( item['usage']['qualifier'] == key){
        fieldQualifier = 'usage';
      } else if (item['bulk']['qualifier'] == key){
        fieldQualifier = 'bulk';
      }

        if(item['ecom']['qualifier'] == key || item['usage']['qualifier'] == key || item['bulk']['qualifier'] == key){
          arr.push({
            odr_flow_id: item.odr_flow_id,
            step: item.step,
            flow_code: item.flow_code,
            odr_sts: item.odr_sts,
            name: item.name,
            description: item.description,
            dependency: item.dependency,
            type: item.type,
            usage: item[fieldQualifier].value,
          })
        }
    });

    return arr;
  }


  private parseError(err){
    this.messages.emit({'status' : 'danger', 'txt' : this._Func.parseErrorMessageFromServer(err)});
  }

  private _disabledShippingArea(disabled: boolean = false, typeItem) {
    this.orderFlowDetail['value'].forEach(step => {
      if (typeItem == 'bulk') {
        if (['ARS'].indexOf(step.flow_code) !== -1 && step.bulk.value != 0) {
          step.bulk.disabled = disabled;
        } else {
          step.bulk.disabled = false;
        }
      }

      if (typeItem == 'usage') {
        const arrayRTLDisable = ['CSR'];
        const defaultCheckRTL = ['CSR'];
        if (['BOL'].indexOf(step.flow_code) !== -1 && step.usage.value != 0) {
          step.usage.disabled = disabled;
        } else {
          step.usage.disabled = false;
        }
        if (arrayRTLDisable.indexOf(step.flow_code) > -1) {
          step.usage.disabled = true;
        }
        if (defaultCheckRTL.indexOf(step.flow_code) > -1) {
          step.usage.value = 1;
        }
      }

      if (typeItem == 'ecom') {
        const arrayEcomDisable = ['POD', 'APO', 'PAM', 'BOL', 'ASS', 'ARS', 'CSR'];
        const defaultCheckEcom = ['PAM', 'ASS', 'ARS', 'CSR'];
        if (arrayEcomDisable.indexOf(step.flow_code) > -1) {
          step.ecom.disabled = true;
        }
        if (defaultCheckEcom.indexOf(step.flow_code) > -1) {
          step.ecom.value = 1;
        }
      }
    });
  }
}
