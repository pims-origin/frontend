import {Component, OnInit} from '@angular/core';
import {RouteParams,Router } from '@angular/router-deprecated';
import {UserService} from '../../common/core/load';
import { Http } from '@angular/http';
import { JwtHelper } from 'angular2-jwt/angular2-jwt';
import {Functions} from "../../common/core/functions";
import {InventoryUpdateMessages } from '../inventory-update/inventory-update-messages';
import {BoxPopupService} from "../../common/popup/box-popup.service";
import {PutAwayServices} from "../putaway.service";
import {WMSBreadcrumb} from '../../common/directives/directives';
import {WMSMessages} from "../../common/directives/messages/messages";
import {PaginationControlsCmp} from "../../common/directives/pagination/pagination-controls-cmp";
import {PaginationService} from "../../common/directives/pagination/pagination-service";
import {PaginatePipe} from "../../common/directives/pagination/paginate-pipe";
declare var jQuery: any;
declare var swal: any;

@Component({
  selector: 'inventory-update',
  directives: [WMSBreadcrumb,WMSMessages,PaginationControlsCmp],
  providers: [BoxPopupService, PutAwayServices, InventoryUpdateMessages,PaginationService],
  pipes: [PaginatePipe],
  templateUrl: 'inventory-update.component.html',

})
export class InventoryUpdateComponent{

  private messages;
  private listMessage = {};
  private timeoutHideMessage;
  private showLoadingOverlay = false;

  private goodReceiptID;
  private whs_id;
  private dataGoodReceipt;
  private dataGoodReceiptItems;
  private isLoadingGRItem;
  private hasPermissionEdit;
  private isHideButton;
  private listMsgs = {
    'ER1': 'Location is not existed or unavailable.',
    'ER2': 'Location is in cycle count process',
    'ER3': 'Location is {{loc_status}}',
    'ER4': 'Location already has pallet'
  }
  private listUncheckLocation = [];

  constructor(
    private _http: Http,
    private _user: UserService,
    private _func: Functions,
    private _putAwayServices: PutAwayServices,
    private _messageService: InventoryUpdateMessages,
    private params: RouteParams,
    private _boxPopupService: BoxPopupService,
    public jwtHelper: JwtHelper,
    private _router: Router) {
  }

  ngOnInit() {
    this.listMessage = this._messageService.messages;
    this.goodReceiptID = this.params.get('id');
    if(localStorage.getItem('whs_id')) {
      this.whs_id=localStorage.getItem('whs_id');
    }
    // this.getGoodReceiptDetail();
    // this.getGoodReceiptItems();
    this.checkPermission();
    jQuery(window).on('scroll.reposition resize.reposition', function() {
      var ul = jQuery('.autocomplete:visible');
      if(ul.length) {
        var input = ul.prev('input');
        ul.css({
          width: input.outerWidth(),
          top: input.offset().top - jQuery(window).scrollTop() + input.outerHeight(),
          left: input.offset().left
        })
      }
    });
  }

  private checkPermission() {
    this.showLoadingOverlay = true;
    this._user.GetPermissionUser().subscribe(
      data => {
        this.hasPermissionEdit = this._user.RequestPermission(data,'updateInventory');
        if(!this.hasPermissionEdit) {
          this._router.parent.navigateByUrl('/deny');
        }
        else {
          this.getGoodReceiptDetail();
          this.getGoodReceiptItems();
        }
      },
      err => {
        this.showMessage('danger', this._func.parseErrorMessageFromServer(err));
        this.showLoadingOverlay = false;
      }
    );
  }

  private showMessage(msgStatus, msg) {
    // if(this.timeoutHideMessage) clearTimeout(this.timeoutHideMessage);
    this.messages = {
      'status': msgStatus,
      'txt': msg
    }
    // this.timeoutHideMessage = setTimeout(() => {
    //     this.messages = null;
    // }, 4000);
    jQuery(window).scrollTop(0);
  }

  private getGoodReceiptDetail () {
    this._putAwayServices.getGoodsReceiptById(this.goodReceiptID)
      .subscribe(
        data => {
          this.dataGoodReceipt = data.data;
        },
        err => {
        }
      );
  }

  private getGoodReceiptItems () {
    this.isLoadingGRItem = true;
    this.showLoadingOverlay = true;
    this._putAwayServices.getGoodReceiptItems(this.goodReceiptID)
      .subscribe(
        data => {
          for(var i = 0, l = data.data.length; i < l; i++) {
            data.data[i].old_loc_id = data.data[i].actual_loc_id ? data.data[i].actual_loc_id : 0;
            data.data[i].checked = data.data[i].assigned_loc_id && (!data.data[i].actual_loc_id || data.data[i].actual_loc_id == data.data[i].assigned_loc_id) ? true : false;
            data.data[i].disable_checkbox = data.data[i].actual_loc_code  ? true : false;
            if(data.data[i].actual_loc_id) {this.listUncheckLocation.push(data.data[i].actual_loc_id)};
            if(data.data[i].actual_loc_code == null){
              data.data[i].actual_loc_code = '';
            }
            if(!data.data[i].disable_checkbox){
              this.isHideButton = true;
            }
          }
          this.dataGoodReceiptItems = data.data;
          this.isLoadingGRItem = false;
        },
        err => {
          if(err.json().errors.message != undefined){
            this.showMessage('danger', err.json().errors.message);
          }
          this.showLoadingOverlay = false;
        },
        () => {this.showLoadingOverlay = false;this.isLoadingGRItem = false;}
      );
  }

  // V1
  // private submitForm(params) {
  //   this.showLoadingOverlay = true;
  //   var submitData = [],
  //     hasError = false;
  //   let l = this.dataGoodReceiptItems.length;
  //   for(let i = 0; i < l; i++) {
  //     let item = this.dataGoodReceiptItems[i],
  //       locationID = !item.checked ? item.actual_loc_id : item.assigned_loc_id;

  //     if(locationID) {
  //       submitData.push({
  //         plt_id: item.plt_id,
  //         loc_id: locationID,
  //         old_loc_id: item.old_loc_id
  //       })
  //     }
  //     else {
  //       if(!item.checked) {
  //         item.hasErrRequired = true;
  //       }
  //     }
  //     // @hoangtran 29/11/2016 check out actual location before saving
  //     let locationValue = '';
  //     if(!item.checked && item.actual_loc_code != '') {
  //       locationValue = item.actual_loc_code;
  //     }else{
  //       locationValue = item.assigned_loc_code;
  //     }
  //     let param = '?loc_code='+encodeURIComponent(locationValue)+'&limit=20';
  //     //item.searching = true;
  //     if(item.subscribe) {
  //       item.subscribe.unsubscribe();
  //     }
  //     item.subscribe = this._putAwayServices.searchLocation(this.whs_id, this.dataGoodReceipt.cus_id, param)
  //       .subscribe(
  //         data => {
  //           item.listAutocompleteLocation = data.data
  //           this.checkExistLocation(item, locationValue);
  //           if(item.hasErrRequired || item.notExist || item.searching || item.hasErrGreater || item.duplicated) {
  //             hasError = true;
  //           }
  //           //API calling
  //           if(i == l - 1){
  //             if(!hasError) {
  //               params = params ? this.goodReceiptID + params : this.goodReceiptID;
  //               this._putAwayServices.updateGoodReceiptItems(params, JSON.stringify(submitData))
  //                 .subscribe(
  //                   data => {
  //                     // console.log('update success', data);
  //                     this.showMessage('success', this.listMessage['update_success']);
  //                     setTimeout(() => {
  //                       this._router.parent.navigateByUrl('/putaway/inventory-update-list');
  //                     }, 600);
  //                     this.showLoadingOverlay = false;
  //                   },
  //                   err => {
  //                     if(typeof err == 'object' && err._body) {
  //                       let parseErr = JSON.parse(err._body);
  //                       if(parseErr.errors && parseErr.errors.status_code == 400 && (parseErr.errors.code == 4 || parseErr.errors.code == 5)) {
  //                         /*params = parseErr.errors.code == 4 ? '?removeLoc=1' : '?activeLoc=1';
  //                         swal({
  //                           title: '<h4>' + parseErr.errors.message + '</h4>',
  //                           type: 'warning',
  //                           showCancelButton: true,
  //                           showConfirmButton: true,
  //                           confirmButtonColor: '#3085d6',
  //                           cancelButtonText: 'No',
  //                           confirmButtonText: 'Yes',
  //                           confirmButtonClass: 'btn btn-primary',
  //                           cancelButtonClass: 'btn btn-default margin-left',
  //                           buttonsStyling: false
  //                         }).then((agree) => {
  //                           if(agree) {
  //                             this.submitForm(params)
  //                           }
  //                         });*/
  //                         this.showMessage('danger', 'Some location already have pallets.');
  //                       }
  //                       else {
  //                         this.showMessage('danger', this._func.parseErrorMessageFromServer(err));
  //                       }
  //                     }
  //                     else {
  //                       this.showMessage('danger', this._func.parseErrorMessageFromServer(err));
  //                     }
  //                     this.showLoadingOverlay = false;
  //                   },
  //                   () => {this.showLoadingOverlay = false;}
  //                 );
  //             }else{
  //               this.showLoadingOverlay = false;
  //             }
  //           }
  //         },
  //         err => {
  //           this.showLoadingOverlay = false;
  //         },
  //         () => {

  //         }
  //       )// end
  //   }
  // }

  // V2
  private submitForm(params) {    
    var submitData = [],
        listLocation = [],
        hasErr = false;
    this.messages = null;
    for(let i = 0, l = this.dataGoodReceiptItems.length; i < l; i++) {
      let item = this.dataGoodReceiptItems[i],
        locationID = !item.checked ? item.actual_loc_id : item.assigned_loc_id;

      if(locationID) {
        submitData.push({
          plt_id: item.plt_id,
          loc_id: locationID,
          old_loc_id: item.old_loc_id
        })
        if(this.listUncheckLocation.indexOf(locationID) == -1) {
          listLocation.push(locationID);
        }
      }
      else {
        if(!item.checked && !item.actual_loc_code.trim()) {
          item.hasErrRequired = true;
          hasErr = true;
        }
      }

      if (item.duplicated) {
        hasErr = true;
      }
    }
 
    if(!hasErr && ((listLocation.length + this.listUncheckLocation.length) == this.dataGoodReceiptItems.length)) {
      let dataCheck = {loc_ids: listLocation};
      this.showLoadingOverlay = true;
      this._putAwayServices
        .validateGoodReceiptItems(this.whs_id, this.dataGoodReceipt.cus_id, JSON.stringify(dataCheck))
        .subscribe(
          data => {
            if(data.valid) {
              params = params ? this.goodReceiptID + params : this.goodReceiptID;
              this._putAwayServices.updateGoodReceiptItems(params, JSON.stringify(submitData))
                .subscribe(
                  data => {
                    this.showMessage('success', this.listMessage['update_success']);
                    setTimeout(() => {
                      this._router.parent.navigateByUrl('/putaway/inventory-update-list');
                    }, 600);
                    this.showLoadingOverlay = false;
                  },
                  err => {
                    this.showMessage('danger', this._func.parseErrorMessageFromServer(err));
                    this.showLoadingOverlay = false;
                  }
                );
            }
            else {
              let errMsg = '';
              for(let i = 0; i < data.errors.length; i++) {
                errMsg += `<p>${data.errors[i]}</p>`;
              }
              if(errMsg) {
                this.showMessage('danger', errMsg);
              }
              this.showLocationsError(listLocation, data.details);
              this.showLoadingOverlay = false;
            }
          },
          err => {
            this.showMessage('danger', this._func.parseErrorMessageFromServer(err));
            this.showLoadingOverlay = false;
          });
    }
  }

  private showLocationsError(listLocation, errDetails) {
    for(let i = 0, l = listLocation.length; i < l; i++) {
      let err = errDetails[listLocation[i]];
      if(err) {
        this.dataGoodReceiptItems[i].notExist = true;
        this.dataGoodReceiptItems[i].notExistMsg = err;
      }
    }
  }

  private cancel() {
    var that = this;
    this._boxPopupService.showWarningPopup()
      .then(function (ok) {
        if(ok)
          that._router.navigateByUrl('/putaway/inventory-update-list');
      });
  }

  private toggleActualLocation(item) {
    item.checked = !item.checked;
    item.actual_loc_id = null;
    item.actual_loc_code = '';
    if(item.checked) {
      // item.actual_loc_id = item.assigned_loc_id;
      // item.actual_loc_code = item.assigned_loc_code;
      item.notExist = false;
      item.hasErrRequired = false;
      clearTimeout(item.timeoutHideDropdown);
      item.showDropdown = false;
      jQuery('.autocomplete:visible').css('display', 'none');
      item.listAutocompleteLocation = null;
      if(item.subscribe) {
        item.subscribe.unsubscribe();
      }
      item.searching = false;
    }
    this.checkDuplicated();
  }

  private searchLocation(item, value) {
    item.searching = true;
    let param='?loc_code='+encodeURIComponent(value)+'&limit=20';
    if(item.subscribe) {
      item.subscribe.unsubscribe();
    }

    item.subscribe = this._putAwayServices.searchLocation(this.whs_id, this.dataGoodReceipt.cus_id, param)
      .subscribe(
        data => {
          item.listAutocompleteLocation = data.data;
          if(item.listAutocompleteLocation && item.listAutocompleteLocation.length) {
            for(var i = 0, l = item.listAutocompleteLocation.length; i < l; i++) {
              var loc = item.listAutocompleteLocation[i];
              if(loc.loc_code == value) {
                item.actual_loc_id = loc.loc_id;
              }
            }
          }
          this.checkDuplicated();
        },
        err => {},
        () => {item.searching = false;}
      )
  }

  private checkExistLocation(item, value) {
    let notExist = true,
      errMsg = this.listMsgs.ER1;
    if(item.listAutocompleteLocation && item.listAutocompleteLocation.length) {
      for(let i = 0, l = item.listAutocompleteLocation.length; i < l; i++) {
        let loc = item.listAutocompleteLocation[i];
        if(loc.loc_code == value) {
          item.act_loc_id = loc.loc_id;
          notExist = false;
          // if(loc.loc_sts_code != 'AC') {
          //   notExist = true;
          //   if(loc.loc_sts_code == 'CL') {
          //     errMsg = this.listMsgs.ER2;
          //   }
          //   else {
          //     errMsg = this.listMsgs.ER3.replace('{{loc_status}}', loc.loc_sts_code_name);
          //   }
          // }
          // else 
          //   if(loc.has_pallet == 1) {
          //     notExist = true;
          //     errMsg = this.listMsgs.ER4;
          //   }
          break;
        }
      }
    }
    item.notExist = notExist;
    item.notExistMsg = errMsg;
  }

  private checkDuplicated() {
    var duplicates = {},
      listLocationsID = [],
      arrDuplicatedPos = [];

    for (var i = 0; i < this.dataGoodReceiptItems.length; i++) {
      var item = this.dataGoodReceiptItems[i],
        locationID = item.checked ? item.assigned_loc_id : item.actual_loc_id;
      item.duplicated = false;
      if(locationID) {
        listLocationsID.push(locationID);
      }
      else {
        listLocationsID.push('_null');
      }
    }
    for (var i = 0; i < listLocationsID.length; i++) {
      if(duplicates.hasOwnProperty(listLocationsID[i])) {
        duplicates[listLocationsID[i]].push(i);
      } else if (listLocationsID.lastIndexOf(listLocationsID[i]) !== i) {
        duplicates[listLocationsID[i]] = [i];
      }
    }

    for(let key in duplicates) {
      if(key != '_null') {
        arrDuplicatedPos = arrDuplicatedPos.concat(duplicates[key]);
      }
    }
    // console.log(arrDuplicatedPos);
    if(arrDuplicatedPos.length) {
      for(let i = 0, l = arrDuplicatedPos.length; i < l; i++) {
        this.dataGoodReceiptItems[arrDuplicatedPos[i]].duplicated = true;
      }
    }
  }

  private showDropdown(item, $event) {
    var value = $event.target.value;
    item.showDropdown = true;
    this.searchLocation(item, value);
    var input = jQuery($event.target);
    var ul = input.next('ul');
    ul.css({
      display: 'block',
      position: 'fixed',
      width: input.outerWidth(),
      top: input.offset().top - jQuery(window).scrollTop() + input.outerHeight(),
      left: input.offset().left
    })
  }

  private hideDropDown(item, $event) {
    item.timeoutHideDropdown = setTimeout(() => {
      var value = jQuery.trim($event.target.value);
      item.showDropdown = false;
      var input = jQuery($event.target);
      var ul = input.next('ul');
      ul.css({ display: 'none' })
      if(!item.checked) {
        if(!value) {
          item.hasErrRequired = true;
        }
        else {
          if(!item.searching) {
            this.checkExistLocation(item, value);
            this.checkDuplicated();
          }
          else {
            item.searching = true;
            if(item.subscribe) {
              item.subscribe.unsubscribe();
            }
            let param='?loc_code='+encodeURIComponent(value)+'&limit=20';
            item.subscribe = this._putAwayServices.searchLocation(this.whs_id, this.dataGoodReceipt.cus_id, param)
              .subscribe(
                data => {
                  item.listAutocompleteLocation = data.data;
                  this.checkExistLocation(item, value);
                  this.checkDuplicated();
                },
                err => {},
                () => {item.searching = false;}
              )
          }
        }
      }
    }, 200)
  }

  private autoSearchLocation(item, value) {
    item.hasErrRequired = jQuery.trim(value) ? false : true;
    item.notExist = false;
    item.notExistMsg = this.listMsgs.ER1;
    item.actual_loc_id = null;
    if(!item.hasErrRequired) {
      this.searchLocation(item, value);
    }
  }

  private selectItem(item, locID, locCode, $event) {
    clearTimeout(item.timeoutHideDropdown);
    item.hasErrRequired = false;
    item.showDropdown = false;
    item.actual_loc_code = locCode;
    item.actual_loc_id = locID;
    jQuery($event.target).closest('ul').css('display', 'none');
    if(item.assigned_loc_code == locCode){
      item.checked = true;
      //item.actual_loc_code = '';
    }else{
      this.checkExistLocation(item, locCode);
      this.checkDuplicated();
    }
  }
}
