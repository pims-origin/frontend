
import {Component} from '@angular/core';
import {API_Config} from '../../common/core/API_Config';
import {BaseTable} from '../../common/core/classes_us/base-table';
import {TableFunctions} from '../../common/core/table-functions';
import {DateFunctions} from '../../common/core/functions/date-functions';
import {BaseService} from '../../common/core/classes_us/base-service';
import {CustomersService} from '../../master-data/customers/customers-service';
import {TableStructure} from '../../common/directives/directives';

@Component ({
    selector: 'receiving-report',
    directives: [
        TableStructure
    ],
    templateUrl: '../../common/directives/table-structure/table.component.html',
    providers: [
        BaseService,
        TableFunctions,
        DateFunctions,
        CustomersService
    ]
})

export class RcvReportComponent extends BaseTable
{
    public tableID = 'available-inventory';
    public url = this._api.API_RECEIVING_REPORTS;
    public headerUrlSuffix = 'rep';
    public title = 'Receiving Report';
    public permissionPage = 'viewReport';
    public useExport = true;
    public exportMethod = 'GET';

    public headerDef = [
        {
            id: 'ver_table',
            value: 1 + Math.random() * 10000
        },
        this.columnList.warehouse,
        this.columnList.customer,
        this.columnList.container,
        this.columnList.asnNumber,
        this.columnList.receivingNumber,
        this.columnList.asnCreatedAt,
        this.columnList.asnFullName,
        this.columnList.asnttlsku,
        this.columnList.asnttlcarton,
        this.columnList.cube,
        this.columnList.asnttlpieces,
        this.columnList.grCreatedAt,
        this.columnList.grDate,
        this.columnList.grFullName,
        this.columnList.grttlsku,
        this.columnList.grttlcarton,
        this.columnList.grttlpieces,
        this.columnList.grdtlpalletotal,
        
    ];

    public unsortableColumns = [
        'warehouse',
        'asnttlsku',
        'asnttlcarton',
        'asnttlpieces',
        'grttlsku',
        'grttlcarton',
        'grttlpieces',
        'asnFullName',
        'grFullName',
        'asnNumber',
        'receivingNumber',
        'asnCreatedAt',
        'grCreatedAt',
        'customer'
    ];

    public searches = [
        this.searchList.customer,
        this.searchList.container,
        this.searchList.createdGrStart,
        this.searchList.createdGrFinish,
    ];

    /*
    ****************************************************************************
    */

    constructor (
        public _api: API_Config,
        public _tableFunc: TableFunctions,
        public _tableService: BaseService,
        public _customersService: CustomersService
    )
    {
        super();

        this._tableFunc.permissionCall(this);
    }

    //**************************************************************************

}
