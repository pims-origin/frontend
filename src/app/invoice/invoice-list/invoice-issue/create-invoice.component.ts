
import {Component, ViewChild} from '@angular/core';
import {RouteParams} from '@angular/router-deprecated';
import {API_Config} from '../../../common/core/API_Config';
import {TableFunctions} from '../../../common/core/table-functions';
import {TableStructure} from '../../../common/directives/directives';
import {DateFunctions} from '../../../common/core/functions/date-functions';
import {CountryService} from '../../../master-data/country/country-service';
import {StateService} from '../../../master-data/state/state-service';
import {BaseService} from '../../../common/core/classes_us/base-service';

import {InvoiceIssue} from './invoice-issue';

import {ClientInfoCompoundComponent} from '../client-info/client-info-compound.component';
import {InvoiceHeaderComponent} from './invoice-header.component';
import {InvoiceBodyComponent} from './invoice-body.component';

import {Confirm} from '../../../common/directives/popup-modal_us/confirm/confirm';

@Component({
    selector: 'invoice-issue',
    directives: [
        TableStructure,
        ClientInfoCompoundComponent,
        InvoiceHeaderComponent,
        InvoiceBodyComponent,
        Confirm
    ],
    providers: [
        TableFunctions,
        CountryService,
        StateService,
        DateFunctions,
        BaseService
    ],
    templateUrl: 'invoice-issue.component.html',
})

export class CreateInvoiceComponent extends InvoiceIssue
{
    @ViewChild(Confirm) confirm: Confirm;

    @ViewChild(InvoiceHeaderComponent) _invoiceHeaderComponent: InvoiceHeaderComponent;
    @ViewChild(InvoiceBodyComponent) _invoiceBodyComponent: InvoiceBodyComponent;

    constructor (
        protected _routeParams: RouteParams,
        protected _api: API_Config,
        protected _tableFunc: TableFunctions,
        protected _dateFunctions: DateFunctions,
        protected _countryService: CountryService,
        protected _stateService: StateService,
        protected _baseService: BaseService
    )
    {
        super(
            _api,
            _dateFunctions,
            _baseService
        );

        let url = this._api.API_Invioce_Process + '/getCreateParams'
                + '?inv_cr_id=' + this._routeParams.get('inv_cr_id');

        this._baseService.get(url)
            .subscribe(
                data => {

                    this.dateCaption = 'Date Range Selected: ' + data.storRange;

                    this.showInvoiceIssue(data);
                },
                err => {
                },
                () => {
                    this.showLoadingOverlay = false;
                }
        );
    }

    //**************************************************************************

    public goBack(action)
    {
        if (action == 'print') {

            window.top.close();
            
            return true;
        }

        let message = 'The tab will be closed. Invoice will not be created.'
                    + ' Proceed?';

        this.confirm.show(message, {
            action: 'closeTab'
        });
    }

    //**************************************************************************

    public onConfirm($event)
    {
        if ($event.response) {
            window.top.close();
        }
    }

    //**************************************************************************
}

