import {Component, Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {Functions, API_Config} from '../../../common/core/load';

@Injectable()
export class OrderListService {

    public AuthHeader = this._Func.AuthHeader();
    public AuthHeaderPost = this._Func.AuthHeaderPost();

    constructor(
        private _Func: Functions,
        private http: Http,
        private _api: API_Config) {
    }

    getOrderStatusList(search) {
        return this.http.get(this._api.API_ORDERS_CORE + '/order-statuses?' + search,{ headers: this.AuthHeader })
            .map((res: Response) => res.json());
    }

    getOrderTypeList() {
        return this.http.get(this._api.API_ORDERS_CORE + '/order-types',{ headers: this.AuthHeader })
            .map((res: Response) => res.json());
    }

    getOrderList(params) {
        return this.http.get(this._api.API_ORDERS_CORE + '/orders' + params,{ headers: this.AuthHeader })
            .map((res: Response) => res.json());
    }

    getOrderShippingList(params) {
        return this.http.get(this._api.API_ORDERS_CORE + '/order-shippings' + params,{ headers: this.AuthHeader })
            .map((res: Response) => res.json());
    }

    getListCustomersByUser($params) {
        return  this.http.get(this._api.API_Customer_User + $params + '&sort[cus_name]=asc' , {headers: this.AuthHeader})
            .map(res => res.json());
    }

    getListCRSByCustomer($params, $wh_id = '') {
        return  this.http.get(this._api.API_Authen + '/users/get-csr/' + $params + $wh_id, {headers: this.AuthHeader})
            .map(res => res.json());
    }

    cancelOrder(listOrder) {
        return this.http.get(this._api.API_ORDERS_CORE+'/orders/cancel?odr_id=' +listOrder, { headers: this.AuthHeader })
            .map((res: Response) => res.json());
    }

    // v1
    // shipOrder(listOrder) {
    //   return this.http.get(this._api.API_ORDERS_CORE+'/order-shippings/shipped?order_id=' +listOrder, { headers: this.AuthHeader })
    //     .map((res: Response) => res.json());
    // }

    // v2
    shipOrder(orderId, shipped_dt) {
        return this.http.get(this._api.API_ORDERS_CORE+'/order-shippings/shipped?order_id=' +orderId + '&shipped_dt=' + shipped_dt, { headers: this.AuthHeader })
            .map((res: Response) => res.json());
    }

    saveShippedDate(stringData, $orderID) {
        return this.http.put(this._api.API_ORDERS_CORE+ '/orders/' +  $orderID + '/update-shipdt', stringData, { headers: this._Func.AuthHeaderPostJson() })
            .map((res: Response) => res.json().data);
    }

    completePikingOrder(orderId) {
        return this.http.get(this._api.API_ORDERS+'/complete-order-picking/' + orderId, { headers: this.AuthHeader })
            .map((res: Response) => res.json());  
    }

    completeXDock(data, cusId, whsId) {
        return this.http.post(this._api.API_URL_ROOT+'/core/barcode/v1/' + whsId + '/'+ cusId + '/completed_order', data, { headers: this._Func.AuthHeaderPostJson() })
            .map((res: Response) => res.json());
    }

    updateScheduleRouteDate(orderId, data){
        return this.http.put(this._api.API_ORDERS + '/edit-schedule-ship-and-routed-date/' + orderId, data, { headers: this._Func.AuthHeaderPostJson() })
            .map((res: Response) => res.json());
    }

    updateComment(data) {
        return this.http.put(this._api.API_ORDERS + '/update-comments-many-orders', data, { headers: this._Func.AuthHeaderPostJson() })
            .map((res: Response) => res.json().data);
    } 
    
    mergeOrder(data) {
        return this.http.put(this._api.API_ORDERS + '/merge-orders', data, { headers: this._Func.AuthHeaderPostJson() })
            .map((res: Response) => res.json().data);
    }
}
