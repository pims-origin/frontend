import {Component,Input,ElementRef,AfterViewInit} from '@angular/core';
import { FORM_DIRECTIVES,ControlGroup,FormBuilder, Control ,Validators} from '@angular/common';
import {RouteParams,Router ,RouteData} from '@angular/router-deprecated';
import {API_Config, UserService, Functions} from '../../common/core/load';
import { ValidationService } from '../../common/core/validator';
import { Http } from '@angular/http';
import { ControlMessages } from '../../common/core/control-messages';
import {GoBackComponent} from "../../common/component/goBack.component";
import {BlockStockServices} from "../block-stock-service";
import {ErrorMessageForControlService} from "../../common/services/error-message-for-control.service";;
import {BoxPopupService} from "../../common/popup/box-popup.service";
import {WMSBreadcrumb,WMSPagination,WMSMessages} from '../../common/directives/directives';
declare var jQuery:any;

@Component({
  selector: 'create-cycle-count',
  directives: [
    FORM_DIRECTIVES,
    ControlMessages,
    GoBackComponent,
    WMSBreadcrumb,
    WMSPagination,
    WMSMessages
  ],
  providers: [
    BlockStockServices,
    ValidationService,
    FormBuilder,
    ErrorMessageForControlService,
    BoxPopupService,
    ValidationService
  ],
  templateUrl: 'create-block-stock-reason.component.html',
})

export class CreateBlockStockReasonComponent {
  Form: ControlGroup;
  public loading = [];
  private messages;
  private action;
  private whs_id;
  private listNull = {};
  private dataListReason=[];
  private Pagination={};
  private perPage=20;
  private currentPage = 1;
  private numLinks = 3;
  private showForm:boolean=true;
  private submitForm:boolean=false;
  private showLoadingOverlay = false;

  constructor(private _http:Http,
              private _Func:Functions,
              private _user:UserService,
              private _Valid: ValidationService,
              private _blockStockService: BlockStockServices,
              private _boxPopupService:BoxPopupService,
              private fb:FormBuilder,
              _RTAction:RouteData,
              private _router:Router) {


    if (localStorage.getItem('whs_id')) {
      this.whs_id = localStorage.getItem('whs_id');
      this.checkPermission();
    }

  }

  private canCreate;
  private canView;
  private canEdit;

  private checkPermission() {

    this.showLoadingOverlay = true;

    this._user.GetPermissionUser().subscribe(
        data => {

          this.canCreate = this._user.RequestPermission(data, 'createBlockReason');
          this.canView = this._user.RequestPermission(data, 'viewBlockReason');
          this.canEdit = this._user.RequestPermission(data, 'editBlockReason');
          if(!this.canCreate && !this.canView && !this.canEdit){
            this._router.parent.navigateByUrl('/deny');
          }

          this.showLoadingOverlay = false;
          this.formBuilder();
          this.getListReason();

        },
        err => {

          this.parseError(err);

          this.showLoadingOverlay = false;
        }
    );
  }

  private formBuilder(data={}){

    this.Form =this.fb.group({
      id: [data['id']],
      block_rsn_name: [data['block_rsn_name'],Validators.compose([Validators.required, this._Valid.validateSpace])],
      block_rsn_des: [data['block_rsn_des']],
      whs_id:[this.whs_id]
    });

  }


  // Get reason
  private getListReason(page=1) {

    this.showLoadingOverlay=true;
    let param="limit="+this.perPage+"&page="+page;
    this._blockStockService.getListReason(param)
        .subscribe(
            data => {
              this.showLoadingOverlay=false;
              this.dataListReason = data.data;
              this.Pagination = data['meta']['pagination'];
              this.Pagination['numLinks'] = 3;
              this.Pagination['tmpPageCount'] = this._Func.Pagination(this.Pagination);
            },
            err => {
              this.showLoadingOverlay=false;
              this.parseError(err);
            },
            () => {
            }
        );
  }

  private Save(){

    this.submitForm=true;
    if(this.Form.valid){
      this.showLoadingOverlay=true;
      this._blockStockService.addReasonBlockStock(JSON.stringify(this.Form.value))
          .subscribe(
              data => {
                this.showLoadingOverlay=false;
                this.messages = this._Func.Messages('success', this._Func.msg('BS001'));
                this.getListReason(1);

                this.showForm=false;
                setTimeout(()=>{
                  this.showForm=true;
                  this.submitForm=false;
                  this.formBuilder();
                })

              },
              err => {
                this.showLoadingOverlay=false;
                this.parseError(err);
              },
              () => {}
          );

    }

  }

  // Event when user filter form
  private onPageSizeChanged ($event,el) {

    this.perPage = $event.target.value;

    if((this.Pagination['current_page'] - 1) * this.perPage >= this.Pagination['total']) {
      this.currentPage = 1;
    }
    else {
      this.currentPage = this.Pagination['current_page'];
    }
    this.getListReason(this.currentPage);
  }

  // Show error when server die or else
  private parseError(err) {
    this.messages = {'status': 'danger', 'txt': this._Func.parseErrorMessageFromServer(err)};
  }

}