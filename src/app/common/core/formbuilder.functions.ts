/*
* Global function use for page have formbuilder
* @tien.nguyen
* */
import {Component} from '@angular/core';
import { ControlGroup, ControlArray, Control} from '@angular/common';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';

declare var jQuery: any;
@Component({
    providers: [],
})
export class FormBuilderFunctions
{
    constructor() {}

    /*===================================
     * Generator string to check duplicated
     *=================================== */
    public _createItemToCheckDuplicated(dataArray:Object,key){

        let string='';
        let validKey=0;
        for (let i in key){
            if(dataArray[key[i]]){
                string+=dataArray[key[i]];
                validKey++;
            }
        }
        if(validKey==key.length){
            return string;
        }else{
            return false;
        }

    }


    /*=================================================
     * resetErrorItemCheckDuplicated
     * ================================================*/

    public removeErrorsByFieldName(controlArray:ControlArray,keys,fieldname_to_delete){

        let detail=controlArray.value;
        for(let row in detail){
            for(let i in keys){
                let control=controlArray.controls[row]['controls'][keys[i]];
                if(control.hasError(fieldname_to_delete)){
                    delete  control.errors[fieldname_to_delete];
                    if(!Object.keys(control.errors).length) {
                        this.setErrorsControlItem(controlArray,row,keys[i],null);
                    }

                    if (!control.value && keys[i] === 'dtl_po') {

                        this.setErrorsControlItem(controlArray,row,keys[i], {required: true});
                    }

                }
            }
        }

    }


    /*=================================================
     * setErrorsControlItem
     * ================================================*/

    public setErrorsControlItem(controlArray:ControlArray,row,control,err){
        (<Control>controlArray.controls[row]['controls'][control]).setErrors(err);
    }


    /*==================================
     * checkDuplicatedValue
     * ===============================*/
    public checkDuplicatedKey(controlArray:ControlArray,keys){

        this.removeErrorsByFieldName(controlArray,keys,'duplicated');
        let Arr=controlArray.value;
        for(let i=0;i<Arr.length;i++) {
            let item = this._createItemToCheckDuplicated(Arr[i],keys);
            for(let j=i+1;j<Arr.length;j++) {
                let itemj=this._createItemToCheckDuplicated(Arr[j],keys);
                if(i==j) {continue;}
                if((item&&itemj)&&item==itemj) {
                    this.setDuplicateItem(controlArray,i,j,keys);
                }
            }
        }

    }

    /*============================================================
     * setDuplicateItem
     * ==========================================================*/
    public setDuplicateItem(controlArray:ControlArray,row,next_row,keys){

        for(let i in keys){
            this.setErrorsControlItem(controlArray,row,keys[i],{duplicated:true});
            this.setErrorsControlItem(controlArray,next_row,keys[i],{duplicated:true});
            (<Control>controlArray.controls[row]['controls'][keys[i]]).markAsTouched();
            (<Control>controlArray.controls[next_row]['controls'][keys[i]]).markAsTouched();
        }

    }

    /*==================================================
     * removeItemArray
     * =============================================*/
    public emptyControlArray(controlArray:ControlArray)
    {
        while (controlArray.controls.length){
            controlArray.removeAt(0);
        }
    }

    /*============================================
     * getItemchecked
     *============================================*/
    public getItemchecked(controlArray:ControlArray){
        let selected=0;
        controlArray.controls.forEach((item)=>{
            if(item['selected']==true) {selected++;}
        });
        return selected;
    }

    /*============================================
     * setListItemsSelected
     *============================================*/
    public setListItemsSelected(controlArray:ControlArray,value)
    {
        controlArray.controls.forEach((item) => {
            item['selected']=value;
        });
    }

    /*================================
     * getValueControlItem
     *============================== */
    public getValueControlItem(controlArray:ControlArray,row,control){
        return controlArray.controls[row]['controls'][control].value;
    }

    /*==================================================
     * setValueControlItem
     *================================================ */
    public setValueControlItem(controlArray:ControlArray,row,control,value){
        (<Control>controlArray.controls[row]['controls'][control]).updateValue(value);
    }


    /*
     *
     * */
    public handelDropdownDatePicker(formGroup:ControlGroup, selector: string = '.datepicker-asn')
    {
        let that=this;
        jQuery(selector).each(function() {
            var elem =jQuery(this);
            elem.datepicker()
                .on('changeDate', function(e) {
                    var ngControl=elem.attr('ngControl');
                    var date= e.currentTarget.value;
                    that.setValueControl(formGroup,ngControl,date);
                });
        });

    }

    /*============================================
     * setValueControl
     * ==========================================*/
    public setValueControl(formGroup:ControlGroup,control,value){
        (<Control>formGroup['controls'][control]).updateValue(value);
    }

    /*============================================
     * get value control
     * ==========================================*/
    public getValueControl(formGroup:ControlGroup,control){
       return formGroup['controls'][control].value;
    }

    /*============================================
     * set error control
     * ==========================================*/
    public setErrorControl(formGroup:ControlGroup,control,error={}){
        (<Control>formGroup['controls'][control]).setErrors(error);
    }

    /*===============================
     * setNewValue
     * ==============================*/
    public setNewValue(formGroup:ControlGroup,control,object,value){
        formGroup['controls'][control][object]=value;
    }

    /*===============================
     * setNewValue
     * ==============================*/
    public createNewObjectItemControl(controlArray:ControlArray,row,control,object,value){
        controlArray.controls[row]['controls'][control][object]=value;
    }

    /*==============================
    * deleteItem
    * ============================*/
    public deleteItem(controlArray:ControlArray) {


        return Observable.create(observer => {

            // create array temp
            let Arr = controlArray.value;
            let ArrTemp = Arr.slice();
            let deteteAll=this.getItemchecked(controlArray)==Arr.length;
            if (deteteAll) {
                this.emptyControlArray(controlArray);
                controlArray['selectedAll']=false;
            }
            else {
                let rows=controlArray.controls;
                for(let i=0;i<rows.length;i++){
                    if(rows[i]['selected']==true) {
                        controlArray.removeAt(i);
                        i--;
                    }
                }
            }
            observer.next(deteteAll);
            observer.complete();

        });



    }

    /*==================================
     * Selected
     * =================================*/
    public Selected(controlArray:ControlArray,item,$event) {
        
        if ($event.target.checked) { item['selected']=true;}
        else { item['selected']=false; }
        if(this.getItemchecked(controlArray)==controlArray.value.length){
            controlArray['selectedAll']=true;
        }else{
            controlArray['selectedAll']=false;
        }

    }

    /*=======================================
    * controlArray:ControlArray
    *===================================== */
    public CheckAll(controlArray:ControlArray,event) {
        if (event) {
            controlArray['selectedAll']=true;
            this.setListItemsSelected(controlArray,true);
        }
        else {
            controlArray['selectedAll']=false;
            this.setListItemsSelected(controlArray,false);
        }
    }

    public getValidControlItem(controlArray:ControlArray,row,control){
       return  controlArray.controls[row]['controls'][control].valid;
    }

}
