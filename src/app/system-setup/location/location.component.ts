import {Component} from '@angular/core';
import {Router, RouterLink, RouteConfig, ROUTER_DIRECTIVES} from '@angular/router-deprecated';
import {LocationListComponent} from "./location-list/location-list.component";
import {CRULocation} from "./cru-location/location-cru";

@Component ({
    selector: 'location-management',
    directives: [ROUTER_DIRECTIVES],
    template: `<router-outlet></router-outlet>`,
})

@RouteConfig([

    /*{ path: '/new', component:AddLocationComponent  , name: 'Add Location'},
    { path: '/:id/edit', component:EditLocationComponent  , name: 'Edit Location'},
    { path: '/:id', component:LocationDetailComponent  , name: 'Location Detail'},
    { path: '/', component:LocationListComponent  , name: 'Location List', useAsDefault: true},*/

    // new cru
    { path: '/new', component:CRULocation  , name: 'Add Location' , data:{action:'new'}},
    { path: '/:id/edit', component:CRULocation  , name: 'Edit Location' , data:{action:'edit'}},
    { path: '/:id', component:CRULocation  , name: 'Location Detail' , data:{action:'view'}},
    { path: '/', component:LocationListComponent  , name: 'Location List', useAsDefault: true}
])
export class LocationManagementComponent {

}
