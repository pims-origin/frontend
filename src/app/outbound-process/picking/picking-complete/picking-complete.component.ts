import { Component } from '@angular/core';
import { FORM_DIRECTIVES, ControlGroup, FormBuilder, ControlArray, Control, Validators } from '@angular/common';
import { API_Config, Functions, UserService, FormBuilderFunctions } from '../../../common/core/load';
import { RouteParams, Router, RouteData } from '@angular/router-deprecated';
import { ValidationService } from '../../../common/core/validator';
import { BoxPopupService } from "../../../common/popup/box-popup.service";
import { OrderBy } from "../../../common/pipes/order-by.pipe";
import { WMSBreadcrumb, WMSPagination, WMSMessages, ProGressBarDirective, RFIDUnsignedCarton } from '../../../common/directives/directives';
import { GoBackComponent } from "../../../common/component/goBack.component";
import { PickingCompleteServices } from '../picking-complete/picking-complete-service';
import { WarningBoxPopup } from "../../../common/popup/warning-box-popup";
import { PickingCompleteFunctions } from "./picking-functions";
import { EventTrackingDirective } from "../../../common/directives/event-tracking/event-tracking";

declare var jQuery: any;
declare var $: any;
declare var swal: any;
declare var saveAs: any;
@Component({
    selector: 'picking-complete',
    directives: [FORM_DIRECTIVES, GoBackComponent, ProGressBarDirective, WMSBreadcrumb, WMSMessages, WMSPagination, RFIDUnsignedCarton, EventTrackingDirective],
    pipes: [OrderBy],
    providers: [ValidationService, FormBuilder, PickingCompleteFunctions, BoxPopupService, PickingCompleteServices, BoxPopupService],
    templateUrl: 'picking-complete.component.html',
})

export class PickingCompleteComponent {

    /** list permission need to check **/
    public editItem;
    public viewItem;
    public allowAccess = false;
    /* end list permission */
    PickingForm: ControlGroup
    private messages;
    private showLoadingOverlay = false;

    private dataWavepickItems;
    private waveNumber;
    private orderNumbers;
    private customerName;
    private whs_id;
    private wv_id;
    private action;
    private showAlert = false;
    private validForm = false;
    items: ControlGroup[] = [];
    itemsArray: ControlArray = new ControlArray(this.items);
    selectAll = false;
    public wpOrder = [];
    public wpSKU = {};
    public submitForm = false;

    constructor(
        private _func: Functions,
        private params: RouteParams,
        private _router: Router,
        private _Valid: ValidationService,
        private _user: UserService,
        private _API: API_Config,
        private _boxPopupService: BoxPopupService,
        private _pickingCompleteService: PickingCompleteServices,
        _RTAction: RouteData,
        private pickFunc: PickingCompleteFunctions,
        private fbdFunc: FormBuilderFunctions,
        private fb: FormBuilder) {
        this.checkPermission();
        this.whs_id = localStorage.getItem('whs_id');
        this.wv_id = this.params.get('id');
        this.action = _RTAction.get('action');
    }

    private showMessage(msgStatus, msg) {
        this.messages = {
            'status': msgStatus,
            'txt': msg
        }
        jQuery(window).scrollTop(0);
    }

    private WavepickDetail = {};
    private getWavepickDetail() {

        this._pickingCompleteService.getWavepickDetail(this._func.getWareHouseId(), this.wv_id)
            .subscribe(
                data => {
                    data = data.data;
                    this.WavepickDetail = data;
                    this.waveNumber = data.wv_num;
                    this.customerName = data.cus_name;
                    this.orderNumbers = data.odr_num;
                    this.dataWavepickItems = data.details;
                    this.showLoadingOverlay = false;
                    this.__detecteValidForm();
                },
                err => {
                    this.showMessage('danger', this._func.parseErrorMessageFromServer(err));
                    this.showLoadingOverlay = false;
                }
            );
    }

    private __detecteValidForm() {

        let valid = 0;
        this.dataWavepickItems.forEach((item) => {
            item['invalid_qty'] = item['piece_qty'] < item['act_piece_qty'] ? true : false;
            if (Array.isArray(item['act_loc'])) {
                item['suggest_loc_required'] = false;
                valid++;
            } else {
                item['suggest_loc_required'] = true;
            }
        });

        if (valid !== this.dataWavepickItems.length) {
            this.validForm = false;
            return;
        }
        this.validForm = true;

    }

    private checkPermission() {
        this.showLoadingOverlay = true;
        this._user.GetPermissionUser().subscribe(
            data => {
                this.editItem = this._user.RequestPermission(data, 'updateOrderPicking');
                this.viewItem = this._user.RequestPermission(data, 'viewWavePick');

                switch (this.action.toLowerCase()) {
                    case 'view':
                        this.allowAccess = this.viewItem || this.editItem;
                        break;
                    case 'edit':
                        this.allowAccess = this.editItem;
                        break;
                    default:
                        this.allowAccess = this.viewItem;
                        break;
                }

                if (!this.allowAccess) {
                    this.redirectDeny();
                } else {
                    this._init();
                }
            },
            err => {
                // console.log('check permission err', err);
                this.showMessage('danger', this._func.parseErrorMessageFromServer(err));
                this.showLoadingOverlay = false;
            }
        );
    }

    private redirectDeny() {
        this._router.parent.navigateByUrl('/deny');
    }

    private _init() {
        this.getWavepickDetail();
        this.getWarepickOrders();
        this.formBuilder();
    }

    private formBuilder() {
        this.PickingForm = this.fb.group({
            wv_id: new Control(this.wv_id),
            items: this.itemsArray
        });
        this.addRowItem();
    }

    private addRowItem() {
        this.itemsArray['selectedAll'] = false;
        this.itemsArray.push(
            new ControlGroup({
                'odr_id': new Control('', Validators.compose([Validators.required])),
                'item_id': new Control(''),
                'description': new Control(''),
                'sku': new Control('', Validators.compose([Validators.required, this._Valid.validateSpace])),
                'color': new Control(''),
                'size': new Control(''),
                'lot': new Control(''),
                'new_picked_qty': new Control('', Validators.compose([Validators.required, this._Valid.inPutOnlyNumber, this._Valid.isZero])),
                'pack_size': new Control(''),
                'remain_qty': new Control(''),
                'picked_loc': new Control('', Validators.compose([Validators.required])),
                'picked_loc_id': new Control(''),
                'selected': new Control(false),
                'wv_dtl_id': new Control(''),
                'old_picked_qty': new Control(''),
            })
        );
        return;

    }

    private setValueForControlItem(FormGroup, Control, Row, Value) {
        (<Control>FormGroup[Row]['controls'][Control]).updateValue(Value);
    }

    private autoSearchData = {};
    private autoComplete(row, key: any = '', fieldname) {
        if (key) {
            this.items[row]['showLoading'] = true;
            let searchkey = this._func.trim(key);
            let param = '?item_id=' + this.items[row].value['item_id'] + '&is_ecom=' + this.WavepickDetail['is_ecom'] + '&loc_code=' + encodeURIComponent(searchkey) + '&lot=' + this.items[row].value['lot'] + '&limit=20';
            this._pickingCompleteService.searchLocation(param).debounceTime(400).distinctUntilChanged().subscribe(
                data => {
                    this.items[row]['showLoading'] = false;
                    this.autoSearchData[row] = data.data;
                    this.checkExistLocation(key, data.data, fieldname, row);
                },
                err => {
                    this.items[row]['showLoading'] = false;
                    this._func.parseErrorMessageFromServerStandard(err);
                    this.checkExistLocation(key, [], fieldname, row);
                },
                () => {
                }
            );
        }
    }

    selectedItem(control, row, data) {
        this.setValueForControlItem(this.items, 'picked_loc', row, data['loc_code']);
        this.setValueForControlItem(this.items, 'picked_loc_id', row, data['loc_id']);
        // update avail
        setTimeout(() => {
            this.checkLoc_Status(control, data, row);
        });

    }

    checkLoc_Status(control, loc_item, row) {
        // reset value inactived
        if (loc_item['loc_sts_code'] == 'LK') {
            this.setErrorForControlItem(this.items, control, row, { 'loc_status_is_locked': true });
        }
        if (loc_item['loc_sts_code'] == 'IA') {
            // show popup change stt
            this.setErrorForControlItem(this.items, control, row, { 'inactived': true });
        }
        if (loc_item['loc_sts_code'] == 'CL') {
            this.setErrorForControlItem(this.items, control, row, { 'loc_status_is_locked_cycle': true });
        }
    }

    private setErrorForControlItem(FormGroup, Control, Row, Err) {
        (<Control>FormGroup[Row]['controls'][Control]).setErrors(Err);
    }

    deleteItem() {

        let n = this;
        if (this.fbdFunc.getItemchecked(this.itemsArray) > 0) {

            /*case 1: modify text*/
            let warningPopup = new WarningBoxPopup();
            warningPopup.text = this._func.msg('PBAP002');
            this._boxPopupService.showWarningPopup(warningPopup)
                .then(function (dm) {
                    if (!dm) {
                        return;
                    } else {
                        n.fbdFunc.deleteItem(n.itemsArray).subscribe(deletedAll => {
                            if (deletedAll) {
                                n.addRowItem();
                            }
                            n.itemsArray['selectedAll'] = false;
                        });
                    }
                });
        }
        else {

            this.messages = this._func.Messages('danger', this._func.msg('VR113'));

        }
    }

    checkExistLocation(str: string, arr: Array<any>, fieldname, row) {
        for (let i in arr) {
            if (this._func.trim(str) == this._func.trim(arr[i]['loc_code'])) {// auto select item if match
                //this.setErrorForControlItem(this.items,fieldname,row,null);
                this.selectedItem(fieldname, row, arr[i]);
                return;
            }
        }
        // if not found code -> set invalid
        this.setErrorForControlItem(this.items, fieldname, row, { 'invalid': true });
        return;
    }

    private getWarepickOrders() {
        this._pickingCompleteService.getWarepickOrders(this.wv_id).subscribe(
            data => {
                this.wpOrder = data['data'];
            }
        );
    }

    private getWarepickSKU(i, evt) {
        if (evt) {
            this.items[i]['showLoadingSKU'] = true;
            const odr_id = this.items[i]['controls']['odr_id'].value;
            this._pickingCompleteService.getWarepickSKU(this.wv_id, odr_id, evt).subscribe(
                data => {
                    this.wpSKU[i] = data['data'].filter(x => x.remain_qty > 0);
                    const select = this.wpSKU[i].find(x => x.sku == this.items[i].value['sku']);
                    if (select) {
                        this.selectedItemSKU(i, select);
                    } else {
                        this.resetSKU(i);
                        this.setErrorForControlItem(this.items, 'sku', i, { 'invalid': true });
                    }
                    this.items[i]['showLoadingSKU'] = false;
                },
                error => {
                    this.items[i]['showLoadingSKU'] = false;
                }
            );
        } else {
            this.resetSKU(i);
        }
    }

    public changeOrder(i, $evt) {
        this.setValueForControlItem(this.items, 'sku', i, '');
        this.resetSKU(i);
        this.wpSKU[i] = [];
    }

    public selectedItemSKU(row, data) {
        this.setValueForControlItem(this.items, 'sku', row, data['sku']);
        this.setValueForControlItem(this.items, 'color', row, data['color']);
        this.setValueForControlItem(this.items, 'pack_size', row, data['pack_size']);
        this.setValueForControlItem(this.items, 'remain_qty', row, data['remain_qty']);
        this.setValueForControlItem(this.items, 'size', row, data['size']);
        this.setValueForControlItem(this.items, 'item_id', row, data['item_id']);
        this.setValueForControlItem(this.items, 'lot', row, data['lot']);
        this.setValueForControlItem(this.items, 'description', row, data['description']);
        this.setValueForControlItem(this.items, 'wv_dtl_id', row, data['wv_dtl_id']);
        this.setValueForControlItem(this.items, 'old_picked_qty', row, data['picked_qty']);
    }

    public updateWarePick() {
        this.submitForm = true;
        if (this.PickingForm.valid) {
            const value = this.PickingForm.value;
            const data = {};
            data['wv_id'] = value.wv_id;
            data['order_id'] = value.items[0].odr_id;
            data['items'] = [];
            data['picked_loc_ids'] = [];
            value.items.forEach((element, index) => {
                const new_picked_qty = this.AddValuePickedQTY(value.items, element, index);
                if (new_picked_qty > element.remain_qty) {
                    this.setErrorForControlItem(this.items, 'new_picked_qty', index, {'invalid': true});
                }
                data['picked_loc_ids'].push(element.picked_loc_id);
                data['items'][index] = {item_id: element.item_id, picked_loc_id: element.picked_loc_id, picked_loc_name: element.picked_loc, picked_qty: Number(new_picked_qty)+Number(element.old_picked_qty), wv_dtl_id: element.wv_dtl_id };
            });
            if (this.PickingForm.valid) {
                this._pickingCompleteService.updateWarePick(this.wv_id, JSON.stringify(data)).subscribe(
                    data => {
                        this.messages = this._func.Messages('success', this._func.msg('CSRC02'));
                        this.showLoadingOverlay = false;
                        setTimeout(() => {
                            this._router.parent.navigateByUrl('/outbound/picking');
                        }, 1000);
                    },
                    err => {
                        this.showLoadingOverlay = false;
                        this.messages = this._func.Messages('danger', this._func.parseErrorMessageFromServer(err));
                    },
                );
            }
        }
    }

    AddValuePickedQTY(Items, item, i) {
        let new_picked_qty = Number(item.new_picked_qty);
        for (let index = 0; index < Items.length ; index ++) {
            if (i === index){
                return new_picked_qty;
            }
            if (Items[index].item_id=== item.item_id&&Items[index].odr_id === item.odr_id&&Items[index].picked_loc_id === item.picked_loc_id) {
                new_picked_qty += Number(Items[index].new_picked_qty);
                return new_picked_qty;
            }
        }
    }

    public resetSKU(row) {
        this.setValueForControlItem(this.items, 'color', row, '');
        this.setValueForControlItem(this.items, 'pack_size', row, '');
        this.setValueForControlItem(this.items, 'remain_qty', row, '');
        this.setValueForControlItem(this.items, 'size', row, '');
        this.setValueForControlItem(this.items, 'item_id', row, '');
        this.setValueForControlItem(this.items, 'lot', row, '');
        this.setValueForControlItem(this.items, 'description', row, '');
        this.setValueForControlItem(this.items, 'wv_dtl_id', row, '');
        this.setValueForControlItem(this.items, 'old_picked_qty', row, '');
        this.setValueForControlItem(this.items, 'new_picked_qty', row, '');
        this.setValueForControlItem(this.items, 'picked_loc_id', row, '');
        this.setValueForControlItem(this.items, 'picked_loc', row, '');
    }

    public checkWithRemainQTY(row, evt) {
        const picked_qty = evt.target.value;
        if (picked_qty && Number(picked_qty) !== 0) {
            if (Number(picked_qty) > Number(this.items[row].value['remain_qty'])) {
                this.setErrorForControlItem(this.items, 'new_picked_qty', row, { 'invalid': true });
            } else {
                this.setErrorForControlItem(this.items, 'new_picked_qty', row, null);
            }
        }
    }

    private checkInputNumber(evt) {
        this._Valid.isNumber(evt, true);
    }

    private cancel() {
        var that = this;
        this._boxPopupService.showWarningPopup()
          .then(function (ok) {
            if(ok)
              that._router.navigateByUrl('/outbound/picking');
          });
      }


}