
import {Component, Input, Injectable} from '@angular/core';
import {TableFunctions} from '../../../core/table-functions';
import {SearchesService} from './searches-service';
import {OrderBy} from '../../../pipes/order-by.pipe';

declare var jQuery: any;

@Component ({
    selector: 'searches',
    directives:[],
    providers: [
        SearchesService,
        TableFunctions
    ],
    pipes: [OrderBy],
    templateUrl: 'searches.html'
})

@Injectable()

export class Searches {

    @Input() searches: any [];
    @Input() parentComponent = <any> {};

    constructor(
        private _searchesService: SearchesService
    )
    {

    }

    //**************************************************************************

    public reset()
    {
        this._searchesService.reset(this.parentComponent);
    }

    //**************************************************************************

    public search()
    {
        this._searchesService.search({
            tableComponent: this.parentComponent
        });
    }

    //**************************************************************************

	public loadAutocomplete($event, search)
    {
        this._searchesService.autocomplete({
            tableComponent: this.parentComponent,
            autocomplete_field: search.name,
            autocomplete_value: $event
        });
	}

    //**************************************************************************

	public selectAutocomplete(text, search)
    {
        let value = text.trim();

        jQuery('.autocomplete-' + search.name ).val(value);
	}

    //**************************************************************************

}
