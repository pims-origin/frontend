import {ControlGroup,Control } from '@angular/common';
/*
  Custom validators to use everywhere.
*/
// SINGLE FIELD VALIDATORS
export function emailValidator(control: Control): {[key: string]: any} {
  var emailRegexp = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;
  if (control.value && !emailRegexp.test(control.value)) {
    return {invalidEmail: true};
  }
}

//CONTROL GROUP VALIDATORS
export function matchingPasswords(passwordKey: string, confirmPasswordKey: string) {
  return (group: ControlGroup): {[key: string]: any} => {
    let password = group.controls[passwordKey];
    let confirmPassword = group.controls[confirmPasswordKey];

    if (password.value !== confirmPassword.value) {
      return {
        mismatchedPasswords: true
      };
    }
  }
}



//CONTROL GROUP VALIDATORS BY HUY
export function numberCompare(number1: string, number2: string) {
  return (group: ControlGroup): {[key: string]: any} => {
    let numberA = group.controls[number1];
    let numberB = group.controls[number2];

    if (numberA.value > numberB.value) {
      return {
        compareResult: true
      };
    }
  }
}

/*
*
* This is zone master of Validation function
*
* */
export class ValidationService {

  static getValidatorErrorMessage(validatorName:string, validatorValue?:any) {
    let config = {
      'required': 'Required',
      'invalidCreditCard': 'Is invalid credit card number',
      'invalidEmailAddress': 'Invalid email address',
      'invalidPassword': 'Invalid password. Password must be at least 6 characters long, and contain a number.',
      'invalidInt': 'Input is interger.',
      'invalidNumber': 'Input is number.',
      'invalidSpace': 'Input not space.',
    };
    return config[validatorName];
  }

  // Validate number
  public validateInt(control:Control):{[key:string]:any} {

    var re = /^([0-9]*)$/;
    if (control.value && !re.test(control.value)) {
      return {'invalidInt': true};
    }
  }

  // Validate number
  public  validateNumber(control) {

    var re = /^([0-9,.]*)$/;

    if (!re.test(control.value)) {
      return {'invalidNumber': true};
    }

    return null;
  }

  public  inPutOnlyNumber(control:Control):{[key:string]:any} {

    var re = /^([0-9,.]*)$/;

    if (control.value && !re.test(control.value)) {
      return {'invalidNumber': true};
      ;
    }
    else {
      return null;
    }

  }

  /*
  * Using update form
  * */
  public  inPutOnlyNumberUpdate(control:Control):{[key:string]:any} {

    var re = /^([0-9,.]*)$/;

    if (control.value && !re.test(control.value)) {
      return {'invalidNumber': true};
      ;
    }
    else {
      return null;
    }

  }

  isNumber(evt,int=false) {
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)){
      if(int) {
        var flg = false;
        evt.preventDefault();
      }
      else {
        if(charCode != 46) {
          evt.preventDefault();
        }
      }
    }
    return flg;
  }

  checkInputFloat(evt) {
      var charCode = (evt.which) ? evt.which : evt.keyCode;
      if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 46){
          evt.preventDefault();
      }
  }

  public validateFloatKeyPress(value, evt) {
    const charCode = (evt.which) ? evt.which : evt.keyCode;
    const numOfDot = (value + event['key']).split('.');
    const numOfDecimal = numOfDot.length > 1 ? numOfDot[1] : 0;

    if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
      evt.preventDefault();
    } else if (numOfDot.length > 2 && charCode == 46) {
      evt.preventDefault();
    } else if (numOfDecimal !== 0 && numOfDecimal.length > 2) {
      evt.preventDefault();
    }
  }

  public isDecimalNubmerLength5_2(val) {
    const reg = /^\d{1,5}(\.\d{1,2})?$/;
    return val && reg.test(val);
  }

  public isDecimalNubmerLength15_2(val) {
    const reg = /^\d{1,13}(\.\d{1,2})?$/;
    return val && reg.test(val);
  }

  // Validate Phone Number
  public  validatePhone(control:Control):{[key:string]:any} {

    var re = /^\\+[0-9]{1,3}\\.[0-9]{4,14}(?:x.+)?$/;
    if (!re.test(control.value)) {
      control.updateValue('')
      return {'invalidPhone': true};
    }
    return null;

  }

  // Validate space
  public validateSpace(control:Control):{[key:string]:any} {
    let str = '' + control.value;
    if (control.value && !str.replace(/\s/g, '').length) {
      return {'invalidSpace': true};
    }
    return null;
  }

  // Validate space
  public validateLocCode(control:Control):{[key:string]:any} {
    let str = '' + control.value;
    var re = /^([0-9A-Za-z]{1,9}-[0-9A-Za-z]{1,9}-[0-9A-Za-z]{1,9})?(-[0-9A-Za-z]{1,9})?(-[0-9A-Za-z]{1,9})$/;
    if (control.value && !re.test(control.value)) {
      return {'invalidLocCode': true};
    }
    return null;
  }

  public emailValidator(control:Control):{[key:string]:any} {
    // RFC 2822 compliant regex
    if (control.value && control.value.match(/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)) {
      return null;
    } else {
      if (control.value != '') {
        return {'invalidEmailAddress': true};
      }
    }
  }

  public validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

  public warehouseCodeValidator(control:Control) {

      return control.value && !/^[a-z0-9]{1,3}$/i.test(control.value) ? {'invalidWarehouseCode': true} : null;

  }

  public checkWHSCodeLength(control:Control)
  {
    return control.value && control.value.length <3 ? {'invalidWarehouseCodeLength': true} : null;
  }

  public countryCodeValidator(control:Control) {

    return control.value && !/^[a-z0-9]{1,2}$/i.test(control.value) ? {'invalidCountryCode': true} : null;

  }

  public checkCountryCodeLength(control:Control)
  {
    return control.value && control.value.length <2 ? {'invalidCountryCodeLength': true} : null;
  }

  public isDecimalNumber(control:Control) {
    let value = control.value && !/^\d{1,5}(\.\d{1,7})?$/.test(control.value) ? {'invalidDecimalNumber': true} : null;
    return value
  }

  public isMaxDecimalNumber(control:Control) {
    let value = control.value && !/^\d{1,4}(\.\d{1,7})?$/.test(control.value) ? {'invalidMaxDecimalNumber': true} : null;
    return value
  }

  public isLaborCost(control:Control) {
    let value = control.value && !/^\d{1,6}(\.\d{1,9})?$/.test(control.value) ? {'invalidDecimalNumber': true} : null;
    return value
  }

  public isDecimalNumberMore(control:Control) {
    let value = control.value && !/^\d{1,9}(\.\d{1,10})?$/.test(control.value) ? {'invalidDecimalNumberMore': true} : null;
    return value
  }

  public isZero(control:Control) {
    let value = control.value;
    if (value == '' && value !== 0 || value == null) {
      return null;
    }
    else {
      return value <= 0 ? {'isZero': true} : null;
    }
  }

  public customRequired(control:Control):{[key:string]:any} {
    return control.value && control.value.replace(/ /g, '') ? null : {'required': true};
  }

  public validatePassword(control) {
    return control.value && !(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z!@#\$%\^\&*\)\(+=._-]{8,}$/).test(control.value) ? {'invalidPassword': true} : null;
  }

  toDay() {

    let today = new Date();
    let dd = '' + today.getDate();
    let mm = '' + (today.getMonth() + 1); //January is 0!
    let yyyy = today.getFullYear();
    if (parseInt(dd) < 10) {
      dd = '0' + dd
    }
    if (parseInt(mm) < 10) {
      mm = '0' + mm
    }
    return mm + '/' + dd + '/' + yyyy;
  }


  cancelByDate(shipbydate:string, cancelbydate:string, reqcmpldt:string, schdShipDt: string) {

    return (group:ControlGroup):{[key:string]:any} => {

      let currentDay = new Date(this.toDay());
      let ship_by_date = group.controls[shipbydate].value ? new Date(group.controls[shipbydate].value) : null;
      let cancel_by_date = group.controls[cancelbydate].value ? new Date(group.controls[cancelbydate].value) : null;
      let req_cmpl_dt = group.controls[reqcmpldt].value ? new Date(group.controls[reqcmpldt].value) : null;
      let schd_ship_dt = group.controls[schdShipDt].value ? new Date(group.controls[schdShipDt].value) : null;


      /*if (ship_by_date && ship_by_date < currentDay) {
        group.controls[shipbydate].setErrors({invalidDate: true});
      }
      else {
        console.log('set null shipbydate');
        group.controls[shipbydate].setErrors(null);
      }*/

      if (schd_ship_dt && schd_ship_dt < currentDay) {
        group.controls[schdShipDt].setErrors({invalidDate: true});
      } else {
        // console.log('set null schd_ship_dt');
        group.controls[schdShipDt].setErrors(null);
      }

      // check cancel_by_date
      if (cancel_by_date) {

        let err = 0;

        if (cancel_by_date < currentDay) {
          group.controls[cancelbydate].setErrors({invalidDate: true});
          err++;
        }
        if (ship_by_date>cancel_by_date) {
          group.controls[cancelbydate].setErrors({greater_ship_by_dt: true});
          err++;
        }

        if (!err) {
          group.controls[cancelbydate].setErrors(null);
        }
      }


      // check req_cmpl_dt
      if (req_cmpl_dt) {

        let err = 0;
        // if (req_cmpl_dt < currentDay) {
        //   group.controls[reqcmpldt].setErrors({invalidDate: true});
        //   err++;
        // }
        if (req_cmpl_dt > ship_by_date) {
          group.controls[reqcmpldt].setErrors({smaller_req_cmpl_dt: true});
          err++;
        }

        // if valid set null
        if (!err) {
          group.controls[reqcmpldt].setErrors(null);
        }
      }

      return null;
    }
  }

  validDate(fields: string[]) {
    return (group: ControlGroup): {[key: string]: any} => {

      const currentDay = new Date(this.toDay());

      fields.forEach(field => {
        const field_date = group.controls[field].value ? new Date(group.controls[field].value) : null;
        if (field_date && field_date < currentDay) {
          group.controls[field].setErrors({invalidDate: true});
        } else {
          console.log('set null for ', field);
          group.controls[field].setErrors(null);
        }
      });

      return null;
    }
  }

  /*
  * Check UPC CODE
  * */
  /*
   *  sum_even_number_digits
   * */
  sum_even_number_digits(number_string){

    let sumOdd=0;
    for(let i=0;i<number_string.length-1;i++){
      if(i%2==1){
        sumOdd=sumOdd+parseInt(number_string[i]);
      }
    }
    return sumOdd;

  }

  /*
   *  sum_odd_number_digits
   * */
  sum_odd_number_digits(number_string){

    let sumOdd=0;
    for(let i=0;i<number_string.length-1;i++){
      if(i%2==0){
        sumOdd=sumOdd+parseInt(number_string[i]);
      }
    }
    return sumOdd;

  }

  /*
   * checkUPCCode
   * */

  checkUPClength(control:Control)
  {
    return control.value && control.value.length!==12 ? {'invalid_UPC_length': true} : null;
  }

  checkUPCdigitCode(control:Control){
    let str_number=control.value;
    let lastDigitUPC;
    let that=new ValidationService();
    if(str_number&&str_number.length==12) {
        let evenSum = that.sum_even_number_digits(str_number);
        let oddSum = that.sum_odd_number_digits(str_number);
        const result = ((oddSum * 3) + evenSum) % 10;
        let digit_value = result !== 0 ? 10 - result : result;
        lastDigitUPC = str_number.charAt(str_number.length - 1);
        return parseInt(lastDigitUPC) !== digit_value ? {'invaild_UPC_code':true} : null;
    }
    return null;
  }

  public validatePasswordByPolicy(newpwd: any,oldpwd =''){
    let errMsgs = [];
    if(newpwd != undefined && newpwd != ''){
      if(newpwd.length < 8){
        errMsgs.push('User password must have at least 8 characters');
      }
      let obj = {
        isNum: false,
        isLowerCase: false,
        isUpperCase: false,
        isSpecialChar: false
      };
      let specialChars = "<>@!#$%^&*()_+[]{}?:;|'\"\\,./~`-=";

      for(let i = 0; i < newpwd.length; i++){
        let letter = newpwd[i];
        if(!isNaN(newpwd.charAt(i)*1)){
          obj['isNum'] = true;
        }else{
          if(letter == letter.toUpperCase()){
            obj['isUpperCase'] = true;
          }
          if(letter == letter.toLowerCase()){
            obj['isLowerCase'] = true;
          }
        }
      }

      if(!obj['isNum']){
        errMsgs.push('User password must contain at least one number');
      }
      if(!obj['isLowerCase']){
        errMsgs.push('User password must contain at least one lowercase letter');
      }
      if(!obj['isUpperCase']){
        errMsgs.push('User password must contain at least one capital letter');
      }

      for(let j = 0; j < specialChars.length; j++){
        if(newpwd.indexOf(specialChars[j]) > -1){
          obj['isSpecialChar'] = true;
        }
      }
      if(!obj['isSpecialChar']){
        errMsgs.push('User password must contain at least one special letter');
      }

      if(newpwd !== oldpwd){
        errMsgs.push('User confirm password does not match');
      }
    }else{
      errMsgs.push('New password cannot be empty');
    }
    /*if(errMsgs.length > 0){
      return false;
    }
    return true;*/
    return errMsgs;
  }

  public invalidLength2Or3(control:Control) {
    return control.value && !/^[a-zA-Z0-9]{2,3}$/i.test(control.value) ? {'invalidLength2Or3': true} : null;
  }

  public invalidLength3(control:Control) {
    return control.value && !/^[a-zA-Z0-9]{3}$/i.test(control.value) ? {'invalidLength3': true} : null;
  }

  public invalidNameStr(control:Control) {
    return control.value && !/^[a-zA-Z0-9 _-]*$/i.test(control.value) ? {'invalidNameStr': true} : null;
  }

  public invalidCodeStr(control:Control) {
    return control.value && !/^[a-zA-Z0-9]*$/i.test(control.value) ? {'invalidCodeStr': true} : null;
  }

  public invalidCommodityClass(control:Control) {
    return control.value && !/^(?!(000)).*[0-9]{3}$/i.test(control.value) ? {'invalidCommodityClass': true} : null;
  }

  public invalidCommodityNMFC(control:Control) {
    return control.value && !/^(?!(00000)).*[0-9]{5}$/i.test(control.value) ? {'invalidCommodityNMFC': true} : null;
  }

  // Validate space
  public validateEco(control:Control):{[key:string]:any} {
    let str = '' + control.value;
    if (str.toLowerCase().trim() == 'eco') {
      return {'invalidEco': true};
    }
    return null;
  }

  /** 
  * greaterThan: compare two number
  * @param maxCtrl: Control
  * @param minCtrl: Control
  * @param equal: boolean - if true then minCtrl == maxCtrl return true
  * @return boolean - rasie 'greaterThan' error for minCtrl control if false is returned
  */
  public greaterThan(maxCtrl: Control, minCtrl: Control, equal: boolean = false) {  
    if(maxCtrl.valid && minCtrl.valid && minCtrl.value != null && maxCtrl.value != null && minCtrl.value !== '' && maxCtrl.value !== '') {
      let min = parseFloat(minCtrl.value), max = parseFloat(maxCtrl.value);
      minCtrl.setErrors(null);
      
      if(min > max || (!equal && min == max)) {
        (<Control>minCtrl).setErrors({'greaterThan': true});
        return false;
      }
    }
    return true;
  }
}
