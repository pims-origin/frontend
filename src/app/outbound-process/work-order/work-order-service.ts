import {Component, Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {API_Config, Functions} from '../../common/core/load';

@Injectable()
export class WorkOrderServices {

  public AuthHeader = this._Func.AuthHeader();
  public AuthHeaderPost = this._Func.AuthHeaderPost();

  constructor(private _Func: Functions, private _API: API_Config, private http: Http) {

  }


  searchOrderNumber(params) {
    return this.http.get(this._API.API_ORDERS+params,{ headers: this.AuthHeader })
      .map((res: Response) => res.json());
  }


  getChargeCode(cus_id){
    return  this.http.get(this._API.API_WORK_ORDER +'/getChargeCode/'+cus_id , {headers: this.AuthHeader})
      .map(res => res.json());
  }

  getODinfo($params){
    return  this.http.get(this._API.API_WORK_ORDER +'/getOrderInfoSku/'+$params, {headers: this.AuthHeader})
      .map(res => res.json());
  }

  getCustomersByWH($params) {
    return  this.http.get(this._API.API_CUSTOMER_MASTER + '/customer-warehouses' + $params + '&sort[cus_name]=asc' , {headers: this.AuthHeader})
      .map(res => res.json());
  }

  createWorkOrder(data){
    return this.http.post(this._API.API_WORK_ORDER, data, { headers: this._Func.AuthHeaderPostJson() })
      .map((res: Response) => res.json().data);
  }

  updateWorkOrder(data){
    return this.http.put(this._API.API_WORK_ORDER, data, { headers: this._Func.AuthHeaderPostJson() })
      .map((res: Response) => res.json().data);
  }

  getWorkOrderList($param){
    return  this.http.get(this._API.API_WORK_ORDER+'/workOrderList'+$param, {headers: this.AuthHeader})
      .map(res => res.json());
  }

  getOrderList(params) {
    return this.http.get(this._API.API_ORDERS+ params,{ headers: this.AuthHeader })
        .map((res: Response) => res.json());
  }

  getWorkOrderDetail(wodr_id){
    return  this.http.get(this._API.API_WORK_ORDER+'/'+wodr_id, {headers: this.AuthHeader})
      .map(res => res.json().data);
   // return {"cus_id":"42262","ref_code":null,"odr_num":"","wo_ex_notes":"External Comments","wo_in_notes":"Internal Comments","type":0,"details":[{"item_id":"18481","chargecodes":[{"chg_code_id":1735,"chg_code":" REC-VOL","chg_code_des":null,"chg_uom_id":37567,"chg_type_id":3364,"created_at":{"date":"2016-07-09 09:06:25.000000","timezone_type":3,"timezone":"UTC"},"updated_at":{"date":"1970-01-01 00:00:00.000000","timezone_type":3,"timezone":"UTC"},"deleted_at":915148800,"wo_qty":"5","invaild_qty":false}],"duplicated":false,"value_selected":{"item_id":19140,"sku":"1","size":"1","color":"1","qty":"3"}},{"item_id":"18494","chargecodes":[{"chg_code_id":1735,"chg_code":" REC-VOL","chg_code_des":null,"chg_uom_id":37567,"chg_type_id":3364,"created_at":{"date":"2016-07-09 09:06:25.000000","timezone_type":3,"timezone":"UTC"},"updated_at":{"date":"1970-01-01 00:00:00.000000","timezone_type":3,"timezone":"UTC"},"deleted_at":915148800,"wo_qty":"4","invaild_qty":false}],"duplicated":false,"value_selected":{"item_id":19140,"sku":"1","size":"1","color":"1","qty":"3"}},{"item_id":"19142","chargecodes":[{"chg_code_id":1735,"chg_code":" REC-VOL","chg_code_des":null,"chg_uom_id":37567,"chg_type_id":3364,"created_at":{"date":"2016-07-09 09:06:25.000000","timezone_type":3,"timezone":"UTC"},"updated_at":{"date":"1970-01-01 00:00:00.000000","timezone_type":3,"timezone":"UTC"},"deleted_at":915148800,"wo_qty":"3","invaild_qty":false}],"duplicated":false,"value_selected":{"item_id":19140,"sku":"1","size":"1","color":"1","qty":"3"}}]};

  }

  changeStatusFinal(wodr_id){
    return this.http.put(this._API.API_WORK_ORDER + '/changestatus/' + wodr_id, '',{ headers: this._Func.AuthHeaderPostJson()})
        .map((res: Response) => res.json().data);
  }


}
