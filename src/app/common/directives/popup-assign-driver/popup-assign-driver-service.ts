import {Component, Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {Functions, API_Config} from '../../../common/core/load';

@Injectable()
export class PopupAssignDriverService {
    public AuthHeader = this._Func.AuthHeader();
    public headerPostJson = this._Func.AuthHeaderPostJson();

    constructor(
        private _Func: Functions, 
        private http: Http, 
        private _api: API_Config) {
    }
    
    public listUserRoleDriver() {
        return this.http.get(this._api.API_URL_ROOT + '/core/master-service/v1/roles/driver/users', { headers: this.AuthHeader })
            .map((res: Response) => res.json());
    }

    public saveDriverUser(data) {
        return this.http.post(this._api.API_URL_ROOT + '/core/orders/v1/drivers/assign', data, { headers: this.headerPostJson })
            .map((res: Response) =>  res.json());
    }
}