import {Component} from '@angular/core';
import { Router } from '@angular/router-deprecated';
import {API_Config, UserService, Functions} from '../../../common/core/load';
import {CountryService} from '../country-service';
import { FORM_DIRECTIVES,ControlGroup,FormBuilder, Control} from '@angular/common';
import { Http } from '@angular/http';
import {WMSBreadcrumb, WMSPagination} from '../../../common/directives/directives';
declare var jQuery: any;
@Component ({
    selector: 'country-list',
    providers: [CountryService,FormBuilder],
    directives: [FORM_DIRECTIVES,WMSBreadcrumb, WMSPagination],
    templateUrl: 'country-list.component.html',

})

export class CountryListComponent {
    private countryList: Array<any> = [];
    private selectedAll = false;
    public listSelectedItem = [];
    private Pagination;
    private totalCount = 0;
    private tmpPageCount;
    private pageCount=0;
    private ItemOnPage=0;
    private currentPage = 1;
    private perPage = 20;
    private numLinks = 3;
    private messages;
    searchForm: ControlGroup;
    public loading = [];
    public showLoadingOverlay = false;
    private hasEditPermission = false;
    private hasCreatePermission = false;
    private viewUser = false;
    private searchCriteria = {};
    constructor(
        private _http: Http,
        private _func: Functions,
        private _user: UserService,
        private fb: FormBuilder,
        private _countryService: CountryService,
        private _router: Router) {

        this.builderSearchForm();
        this.searchCriteria = this._func.genSearchCriteriaFromForm(this.searchForm);
        this.checkPermission()
    }

    // Check permission for user using this function page
    private checkPermission() {
        this.showLoadingOverlay = true;
        this._user.GetPermissionUser().subscribe(
            data => {
                this.hasEditPermission = this._user.RequestPermission(data, 'editCountry');
                this.hasCreatePermission = this._user.RequestPermission(data, 'createCountry');
                this.viewUser = this._user.RequestPermission(data,'viewCountry');
                /* Check orther permission if View allow */
                if(!this.viewUser) {
                    this._router.parent.navigateByUrl('/deny');
                }
                else {
                    this.getCountryList();
                }
            },
            err => {
                this.showMessage('danger', this._func.parseErrorMessageFromServer(err));
                this.showLoadingOverlay = false;
            }
        );
    }

    private getCountryList(page = null) {
        let param="?page="+page+"&limit="+this.perPage;
        if(Object.keys(this.searchCriteria).length !== 0) // check if searchCriteria Object not empty  ( {} )
        {
            param += '&sys_country_code=' + encodeURIComponent(this.searchCriteria['sys_country_code']) +
                        '&sys_country_name=' + encodeURIComponent(this.searchCriteria['sys_country_name']);
        }
        if(this.sortQuery != '') {
            param += '&'+this.sortQuery;
        }
        this.loading['getCountry'] = true;
        this.showLoadingOverlay = true;
        this._countryService.getCountry(param).subscribe(
            data => {
                this.selectedAll = false;
                this.listSelectedItem=[];
                this.countryList = data.data;
                //pagin function
                this.Pagination=data['meta']['pagination'];
                this.Pagination['numLinks']=3;
                this.Pagination['tmpPageCount']=this._func.Pagination(this.Pagination);

                this.loading['getCountry'] = false;
                this.showLoadingOverlay = false;
            },
            err => {},
            () => {}
        );
    }

    private checkSelectItem(item) {
        let id = item['sys_country_id'];
        item.selected = !item.selected;

        this.selectedAll=false;
        if (item.selected) {
            this.listSelectedItem.push(id);
            this.setItemListSelected(id, true);

            if(this.listSelectedItem.length==this.countryList.length)
            {
                this.selectedAll=true;
            }
        }
        else {
            let el = this.listSelectedItem.indexOf(id);
            // remove item out of array
            this.listSelectedItem.splice(el, 1);
            this.setItemListSelected(id, false);
        }
    }

    private setItemListSelected(id,value) {
        this.countryList.forEach((item) => {
            if(item['sys_country_id'] == id) {
                item['selected'] = value;
            }
        });
    }

    private checkAll(event) {
        this.selectedAll = !this.selectedAll;
        // set empty array  ListSelectedItem
        this.listSelectedItem=[];
        // Loop
        this.countryList.forEach((item) => {
            item['selected'] = this.selectedAll;
            if(this.selectedAll) {
                // if check all = true , pushing to ListSelectedItem
                this.listSelectedItem.push(item['sys_country_id']);
            }
        })
    }

    private editSelectedItem() {
        if (this.listSelectedItem.length > 1) {
            this.messages = this._func.Messages('danger',this._func.msg('CF004'));
        }
        else {
            if(this.listSelectedItem.length) {
                this._router.navigateByUrl('/master-data/country/' + this.listSelectedItem[0] + '/edit');
            }
            else {
                this.messages = this._func.Messages('danger',this._func.msg('CF005'));
            }
        }
    }

    private onPageSizeChanged($event) {
        this.perPage = $event.target.value;
        if((this.Pagination['current_page'] - 1) * this.perPage >= this.Pagination['total']) {
            this.currentPage = 1;
        }
        else {
            this.currentPage = this.Pagination['current_page'];
        }
        this.getCountryList(this.currentPage);
    }

    private filterList(num) {
        this.currentPage = num;
        this.getCountryList(num);
    }

    private getPage(num) {
        let arr=new Array(num);
        return arr;
    }

    private builderSearchForm() {
        this.searchForm =this.fb.group({
          'sys_country_code': [''],
          'sys_country_name': ['']
        });
    }

    search(data) {
        this.messages = null;
        let param="?page="+1+"&limit="+this.perPage;
        if(this.sortQuery != '') {
            param += '&'+this.sortQuery;
        }
        this.searchCriteria = this._func.genSearchCriteriaFromForm(this.searchForm);
        let encodeSearchQuery = param + "&sys_country_code="+encodeURIComponent(this.searchCriteria['sys_country_code'])+
                                "&sys_country_name="+encodeURIComponent(this.searchCriteria['sys_country_name']);

        this.loading['searching']=true;
        this.showLoadingOverlay = true;
        this._countryService.search(encodeSearchQuery).subscribe(
          data => {
            // disable loading icon
            this.loading['searching']=false;
            this.selectedAll = false;
            this.listSelectedItem=[];

            this.countryList = data.data;
            //pagin function
            this.Pagination=data['meta']['pagination'];
            this.Pagination['numLinks']=3;
            this.Pagination['tmpPageCount']=this._func.Pagination(this.Pagination);
            this.showLoadingOverlay = false;
          },
          err =>{
            this.messages = this._func.Messages('danger','Not found');
          },
          () => {}
        );
    }

    resetSearch(data) {
        // if(data.sys_country_code || data.sys_country_name) {
            (<Control>this.searchForm.controls["sys_country_code"]).updateValue("");
            (<Control>this.searchForm.controls["sys_country_name"]).updateValue("");
            this.searchCriteria = this._func.genSearchCriteriaFromForm(this.searchForm);
            this.getCountryList(this.Pagination['current_page']);
        // }
    }
    private showMessage(msgStatus, msg) {
        this.messages = {
            'status': msgStatus,
            'txt': msg
        }
        jQuery(window).scrollTop(0);
    }

    private FieldsToSortFlag = {
        sys_country_code: false,
        sys_country_name: false,
    };
    private countClick = {
        sys_country_code: 3,
        sys_country_name: 3,
    }
    // private sortQuery = '';
    private sortQuery = 'sort[sys_country_code]=asc';
    sort(field) {
        if(this.countClick[field] == 3) {
            this.countClick[field] = 0;
        }

        this.countClick[field] ++;
        if(this.countClick[field] == 3) {
            this.sortQuery = 'sort[sys_country_code]=asc';
        }
        else {
            this.FieldsToSortFlag[field] = !this.FieldsToSortFlag[field];

            this.sortQuery = 'sort['+field+']';
            if(this.FieldsToSortFlag[field]) {
                this.sortQuery += '=asc';
            } else {
                this.sortQuery += '=desc';
            }
        }

        if(field) {
            for (let sortField in this.FieldsToSortFlag ) {
                if(sortField != field) {
                    this.FieldsToSortFlag[sortField] = false;
                    this.countClick[sortField] = 3;
                }
            }
        }
        this.getCountryList(this.currentPage);
    }
}
