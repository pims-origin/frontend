import { Component } from "@angular/core";
import { Router, ROUTER_DIRECTIVES } from '@angular/router-deprecated';
import { ControlGroup, FormBuilder, Control, Validators } from '@angular/common';
import { API_Config, UserService, Functions, FormBuilderFunctions } from '../../common/core/load';
import { ValidationService } from '../../common/core/validator';
import { WMSMessages, WMSBreadcrumb, WMSDatepicker  } from '../../common/directives/directives';
import { DailyInventoryService } from "./daily-inventory-report-service";

declare var saveAs: any;
declare const jQuery:any;
declare const $:any;
var moment = require('moment');

@Component({
    selector: 'daily-inventory-report',
    providers: [DailyInventoryService],
    directives: [ROUTER_DIRECTIVES, WMSBreadcrumb, WMSDatepicker ],
    templateUrl: 'daily-inventory-report.component.html',

})
export class DailyInventoryReportComponent {

    private whs_id;
    private messages;
    exportForm:ControlGroup;
    private listActivity:Array<any> = [];
    private showLoadingOverlay:boolean = false;
    private customers:Array<any> = [];
    private showError= false;

    // Check permission for user using this function page
    private canAccess:boolean = false;
    private canReset:boolean = false;

    // Export csv file
    private fileName:string = 'Daily-Inventory-Report';

	public monthList = [];
	public years = []; 
	curYear = new Date().getFullYear();
    curMonth = new Date().getMonth()+1;
    public minYear = 2018;
    public minMonth = 4;

    constructor(private formBuilder:FormBuilder,
                private eventService:DailyInventoryService,
                private _Func:Functions,
                private _fbFunc:FormBuilderFunctions,
                private _Valid: ValidationService,
                private _user:UserService,
                private _router:Router,
                private _API:API_Config) {
        this.whs_id = localStorage.getItem('whs_id');
        this.buildExportForm();
        this.checkPermission();
        this._buildYear();
        this._buildMonth();

    }

    private current_date:any;
    buildExportForm() {
        this.current_date = moment().format('YYYY-MM-DD');
        this.exportForm = this.formBuilder.group({
            cus_id: ['', Validators.compose([Validators.required,this._Valid.validateInt])],
            date: [this.current_date],
            reset: [true],
            month: [this.curMonth.toString(), Validators.required],
            year: [this.curYear, Validators.required],
        });
    }

    private min_date:any;
    private chanegCus(cus_id:any){
        this.min_date = this.getCusObjectById(cus_id);
    }

    private getCusObjectById(id:any){

        for(let cus of this.customers){
            if(cus['cus_id']==id){
                return cus['inv_date'];
            }
        }
        return this.current_date;

    }

    private checkPermission() {
        this.showLoadingOverlay = true;
        this._user.GetPermissionUser().subscribe(
            data => {
                this.showLoadingOverlay = false;
                this.canAccess = this._user.RequestPermission(data, 'viewInventoryReport');
                this.canReset = this._user.RequestPermission(data, 'resetDataInventoryReport');
                if (this.canAccess) {
                    this.getCustomers();
                }
                else {
                    this._router.parent.navigateByUrl('/deny');
                }
            },
            err => {
                this.messages = this._Func.Messages('danger', this._Func.parseErrorMessageFromServer(err));
                this.showLoadingOverlay = false;
            }
        );
    }

    getCustomers() {
        const params = `/${this.whs_id}?limit=100`;

        this.eventService.getCustomersByWH(params).subscribe(
            data => {
                this.customers = data.data;
            },
            err => {
                this.parseError(err);
            },
            () => {}
        );
    }

    export() {
        if(this.exportForm.valid) {
            this._exportInventory()
        } else {
            this.showError = true;
        }
    }

    private _exportInventory() {
        const cus_id = this.exportForm.value['cus_id'];
        const month = this.exportForm.value['month'].length > 1? this.exportForm.value['month']: `0${this.exportForm.value['month']}`;
        const date = this.exportForm.value['year'] + '-' + month  + '-01';
        let api = this._API.API_REPORTS_CORE  + '/inventory/daily_report/' +  this.whs_id + '/' + cus_id +'?select_day='+date;
        
        api += this.exportForm.value['reset'] ? '&reset=true' : '';
        this.fileName = 'Monthly-Inventory-Report';
        this.printToExcel(api);
    }

    private _exportPallet() {
        let cus_id = this.exportForm.value['cus_id'];
        let api = this._API.API_REPORTS_CORE  + '/pallet/customers/' +  this.whs_id + '/' + cus_id +'?select_day='+this.exportForm.value['date'];
        this.fileName = 'Daily-Pallet-Report';
        this.printToExcel(api);
    }

    private printToExcel(api) {
        let that = this;
        this.showLoadingOverlay = true;
        try{
            let xhr = new XMLHttpRequest();
            xhr.open("GET", api , true);
            xhr.setRequestHeader("Authorization", 'Bearer ' + that._Func.getToken());
            xhr.responseType = 'blob';

            xhr.onreadystatechange = function () {
                if(xhr.readyState == 2) {
                    if(xhr.status == 200) {
                        xhr.responseType = "blob";
                    } else {
                        xhr.responseType = "text";
                    }
                }

                if(xhr.readyState === 4) {
                    if(xhr.status === 200) {
                        //update printed label value
                        var today = new Date();
                        var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
                            "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
                        ];
                        var day = today.getDate();
                        var month = monthNames[today.getMonth()];
                        var year = today.getFullYear().toString().substr(2,2);
                        var fileName = that.fileName + '_' + month + day + year;
                        that.showLoadingOverlay = false;
                        var blob = new Blob([this.response], {type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'});
                        let fileExt = '.xls';
                        saveAs.saveAs(blob, fileName + fileExt);
                    } else {
                        let errMsg = '';
                        if(xhr.response) {
                            try {
                                var res = JSON.parse(xhr.response);
                                errMsg = res.errors && res.errors.message ? res.errors.message : xhr.statusText;
                            }
                            catch(err){errMsg = xhr.statusText}
                        }
                        else {
                            errMsg = xhr.statusText;
                        }
                        if(!errMsg) {
                            errMsg = that._Func.msg('VR100');
                        }
                        that.showMessage('danger', errMsg);
                    }
                }
            }
            xhr.send();
        } catch (err) {
            that.showMessage('danger', that._Func.msg('VR100'));
        }

    }

    private showMessage(msgStatus, msg) {
        this.messages = {
            'status': msgStatus,
            'txt': msg
        }
        jQuery(window).scrollTop(0);
    }

    private parseError(err) {
        err = err.json();
        this.messages = this._Func.Messages('danger', err.errors.message);
    }

    private updateMessages(messages) {
        this.messages = messages;
    }

    private _buildYear() {
		for (let i = this.curYear; i >= this.minYear; i--) {
			this.years.push(i);
        }
    }

    private _buildMonth(year = null, first = true) {
        this.monthList = [];
        let min = 1;
        let max = 12;
		if (!year) {
            if (first) {
                max = this.curMonth;
                if (this.curYear == this.minYear) {
                    min = this.minMonth;
                }
            }
			for (let i = min; i <= max; i++) {
				this.monthList.push(i);
			}
		} else {
            if (year == this.minYear) {
                min = this.minMonth;
            }
            if (year == this.curYear) {
                max = this.curMonth;
            }
            for (let i = min; i <= max; i++) {
                this.monthList.push(i);
            }

            let existedSelectMonth = false;
			this.monthList.forEach(item => {
				if (this.exportForm.value['month'] == item) {
					existedSelectMonth = true;
				}
			});
			if (!existedSelectMonth) {
                this._setField('month', '');
			}

		}
        
    }
    
    changeYear(evt) {
		const year = evt.target.value;
		this._buildMonth(year, false);
    }
    
    changeMonth($event: any, period: string) {
		const val = $event.target.value;
		if (val !== '') {
			this._setField(period, val);
		}
    }
    
    
	_setField(field: string, val: any) {
		this._fbFunc.setValueControl(this.exportForm, field, val);
	}
}
