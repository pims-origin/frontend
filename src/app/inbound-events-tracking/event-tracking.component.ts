import {Component,OnInit} from "@angular/core";
import { Router, RouterLink, ROUTER_DIRECTIVES } from '@angular/router-deprecated';
import { FORM_DIRECTIVES,ControlGroup,FormBuilder, Control,Validators} from '@angular/common';
import {API_Config, UserService, Functions} from '../common/core/load';
import { Http } from '@angular/http';
import {WarningBoxPopup} from "../common/popup/warning-box-popup";
import {BoxPopupService} from "../common/popup/box-popup.service";
import {EventTrackingService} from './event-tracking-service';
import { ValidationService } from '../common/core/validator';
import {WMSPagination,AgGridExtent,WMSMessages} from '../common/directives/directives';
import 'ag-grid-enterprise/main';
import {FormBuilderFunctions} from '../common/core/formbuilder.functions';


@Component({
  selector: 'event-tracking',
  providers:[EventTrackingService,ValidationService,BoxPopupService],
  directives: [ROUTER_DIRECTIVES,AgGridExtent,WMSMessages,WMSPagination],
  templateUrl: 'event-tracking.component.html',

})
export class EventTrackingComponent{

  private headerURL = this.apiService.API_User_Metas+'/evt';
  private columnData = {
    owner: {
      title:'Owner',
      width:120,
      pin:true,
      ver_table:'08/14/20164534',
      sort:true
    },
    trans_num: {
      title:'Transaction Number',
      width:150,
      pin:true,
      sort:true
    },
    evt_code:{
      title:'Event',
      width:80,
      pin:true,
      sort:true
    },
    info: {
      title:'Info',
      width:220,
      sort:true
    },
    created_at:{
      title:'Date Time',
      width:120,
      sort:true
    },
    user: {
      title:'User',
      width:150,
      sort:true
    }
  };

  private Pagination;
  private perPage=20;
  private whs_id;
  private messages;
  private Loading={};
  private searching={};
  searchForm:ControlGroup;
  private isSearch:boolean=false;
  private listEventTracking:Array<any>=[];
  private sortData={fieldname:'created_at',sort:'desc'};
  private showLoadingOverlay:boolean=false;
  private indicatoList:Array<any>=[];
  private listOwner:Array<any>=[];

  constructor(
    private eventService:EventTrackingService,
    private _Func: Functions,
    private _boxPopupService: BoxPopupService,
    private  apiService:API_Config,
    private _user:UserService,
    private fbdFunc:FormBuilderFunctions,
    private fb: FormBuilder,
    private _http: Http,
    private _Valid: ValidationService,
    private _router: Router) {

    this.checkPermission();

    this.buildForm();
    this.whs_id = localStorage.getItem('whs_id');


  }

  // Check permission for user using this function page
  private inboundEventTracking;
  private allowAccess:boolean=false;
  private checkPermission() {
    this.showLoadingOverlay = true;
    this._user.GetPermissionUser().subscribe(
      data => {
        this.showLoadingOverlay = false;
        this.inboundEventTracking = this._user.RequestPermission(data, 'inboundEventTracking');
        if(this.inboundEventTracking){
          this.allowAccess=true;
          // add for show "No rows to show"
          //called after the constructor and called  after the first ngOnChanges()
          this.getListEventTracking();
          this._indicatoList();
        }
        else{
          this.redirectDeny();
        }
      },
      err => {
        this.messages=this._Func.Messages('danger', this._Func.parseErrorMessageFromServer(err));
        this.showLoadingOverlay = false;
      }
    );
  }

  redirectDeny(){
    this._router.parent.navigateByUrl('/deny');
  }

  private buildForm(){
    this.searchForm =this.fb.group({
      indicator:[''],
      owner: [''],
      trans_num: [''],
      cus: [''],
      from:[''],
      to:[''],
      container: [''],
    });

    setTimeout(()=>{
      this.fbdFunc.handelDropdownDatePicker(this.searchForm);
    },1000);

  }

  private onPageSizeChanged($event)
  {
    this.perPage = $event.target.value;
    if(this.Pagination){
      if((this.Pagination['current_page'] - 1) * this.perPage >= this.Pagination['total']) {
        this.Pagination['current_page'] = 1;
      }
      this.getListEventTracking(this.Pagination['current_page']);
    }

  }

  private _indicatoList(){

    let param="?limit=9999";
    this.eventService.indicatoList(param).subscribe(
        data => {
          this.indicatoList=data.data;
        },
        err => {
          this.parseError(err);
        }
    );

  }

  private searchParam;
  Search()
  {
    this.isSearch=true;
    let data=this.searchForm.value;
    this.searchParam="&owner="+data['owner']+"&trans_num="+encodeURIComponent(data['trans_num']);
    this.searchParam+="&indicator="+data['indicator']+"&created_at_from="+data['from']+"&created_at_to="+data['to'];
    this.getListEventTracking();
  }

  private getListEventTracking(page=null){

    let param="?sort["+this.sortData['fieldname']+"]="+this.sortData['sort']+"&page="+page+"&limit="+this.perPage;
    if(this.isSearch){
      param=param+this.searchParam;
    }
    this.showLoadingOverlay=true;

    this.eventService.getListEventTracking(param).subscribe(
      data => {
        this.showLoadingOverlay=false;
        this.listEventTracking = this._Func.formatData(data.data);
        //pagin function
        this.Pagination=data['meta']['pagination'];
        this.Pagination['numLinks']=3;
        this.Pagination['tmpPageCount']=this._Func.Pagination(this.Pagination);
      },
      err => {
        this.parseError(err);
        this.showLoadingOverlay=false;
      },
      () => {}
    );

  }

  autoComplete(key:any) {


    let param = "?owner="+key+"&limit=20";
    let enCodeSearchQuery = this._Func.FixEncodeURI(param);
    this.Loading['owner']=false;

    if (this.searchForm['subscribe']) {
      this.searchForm['subscribe'].unsubscribe();
    }

    if(key){
      // do Search
      this.Loading['owner']=true;
      this.searchForm['subscribe'] = this.eventService.searchOwner(enCodeSearchQuery).debounceTime(400).distinctUntilChanged().subscribe(
          data => {
            // disable loading icon
            this.Loading['owner']=false;
            this.listOwner = data.data;
          },
          err => {
            this.Loading['owner']=false;
            this.parseError(err);
          },
          () => {
          }
      );
    }


  }

  selectedItem(item){

    this.fbdFunc.setValueControl(this.searchForm,'owner',item['owner']);

  }


  /*
   * Reset Form Builder
   * */
  ResetSearch()
  {
    this.isSearch=false; // remove action search
    this._Func.ResetForm(this.searchForm);
    this.getListEventTracking(this.Pagination['current_page']);
  }

  // Show error when server die or else
  private parseError (err) {
    err = err.json();
    this.messages = this._Func.Messages('danger', err.errors.message);
  }

}
