import {Component, AfterViewInit } from '@angular/core';
import { FORM_DIRECTIVES,ControlGroup,FormBuilder, Control ,ControlArray,Validators} from '@angular/common';
import {RouteParams,Router ,RouteData} from '@angular/router-deprecated';
import {UserService, Functions} from '../../../common/core/load';
import {CancelOrderServices} from './cancel-order-detail-service';
import { ValidationService } from '../../../common/core/validator';
import {WMSBreadcrumb,PaginatePipe,WMSMessages, PaginationControlsCmp, PaginationService} from '../../../common/directives/directives';
import { Http } from '@angular/http';
import {GoBackComponent} from "../../../common/component/goBack.component";
import {BoxPopupService} from "../../../common/popup/box-popup.service";
import {WarningBoxPopup} from "../../../common/popup/warning-box-popup";
import {BackOrder, OrderDetail, Packing, StagingPallet, Wave, WordOrder} from "./cancel-order-info";
declare var jQuery: any;
@Component({
  selector: 'cancel-order-detail',
  providers: [ CancelOrderServices],
  directives: [FORM_DIRECTIVES,WMSMessages,GoBackComponent,WMSBreadcrumb],
  pipes:[],
  templateUrl: 'cancel-order-detail.component.html',
})

export class CancelOrderDetailComponent implements AfterViewInit {
  showError = false;
  isCanceled = false;
  canCanceledRMA = false;
  private messages;
  private showLoadingOverlay = false;
  odrDetail = {};
  backOdrList = [];
  packing = {list:[], totalSKU:0};
  stagingPallet = {list:[], totalPallet:0};
  wavePick = {list:[], totalSKU:0};
  wordOdr = {list:[], totalSKU:0};
  private allowAccess=false;
  orderId;
  data;
  public whs_id: string;
  act_cancel_dt: string = '';
  constructor(
    private _http: Http,
    private _user:UserService,
    private _Func: Functions,
    private params: RouteParams,
    private _router: Router,
    private _cancelOrderService: CancelOrderServices) {
    this.whs_id = this._Func.lstGetItem('whs_id');
    this.checkPermission();

  }

  ngAfterViewInit () {
    const that = this;
    setTimeout(() => {
      jQuery('#act_cancel_dt').datepicker().on('changeDate', function(e) {
          that.act_cancel_dt = e.currentTarget.value;
          console.log(that.act_cancel_dt);
          that.showError = that.act_cancel_dt === '';
      });
    }, 1000);
  }

  private viewOrder=false;
  private checkPermission() {
    this.showLoadingOverlay = true;
    this._user.GetPermissionUser().subscribe(
      data => {
        this.showLoadingOverlay = false;
        this.viewOrder = this._user.RequestPermission(data, 'viewOrder');
        if(this.viewOrder){
          this.allowAccess=true;
          this.getOrderDetail();
        }
        else{
          this.redirectDeny();
        }
      },
      err => {
        this.messages=this._Func.Messages('danger', this._Func.parseErrorMessageFromServer(err));
        this.showLoadingOverlay = false;
      }
    );
  }
  redirectDeny(){
    this._router.parent.navigateByUrl('/deny');
  }

  getOrderDetail() {
    this.showLoadingOverlay = true;
    this.orderId = this.params.get('id');
    this._cancelOrderService.getOrderDetail(this.orderId)
        .subscribe(
            result => {
              this.data = result.data;
              let ordSts = this.data['odr_sts'];
              this.isCanceled = ordSts == 'CC'? true:false;
              this.canCanceledRMA = ['NW', 'AL', 'PK', 'CC', 'SH', 'RMA'].indexOf(ordSts) === -1? true:false;

              this.odrDetail
              this.backOdrList = this.data['backOrderList'];

              this.packing.list = this.data['packList'];
              this.packing.totalSKU = this.data['packList_sku_ttl']//packList_sku_ttl

              this.stagingPallet.list = this.data['palletList'];
              this.stagingPallet.totalPallet = this.data['pallet_ttl']; //pallet_ttl

              this.wavePick.list = this.data['wavePickList'];
              this.wavePick.totalSKU = this.data['wavPick_sku_ttl'];

              this.wordOdr.list = this.data['workOrderList'];
              this.wordOdr.totalSKU = this.data['workOrderList_sku_ttl']; //workOrderList_sku_ttl

              this.showLoadingOverlay = false;
              const today = this._Func.getToday();
              this.act_cancel_dt = this.data['act_cancel_dt'] ? this.data['act_cancel_dt'] : (this.isCanceled ? '' : today);
            },
            err => {
              this.showMessage('danger', this._Func.parseErrorMessageFromServer(err));
              this.showLoadingOverlay = false;
            }
        );
  }

  private showMessage(msgStatus, msg) {
    this.messages = {
      'status': msgStatus,
      'txt': msg
    }
    jQuery(window).scrollTop(0);
  }

  private cancelOrder() {
    if (this.act_cancel_dt !== '') {
      this.showLoadingOverlay = true;
      this.showError = false;
      let that = this;
      this._cancelOrderService.cancelOrder(this.orderId, localStorage.getItem('whs_id'), this.act_cancel_dt)
          .subscribe(
              data => {
                  this.showLoadingOverlay = false;
                  this.showMessage('success', data.data);
                  setTimeout( function(){
                      that._router.parent.navigateByUrl('/outbound/cancel-order');
                  }, 600);
                  // this.messages = this._Func.Messages('success', this._Func.msg('VR107'));
              },
              err => {
                  this.showLoadingOverlay = false;
                  this.showMessage('danger', this._Func.parseErrorMessageFromServer(err));
              }
          )
    } else {
       this.showError = true;
    }

  }

  private cancelRMA() {
    if (this.act_cancel_dt !== '') {
      this.showLoadingOverlay = true;
      this.showError = false;
      let that = this;
      this._cancelOrderService.cancelRMA(this.orderId, localStorage.getItem('whs_id'), this.act_cancel_dt)
          .subscribe(
              data => {
                  this.showLoadingOverlay = false;
                  this.showMessage('success', data.data);
                  setTimeout( function(){
                      that._router.parent.navigateByUrl('/outbound/cancel-order');
                  }, 600);
                  // this.messages = this._Func.Messages('success', this._Func.msg('VR107'));
              },
              err => {
                  this.showLoadingOverlay = false;
                  this.showMessage('danger', this._Func.parseErrorMessageFromServer(err));
              }
          )
    } else {
       this.showError = true;
    }

  }

  checkDate($event) {
    this.showError = $event.target.value === '';
  }

}




