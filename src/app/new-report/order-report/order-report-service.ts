import {Component, Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {API_Config, Functions} from '../../common/core/load';

@Injectable()
export class OrderReportService {

  public AuthHeader = this._Func.AuthHeader();
  public AuthHeaderPost = this._Func.AuthHeaderPost();

  constructor(private _Func: Functions, private _API: API_Config, private http: Http)
  {
  }

  getListActivity(whi, params)
  {
    return this.http.get(this._API.API_Warehouse+'/'+whi+'/report/order'+params,{ headers: this.AuthHeader })
        .map((res: Response) => res.json());
  }

  getCustomersByWH($params) {
    return  this.http.get(this._API.API_CUSTOMER_MASTER + '/customer-warehouses' + $params + '&sort[cus_name]=asc' , {headers: this.AuthHeader})
        .map(res => res.json());
  }

  // Get customers by WH
  public getUserList ($params) {

    return  this.http.get(this._API.API_Users +  $params, {headers: this.AuthHeader})
      .map(res => res.json());

  }

  public getStatusList () {

    return  this.http.get(this._API.API_Warehouse+'/report/order-status-not-shipping', {headers: this.AuthHeader})
      .map(res => res.json());

  }

}
