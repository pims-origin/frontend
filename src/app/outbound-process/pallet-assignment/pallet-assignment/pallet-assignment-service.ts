import {Component, Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {API_Config, Functions} from '../../../common/core/load';

@Injectable()
export class PalletAssignmentServices {

    public AuthHeader = this._Func.AuthHeader();
    public AuthHeaderPost = this._Func.AuthHeaderPost();

    constructor(private _Func: Functions,
                private _http: Http,
                private _API: API_Config) {

    }

    getOrderCarton(order_ids) {
        return this._http.get(this._API.API_Pack + '/show-packs-assign-pallets?odr_hdr_ids=' + order_ids,{ headers: this.AuthHeader })
            .map((res: Response) => res.json());
    }

    assignPallet(stringData) {
        return this._http.put(this._API.API_Pack + '/packs-assign-pallets/0', stringData, { headers: this._Func.AuthHeaderPostJson() })
            .map((res: Response) => res.json());
    }
}
