import {Component} from '@angular/core';
import { FORM_DIRECTIVES,ControlGroup,FormBuilder, Control ,Validators} from '@angular/common';
import {API_Config, Functions} from '../../../common/core/load';
import {RouteParams,Router,RouteData } from '@angular/router-deprecated';
import {  ValidationService } from '../../../common/core/validator';
import { WarehouseServices } from '../warehouse-service';
import { WarehouseServicesMaster } from '../../../common/services/warehouse.service.master';
import {BoxPopupService} from "../../../common/popup/box-popup.service";
import {ErrorMessageForControlService} from "../../../common/services/error-message-for-control.service";
import { OrderBy } from "../../../common/pipes/order-by.pipe";
import {WMSBreadcrumb,WMSPagination,WMSMessages} from '../../../common/directives/directives';
import {GoBackComponent} from "../../../common/component/goBack.component";
import {UserService} from "../../../common/users/users.service";
declare var jQuery: any;
@Component ({
  selector: 'edit-warehouse',
  directives: [FORM_DIRECTIVES,GoBackComponent,WMSBreadcrumb,WMSMessages,WMSPagination],
  pipes: [OrderBy],
  providers: [ValidationService,WarehouseServicesMaster,FormBuilder, WarehouseServices, BoxPopupService, ErrorMessageForControlService],
  templateUrl: 'cru-warehouse.component.html',
})

export class CruWarehouseComponent {

  private WarehouseData:Array<any>=[];
  WareHouseForm: ControlGroup;
  private TitlePage='View Customer';
  public errMessages=[];
  public messages;
  private Loading=[];
  private WareHouseDetail=[];
  private submitForm:boolean=false;
  private  warehouse_id;
  private Country=[];
  private StateProvinces=[];
  private Status = [];
  private IsView:boolean=false;
  private Action;
  private  ListData=[];
  private ListActive:string='GetListData';
  private showLoadingOverlay:boolean=false;

  // Pagin
  private Pagination = [];
  private perPage=20;
  private showPagin=true;
  private TabListName;
  private allowAccess:boolean=false;
  // configuration
  configurationForm: ControlGroup;
  configInfo: any;
  timezones: string[] = [];

  constructor(
      private _Func: Functions,
      private _WareHouseService:WarehouseServices,
      private params: RouteParams,
      private _WHSM:WarehouseServicesMaster,
      private _router:Router,
      private _user: UserService,
      private _Valid: ValidationService,
      private _boxPopupService: BoxPopupService,
      _RTAction: RouteData,
      private fb: FormBuilder) {

    // get country
    this.GetCountry();
    this.getWarehouseStatus();
    this.Action=_RTAction.get('action');
    this.checkPermission();

  }

  private createWarehouse;
  private editWarehouse;
  private viewWarehouse;
  private checkPermission() {
    this.showLoadingOverlay = true;
    this._user.GetPermissionUser().subscribe(
      data => {
        this.createWarehouse = this._user.RequestPermission(data, 'createWarehouse');
        this.editWarehouse = this._user.RequestPermission(data,'editWarehouse');
        this.viewWarehouse=this._user.RequestPermission(data,'viewWarehouse');

          // pass
          this.showLoadingOverlay = false;

          if(this.Action=='view')
          {
            if(!this.viewWarehouse){
              this.redirectDeny();
            }
            else{
              this.allowAccess=true;
            }
            this.IsView=true;
            this.TitlePage='View Warehouse';

          }

          if(this.Action=='edit')
          {
            if(!this.editWarehouse){
              this.redirectDeny();
            }
            else{
              this.allowAccess=true;
            }
            this.TitlePage='Edit Warehouse';
          }

          if(this.Action=='new')
          {
            if(!this.createWarehouse){
              this.redirectDeny();
            }
            else{
              this.allowAccess=true;
            }
            this.TitlePage='Add Warehouse';
            this.formBuilder();
          }

          if(this.Action=='edit'||this.Action=='view')
          {
            // edit
            // get detail
            this.warehouse_id=this.params.get('id');
            this.getConfiguration();
            if(this.IsView){
              // only load on view page
              this.GetListData(1,'GetZone');
            }
            this.showLoadingOverlay=true;
            this._WareHouseService.GetDetailWareHouse(this.warehouse_id).subscribe(
              data => {
                this.WareHouseDetail=data;
                this.showLoadingOverlay = false;
                this.GetState(this.WareHouseDetail['whs_country_id']);
                this.FormBuilderViewEdit();
              },
              err => {
                this.showLoadingOverlay = false;
                this.messages = this._Func.parseErrorMessageFromServer(err);
              },
              () => {}
            );
          }

          // end pass
      },
      err => {
        this.messages=this._Func.Messages('danger', this._Func.parseErrorMessageFromServer(err));
        this.showLoadingOverlay = false;
      }
    );
  }
  redirectDeny(){
    this._router.parent.navigateByUrl('/deny');
  }
  /*
   * Load state of country when change
   * */
  ChangeCountry(item)
  {
    if(item)
    {
      this.StateProvinces=[];
      this._Func.ResetState(this.WareHouseForm,'whs_state_id');
      this._Func.ResetState(this.WareHouseForm,'whs_add_state_id');
      this.GetState(item);
    }
    else{
      this.StateProvinces=[];
      this._Func.ResetState(this.WareHouseForm,'whs_state_id');
      this._Func.ResetState(this.WareHouseForm,'whs_add_state_id');
    }

  }

  getWarehouseStatus() {
    this._WHSM.Status().subscribe(
        data=>{
          this.Status=data.data;
        },
        err=>{
          this.errMessages.push('Could not load country.');
        }
    );
  }

  /*
   * Get Country
   * */
  GetCountry()
  {
    let params='?limit=1000&sort[sys_country_name]=asc';
    this._WareHouseService.Country(params).subscribe(
      data=>{
        this.Country=data.data;
      },
      err=>{
        this.errMessages.push('Could not load country.');
      },
      ()=> {}
    );
  }

  GetState(idCountry)
  {
    this.Loading['state']=true;
    let params='states?sort[sys_state_name]=asc';
    this._WareHouseService.State(idCountry,params).subscribe(
      data=>{
        this.Loading['state']=false;
        this.StateProvinces=data.data;
      },
      err=>{
        this.Loading['state']=false;
        this.errMessages.push('Could not load State.');
      }
    );
  }



  FormBuilderViewEdit() {
    const fdx = this.configInfo ? this._getQualifier(this.configInfo, 'FDX') : '';
    const ups = this.configInfo ? this._getQualifier(this.configInfo, 'UPS') : '';

    this.WareHouseForm = this.fb.group({
      whs: this.fb.group({
        whs_name: [this.WareHouseDetail['whs_name'], Validators.required],
        whs_status: [this.WareHouseDetail['whs_status'], Validators.required],
        whs_code: [this.WareHouseDetail['whs_code'], Validators.compose([Validators.required, this._Valid.checkWHSCodeLength, this._Valid.warehouseCodeValidator])],
        whs_country_id: [this.WareHouseDetail['whs_country_id'], Validators.required],
        whs_state_id: [this.WareHouseDetail['whs_state_id'], Validators.required],
        whs_city_name: [this.WareHouseDetail['whs_city_name'], Validators.required],
      }),
      contacts: this.fb.group({
        whs_con_id: [this.WareHouseDetail['contacts']['whs_con_id']],
        whs_con_lname: [this.WareHouseDetail['contacts']['whs_con_lname'], Validators.required],
        whs_con_fname: [this.WareHouseDetail['contacts']['whs_con_fname'], Validators.required],
        whs_con_email: [this.WareHouseDetail['contacts']['whs_con_email'], Validators.compose([Validators.required, this._Valid.emailValidator])],
        whs_con_phone: [this.WareHouseDetail['contacts']['whs_con_phone'], Validators.required],
        whs_con_mobile: [this.WareHouseDetail['contacts']['whs_con_mobile']],
        whs_con_ext: [this.WareHouseDetail['contacts']['whs_con_ext']],
        whs_con_position: [this.WareHouseDetail['contacts']['whs_con_position']],
        whs_con_department: [this.WareHouseDetail['contacts']['whs_con_department']] // DEMO,
      }),
      shipping: this.fb.group({
        whs_add_id: [this.WareHouseDetail['shipping']['whs_add_id']],
        whs_add_line_1: [this.WareHouseDetail['shipping']['whs_add_line_1'], Validators.required],
        whs_add_line_2: [this.WareHouseDetail['shipping']['whs_add_line_2']],
        whs_add_city_name: [this.WareHouseDetail['shipping']['whs_add_city_name'], Validators.required],
        //whs_add_country_id: [this.WareHouseDetail['shipping']['whs_add_country_id'], Validators.required], // add to demo
        whs_add_state_id: [this.WareHouseDetail['shipping']['whs_add_state_id'], Validators.required],
        whs_add_postal_code: [this.WareHouseDetail['shipping']['whs_add_postal_code'], Validators.required],
        whs_add_type: [this.WareHouseDetail['shipping']['whs_add_type']] // demo
      }),
      billing: this.fb.group({
        whs_add_id: [this.WareHouseDetail['billing']['whs_add_id']],
        whs_add_line_1: [this.WareHouseDetail['billing']['whs_add_line_1'], Validators.required],
        whs_add_line_2: [this.WareHouseDetail['billing']['whs_add_line_2']],
        //whs_add_country_id: [this.WareHouseDetail['billing']['whs_add_country_id'], Validators.required], // add to demo
        whs_add_city_name: [this.WareHouseDetail['billing']['whs_add_city_name'], Validators.required],
        whs_add_state_id: [this.WareHouseDetail['billing']['whs_add_state_id'], Validators.required],
        whs_add_postal_code: [this.WareHouseDetail['billing']['whs_add_postal_code'], Validators.required],
        whs_add_type: [this.WareHouseDetail['billing']['whs_add_type']]  // add to demo
      }),
      meta: this.fb.group({
        whs_id: [this.warehouse_id],
        whs_meta_value: [this.WareHouseDetail['meta'] ? this.WareHouseDetail['meta']['whs_meta_value'] : false],
        whs_qualifier: ["mod"],
      }),
      configuration: this.fb.group({
        wle: [this.configInfo ? this._getQualifier(this.configInfo, 'wle') : ''],
        wtz: [this.configInfo ? this._getQualifier(this.configInfo, 'wtz') : ''],
        wno: [this.configInfo ? this._getQualifier(this.configInfo, 'wno') : '']
      }),
      carrier: this.fb.group({
        fdx: this.fb.group({
          FEDEX_KEY: [fdx ? fdx['FEDEX_KEY'] : ''],
          FEDEX_PASSWORD: [fdx ? fdx['FEDEX_PASSWORD'] : ''],
          FEDEX_METER_NUMBER: [fdx ? fdx['FEDEX_METER_NUMBER'] : ''],
          FEDEX_ACCOUNT_NUMBER: [fdx ? fdx['FEDEX_ACCOUNT_NUMBER'] : ''],
          FEDEX_APP_ENV: [fdx ? fdx['FEDEX_APP_ENV'] : ''],
          visible: [fdx ? fdx['visible'] : false]
        }),
        ups: this.fb.group({
          UPS_LICENSE_NUM: [ups ? ups['UPS_LICENSE_NUM'] : ''],
          UPS_USERNAME: [ups ? ups['UPS_USERNAME'] : ''],
          UPS_PASSWORD: [ups ? ups['UPS_PASSWORD'] : ''],
          UPS_APP_ENV: [ups ? ups['UPS_APP_ENV'] : ''],
          UPS_ACCOUNT_NUMBER: [ups ? ups['UPS_ACCOUNT_NUMBER'] : ''],
          visible: [ups ? ups['visible'] : false]
        })
      })
    });
  }

 formBuilder(){
   //console.log('rebuild form',this.WareHouseDetail['contacts']['whs_con_fname']);
   this.WareHouseForm =this.fb.group({
     whs:this.fb.group({
       whs_name: [null, Validators.required],
       whs_status: ['AC', Validators.required],
       whs_code: [null, Validators.compose([Validators.required, this._Valid.checkWHSCodeLength,this._Valid.warehouseCodeValidator])],
       whs_country_id : ['', Validators.required],
       whs_state_id: ['', Validators.required],
       whs_city_name: [null, Validators.required],
     }),
     contacts:this.fb.group({
       whs_con_fname: [null, Validators.required],
       whs_con_lname: [null, Validators.compose([Validators.required, this._Valid.validateSpace])],
       whs_con_email:[null, Validators.compose([Validators.required, this._Valid.emailValidator])],
       whs_con_phone: [null,Validators.required],
       whs_con_mobile:[null],
       whs_con_ext: [''],
       whs_con_position:[''],
       whs_con_department:["department"] // DEMO,
     }),
     shipping:this.fb.group({
       whs_add_line_1:[null, Validators.required],
       whs_add_line_2: [null],
       whs_add_city_name : [null, Validators.required],
       whs_add_country_code: ['70000', Validators.required], // add to demo
       whs_add_state_id:['', Validators.required],
       whs_add_postal_code:[null, Validators.required],
       whs_add_type:["SH"] // demo
     }),
     billing:this.fb.group({
       whs_add_line_1: [null, Validators.required],
       whs_add_line_2: [null],
       whs_add_country_code: ['70000', Validators.compose([Validators.required, this._Valid.validateSpace])], // add to demo
       whs_add_city_name : [null, Validators.compose([Validators.required, this._Valid.validateSpace])],
       whs_add_state_id:[null, Validators.required],
       whs_add_postal_code:[null, Validators.compose([Validators.required, this._Valid.validateSpace])],
       whs_add_type:["BI"]  // add to demo
     })
     ,
     meta:this.fb.group({
       whs_id: [''],
       whs_meta_value: [false],
       whs_qualifier: ["mod"],
     })
   });
 }


  /*=============================================
  *
  * @tien.nguyen
  * 1/12/2016
  *
  * =============================================*/
  change_whs_meta_value($event)
  {

    let that=this;
    if($event.target.checked)
    {
      setTimeout(function(){
        that.setControlValue('meta',true,'whs_meta_value');
        $event.target.checked=true;
      }, 100);
    }
    else{
      setTimeout(function(){
        that.setControlValue('meta',false,'whs_meta_value');
        $event.target.checked=false;
      }, 100);
    }
  }

  /*
  * @tien.nguyen 1/12/2016
  * Update control value function
  * */
  setControlValue(control,value,subcontrol)
  {
    if(subcontrol)
    {
      (<Control>this.WareHouseForm.controls[control]['controls'][subcontrol]).updateValue(value);
    }
    else{
      (<Control>this.WareHouseForm.controls[control]).updateValue(value);
    }

  }

  Save(data :any):void{
    this.submitForm=true;
    if(this.WareHouseForm.valid) {
      let data_json = JSON.stringify(data);
        if (this.Action == 'new') {
          // add new
          this.addNew(data_json);
        }
        else {
          // edit zone type
          this.Update(data_json);
        }
    }
    else{
    }

  }

  private Update(data)
  {

    this.submitForm=true;

    var n=this;

    /*check tabs*/
    this.showLoadingOverlay=true;
    if(this.WareHouseForm.valid)
    {
      this._WareHouseService.Update_WareHouse(this.warehouse_id,data).subscribe(
          data => {
            const configInfo = this.WareHouseForm.value.configuration;
            configInfo['FDX'] = this.WareHouseForm.value.carrier['fdx']; // carrier
            configInfo['UPS'] = this.WareHouseForm.value.carrier['ups']; // carrier
            configInfo['wle'] = configInfo['wle'] !== '' ? configInfo['wle'] : 0;
            // console.log(configInfo);
          if ((configInfo['wle'] !== null && configInfo['wle'] !== '' && configInfo['wtz'] !== null
            && configInfo['wtz'] !== '') || (configInfo['FDX'] !== null && configInfo['FDX'] !== '' && configInfo['UPS'] !== null
              && configInfo['UPS'] !== '')) {
              const configInfoJson = JSON.stringify(this._prepareQualifier(configInfo));

              this._WareHouseService.updateConfiguration(this.warehouse_id, configInfoJson).subscribe(
                res => {
                  this.showLoadingOverlay= false;
                  this.messages = this._Func.Messages('success','Warehouse updated successfully!');
                  setTimeout(function(){
                    n._router.parent.navigateByUrl('/system-setup/warehouse');
                  }, 600);
                },
                err => {
                  this.showLoadingOverlay = false;
                  let errMsg = err.json().message || err.json().errors.message;
                  this.messages = this._Func.Messages('danger',errMsg);
                }
              );
            } else {
              this.showLoadingOverlay= false;
              this.messages = this._Func.Messages('success','Warehouse updated successfully!');
              setTimeout(function(){
                n._router.parent.navigateByUrl('/system-setup/warehouse');
              }, 600);
            }
          },
          err => {
            this.showLoadingOverlay=false;
            let errMsg = err.json().message || err.json().errors.message;
            if(errMsg){
              this.messages = this._Func.Messages('danger',errMsg);
            }
            else{
              this.messages = this._Func.Messages('danger',this._Func.msg('VR111'));
            }
          },
          () => {}
      );
    }
    // else{
    //   this.messages = this._Func.Messages('danger', this._Func.msg('VR108'));
    // }


  }

  addNew(data){
    let n=this;
    this.showLoadingOverlay=true;
    this._WareHouseService.Add_New_WareHouse(data).subscribe(
      data => {
        this.showLoadingOverlay = false;
        //Reset Form
        this.messages = this._Func.Messages('success', this._Func.msg('VR107'));
        if(data['whs_id'])
        {
          setTimeout(function(){
            n._router.parent.navigateByUrl('/system-setup/warehouse');
          }, 600);

        }
      },
      err => {
        this.showLoadingOverlay=false;
        if(err.json().errors.message)
        {
          this.messages = this._Func.Messages('danger',err.json().errors.message);
        }
        else{
          this.messages = this._Func.Messages('danger','Data is invalid ! Please fix it and try again');
        }
      },
      () => {}
    );

  }

  /*
   * Function get All List
   * Use Eval() to handel dynamic function name
   * */

  GetListData(page=null,ListName :string)
  {
    let param="page="+page+"&limit="+this.perPage;
    this.Loading['LoadList']=true;

    if(ListName!=this.ListActive)
    {
      this.ListActive=ListName;
      //reset Pagination , ListData
      this.Pagination=[];
      this.ListData=[];
    }

    eval(`this._WareHouseService.`+ListName+`(this.warehouse_id,param).subscribe(
      data => {
        this.Loading['LoadList']=false;
        this.ListData=data.data;
        console.log('data list',data);
        console.log('param >> ',param);
        //pagin function
        this.Pagination=data['meta']['pagination'];
        this.Pagination['numLinks']=3;
        this.Pagination['tmpPageCount']=this._Func.Pagination(this.Pagination);
      },
      err => {
        console.log(err);
        this.Loading['LoadList']=false;
        this.messages = this._Func.Messages('danger','Get '+ListName+' has error !');
      },
      () => console.log('Get data complete')
    );`);
  }

  private onPageSizeChanged($event)
  {
    this.perPage = $event.target.value;
    if((this.Pagination['current_page'] - 1) * this.perPage >= this.Pagination['total']) {
      this.Pagination['current_page'] = 1;
    }
    this.GetListData(this.Pagination['current_page'],this.ListActive);
  }



  private filterList(num)
  {
    this.GetListData(num,this.ListActive);
  }


  showError(controlName)
  {
    return "<span *ngIf='" + controlName + ".control.touched&&" + controlName + ".control.hasError('required')||ListNull." + controlName + "' class='error'>" + controlName + " is required.</span>";
  }

  private Cancel()
  {
    let n = this;
    /*case 1: modify text*/
    // let warningPopup = new WarningBoxPopup();
    // warningPopup.text = "Are you sure to cancel";
    // this._boxPopupService.showWarningPopup(warningPopup)

    /*case 2: default*/
    this._boxPopupService.showWarningPopup()
        .then(function (dm) {
          if(dm)
            n._router.parent.navigateByUrl('/system-setup/warehouse/');
        })
  }

  _encodeURIComponent(params: string[], key: string): string {
    let result = '';
    params.forEach((val,index) => {
      result += index === 0 ? encodeURIComponent(key) + '=' + encodeURIComponent(val) : '&' + encodeURIComponent(key) + '=' + encodeURIComponent(val);
    })
    return result;
  }

  getConfiguration() {
	const qualifiers = ['wle', 'wtz', 'wno', 'fdx', 'ups'];
    const params = '?' + this._encodeURIComponent(qualifiers, 'whs_qualifier[]');
    this._WareHouseService.getConfiguration(this.warehouse_id, params).subscribe(
      data => {
        this.configInfo = data;
      },
      err => {
        this.errMessages.push('Could not load configuration.');
      },
      () => { }
    );
  }

  _getQualifier(arr: any[], key: string): any {
    let val = '';
    arr.forEach(item => {
      if(item['whs_qualifier'] === key) {
        val = key.toLowerCase() === 'ups' || key.toLowerCase() === 'fdx' ? JSON.parse(item['whs_meta_value']) : item['whs_meta_value'];
      }
    });
    return val;
  }

  _prepareQualifier(obj: any): any[] {
    let val = [];
    for (const key in obj) {
      if (obj.hasOwnProperty(key)) {
        val.push({'whs_qualifier': key, 'whs_meta_value': obj[key]});
      }
    }
    return val;
  }

  /*
  * Disable input text
  * */
  checkInputNumber(evt,int=false) {
    this._Valid.isNumber(evt, true);
  }

  loadTimezones($event: string) {
    if (!$event || $event === '') {
      return;
    }

    const params = `?search=${$event}&limit=10`;
    const enCodeSearchQuery = this._Func.FixEncodeURI(params);

    this.Loading['timezone'] = true;
    this._WareHouseService.getTimezones(enCodeSearchQuery).subscribe(
        data => {
            this.timezones = data;
            this.Loading['timezone'] = false;
        },
        err => {
            this.messages = this._Func.Messages('danger', this._Func.parseErrorMessageFromServer(err));
            this.Loading['timezone'] = false;
        }
    )
  }

  selectTimezone(val: string) {
      (<Control>this.WareHouseForm.controls['configuration']['controls']['wtz']).updateValue(val);
  }
}
