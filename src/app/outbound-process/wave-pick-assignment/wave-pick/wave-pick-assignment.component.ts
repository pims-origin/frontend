import {Component,Input} from '@angular/core';
import { Router } from '@angular/router-deprecated';
import { FORM_DIRECTIVES,ControlGroup,FormBuilder, Control} from '@angular/common';
import {API_Config,UserService, Functions} from '../../../common/core/load';
import { Http } from '@angular/http';
import {WMSBreadcrumb, AdvanceTable, WMSPagination, WMSMessages, WMSMultiSelectComponent, PopupAssignPicker} from '../../../common/directives/directives';
import {WavePickServices} from '../wave-pick/wave-pick-assignment-service';
declare var jQuery: any;
declare var saveAs: any;
@Component ({
  selector: 'picking-list',
  providers: [FormBuilder, WavePickServices],
  directives: [FORM_DIRECTIVES, WMSBreadcrumb, AdvanceTable, WMSMessages, WMSPagination, WMSMultiSelectComponent, PopupAssignPicker],
  templateUrl: 'wave-pick-assignment.component.html',
})

export class WavePickAssignmentComponent {

  /** list permission need to check **/
  private hasUpdateOrderPickingPermission;
  private hasPutawayAssignment;
  private viewWavePick;
  /* end list permission */

  private messages;
  private showLoadingOverlay = false;

  private tableID = 'picking-list';
  private headerURL = this._API.API_User_Metas + '/wpa';
  private headerDef = [{id: 'ver_table', value: 6},
                    {id: 'ck', name: '', width: 25},
                    {id: 'wv_sts', name: 'Status', width: 70},
                    {id: 'wv_num', name: 'Wave Pick Number', width: 150},
                    {id: 'odr_num', name: 'Order Number', width: 150, 'unsortable': true},
                    {id: 'cus_odr_num', name: 'Customer Order Number', width: 150, 'unsortable': true},
                    {id: 'cus_po', name: 'PO', width: 100, 'unsortable': true},
                    {id: 'num_sku', name: '#SKU', width: 40,'unsortable': true},
                    {id: 'num_odr', name: '#Orders', width: 40,'unsortable': true},
                    {id: 'pack_size', name: 'Pack Size', width: 90, 'unsortable': true},
                    {id: 'piece_qty_ttl', name: 'Allocated QTY', width: 90, 'unsortable': true},
                    {id: 'piece_ctns_ttl', name: 'Allocated CTNS', width: 90, 'unsortable': true},
                    {id: 'act_piece_ttl', name: 'Picked QTY', width: 90, 'unsortable': true},
                    {id: 'act_piece_ctns_ttl', name: 'Picked CTNS', width: 90, 'unsortable': true},
                    {id: 'picker', name: 'Picker', width: 130, 'unsortable': true},
                    {id: 'user', name: 'User', width: 130, 'unsortable': true},
                    {id: 'created_at', name: 'Created Date', width: 90},];
  private pinCols = 2;
  private rowData: any[] = [];
  private getSelectedRow;
  private objSort;
  private searchParams;
  private refreshRowData = false;

  private Pagination;
  private totalCount = 0;
  private tmpPageCount;
  private pageCount=0;
  private ItemOnPage=0;
  private currentPage = 1;
  private perPage = 20;
  private numLinks = 3;

  private wavepickStatus = [];
  private assignUser;
  private listSelectedGR = [];
  public refreshMultiSelect = false;

  public listSelectedItem = [];
  public URLAssign = this._API.API_Wavepick+'/assign-picker';
  public URLget = this._API.API_Authen + '/users/get-picker/';

  constructor(
  	private _http: Http,
    private _func: Functions,
    private _API: API_Config,
    private fb: FormBuilder,
    private _user: UserService,
    private _pickingListService: WavePickServices,
    private _router:Router){

    this.checkPermission();
    
  }

  private showMessage(msgStatus, msg) {
      this.messages = {
          'status': msgStatus,
          'txt': msg
      };
      jQuery(window).scrollTop(0);
  }

  private checkPermission() {
    this.showLoadingOverlay = true;
    this._user.GetPermissionUser().subscribe(
      data => {

        this.hasPutawayAssignment = this._user.RequestPermission(data,'wavepickAssignment');
        this.viewWavePick = this._user.RequestPermission(data,'viewWavePick');

        if(!this.viewWavePick) {
            this._router.parent.navigateByUrl('/deny');
        }
        else {
          this.getWavepickStatusList();
        }
      },
      err => {
        // console.log('check permission err', err);
        this.showMessage('danger', this._func.parseErrorMessageFromServer(err));
        this.showLoadingOverlay = false;
      }
    );
  }

  private getWavepickStatusList () {
    this._pickingListService.getWavepickStatusList()
      .subscribe(
        data => {
          this.wavepickStatus = data.data;
          let preSelectedStatus = false;
          for (let i = 0; i < this.wavepickStatus.length; i++) {
            const status = this.wavepickStatus[i];
            if (status.checked) {
              preSelectedStatus = true;
              
              break;
            }
          }
          if (preSelectedStatus) {
            setTimeout(() => {
              this.search();
            }, 500);
          } else {
            this.getPickingList(1);
          }
        }
      );
  }

  private getPickingList(page = null){
    this.showLoadingOverlay = true;
    if(!page) page = 1;
    let params= "?page=" + page + "&limit=" + this.perPage + '&type=wv_assign';
    if(this.objSort && this.objSort['sort_type'] != 'none') {
      params += '&sort['+ this.objSort['sort_field']+ ']='+ this.objSort['sort_type'];
    }
    else {
      params += '&sort[created_at]=desc';
    }

    if(this.searchParams) {
      params += this.searchParams;
    }

    this._pickingListService.getPickingList(params)
      .subscribe(
        data => {
          this.createRowData(data);
          this.showLoadingOverlay = false;
        },
        err => {
          this.showMessage('danger', this._func.parseErrorMessageFromServer(err));
          this.showLoadingOverlay = false;
        }
      );
  }

  private createRowData(data) {
      var rowData: any[] = [];
      if (typeof data.data != 'undefined') {
        this.initPagination(data);

        data = this._func.formatData(data.data);
        for (var i = 0; i < data.length; i++) {
          rowData.push(({
            wv_sts: data[i].wv_sts,
            wv_num: '<a href="#/outbound/picking/' + data[i].wv_id + '">'+ data[i].wv_num +'</a>',
            num_sku: data[i].num_sku,
            num_odr: data[i].num_odr,
            cus_odr_num: data[i].cus_odr_num,
            odr_num: data[i].odr_num,
            cus_po: data[i].cus_po,
            user: data[i].user,
            picker: data[i].picker_name,
            created_at: data[i].created_at,
            wp_id: data[i].wv_id,
            cus_id: data[i].cus_id,
            piece_qty_ttl: data[i].piece_qty_ttl,
            piece_ctns_ttl: data[i].piece_ctns_ttl,
            act_piece_ttl: data[i].act_piece_ttl,
            act_piece_ctns_ttl: data[i].act_piece_ctns_ttl,
            pack_size: data[i].pack_size,
            wp_num: data[i].wv_num
          }));
        }
      }
      else {
        this.rowData = [];
        this.Pagination = null;
      }

      this.rowData = rowData;
  }

  private initPagination(data){
    let meta = data['meta'];
    if(meta['pagination']) {
      this.Pagination=meta['pagination'];
    }
    else {
      let perPage = meta['perPage'],
          curPage = meta['currentPage'],
          total = meta['totalCount'];
      let count = perPage * curPage > total ? perPage * curPage - total : perPage;
      this.Pagination = {
        "total": total,
        "count": count, // TODO
        "per_page": perPage,
        "current_page": curPage,
        "total_pages": meta['pageCount'],
        "links": {
          "next": data['link']['next']
        }
      }
    }
    this.Pagination['numLinks']=3;
    this.Pagination['tmpPageCount']=this._func.Pagination(this.Pagination);
  }

  private filterList(pageNumber) {
      this.getPickingList(pageNumber);
  }

  private getPage(pageNumber) {
      let arr=new Array(pageNumber);
      return arr;
  }

  private onPageSizeChanged($event) {
      this.perPage = $event.target.value;
      if((this.Pagination['current_page'] - 1) * this.perPage >= this.Pagination['total']) {
          this.currentPage = 1;
      }
      else {
          this.currentPage = this.Pagination['current_page'];
      }
      this.getPickingList(this.currentPage);
  }

  private search(page = 1) {
    this.searchParams = '';
    let params_arr = jQuery("#form-filter").serializeArray() ;
    for (let i in params_arr) {
        if (params_arr[i].value == "") continue;
        this.searchParams +="&" +  encodeURIComponent(params_arr[i].name.trim()) + "=" + encodeURIComponent(params_arr[i].value.trim());
    }

      this.getPickingList(page);
  }

  private doSort(objSort) {
    this.objSort = objSort;
    this.getPickingList(this.Pagination.current_page);
  }

  private reset() {
      jQuery("#form-filter input[type=text], #form-filter select").each(function( index ) {
          jQuery(this).val("");
      });
      this.refreshMultiSelect = true;
      this.searchParams = '';
      this.getPickingList(1);
  }

  updatePicking() {
    this.getSelectedRow = 'edit';
  }
  printWavePick() {
    this.getSelectedRow = 'print-wavepick';
  }
  afterGetSelectedRow($event) {
    var listSelectedItem = $event.data;

    this.listSelectedGR = [];
    for(let i = 0, l = listSelectedItem.length; i < l; i++) {
      this.listSelectedGR.push(listSelectedItem[i].wp_id);
    }

    this.messages = null;
    switch ($event.action) {
      case 'edit':
        if (listSelectedItem.length > 1) {
          this.showMessage('danger', 'Please choose only one item to pick!');
        }
        else {
          if (listSelectedItem.length < 1) {
            this.showMessage('danger', 'Please choose one item to pick!');
          }
          else {
            if(listSelectedItem[0].wv_sts != 'New') {
              this.showMessage('danger', this._func.msg('VR130'));
            }
            else {
              this._router.parent.navigateByUrl('/outbound/picking/' + listSelectedItem[0].wp_id + '/edit');
            }
          }
        }
        break;
      case 'print-wavepick':
        if (listSelectedItem.length > 1) {
          this.showMessage('danger', this._func.msg('VR141'));
        }else{
          if (listSelectedItem.length < 1) {
            this.showMessage('danger', this._func.msg('VR141'));
          }else{
            this.printWavepickTicket(listSelectedItem[0].wp_id);
          }
        }
        break;

      case 'assignPicker':
        this.messages = false;
        this.assignUser = null;
        if (listSelectedItem.length < 1) {
          this.showMessage('danger', this._func.msg('WPS01'));
        }
        else {
          const listWPCompleted = [];
          for(let i = 0, l = listSelectedItem.length; i < l; i++) {
            if(listSelectedItem[i].wv_sts === 'Completed') {
              listWPCompleted.push(listSelectedItem[i].wp_num);
            }
          }

          if(listWPCompleted.length > 0) {
            this.showMessage('danger', this._func.msg('WP004').replace('{0}', listWPCompleted.join(', ')));
          }
          else {
            this.listSelectedItem = this.listSelectedGR;
          }
        }
        break;

        case 'assignPickers':
        
        if (listSelectedItem.length > 1) {
          this.showMessage('danger', 'Please choose only one item to assign Pickers!');
        }
        else {
          if (listSelectedItem.length < 1) {
            this.showMessage('danger', 'Please choose one item to assign Pickers!');
          }
          else {
            if(listSelectedItem[0].wv_sts == "Completed") {
              this.showMessage('danger', this._func.msg('WP001'));
            }
            else {
              this._router.parent.navigateByUrl('/outbound/assign-pickers/' + listSelectedItem[0].wp_id);
            }
          }
        }
        break;
    }
    this.getSelectedRow = false;
  }

  private expandTable = false;
  private viewListFullScreen() {
    this.expandTable = true;
    setTimeout(() => {
      this.expandTable = false;
    }, 500);
  }

  private printWavepickTicket(WpId){
    let that = this;
    let api = that._API.API_Wavepick  + '/print?wv_id=' +  WpId;
    try{
      let xhr = new XMLHttpRequest();
      xhr.open("GET", api , true);
      xhr.setRequestHeader("Authorization", 'Bearer ' +that._func.getToken());
      xhr.responseType = 'blob';

      xhr.onreadystatechange = function () {
        if(xhr.readyState == 2) {
          if(xhr.status == 200) {
            xhr.responseType = "blob";
          } else {
            xhr.responseType = "text";
          }
        }

        if(xhr.readyState === 4) {
          if(xhr.status === 200) {
            //update printed label value

            var today = new Date();
            var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
              "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
            ];
            var day = today.getDate();
            var month = monthNames[today.getMonth()];
            var year = today.getFullYear().toString().substr(2,2);
            var fileName = 'Wavepick_' + month+ day + year;
            var blob = new Blob([this.response], {type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'});
            saveAs.saveAs(blob, fileName + '.xlsx');
          }else{
            let errMsg = '';
            if(xhr.response) {
              try {
                var res = JSON.parse(xhr.response);
                errMsg = res.errors && res.errors.message ? res.errors.message : xhr.statusText;
              }
              catch(err){errMsg = xhr.statusText}
            }
            else {
              errMsg = xhr.statusText;
            }
            if(!errMsg) {
              errMsg = that._func.msg('VR100');
            }
            that.showMessage('danger', errMsg);
          }
        }
      }
      xhr.send();
    }catch (err){
      that.showMessage('danger', that._func.msg('VR100'));
    }

    /*if (f > 0) {

    }else{
      that.showMessage('danger', that._func.msg('CF005'));
    }*/

  }

  private assignPicker() {

    this.getSelectedRow = 'assignPicker';
  }

  private assignPickers() {
    
    this.getSelectedRow = 'assignPickers';
  }

  updatePutterInPAList() {

    this.search(this.Pagination['current_page']);
    for(var i = 0, l = this.rowData.length; i < l; i++) {
      if(this.listSelectedGR.indexOf(this.rowData[i].odr_id) != -1) {
        this.rowData[i].csr_name = this.assignUser.first_name + ' ' + this.assignUser.last_name;
      }
    }
    this.refreshList();
  }

  refreshList() {
    this.refreshRowData = true;
    setTimeout(()=>{
      this.refreshRowData = false;
    }, 400);
  }
}
