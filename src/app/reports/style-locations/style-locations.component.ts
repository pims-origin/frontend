
import {Component} from '@angular/core';
import {API_Config} from '../../common/core/API_Config';
import {BaseTable} from '../../common/core/classes_us/base-table';
import {Functions} from '../../common/core/functions';
import {TableFunctions} from '../../common/core/table-functions';
import {DateFunctions} from '../../common/core/functions/date-functions';
import {BaseService} from '../../common/core/classes_us/base-service';
import {CustomersService} from '../../master-data/customers/customers-service';
import {TableStructure} from '../../common/directives/directives';

@Component ({
    selector: 'style-locations',
    directives: [
        TableStructure
    ],
    templateUrl: '../../common/directives/table-structure/table.component.html',
    providers: [
        BaseService,
        CustomersService,
        TableFunctions,
        DateFunctions
    ]
})

export class StyleLocationsComponent extends BaseTable
{
    public tableID = 'style-locations';
    public url = this._api.API_STYLE_LOCATIONS_REPORTS;
    public headerUrlSuffix = 'rep';
    public title = 'Search Style Locations';
    public permissionPage = 'viewReport';

    public headerDef = [
        {
            id: 'ver_table',
            value: 1 + Math.random() * 10000
        },
        this.columnList.sku,
        this.columnList.plateNumber,
        this.columnList.location,
        this.columnList.cartonStatus,
        this.columnList.container,
        this.columnList.cartonPackSize,
        this.columnList.pieceRemain,
        this.columnList.totalCartons,
        this.columnList.totalPieces,
        this.columnList.customer,
        this.columnList.upc,
        this.columnList.sku,
        this.columnList.color,
        this.columnList.size,
        this.columnList.po,
        this.columnList.lot,
        this.columnList.length,
        this.columnList.width,
        this.columnList.height,
        this.columnList.weight,
        this.columnList.createdAt
    ];

    public unsortableColumns = [
        'plateNumber',
        'customer',
        'pieceRemain',
        'totalCartons',
        'totalPieces'
    ];

    public searches = [
        this.searchList.customer,
        this.searchList.container,
        this.searchList.location,
        this.searchList.upc,
        this.searchList.sku
    ];

    /*
    ****************************************************************************
    */

    constructor (
        public _api: API_Config,
        public _func: Functions,
        public _tableFunc: TableFunctions,
        public _tableService: BaseService,
        public _customersService: CustomersService
    )
    {
        super();

        this._tableFunc.permissionCall(this);
    }

    //**************************************************************************

}
