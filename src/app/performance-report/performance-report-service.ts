import {Component, Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {API_Config, Functions} from '../common/core/load';

@Injectable()
export class PerformanceReportService {

  public AuthHeader = this._Func.AuthHeader();
  public AuthHeaderPost = this._Func.AuthHeaderPost();

  constructor(private _Func: Functions, private _API: API_Config, private http: Http)
  {
  }

  getListActivity(params)
  {
    return this.http.get(this._API.API_GOODS_RECEIPT_MASTER+'/event-reports/'+params,{ headers: this.AuthHeader })
      .map((res: Response) => res.json());
  }

  getListPerformance(params)
  {
    return this.http.get(this._API.API_GOODS_RECEIPT_MASTER+'/performance-reports/'+params,{ headers: this.AuthHeader })
      .map((res: Response) => res.json());
  }

  // Get customers by WH
  public getUserList ($params) {

    return  this.http.get(this._API.API_Users +  $params, {headers: this.AuthHeader})
      .map(res => res.json());

  }

}
