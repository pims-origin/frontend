import {Component, Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {API_Config, Functions} from '../../common/core/load';

@Injectable()
export class GateManagement_REPORT_Services {

  public AuthHeader = this._Func.AuthHeader();
  public AuthHeaderPost = this._Func.AuthHeaderPost();

  constructor(private _Func: Functions, private _API: API_Config, private http: Http) {

  }

  public getListGateManagement ($params = '') {
    return this.http.get(this._API.API_Container_Plug + '/gate/gate-dtls/report' +  $params, {headers: this.AuthHeader})
      .map(res => res.json());
  }

  getCustomersByWH($params) {
    return  this.http.get(this._API.API_Customer_List_All + $params + '&sort[cus_name]=asc' , {headers: this.AuthHeader})
      .map(res => res.json());
  }

  getListContainer() {
    return  this.http.get(this._API.API_GOODS_RECEIPT + '/containers?limit=1000', {headers: this.AuthHeader})
      .map(res => res.json());
  }

  getListGate() {
    return  this.http.get(this._API.API_Container_Plug + '/gate/list', {headers: this.AuthHeader})
      .map(res => res.json());
  }

}
