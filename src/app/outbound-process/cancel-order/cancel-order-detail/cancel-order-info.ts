export class OrderDetail {
    constructor(
        public odrId,
        public odrNumb,
        public skuTotal,
        public cusId,
        public cusName,
        public cusCode,
        public cusPO,
        public odrSts,
        public odrStsName,
        public csrId,
        public csrName,
        public orgOdrType,
        public odrType,
        public odrTypeName,
        public refCod
    ) {}
}

export class BackOrder {
    constructor(
        public odrId,
        public odrSts,
        public oderType,
        public skuTotal
    ) {}
}

export class Wave {
    constructor(
        public wvId,
        public wvNum,
        public skuTotal,
        public wvSts
    ) {}
}

export class Packing {
    constructor(
        public packHeaderId,
        public packHeaderNum,
        public packSts,
        public skuTotal
    ) {}
}

export class WordOrder {
    constructor(
        public woHeaderId,
        public woHeaderNum,
        public woSts
    ) {}
}

export class StagingPallet {
    constructor(
        public packHeaderId,
        public skuTotal,
        public packHeaderNum,
        public packSts
    ) {}
}