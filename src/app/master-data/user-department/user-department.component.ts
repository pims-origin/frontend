/**
 * Created by admin on 12/28/2016.
 */
import {Component} from '@angular/core';
import {Router, RouterLink, RouteConfig, ROUTER_DIRECTIVES} from '@angular/router-deprecated';
import {UserDepartmentListComponent} from "./user-department-list/user-department-list.component";
import {CruUserDepartmentComponent} from "./cru-user-department/cru-user-department.component";


@Component ({
    selector: 'user-department',
    directives: [ROUTER_DIRECTIVES],
    template: `<router-outlet></router-outlet>`,
})

@RouteConfig([
    { path: '/', component:UserDepartmentListComponent  , name: 'User Department List', useAsDefault: true, data:{action:'list'} },
    { path: '/new', component:CruUserDepartmentComponent  , name: 'New User Department', data:{action:'new'} },
    { path: ':id', component:CruUserDepartmentComponent  , name: 'View User Department' ,data:{action:'view'}},
    { path: ':id/edit', component:CruUserDepartmentComponent  , name: 'Edit User Department' ,data:{action:'edit'}}
])
export class UserDepartmentComponent {


}
