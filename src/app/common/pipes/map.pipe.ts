/**
 * Created by admin on 6/15/2016.
 */
import {Pipe, PipeTransform} from '@angular/core';

/*
 @Pipe({name: 'mapValues'})
 export class MapValuesPipe implements PipeTransform {
 transform(value: any, args?: any[]): Object[] {
 let returnArray = [];

 value.forEach((entryVal, entryKey) => {
 returnArray.push({
 key: entryKey,
 val: entryVal
 });
 });

 return returnArray;
 }
 }
 */

@Pipe({
  name: 'mapValues'
})
export class MapValuesPipe {
  transform(dict: Object): Array<any> {
    var a = [];
    for (var key in dict) {
      if (dict.hasOwnProperty(key)) {
        a.push({key: key, val: dict[key]});
      }
    }
    return a;
  }
}
