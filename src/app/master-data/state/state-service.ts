import {Component, Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {API_Config, Functions} from '../../common/core/load';

@Injectable()
export class StateService {

  public AuthHeader = this._Func.AuthHeader();
  public AuthHeaderPost = this._Func.AuthHeaderPost();

  constructor(private _Func: Functions, private _API: API_Config, private http: Http)
  {

  }

  getStateList(param) {
    return this.http.get(this._API.API_State+param,{ headers: this.AuthHeader })
      .map((res: Response) => res.json());
  }

    //**************************************************************************

    get()
    {
        let params = '?sort[sys_state_name]=asc';

        return this.http.get(this._API.API_State + '?sort[sys_state_name]=asc', {
            headers: this.AuthHeader
        })
        .map((res: Response) => res.json());
    }

    //**************************************************************************

    getDropdown()
    {
        let params = '?sort[sys_state_name]=asc';

        return this.http.get(this._API.API_State + '/dropdown' + params, {
            headers: this.AuthHeader
        })
        .map((res: Response) => res.json());
    }

    //**************************************************************************

  getStateDetail(stateID) {
    return this.http.get(this._API.API_State+'/'+stateID, { headers: this.AuthHeader })
      .map((res: Response) => res.json().data);
  }

  updateState(stateID, stringData) {
    return this.http.put(this._API.API_State+'/'+stateID, stringData, { headers: this._Func.AuthHeaderPostJson() })
      .map((res: Response) => res.json().data);
  }

  addNewState(stringData) {
    return this.http.post(this._API.API_State, stringData, { headers: this._Func.AuthHeaderPostJson() })
      .map((res: Response) => res.json().data);
  }

  search(param) {
    return this.http.get(this._API.API_State+param,{ headers: this.AuthHeader })
      .map((res: Response) => res.json());
  }

}
