import {Component, Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {API_Config,Functions} from '../../../common/core/load';

@Injectable()
export class masterPalletServices {

    public AuthHeader = this._Func.AuthHeader();
    public AuthHeaderPost = this._Func.AuthHeaderPost();

    constructor(private _Func: Functions,
                private _API: API_Config,
                private http: Http) {

    }

    public getMasterPalletList(wh_id, id, params){
        return this.http.get(this._API.API_ORDERS_V2 + '/whs/' + wh_id +'/bol/master-pallet-list/' + id + params,{ headers: this.AuthHeader })
            .map((res: Response) => res.json());
    }

}
