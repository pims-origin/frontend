import {Component} from '@angular/core';
import {Router, RouterLink, RouteConfig, ROUTER_DIRECTIVES} from '@angular/router-deprecated';
import {RoleListComponent} from "./role-list/role-list.component";
import {CRURoleComponent} from "./cru-role/cru-role.component";

@Component ({
  selector: 'role-management',
  directives: [ROUTER_DIRECTIVES],
  template: `<router-outlet></router-outlet>`
})

@RouteConfig([
  { path: '/', component: RoleListComponent, name: 'Role List', useAsDefault: true },
  { path: '/new', component: CRURoleComponent, name: 'Create Role',data: {Action: 'New'} },
  { path: '/:id', component: CRURoleComponent, name: 'Role Detail' ,data: {Action: 'View'}},
  { path: '/:id/:edit', component: CRURoleComponent, name: 'Edit Role',data: {Action: 'Edit'} }

])
export class RoleManagementComponent {


}
