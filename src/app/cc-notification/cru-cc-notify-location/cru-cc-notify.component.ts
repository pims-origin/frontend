import {Component, DynamicComponentLoader, ElementRef, Injector} from '@angular/core';
import {FORM_DIRECTIVES, ControlGroup, FormBuilder, Control, Validators, ControlArray} from '@angular/common';
import {Http} from '@angular/http';
import {Router, RouterLink, ROUTER_DIRECTIVES, RouteParams, RouteData} from '@angular/router-deprecated';
import 'ag-grid-enterprise/main';
import 'rxjs/add/operator/map';
import {API_Config} from "../../common/core/API_Config";
import {Functions} from "../../common/core/functions";
import {CCNotifyServices} from "../cc-notify-service";
import {JwtHelper} from "angular2-jwt/angular2-jwt";
import {BoxPopup} from "../../common/popup/box-popup";
import {WMSMessages, WMSBreadcrumb} from '../../common/directives/directives';
import {WarningBoxPopup} from "../../common/popup/warning-box-popup";
import {BoxPopupService} from "../../common/popup/box-popup.service";
import {OrderBy} from "../../common/pipes/order-by.pipe";
import {UserService} from "../../common/users/users.service";
import {FormBuilderFunctions} from "../../common/core/formbuilder.functions";
import { ValidationService } from '../../common/core/validator';
declare var jQuery:any;
declare var swal:any;

@Component({
    selector: 'cru-cc-notify',
    templateUrl: 'cru-cc-notify.component.html',
    providers: [CCNotifyServices, BoxPopupService, ValidationService],
    directives: [WMSMessages, WMSBreadcrumb],
    pipes: [OrderBy],
    styles: [
        `
        .isError {
            border-color: #D91E18;
        }
      `
    ]
})

export class CruCCNotifyComponent {

    private whs_id = '';
    private messages;
    private autoSearchData = [];
    private showLoadingOverlay:boolean = false;
    private ccnlForm:ControlGroup;
    private Action;
    private TitlePage:string = "";
    private createCcnl:boolean = false;
    private editCcnl:boolean = false;
    items:ControlGroup[] = [];
    itemsTable:ControlArray = new ControlArray(this.items);
    private submitForm = {};
// Construct
    constructor(private _router:Router,
                private _ccnotifyServices:CCNotifyServices,
                private fbdFun:FormBuilderFunctions,
                private _boxPopupService:BoxPopupService,
                private _Func:Functions,
                private _Valid: ValidationService,
                private fb: FormBuilder,
                private _RTAction:RouteData,
                private _user:UserService) {
        this.whs_id = this._Func.lstGetItem('whs_id');
        this.Action = _RTAction.get('action');
        //this.dataAll['listItem'] = [];
        this.checkPermission();
        //this.formBulder();
    }

    private hasPermissionCreate = false;
    private checkPermission() {
        this.showLoadingOverlay = true;
        this._user.GetPermissionUser().subscribe(
            data => {
                this.showLoadingOverlay = false;
                this.hasPermissionCreate = this._user.RequestPermission(data,'createCycleCountNotification');

                if(!this.hasPermissionCreate) {
                    this._router.parent.navigateByUrl('/deny');
                }

                //this.createCcnl= this._user.RequestPermission(data, 'createLocation');
                //this.editCcnl = this._user.RequestPermission(data,'editLocation');
                this.createCcnl = true;
                this.editCcnl = true;

                this.getListReason();
                // Action new
                if (this.Action == 'add') {
                    if (this.createCcnl) {
                        this.TitlePage = 'Add Cycle Count Notification - Location';
                        this.formBuilder();
                    }
                    else {
                        this.redirectDeny();
                    }
                }

                /*
                 * Action router Edit
                 * */
                if (this.Action == 'edit') {
                    if (this.editCcnl) {
                        this.TitlePage = 'Edit Cycle Count Notification - Location';
                        this.getDetail();
                    }
                    else {
                        this.redirectDeny();
                    }
                }

            },
            err => {
                this.messages = {'status' : 'danger', 'txt' : this._Func.parseErrorMessageFromServerStandard(err)};
                this.showLoadingOverlay = false;
            }
        );
    }

    private getDetail() {
        alert('TODO');
    }

    private redirectDeny() {
        this._router.parent.navigateByUrl('/deny');
    }

    private formBuilder(data = {}) {
        this.ccnlForm = this.fb.group({
            details: this.itemsTable,
        });

        /*TODO if edit => load data to table*/
        /*else create 5 colunms for adding new*/
        for (var i = 0; i < 5; i++) {
            this.addNewItem();
        }

        this.ccnlForm.valueChanges.subscribe(data =>{
            this.fbdFun.checkDuplicatedKey(this.itemsTable,['location', 'item_id']);
            //this.checkField(this.itemsTable, 'remain_qty');
        });
    }

    private addNewItem(data = {}) {
        this.itemsTable.push(
            new ControlGroup({
                location: new Control(data['location'],Validators.compose([Validators.required])),
                cc_ntf_sts: new Control(null),
                reason: new Control('', Validators.compose([Validators.required])),
                loc_id: new Control(null),
                item_id: new Control(null),
                sku: new Control(data['sku'],Validators.compose([Validators.required])),
                lot: new Control(data['lot']),
                size: new Control(data['size']),
                color: new Control(data['color']),
                pack: new Control(data['ctn_pack_size']),
                //remain_qty: new Control(data['remain_qty'], Validators.compose([Validators.required , this._Valid.isZero])),
                cus_id: new Control(null),
                uom_code: new Control(null),
                uom_name: new Control(null),
                des: new Control(null),

            })
        );
    }

    Save(form) {

        //if (!form.valid) return false;
        let _check = this.checkValidate(form);

        if(_check){
            let dataSend = [];
            for(let i in form.value['details']){

                if (form.value['details'][i]['location'] && form.value['details'][i]['location'] != null) {

                    dataSend.push({
                        "loc_code": form.value['details'][i]['location'],
                        "loc_id": form.value['details'][i]['loc_id'],
                        "reason": form.value['details'][i]['reason'],
                        "cc_ntf_sts": form.value['details'][i]['cc_ntf_sts'],
                        "item_id": form.value['details'][i]['item_id'],
                        "sku": form.value['details'][i]['sku'],
                        "lot": form.value['details'][i]['lot'],
                        "pack": form.value['details'][i]['pack'],
                        "size": form.value['details'][i]['size'],
                        "color": form.value['details'][i]['color'],
                        "cus_id": form.value['details'][i]['cus_id'],
                        "uom_code": form.value['details'][i]['uom_code'],
                        "uom_name": form.value['details'][i]['uom_name'],
                        "des": form.value['details'][i]['des'],
                        "remain_qty": form.value['details'][i]['pack']

                    });
                }

            }
            this.showLoadingOverlay = true;
            this._ccnotifyServices.addNewCCNL(this.whs_id, JSON.stringify(dataSend)).debounceTime(400).distinctUntilChanged().subscribe(
                data => {
                    this.messages = {
                        'status': 'success',
                        'txt': 'Add cycle count notification - location successfully.'
                    };
                    this.showLoadingOverlay = false;

                    setTimeout(() => {
                        this._router.parent.navigateByUrl('/cycle-count-notify');
                    }, 400);
                },
                err => {
                    this.messages = {'status': 'danger', 'txt': this._Func.parseErrorMessageFromServer(err)};
                    this.showLoadingOverlay = false;
                }
            )
        }
    }

    private checkValidate(form){

        if(!this.items[0]['valid'] || this.items[0]['sku_selected'] || this.items[0]['sku_not_existed']){

            this.items[0]['submitForm']=true;
            return false;
        }

        let check = false;
        for(let i in form.value['details']){

            if (i == '0') continue;

            if((!form.value['details'][i]['location'] || !form.value['details'][i]['reason']
            || !form.value['details'][i]['sku'] || this.items[i]['sku_selected'] || this.items[i]['sku_not_existed']) &&
                (form.value['details'][i]['location'] || form.value['details'][i]['reason']
                ||form.value['details'][i]['sku'] )){

                this.items[i]['submitForm']=true;
                check = true;
            }
        }

        var submit = true;
        jQuery.each( this.listNull, function( key, value ) {

            if (value) {
                submit = false;
                return false;
            }
        });

        if (!submit) return false;

        if(check){
            return false;
        }
        return true;
    }

    deleteItem() {
        let n = this;
        if (this.fbdFun.getItemchecked(this.itemsTable) > 0) {

            /*case 1: modify text*/
            let warningPopup = new WarningBoxPopup();
            warningPopup.text = this._Func.msg('PBAP002');
            this._boxPopupService.showWarningPopup(warningPopup)
                .then(function (dm) {
                    if (!dm) {
                        return;
                    } else {
                        n.fbdFun.deleteItem(n.itemsTable).subscribe(deletedAll=>{
                            if(deletedAll) {
                                n.addNewItem();
                            }
                        });
                    }
                });
        }
        else {

            this.messages = this._Func.Messages('danger', this._Func.msg('VR113'));

        }
    }

    selectedItem(controlArray:ControlArray, row, dataItem, field = 'location') {

        if (field == 'sku') {

            this.items[row]['sku_not_existed'] = false;
            this.flagExistSku = true;
            this.setDataFieldBySku(controlArray, row, dataItem, true);
            //this.checkField(controlArray, 'remain_qty');

        } else {

            this.items[row]['flagDisSku']=true;
            this.flagExistLocation = true;
            this.items[row]['not_existed'] =  false;
            this.items[row]['sku_selected'] =  false;
            this.fbdFun.setValueControlItem(controlArray, row, field, dataItem['loc_code']);
            this.fbdFun.setValueControlItem(controlArray, row, 'loc_id', dataItem['loc_id']);
        }

        controlArray.controls[row]['disabled'] = true;

    }

    cancel() {
        let that = this;
        that._boxPopupService.showWarningPopup()
            .then(function (dm) {
                if (dm)
                    that._router.parent.navigateByUrl('/cycle-count-notify');
            })

    }

    /*saveCycleCount() {
        let dataSend = [];
        for (let i in this.dataAll['listItem']) {
            let isValidate = this.validateForm(i);
            if (!isValidate) {
                this.messages = {'status': 'danger', 'txt': 'You must fill out all required inputs before submission.'};
                break;
            }

            if (isValidate) {
                let temp = {
                    "loc_code": this.dataAll['listItem'][i]['location'],
                    "reason": this.dataAll['listItem'][i]['reason']
                }

                dataSend.push(temp);
            }
        }

        if (dataSend.length == this.dataAll['listItem'].length) {
            this.messages = {};
            this.showLoadingOverlay = true;
            this._ccnotifyServices.addNewCCNL(this.whs_id, JSON.stringify(dataSend)).debounceTime(400).distinctUntilChanged().subscribe(
                data => {
                    this.messages = {
                        'status': 'success',
                        'txt': 'Add cycle count notification - location successfully.'
                    };
                    this.showLoadingOverlay = false;

                    setTimeout(() => {
                        this._router.parent.navigateByUrl('/cycle-count-notify');
                    }, 400);
                },
                err => {
                    this.messages = {'status': 'danger', 'txt': this._Func.parseErrorMessageFromServer(err)};
                    this.showLoadingOverlay = false;
                }
            )
        }
    }*/


    private autoCompleteLocation(controlArray: ControlArray,row, key) {

            this.items[row]['flagDisSku']= false;
            this.items[row]['not_existed'] =  false;
            this.items[row]['showLoading']=true;
            let param = '/' + this.whs_id + "/complete-add-ccn?keyword=" + this._Func.trim(key);

            if(this.items[row]['subscribe']) {
                this.items[row]['subscribe'].unsubscribe();
            }
            // do Search
            this.items[row]['subscribe']=this._ccnotifyServices.getLocationCCNL(param).debounceTime(400).distinctUntilChanged().subscribe(
                data => {
                    this.items[row]['showLoading']=false;
                    this.autoSearchData[row] = data.data;
                    this.ckExistLocation(row, key);
                },
                err =>{
                    this.items[row]['showLoading']=false;
                    this.messages = {'status': 'danger', 'txt': this._Func.parseErrorMessageFromServer(err)};
                }
            );

        this.setDataFieldBySku(controlArray, row, {}, true);

    }

    private autoComplete(controlArray:ControlArray, row, key, field, item) {

        this.messages = false;
        this.items[row]['auto' + field] = [];
        this.items[row]['showLoading' + field]=true;
        this.flagExistSku = false;
        this.items[row]['sku_not_existed'] = false;

        if(this.items[row]['subscribe']) {
            this.items[row]['subscribe'].unsubscribe();
        }
        // do Search
        var param = `?sku=` +this._Func.trim(key)+ `&limit=20`;

        // if (controlArray.value[row]['loc_id'] != null) {
        //
        //     param += `&loc_id=`+  controlArray.value[row]['loc_id'];
        // }
        this.items[row]['subscribe']=this._ccnotifyServices.getSKUs(this.whs_id,param).debounceTime(400).distinctUntilChanged().subscribe(
            data => {
                this.items[row]['showLoading' + field]= false;
                this.items[row]['auto' + field] = data.data;
                this.ckExistSKu(row, item);
            },
            err =>{
                this.items[row]['showLoading' + field]= false;
                this.messages = {'status': 'danger', 'txt': this._Func.parseErrorMessageFromServer(err)};
            }
        );

       this.setDataFieldBySku(controlArray, row);
    }

    private listNull = {};
    private flagExistLocation = false;
    private ckExistLocation(row, value) {

        this.flagExistLocation = false;
        for( var i = 0 ; i < this.autoSearchData[row].length; i++) {

            if (this.autoSearchData[row][i]['loc_code'].trim() == value.trim()) {

                this.items[row]['flagDisSku']= true;
                this.flagExistLocation = true;
                break;
            }
        }

    }

    private eventCkExistLocation(row, item) {

        setTimeout(() => {

            item['not_existed'] =  false;

            if (!this.flagExistLocation) {
                item['not_existed'] =  true;
        }
        },200);

    }

    private setDataFieldBySku(controlArray:ControlArray, row, dataItem = {}, flagSetSku = false) {

        this.fbdFun.setValueControlItem(controlArray, row, 'size', dataItem['size']);
        this.fbdFun.setValueControlItem(controlArray, row, 'color', dataItem['color']);
        this.fbdFun.setValueControlItem(controlArray, row, 'lot', dataItem['lot']);
        this.fbdFun.setValueControlItem(controlArray, row, 'pack', dataItem['ctn_pack_size']);
        this.fbdFun.setValueControlItem(controlArray, row, 'cc_ntf_sts', dataItem['cc_ntf_sts']);
        this.fbdFun.setValueControlItem(controlArray, row, 'item_id', dataItem['item_id']);
        this.fbdFun.setValueControlItem(controlArray, row, 'cus_id', dataItem['cus_id']);
        this.fbdFun.setValueControlItem(controlArray, row, 'uom_code', dataItem['uom_code']);
        this.fbdFun.setValueControlItem(controlArray, row, 'uom_name', dataItem['uom_name']);
        this.fbdFun.setValueControlItem(controlArray, row, 'des', dataItem['des']);
        //this.fbdFun.setValueControlItem(controlArray, row, 'remain_qty', dataItem['remain']);

        if (flagSetSku) {

            this.fbdFun.setValueControlItem(controlArray, row, 'sku', dataItem['sku']);
        }
    }

    checkInputNumber(evt, flagInt = false)
    {
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)){
            if(flagInt) {
                evt.preventDefault();
            }
            else {
                if(charCode != 46) {
                    evt.preventDefault();
                }
            }
        }
    }


    private checkField(controlArray:ControlArray, controlname , err = {'lesspacksize': false}){

        let Arr = controlArray.value;

        for(let row in Arr){

            var control = (<Control>controlArray['controls'][row]['controls'][controlname]);
            if(control.hasError('lesspacksize')){
                delete  control.errors['lesspacksize'];
                if(!Object.keys(control.errors).length) {

                    control.setErrors(null);
                }
            }

            if (Arr[row]['remain_qty'] > Arr[row]['pack'] && Arr[row]['pack'] != null && Arr[row]['remain_qty'] != null ) {

                (<Control>controlArray['controls'][row]['controls'][controlname]).setErrors({'lesspacksize': true});
                (<Control>controlArray['controls'][row]['controls'][controlname]).markAsTouched();
            }
        }
    }

    private dataListReason = []
    private getListReason() {

        this.messages = null
        this.showLoadingOverlay = true;
        this._ccnotifyServices.getListReason().debounceTime(400).distinctUntilChanged().subscribe(
            data => {

                this.dataListReason = data.data;
                this.showLoadingOverlay = false;
            },
            err => {
                this.messages = {'status': 'danger', 'txt': this._Func.parseErrorMessageFromServer(err)};
                this.showLoadingOverlay = false;
            }
        )
    }

    private flagExistSku = true;
    private eventCkExistSku(row, item) {

        setTimeout(() => {
            item['sku_selected'] = false;

            if (!this.flagExistSku) {
                item['sku_selected'] = true;
            }
        },200);

        this.ckExistSKu(row, item);
    }

    private ckExistSKu(row, item) {

        // Check exist SKU
        var flag = false;
        for (var i = 0; i < item['autosku'].length; i++) {

            if (item['autosku'][i]['sku'] == item['value']['sku']) {
                flag = true;
                break;
            }
        }

        item['sku_not_existed'] = false;
        if (!flag) {
            item['sku_not_existed'] = true;
        }
    }

}
