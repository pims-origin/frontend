import {Component, Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {API_Config, Functions} from '../../../common/core/load';

@Injectable()
export class CancelOrderServices {

    public AuthHeader = this._Func.AuthHeader();
    public AuthHeaderPost = this._Func.AuthHeaderPost();

    constructor(private _Func: Functions, private _API: API_Config, private http: Http) {

    }

    getOrderDetail(id) {
        return this.http.get(this._API.API_ORDERS_CANCEL + '/' + id, {
            headers: this.AuthHeader
        }).map(res => res.json());
    }

    cancelOrder(listOrder, whs_id, act_cancel_dt) {
        return this.http.get(this._API.API_ORDERS_V2+ '/whs/' + whs_id + '/order/cancel?odr_id=' +listOrder + '&act_cancel_dt='+act_cancel_dt, { headers: this.AuthHeader })
            .map((res: Response) => res.json());
    }

    cancelRMA(listOrder, whs_id, act_cancel_dt) {
        return this.http.get(this._API.API_ORDERS_V2+ '/whs/' + whs_id + '/order/cancel-rma?odr_id=' +listOrder + '&act_cancel_dt='+act_cancel_dt, { headers: this.AuthHeader })
            .map((res: Response) => res.json());
    }
}
