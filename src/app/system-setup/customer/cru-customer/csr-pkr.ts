import {Functions} from '../../../common/core/load';
export class User {
    userID = '';
    whsID = '';
    defaultSet:number = null;
    defaultCheck:boolean = false;
    isDuplicated:boolean = false;
    hasErrorNotSetDefault:boolean = false;
    constructor(userId = '', defaultCheck=false, defaultSet:number=0, whsID = '') {
        this.userID = userId;
        this.defaultSet = defaultSet;
        this.defaultCheck =  defaultCheck;
        this.whsID = whsID;
    }
}

export class CsrPkr {
    qualifier:string = 'CSR';
    users: Array<User> = [];
    notSetDefault: boolean = false;
    private _func: Functions;
    constructor(type=0) {
        if(type == 1) {
            this.qualifier = 'PKR'
        }
    }

    setDefault(numberValue, checkByIndex=false) {
        this.users = this.users.map(function (user:User, index) {
            // user.defaultSet = 0;
            // user.defaultCheck = false;
            if(checkByIndex == true) {
                if(index == numberValue) {
                    user.defaultSet = 1;
                    user.defaultCheck = true;
                }
            } else { //set default check by user id
                if(user.userID == numberValue) {
                    user.defaultSet = 1;
                    user.defaultCheck = true;
                }
            }
            return user;
        })
    }

    addUser(user:User) {
        this.users.push(user);
        if(user.defaultCheck == true) {
            this.setDefault(this.users.length-1, true);
        }
        return user;
    }

    getUserByUserId(userId) {
        for(let i = 0; i < this.users.length; i++) {
            if(this.users[i].userID == userId) {
                return this.users[i];
            }
        }
    }

    checkDuplicated() {
        this.resetUserDuplicate();
        for(let i = 0; i < this.users.length-1; i++) {
            let user = this.users[i];
            if(user.userID == '') break;
            for(let j = i+1; j < this.users.length; j++) {
                if(user.userID == this.users[j].userID && user.whsID == this.users[j].whsID) {
                    user.isDuplicated = true;
                    this.users[j].isDuplicated = true;
                }
            }
        }
    }

    checkAreNotSetDefault() {
        let value = this.users.some(function(user:User) {
            return user.defaultCheck == true;
        })
        return this.notSetDefault = !value;
    }

    resetUserDuplicate() {
        this.users.forEach(function (user:User) {
            user.isDuplicated = false;
        })
    }

    resetHasErrNotSetDefault() {
        this.users.forEach(function (user:User) {
            user.hasErrorNotSetDefault = false;
        })
    }

    checkAreNotSetDefaultForEachWarehouse(dataGroup:Array<any>) {
        this.resetHasErrNotSetDefault();
        // let dataGroup = Functions.groupBy(this.users, 'id')
        dataGroup.forEach( (obj) => {
            let whsIDGroup = obj['group'];
            if(whsIDGroup != '') {
                let isErr = obj['data'].some((user:User) => {
                    return user.defaultCheck == true;
                })
                if(isErr == false) {
                    this.users.forEach((user:User) => {
                        if(user.whsID == whsIDGroup) {
                            user.hasErrorNotSetDefault = true;
                        }
                    })
                }
            }

        })
    }

}