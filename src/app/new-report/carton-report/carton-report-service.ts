import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {API_Config, Functions} from '../../common/core/load';

@Injectable()
export class CartonReportService {
	private authHeader:any;
	private authHeaderPost:any;
	private whsId:string;

	constructor(private func:Functions, private api:API_Config, private http:Http) {
		this.authHeader = func.AuthHeader();
		this.authHeaderPost = func.AuthHeaderPost();
		this.whsId = localStorage.getItem('whs_id');
	}

	getReports(params:string = '') {
		return this.http.get(`${this.api.API_CARTONS_REPORTS}/reports/${this.whsId}` + params, {headers: this.authHeader})
			.map(res => res.json());
	}

	getReportAPIUrl(params:string = '') {
		return `${this.api.API_CARTONS_REPORTS}/reports/${this.whsId}${params}&export=1`;
	}

	getCustomers(params:string = '') {
		return this.http.get(`${this.api.API_CARTONS_REPORTS}/customers/${this.whsId}` + params, {headers: this.authHeader})
			.map(res => res.json());
	}
	getLocationType(params:string = '') {
		return this.http.get(`${this.api.API_REPORTS_CORE}/carton/get-loc-type`, {headers: this.authHeader})
			.map(res => res.json());
	}

	getCTNRNums(params:string = '') {
		return this.http.get(`${this.api.API_CARTONS_REPORTS}/auto-ctnr-num/${this.whsId}` + params, {headers: this.authHeader})
			.map(res => res.json());
	}

	getCartonNums(params:string = '') {
		return this.http.get(`${this.api.API_CARTONS_REPORTS}/auto-ctn-num/${this.whsId}` + params, {headers: this.authHeader})
			.map(res => res.json());
	}
	
	getLocs(params:string = '') {
		return this.http.get(`${this.api.API_CARTONS_REPORTS}/auto-loc-code/${this.whsId}` + params, {headers: this.authHeader})
			.map(res => res.json());
	}
	
	getRFIDs(params:string = '') {
		return this.http.get(`${this.api.API_CARTONS_REPORTS}/auto-rfid/${this.whsId}` + params, {headers: this.authHeader})
			.map(res => res.json());
	}
	
	getPalletRFIDs(params:string = '') {
		return this.http.get(`${this.api.API_CARTONS_REPORTS}/auto-pallet-rfid/${this.whsId}` + params, {headers: this.authHeader})
			.map(res => res.json());
	}

	getSKUs(params:string = '') {
		return this.http.get(`${this.api.API_CARTONS_REPORTS}/auto-sku/${this.whsId}` + params, {headers: this.authHeader})
			.map(res => res.json());
	}

	getAutocompleteLocation(params:string = '') {
		return this.http.get(`${this.api.API_REPORTS_CORE}/auto-complete` + params, {headers: this.authHeader})
			.map(res => res.json());
	}

	getCartonStatus(params:string = '') {
		return this.http.get(`${this.api.API_REPORTS_CORE}/carton/status`, {headers: this.authHeader})
			.map(res => res.json());
	}

	getTotalRec(whs_id, params) {
		return this.http.get(`${this.api.API_REPORTS_CORE}/carton/reports/${whs_id}${params}&export=1&getTotalRec=1`, {headers: this.authHeader})
		.map(res => res.json());
	}
}
