
import {Component} from '@angular/core';
import {RouteConfig, ROUTER_DIRECTIVES} from '@angular/router-deprecated';
import {ReceivingReportComponent} from "./receiving/receiving.routes";
import {LandingPageComponent} from "./landing-page/landing-page.component";
import {InventoryComponent} from './inventory/inventory.component';
import {AvailableInventoryComponent} from './available-inventory/available-inventory.component';
import {WarehouseLocationsComponent} from './warehouse-locations/warehouse-locations.component';
import {StyleLocationsComponent} from './style-locations/style-locations.component';
import {InventoryHistoryComponent} from './inventory-history/inventory-history.component';
import {CustomersComponent} from './customers/customers.component';
import {OrdersComponent} from './orders/orders.component';
import {OnlineOrdersComponent} from './online-orders/online-orders.component';
import {WavePicksComponent} from './wave-picks/wave-picks.component';
import {OpenOrdersComponent} from './open-orders/open-orders.component';
import {RcvReportComponent} from './receiving-report/receiving-report.component';
import {ActRcvComponent} from './actual-receiving/actual-receiving.component';
import {RcvCtnrComponent} from './receiving-ctnr/receiving-container.component';
import {OutboundOrdComponent} from './outbound-orders/outbound-order.component';

import {StoragesComponent} from './Storages/storages.component';
import {LocationUtilizationComponent} from './locationutilization/locationutilization.component';
import {DimensionsComponent} from './dimensions/dimensions.component';

@Component ({
  selector: 'reports',
  directives: [ROUTER_DIRECTIVES],
  template: `<router-outlet></router-outlet>`
})

@RouteConfig([
    {
        path: '/',
        component: LandingPageComponent,
        name: 'Landing Page Report',
        useAsDefault: true
    },
    {
        path: '/receiving/...',
        component: ReceivingReportComponent,
        name: 'Warehouse Management'
    },
    {
        path: '/inventory',
        component: InventoryComponent,
        name: 'Search Inventory'
    },
    {
        path: '/available-inventory',
        component: AvailableInventoryComponent,
        name: 'Available Inventory'
    },
    {
        path: '/warehouse-locations',
        component: WarehouseLocationsComponent,
        name: 'Warehouse Locations'
    },
    {
        path: '/style-locations',
        component: StyleLocationsComponent,
        name: 'Style Locations'
    },
    {
        path: '/inventory-history',
        component: InventoryHistoryComponent,
        name: 'Inventory History'
    },
    {
        path: '/customers',
        component: CustomersComponent,
        name: 'Customers'
    },
    {
        path: '/orders',
        component: OrdersComponent,
        name: 'Orders'
    },
    {
        path: '/online-orders',
        component: OnlineOrdersComponent,
        name: 'OnlineOrders'
    },
    {
        path: '/open-orders',
        component: OpenOrdersComponent,
        name: 'Open Orders'
    },
    {
        path: '/wave-picks',
        component: WavePicksComponent,
        name: 'Wave Picks'
    },
    {
        path: '/receiving-report',
        component: RcvReportComponent,
        name: 'Receiving Reports'
    },
     {
        path: '/storages',
        component: StoragesComponent,
        name: 'Storages'
    },
    {
        path: '/locationutilization',
        component: LocationUtilizationComponent,
        name: 'LocationUtilization'
    },
    {
        path: '/actual-receiving',
        component: ActRcvComponent,
        name: 'Actual Receiving Report'
    },
    {
        path: '/receiving-container',
        component: RcvCtnrComponent,
        name: 'Receiving Container Report'
    },
    {
        path: '/dimensions',
        component: DimensionsComponent,
        name: 'Dimensions'
    },
    {
        path: '/outbound-order',
        component: OutboundOrdComponent,
        name: 'Outbound Order'
    },
])

export class ReportsComponent
{

}
