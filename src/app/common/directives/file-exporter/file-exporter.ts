import {Component, Input} from '@angular/core';

declare var $:any;

@Component({
	selector: 'file-exporter',
	templateUrl: 'file-exporter.html',
})

export class FileExporter {
	private exportedData:Array<any>;
	private exportedHeader:Array<any>;
	private namedFile:string;

	@Input('dataExport') set dataExport(value:any) {
		this.exportedData = value;
	}

	@Input('headerExport') set headerExport(value:any) {
		this.exportedHeader = value;
	}

	@Input('fileName') set fileName(value:any) {
		this.namedFile = value;
	}

	constructor() {
	}

	private exportCsv() {
		var content = this.exportedData;
		var finalVal = this.exportedHeader.join(',') + '\n';

		for (var i = 0; i < content.length; i++) {
			var value = content[i];

			for (var j = 0; j < value.length; j++) {
				var innerValue = (!value[j] && value[j] !== false) ? '' : value[j].toString();
				var result = innerValue.replace(/"/g, '""');
				if (result.search(/("|,|\n)/g) >= 0)
					result = '"' + result + '"';
				if (j > 0)
					finalVal += ',';
				finalVal += result;
			}

			finalVal += '\n';
		}
		var blob = new Blob([finalVal], {type: 'text/csv;charset=utf-8;'});

		if (navigator.msSaveBlob) { // IE 10+
			navigator.msSaveBlob(blob, finalVal);
		} else {
			var downloadLink = $(`<a style=\"visibility:hidden;\" href=\"data:text/csv;charset=utf-8,${encodeURIComponent(finalVal)}\" download=\"${this.namedFile}\"></a>`).appendTo('body');
			downloadLink[0].click();
			downloadLink.remove();
		}
	}
}
