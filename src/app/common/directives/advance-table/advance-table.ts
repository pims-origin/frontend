import {Component, OnInit, Input,Output,EventEmitter, Injectable, ElementRef} from '@angular/core';
import {Http} from '@angular/http';
import {Functions} from '../../core/load';
import {AgGridNg2} from 'ag-grid-ng2/main';
import {GridOptions} from 'ag-grid/main';
import 'ag-grid-enterprise/main';
declare var jQuery: any;
@Component({
  selector: 'advance-table',
  providers:[],
  directives: [AgGridNg2],
  templateUrl: 'advance-table.html',
})
@Injectable()
export class AdvanceTable {
  private columnDefs:Array<any>=[];
  private table;
  private timeoutUpdateHeader;
  private gridOptions: GridOptions;
  @Input() tableID: string;
  @Input() headerURL: string;
  @Input() headerDef: any[];
  @Input() pinCols: number;
  @Input() rowData: any[];
  @Input('rowData') set resetCheckboxAll(value: Array<any>) {
    this.table && this.table.find('.ag-header-cell :checkbox').prop('checked', false);
    let that=this;
    setTimeout(() => {
      that.setTitleforEllipsisText();
    },1000);
  }
  @Input('getSelectedRow') set returnSelectedRows(value) {
    if(value) {
      this.returnSelectedRow.emit({data: this.gridOptions.api.getSelectedRows(), action: value});
    }
  }
  @Input('refreshRowData') set refreshRowData(value) {
    if(value) {
      this.gridOptions.api.refreshRows(this.gridOptions.api.getSelectedNodes());
    }
  }
  @Input('expandTable') set expandTable(value) {
    if(value) {
      var allColumnIds = [];
      this.columnDefs.forEach( function(columnDef) {
        allColumnIds.push(columnDef.field);
      });
      this.gridOptions.columnApi.autoSizeColumns(allColumnIds);
      this.gridOptions.api.sizeColumnsToFit();
    }
  }
  @Output() returnSelectedRow  = new EventEmitter();
  @Output() cellClicked  = new EventEmitter();
  @Output() sort = new EventEmitter();
  @Output() selectionChanged = new EventEmitter();
  private inputClipboard;
  constructor(
      el: ElementRef,
      private _func: Functions,
      private _http: Http){
    var that = this;
    console.log('init', this.headerDef);
    this.gridOptions = <GridOptions>{
      onClipboardPaste: (event) => {
        try {
          var focusCell = that.gridOptions.api.getFocusedCell();
          var cellValue = that.rowData[focusCell['rowIndex']][focusCell['column']['colId']].toString();
          var jValue = jQuery(cellValue);
          if(jValue.length) {
            var selectedNoteText = jValue.hasClass('hover-value') ? jValue.find(' > a').text() : jValue.text();
            if(selectedNoteText == '') selectedNoteText = ' ';
            if(!that.inputClipboard) {
              that.inputClipboard = jQuery('<textarea class="input-clipboard"></textarea>').appendTo('body');
            }
            that.inputClipboard.text(selectedNoteText);
            that.inputClipboard.select();
            document.execCommand('copy');
          }
        }
        catch(err) {}
      }
    };
  }
  ngOnInit() {}
  ngAfterViewInit() {
    this.table = jQuery('#' + this.tableID);
    this.createColumnDefs();
  }
  private checkCheckAll() {
    var table = this.table;
    let checkboxAll = table.find('.ag-header-cell :checkbox');
    try {
      if(this.rowData.length) {
        if(this.gridOptions.api.getSelectedRows().length==this.rowData.length){
          checkboxAll.prop('checked', true);
        }
        else {
          checkboxAll.prop('checked', false);
        }
      }
    }
    catch(err) {}
  }
  private initToggleCheckAll() {
    var that = this,
        table = this.table;
    table.on('change.toggle', '.ag-header-cell :checkbox', function(){
      var checkboxAll = jQuery(this);
      if(checkboxAll.prop('checked')) {
        that.gridOptions.api.selectAll();
      }
      else {
        that.gridOptions.api.deselectAll();
      }
    });
  }
  private initDragDrop() {
    var that = this,
        table = this.table,
        headerCols = table.find('.ag-header-cell'),
        fixedColEdge = headerCols.filter('.column_fixed').last(),
        fixedIndexEdge = headerCols.index(fixedColEdge),
        lastIndex = headerCols.length - 1,
        drag = false,
        dragElm, dragIdx, leftPos, rightPos, lastMousePos;
    table.off('mousedown', '.ag-header-cell-label').on('mousedown', '.ag-header-cell-label', function(event) {
      event.preventDefault();
      dragElm = jQuery(this);
      dragIdx = table.find('.ag-header-cell-label').index(dragElm);
      if(dragIdx > fixedIndexEdge) {
        drag = true;
        leftPos = dragElm.offset().left;
        rightPos = leftPos + dragElm.width();
        lastMousePos = event.pageX;
      }
    }).off('mousemove').on('mousemove', function(event) {
      event.preventDefault();
      if(drag) {
        var currPos = event.pageX;
        if(currPos > lastMousePos && currPos > rightPos && dragIdx < lastIndex) {
          that.gridOptions.columnApi.moveColumn(dragIdx, dragIdx+1);
          dragIdx++;
          dragElm = table.find('.ag-header-cell-label').eq(dragIdx);
          leftPos = dragElm.offset().left;
          rightPos = leftPos + dragElm.width();
        }
        if(currPos < lastMousePos && currPos < leftPos && dragIdx > fixedIndexEdge + 1) {
          that.gridOptions.columnApi.moveColumn(dragIdx, dragIdx-1);
          dragIdx--;
          dragElm = table.find('.ag-header-cell-label').eq(dragIdx);
          leftPos = dragElm.offset().left;
          rightPos = leftPos + dragElm.width();
        }
        lastMousePos = currPos;
      }
    }).off('mouseup mouseleave').on('mouseup mouseleave', function() {
      drag = false;
    });
  }
  private initSortable() {
    var _this = this,
        allCols = this.table.find('.sorting');
    this.table.on('click.sort', '.sortable .ag-header-cell-label', function() {
      var col = jQuery(this).closest('.sortable'),
          sortType, sortClass, sortField = col.attr('colid');
      if(col.hasClass('sorting')) { sortType = 'asc'; sortClass='sorting-asc'; };
      if(col.hasClass('sorting-asc')) { sortType = 'desc';  sortClass='sorting-desc';};
      if(col.hasClass('sorting-desc')) { sortType = 'none';  sortClass='sorting';};
      allCols.removeClass('sorting sorting-asc sorting-desc').addClass('sorting');
      col.removeClass('sorting sorting-asc sorting-desc').addClass(sortClass);
      _this.sort.emit({sort_type: sortType, sort_field: sortField});
    });
  }
  private createColumnDefs() {
    var tmp_table_header;
    this._http.get(this.headerURL,{ headers: this._func.AuthHeader()})
        .map((res) => res.json())
        .subscribe(
            data => {
              if(data && data.data) {
                tmp_table_header = JSON.parse(data.data);
                if(!jQuery.isArray(tmp_table_header) || tmp_table_header[0].id != 'ver_table' || this.headerDef[0].id != 'ver_table' || tmp_table_header[0].value != this.headerDef[0].value) {
                  console.log('set null tmp_table_header', tmp_table_header);
                  tmp_table_header = null;
                }
              }
            },
            err => {
              this.generateHeader(tmp_table_header);
            },
            () => {
              this.generateHeader(tmp_table_header);
            }
        )
  }
  private generateHeader(tmp_table_header) {
    var fullScreen = false;
    if(!tmp_table_header) {
      fullScreen = true;
      tmp_table_header = this.headerDef;
      this._http.put(this.headerURL, JSON.stringify({'value': JSON.stringify(this.headerDef)}), { headers: this._func.AuthHeaderPostJson()})
          .map((res) => res.json().data).subscribe();
    }
    this.columnDefs = [];
    for(let i = 0, l = tmp_table_header.length; i < l; i++) {
      if(tmp_table_header[i].id != 'ver_table') {
        let dataDefs = {
          headerName: tmp_table_header[i].name,
          headerTooltip : tmp_table_header[i].name,
          field: tmp_table_header[i].id,
          width: tmp_table_header[i].width,
          headerClass: i <= this.pinCols + 1 ? 'column_fixed' : '',
          cellClass: i <= this.pinCols + 1 ? 'row_column_fixed' : '',
          suppressMovable: true, suppressMenu: false, editable: false
        };
        if(tmp_table_header[i].id != 'ck') {
          for(let j = 0, k = this.headerDef.length; j < k; j++) {
            if(this.headerDef[j].id == tmp_table_header[i].id && !this.headerDef[j]['unsortable']) {
              dataDefs['headerClass'] += ' sorting sortable';
            }
          }
        }
        if(tmp_table_header[i].id == 'ck') {
          dataDefs['headerName'] = '';
          dataDefs['sortingOrder'] = [null];
          dataDefs['checkboxSelection'] = true;
          dataDefs['headerCellTemplate']= function(){
            var eCell = document.createElement('div');
            eCell.innerHTML = '<div id="agHeaderCellLabel" class="ag-header-cell-label"><input type="checkbox"></div>';
            return eCell;
          };
          dataDefs['suppressSizeToFit'] = true;
        }
        this.columnDefs.push(dataDefs)
      }
    }
    setTimeout(() => {
      if(fullScreen) {
        this.gridOptions.api.sizeColumnsToFit();
      }
      this.initDragDrop();
      this.initToggleCheckAll();
      this.initSortable();
    }, 400)
  }
  private updateHeader() {
    var table_state = this.gridOptions.columnApi.getColumnState();
    if(this.timeoutUpdateHeader) {
      clearTimeout(this.timeoutUpdateHeader);
    }
    this.timeoutUpdateHeader = setTimeout(() => {
      var tmp = [];
      tmp.push({
        id: this.headerDef[0].id,
        value: this.headerDef[0].value
      });
      for (var i = 0; i < table_state.length; i++){
        tmp.push({
          'id': table_state[i].colId,
          'width': table_state[i].width,
          'name': this.gridOptions.columnApi.getColumn(table_state[i].colId)['colDef'].headerName
        })
      }
      this._http.put(this.headerURL, JSON.stringify({'value': JSON.stringify(tmp)}), { headers: this._func.AuthHeaderPostJson() })
          .map((res) => res.json().data).subscribe(
          data => {
          },
          err => {
          }
      );
    }, 400);
  }
  private onColumnMoved($event) {
    this.updateHeader();
    if(this.rowData.length) {
      this.checkCheckAll();
    }
    else {
      this.table.find('.ag-header-cell :checkbox').prop('checked', false);
    }
  }
  private onColumnResized() {
    this.updateHeader();
    this.checkCheckAll();
  }
  private onSelectionChanged() {
    this.selectionChanged.emit(this.gridOptions.api.getSelectedRows());
    this.checkCheckAll();
  }
  private onCellClicked($event) {
    this.cellClicked.emit($event);
  }
  private setTitleforEllipsisText(){
    let that=this;
    var table = this.table;
    table.find('.ag-cell').each(function(i,e){
      var cell = jQuery(this);
      if(cell.attr('id') != 'ck') {
        var children = cell.children();
        if(!children.length) {
          let text = cell.text().replace(/"/g, '&#34;').replace(/</g, '&lt;');
          cell.html(`<span title="${text}">${text}</span>`);
        }
        else {
          if(children[0].tagName == 'A') {
            let text = children.text().replace(/"/g, '&#34;').replace(/</g, '&lt;');
            children.html(`<span title="${text}">${text}</span>`);
          }
        }
      }
    });
  }
}
