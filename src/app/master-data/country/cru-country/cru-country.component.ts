import {Component} from '@angular/core';
import { FORM_DIRECTIVES,ControlGroup,FormBuilder, Control ,Validators} from '@angular/common';
import {RouteParams,Router,RouteData } from '@angular/router-deprecated';
import {API_Config, UserService, Functions} from '../../../common/core/load';
import {CountryService} from '../country-service';
import { Http } from '@angular/http';
import {GoBackComponent} from "../../../common/component/goBack.component";
import { ValidationService } from '../../../common/core/validator';
import {ErrorMessageForControlService} from "../../../common/services/error-message-for-control.service";
import {BoxPopup} from "../../../common/popup/box-popup";
import {WarningBoxPopup} from "../../../common/popup/warning-box-popup";
import {BoxPopupService} from "../../../common/popup/box-popup.service";
import {JwtHelper} from "angular2-jwt/angular2-jwt";
declare var jQuery: any;
import {WMSBreadcrumb,WMSMessages} from '../../../common/directives/directives';
@Component({
    selector: 'cru-country',
    providers: [CountryService,FormBuilder,ValidationService, ErrorMessageForControlService, BoxPopupService],
    directives: [FORM_DIRECTIVES,GoBackComponent,WMSMessages,WMSBreadcrumb],
    templateUrl: 'cru-country.component.html',

})

export class CURCountryComponent {

    public loading = [];
    private messages;
    private action;
    private actionText;
    private hasCountry:boolean = false;
    private countryID;
    private idDetail;
    private detail;
    private isView:boolean = false;
    private TitlePage='Country';
    countryForm: ControlGroup;
    private showError:boolean=false;
    public showLoadingOverlay = false;
    private _submitForm=false;

    constructor(
        private _http: Http,
        private _func: Functions,
        private _user: UserService,
        private params: RouteParams,
        private _Valid: ValidationService,
        private _ErrMsgControlService: ErrorMessageForControlService,
        private _boxPopupService: BoxPopupService,
        public jwtHelper: JwtHelper,
        private _countryService: CountryService,
        private fb: FormBuilder,
        _RTAction: RouteData,
        private _router: Router) {

        this.checkPermission();
        this.action = _RTAction.get('action');
        this._ErrMsgControlService.errorMsg.invalidSpace = 'is required.';  // set error message for invalid space

        // this.buildForm();

        if(this.action=='view')
        {
            this.isView=true;
            this.TitlePage='View Country';
        }
        if(this.action=='edit')
        {
            this.TitlePage='Edit Country';
        }
        if(this.action=='edit'||this.action=='view')
        {
            // edit

            this.idDetail=this.params.get('id');
            this._countryService.getCountryDetail(this.idDetail).subscribe(
                data => {
                    this.hasCountry = true;
                    // console.log('get country detail success', data);
                    this.detail = data;
                    this.FormBuilderEdit();

                },
                err => {
                    this._router.parent.navigateByUrl('/master-data/country');
                },
                () => {}
            );

        }
        else{
            // Action new
            if(this.action=='new')
            {
                this.TitlePage='Add Country';
                this.FormBuilder();
                // if(this.chgTypeForm.touched) {
                // this._genErrMsgForMultiControls(this.chgTypeForm, this.errorMsg);
                // this._ErrMsgControlService(this.chgTypeForm);
                // }
            }
            else{

            }

        }
        
    }

    private checkPermission() {
        this.showLoadingOverlay = true;
        this._user.GetPermissionUser().subscribe(
            data => {
                if((this.action == 'view' && !this._user.RequestPermission(data,'viewCountry')) || (this.action == 'edit' && !this._user.RequestPermission(data,'editCountry')) || (this.action == 'new' && !this._user.RequestPermission(data,'createCountry'))) {
                    this._router.parent.navigateByUrl('/deny');
                }
                this.showLoadingOverlay = false;
            },
            err => {
                this.showMessage('danger', this._func.parseErrorMessageFromServer(err));
                this.showLoadingOverlay = false;
            }
        );
    }

    FormBuilder()
    {
        this.countryForm =this.fb.group({
            sys_country_code: [null, Validators.compose([Validators.required, this._Valid.validateSpace, this._Valid.checkCountryCodeLength, this._Valid.countryCodeValidator])],
            sys_country_name: [null, Validators.compose([Validators.required, this._Valid.validateSpace, this._Valid.invalidNameStr])]
        });
    }

    FormBuilderEdit()
    {
        this.countryForm =this.fb.group({
            sys_country_code: [this.detail['sys_country_code'], Validators.compose([Validators.required, this._Valid.validateSpace,this._Valid.checkCountryCodeLength, this._Valid.countryCodeValidator])],
            sys_country_name: [this.detail['sys_country_name'], Validators.compose([Validators.required, this._Valid.validateSpace, this._Valid.invalidNameStr])]
        });
    }


    submitForm(data :Object):void{

        let n=this;
        this._ErrMsgControlService._genErrMsgForMultiControls(this.countryForm);

        this._submitForm=true;

        if(this.countryForm.valid) {

            // trim input of 2 fields
            this._trimObjProperty(data, ['sys_country_code', 'sys_country_name']);

            let data_json = JSON.stringify(data);

            if (this.action == 'new') {
                // add new
                this.addNew(data_json);
            }
            else {
                // edit zone type
                this.update(this.idDetail,data_json);
            }

        }
        // else{
        //     this.messages = this._Func.Messages('danger', this._Func.msg('VR108'));
        // }

    }


    update(countryID, stringData) {
        let that = this;
        this.loading['AddUpdate']=true;
        this._countryService.updateCountry(countryID,stringData).subscribe(
          data => {
            this.loading['AddUpdate']=false;
            this.messages = this._func.Messages('success', this._func.msg('VR109'));

            setTimeout(function(){
                that._router.parent.navigateByUrl('/master-data/country');
            }, 600);
          },
          err => {
            this.loading['AddUpdate']=false;
            this.showMessage('danger', this._func.parseErrorMessageFromServer(err));
          },
          () => {}
        );
    }

    addNew(stringData) {
        let that = this;
        this.loading['AddUpdate']=true;
        this._countryService.addNewCountry(stringData).subscribe(
            data => {
                this.loading['AddUpdate'] = false;
                this.messages = this._func.Messages('success', this._func.msg('VR107'));
                setTimeout(function(){
                    that._router.parent.navigateByUrl('/master-data/country');
                }, 600);
            },
            err => {
                this.loading['AddUpdate'] = false;
                this.showMessage('danger', this._func.parseErrorMessageFromServer(err));
            },
            () => {}
        );
    }

    /*
     * obj: object
     * properties: keys of object need to trim
     * */
    _trimObjProperty(obj, properties)
    {
        properties.forEach(function(prop) {
            obj[prop] = obj[prop].trim();
        });
    }

    cancel() {
        let n = this;
        /*case 1: modify text*/
        // let warningPopup = new WarningBoxPopup();
        // warningPopup.text = "Are you sure to cancel";
        // this._boxPopupService.showWarningPopup(warningPopup)

        /*case 2: default*/
        this._boxPopupService.showWarningPopup()
            .then(function (dm) {
                if(dm)
                    n._router.navigateByUrl('/master-data/country');

            });
    }

    detectControlErr(control) {
        // this._genErrMsgForControl(control, this.errorMsg);
        this._ErrMsgControlService._genErrMsgForControl(control);
    }

    private showMessage(msgStatus, msg) {
        // if(this.timeoutHideMessage) clearTimeout(this.timeoutHideMessage);
        this.messages = {
            'status': msgStatus,
            'txt': msg
        }
        // this.timeoutHideMessage = setTimeout(() => {
        //     this.messages = null;
        // }, 4000);
        jQuery(window).scrollTop(0);
    }
}
