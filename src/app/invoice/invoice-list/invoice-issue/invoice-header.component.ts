
import {Component, ViewChild} from '@angular/core';
import {API_Config} from '../../../common/core/API_Config';
import {BaseTable} from '../../../common/core/classes_us/base-table';
import {TableStructure} from '../../../common/directives/directives';

import {Alert} from '../../../common/directives/popup-modal_us/alert/alert';

@Component({
    selector: 'invoice-header',
    directives: [
        TableStructure,
        Alert
    ],
    templateUrl: '../../../common/directives/table-structure/table.component.html',
})

export class InvoiceHeaderComponent extends BaseTable
{
    @ViewChild(Alert) alert: Alert;

    public tableID = 'invoice-header';
    public headerURL = this._API_Config.API_User_Metas + '/rep';

    public headerDef = [
        {
            id: 'ver_table',
            value: 1 + Math.random() * 10000
        },
        {
            id: 'cus_name',
            name: 'CUST',
            width: 300,
            unsortable: true
        },
        {
            id: 'cus_code',
            name: 'CUST REF',
            width: 250,
            unsortable: true
        },
        {
            id: 'inv_num',
            name: 'INV NBR',
            width: 250,
            unsortable: true
        },
        {
            id: 'inv_dt',
            name: 'INV DT',
            width: 200,
            unsortable: true
        },
        {
            id: 'terms',
            name: 'TERMS',
            width: 200,
            unsortable: true
        }
    ];

    public skipPageSize = true;
    public skipExport = true;
    public skipPagination = true;

    constructor (
        private _API_Config: API_Config
    )
    {
        super();

        this.tableComponent = this;
    }

    //**************************************************************************

    public gridUpdate(tableData)
    {
        this.rowData = [];

        this.rowData.push(tableData);
    }

    //**************************************************************************

}