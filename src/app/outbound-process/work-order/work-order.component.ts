import {Component} from '@angular/core';
import {RouteConfig, ROUTER_DIRECTIVES} from '@angular/router-deprecated';
import {CRUWorkOrderCC} from "./work-order-cru";
import {WorkOrderList} from "./work-order-list/work-order-list.component";

@Component ({
    selector: 'work-order',
    directives: [ROUTER_DIRECTIVES],
    template: `<router-outlet></router-outlet>`
})

@RouteConfig([
    { path: '/', component: WorkOrderList  , name: 'Work Order List', useAsDefault: true },
    { path: '/new', component: CRUWorkOrderCC  , name: 'Create Work Order' , data:{action:'new'}},
    { path: '/:cus_id/:ord_num/new', component: CRUWorkOrderCC  , name: 'Create Work Order' , data:{action:'new'}},
    { path: '/:id', component: CRUWorkOrderCC  , name: 'View Work Order', data:{action:'view'} },
    { path: '/:id/edit', component: CRUWorkOrderCC  , name: 'Edit Work Order', data:{action:'edit'} },
])
export class WorkOrder {



}
