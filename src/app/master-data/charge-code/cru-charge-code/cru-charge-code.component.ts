import {Component} from '@angular/core';
import { FORM_DIRECTIVES,ControlGroup,FormBuilder, Control ,Validators} from '@angular/common';
import {RouteParams,Router ,RouteData} from '@angular/router-deprecated';
import {API_Config, UserService, Functions} from '../../../common/core/load';
import { ValidationService } from '../../../common/core/validator';
import { Http } from '@angular/http';
import { ControlMessages } from '../../../common/core/control-messages';
import {GoBackComponent} from "../../../common/component/goBack.component";
import {ChargeCodeService} from "../charge-code-service";
import {UOMServices} from "../../uom/uom-service";
import {ChargeTypeService} from "../../charge-type/charge-type-service";
import {ChargeCode} from "../charge-code";
import {ErrorMessageForControlService} from "../../../common/services/error-message-for-control.service";
import {BoxPopup} from "../../../common/popup/box-popup";
import {WarningBoxPopup} from "../../../common/popup/warning-box-popup";
import {BoxPopupService} from "../../../common/popup/box-popup.service";
import {JwtHelper} from "angular2-jwt/angular2-jwt";
import { OrderBy } from "../../../common/pipes/order-by.pipe";
import {WMSBreadcrumb,WMSMessages} from '../../../common/directives/directives';
declare var jQuery: any;

@Component({
    selector: 'cru-charge-code',
    providers: [ChargeCodeService,ValidationService,FormBuilder, UOMServices, ChargeTypeService,ErrorMessageForControlService, BoxPopupService],
    directives: [FORM_DIRECTIVES,ControlMessages,GoBackComponent,WMSMessages,WMSBreadcrumb],
    pipes: [OrderBy],
    templateUrl: 'cru-charge-code.component.html',
})

export class CruChargeCodeComponent {
    public loading = [];
    private messages;
    private action;
    private idDetail;
    private TitlePage = 'Charge Code';
    private isView:boolean=false;
    private listChargeType = [];
    private listChargeUOM = [];
    private selectedUOMId = 0;
    private selectedChgTypeId = 0;
    public showLoadingOverlay = false;
    private detail;
    private submitted:boolean = false;

    chgCodeForm:ControlGroup;

    private errorMsg =
        {
            'required': 'is required.',
            'invalidSpace': 'has invalid space.',
            // 'invalidEmailAddress': 'must be email address.'
        };

    chgCode = new ChargeCode(null, '',
                            '',
                            '',
                            '');

    constructor(
        private _http: Http,
        private _Func: Functions,
        private _user: UserService,
        private params: RouteParams,
        private _Valid: ValidationService,
        private _chgCodeService: ChargeCodeService,
        private _chgTypeService: ChargeTypeService,
        private _uomService: UOMServices,
        private _boxPopupService: BoxPopupService,
        public jwtHelper: JwtHelper,
        private _ErrMsgControlService: ErrorMessageForControlService,
        private fb: FormBuilder,
        _RTAction: RouteData,
        private _router: Router) {

        this.action=_RTAction.get('action');

        this.checkPermission();
        this._ErrMsgControlService.errorMsg.invalidSpace = 'is required.';  // set error message for invalid space
        this._uomService.getUOM('?limit=500').subscribe(
            data => {
                this.listChargeUOM = data.data;

            },
            err => {},
            () => {}
        );

        this._chgTypeService.getChargeType('?limit=500').subscribe(
            data => {
                this.listChargeType = data.data;
            },
            err => {},
            () => {}
        );


        if(this.action=='view')
        {
            this.isView=true;
            this.TitlePage='View Charge Code';
        }
        if(this.action=='edit')
        {
            this.TitlePage='Edit Charge Code';
        }
        if(this.action=='edit'||this.action=='view')
        {
            // edit

            this.chgCode.id = +this.params.get('id');
            this._chgCodeService.getChargeCodeDetail(this.chgCode.id).subscribe(
                data => {
                    // this.detail=data;
                    this.chgCode.des = data.chg_code_des;
                    this.chgCode.name = data.chg_code;
                    this.chgCode.uomId = data.chg_uom_id;
                    this.chgCode.chgTypeId = data.chg_type_id;
                    this.FormBuilderEdit();
                },
                err => {
                    this._router.parent.navigateByUrl('/master-data/charge-code');
                },
                () => {}
            );

        } else {
            // Action new
            if(this.action=='new')
            {
                this.TitlePage='Add Charge Code';
                // this.chgCode.uomId = '';
                // this.chgCode.chgTypeId = '';
                this.FormBuilder();
            }
            else{

            }
        }

    }

    private checkPermission() {
        this.showLoadingOverlay = true;
        this._user.GetPermissionUser().subscribe(
            data => {
                if((this.action == 'view' && !this._user.RequestPermission(data,'viewChargeCode')) || (this.action == 'edit' && !this._user.RequestPermission(data,'editChargeCode')) || (this.action == 'new' && !this._user.RequestPermission(data,'createChargeCode'))) {
                    this._router.parent.navigateByUrl('/deny');
                }
                this.showLoadingOverlay = false;
            },
            err => {
                this.showMessage('danger', this._Func.parseErrorMessageFromServer(err));
                this.showLoadingOverlay = false;
            }
        );
    }

    FormBuilder()
    {
        let n = this;
        this.chgCodeForm =this.fb.group({
            chg_code:[this.chgCode.name, Validators.compose([Validators.required, this._Valid.validateSpace, this._Valid.invalidLength3])],
            chg_type_id:[null, Validators.compose([Validators.required])],
            chg_uom_id: [null, Validators.compose([Validators.required])],
            chg_code_des :[this.chgCode.des]
        });
    }

    FormBuilderEdit()
    {
        this.chgCodeForm =this.fb.group({
            chg_code:[this.chgCode.name, Validators.compose([Validators.required, this._Valid.validateSpace, this._Valid.invalidLength3])],
            chg_type_id:[this.chgCode.chgTypeId, Validators.compose([Validators.required])],
            chg_uom_id: [this.chgCode.uomId, Validators.compose([Validators.required])],
            chg_code_des :[this.chgCode.des]
        });
    }


    /*
     * Save
     * */
    save(form):void{
        let data = form.value;
        let n=this;
        this._ErrMsgControlService._genErrMsgForMultiControls(form);
        this.submitted = true;
        if(form.valid) {

            n._trimObjProperty(data, ['chg_code']);
            let data_json = JSON.stringify(data);

            if (n.action == 'new') {
                // add new
                this.addNew(data_json);
            }
            else {
                // edit zone type
                this.update(this.chgCode.id, data_json);
            }

        }
        // else{
        //     this.messages = this._Func.Messages('danger', this._Func.msg('VR108'));
        // }

    }

    update(id,data)
    {
        let n=this;
        this.loading['AddUpdate']=true;
        this._chgCodeService.updateChgCode(id,data).subscribe(
            data => {
                this.loading['AddUpdate']=false;
                //Reset Form
                this.messages = this._Func.Messages('success', this._Func.msg('VR109'));
                setTimeout(function(){
                    n._router.parent.navigateByUrl('/master-data/charge-code');
                }, 600);
            },
            err => {
                this.loading['AddUpdate']=false;
                this.showMessage('danger', this._Func.parseErrorMessageFromServer(err));
            },
            () => {}
        );
    }

    /*
     *
     * */
    addNew(data)
    {
        let n=this;
        this.loading['AddUpdate']=true;
        // Object.keys(this.chgCodeForm).forEach(function (controlName) {
        //     let control = this.chgCodeForm.controls[controlName];
        //     this.detectControlErr(control);
        // });
        this._chgCodeService.addNewChgCode(data).subscribe(
            data => {
                this.loading['AddUpdate'] = false;
                //Reset Form
                this.messages = this._Func.Messages('success', this._Func.msg('VR107'));
                setTimeout(function(){
                    n._router.parent.navigateByUrl('/master-data/charge-code');
                }, 600);
            },
            err => {
                this.loading['AddUpdate'] = false;
                this.showMessage('danger', this._Func.parseErrorMessageFromServer(err));
            },
            () => {}
        );
    }

    cancel()
    {
        let n = this;
        /*case 1: modify text*/
        // let warningPopup = new WarningBoxPopup();
        // warningPopup.text = "Are you sure to cancel";
        // this._boxPopupService.showWarningPopup(warningPopup)

        /*case 2: default*/
        this._boxPopupService.showWarningPopup()
            .then(function (dm) {
                if(dm)
                    n._router.navigateByUrl('master-data/charge-code');

            });
    }

    /*
     * obj: object
     * properties: keys of object need to trim
     * */
    _trimObjProperty(obj, properties)
    {
        properties.forEach(function(prop) {
            obj[prop] = obj[prop].trim();
        });
    }

    detectControlErr(control) {
        this._ErrMsgControlService._genErrMsgForControl(control);
    }

    private showMessage(msgStatus, msg) {
        // if(this.timeoutHideMessage) clearTimeout(this.timeoutHideMessage);
        this.messages = {
            'status': msgStatus,
            'txt': msg
        }
        // this.timeoutHideMessage = setTimeout(() => {
        //     this.messages = null;
        // }, 4000);
        jQuery(window).scrollTop(0);
    }
}
