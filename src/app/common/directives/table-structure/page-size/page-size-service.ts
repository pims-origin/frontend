
import {Injectable} from '@angular/core';
import {TableFunctions} from '../../../core/table-functions';

@Injectable()

export class PageSizeService {

    constructor(
        private _tableFunc: TableFunctions
    )
    {

    }

    /*
    ****************************************************************************
    */

    public pageResize(data)
    {
        let tableComponent = data.tableComponent,
            perPage = data.hasOwnProperty('perPage') ? data.perPage : 'perPage',
            tablePagination = data.hasOwnProperty('tablePagination') ?
                data.tablePagination : 'Pagination';

        let pagination = tableComponent[tablePagination];

        let rows = (pagination['current_page'] - 1) * tableComponent[perPage];

        let page = rows >= pagination['total'] ? 1 : pagination['current_page'];

        let params = {
            parentComponent: tableComponent,
            page: page
        };

        if (data.hasOwnProperty('selectTable')) {
            params['selectTable'] = data.selectTable;
        };

        this._tableFunc.callGetTableData(params);

        return page;
    }

    /*
    ****************************************************************************
    */


}