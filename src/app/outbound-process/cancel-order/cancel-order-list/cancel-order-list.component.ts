import {Component,Input} from '@angular/core';
import {OrderListComponent} from '../../order/order-list/order-list.component';
@Component ({
  selector: 'cancel-order-list',
  directives: [OrderListComponent],
  templateUrl: 'cancel-order-list.component.html',
})

export class CancelOrderListComponent {

  constructor(){}
}
