
import {Component} from '@angular/core';
import {API_Config} from '../../common/core/API_Config';
import {BaseTable} from '../../common/core/classes_us/base-table';
import {TableFunctions} from '../../common/core/table-functions';
import {DateFunctions} from '../../common/core/functions/date-functions';
import {BaseService} from '../../common/core/classes_us/base-service';
import {TableStructure} from '../../common/directives/directives';

@Component ({
    selector: 'warehouse-locations',
    directives: [
        TableStructure
    ],
    templateUrl: '../../common/directives/table-structure/table.component.html',
    providers: [
        BaseService,
        TableFunctions,
        DateFunctions
    ]
})

export class WarehouseLocationsComponent extends BaseTable
{
    public tableID = 'warehouses-locations';
    public url = this._api.API_WAREHOUSE_LOCATIONS_REPORTS;
    public headerUrlSuffix = 'rep';
    public title = 'Search Warehouse Locations';
    public permissionPage = 'viewReport';

    public headerDef = [
        {
            id: 'ver_table',
            value: 1 + Math.random() * 10000
        },
        this.columnList.location,
        this.columnList.locationAlternativeName,
        this.columnList.warehouse,
        this.columnList.zoneName,
        this.columnList.locationType,
        this.columnList.locationStatus,
        this.columnList.locationAvailableCapacity,
        this.columnList.locationLength,
        this.columnList.locationWidth,
        this.columnList.locationHeight,
        this.columnList.locationMaxWeight,
        this.columnList.locationWeightCapacity,
        this.columnList.locationMinCount,
        this.columnList.locationMaxCount
    ];

    public searches = [
        this.searchList.location
    ];

    /*
    ****************************************************************************
    */

    constructor (
        public _api: API_Config,
        public _tableFunc: TableFunctions,
        public _tableService: BaseService
    )
    {
        super();

        this._tableFunc.permissionCall(this);
    }

    //**************************************************************************

}
