import {Component, Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {Functions, API_Config} from '../common/core/load';

@Injectable()

export class PutAwayServices {

    private headerGet = this._func.AuthHeader();
    private headerPostJson = this._func.AuthHeaderPostJson();
    private urlAPIContainer = this._api.API_Container;
    private urlAPIItem = this._api.API_ItemMaster;
    private urlAPIMeasurements = this._api.API_Measurements;
    private urlAPICustomer = this._api.API_Customer;
    private urlAPIAsn = this._api.API_Asns;
    private urlAPIUoms = this._api.API_UOM;
    private urlAPIGoodsReceipt = this._api.API_Goods_Receipt;
    private urlAPIUsers = this._api.API_Users;
    private urlAPICustomerMaster = this._api.API_CUSTOMER_MASTER;
    private urlAPIPutAway = this._api.API_Put_Away;
    private urlAPIUserMetas = this._api.API_User_Metas;

    constructor(
        private _func: Functions,
        private _api: API_Config,
        private _http: Http) {

    }

    public getAsnByIdCtnId ($id, $ctn_id) {

        return  this._http.get(this.urlAPIAsn + "/" + $id + '/containers/' + $ctn_id, {headers: this.headerGet})
            .map(res => res.json());
    }

    public getUserMeta ($params = '/pa') {

        return  this._http.get(this.urlAPIUserMetas + $params, {headers: this.headerGet})
            .map(res => res.json());
    }

    getListCRSByCustomer($params, $whs_id = '') {
        return  this._http.get(this._api.API_Authen + '/users/get-putter/' + $params + $whs_id, {headers: this.headerGet})
            .map(res => res.json());
    }

    assignPutter(stringData) {
        return this._http.put(this._api.API_Put_Away+'/assign-putter', stringData, { headers: this.headerPostJson })
            .map((res: Response) => res.json().data);
    }

    // Put sort table
    public putUserMeta (params, $s_link = "/pa") {

        return this._http.put(this.urlAPIUserMetas + $s_link, params , {headers: this.headerPostJson})
            .map(res => res.json())

    }

    // Save ASN
    public saveAsn(params) {

        return this._http.post(this.urlAPIAsn, params , {headers: this.headerPostJson})
            .map(res => res.json())

    }

    // Edit ASN
    public editAsn($idAsn, params) {

        return this._http.put(this.urlAPIAsn + "/" + $idAsn, params , {headers: this.headerPostJson})
            .map(res => res.json())

    }

    // Edit ASN for new containers
    public editAsnNewCtns($idAsn, params, $idCtn = 0) {

        return this._http.put(this.urlAPIAsn + "/" + $idAsn + "/containers/" + $idCtn, params , {headers: this.headerPostJson})
            .map(res => res.json())

    }

    public saveGoodReceipt(params) {

        return this._http.post(this.urlAPIGoodsReceipt, params , {headers: this.headerPostJson})
            .map(res => res.json())
    }

    // Get customers
    public getCustomers ($params = '') {

        return  this._http.get(this.urlAPICustomer + $params, {headers: this.headerGet})
            .map(res => res.json());

    }

    // Get customers
    public getCustomersByUser ($user_id, $params = '') {

        return  this._http.get(this.urlAPIUsers + '/info/' + $user_id + $params, {headers: this.headerGet})
            .map(res => res.json());

    }

    // Get customers by WH
    public getCustomersByWH ($params) {

        return  this._http.get(this.urlAPICustomerMaster + '/customer-warehouses' + $params + '&sort[cus_name]=asc' , {headers: this.headerGet})
            .map(res => res.json());

    }

    // Get uoms
    public getUoms () {

        return  this._http.get(this.urlAPIUoms, {headers: this.headerGet})
            .map(res => res.json());

    }

    // Get measurements
    public getMeasurements () {

        return  this._http.get(this.urlAPIMeasurements, {headers: this.headerGet})
            .map(res => res.json());

    }

    // Get containers
    public getContainersByASN (asnId) {

        return this._http.get(this.urlAPIAsn + '/' + asnId + '/list-containers', {headers: this.headerGet})
            .map(res => res.json());

    }

    // Autocomplete for search
    public getItemBySKU (sku, cus_id = '') {

        return this._http.get(this.urlAPIItem +  '?sku=' + sku + '&cus_id=' + cus_id, {headers: this.headerGet})
            .map(res => res.json().data);


    }

    // Autocomplete for search by name
    public getContainerByName ($params) {

        return this._http.get(this.urlAPIContainer + $params, {headers: this.headerGet})
            .map(res => res.json().data);


    }

    // Get container
    public getContainer ($params = '') {

        return this._http.get(this.urlAPIContainer +  $params, {headers: this.headerGet})
            .map(res => res.json());

    }

    // Get list asn
    public getListAsn ($params = '') {

        return this._http.get(this.urlAPIAsn +  $params, {headers: this.headerGet})
            .map(res => res.json());

    }

    // Get list asn
    public getListPutAway ($params = '') {

        return this._http.get(this.urlAPIPutAway +  $params, {headers: this.headerGet})
            .map(res => res.json());

    }

    public printListPutAway(goodReceiptID) {
        return this._http.get(this.urlAPIGoodsReceipt + '/' + goodReceiptID + '/print', {headers: this.headerGet})
            .map(res => res.json());

    }

    public getGoodsReceiptById ($id) {
        return this._http.get(this.urlAPIGoodsReceipt  + '/'  + $id , {headers: this.headerGet})
            .map(res => res.json())
    }

    public getGoodReceiptItems ($id) {
        return this._http.get(this.urlAPIPutAway  + '/itemupdate/' + $id , {headers: this.headerGet})
            .map(res => res.json())
    }

    public updateGoodReceiptItems($id, params) {
        return this._http.put(this.urlAPIPutAway + "/itemupdate/" + $id , params , {headers: this.headerPostJson})
            .map(res => res.json())
    }

    public searchLocation(whsId,param) {
        return this._http.get(this._api.API_Warehouse + "/" + whsId + "/locations" + param,{ headers: this.headerGet })
      .map((res: Response) => res.json());
    }
}
