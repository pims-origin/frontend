import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {API_Config, Functions} from '../common/core/load';

@Injectable()
export class MigrationService {
	private authHeader:any;
	private authHeaderPost:any;
	private whsId:string;

	constructor(private func:Functions, private api:API_Config, private http:Http) {
		this.authHeader = func.AuthHeader();
		this.whsId = localStorage.getItem('whs_id');
	}

	getCustomersByWH() {
		return this.http.get(`${this.api.API_CUSTOMER_MASTER}/customer-warehouses?whs_id=${this.whsId}&&limit=10000&sort[cus_name]=asc`,
			{headers: this.authHeader}).map(res => res.json());
	}
}
