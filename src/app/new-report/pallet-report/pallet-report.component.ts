import {Component} from "@angular/core";
import {Router, ROUTER_DIRECTIVES} from '@angular/router-deprecated';
import {ControlGroup, FormBuilder, Control} from '@angular/common';
import {API_Config, UserService, Functions} from '../../common/core/load';
import {BoxPopupService} from "../../common/popup/box-popup.service";
import {PalletReportService} from './pallet-report-service';
import {WMSPagination, AgGridExtent, WMSMessages, ReportFileExporter} from '../../common/directives/directives';
import 'ag-grid-enterprise/main';

declare const $:any;

@Component({
	selector: 'pallet-report',
	providers: [PalletReportService, BoxPopupService],
	directives: [ROUTER_DIRECTIVES, AgGridExtent, WMSMessages, WMSPagination, ReportFileExporter],
	templateUrl: 'pallet-report.component.html',

})
export class PLReportComponent {
	private pagination:any;
	private perPage:number = 20;
	private whs_id:string;
	private messages:any;
	private searchForm:ControlGroup;
	private sortData = {fieldname: 'updated_at', sort: 'desc'};
	private showLoadingOverlay:boolean = false;
	private grReports:Array<any> = [];
	private headerURL:string = this.apiService.API_User_Metas + '/plr';
	private columnData:any = {
		cus_code: {
			title: 'Customer Code',
			width: 120,
			pin: true,
			sort: true,
			ver_table: '15/2/19'
		},
		cus_name: {
			title: 'Customer Name',
			width: 120,
			pin: true,
			sort: true
		},
		plt_num: {
			title: 'Pallet RFID',
			width: 170,
			pin: true,
			id: true,
			sort: true
		},
		plt_sts_name: {
			title: 'Pallet Status',
			width: 100,
			pin: true,
			id: true,
			sort: true
		},
		loc_code: {
			title: 'Location',
			width: 50,
			pin: true,
			sort: true
		},
		loc_sts_name: {
			title: 'Location Status',
			width: 100,
			pin: true,
			sort: true
		},
		sku: {
			title: 'SKU',
			width: 50,
			pin: true,
			sort: true
		},
		size: {
			title: 'Size',
			width: 50,
			sort: true
		},
		color: {
			title: 'Color',
			width: 50,
			sort: true
		},
		ctn_pack_size: {
			title: 'Pack size',
			width: 50,
			sort: true
		},
		lot: {
			title: 'Lot',
			width: 50,
			sort: true
		},
		des: {
			title: 'Description',
			width: 50,
			sort: true
		},
		upc: {
			title: 'UPC',
			width: 50,
			sort: true
		},
		current_ctns: {
			title: 'CTNS',
			width: 70,
			sort: true
		},
		level: {
			title: 'Level',
			width: 70,
			sort: true
		},
		row: {
			title: 'Row',
			width: 70,
			sort: true
		},
		aisle: {
			title: 'Aisle',
			width: 70,
			sort: true
		},
		current_pieces: {
			title: 'QTY',
			width: 70,
			sort: true
		},
		dmg_ctns: {
			title: 'Damaged CTNS',
			width: 70,
			sort: true
		},
		created_at: {
			title: 'Created At',
			width: 100,
			sort: true
		}
	};
	private customers: any[] = [];
	private ctnrNums: any[] = [];
	private refCodes: any[] = [];
	private grNums: any[] = [];
	private SKUs: any[] = [];
	public isLoading: any[] = [];
	private currentSKU:any = {};

	// Check permission for user using this function page
	private canAccess:boolean = false;

	// Export csv file
	private exportAPIUrl:string = '';
	private fileName:string = 'Pallet-Report.csv';

	constructor(private formBuilder:FormBuilder,
	            private _service:PalletReportService,
	            private func:Functions,
	            private userService:UserService,
	            private apiService:API_Config,
	            private router:Router) {
		this.buildSearchForm();
		this.whs_id = localStorage.getItem('whs_id');
		this.checkPermission();
		this.initializeCurSKU();
		this.initShowCartonList();
	}

	buildSearchForm() {
		this.searchForm = this.formBuilder.group({
			cus_id: [''],
			gr_hdr_num: [''],
			on_rack: [''],
			rfid: [''],
			loc_code: [''],
			sku: [''],
			size: [''],
			color: [''],
			level: [''],
			row: [''],
			aisle: [''],
		});
	}

	initializeCurSKU() {
		this.currentSKU = {size: '', color: '', lot: '', item_id: ''};
	}

	private checkPermission() {
		this.showLoadingOverlay = true;
		this.userService.GetPermissionUser().subscribe(
			data => {
				this.showLoadingOverlay = false;
				this.canAccess = this.userService.RequestPermission(data, 'viewReport');
				if (this.canAccess) {
					this.getCustomers();
					this.getGRReports();
				} else {
					// redirect to deny page.
					this.router.parent.navigateByUrl('/deny');
				}
			},
			err => {
				this.messages = this.func.Messages('danger', this.func.parseErrorMessageFromServer(err));
				this.showLoadingOverlay = false;
			}
		);
	}

	private changePageSize($event:any) {
		this.perPage = $event.target.value;
		if (this.pagination) {
			if ((this.pagination['current_page'] - 1) * this.perPage >= this.pagination['total']) {
				this.pagination['current_page'] = 1;
			}
			this.getGRReports(this.pagination['current_page']);
		}

	}

	private getGRReports(pageNum:number = 1) {
		const sortField = this.sortData['fieldname'] ? this.sortData['fieldname'] : 'updated_at';
		const sortValue = this.sortData['sort'] ? this.sortData['sort'] : 'desc';
		this.searchForm.value['page'] = pageNum;
		this.searchForm.value['limit'] = this.perPage;
		this.searchForm.value['created_at_from'] = $("#created_at_from").val();3
		this.searchForm.value['created_at_to'] = $("#created_at_to").val();
		this.searchForm.value['lot'] = this.currentSKU['lot'];
		this.searchForm.value['item_id '] = this.currentSKU['item_id'];

		const params = '?' + $.param(this.searchForm.value) + '&sort['+sortField+']=' + sortValue;
		this.exportAPIUrl = this._service.getReportAPIUrl(params);

		this.showLoadingOverlay = true;
		this._service.getReports(params)
			.subscribe(
				data => {
					this.showLoadingOverlay = false;
					this.grReports = this.renderLink(data.data);

					// paging
					this.pagination = data['meta']['pagination'];
					this.pagination['numLinks'] = 3;
					this.pagination['tmpPageCount'] = this.func.Pagination(this.pagination);
				},
				err => {
					this.messages = this.func.Messages('danger', err.json().errors.message);
					this.showLoadingOverlay = false;
				},
				() => {}
			);
	}

	private getCustomers() {
		const params:string = '?limit=2000';
		this._service.getCustomers(params).subscribe(
			data => {
				this.customers = data['data'];
			},
			err => {
				this.messages = this.func.Messages('danger', err.json().errors.message);
			},
			() => {
			}
		);
	}

	loadRefCodes($event:any) {
		const cusId = this.searchForm.value['cus_id'];
		const ctnrNum = this.searchForm.value['ctnr_num'];
		const refCodesKeySearch = this.searchForm.value['ref_code'].trim();
		const params = `?cus_id=${cusId}&ctnr_num=${ctnrNum}&ref_code=${refCodesKeySearch}&limit=20`;
		const enCodeSearchQuery = this.func.FixEncodeURI(params);

		if (!refCodesKeySearch) {
			return;
		}
		this.isLoading['RefCodes'] = true;
		this._service.getRefCodes(enCodeSearchQuery).debounceTime(400).distinctUntilChanged().subscribe(
			data => {
				this.isLoading['RefCodes'] = false;
				this.refCodes = data['data'];
			},
			err => {
				this.isLoading['RefCodes'] = false;
				this.messages = this.func.Messages('danger', err.json().errors.message);
			},
			() => {
			}
		);
	}

	loadGRNums($event:any) {
		const cusId = this.searchForm.value['cus_id'];
		const ctnrNum = this.searchForm.value['ctnr_num'];
		const refCode = this.searchForm.value['ref_code'];
		const grNumsKeySearch = this.searchForm.value['gr_hdr_num'].trim();
		const params = `?cus_id=${cusId}&gr_hdr_num=${grNumsKeySearch}&limit=20`;
		const enCodeSearchQuery = this.func.FixEncodeURI(params);

		if (!grNumsKeySearch) {
			return;
		}
		this.isLoading['GRNums'] = true;
		this._service.getGRNums(enCodeSearchQuery).debounceTime(400).distinctUntilChanged().subscribe(
			data => {
				this.isLoading['GRNums'] = false;
				this.grNums = data['data'];
			},
			err => {
				this.isLoading['GRNums'] = false;
				this.messages = this.func.Messages('danger', err.json().errors.message);
			},
			() => {
			}
		);
	}

	loadSKUs($event:any) {
		this.initializeCurSKU();
		if (!this.searchForm.value['sku']) {
			return;
		}

		let querySKUsObj = Object.assign({}, this.searchForm.value);
		querySKUsObj['limit'] = 20;

		this.isLoading['SKUs'] = true;
		this._service.getSKUs('?' + $.param(querySKUsObj)).debounceTime(400).distinctUntilChanged().subscribe(
			data => {
				this.isLoading['SKUs'] = false;
				this.SKUs = data['data'];
			},
			err => {
				this.isLoading['SKUs'] = false;
				this.messages = this.func.Messages('danger', err.json().errors.message);
			},
			() => {
			}
		);
	}

	reset() {
		this.resetSearchForm();
		this.initializeCurSKU();
		this.getGRReports();
	}

	search() {
		this.getGRReports();
	}

	selectCTNRNum($event, ctnrNum) {
		(<Control>this.searchForm.controls['ctnr_num']).updateValue(ctnrNum['ctnr_num']);
	}

	selectRefCode($event, refCode) {
		(<Control>this.searchForm.controls['ref_code']).updateValue(refCode['ref_code']);
	}

	selectGRNum($event, grNum) {
		(<Control>this.searchForm.controls['gr_hdr_num']).updateValue(grNum['gr_hdr_num']);
	}

	selectSKU($event, sku) {
		this.currentSKU = sku;
		(<Control>this.searchForm.controls['sku']).updateValue(sku['sku']);
		(<Control>this.searchForm.controls['size']).updateValue(sku['size']);
		(<Control>this.searchForm.controls['color']).updateValue(sku['color']);
	}

	resetSearchForm() {

		this.func.ResetForm(this.searchForm);

		$("#created_at_from").val("");
		$("#created_at_to").val("");
	}

	private updateMessages(messages) {
		this.messages = messages;
	}
	
	private renderLink(arr:Array<any>=[]){
		
		arr.forEach((item)=>{
			
			item['current_ctns'] = `<a data-pallet="${item['plt_id']}" data-item-id="${item['item_id']}" data-lot="${item['lot']}" data-pack-size="${item['ctn_pack_size']}">${item['current_ctns']}</a>`;
			
		});
		
		return arr;
		
	}
	
	private cartonListPopup = [];
	private selectedPalletId;
	private selectedItemID;
	private selectedLot;
	private selectedPackSize;
	
	// carton list pagination
	private cartonPerPage = 20;
	private cartonPagination;
	
	private initShowCartonList() {
		var that = this;
		$('body').off('click.showCartonList').on('click.showCartonList', '[data-pallet]', function() {
			var elm = $(this);
			that.showLoadingOverlay = true;
			that.selectedPalletId = elm.data('pallet');
			that.selectedItemID = elm.data('item-id');
			that.selectedLot = elm.data('lot');
			that.selectedPackSize = elm.data('pack-size');
			that.getCartonsList();
		});
	}
	
	private getCartonsList(pageNum:number = 1) {
		var params = `?sort[item_id]=asc&page=${pageNum}&limit=20&item_id=${this.selectedItemID}&lot=${this.selectedLot}&ctn_pack_size=${this.selectedPackSize}`;

		this.showLoadingOverlay = true;
		this._service.getCartonsList(this.selectedPalletId, params)
			.subscribe(
				data => {
					this.showLoadingOverlay = false;
					this.cartonListPopup = data.data;
					
					// paging
					this.cartonPagination = data['meta']['pagination'];
					this.cartonPagination['numLinks'] = 3;
					this.cartonPagination['tmpPageCount'] = this.func.Pagination(this.cartonPagination);
					
					$('#carton-list-popup').modal('show');
				},
				err => {
					this.messages = this.func.Messages('danger', err.json().errors.message);
					this.showLoadingOverlay = false;
				},
				() => {}
			);
	}	

	// for Autocomplete levels
	private levels: any[] = [];
	
	loadLevels($event:any) {
		const level = this.searchForm.value['level'].trim();
		const params = `/loc-level?page=1&limit=20&level=${level}`;
		const enCodeSearchQuery = this.func.FixEncodeURI(params);

		if (!level) {
			return;
		}
		this.isLoading['level'] = true;
		this._service.getAutocompleteLocation(enCodeSearchQuery).debounceTime(400).distinctUntilChanged().subscribe(
			data => {
				this.isLoading['level'] = false;
				this.levels = data['data'];
			},
			err => {
				this.isLoading['level'] = false;
				this.messages = this.func.Messages('danger', err.json().errors.message);
			},
			() => {
			}
		);
	}
	
	selectLevel($event, item) {
		(<Control>this.searchForm.controls['level']).updateValue(item['level']);
	}
	// *************************************************************************************
	// for Autocomplete rows
	private rows: any[] = [];
	
	loadRows($event:any) {
		const row = this.searchForm.value['row'].trim();
		const params = `/loc-row?page=1&limit=20&row=${row}`;
		const enCodeSearchQuery = this.func.FixEncodeURI(params);

		if (!row) {
			return;
		}
		this.isLoading['row'] = true;
		this._service.getAutocompleteLocation(enCodeSearchQuery).debounceTime(400).distinctUntilChanged().subscribe(
			data => {
				this.isLoading['row'] = false;
				this.rows = data['data'];
			},
			err => {
				this.isLoading['row'] = false;
				this.messages = this.func.Messages('danger', err.json().errors.message);
			},
			() => {
			}
		);
	}
	
	selectRow($event, item) {
		(<Control>this.searchForm.controls['row']).updateValue(item['row']);
	}
	// *************************************************************************************
	// for Autocomplete aisles
	private aisles: any[] = [];
	
	loadAisles($event:any) {
		const aisle = this.searchForm.value['aisle'].trim();
		const params = `/loc-aisle?page=1&limit=20&aisle=${aisle}`;
		const enCodeSearchQuery = this.func.FixEncodeURI(params);

		if (!aisle) {
			return;
		}
		this.isLoading['aisle'] = true;
		this._service.getAutocompleteLocation(enCodeSearchQuery).debounceTime(400).distinctUntilChanged().subscribe(
			data => {
				this.isLoading['aisle'] = false;
				this.aisles = data['data'];
			},
			err => {
				this.isLoading['aisle'] = false;
				this.messages = this.func.Messages('danger', err.json().errors.message);
			},
			() => {
			}
		);
	}
	
	selectAisle($event, item) {
		(<Control>this.searchForm.controls['aisle']).updateValue(item['aisle']);
	}
	// *************************************************************************************
}
