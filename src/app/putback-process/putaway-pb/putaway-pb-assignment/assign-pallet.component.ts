import {Component} from "@angular/core";
import {FORM_DIRECTIVES, ControlGroup, FormBuilder, Control, ControlArray, Validators} from '@angular/common';
import {Router, RouteParams} from "@angular/router-deprecated";
import {BoxPopupService} from "../../../common/popup/box-popup.service";
import {WarningBoxPopup} from "../../../common/popup/warning-box-popup";
import {ValidationService} from "../../../common/core/validator";
import {WMSMessages} from "../../../common/directives/messages/messages";
import {UserService, Functions, FormBuilderFunctions} from '../../../common/core/load';
import {PaPbServices} from '../pa-pb.service';
import {API_Config} from "../../../common/core/load";

declare var saveAs:any;
declare var jQuery:any;

@Component({
	selector: 'assign-pallet',
	providers: [FormBuilder, BoxPopupService, UserService, PaPbServices],
	directives: [WMSMessages],
	templateUrl: 'assign-pallet.component.html',
})

export class AssignPalletComponent {
	private canAccess:boolean = false;
	private returnId:string = '';
	private messages:any;
	private showLoadingOverlay:boolean = false;
	private SKUs:Array<any> = [];
	private palletForm:ControlGroup;
	private items:ControlGroup[] = [];
	private itemsArray:ControlArray = new ControlArray(this.items);
    private pickedTotal:number = 0;
    private remainQTY:number = 0;
    private submitted:boolean = false;

	constructor(private router:Router,
	            private routerParams:RouteParams,
	            private formBuilder:FormBuilder,
	            private fbdFuncs:FormBuilderFunctions,
	            private funcs:Functions,
	            private boxPopupService:BoxPopupService,
	            private validationService:ValidationService,
	            private userService:UserService,
                private _api: API_Config,
	            private paPBServices:PaPbServices) {
		this.returnId = this.routerParams.get('id');
		this.checkPermission();
	}

	private checkPermission() {
		this.showLoadingOverlay = true;
		this.userService.GetPermissionUser().subscribe(
			data => {
				this.showLoadingOverlay = false;
				const isPermitted = this.userService.RequestPermission(data, 'assignPalletPutback');

				if (!isPermitted) {
					this.router.parent.navigateByUrl('/deny');
				} else {
					this.canAccess = true;
					this.buildPalletForm();
					this.getIems();
				}
			},
			err => {
				this.showLoadingOverlay = false;
				this.parseServerError(err);
			}
		)
	}

	private getIems() {
		this.showLoadingOverlay = true;
		this.paPBServices.getItems(this.returnId).subscribe(
			data => {
				this.showLoadingOverlay = false;
                this.remainQTY = this.pickedTotal = data['picked_ttl'];
				this.SKUs = data['data'];
			},
			err => {
				this.showLoadingOverlay = false;
				this.parseServerError(err);
			}
		)
	}

	private buildPalletForm() {
		this.palletForm = this.formBuilder.group({
			print: ['1'],
			items: this.itemsArray
		});

		// init the first row for itemsArray
		this.addItem();

        this.itemsArray.valueChanges.subscribe(data=>{
	        this.checkDuplicatedPalletId(this.itemsArray,['plt_rfid']);
        });
	}

	public checkDuplicatedPalletId(controlArray:ControlArray,keys){
		this.fbdFuncs.removeErrorsByFieldName(controlArray,keys,'duplicated');

		let Arr=controlArray.value;
		if (!this.isExistingKGUOM(Arr)) {
			return;
		}

		for(let i=0;i<Arr.length;i++) {
			let item = this.fbdFuncs._createItemToCheckDuplicated(Arr[i],keys);
			for(let j=i+1;j<Arr.length;j++) {
				let itemj=this.fbdFuncs._createItemToCheckDuplicated(Arr[j],keys);
				if(i==j) {continue;}
				if((item&&itemj)&&item==itemj) {
					this.fbdFuncs.setDuplicateItem(controlArray,i,j,keys);
				}
			}
		}
	}

	private isExistingKGUOM(items) {
		for (const item of items) {
			if (item['uom_name'].toUpperCase() === 'KG' && item['plt_rfid']) {
				return true;
			}
		}
		return false;
	}

	private addItem() {
		this.itemsArray.push(
			new ControlGroup({
				'item_id': new Control(''),
				'sku': new Control('', Validators.compose([Validators.required])),
				'size': new Control(''),
                'color': new Control(''),
                'des': new Control(''),
				'lot': new Control(''),
				'pack_size': new Control(''),
				'qty_kg': new Control('', Validators.compose([Validators.required,this.validationService.inPutOnlyNumber, this.validationService.isZero])),
				'ctns': new Control('', Validators.compose([Validators.required,this.validationService.inPutOnlyNumber, this.validationService.validateInt, this.validationService.isZero])),
				'plt_rfid': new Control('', Validators.compose([Validators.required])),
				'odr_hdr_id': new Control(''),
				'odr_dtl_id': new Control(''),
				'new_sku': new Control(''),
                'picked_qty': new Control(''),
                'picked_ctns': new Control(''),
				'uom_code': new Control(''),
				'uom_name': new Control('')
			})
		);
	}

    delItem() {
        if (this.fbdFuncs.getItemchecked(this.itemsArray) > 0) {
            let warningPopup = new WarningBoxPopup();
            warningPopup.text = this.funcs.msg('PBAP002');
            this.boxPopupService.showWarningPopup(warningPopup)
                .then((dm) => {
                    if (!dm) {
                        return;
                    } else {
                        this.fbdFuncs.deleteItem(this.itemsArray).subscribe(deletedAll=> {
                            if (deletedAll) {
                                this.addItem();
                            }
                            this.calculateRemainQTY();
                        });
                    }
                });
        }
        else {
            this.messages = this.funcs.Messages('danger', this.funcs.msg('VR113'));
        }
    }

    private submit() {
        this.submitted = true;

        if (this.palletForm.valid) {
            if (this.remainQTY === 0) {
                const palletJson = JSON.stringify(this.palletForm.value);
                this.assignToPallet(this.returnId, palletJson);
            } else {
                this.messages = this.funcs.Messages('danger', 'Remaining QTY must be equal 0. Please assign all items and cartons to the pallet.');
            }
        }
    }

    private chooseSKU(itemIndex, target, control:ControlGroup) {
        if (!target.value) {
            (<Control>this.palletForm.controls['items']['controls'][itemIndex]['controls']['item_id']).updateValue('');
            (<Control>this.palletForm.controls['items']['controls'][itemIndex]['controls']['size']).updateValue('');
            (<Control>this.palletForm.controls['items']['controls'][itemIndex]['controls']['color']).updateValue('');
            (<Control>this.palletForm.controls['items']['controls'][itemIndex]['controls']['des']).updateValue('');
            (<Control>this.palletForm.controls['items']['controls'][itemIndex]['controls']['lot']).updateValue('');
            (<Control>this.palletForm.controls['items']['controls'][itemIndex]['controls']['pack']).updateValue('');
            (<Control>this.palletForm.controls['items']['controls'][itemIndex]['controls']['odr_hdr_id']).updateValue('');
            (<Control>this.palletForm.controls['items']['controls'][itemIndex]['controls']['odr_dtl_id']).updateValue('');
            (<Control>this.palletForm.controls['items']['controls'][itemIndex]['controls']['new_sku']).updateValue('');
            (<Control>this.palletForm.controls['items']['controls'][itemIndex]['controls']['picked_qty']).updateValue('');
            (<Control>this.palletForm.controls['items']['controls'][itemIndex]['controls']['picked_ctns']).updateValue('');
            (<Control>this.palletForm.controls['items']['controls'][itemIndex]['controls']['uom_code']).updateValue('');
            (<Control>this.palletForm.controls['items']['controls'][itemIndex]['controls']['uom_name']).updateValue('');
            (<Control>this.palletForm.controls['items']['controls'][itemIndex]['controls']['qty_kg']).updateValue('');
            (<Control>this.palletForm.controls['items']['controls'][itemIndex]['controls']['ctns']).updateValue('');
            (<Control>this.palletForm.controls['items']['controls'][itemIndex]['controls']['plt_rfid']).updateValue('');
        } else {
            const sku = this.SKUs[target.selectedIndex - 1];
            (<Control>this.palletForm.controls['items']['controls'][itemIndex]['controls']['item_id']).updateValue(sku.item_id);
            (<Control>this.palletForm.controls['items']['controls'][itemIndex]['controls']['size']).updateValue(sku.size);
            (<Control>this.palletForm.controls['items']['controls'][itemIndex]['controls']['color']).updateValue(sku.color);
            (<Control>this.palletForm.controls['items']['controls'][itemIndex]['controls']['des']).updateValue(sku.des);
            (<Control>this.palletForm.controls['items']['controls'][itemIndex]['controls']['lot']).updateValue(sku.lot);
            (<Control>this.palletForm.controls['items']['controls'][itemIndex]['controls']['pack_size']).updateValue(sku.pack);
            (<Control>this.palletForm.controls['items']['controls'][itemIndex]['controls']['odr_hdr_id']).updateValue(sku.odr_hdr_id);
            (<Control>this.palletForm.controls['items']['controls'][itemIndex]['controls']['odr_dtl_id']).updateValue(sku.odr_dtl_id);
            (<Control>this.palletForm.controls['items']['controls'][itemIndex]['controls']['new_sku']).updateValue(sku.new_sku);
            (<Control>this.palletForm.controls['items']['controls'][itemIndex]['controls']['picked_qty']).updateValue(sku.picked_qty);
            (<Control>this.palletForm.controls['items']['controls'][itemIndex]['controls']['picked_ctns']).updateValue(sku.picked_ctns);
            (<Control>this.palletForm.controls['items']['controls'][itemIndex]['controls']['uom_code']).updateValue(sku.uom_code);
            (<Control>this.palletForm.controls['items']['controls'][itemIndex]['controls']['uom_name']).updateValue(sku.uom_name != 'KG'? 'CARTON': sku.uom_name);
	        (<Control>this.palletForm.controls['items']['controls'][itemIndex]['controls']['qty_kg']).updateValue('');
	        (<Control>this.palletForm.controls['items']['controls'][itemIndex]['controls']['ctns']).updateValue('');
            (<Control>this.palletForm.controls['items']['controls'][itemIndex]['controls']['plt_rfid']).updateValue('');
            (<Control>this.palletForm.controls['items']['controls'][itemIndex]['controls']['qty_kg'])['disabled'] = sku.new_sku == 1;
        }
    }

    private changeCTNSValue(control:ControlGroup) {
        let ctnsValue = control.value['ctns'];
        let uomName = control.value['uom_name'];

	    if (uomName.toUpperCase() !== 'CARTON') {
		    return;
	    }
        if (!ctnsValue || isNaN(ctnsValue)) {
            return;
        }

        const pieceQTYValue = parseInt(ctnsValue) * parseInt(control.value['pack_size']);
        if(!control.value['new_sku'] && pieceQTYValue > parseInt(control.value['picked_qty'])) {
            // set error
            control.controls['ctns'].setErrors({'invalidCTNS':true});
        } else {
            (<Control>control.controls['qty_kg']).updateValue(pieceQTYValue);
        }
        this.calculateRemainQTY();
    }

	private changePieceQTYValue(indexItem, control:ControlGroup) {
		let pieceQTYValue = control.value['qty_kg'];
		let uomName = control.value['uom_name'];
        const isCartonUOM = uomName.toUpperCase() === 'CARTON';
        const isKG = uomName.toUpperCase() === 'KG';

		if (!pieceQTYValue || isNaN(pieceQTYValue)) {
			return;
		}

		pieceQTYValue = Number(pieceQTYValue);
		if (isCartonUOM && !Number.isInteger(pieceQTYValue)) {
			control.controls['qty_kg'].setErrors({'invalidCartonUOMPieceQTY': true});
			return;
		}

		if (!control.value['new_sku'] && pieceQTYValue > Number(control.value['picked_qty'])) {
			// set error
			control.controls['qty_kg'].setErrors({'invalidPieceQTY': true});
			return;
		}

		if (isCartonUOM) {
			const ctnsValue = Math.ceil(Number(pieceQTYValue) / Number(control.value['pack_size']));
			(<Control>control.controls['ctns']).updateValue(ctnsValue);
        }

        if (isKG) {
            let validDecimal = this.validationService.isDecimalNumber((<Control>control.controls['qty_kg']));
            if(validDecimal) {
                control.controls['qty_kg'].setErrors(validDecimal);
                return;
            }
        }
        
		this.calculateRemainQTY();
	}

    private changePalletID(control:ControlGroup) {
        const palletIDValue = control.value['plt_rfid'];
        if (!palletIDValue) {
            return;
        }

        this.paPBServices.checkPalletId(`?plt_rfid=${palletIDValue}`).subscribe(
            data => {},
            err => {
                control.controls['plt_rfid'].setErrors({'invalidPalletID': true});
                control.controls['plt_rfid']['error_api'] = err.json()['errors']['message'];

            }
        )
    }

    private calculateRemainQTY(){
        this.remainQTY = this.pickedTotal;
        setTimeout(()=>{
            this.items.forEach((control:ControlGroup)=>{
                this.remainQTY -= Number(control.value['qty_kg'] || 0);
            });
			this.remainQTY = Number(this.remainQTY.toFixed(2));
        });


    }

	private parseServerError(err:any) {
		this.messages = {status: 'danger', txt: this.funcs.parseErrorMessageFromServer(err)};
	}

	private cancel() {
		const that = this;
		this.boxPopupService.showWarningPopup()
			.then(function (dm) {
				if (dm)
					that.router.parent.navigateByUrl('/putback-process/pb-receipt');
			});
	}
    
    private assignToPallet(returnId, palletJson) {
        let api = `${this._api.API_ORDERS_V2}/putback/pal-assign/${returnId}`;
        let that = this;
        try{
            that.showLoadingOverlay = true;
            let xhr = new XMLHttpRequest();
            //xhr.open("GET", api , true);
            xhr.open("POST", api, true);
            xhr.setRequestHeader("Authorization", 'Bearer ' + that.funcs.getToken());
	        xhr.setRequestHeader('Content-Type', 'application/json');
            xhr.responseType = 'blob';

            xhr.onreadystatechange = function () {
                if(xhr.readyState == 2) {
                    if(xhr.status == 200) {
                        xhr.responseType = "blob";
                    } else {
                        xhr.responseType = "text";
                    }
                }

                if(xhr.readyState === 4) {
                    if(xhr.status === 200) {
                        var fileName = `assign_pallet_${returnId}`;
                        var blob = new Blob([this.response], {type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'});
                        saveAs.saveAs(blob, fileName + '.pdf');
                        that.messages = that.funcs.Messages('success', that.funcs.msg('PBAP003'));
                        setTimeout(() => {
                            that.router.parent.navigateByUrl('/putback-process/putaway');
                        }, 600);
                    }else{
                        if(xhr.status === 201) {
                            var msg = '';
                            try {
                                msg = JSON.parse(xhr.response).message;
                            }
                            catch(err) {}

                            if(msg) {
                                let warningPopup = new WarningBoxPopup();
                                warningPopup.text = msg;
                                warningPopup.showCancelButton = false;
                                warningPopup.confirmButtonText = 'OK';
                                that.boxPopupService.showWarningPopup(warningPopup)
                                    .then(function (dm) {
                                        that.router.parent.navigateByUrl('/putback-process/putaway');
                                    });
                            }
                            else {
                                that.messages = that.funcs.Messages('success', that.funcs.msg('PBAP003'));
                                setTimeout(() => {
                                    that.router.parent.navigateByUrl('/putback-process/putaway');
                                }, 600);
                            }
                        }
                        else {
                            let errMsg = '';
                            if(xhr.response) {
                                try {
                                    var res = JSON.parse(xhr.response);
                                    errMsg = res.errors && res.errors.message ? res.errors.message : xhr.statusText;
                                }
                                catch(err){errMsg = xhr.statusText}
                            }
                            else {
                                errMsg = xhr.statusText;
                            }
                            if(!errMsg) {
                                errMsg = that.funcs.msg('VR100');
                            }
                            that.messages = {status: 'danger', txt: errMsg};
                        }
                    }
                    that.showLoadingOverlay = false;
                }
            }
            xhr.send(palletJson);
        } catch (err) {
            that.messages = {status: 'danger', txt: that.funcs.msg('VR100')};
            that.showLoadingOverlay = false;
        }
    }

	private validateFloatKeyPress(value, evt) {
		this.validationService.validateFloatKeyPress(value, evt);
    }
    
    private formatPalletID(val: string, control: ControlGroup, field: string) {
        this.fbdFuncs.setValueControl(control, field, val.toUpperCase());
    }
}