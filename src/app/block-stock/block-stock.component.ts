import {Component} from '@angular/core';
import {Router, RouterLink, RouteConfig, ROUTER_DIRECTIVES} from '@angular/router-deprecated';
import {WMSBreadcrumb} from "../common/directives/wms-breadcrumb/wms-breadcrumb";
import {CreateBlockStockComponent} from "./create-block-stock/create-block-stock.component";
import {BlockStockDetailComponent} from "./block-stock-detail/block-stock-detail.component";
import {CreateBlockStockReasonComponent} from "./create-block-stock-reason/create-block-stock-reason.component";

@Component ({
    selector: 'block-stock-management',
    directives: [
      ROUTER_DIRECTIVES,
      WMSBreadcrumb
    ],
    template: `<router-outlet></router-outlet>`,
})

@RouteConfig([
  {
    path: '/create',
    component: CreateBlockStockComponent,
    name: 'Create Block Stock',
    data:{
      action:'create'
    }
  },
    {
        path: '/:id',
        component: BlockStockDetailComponent,
        name: 'BlockStock Detail',
        data:{
            action:'view'
        }
    },
    {
        path: '/create-reason',
        component: CreateBlockStockReasonComponent,
        name: 'Create Block Stock Reason',
        data:{
            action:'createReason'
        }
    },
])

export class BlockStockManagementComponent {

}
