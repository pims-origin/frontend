import {Component, Input} from '@angular/core';
import {DecimalPipe} from '@angular/common';

import {WMSNumberFormatter} from '../../common/pipes/wms-number-formatter';
import {WMSPagination} from '../../common/directives/directives';
import {DashboardServices} from '../dashboard.service';
import {Functions} from '../../common/core/load';
import {PaginatePipe, PaginationControlsCmp} from '../../common/directives/directives';

declare let Chart:any;

@Component({
	selector: 'room-capacity-dashboard',
	templateUrl: 'room-capacity-dashboard.html',
	directives: [WMSPagination, PaginationControlsCmp],
	providers: [DashboardServices],
	pipes: [PaginatePipe, DecimalPipe, WMSNumberFormatter]
})

export class RoomCapacityDashboard {
	private whsId:string = '';

	@Input('currentWHSId') set updateWHSId(value:any) {
		this.whsId = value;
		if (value) {
			this.getRoomAdmin();
		}
	}

	constructor(private dashboardServices:DashboardServices, private funcs:Functions) {
	}

	ngAfterViewInit()
	{
		// this.getRoomAdmin();
	}

	private totalLocationRoom:any;
	private listDataRoomAdmin:any;
	private getRoomAdmin() {
		this.dashboardServices.getRoomAdmin().subscribe(
			data => {
				this.listDataRoomAdmin = data.data;
				this.totalLocationRoom = data['meta'];
				this.drawRoomCapaciesChart();
			},
			err => {

			}
		);
	}

	drawRoomCapaciesChart() {
		const chartColors = ['#36c6d3', 'rgb(0, 128, 0)'];
		let dataLabels = [];
		let dataUsed = [];
		let dataUnused = [];

		for(let room of this.listDataRoomAdmin) {
			dataLabels.push(room.room);
			dataUsed.push(room.used);
			dataUnused.push( parseInt(room.total) -  parseInt(room.used));
		}
		let barChartData = {
			labels: dataLabels,
			datasets: [{
				label: 'Used Location',
				backgroundColor: chartColors[0],
				data: dataUsed
			}, {
				label: 'Unused Location',
				backgroundColor: chartColors[1],
				data: dataUnused
			}]

		};

		let ctx = document.getElementById('room-chart-wrapper');
		if (ctx == null) {
			return true;
		}
		new Chart(ctx, {
			type: 'bar',
			data: barChartData,
			options: {
				title:{
					display:false,
					// text:""
				},
				tooltips: {
					mode: 'label',
					callbacks: {
						title: (tooltipItem, data) => {
							const item = this.listDataRoomAdmin[tooltipItem[0].index];
							return `${item.room}: ${this.funcs.formatSWISNumber(item.total)}`;
						},
						label: (tooltipItem, data) => {
							return data.datasets[tooltipItem.datasetIndex].label + ": " + this.funcs.formatSWISNumber(tooltipItem.yLabel);
						}
					},
				},
				responsive: true,
				scales: {
					xAxes: [{
						stacked: true,
					}],
					yAxes: [{
						stacked: true,
						ticks: {
							beginAtZero: true,
							callback: (value, index, values) => {
								if (value.toString().indexOf(".") !== -1) {
									return this.funcs.formatSWISNumber(value.toFixed(1));
								}
								return this.funcs.formatSWISNumber(value);
							}
						}

					}]
				}
			}
		});
	}
}
