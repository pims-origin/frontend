
import {Component, Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {API_Config, Functions} from '../core/load';

@Injectable()

export class CustomerService {

    private headerGet = this._Func.AuthHeader();
    private urlAPICustomerMaster = this._API.API_CUSTOMER_MASTER;

    constructor(
        private _Func: Functions,
        private _API: API_Config,
        private _http: Http
    )
    {

    }

    /*
    ****************************************************************************
    */

    public get()
    {
        return this._http.get(this._API.API_CUSTOMER_NAMES + '?sort[cus_name]=asc', {
            headers: this.headerGet
        }).map(res => res.json());
    }

    /*
    ****************************************************************************
    */

}

