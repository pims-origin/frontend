import {Component,Input, Output,EventEmitter} from '@angular/core';
import { FORM_DIRECTIVES,ControlGroup,Control,ControlArray,FormBuilder,Validators} from '@angular/common';
import {Router, RouteData, RouteParams } from '@angular/router-deprecated';
import {PalletServices} from "./pallet-service";
import { ValidationService } from '../../common/core/validator';
import {UserService, API_Config, Functions} from '../../common/core/load';
import {WMSPagination, FileExporter} from '../../common/directives/directives';
import {WarningBoxPopup} from "../../common/popup/warning-box-popup";
import {BoxPopupService} from "../../common/popup/box-popup.service";

declare var jQuery: any;
@Component ({
    selector: 'pallet-directive',
    directives:[FORM_DIRECTIVES,WMSPagination, FileExporter],
    providers: [PalletServices,ValidationService],
    templateUrl: 'pallet-template.html'
})

export class PalletDirective {

    private whs_id;
    private PalletDetail={data:[]};
    private gdId:any;
    private Pagination={};
    private DamagedCartonList:Array<any>=[];
    private DamagedCartonDetail={};
    private showLoadingOverlay:boolean=false;
    private perPage=20;
    private grStsCode: string;
    private grStsName: string;
    private reveivingStatus: string = '';
    private asnStsCode: string;
    public locations: any[] = [];

    @Input() createdFrom: string;

    @Input('grStatusCode') set setGrStatusCode(value: string) {
        if (value) {
            this.grStsCode = value;
            this.getData();
        }
    }
    @Input('grStatusName') set setGrStatusName(value: string) {
        if (value) {
            this.grStsName = value;
        }
    }
    @Input('asnStatus') set setASNStatusCode(value: string) {
        if (value) {
            this.asnStsCode = value;
        }
    }
    @Input() refNum: string;
    @Input() refCusID: string;
    @Input() hasPermissionUpdatePallet : boolean;
    @Output() messages = new EventEmitter();
    private headerExport = ['Good Receipt ID', 'Status', 'LPN', 'RFID', 'Putaway Location', 'Current Location', 'Item ID', 'SKU', 'Size', 'Color', 'Lot', 'UOM', 'Weight', 'Pack Size', 'Description', 'Init CTNS', 'Current CTNS', 'Init QTY', 'Current QTY', 'Expired Date'];
    private dataExport: any[];
    private fileName:string = 'GR-pallet-tab.csv';

    constructor(
        private fb: FormBuilder,
        private _Valid: ValidationService,
        private params: RouteParams,
        private _Func: Functions,
        private api_config: API_Config,
        private boxPopupService:BoxPopupService,
        private _service:PalletServices) {
        this.whs_id=this._Func.lstGetItem('whs_id');
        this.gdId=this.params.get('id');
    }

    private getData(page=''){

        let param='?limit='+this.perPage+'&page='+page;
        this.showLoadingOverlay=true;
        this._service.getGoodReceiptList(this.whs_id, this.gdId,param).subscribe(
            data => {
                this.buildData(data);
            },
            err => {
                this.showLoadingOverlay=false;
                this.parseError(err);
            }
        );
    }

    buildData(data:any) {
        this.PalletDetail['data']=data['data'];
        // this.DamagedCartonDetail=data.data;
        this.showLoadingOverlay=false;
        this.dataExport = [];
        this.fileName = 'Pallet-tab-' + this.refNum + '.csv';
        let blacklist = [];
        for(let palletItem of this.PalletDetail['data']) {
            palletItem.curr_ctn_ttl = palletItem.ctn_ttl ? palletItem.ctn_ttl.toString() : 0;
            palletItem.curr_expired_date = palletItem.expired_date;
            palletItem.can_edit_cur_ctns = false;// this.asnStsCode !== 'CO' && palletItem.odr_id === null;
            palletItem.cur_qty_bak = palletItem.cur_qty ? palletItem.cur_qty.toString() : 0;
            palletItem.can_edit_cur_qty = false;
            palletItem.act_loc_code_bak = palletItem.act_loc_code;
            if (palletItem.item_status_code === 'RG') {
                blacklist.push(palletItem.plt_num);
                if (!this.reveivingStatus) {
                    this.reveivingStatus = palletItem.item_status;
                }
            }
            // prepare data to export csv
            
        }
        for (let palletItem of this.PalletDetail['data']) {
            palletItem.plt_sts_name = blacklist.indexOf(palletItem.plt_num) !== -1 ? this.reveivingStatus : this.grStsName;
            this.dataExport.push([
                palletItem.gr_dtl_id,
                palletItem.plt_sts_name,// palletItem.plt_sts,
                palletItem.plt_num,
                palletItem.rfid,
                palletItem.putaway_location,
                palletItem.act_loc_code,
                palletItem.item_id,
                palletItem.sku,
                palletItem.size,
                palletItem.color,
                palletItem.lot,
                palletItem.uom_name,
                palletItem.weight,
                palletItem.pack,
                palletItem.des,
                palletItem.init_ctns,
                palletItem.ctn_ttl,
                palletItem.init_piece_ttl,
                palletItem.cur_qty,
                palletItem.expired_date,
            ]);
        }
        if(data['meta']){
            this.Pagination=data['meta']['pagination'];
            this.Pagination['numLinks']=3;
            this.Pagination['tmpPageCount']=this._Func.Pagination(this.Pagination);
        }
    }

    private changeCtns(item={}){
        if(!item['hasErrorRequiredCTN']) {
            if(item['ctn_ttl'] == 0) {
                let warningPopup = new WarningBoxPopup();
                warningPopup.text = 'Are you sure you want to update the CTNS to 0?';
                this.boxPopupService.showWarningPopup(warningPopup)
                    .then((ok) => {
                        if(ok) {
                            this.updateCarton(item);
                        }
                        else {
                            item['ctn_ttl'] = item['curr_ctn_ttl'];
                        }

                    });
            }
            else {
                // Show popup. When CTNS > 0 and forcus change.
                let warningPopup = new WarningBoxPopup();
                warningPopup.text = 'Are you sure you want to update the CTNS?';
                this.boxPopupService.showWarningPopup(warningPopup)
                .then((ok) => {
                    if(ok) {
                        this.updateCarton(item);
                    } else {
                        item['ctn_ttl'] = item['curr_ctn_ttl'];
                    }
                });
            }
        }
    }

    private changeQty(item={}){
        if(!item['hasErrorRequiredQTY'] && !item['hasErrorLess300000']) {
            if(item['cur_qty'] == 0) {
                let warningPopup = new WarningBoxPopup();
                warningPopup.text = 'Are you sure you want to update the QTY?';
                this.boxPopupService.showWarningPopup(warningPopup)
                    .then((ok) => {
                        if(ok) {
                            this.updateQty(item);
                        }
                        else {
                            item['cur_qty'] = item['cur_qty_bak'];
                        }
                    });
            }
            else {
                // Show popup. When QTY > 0 and forcus change.
                let warningPopup = new WarningBoxPopup();
                warningPopup.text = 'Are you sure you want to update the QTY?';
                this.boxPopupService.showWarningPopup(warningPopup)
                .then((ok) => {
                    if(ok) {
                        this.updateQty(item);
                    }  else {
                        item['cur_qty'] = item['cur_qty_bak'];
                    }
                });
            }
        }
    }

    private updateCarton(item) {
        this.showLoadingOverlay=true;
        const curPage = this.Pagination['current_page'] ? this.Pagination['current_page'] : '';

        setTimeout(()=>{
            this._service.updateCtns(this.whs_id, this.gdId, JSON.stringify(item)).subscribe(data=>{
                this.showLoadingOverlay=false;
                let msg=this._Func.Messages('success',this._Func.msg('VR109'));
                this.messages.emit(msg);
                item['curr_ctn_ttl'] = item['ctn_ttl'];
                item['curr_expired_date'] = item['expired_date'];
                item['cur_qty_bak'] = item['cur_qty'];
                this.getData(curPage);
            },err=>{
                this.parseError(err);
                this.showLoadingOverlay=false;
                item['ctn_ttl'] = item['curr_ctn_ttl'];
                item['expired_date'] = item['curr_expired_date'];
                item['cur_qty'] = item['cur_qty_bak'];
            });
        })
    }

    private updateQty(item) {
        this.showLoadingOverlay=true;
        setTimeout(()=>{
            this._service.updateQty(this.whs_id, this.gdId, JSON.stringify(item)).subscribe(data=>{
                this.showLoadingOverlay=false;
                let msg=this._Func.Messages('success',this._Func.msg('VR109'));
                this.messages.emit(msg);
                item['curr_ctn_ttl'] = item['ctn_ttl'];
                item['curr_expired_date'] = item['expired_date'];
                item['cur_qty_bak'] = item['cur_qty'];
            },err=>{
                this.parseError(err);
                this.showLoadingOverlay=false;
                item['ctn_ttl'] = item['curr_ctn_ttl'];
                item['expired_date'] = item['curr_expired_date'];
                item['cur_qty'] = item['cur_qty_bak'];
            });
        })
    }

    private changeExpDate(item, event) {
        let warningPopup = new WarningBoxPopup();
        warningPopup.text = 'Are you sure you want to update the Exp Date?';
        this.boxPopupService.showWarningPopup(warningPopup)
        .then((ok) => {
            if(ok) {
                setTimeout(() => {
                    var currentVal = jQuery(event.target).val() || '';
                    // console.log('change date', currentVal, item.expired_date);
                    if(!item.expired_date) item.expired_date = '';
                    if(currentVal != item.expired_date) {
                        item.expired_date = currentVal;
                        this.showLoadingOverlay=true;
                        this._service.updateExpireDate(this.whs_id, item['gr_dtl_id'], item.plt_id, JSON.stringify(item)).subscribe(data=>{
                            this.showLoadingOverlay=false;
                            let msg=this._Func.Messages('success',this._Func.msg('VR109'));
                            this.messages.emit(msg);
                            item['curr_ctn_ttl'] = item['ctn_ttl'];
                            item['curr_expired_date'] = item['expired_date'];
                            item['cur_qty_bak'] = item['cur_qty'];
                        },err=>{
                            this.parseError(err);
                            this.showLoadingOverlay=false;
                            item['ctn_ttl'] = item['curr_ctn_ttl'];
                            item['expired_date'] = item['curr_expired_date'];
                            item['cur_qty'] = item['cur_qty_bak'];
                        });
                    }
                }, 200);
            } else {
                item['expired_date'] = item['curr_expired_date'];
            }
        });
    }

    private onPageSizeChanged($event:any)
    {
        this.perPage = $event.target.value;
        if(this.Pagination){
            if((this.Pagination['current_page'] - 1) * this.perPage >= this.Pagination['total']) {
                this.Pagination['current_page'] = 1;
            }
            this.getData(this.Pagination['current_page']);
        }

    }

    private parseError(err){
        let msg={'status' : 'danger', 'txt' : this._Func.parseErrorMessageFromServer(err)};
        this.messages.emit(msg);
    }

    private checkInputNumber(evt, int=true) {
        this._Valid.isNumber(evt, int);
    }

    private formatValue(evt, item) {
        item.hasErrorRequiredCTN = false;
        var value = evt.target.value;
        if(!value && value !== 0) {
            value = '';
        }
        else {
            if(isNaN(value)) {
                value = '';
            }
            else {
                value = parseInt(value);
            }
            if((value <= '' && value !== 0)) {
                value = '';
            };
        }
        evt.target.value = value;
        if(!value && value !== 0) item.hasErrorRequiredCTN = true;
        item.ctn_ttl = value;
    }

    private validdateQty(evt, item) {
        item.hasErrorRequiredQTY = false;
        item.hasErrorLess300000 = false;
        var value = evt.target.value;

        // if(value.length == 0)
        //     item.hasErrorRequiredQTY = true;

        if(parseFloat(value) >= 300000)
            item.hasErrorLess300000 = true;

        item.cur_qty = value;
    }

    autoSearchLocation(key, item) {
        const keystrim = this._Func.trim(key);
        if (!keystrim) {
            return;
        }
        let n = this;
        n.locations = [];
        item['loading'] = true;
        // do Search
        let param = '?loc_code=' + encodeURIComponent(keystrim) + '&cus_id='+ this.refCusID +'&limit=20';
        if (item.odr_id) {
            n._service.autoSearchLocationOrderIdNotNull(n.whs_id, param).debounceTime(400).distinctUntilChanged().subscribe(
                data => {
                    n.locations = data.data;
                    n.checkKeyExist(n.locations, keystrim, item);
                },
                err => {
                    console.log(err);
                },
                () => {
                    // disable loading icon
                    item['loading'] = false;
                }
            );
        } else {
            n._service.autoSearchLocation(n.whs_id, param).debounceTime(400).distinctUntilChanged().subscribe(
                data => {
                    n.locations = data.data;
                    n.checkKeyExist(n.locations, keystrim, item);
                },
                err => {
                    console.log(err);
                },
                () => {
                    // disable loading icon
                    item['loading'] = false;
                }
            );
        }
    }

    checkKeyExist(data: any[], key: string, item: any) {
        for (let i=0; i < data.length; i++) {
            if (data[i]['loc_code'] && data[i]['loc_code'].toUpperCase() === key) {
                this.chooseItem(data[i], item);
                break;
            }
        }
    }
    
    chooseItem(data, item) {
        item['act_loc_code'] = data['loc_code'];
        this.locations = [];
        this.updateLocation(item);
    }

    updateLocation(item) {
        setTimeout(() => {
            const param = {
                'loc_code': item['act_loc_code'],
                'plt_id': item['plt_id']
            };
            if (!param['loc_code']) {
                return;
            }
            if (item.odr_id) {
                this._service.updateLocationXDock(this.whs_id, this.gdId, JSON.stringify(param)).subscribe(
                    data => {
                        this.showLoadingOverlay = false;
                        let msg = this._Func.Messages('success',this._Func.msg('VR109'));
                        this.messages.emit(msg);
                        item['act_loc_code_bak'] = item['act_loc_code'];
                    }, err => {
                        this.parseError(err);
                        this.showLoadingOverlay = false;
                        item['act_loc_code'] = item['act_loc_code_bak'];
                    }
                );
            } else {
                this._service.updateLocation(this.whs_id, this.gdId, JSON.stringify(param)).subscribe(
                    data => {
                        this.showLoadingOverlay = false;
                        let msg = this._Func.Messages('success',this._Func.msg('VR109'));
                        this.messages.emit(msg);
                        item['act_loc_code_bak'] = item['act_loc_code'];
                    }, err => {
                        this.parseError(err);
                        this.showLoadingOverlay = false;
                        item['act_loc_code'] = item['act_loc_code_bak'];
                    }
                );
            }
        }, 200);
    }

    showDropdown($event) {
        const input = jQuery($event.target);
        const ul = input.next('ul');
        ul.css({
            display: 'block',
            position: 'fixed',
            // width: input.outerWidth(),
            top: input.offset().top - jQuery(window).scrollTop() + input.outerHeight(),
            left: input.offset().left
        })
    }

    hideDropDown($event) {
        setTimeout(() => {
            const input = jQuery($event.target);
            const ul = input.next('ul');
            ul.css({ display: 'none' });
        }, 200);
    }

    deletePallet(item) {
        let warningPopup = new WarningBoxPopup();
        warningPopup.text = 'Are you sure you want to delete?';
        this.boxPopupService.showWarningPopup(warningPopup)
        .then((ok) => {
            if(ok) {
                item['ctn_ttl'] = 0;
                this.updateCarton(item);
            }

        });
    }

}
