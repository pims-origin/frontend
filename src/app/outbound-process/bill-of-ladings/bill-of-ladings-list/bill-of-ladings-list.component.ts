import {Component, OnInit} from "@angular/core";
import {Router, RouterLink, ROUTER_DIRECTIVES} from '@angular/router-deprecated';
import {FORM_DIRECTIVES, ControlGroup, FormBuilder, Control, Validators} from '@angular/common';
import {API_Config, UserService, Functions} from '../../../common/core/load';
import {Http} from '@angular/http';
import {BoxPopupService} from "../../../common/popup/box-popup.service";
import {ValidationService} from '../../../common/core/validator';
import {WMSPagination, AgGridExtent, WMSMessages} from '../../../common/directives/directives';
import 'ag-grid-enterprise/main';
import {BillOfLadingListService} from "./boll-list-service";
declare var jQuery:any;
declare var saveAs: any;

@Component({
    selector: 'bill-of-ladings-list',
    providers: [BillOfLadingListService, ValidationService, BoxPopupService],
    directives: [ROUTER_DIRECTIVES, AgGridExtent, WMSMessages, WMSPagination],
    templateUrl: 'bill-of-ladings-list.component.html',
})

export class BillOfLadingsListComponent {

    private headerURL = this.apiService.API_User_Metas + '/bol';
    private columnData = {
        ship_id_ck: {
            attr: 'checkbox',
            id: true,
            title: '#',
            width: 25,
            pin: true,
            ver_table: '26/07/2018-10'
        },
        ship_sts_name:{
            title: 'Status',
            width: 60,
            pin: true,
            sort: true
        },
        ship_id:{
            title: 'BOL ID',
            width: 90,
            pin: true,
            sort: true,
            hidden:true
        },
        ship_sts:{
            hidden:true
        },
        cus_name: {
            title: 'Customer Name',
            width: 130,
            pin: true
        },
        schd_ship_dt: {
            title: 'Scheduled To Ship',
            width: 100,
            pin: true,
            sort: true
        },
        ship_to_name: {
            title: 'Company',
            width: 100,
            pin: true,
            sort: true
        },
        address: {
            title: 'Address',
            width: 300,
            sort: true
        },
        bol_label: {
            title: 'BOL Number',
            width: 160,
            url:'#/outbound/bill-of-ladings/',
            field_get_id:'ship_id',
            sort: true
        },
        bol_num: {
            hidden:true
        },
        odr_count: {
            title: 'Order Count',
            width: 90
        }
    };
    private Pagination;
    private perPage = 20;
    private whs_id;
    private Loading = {};
    private searching = {};
    searchForm:ControlGroup;
    private isSearch:boolean = false;
    private billOfLadingList:Array<any> = [];
    private sortData = {fieldname: 'created_at', sort: 'desc'};
    private showLoadingOverlay:boolean = false;
    private messages;
    private Customers:Array<any> = [];
    private ListSelectedItem:Array<any> = [];

    private addressList = [];
    printBOLLPNForm: ControlGroup;

    constructor(private bolService:BillOfLadingListService,
                private _Func:Functions,
                private _boxPopupService:BoxPopupService,
                private apiService:API_Config,
                private _user:UserService,
                private fb:FormBuilder,
                private _http:Http,
                private _Valid:ValidationService,
                private _router:Router) {

        this.checkPermission();
        this.buildSearchForm();
        this.buildFormPrintBOLLPN();
        this.renderFullAddress();
    }

    // Check permission for user using this function page
    private createBol;
    private viewBol;
    private editBol;
    private allowAccess:boolean = false;

    private checkPermission() {
        this.showLoadingOverlay = true;
        this._user.GetPermissionUser().subscribe(
            data => {
                this.createBol = this._user.RequestPermission(data, 'createBOL');
                this.viewBol = this._user.RequestPermission(data, 'viewBOL');
                this.editBol = this._user.RequestPermission(data, 'editBOL');
                /* Check orther permission if View allow */
                if (!this.viewBol) {
                    this.redirectDeny();
                }
                else {
                    this.allowAccess = true;
                    this.whs_id = localStorage.getItem('whs_id');
                    this.getCustomersByWH();
                    this.getBOLlist();
                }
            },
            err => {
                this.messages = this._Func.Messages('danger', this._Func.parseErrorMessageFromServer(err));
                this.showLoadingOverlay = false;
            }
        );
    }

    buildSearchForm() {
        this.searchForm = this.fb.group({
            cus_id: [''],
            sh_ready: [''],
            full_add: [''],
            bol_num: [''],
            bol_label: ['']
        });
    }

    private searchParam;

    Search() {
        this.isSearch = true;
        let data = this.searchForm.value;
        // set status search is true
        // assign to SearchQuery
        this.searchParam = "&cus_id=" + data['cus_id'] + "&sh_ready=" + encodeURIComponent(data['sh_ready']) + "&full_add=" + encodeURIComponent(data['full_add']) +
            "&bol_num=" + encodeURIComponent(data['bol_num']) + "&bol_label=" + encodeURIComponent(data['bol_label']);

        this.getBOLlist(1);

    }

    redirectDeny() {
        this._router.parent.navigateByUrl('/deny');
    }
    // get list custommer

    private getCustomersByWH() {

        let params = '?whs_id=' + this.whs_id + "&limit=10000";
        this.bolService.getCustomersByWH(params)
            .subscribe(
                data => {
                    this.Customers = data.data;
                },
                err => {
                    this.parseError(err);
                },
                () => {
                }
            );

    }

    private renderFullAddress(cus_id='',ship_ready='') {

        /*
         Empty list
        * */
        this.addressList=[];

        this.Loading['loadAđress']=true;
        let params='?ship_ready=' + ship_ready + '&cus_id=' + cus_id + '&t=l';
        this.bolService.getFullAddressList(params)
            .subscribe(
                data => {
                    this.Loading['loadAđress']=false;
                    this.addressList = data.data;
                },
                err=>{
                    this.Loading['loadAđress']=false;
                    this.parseError(err);
                },
                ()=>{
                    this.Loading['loadAđress']=false;
                }
            );

    }

    /*===========================================================================================
    * Change customer on search
    * ========================================================================================== */

    private changeCustomer($event){

        let cusValue=$event.target.value;
        if(cusValue){
            this.renderFullAddress($event.target.value,this.searchForm.value['sh_ready']);
        }
        else{
            this.renderFullAddress();
        }

    }

    /*===========================================================================================
     * Change page size
     * ========================================================================================== */
    private onPageSizeChanged($event) {
        this.perPage = $event.target.value;
        if (this.Pagination) {
            if ((this.Pagination['current_page'] - 1) * this.perPage >= this.Pagination['total']) {
                this.Pagination['current_page'] = 1;
            }
            this.getBOLlist(this.Pagination['current_page']);
        }

    }

    /*
     * Router edit
     * */
    Edit() {

        this.scolltoTop();
        if (this.ListSelectedItem.length > 1) {
            this.messages = this._Func.Messages('danger', 'Please choose only one item to update!');
        }
        else {
            if (this.ListSelectedItem.length) {
                // this._router.navigateByUrl('/outbound/bill-of-ladings/' + this.ListSelectedItem[0]['bol_id'] + '/edit');
                if(this.ListSelectedItem[0]['ship_sts']!=='NW')
                {
                    this.messages = this._Func.Messages('danger', this._Func.msg('BOL001'));
                }
                else {
                    this._router.navigateByUrl('/outbound/bill-of-ladings/' + this.ListSelectedItem[0]['ship_id'] + '/edit');
                }

            }
            else {
                this.messages = this._Func.Messages('danger', 'Please choose one item to update!');
            }
        }
    }



    private getBOLlist(page = null) {

        let param = "?sort[" + this.sortData['fieldname'] + "]=" + this.sortData['sort'] +"&whs_id="+this.whs_id+"&page=" + page + "&limit=" + this.perPage;
        if (this.isSearch) {
            param = param + this.searchParam;
        }
        this.showLoadingOverlay = true;

        this.bolService.getBillOfLadingList(param).subscribe(
            data => {
                this.showLoadingOverlay = false;
                this.billOfLadingList = this._Func.formatData(data.data);
                //pagin function
                this.Pagination = data['meta']['pagination'];
                this.Pagination['numLinks'] = 3;
                this.Pagination['tmpPageCount'] = this._Func.Pagination(this.Pagination);
            },
            err => {
                this.parseError(err);
                this.showLoadingOverlay = false;
            },
            () => {}
        );

    }

    scolltoTop() {
        jQuery('html, body').animate({
            scrollTop: 0
        }, 700);
    }

    ResetSearch() {
        this.isSearch = false; // remove action search
        this._Func.ResetForm(this.searchForm);
        this.getBOLlist(this.Pagination['current_page']);
    }

    // Show error when server die or else
    private parseError(err) {
        err = err.json();
        this.messages = this._Func.Messages('danger', err.errors.message);
    }

    private printBOLNum;
    private printBOLId;
    showErrorWapLPN = false;
    private checkPrintBOLLPN() {
        this.messages = null;
        this.showErrorWapLPN = false;
        if (this.ListSelectedItem.length != 1) {
            this.messages = this._Func.Messages('danger', this._Func.msg('BOL004'));
            return false;
        }

        if(this.ListSelectedItem[0]['ship_sts']!=='NW')
        {
            this.messages = this._Func.Messages('danger', this._Func.msg('BOL005'));
            return false;
        }

        this.printBOLNum = this.ListSelectedItem[0].bol_num;
        this.printBOLId = this.ListSelectedItem[0].ship_id;
        (<Control>this.printBOLLPNForm.controls['num_of_pallet']).updateValue('');

        jQuery('#print-bol-lpn').modal('show');
    }

    buildFormPrintBOLLPN() {
        this.printBOLLPNForm = this.fb.group({
            num_of_pallet: ['', Validators.compose([Validators.required, this._Valid.isZero,this._Valid.inPutOnlyNumber])]
        });
    }

    printBOLLPN() {
        var that = this;
        this.showErrorWapLPN = true;
        if(this.printBOLLPNForm.valid) {
            let api = that.apiService.API_ORDERS_CORE + '/orders/print-bol-lpn/' + that.printBOLId + '?new_lpn=' + (<Control>this.printBOLLPNForm.controls['num_of_pallet']).value;
            try{
                that.showLoadingOverlay = true;
                let xhr = new XMLHttpRequest();
                xhr.open("GET", api , true);
                xhr.setRequestHeader("Authorization", 'Bearer ' + that._Func.getToken());
                xhr.responseType = 'blob';

                xhr.onreadystatechange = function () {
                    if(xhr.readyState == 2) {
                        if(xhr.status == 200) {
                            xhr.responseType = "blob";
                        } else {
                            xhr.responseType = "text";
                        }
                    }

                    if(xhr.readyState === 4) {
                        if(xhr.status === 200) {
                            var fileName = `License_Plate_${that.printBOLNum}`;
                            var blob = new Blob([this.response], {type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'});
                            saveAs.saveAs(blob, fileName + '.pdf');
                        }else{
                            let errMsg = '';
                            if(xhr.response) {
                                try {
                                    var res = JSON.parse(xhr.response);
                                    errMsg = res.errors && res.errors.message ? res.errors.message : xhr.statusText;
                                }
                                catch(err){errMsg = xhr.statusText}
                            }
                            else {
                                errMsg = xhr.statusText;
                            }
                            if(!errMsg) {
                                errMsg = that._Func.msg('VR100');
                            }
                            that.showMessage('danger', errMsg);
                        }
                        jQuery('#print-bol-lpn').modal('hide');
                        that.showLoadingOverlay = false;
                    }
                }
                xhr.send();
            }catch (err){
                that.showMessage('danger', that._Func.msg('VR100'));
                that.showLoadingOverlay = false;
            }
        }
    }

    private showMessage(msgStatus, msg) {
        this.messages = {
            'status': msgStatus,
            'txt': msg
        }
        jQuery(window).scrollTop(0);
    }

    private checkInputNumber(evt){
        evt = evt || window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (!evt.ctrlKey && charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 46){
            evt.preventDefault();
        }
    }

}
