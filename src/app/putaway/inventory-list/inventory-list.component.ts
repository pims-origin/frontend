/*
 PA list
 */
import {Component} from '@angular/core';
import { Router, RouterLink, ROUTER_DIRECTIVES, RouteParams } from '@angular/router-deprecated';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import {Functions} from "../../common/core/functions";
import {API_Config} from "../../common/core/API_Config";
import {JwtHelper} from "angular2-jwt/angular2-jwt";
import {PutAwayServices} from "../putaway.service";
import {WMSPagination, AdvanceTable, WMSBreadcrumb, WMSMessages} from '../../common/directives/directives';
import { OrderBy } from "../../common/pipes/order-by.pipe";
import {UserService} from "../../common/users/users.service";
declare var jQuery: any;
declare var jsPDF: any;

@Component({
    selector: 'inventory-list',
    directives: [ROUTER_DIRECTIVES, AdvanceTable,WMSMessages,WMSBreadcrumb, WMSPagination],
    templateUrl: 'inventory-list.component.html',
    pipes: [OrderBy],
    providers: [PutAwayServices]

})
export class InventoryListComponent{
    private tableID = 'gr-list';
    private headerURL = this._API_Config.API_User_Metas + '/IVU';
    private headerDef = [{id: 'ver_table', value: 1},
                      {id: 'ck', name: '', width: 30},
                      {id: 'gr_sts', name: 'Status', width: 70},
                      {id: 'gr_hdr_num', name: 'Goods Receipt #', width: 150},
                      {id: 'container', name: 'Container', width: 120, unsortable: true},
                      {id: 'asn_number', name: 'ASN Number', width: 130, unsortable: true},
                      {id: 'customer', name: 'Customer', width: 170, unsortable: true},
                      {id: 'ref_code', name: 'Ref Code', width: 80, unsortable: true},
                      {id: 'num_of_ctns', name: '# of Exp CTNs', width: 50, unsortable: true},
                      {id: 'actual_num_of_ctns', name: '# of Act CTNs', width: 50, unsortable: true},
                      {id: 'x_dock', name: 'X-Dock', width: 50, unsortable: true},
                      {id: 'damaged', name: 'Damaged', width: 50, unsortable: true},
                      {id: 'putter_name', name: 'Handler', width: 130, unsortable: true},
                      {id: 'on_rack', name: 'On Rack', width:50, unsortable: true},
                      {id: 'user', name: 'User', width: 130, unsortable: true}];
    private pinCols = 5;
    private rowData: any[];
    private getSelectedRow;
    private objSort;
    private searchParams;

    private headerExport = ['Good Receipt Number', 'Container', 'ASN Number' ,'Customer', 'REF Code', 'Number Of Expected Cartons', 'Number Of Actual Cartons', 'X-Dock', 'Damaged'];
    private dataExport: any[];

    private whs_id: any;
    private Pagination;
    private totalCount = 0;
    private tmpPageCount;
    private pageCount=0;
    private ItemOnPage=0;
    private currentPage = 1;
    private perPage = 20;
    private numLinks = 3;
    private messages;
    private dataListINV = [];
    private dataListCustomer = [];
    private showLoadingOverlay = false;
    private hasPermissionView = false;
    private currentUserID = '';

    // Construct
    constructor (
        private _putAwayServices: PutAwayServices,
        private _func: Functions,
        private _API_Config: API_Config,
        private _http: Http,
        public jwtHelper: JwtHelper,
        private _user: UserService,
        private _router:Router
    ) {
        this.whs_id = localStorage.getItem('whs_id');
        this.getCustomersByWH();
        // this.getListPutAway(1);
        this.checkPermission();

    }

    private checkPermission() {
      this.showLoadingOverlay = true;
      this._user.GetPermissionUser().subscribe(
          data => {
              this.hasPermissionView = this._user.RequestPermission(data,'updateInventory');
              if(!this.hasPermissionView) {
                  this._router.parent.navigateByUrl('/deny');
              }
              else {

                this.getListPutAway(1);
              }
          },
          err => {
              this.messages = this._func.Messages('danger', this._func.parseErrorMessageFromServer(err));
              this.showLoadingOverlay = false;
          }
      );
    }

    // Format data for ag-grid
    private createRowData(data) {
        var rowData: any[] = [];
        // Check data
        if (typeof data.data != 'undefined') {

            data = this._func.formatData(data.data);
            this.dataExport = [];
            for (var i = 0; i < data.length; i++) {

                this.dataExport.push([
                    data[i].gr_sts_name,
                    data[i].gr_hdr_num,
                    data[i].ctnr_num,
                    data[i].asn_hdr_num,
                    data[i].cus_code,
                    data[i].asn_hdr_ref,
                    data[i].gr_ctn_ttl,
                    data[i].gr_act_ctn_ttl,
                    data[i].gr_crs_doc,
                    data[i].gr_dtl_is_dmg == '0' ? 'No' : 'Yes',
                    data[i].user_name
                ]);

                rowData.push({
                    gr_sts: data[i].gr_sts_name,
                    gr_hdr_num: data[i].gr_hdr_num,
                    container: data[i].ctnr_num,
                    asn_number: data[i].asn_hdr_num,
                    customer: data[i].cus_code,
                    ref_code: data[i].asn_hdr_ref,
                    user: data[i].user_name,
                    num_of_ctns: data[i].gr_ctn_ttl,
                    actual_num_of_ctns: data[i].gr_act_ctn_ttl,
                    x_dock: data[i].gr_crs_doc,
                    damaged: data[i].gr_dtl_is_dmg == '0' ? 'No' : 'Yes',
                    gr_hdr_id: data[i].gr_hdr_id,
                    on_rack: data[i].putaway_sts == 'CO' ? 'Yes' : 'No',
                    putter_name: data[i].putter_name,
                    created_from: data[i].created_from
                })
            }
        }

        this.rowData = rowData;
    }

    // Export data to PDF format
    private exportPdf(){

        var doc = new jsPDF('p', 'pt');

        doc.autoTable(this.headerExport, this.dataExport, {

            theme: 'grid'
        });

        doc.save('good_receipt_list.pdf');
    }

    private exportCsv(){

        var content = this.dataExport;
        var finalVal = this.headerExport.join(',') + '\n';

        for (var i = 0; i < content.length; i++) {
            var value = content[i];

            for (var j = 0; j < value.length; j++) {
                var innerValue =  value[j]===null?'':value[j].toString();
                var result = innerValue.replace(/"/g, '""');
                if (result.search(/("|,|\n)/g) >= 0)
                    result = '"' + result + '"';
                if (j > 0)
                    finalVal += ',';
                finalVal += result;
            }

            finalVal += '\n';
        }
        var blob = new Blob([finalVal], { type: 'text/csv;charset=utf-8;' });

        if (navigator.msSaveBlob) { // IE 10+
            navigator.msSaveBlob(blob, finalVal);
        } else {
            var downloadLink = jQuery('<a style="visibility:hidden;" href="data:text/csv;charset=utf-8,' + encodeURIComponent(finalVal) +'" download="dataGoodReceipt.csv"></a>').appendTo('body');
            downloadLink[0].click();
            downloadLink.remove();
        }
    }

    // Get ASN
    private getListPutAway(page = null, $params = '') {

        var currentUserID =  JSON.parse(localStorage.getItem('logedUser'));

        this.showLoadingOverlay = true;
        if(!page) page = 1;
        var params="?page="+page+"&limit="+this.perPage + $params;;
        params += "&whs_id=" + this.whs_id + '&putter_id=' + currentUserID['user_id'] ;
        if(this.objSort && this.objSort['sort_type'] != 'none') {
          params += '&sort['+ this.objSort['sort_field']+ ']='+ this.objSort['sort_type'];
        }
        else {
          params += '&sort[created_at]=desc';
        }
        if(this.searchParams) {
          params += this.searchParams;
        }

        this._putAwayServices.getListPutAway(params)
            .subscribe(
                data => {

                    // jQuery("#gr-list").find('.ag-header-cell :checkbox').prop("checked", false);
                    this.dataListINV = data.data;
                    this.initPagination(data);
                    this.createRowData(data);
                    this.showLoadingOverlay = false;
                },
                err => {

                    // this.flagLoading = false;
                    // this.flagLoading2 = false;
                    this.parseError(err);
                    this.showLoadingOverlay = false;
                },
                () => {

                    // this.flagLoading = false;
                    // this.flagLoading2 = false;
                }
            );
    }

    // Get customers
    private getCustomersByWH () {

        var $params ='?limit=10000';

        this._putAwayServices.getCustomersByWH($params)
            .subscribe(
                data => {

                    this.dataListCustomer = data.data;
                },
                err => {

                    this.parseError(err);
                },
                () => {}
            );

    }

    private filterList(pageNumber) {
        this.getListPutAway(pageNumber);
    }

    private getPage(pageNumber) {
        let arr=new Array(pageNumber);
        return arr;
    }

    // Set params for pagination
    private initPagination(data){
        var meta = data.meta;
        this.Pagination=meta['pagination'];
        this.Pagination['numLinks']=3;
        this.Pagination['tmpPageCount']=this._func.Pagination(this.Pagination);

    }
    // Event when user filter form
    private onPageSizeChanged ($event) {
        this.perPage = $event.target.value;
        if((this.Pagination['current_page'] - 1) * this.perPage >= this.Pagination['total']) {
            this.currentPage = 1;
        }
        else {
            this.currentPage = this.Pagination['current_page'];
        }
        this.getListPutAway(this.currentPage);
    }

    // Edit
    // Navigation to edit page
    private edit() {

        // this.messages = {};
        var allItem = jQuery('#gr-list .ag-body-container :checkbox'),
            checkedItem = allItem.filter(':checked');
        if(checkedItem.length > 1 ) {
            this.messages = this._func.Messages('danger', this._func.msg('CF004'));
            return false;
        }

        if(checkedItem.length < 1 ) {
            this.messages = this._func.Messages('danger', this._func.msg('CF005'));
            return false;
        }

        var itemIndex = allItem.index(checkedItem),
            itemID = this.dataListINV[itemIndex].gr_hdr_id;

        this._router.parent.navigateByUrl('/goods-receipt/' + itemID + "/edit");
    }

    private update_invent($event) {
        this.getSelectedRow = 'update-inventory';
    }

    afterGetSelectedRow($event) {
      var listSelectedItem = $event.data;
      switch ($event.action) {
        case 'edit':
          break;
        case 'delete':
          break;
        case 'update-inventory':
          if (listSelectedItem.length > 1) {
            this.messages = this._func.Messages('danger', 'Please choose only one item to update inventory!');
          }
          else {
            if (listSelectedItem.length < 1) {
              this.messages = this._func.Messages('danger', 'Please choose one item to update inventory!');
            }
            else {
              if (listSelectedItem[0].created_from.toLowerCase() !== 'wms') {
                this.messages = this._func.Messages('danger', this._func.msg('PAS08'));
              } else {
                this._router.parent.navigateByUrl('/putaway/inventory-update/' + listSelectedItem[0].gr_hdr_id);
              }
            }
          }
          break;
      }

      this.getSelectedRow = false;
    }

    private doSort(objSort) {
      this.objSort = objSort;
      this.getListPutAway(this.Pagination.current_page);
    }

    // Search
    private search () {

        this.searchParams = '';
        let params_arr = jQuery("#form-filter").serializeArray() ;
        for (let i in params_arr) {
            if (params_arr[i].value == "") continue;
            this.searchParams +="&" +  params_arr[i].name.trim() + "=" + encodeURIComponent(params_arr[i].value.trim());
        }
        this.getListPutAway(1);

    }

    // Reset form
    private reset() {

        jQuery("#form-filter input, #form-filter select").each(function( index ) {

            jQuery(this).val("");
        });
        this.searchParams = '';
        this.getListPutAway(1);

    }

    // Show error when server die or else
    private parseError (err) {
        try {
            err = err.json();
            this.messages = this._func.Messages('danger', err.errors.message);
        }
        catch(error) {
            this.messages = this._func.Messages('danger', this._func.msg('VR100'));
        }

    }

    private expandTable = false;
    private viewListFullScreen() {
      this.expandTable = true;
      setTimeout(() => {
        this.expandTable = false;
      }, 500);
    }
}
