import {Component} from '@angular/core';
import {Router, RouterLink, RouteConfig, ROUTER_DIRECTIVES} from '@angular/router-deprecated';
import {CURZoneTypeComponent} from "./cur-zone-type/cur-zone-type.component";
import {ZoneTypeListComponent} from "./zone-type-list/zone-type-list.component";


@Component ({
    selector: 'zone-type',
    directives: [ROUTER_DIRECTIVES],
    template: `<router-outlet></router-outlet>`,
})

@RouteConfig([
    { path: '/', component:ZoneTypeListComponent  , name: 'Zone Type List', useAsDefault: true, data:{action:'list'} },
    { path: '/new', component:CURZoneTypeComponent  , name: 'New Zone Type', data:{action:'new'} },
    { path: ':id', component:CURZoneTypeComponent  , name: 'View Zone Type' ,data:{action:'view'}},
    { path: ':id/edit', component:CURZoneTypeComponent  , name: 'Edit Zone Type' ,data:{action:'edit'}}
])
export class ZoneTypeComponent {


}
