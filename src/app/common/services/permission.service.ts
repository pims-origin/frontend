import {Component, Injector, OnInit} from '@angular/core';
import {RouteConfig, Router, RouterLink, RouteParams, RouteData, ROUTER_DIRECTIVES} from '@angular/router-deprecated';
import { Http, Headers } from '@angular/http';
import {API_Config, Functions} from '../../common/core/load';
import 'rxjs/Rx';
@Component ({

})
export class PermissionService implements OnInit{


  private AuthHeader = this._Func.AuthHeader();
  api_user_permissions = this._API.API_USER_Permission;
  private myPermission :any = [];
  public items:Array<any> =[1,2];
  active: boolean;
  message: string = '';
  constructor(
    public router: Router,
    public http: Http,
    private _Func: Functions,
    private _API: API_Config
  ) {}
  ngOnInit(){
    localStorage.removeItem('message');
  }

  userPermission(apiPermission) {
    localStorage.removeItem('message');
     this.http.get(this.api_user_permissions, {
          headers: this.AuthHeader
        })
        .map(res => {
           return res.json().data;
         })
        .subscribe(
             function(data){
               for(var d of data) {
                 if(d.name == apiPermission){
                   this.active = true;
                   localStorage.setItem('message', localStorage.getItem('jwt'));
                   break;
                 }
               }
             },
            err => {},
            () => {}
        );

  }
}
