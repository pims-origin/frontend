import {Component,OnInit} from "@angular/core";
import { Router,RouteParams, RouterLink, ROUTER_DIRECTIVES } from '@angular/router-deprecated';
import { FORM_DIRECTIVES,FormBuilder} from '@angular/common';
import {UserService, Functions} from '../../../common/core/load';
import {WMSBreadcrumb,WMSPagination,PaginationControlsCmp,WMSMessages} from '../../../common/directives/directives';
import {CustomerServices} from "../../../system-setup/customer/customer-service";
import {RFGunServices} from "../../rf-gun-service";
@Component({
  selector: 'rf-putaway-update',
  providers: [CustomerServices,RFGunServices,UserService],
  directives: [FORM_DIRECTIVES,WMSMessages,WMSBreadcrumb],
  templateUrl: 'rf-relocate.html',
})
export class RF_RelocateComponent {

  private allowAccess;
  private showLoadingOverlay;
  private messages;
  private rfGunPermission;
  private whs_id;
  private relocateCode;
  private whs={};

  constructor(private rfGunService:RFGunServices,
              private _router: Router,
              private params: RouteParams,
              private _user:UserService,
              private _Func: Functions)
  {
    this.checkPermission();
  }


  checkPermission(){

    this.showLoadingOverlay = true;
    this._user.GetPermissionUser().subscribe(
      data => {
        this.showLoadingOverlay = false;
        this.rfGunPermission = this._user.RequestPermission(data, 'viewRFGUN');
        if(this.rfGunPermission) {
          this.allowAccess=true;
          this.whs_id = localStorage.getItem('whs_id');
          this.getWareHouse(this.whs_id);
        }
        else{
          this.redirectDeny();
        }
      },
      err => {
        this.parseError(err);
        this.showLoadingOverlay = false;
      }
    );
  }

  getWareHouse(whs_id)
  {

    this.showLoadingOverlay = true;
    let param='';
    this.rfGunService.get_warehouse(param).subscribe(
      data => {
        this.whs=this.exportWareHouseItem(data.data,this.whs_id);
        this.showLoadingOverlay = false;
      },
      err => {
        this.parseError(err);
        this.showLoadingOverlay = false;
      }
    );
  }


  /*
  * exportWareHouseItem
  * */
  exportWareHouseItem(data,whs_id)
  {
    for(let i in data)
    {
      if(data[i]['whs_id']==whs_id)
      {
        return data[i];
      }
    }
    return 0;
  }

  redirectDeny(){
    this._router.parent.navigateByUrl('/deny');
  }

  removeEmptyKey(object)
  {
    let data_tmp=JSON.parse(JSON.stringify(object));
    Object.keys(object['codes']).map(
      key =>{
        if(object['codes'][key]==''){
          delete data_tmp['codes'][key];
        }
      }
    );
    return data_tmp
  }

  /*
   * updateLocation
   * */
  reLocate(data):void
  {
    let data_r={};
    data_r['codes']=(<any>Object).assign({},data.split("\n"));
    data_r=this.removeEmptyKey(data_r);
    this.showLoadingOverlay=true;
    this.rfGunService.reLocate(this.whs_id,JSON.stringify(data_r))
      .subscribe(
        data => {
          this.messages=this._Func.Messages('success',data);
          this.showLoadingOverlay=false;
          this.relocateCode=null;
        },
        err => {
          this.showLoadingOverlay=false;
          this.parseError(err);
        },
        () => {
        }
      );

  }

  // Show error when server die or else
  private parseError (err) {
    err = err.json();
    this.messages = this._Func.Messages('danger', err.errors.message);
  }

}
