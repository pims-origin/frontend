import {Component, OnInit} from '@angular/core';
import { FORM_DIRECTIVES,ControlGroup,FormBuilder, Control ,Validators, ControlArray} from '@angular/common';
import {RouteParams,Router } from '@angular/router-deprecated';
import {
  UserService, Functions, API_Config,
  FormBuilderFunctions
} from '../../../common/core/load';
import {BoxPopupService} from "../../../common/popup/box-popup.service";
import {PaginationControlsCmp, PaginationService, PaginatePipe} from '../../../common/directives/directives';
import {WarningBoxPopup} from "../../../common/popup/warning-box-popup";
import {WMSMessages} from "../../../common/directives/messages/messages";
import {PackingUpdateServices} from '../packing-update-service';
import {GoBackComponent} from "../../../common/component/goBack.component";
import { ValidationService } from '../../../common/core/validator';
import { greaterLevel } from './greater-level.pipe';
declare var jQuery: any;
declare var saveAs: any;

@Component ({
  selector: 'pack-carton',
  providers: [FormBuilder, BoxPopupService, PaginationService, PackingUpdateServices, ValidationService],
  directives: [FORM_DIRECTIVES,PaginationControlsCmp,WMSMessages,GoBackComponent],
  pipes: [PaginatePipe, greaterLevel],
  templateUrl: 'pack-carton.component.html',
})

export class PackCartonComponent {

	/** list permission need to check **/
  private hasUpdateOrderPackingPermission;
  /* end list permission */

  private messages;
  private packCartonMessage;
  private showLoadingOverlay = false;

  private selectedAll = {};
  private allItem = [];
  private itemPerPage = 20;

  private whs_id;
	private order_id;
  private customer_id;
  private carton_id;
	private items_id;
  private action;

	private cartonData;
	private packTypes;
	private packDimension;

  private totalWeight = 0;
	private showError = false;
	packCartonForm: ControlGroup;
  private viewOrder;
  private maxUOM = 0;

  private disableNewSKU = true;
  private items:ControlGroup[] = [];
  private itemsArray:ControlArray = new ControlArray(this.items);
  private uomList = [];
  private existedSKU: any;
  carriers: any[] = [];

  constructor(
    private _user: UserService,
    private _func: Functions,
    private params: RouteParams,
    private _boxPopupService: BoxPopupService,
    private _Valid: ValidationService,
    private fb: FormBuilder,
    private fbFunc: FormBuilderFunctions,
    private _packingUpdateService: PackingUpdateServices,
    private _API: API_Config,
    private _router: Router
  ){
    this.whs_id = localStorage.getItem('whs_id');
    var tmpIds = this.params.get('id').split('::');
    this.order_id = tmpIds[0];
  	this.customer_id = tmpIds[1];
    this.carton_id = tmpIds[2];
    if(this.carton_id) {
      this.action = this.params.get('item');
    }
    else {
      this.action = 'new';
    }
    this.getUOMList();
  	this.items_id = this.params.get('item');
  	this.packCartonForm =this.fb.group({
        pack_ref_id: ['', Validators.required],
        number_of_pack: ['1', Validators.required],
        is_new_sku: [false],
        new_sku: [''],
        uom_id: [''],
        carrier_code: [''],
        weight: [''], // gross weight
        net_weight: [''],
        items: this.itemsArray
    });
    // watch form
    this.packCartonForm.valueChanges.subscribe(data => {
      if (data.is_new_sku && !this.disableNewSKU) {
          this.checkUOM();
      }
  });
  	this.getPackDimensions();
    this.getPackTypes();
  	this.checkPermission();
    this.buildCartonDimensionForm();
    this.getCarriers();
	}

	private showMessage(msgStatus, msg) {
      this.messages = {
          'status': msgStatus,
          'txt': msg
      }
      jQuery(window).scrollTop(0);
  }

  private checkInputNumber (evt, allowDecimal) {
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if(allowDecimal) {
      if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 46){
        evt.preventDefault();
      }
    }
    else {
      if (charCode > 31 && (charCode < 48 || charCode > 57)){
        evt.preventDefault();
      }
    }
  }

  private formatValue(evt, index = null) {
    var elm = jQuery(evt.target),
        value = evt.target.value;
    if(!value) {
      value = null;
    }
    else {
      if(isNaN(value)) {
        value = null;
      }
      else {
        value = parseInt(value);
      }
      if(value <= 0) {
        value = null;
      };
      elm.val(value);
    }
    if(index !== null) {
      let arrControl = this.itemsArray.controls[index]['controls'];
      arrControl['piece_qty'].setErrors(null);
      (<Control>arrControl['piece_qty']).updateValue(value ? value.toString() : '');
      this.calcPackedQty();
      this.checkGreater(index);
    }
    else {
      (<Control>this.packCartonForm.controls['number_of_pack']).updateValue(value ? value.toString() : '');
      this.calcPackedQty();
      for(let i = 0; i < this.allItem.length; i++) {
        this.itemsArray.controls[i]['controls']['piece_qty'].setErrors(null);
        this.checkGreater(i);
      }
    }
  }

  checkGreater(index) {
    let arrControl = this.itemsArray.controls[index]['controls'];
    var numberOfPack = this.packCartonForm.controls['number_of_pack'].value;
    setTimeout(() => {
        if(arrControl['piece_qty'].valid && numberOfPack * arrControl['piece_qty'].value > arrControl['remain_qty'].value) {
          (<Control>arrControl['piece_qty']).setErrors({'greaterThanRemain': true});
        }
    });
  }

  private getPackDimensions(currPacktype = '') {
  	this._packingUpdateService.getPackDimensions()
      .subscribe(
        data => {
          this.packDimension = data.data;
          (<Control>this.packCartonForm.controls['pack_ref_id']).updateValue(currPacktype);
        },
        err => {
        }
      );
  }

  private getPackTypes() {
    this._packingUpdateService.getPackTypes()
      .subscribe(
        data => {
          this.packTypes = data.pack_types;
        },
        err => {
        }
      );
  }

  private allowAccess;
  private checkPermission() {
    this.showLoadingOverlay = true;
    this._user.GetPermissionUser().subscribe(
      data => {
        this.hasUpdateOrderPackingPermission = this._user.RequestPermission(data,'orderPacking');
        this.viewOrder = this._user.RequestPermission(data,'viewOrder');

        if(this.hasUpdateOrderPackingPermission||this.viewOrder) {
          this.allowAccess=true;
          this.getCartonDetail();
        }
        else {
          this._router.parent.navigateByUrl('/deny');
        }
      },
      err => {
        this.showMessage('danger', this._func.parseErrorMessageFromServer(err));
        this.showLoadingOverlay = false;
      }
    );
  }

  private getCartonDetail() {
  	this.showLoadingOverlay = true;
    if(this.action == 'new') {
      this._packingUpdateService.getNewCarton(this.order_id, this.items_id)
        .subscribe(
          data => {
            this.cartonData = data.data;
            this.allItem = this.cartonData.items;
            for(var i = 0, l = this.allItem.length; i < l; i++) {
              this.allItem[i].piece_qty = '';
            }
            this.buildItemLevel();
            this.calcPackedQty();
            this.showLoadingOverlay = false;
          },
          err => {
            this.showMessage('danger', this._func.parseErrorMessageFromServer(err));
            this.showLoadingOverlay = false;
          }
        );
    }

    if(this.action == 'edit' || this.action == 'view') {
      this._packingUpdateService.getCartonDetail(this.order_id, this.carton_id)
        .subscribe(
          data => {
            this.cartonData = data.data;
            (<Control>this.packCartonForm.controls['pack_ref_id']).updateValue(this.cartonData['pack_ref_id']);
            this.allItem = this.cartonData.items;
            if(this.action == 'edit') {
              for(var i = 0, l = this.allItem.length; i < l; i++) {
                this.allItem[i].remain_qty += this.allItem[i].piece_qty;
              }
            }
            this.buildItemLevel();
            this.calcPackedQty();
            this.showLoadingOverlay = false;
          },
          err => {
            this.showMessage('danger', this._func.parseErrorMessageFromServer(err));
            this.showLoadingOverlay = false;
          }
        );
    }
  }

  private calcPackedQty() {
    this.cartonData.piece_ttl = 0;
    var numberOfPack = this.packCartonForm.controls['number_of_pack'].value;
    if(numberOfPack) {
      for(var i = 0, l = this.allItem.length; i < l; i++) {
        let arrControl = this.itemsArray.controls[i]['controls'];
        this.cartonData.piece_ttl += arrControl['piece_qty'].value * numberOfPack;
      }
    }
  }

	cancel() {
    var that = this;
    this._boxPopupService.showWarningPopup()
      .then((ok) => {
        if(ok)
          this._router.navigateByUrl('/outbound/packing/' + this.order_id);

      });
  }

  save(data, printLabel) {
  	this.showError = true;
  	this.messages = null;

    const validNewSku = this.checkValidNewSku(this.packCartonForm.value.new_sku);
    if(!validNewSku) return;

    if(this.packCartonForm.valid) {
      data.whs_id = this.whs_id;
      data.cus_id = this.customer_id;
      if (!this.packCartonForm.value.is_new_sku && this.existedSKU) {
        data.item_id = this.existedSKU.item_id;
      }

      this.showLoadingOverlay = true;
      if(this.action == 'new') {
        this._packingUpdateService.createCarton(this.order_id, JSON.stringify(data))
          .subscribe(
            data => {
              this.showMessage('success', this._func.msg('VR137'));
              if(printLabel) {
                this.printCarton(data.data.pack_hdr_id.join(','));
              }
              else {
                this.calculateRemainQty();
              }
              this.showLoadingOverlay = false;
              this.disableNewSKU = true;
            },
            err => {
              this.showMessage('danger', this._func.parseErrorMessageFromServer(err));
              this.showLoadingOverlay = false;
            }
          );
      }

      if(this.action == 'edit') {
        this._packingUpdateService.editCarton(this.order_id, this.carton_id, JSON.stringify(data))
          .subscribe(
            data => {
              this.showMessage('success', this._func.msg('VR137'));
              if(printLabel) {
                this.printCarton(this.carton_id);
              }
              else {
                this.calculateRemainQty();
              }
              this.showLoadingOverlay = false;
              this._router.parent.navigateByUrl('/outbound/packing/' + this.order_id);
            },
            err => {
              this.showMessage('danger', this._func.parseErrorMessageFromServer(err));
              this.showLoadingOverlay = false;
            }
          );
      }
    }

  }

  private calculateRemainQty(){
    let sum = 0;
    this.showError = false;
    for(let i = 0; i < this.allItem.length ; i++){
      let itemControlArray = this.itemsArray.controls[i]['controls'];
      let newRemainQty = itemControlArray.remain_qty.value - itemControlArray.piece_qty.value * this.packCartonForm.controls['number_of_pack'].value;
      if(newRemainQty >= 0){
        (<Control>itemControlArray['remain_qty']).updateValue(newRemainQty.toString());
      }

      sum += itemControlArray.remain_qty.value;
      (<Control>itemControlArray['piece_qty']).updateValue('');
    }
    
    this.resetNewSku();
    if(sum == 0){
      setTimeout(() => {
        this._router.parent.navigateByUrl('/outbound/packing/' + this.order_id);
      }, 2000);
    }
  }

  private printCarton(cartonId){
    this.showLoadingOverlay = true;
    var that = this;
    let api = that._API.API_Pack  + '/packs/' + that.order_id + '/print?pack_hdr_id=' + cartonId;
    try{
      let xhr = new XMLHttpRequest();
      xhr.open("GET", api , true);
      xhr.setRequestHeader("Authorization", 'Bearer ' +that._func.getToken());
      xhr.responseType = 'blob';

      xhr.onreadystatechange = function () {
        if(xhr.readyState == 2) {
          if(xhr.status == 200) {
            xhr.responseType = "blob";
          } else {
            xhr.responseType = "text";
          }
        }

        if(xhr.readyState === 4) {
          if(xhr.status === 200) {
            var fileName = 'Packed-Cartons-' + that.cartonData['odr_num'];
            var blob = new Blob([this.response], {type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'});
            saveAs.saveAs(blob, fileName + '.pdf');
            if(that.action != 'view') {
              that.calculateRemainQty();
            }
          }else{
            let errMsg = '';
            if(xhr.response) {
              try {
                var res = JSON.parse(xhr.response);
                errMsg = res.errors && res.errors.message ? res.errors.message : xhr.statusText;
              }
              catch(err){errMsg = xhr.statusText}
            }
            else {
              errMsg = xhr.statusText;
            }
            if(!errMsg) {
              errMsg = that._func.msg('VR100');
            }
            that.showMessage('danger', errMsg);
          }
          that.showLoadingOverlay = false;
        }
      }
      xhr.send();
    }catch (err){
      that.showMessage('danger', that._func.msg('VR100'));
      that.showLoadingOverlay = false;
    }
  }

  private onPageSizeChanged($event, page) {
    switch (page) {
      case "itemList":
        this.itemPerPage = parseInt($event.target.value);
        break;
    }
  }

  addCartonDimensionForm: ControlGroup;
  private showErrorAddCartonDimension = false;

  buildCartonDimensionForm() {
    this.addCartonDimensionForm =this.fb.group({
      length:[null, Validators.compose([this._Valid.customRequired, this._Valid.isDecimalNumber])],
      width:[null, Validators.compose([this._Valid.customRequired, this._Valid.isDecimalNumber])],
      height:[null, Validators.compose([this._Valid.customRequired, this._Valid.isDecimalNumber])],
      pack_type:['', Validators.required],
    });
  }

  addCartonDimension() {
    var that = this;
    this.showErrorAddCartonDimension = true;
    if(this.addCartonDimensionForm.valid) {
      let data = this.addCartonDimensionForm.value;
      for(let x in data) {
        data[x] = data[x].trim()
      };
      this.showLoadingOverlay = true;
      var currCartonDimension = this.packCartonForm.value.pack_ref_id;
      this._packingUpdateService.addCartonDimension(JSON.stringify(data)).subscribe(
        data => {
          this.showLoadingOverlay = false;
          jQuery('#add-carton-demension').modal('hide');
          // reset form
          this.showErrorAddCartonDimension = false;
          (<Control>this.addCartonDimensionForm.controls['length']).updateValue(null);
          (<Control>this.addCartonDimensionForm.controls['width']).updateValue(null);
          (<Control>this.addCartonDimensionForm.controls['height']).updateValue(null);
          (<Control>this.addCartonDimensionForm.controls['pack_type']).updateValue('');
          this.packCartonMessage = null;
          this.getPackDimensions(currCartonDimension);
        },
        err => {
          this.showLoadingOverlay = false;
          this.packCartonMessage = this._func.Messages('danger', this._func.parseErrorMessageFromServer(err));
        }
      )
    }
  }

  private getUOMList(page = null) {
    this._packingUpdateService.getUOM()
      .subscribe(
          data => {
              this.uomList = data.data;
          }
      );
  }

  buildItemLevel() {
    let items = this.allItem;
    for(let i = 0; i < items.length; i++) {
      this.itemDetail(items[i]);
    }
  }

  itemDetail(item) {
    this.itemsArray.push(
        new ControlGroup({
            'itm_id': new Control(item['itm_id'].toString()),
            'sku': new Control(item['sku']),
            'color': new Control(item['color']),
            'size': new Control(item['size']),
            'lot': new Control(item['lot']),
            'weight': new Control(item['weight']),
            'uom_id': new Control(item['uom_id']),
            'pack': new Control(item['pack'] ? item['pack'].toString() : ''),
            'requested': new Control(item['requested']),
            'piece_qty': new Control(item['piece_qty'].toString(), Validators.compose([this._Valid.customRequired,this._Valid.validateInt,this._Valid.isZero])),
            'remain_qty': new Control(item['remain_qty'].toString()),
            'ship_track_id': new Control(item['ship_track_id']),
        })
    );
  }

  checkValidNewSku(value) {
    let valid = true;
    if (this.packCartonForm.controls['is_new_sku'].value && !jQuery.trim(value)) {
      valid = false;
      setTimeout(() => {
        (<Control>this.packCartonForm.controls['new_sku']).setErrors({'required': true});
      });
    }
    return valid;
  }

  _setNewSKU(data, updateUOM = true) {
    (<Control>this.packCartonForm.controls['new_sku']).updateValue(data.sku);
    // (<Control>this.packCartonForm.controls['weight']).updateValue(data.weight);
    // (<Control>this.packCartonForm.controls['net_weight']).updateValue(data.net_weight);
    if (updateUOM) {
      (<Control>this.packCartonForm.controls['uom_id']).updateValue(data.uom_id);
    }
  }

  checkNewSKU() {
    let hasErr = false;
    let l = this.itemsArray.controls.length;
    this.disableNewSKU = true;
    this.existedSKU = null;

    if (!this.itemsArray.valid) {
      this.resetNewSku();
    } else {
      if (l === 1) {
        const firstItemCtrl = this.itemsArray.controls[0]['controls'];
        if (firstItemCtrl.piece_qty.valid && firstItemCtrl.piece_qty.value == firstItemCtrl.pack.value) {
          this.resetNewSku();
          (<Control>this.packCartonForm.controls['is_new_sku']).updateValue(false);
          return;
        } else {
          (<Control>this.packCartonForm.controls['is_new_sku']).updateValue(true);          
        }
      }

      
      let data = {
        cus_id: this.customer_id,
        items: this.packCartonForm.value.items
      }

      this.showLoadingOverlay = true;
      this._packingUpdateService.checkExistSKU(JSON.stringify(data)).subscribe(
        data => {
          this.existedSKU = data;
          if (data) {
            this._setNewSKU(data);
            (<Control>this.packCartonForm.controls['is_new_sku']).updateValue(false);
          } else {
            this.disableNewSKU = false;
            (<Control>this.packCartonForm.controls['is_new_sku']).updateValue(true);

            if (l === 1) {
              this._setNewSKU(this.allItem[0], false);
            }
            // this.calculateWeight(this.allItem);
          }
        },
        err => {
          this.showMessage('danger', this._func.parseErrorMessageFromServer(err));
        },
        () => {
          this.showLoadingOverlay = false;
        }
      )
    }
  }

  resetNewSku() {  
    (<Control>this.packCartonForm.controls['new_sku']).updateValue('');
    // (<Control>this.packCartonForm.controls['weight']).updateValue('');
    // (<Control>this.packCartonForm.controls['net_weight']).updateValue('');
    (<Control>this.packCartonForm.controls['uom_id']).updateValue('');
  }

  toggleIsNewSKU() {
    setTimeout(() => {
      if(this.packCartonForm.value['is_new_sku']) {
        this.checkNewSKU();
      } else {
        this.disableNewSKU = true;
        this.resetNewSku();
      }
    })
  }

  private _getLevelByUOMId(uom_id: any): number {
    let level = 0;
    const n = this.uomList.length;
    for (let i = 0; i < n; i++) {
      if (this.uomList[i].sys_uom_id === parseInt(uom_id, 10)) {
        level = this.uomList[i].level_id;
        break;
      }
    }
    return level;
  }

  private checkUOM() {
    const UOMId = this.packCartonForm.value['uom_id'];
    // firstly, reset UOM error
    this.fbFunc.setErrorControl(this.packCartonForm, 'uom_id', null);

    if (!parseInt(UOMId, 10)) {
      this.fbFunc.setErrorControl(this.packCartonForm, 'uom_id', {'required': true});
      return;
    }

    const UOMlevelId = this._getLevelByUOMId(UOMId);

    this.itemsArray['controls'].forEach((ctrl) => {
      const level_id = this._getLevelByUOMId(ctrl['controls']['uom_id'].value);
      if (level_id && level_id > this.maxUOM) {
        this.maxUOM = level_id;
      }
    });

    // do validation
    if (UOMlevelId <= this.maxUOM) {
      this.fbFunc.setErrorControl(this.packCartonForm, 'uom_id', {'invalidUOM': true});
      // this.showError = true;
    }
  }

  private compareValue(index = null) {
    let weightCtrl = <Control>this.packCartonForm.controls['weight'];
    let netWeightCtrl = <Control>this.packCartonForm.controls['net_weight'];
    netWeightCtrl.setErrors(null);
    setTimeout(() => {
      if(weightCtrl.valid && netWeightCtrl.valid && netWeightCtrl.value !== '' && weightCtrl.value !== '' && parseInt(netWeightCtrl.value) > parseInt(weightCtrl.value)) {
        (<Control>netWeightCtrl).setErrors({'greaterThanGross': true});
      }
    })
  }
  // / items[i].pack * parseInt(this.itemsArray.controls[i]['controls'].piece_qty.value);
  private calculateWeight(items) {
    const l = items.length;
    let weight: number = 0;
    for (var i = 0; i < l; i++) {
      if (items[i].weight !== null) {
        weight += parseFloat(items[i].weight) + items[i].pack * parseInt(this.itemsArray.controls[i]['controls'].piece_qty.value)
      }
    }
    
    if (weight) {
      (<Control>this.packCartonForm.controls['weight']).updateValue(weight.toFixed(3));
      (<Control>this.packCartonForm.controls['net_weight']).updateValue(weight.toFixed(3));
    }
  }

  private getCarriers() {
    const params = '?md_type=CR&md_sts=AC&limit=999';
    this.showLoadingOverlay = true;
    this._packingUpdateService.getCarrierList(params).subscribe(
        data => {
          this.showLoadingOverlay = false;
          this.carriers = data['data'];
        },
        err => {
          this.showLoadingOverlay = false;
          this.showMessage('danger', this._func.parseErrorMessageFromServer(err));
        }
    );
  }
  private changeInputWeight($event, item) {
    if (isNaN($event)) {
      // (<Control>this.allItem[item].weight).updateValue(0);
     
    } else {
      this.totalWeight = 0;
      var dataArray = this.packCartonForm.controls.items.value;
      for(let i = 0; i < dataArray.length; i++) {

        this.totalWeight += parseFloat(dataArray[i].weight ? dataArray[i].weight : 0);

      }
      (<Control>this.packCartonForm.controls['net_weight']).updateValue(this.totalWeight.toFixed(3));
      (<Control>this.packCartonForm.controls['weight']).updateValue(this.totalWeight.toFixed(3));
    }
  }

  private calculator($event, item, index) {
      var dataItem = this.packCartonForm.controls.items.value;
      var calculator = this.allItem[index].weight / dataItem[index].pack * $event;
      this.totalWeight += calculator;

      this.packCartonForm.controls.items['controls'][index]['controls']['weight'].updateValue(calculator.toFixed(3));
      (<Control>this.packCartonForm.controls['net_weight']).updateValue(this.totalWeight.toFixed(3));
      (<Control>this.packCartonForm.controls['weight']).updateValue(this.totalWeight.toFixed(3));
    this.changeInputWeight($event, item);
  }
}
