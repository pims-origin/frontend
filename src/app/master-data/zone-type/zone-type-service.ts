import {Component, Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {API_Config, Functions} from '../../common/core/load';

@Injectable()
export class ZoneTypeServices {

  public AuthHeader = this._Func.AuthHeader();
  public AuthHeaderPost = this._Func.AuthHeaderPost();

  constructor(private _Func: Functions, private _API: API_Config, private http: Http)
  {

  }

  /*
   * Add new Zone Type
   * */
  Add_New_ZoneType(data)
  {
    return this.http.post(this._API.API_Zone_Type, data, { headers: this._Func.AuthHeaderPostJson() })
      .map((res: Response) => res.json().data);
  }

  /*
   * Update Update_ZoneType
   * */
  Update_ZoneType(id,data)
  {
    return this.http.put(this._API.API_Zone_Type+'/'+id, data, { headers: this._Func.AuthHeaderPostJson() })
      .map((res: Response) => res.json().data);
  }

  /*
   * Get detail an Zone Type
   * */
  Get_ZoneTypeDetail(id)
  {
    return this.http.get(this._API.API_Zone_Type+'/'+id, { headers: this.AuthHeader })
      .map((res: Response) => res.json().data);
  }

  /*
   * Get zone-type List
   *
   * */
  GetZoneTypeList(param)
  {
    return this.http.get(this._API.API_Zone_Type+param,{ headers: this.AuthHeader })
      .map((res: Response) => res.json());
  }

  /*
   * Get a detail warehouse
   * */
  GetDetailWareHouse(id)
  {
    return this.http.get(this._API.API_Warehouse+'/'+id,{ headers: this.AuthHeader })
      .map((res: Response) => res.json());
  }
  /*
   * Delete function
   * */
  DeleteZoneType(id)
  {
    return this.http.delete(this._API.API_Zone_Type + "/" + id, { headers: this.AuthHeader });
  }

  /*
   *
   * Search function
   * */
  Search(param)
  {
    return this.http.get(this._API.API_Zone_Type+param,{ headers: this.AuthHeader })
      .map((res: Response) => res.json());
  }


}
