import {Component} from "@angular/core";
import {Router, ROUTER_DIRECTIVES} from '@angular/router-deprecated';
import {ControlGroup, FormBuilder, Control} from '@angular/common';
import {API_Config, UserService, Functions} from '../../common/core/load';
import {BoxPopupService} from "../../common/popup/box-popup.service";
import {GRReportService} from './gr-report-service';
import {WMSPagination, AgGridExtent, WMSMessages, ReportFileExporter} from '../../common/directives/directives';
import 'ag-grid-enterprise/main';

declare var jQuery: any;
declare const $:any;

@Component({
	selector: 'gr-report',
	providers: [GRReportService, BoxPopupService],
	directives: [ROUTER_DIRECTIVES, AgGridExtent, WMSMessages, WMSPagination, ReportFileExporter],
	templateUrl: 'gr-report.component.html',

})
export class GRReportComponent {
	private pagination:any;
	private perPage:number = 20;
	private whs_id:string;
	private messages:any;
	private searchForm:ControlGroup;
	private sortData = {fieldname: 'updated_at', sort: 'desc'};
	private showLoadingOverlay:boolean = false;
	private grReports:Array<any> = [];
	private headerURL:string = this.apiService.API_User_Metas + '/shr';
	private columnData:any = {
		cus_code: {
			title: 'Customer Code',
			width: 120,
			pin: true,
			ver_table: '9'
		},
		cus_name: {
			title: 'Customer Name',
			width: 120,
			pin: true,
		},
		gr_hdr_num: {
			title: 'GR Num',
			width: 170,
			pin: true,
			id: true
		},
		ctnr_num: {
			title: 'Carrier Num',
			width: 50,
			pin: true
		},
		ref_code: {
			title: 'Ref Code',
			width: 50,
			pin: true
		},
		item_id: {
			title: 'Item ID',
			width: 50
		},
		sku: {
			title: 'SKU',
			width: 50
		},
		size: {
			title: 'Size',
			width: 50
		},
		color: {
			title: 'Color',
			width: 50
		},
		lot: {
			title: 'Lot',
			width: 50
		},
		pack_size: {
			title: 'Packsize',
			width: 50
		},
		upc: {
			title: 'UPC/EAN',
			width: 50
		},
		gr_qty: {
			title: 'Actual Qty',
			width: 70
		},
		gr_dtl_ept_ctn_ttl: {
			title: 'Expected CTNS',
			width: 70
		},
		gr_dtl_act_ctn_ttl: {
			title: 'Actual CTNS',
			width: 70
		},
		gr_dtl_disc_ctn_ttl: {
			title: 'Discrepany CTNS',
			width: 70
		},
		gr_dtl_plt_ttl: {
			title: 'PLT',
			width: 100
		},
		length: {
			title: 'Length',
			width: 70
		},
		width: {
			title: 'Width',
			width: 70
		},
		height: {
			title: 'Height',
			width: 70
		},
		weight: {
			title: 'Weight',
			width: 70
		},
		volume: {
			title: 'Volume',
			width: 70
		},
		crs_doc_qty: {
			title: 'X-Dock QTY',
			width: 70
		},
		gr_dtl_dmg_ttl: {
			title: 'Dam CTNS',
			width: 100
		},
		asn_hdr_ept_dt: {
			title: 'Expected Date',
			width: 70
		},
		asn_hdr_act_dt: {
			title: 'Goods Receipt Date',
			width: 100
		},
		updated_at: {
			title: 'Putaway Date',
			width: 100
		},
		updated_by: {
			title: 'GR By',
			width: 100
		},
		cube: {
			title: 'Cube',
			width: 100
		}
	};
	private customers:Array<any> = [];
	private ctnrNums:Array<any> = [];
	private isLoadingCTNRNums:boolean = false;
	private refCodes:Array<any> = [];
	private isLoadingRefCodes:boolean = false;
	private grNums:Array<any> = [];
	private isLoadingGRNums:boolean = false;
	private SKUs:Array<any> = [];
	private isLoadingSKUs:boolean = false;
	private currentSKU:any = {};

	// Check permission for user using this function page
	private canAccess:boolean = false;

	// Export csv file
	private exportAPIUrl:string = '';
	private fileName:string = 'Goods-Receipt-Report.csv';

	constructor(private formBuilder:FormBuilder,
	            private grReportService:GRReportService,
	            private func:Functions,
	            private userService:UserService,
	            private apiService:API_Config,
	            private router:Router) {
		this.buildSearchForm();
		this.whs_id = localStorage.getItem('whs_id');
		this.checkPermission();
		this.initializeCurSKU();
	}

	buildSearchForm() {
		this.searchForm = this.formBuilder.group({
			cus_id: [''],
			ctnr_num: [''],
			ref_code: [''],
			gr_hdr_num: [''],
			sku: ['']
		});
	}

	initializeCurSKU() {
		this.currentSKU = {size: '', color: '', lot: '', item_id: ''};
	}

	private checkPermission() {
		this.showLoadingOverlay = true;
		this.userService.GetPermissionUser().subscribe(
			data => {
				this.showLoadingOverlay = false;
				this.canAccess = this.userService.RequestPermission(data, 'viewReport');
				if (this.canAccess) {
					this.getCustomers();
					this.getGRReports();
				} else {
					// redirect to deny page.
					this.router.parent.navigateByUrl('/deny');
				}
			},
			err => {
				this.messages = this.func.Messages('danger', this.func.parseErrorMessageFromServer(err));
				this.showLoadingOverlay = false;
			}
		);
	}

	private changePageSize($event:any) {
		this.perPage = $event.target.value;
		if (this.pagination) {
			if ((this.pagination['current_page'] - 1) * this.perPage >= this.pagination['total']) {
				this.pagination['current_page'] = 1;
			}
			this.getGRReports(this.pagination['current_page']);
		}

	}

	private getGRReports(pageNum:number = 1) {
		this.searchForm.value['page'] = pageNum;
		this.searchForm.value['limit'] = this.perPage;
		this.searchForm.value['item_id'] = this.currentSKU['item_id'];
		this.searchForm.value['lot'] = this.currentSKU['lot'];
		this.searchForm.value['sort[updated_at]'] = 'desc';
		this.searchForm.value['updated_at_from'] = jQuery("#updated_at_from").val();
		this.searchForm.value['updated_at_to'] = jQuery("#updated_at_to").val();
		this.exportAPIUrl = this.grReportService.getReportAPIUrl(`?` + $.param(this.searchForm.value));

		this.showLoadingOverlay = true;
		this.grReportService.getReports(`?` + $.param(this.searchForm.value))
			.subscribe(
				data => {
					this.showLoadingOverlay = false;
					this.grReports = data.data;

					// paging
					this.pagination = data['meta']['pagination'];
					this.pagination['numLinks'] = 3;
					this.pagination['tmpPageCount'] = this.func.Pagination(this.pagination);
				},
				err => {
					this.messages = this.func.Messages('danger', err.json().errors.message);
					this.showLoadingOverlay = false;
				},
				() => {}
			);
	}

	private getCustomers() {
		const params:string = '?limit=200';
		this.grReportService.getCustomers(params).subscribe(
			data => {
				this.customers = data['data'];
			},
			err => {
				this.messages = this.func.Messages('danger', err.json().errors.message);
			},
			() => {
			}
		);
	}

	loadCTNRNums($event:any) {
		const cusId = this.searchForm.value['cus_id'];
		const ctnrNumKeySearch = this.searchForm.value['ctnr_num'].trim();
		const params = `?cus_id=${cusId}&ctnr_num=${ctnrNumKeySearch}&limit=20`;
		const enCodeSearchQuery = this.func.FixEncodeURI(params);

		if (!ctnrNumKeySearch) {
			return;
		}
		this.isLoadingCTNRNums = true;
		this.grReportService.getCTNRNums(enCodeSearchQuery).debounceTime(400).distinctUntilChanged().subscribe(
			data => {
				this.isLoadingCTNRNums = false;
				this.ctnrNums = data['data'];
			},
			err => {
				this.isLoadingCTNRNums = false;
				this.messages = this.func.Messages('danger', err.json().errors.message);
			},
			() => {
			}
		);
	}

	loadRefCodes($event:any) {
		const cusId = this.searchForm.value['cus_id'];
		const ctnrNum = this.searchForm.value['ctnr_num'];
		const refCodesKeySearch = this.searchForm.value['ref_code'].trim();
		const params = `?cus_id=${cusId}&ctnr_num=${ctnrNum}&ref_code=${refCodesKeySearch}&limit=20`;
		const enCodeSearchQuery = this.func.FixEncodeURI(params);

		if (!refCodesKeySearch) {
			return;
		}
		this.isLoadingRefCodes = true;
		this.grReportService.getRefCodes(enCodeSearchQuery).debounceTime(400).distinctUntilChanged().subscribe(
			data => {
				this.isLoadingRefCodes = false;
				this.refCodes = data['data'];
			},
			err => {
				this.isLoadingRefCodes = false;
				this.messages = this.func.Messages('danger', err.json().errors.message);
			},
			() => {
			}
		);
	}

	loadGRNums($event:any) {
		const cusId = this.searchForm.value['cus_id'];
		const ctnrNum = this.searchForm.value['ctnr_num'];
		const refCode = this.searchForm.value['ref_code'];
		const grNumsKeySearch = this.searchForm.value['gr_hdr_num'].trim();
		const params = `?cus_id=${cusId}&ctnr_num=${ctnrNum}&ref_code=${refCode}&gr_hdr_num=${grNumsKeySearch}&limit=20`;
		const enCodeSearchQuery = this.func.FixEncodeURI(params);

		if (!grNumsKeySearch) {
			return;
		}
		this.isLoadingGRNums = true;
		this.grReportService.getGRNums(enCodeSearchQuery).debounceTime(400).distinctUntilChanged().subscribe(
			data => {
				this.isLoadingGRNums = false;
				this.grNums = data['data'];
			},
			err => {
				this.isLoadingGRNums = false;
				this.messages = this.func.Messages('danger', err.json().errors.message);
			},
			() => {
			}
		);
	}

	loadSKUs($event:any) {
		this.initializeCurSKU();
		if (!this.searchForm.value['sku']) {
			return;
		}

		let querySKUsObj = Object.assign({}, this.searchForm.value);
		querySKUsObj['limit'] = 20;

		this.isLoadingSKUs = true;
		this.grReportService.getSKUs('?' + $.param(querySKUsObj)).debounceTime(400).distinctUntilChanged().subscribe(
			data => {
				this.isLoadingSKUs = false;
				this.SKUs = data['data'];
			},
			err => {
				this.isLoadingSKUs = false;
				this.messages = this.func.Messages('danger', err.json().errors.message);
			},
			() => {
			}
		);
	}

	reset() {
		this.resetSearchForm();
		this.initializeCurSKU();
		this.getGRReports();
	}

	search() {
		this.getGRReports();
	}

	selectCTNRNum($event, ctnrNum) {
		(<Control>this.searchForm.controls['ctnr_num']).updateValue(ctnrNum['ctnr_num']);
	}

	selectRefCode($event, refCode) {
		(<Control>this.searchForm.controls['ref_code']).updateValue(refCode['ref_code']);
	}

	selectGRNum($event, grNum) {
		(<Control>this.searchForm.controls['gr_hdr_num']).updateValue(grNum['gr_hdr_num']);
	}

	selectSKU($event, sku) {
		this.currentSKU = sku;
		(<Control>this.searchForm.controls['sku']).updateValue(sku['sku']);
	}

	resetSearchForm() {
		(<Control>this.searchForm.controls['cus_id']).updateValue('');
		(<Control>this.searchForm.controls['ctnr_num']).updateValue('');
		(<Control>this.searchForm.controls['ref_code']).updateValue('');
		(<Control>this.searchForm.controls['gr_hdr_num']).updateValue('');
		(<Control>this.searchForm.controls['sku']).updateValue('');
		jQuery("#updated_at_from").val("");
		jQuery("#updated_at_to").val("");
	}

	private updateMessages(messages) {
		this.messages = messages;
	}
}
