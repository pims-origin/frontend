import {Component} from '@angular/core';
import { FORM_DIRECTIVES,ControlGroup,FormBuilder, Control ,Validators} from '@angular/common';
import {RouteParams,Router ,RouteData} from '@angular/router-deprecated';
import {API_Config, UserService, Functions} from '../../../common/core/load';
import { ValidationService } from '../../../common/core/validator';
import { Http } from '@angular/http';
import { ControlMessages } from '../../../common/core/control-messages';
import {GoBackComponent} from "../../../common/component/goBack.component";
import {UOMServices} from "../uom-service";
import {ErrorMessageForControlService} from "../../../common/services/error-message-for-control.service";
import {BoxPopup} from "../../../common/popup/box-popup";
import {WarningBoxPopup} from "../../../common/popup/warning-box-popup";
import {BoxPopupService} from "../../../common/popup/box-popup.service";
import {JwtHelper} from "angular2-jwt/angular2-jwt";
import {WMSBreadcrumb,WMSMessages} from '../../../common/directives/directives';
declare var jQuery: any;

@Component({
    selector: 'cru-uom',
    providers: [UOMServices,ValidationService,FormBuilder,ErrorMessageForControlService, BoxPopupService],
    directives: [FORM_DIRECTIVES,ControlMessages,GoBackComponent,WMSBreadcrumb,WMSMessages],
    templateUrl: 'cru-uom.component.html',
})

export class CruUOMComponent {

    public loading = [];
    private messages;
    private action;
    private idDetail;
    private TitlePage='UOM';
    private isView:boolean=false;
    private uomDetail=[];
    private detail;
    public showLoadingOverlay = false;
    uomForm: ControlGroup;
    private submitted:boolean = false;

    constructor(
        private _http: Http,
        private _Func: Functions,
        private _user: UserService,
        private params: RouteParams,
        private _Valid: ValidationService,
        private _uomService: UOMServices,
        private _ErrMsgControlService: ErrorMessageForControlService,
        private _boxPopupService: BoxPopupService,
        private fb: FormBuilder,
        _RTAction: RouteData,
        private _router: Router) {

        this.action=_RTAction.get('action');
        this.checkPermission();
        this.getPackingLevel();
        this._ErrMsgControlService.errorMsg.invalidSpace = 'is required.';  // set error message for invalid space

        if(this.action=='view')
        {
            this.isView=true;
            this.TitlePage='View UOM';
        }
        if(this.action=='edit')
        {
            this.TitlePage='Edit UOM';
        }
        if(this.action=='edit'||this.action=='view')
        {
            // edit

            this.idDetail=this.params.get('id');
            this._uomService.getUOMDetail(this.idDetail).subscribe(
                data => {
                    this.detail=data;
                    this.FormBuilderEdit();
                },
                err => {
                    this._router.parent.navigateByUrl('/system-setup/item-master');
                },
                () => {}
            );

        }
        else{
            // Action new
            if(this.action=='new')
            {
                this.TitlePage='Add UOM';
                this.FormBuilder();
            }
            else{

            }
        }


    }

    private packingLevel = [];
    private getPackingLevel() {
        this._uomService.getPackingLevel().subscribe(
                data => {
                    this.packingLevel=data.data;
                }
            );
    }
    
    private checkPermission() {
        this.showLoadingOverlay = true;
        this._user.GetPermissionUser().subscribe(
            data => {
                if((this.action == 'view' && !this._user.RequestPermission(data,'viewUOM')) || (this.action == 'edit' && !this._user.RequestPermission(data,'editUOM')) || (this.action == 'new' && !this._user.RequestPermission(data,'createUOM'))) {
                    this._router.parent.navigateByUrl('/deny');
                }
                this.showLoadingOverlay = false;
            },
            err => {
                this.showMessage('danger', this._Func.parseErrorMessageFromServer(err));
                this.showLoadingOverlay = false;
            }
        );
    }

    FormBuilder()
    {
        //item_code,description,sku,size,color,uom,pack,length,width,height,weight,cus_id,condition
        this.uomForm =this.fb.group({
            sys_uom_code:['', Validators.compose([Validators.required, this._Valid.validateSpace, this.invalidLengthUOMCode])],
            sys_uom_name:['', Validators.compose([Validators.required, this._Valid.validateSpace, this._Valid.invalidNameStr])],
            sys_uom_type:[''],
            level_id:[''],
            sys_uom_des:[''],
        });
    }

    FormBuilderEdit()
    {
        this.uomForm =this.fb.group({
            sys_uom_code:[this.detail['sys_uom_code'], Validators.compose([Validators.required, this._Valid.validateSpace, this.invalidLengthUOMCode])],
            sys_uom_name:[this.detail['sys_uom_name'], Validators.compose([Validators.required, this._Valid.validateSpace, this._Valid.invalidNameStr])],
            sys_uom_type:[this.detail['sys_uom_type']],
            level_id:[this.detail['level_id'] || ''],
            sys_uom_des:[this.detail['sys_uom_des']]
        });
    }


    /*
     * Save
     * */
    save(data :Object):void{

        let n=this;
        this._ErrMsgControlService._genErrMsgForMultiControls(this.uomForm);

        this.submitted = true;
        this.checkValidUOMLevel();
        if(this.uomForm.valid) {
            // trim input of 2 fields
            this._trimObjProperty(data, ['sys_uom_code', 'sys_uom_name']);

            let data_json = JSON.stringify(data);

            if (this.action == 'new') {
                // add new
                this.addNew(data_json);
            }
            else {
                // edit zone type
                this.update(this.idDetail,data_json);
            }

        }
        // else{
        //     this.messages = this._Func.Messages('danger', this._Func.msg('VR108'));
        // }

    }

    /*
     *
     * */
    addNew(data)
    {
        let n=this;
        this.loading['AddUpdate']=true;
        this._uomService.addNewUOM(data).subscribe(
            data => {
                this.loading['AddUpdate'] = false;
                //Reset Form
                this.messages = this._Func.Messages('success', this._Func.msg('VR107'));
                setTimeout(function(){
                    n._router.parent.navigateByUrl('/master-data/uom');
                }, 600);
            },
            err => {
                this.loading['AddUpdate'] = false;
                this.showMessage('danger', this._Func.parseErrorMessageFromServer(err));
            },
            () => {}
        );
    }

    update(id,data)
    {
        let n=this;
        this.loading['AddUpdate']=true;
        this._uomService.updateUOM(id,data).subscribe(
            data => {
                this.loading['AddUpdate']=false;
                //Reset Form
                this.messages = this._Func.Messages('success', this._Func.msg('VR109'));
                setTimeout(function(){
                    n._router.parent.navigateByUrl('/master-data/uom');
                }, 600);
            },
            err => {
                this.loading['AddUpdate']=false;
                this.showMessage('danger', this._Func.parseErrorMessageFromServer(err));
            },
            () => {}
        );
    }

    cancel()
    {
        let n = this;
        /*case 1: modify text*/
        // let warningPopup = new WarningBoxPopup();
        // warningPopup.text = "Are you sure to cancel";
        // this._boxPopupService.showWarningPopup(warningPopup)

        /*case 2: default*/
        this._boxPopupService.showWarningPopup()
            .then(function (dm) {
                if(dm)
                    n._router.navigateByUrl('master-data/uom');

            });
    }

    detectControlErr(control) {
        // this._genErrMsgForControl(control, this.errorMsg);
        this._ErrMsgControlService._genErrMsgForControl(control);
    }

    /*
     * obj: object
     * properties: keys of object need to trim
     * */
    _trimObjProperty(obj, properties)
    {
        properties.forEach(function(prop) {
            obj[prop] = obj[prop].trim();
        });
    }

    private showMessage(msgStatus, msg) {
        // if(this.timeoutHideMessage) clearTimeout(this.timeoutHideMessage);
        this.messages = {
            'status': msgStatus,
            'txt': msg
        };
        // this.timeoutHideMessage = setTimeout(() => {
        //     this.messages = null;
        // }, 4000);
        jQuery(window).scrollTop(0);
    }

    private invalidLengthUOMCode(control:Control) {
        if (!control.value) {
            return null;
        }

        const errObj = {invalidLengthUOMCode: true};
        const valLength = control.value.length;
        if (jQuery('#sys_uom_type').val() === 'Item') {
            return !/^[a-zA-Z0-9]{2}$/i.test(control.value) ? errObj : null;
        } else {
            return !/^[a-zA-Z0-9]{2,3}$/i.test(control.value) ? errObj : null;
        }
    }
    
    private checkMaxLength($event, data) {
        // Check Lengh 2
        return data.value.sys_uom_type == 'Item' ? data.value.sys_uom_code.length < 2 : data.value.sys_uom_code.length < 3;

    }

    private resetUOMCodebyLenght($event, data) {

        jQuery("#sys_uom_code").attr('maxlength', 10);
        if($event.target.value == 'Item') {

            jQuery("#sys_uom_code").attr('maxlength', 2);
            (<Control>this.uomForm.controls['sys_uom_code']).updateValue(this.uomForm.value.sys_uom_code.substr(0, 2));

        }

        if (this.action == 'edit' && $event.target.value != 'Item'){

            (<Control>this.uomForm.controls['sys_uom_code']).updateValue(this.detail['sys_uom_code']);

        }
    }
    
    private checkValidUOMLevel() {
        if(this.uomForm.value.sys_uom_type == 'Item') {
            if(!this.uomForm.value.level_id) {
                (<Control>this.uomForm.controls['level_id']).setErrors({'required': true});
            }
            else {
                (<Control>this.uomForm.controls['level_id']).setErrors(null);
            }
        }
        else {
            (<Control>this.uomForm.controls['level_id']).setErrors(null);
        }
    }
    
    private changePackingLevel(value) {
        setTimeout(() => {
            if(value) {
                (<Control>this.uomForm.controls['level_id']).setErrors(null);
            }
            else {
                (<Control>this.uomForm.controls['level_id']).setErrors({'required': true});
            }
        })
    }
}

