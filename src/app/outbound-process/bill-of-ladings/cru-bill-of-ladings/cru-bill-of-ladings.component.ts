import {Component, Input} from '@angular/core';
import {ControlGroup, FORM_DIRECTIVES, FormBuilder, Validators, Control} from "@angular/common";
import {ValidationService} from "../../../common/core/validator";
import {MapValuesPipe} from "../../../common/pipes/map.pipe";
import {BillOfLadingService} from "../bill-of-lading-service";
import {OrderBy} from "../../../common/pipes/order-by.pipe";
import {UserService, Functions, FormBuilderFunctions} from '../../../common/core/load';
import {WarningBoxPopup} from "../../../common/popup/warning-box-popup";
import {BoxPopupService} from "../../../common/popup/box-popup.service";
import {RouteParams,Router ,RouteData} from '@angular/router-deprecated';
import {API_Config} from "../../../common/core/API_Config";
import {
  PaginatePipe, PaginationService, PaginationControlsCmp,
  WMSPagination, MasterPalletListDirective, WMSMessages, WMSBreadcrumb, DocumentUpload, DmsIn,
  ThirdPartyFormDirective, AutocompleteDirective
} from '../../../common/directives/directives';
import {DeliveryServiceForm} from "../../../master-data/delivery-service/delivery-form";
declare var jQuery: any;
declare var saveAs: any;
@Component({
  selector: 'cru-bill-of-ladings',
  templateUrl: 'cru-bill-of-ladings.component.html',
  directives: [
    FORM_DIRECTIVES,DeliveryServiceForm, PaginationControlsCmp,WMSMessages,WMSBreadcrumb,DocumentUpload, DmsIn,
    WMSPagination, MasterPalletListDirective, ThirdPartyFormDirective, AutocompleteDirective],
  providers: [ValidationService, FormBuilder, BillOfLadingService, PaginationService,BoxPopupService],
  pipes: [MapValuesPipe, OrderBy, PaginatePipe]
})

export class CruBillOfLadingsComponent {
  bOLForm:ControlGroup;
  private listNull = {};
  private commoDesc = [];
  private deliveryService = [];
  private trailerLoad = [];
  private freCounted = [];
  private freChTerms = [];
  private feeTerms = [];
  private shipMethod = [];
  private dataListCustomer = [];
  private addressList = [];

  private ordersListForPopup = [];
  private ordersListForPopupTemp = [];
  private orderNumberFilter = '';
  //private loading = {};
  private selectedAll = {};
  private addedOrders = [];
  private cusID = '';//IMPORTANT => use for search
  private ordID = '';//IMPORTANT => use for search

  private loading = [];
  private states = [];
  private messages;
  private perPage = 20;
  private submitForm = false;
  private messagesText;
  private addCommodityMessage;
  private Action;
  private bolID = '';
  private IsView = false;
  private isShowBtnPrint = false;
  private isEdit = false;
  private isNew = false;
  private showLoadingOverlay:boolean=false;
  // Check permission for user using this function page
  private createBol;
  private viewBol;
  private editBol;
  private canFinalBOL:boolean = false;
  private allowAccess:boolean=false;
  private bolDetailData={};
  private pageTitle;
  private token = '';
  private isShowDocTab = false;
  private cusNameforDoc = '';
  private masterPalletList = [];
  private messages_delivery='';
  public Loading = [];
  private ship_to_name_current={};
  private Country = [];
  private StateProvinces = [];

  constructor(private fb:FormBuilder, private _Valid:ValidationService,
              private _bolService:BillOfLadingService, private _Func: Functions,
              private _boxPopupService: BoxPopupService, private _router: Router,
              _RTAction: RouteData, private _user:UserService, private _api: API_Config,
              private fbdFunc:FormBuilderFunctions,
              private params: RouteParams) {
    this.token = this._Func.getToken();
    this.Action=_RTAction.get('action');
    switch (this.Action) {
      case "new":
        this.isNew = true;
        this.pageTitle = 'ADD BILL OF LADING';
        break;
      case "edit":
        this.isEdit = true;
        this.pageTitle = 'EDIT BILL OF LADING';
        break;
      case "view":
        this.IsView = true;
        this.pageTitle = 'VIEW BILL OF LADING';
        break;
    }
    this.checkPermission();
    // this.formBuilder();
    this.GetCountry();
    if (this.isEdit) {
      this.formBuilderEdit();
    } else {
      this.formBuilderViewNew();
    }
    // this.GetState('US');
    this.buildAddCommodityForm();
  }

  ngAfterViewInit() {
    var that = this;
    var selectShipTo =  jQuery('[name="select-ship-to"]');
    selectShipTo.on('change.updateText', function() {
      var selectedItem = selectShipTo.find('option:selected'),
          carrier = selectedItem.attr('id'),
          shipMethod = selectedItem.attr('class');
      (<Control>that.bOLForm.controls['carrier']).updateValue(carrier);
      if(shipMethod) {
        (<Control>that.bOLForm.controls['ship_method']).updateValue(shipMethod);
      }
    });

  }

  private formBuilderViewNew() {
    this.bOLForm = this.fb.group(
      {
        cus_id: ['', Validators.required],
        ship_ready: ['', Validators.required],
        ship_to_name: [''],
        ship_to: [''],
        bo_label: ['', Validators.compose([Validators.required, this._Valid.validateSpace])],
        special_inst: ['', Validators.compose([Validators.required, this._Valid.validateSpace])],
        carrier: ['', Validators.compose([Validators.required, this._Valid.validateSpace])],
        deli_service: ['NA'],
        party_acc: ['', Validators.compose([Validators.required, this._Valid.validateSpace])],
        cmd_des: ['', Validators.required],
        freight_counted_by: ['' , Validators.required],
        freight_charge_terms: ['', Validators.required],
        freight_charge_cost: [''],
        trailer_loaded_by: ['', Validators.required],
        fee_terms: ['', Validators.required],
        cus_accept: [false],
        is_attach: [false],
        ship_method: [''],
        trailer_num: [''],
        seal_num: [''],
        scac: [''],
        pro_num: [''],
        bill_to_name: [''],
        bill_to_addr_1: [''],
        bill_to_city: [''],
        bill_to_state: ['' ],
        bill_to_zip: [''],
        bol_type: ['0']
      }
    );
  }

  private formBuilderEdit() {
    this.bOLForm = this.fb.group(
      {
        cus_id: ['', Validators.required],
        ship_ready: ['', Validators.required],
        ship_to: [''],
        bo_label: ['', Validators.compose([Validators.required, this._Valid.validateSpace])],
        special_inst: ['', Validators.compose([Validators.required, this._Valid.validateSpace])],
        carrier: ['', Validators.compose([Validators.required, this._Valid.validateSpace])],
        deli_service: ['NA'],
        party_acc: ['', Validators.compose([Validators.required, this._Valid.validateSpace])],
        cmd_des: ['', Validators.required],
        freight_counted_by: ['' , Validators.required],
        freight_charge_terms: ['', Validators.required],
        freight_charge_cost: [''],
        trailer_loaded_by: ['', Validators.required],
        fee_terms: ['', Validators.required],
        cus_accept: [false],
        is_attach: [false],
        ship_method: [''],
        trailer_num: [''],
        seal_num: [''],
        scac: [''],
        pro_num: [''],
        bill_to_name: [''],
        bill_to_addr_1: [''],
        bill_to_city: [''],
        bill_to_state: ['' ],
        bill_to_zip: [''],
        ship_to_name: [''],
        ship_to_state: [''],
        ship_to_zip: [''],
        ship_to_city: [''],
        ship_to_country: ['US'],
        ship_to_addr_1: [''],
        ship_to_addr_2: [''],
        bol_type: ['0']
      }
    );
  }

  private createDeliveryService = false;

  //private userRoles:any;
  private checkPermission(){
    this.showLoadingOverlay = true;
    this._user.GetPermissionUser().subscribe(
      data => {
        this.createBol = this._user.RequestPermission(data,'createBOL');
        this.viewBol = this._user.RequestPermission(data,'viewBOL');
        this.editBol = this._user.RequestPermission(data,'editBOL');
        this.canFinalBOL = this._user.RequestPermission(data,'finalBOL');
        this.createDeliveryService = this._user.RequestPermission(data, 'createDeliveryService');
        //this.userRoles = this._Func.createObjectData(this._user.User['user_roles'],'code','name');

        if((this.Action=='new' && !this.createBol) || (this.Action=='edit' && !this.editBol) || (this.Action=='view' && !this.viewBol)) {
            this._router.parent.navigateByUrl('/deny');
            this.showLoadingOverlay = false;
        }
        else {
          this.loadDropDown();
          if(this.Action=='new')
          {
            (<Control>this.bOLForm.controls['ship_ready']).updateValue('all');
            this.cusID=this.params.get('cusId');
            this.ordID=this.params.get('id');
            if(this.cusID && this.ordID) {
              (<Control>this.bOLForm.controls['cus_id']).updateValue(this.cusID);

              // prefill ship to
              this.renderFullAddress(this.cusID,'all',this.ordID)
                .then((argument) => {
                  for(let i =0;i < this.addressList.length; i ++){
                    let arrValue = this.addressList[i].key.split(',');
                    if(arrValue.indexOf(this.ordID) != -1) {
                      (<Control>this.bOLForm.controls['ship_to']).updateValue(this.addressList[i].key);
                      (<Control>this.bOLForm.controls['ship_to_name']).updateValue(this.addressList[i].company);
                      setTimeout(() => {
                        var shipToElm = jQuery('[name="select-ship-to"]'),
                            selectedItem = shipToElm.find('option:selected'),
                            carrier = selectedItem.attr('id'),
                            shipMethod = selectedItem.attr('class');
                        // (<Control>this.bOLForm.controls['carrier']).updateValue(carrier);
                        if(shipMethod) {
                          (<Control>this.bOLForm.controls['ship_method']).updateValue(shipMethod);
                        }
                      });
                    }
                  }
                })
              this.showLoadingOverlay = true;
              this._bolService.getOrdersList(this.cusID, this.ordID)
                .subscribe(
                  data => {
                    this.addedOrders = data.data;
                    this.showLoadingOverlay = false;
                  },
                  err => {
                    this.showLoadingOverlay = false;
                  }
                );
              this._bolService.getCarrierByOrderId(this.cusID, this.ordID)
                .subscribe(
                  (data) => {
                    (<Control>this.bOLForm.controls['carrier']).updateValue(data.data.carrier);
                  }
                )
            }
            else {
              this.showLoadingOverlay = false;
            }
          }

          if(this.Action=='edit')
          {
            this.getBOLDetail();
          }

          if(this.Action=='view')
          {
            this.getBOLDetail();
            this.buildFormPrintBOLLPN();
          }
        }
      },
      err => {
        this.messagesText = this._Func.Messages('danger', this._Func.parseErrorMessageFromServer(err));
        this.showLoadingOverlay = false;
      }
    );
  }

  

  private getBOLDetail(){
    this.showLoadingOverlay = true;
    this.bolID=this.params.get('id');
    this._bolService.getBOLItem(this.bolID).subscribe(
      data => {
        this.bolDetailData = data;
        // if(data.ship_sts == 'FN') {
          this.isShowBtnPrint = true;
        // }
        this.GetState(this.bolDetailData['ship_to_country']);
        this.formBuilder(data);
        this.showLoadingOverlay = false;
        this.isShowDocTab = true;
        this.cusNameforDoc = this._Func.selectCustomerNameFromID(this.bolDetailData['cus_id'],this.dataListCustomer);
      },
      err => {
        this.showLoadingOverlay = false;
      },
      () => {}
    );

  }

  private formBuilder(data = {}) {
    let ship_ready = data['ship_to_addr'] != '' ? 'all': '';
    let cus_id = data['cus_id'] != undefined ? data['cus_id']: '';
    let order_id = [];
    let id_ship_to = '';
    if(data['items']){
      for(let i = 0; i < data['items'].length; i ++){
        this.addedOrders.push(data['items'][i]);
        order_id.push(data['items'][i]['odr_id']);
      }
      let ords = order_id.length > 0 ? order_id.join(): '';
      this.renderFullAddress(cus_id,ship_ready,ords)
        .then((argument) => {
          var firstOrderID = data['items'][0].odr_id.toString();
          for(let i =0;i < this.addressList.length; i ++){
            let arrValue = this.addressList[i].key.split(',');
            if(arrValue.indexOf(firstOrderID) != -1) {
              id_ship_to = this.addressList[i].key;
            }
          }
          this.renderDataToForm(data, id_ship_to);
        })
    }
    else {
      this.renderDataToForm(data, id_ship_to);
    }

  }

  private renderDataToForm(data, id_ship_to) {
    (<Control>this.bOLForm.controls['cus_id']).updateValue(data['cus_id'] ? data['cus_id']: '');
    (<Control>this.bOLForm.controls['ship_ready']).updateValue(data['ship_to_addr'] != '' ? 'all': '');
    (<Control>this.bOLForm.controls['ship_to_name']).updateValue(data['ship_to_name']);
    (<Control>this.bOLForm.controls['ship_to']).updateValue(id_ship_to != '' ? id_ship_to : '');
    (<Control>this.bOLForm.controls['bo_label']).updateValue(data['bo_label']);
    (<Control>this.bOLForm.controls['special_inst']).updateValue(data['special_inst']);
    (<Control>this.bOLForm.controls['carrier']).updateValue(data['carrier']);
    (<Control>this.bOLForm.controls['deli_service']).updateValue(data['deli_service']);
    (<Control>this.bOLForm.controls['party_acc']).updateValue(data['party_acc']);
    (<Control>this.bOLForm.controls['cmd_des']).updateValue(data['cmd_id'] ? data['cmd_id']:'');
    (<Control>this.bOLForm.controls['freight_counted_by']).updateValue(data['freight_counted_by'] != undefined ? data['freight_counted_by']:'');
    (<Control>this.bOLForm.controls['freight_charge_terms']).updateValue(data['freight_charge_terms'] != undefined ? data['freight_charge_terms']:'');
    (<Control>this.bOLForm.controls['freight_charge_cost']).updateValue(data['freight_charge_cost']);
    (<Control>this.bOLForm.controls['trailer_loaded_by']).updateValue(data['trailer_loaded_by'] != undefined ? data['trailer_loaded_by']:'');
    (<Control>this.bOLForm.controls['fee_terms']).updateValue(data['fee_terms'] ? data['fee_terms']:'');
    (<Control>this.bOLForm.controls['cus_accept']).updateValue(data['cus_accept'] == 1 ? true: false);
    (<Control>this.bOLForm.controls['is_attach']).updateValue(data['is_attach'] == 1 ? true: false);
    (<Control>this.bOLForm.controls['ship_method']).updateValue(data['ship_method'] ? data['ship_method']:'');
    (<Control>this.bOLForm.controls['trailer_num']).updateValue(data['trailer_num']);
    (<Control>this.bOLForm.controls['seal_num']).updateValue(data['seal_num']);
    (<Control>this.bOLForm.controls['scac']).updateValue(data['scac']);
    (<Control>this.bOLForm.controls['pro_num']).updateValue(data['pro_num']);
    (<Control>this.bOLForm.controls['bill_to_name']).updateValue(data['bill_to_name']);
    (<Control>this.bOLForm.controls['bill_to_addr_1']).updateValue(data['bill_to_addr_1']);
    (<Control>this.bOLForm.controls['bill_to_city']).updateValue(data['bill_to_city']);
    (<Control>this.bOLForm.controls['bill_to_state']).updateValue(data['bill_to_state'] != undefined ? data['bill_to_state']:'');
    (<Control>this.bOLForm.controls['bill_to_zip']).updateValue(data['bill_to_zip']);
    (<Control>this.bOLForm.controls['bol_type']).updateValue(data['bol_type']);

    if (this.isEdit) {
      (<Control>this.bOLForm.controls['ship_to_addr_1']).updateValue(data['ship_to_addr_1']);
      (<Control>this.bOLForm.controls['ship_to_city']).updateValue(data['ship_to_city']);
      (<Control>this.bOLForm.controls['ship_to_state']).updateValue(data['ship_to_state']);
      (<Control>this.bOLForm.controls['ship_to_country']).updateValue(data['ship_to_country']);
      (<Control>this.bOLForm.controls['ship_to_zip']).updateValue(data['ship_to_zip']);
    }
  }

  redirectDeny(){
    this._router.parent.navigateByUrl('/deny');
  }

  private doubleClickEvent($event) {
    $event.toElement.readOnly = false;
  }

  private validateNumber($target) {
    this.listNull[$target.id + "_float"] = false;
    this.listNull[$target.id + "_max999"] = false;
    this.listNull[$target.id + "_greater0"] = false;
    if ($target.value != "" && (!this.validateFloat($target.value))) {

      this.listNull[$target.id + "_float"] = true;

    } else if (parseFloat($target.value) > 99999999.99) {

      this.listNull[$target.id + "_max999"] = true;

    } else if (parseFloat($target.value) <= 0 && $target.value != "") {

      this.listNull[$target.id + "_greater0"] = true;
    }
  }

  private validateFloat(float) {
    var re = /^([0-9,.]*)$/;
    return re.test(float);

  }

  // Filter form only show choose one
  private filterFormFloat($event) {
    let $target = $event.target;
    // $target.readOnly = true;
    try {
      if (this.validateMultidot($target.value) || this.validateFirstdot($target.value) || this.validateLastdot($target.value)) {
        this.listNull[$target.id + "_float"] = true;
      }
      return false;
    } catch (err) {
      return false;
    }
  }

  // value = .. return true
  private validateMultidot(float) {

    var rgx = /\.\./;
    return (rgx.test(float));
  }

  // value = .. return true
  private validateFirstdot(float) {

    var rgx = /^([\.|\,]+)([0-9,.]*)/;
    return (rgx.test(float));
  }

  // value = .. return true
  private validateLastdot(float) {

    var rgx = /([0-9,.]*)([\.|\,]+)$/;
    return (rgx.test(float));
  }

  private checkInputNumber(evt, onlyNumber) {
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      if (onlyNumber) {
        evt.preventDefault();
      }
      else {
        if (charCode != 46) {
          evt.preventDefault();
        }
      }
    }
  }

  private loadDropDown() {
    this._bolService.getCommodityDescList().subscribe(
      data => {
        this.commoDesc = data.data;
      }
    );
    this.getDeliveryService();
    this._bolService.getTrailerLoad().subscribe(
      data => {
        this.trailerLoad = data.data;
      }
    );
    this._bolService.getFreCounted().subscribe(
      data => {
        this.freCounted = data.data;
      }
    );
    this._bolService.getFreChTerms().subscribe(
      data => {
        this.freChTerms = data.data;
      }
    );
    this._bolService.getFeeTerms().subscribe(
      data => {
        this.feeTerms = data.data;
      }
    );
    this._bolService.getShipMethod().subscribe(
      data => {
        this.shipMethod = data.data;
      }
    );

    let params = "?limit=10000";
    this._bolService.getListCustomersByUser(params).subscribe(
      data => {
        this.dataListCustomer = data.data;
      }
    );

    this._bolService.loadUSACountry('US','?sort[sys_state_name]=asc&limit=500').subscribe(
      data => {
        this.states = data;
      }
    );
  }

  private getDeliveryService(){
    let param="?limit=9999";
    this._bolService.getDeliveryService(param).subscribe(
        data => {
          this.deliveryService = data.data;
        }
    );
  }

  private submitDeliveryData:boolean=false;
  private _submitDeliveryData(){
    this.submitDeliveryData=true;
    setTimeout(()=>{
      this.submitDeliveryData=false;
    })
  }

  private closeDeliveryPopup(){
    setTimeout(()=>{
      jQuery('#add-delivery-service').modal('hide');
      this.messages_delivery='';
    },1000);
  }

  private renderFullAddress(cus_id, ship_ready, order_id = '') {
    (<Control>this.bOLForm.controls['ship_to']).updateValue('');
    return new Promise((resolve, reject) => {
      if (cus_id != '' && ship_ready != '') {
        this.loading['ship_to'] = true;
        this._bolService.getFullAddressList(cus_id, ship_ready,order_id)
          .subscribe(
            data => {
              this.loading['ship_to'] = false;
              this.addressList = data.data;
              resolve(true);
            },
            err=> {
              this.loading['ship_to'] = false;
              resolve(false);
            }
          );
      } else {
        this.addressList = [];
        resolve(false);
      }
    });

  }

  private renderOrdersData() {
    let cusID = jQuery('[name="select-customer"]').val();
    let ordID = this.bOLForm.value['ship_to'];
    this.orderNumberFilter = '';
    if(this.Action == 'edit') {

      this.loading['ordersListForPopup'] = true;
      this._bolService.getOrdersListByBol(cusID, this.bolID)
        .subscribe(
          data => {
            let temp = [];
            temp = temp.concat(data.data);
            this.ordersListForPopup = this.filterShowDataListPopup(temp.slice(0));
            for(let i = 0,l = this.ordersListForPopup.length; i < l; i++) {
              this.ordersListForPopup[i].selected = false;
            }
            this.ordersListForPopupTemp = this.ordersListForPopup.slice();
            this.loading['ordersListForPopup'] = false;
          },
          err => {
            this.loading['ordersListForPopup'] = false;
          }
        );
        jQuery('#addOrder').modal('show');
    }
    else {
      ordID = ordID ? ordID : '0';
      if (cusID && ordID) {
        this.loading['ordersListForPopup'] = true;
        this.ordID = ordID;
        this._bolService.getOrdersListSameCustomer(cusID, ordID)
          .subscribe(
            data => {
              let temp = [];
              temp = temp.concat(data.data);
              this.ordersListForPopup = this.filterShowDataListPopup(temp.slice(0));
              for(let i = 0,l = this.ordersListForPopup.length; i < l; i++) {
                this.ordersListForPopup[i].selected = false;
              }
              this.ordersListForPopupTemp = this.ordersListForPopup.slice();
              this.loading['ordersListForPopup'] = false;
            },
            err => {
              this.loading['ordersListForPopup'] = false;
            }
          );
        jQuery('#addOrder').modal('show');
      }
    }
  }

  private filterShowDataListPopup(array_data) {
    let ListFilter=this.addedOrders;
    if(ListFilter.length)
    {
      for(var i in ListFilter)
      {
        for(let j=0; j < array_data.length; j++)
        {
          if(ListFilter[i]['odr_id']==array_data[j]['odr_id'])
          {
            let el = array_data.indexOf(array_data[j]);
            array_data.splice(el, 1);
          }
        }
      }
    }

    return array_data;

  }

  private selected($event, item, ListName:string, state, paginId) {

    let Array = eval(`this.` + ListName);
    this.selectedAll[ListName] = false;
    if ($event.target.checked) {
      item['selected'] = true;
      this.checkCheckAll(ListName, state, paginId);
    }
    else {
      item['selected'] = false;
    }
  }

  private checkCheckAll(ListName:string, StatePagin, objName) {
    let Array = eval(`this.` + ListName);
    let el = StatePagin[objName];
    let end = el['end'];
    let chk = 0;
    if (el['size']) {
      if (el['size'] < el['end']) {
        end = el['size'];
      }

      for (let i = el['start']; i < end; i++) {
        if (Array[i]['selected']) {
          chk++;
        }
      }
      if (chk == el['slice'].length) {
        this.selectedAll[ListName] = true;
      }
      else {
        this.selectedAll[ListName] = false;
      }
      // console.log('check all of list ' , ListName ,  this.selectedAll[ListName] );
    } // end if size
  }

  private CheckAll(event, ListName:string, StatePagin, objName) {

    let Array = eval(`this.` + ListName);

    if (!Array.length) {
      this.selectedAll[ListName] = false;
      return;
    }
    if (event) {
      this.selectedAll[ListName] = true;
    }
    else {
      this.selectedAll[ListName] = false;
    }

    //console.log('List checkall from ',StatePagin ,  StatePagin[objName]['start'] , ' to ' , StatePagin[objName]['end']);
    let end = StatePagin[objName]['end'];
    if (StatePagin[objName]['size'] < StatePagin[objName]['end']) {
      end = StatePagin[objName]['size'];
    }
    //console.log('end -> ' , end);
    for (let i = StatePagin[objName]['start']; i < end; i++) {
      //console.log(' i ' , i ,  Array[i]);
      Array[i]['selected'] = this.selectedAll[ListName];
    }
  }

  private saveBOL(param = '') {
    let that = this;
    this.submitForm = true;
    let isSave = true;
    if(this.bOLForm.valid) {
      let orderList = [];
      if(this.addedOrders.length > 0){
        for (let i = 0; i < this.addedOrders.length; i++) {
          let item = {
            odr_id: this.addedOrders[i].odr_id,
            odr_hdr_num : this.addedOrders[i].odr_hdr_num,
            cus_odr: this.addedOrders[i].cus_odr,
            cus_po: this.addedOrders[i].cus_po,
            cus_dept: this.addedOrders[i].cus_dept,
            cus_ticket: this.addedOrders[i].cus_ticket,
            plts: this.addedOrders[i].plts,
            pkgs: this.addedOrders[i].pkgs,
            units: this.addedOrders[i].units,
            weight: this.addedOrders[i].weight,
            add_shipper_info: this.addedOrders[i].add_shipper_info ? this.addedOrders[i].add_shipper_info : null,
          }
          orderList.push(item);
        }

        let dataToPost = {
          cus_id: this.bOLForm.value['cus_id'],
          bill_to_name: this.bOLForm.value['bill_to_name'],
          bill_to_addr_1: this.bOLForm.value['bill_to_addr_1'],
          bill_to_city: this.bOLForm.value['bill_to_city'],
          bill_to_state: this.bOLForm.value['bill_to_state'],
          bill_to_zip: this.bOLForm.value['bill_to_zip'],
          special_inst: this.bOLForm.value['special_inst'],
          carrier: this.bOLForm.value['carrier'],
          deli_service: this.bOLForm.value['deli_service'],
          party_acc: this.bOLForm.value['party_acc'],
          cmd_des: this.bOLForm.value['cmd_des'],
          freight_counted_by: this.bOLForm.value['freight_counted_by'],
          freight_charge_terms: this.bOLForm.value['freight_charge_terms'],
          freight_charge_cost: this.bOLForm.value['freight_charge_cost'],
          trailer_loaded_by: this.bOLForm.value['trailer_loaded_by'],
          fee_terms: this.bOLForm.value['fee_terms'],
          cus_accept: this.bOLForm.value['cus_accept'] ? 1 : 0,
          is_attach: this.bOLForm.value['is_attach'] ? 1 : 0,
          ship_method: this.bOLForm.value['ship_method'],
          trailer_num: this.bOLForm.value['trailer_num'],
          seal_num: this.bOLForm.value['seal_num'],
          scac: this.bOLForm.value['scac'],
          pro_num: this.bOLForm.value['pro_num'],
          bo_label: this.bOLForm.value['bo_label'],
          bol_type: this.bOLForm.value['bol_type'],
          items: orderList
        };

        if(param == 'final'){
          dataToPost['ship_sts'] = 'FN';
          isSave = false;
        }
        // Check if param = New
        if(param == 'new'){
          dataToPost['ship_sts'] = 'NW';
          isSave = true;
        }
        this.showLoadingOverlay = true;
        if(this.isNew){
          this._bolService.createBolV2(this._Func.lstGetItem('whs_id'), JSON.stringify(dataToPost))
            .subscribe(
              data => {
                this.showLoadingOverlay = false;
                this.messagesText = this._Func.Messages('success', this._Func.msg('VR109'));
                var urlRedirect = isSave ? '/outbound/bill-of-ladings/' + data.ship_id + '/edit' : '/outbound/bill-of-ladings/' + data.ship_id;
                setTimeout(() => {
                  that._router.parent.navigateByUrl(urlRedirect);
                  // window.location.reload();
                }, 1000);
              },
              err => {
                this.messagesText = this._Func.Messages('danger', this._Func.parseErrorMessageFromServer(err));
                this.showLoadingOverlay = false;
              }
            );
        }

        if(this.isEdit){
          dataToPost['ship_to_name'] = this.bOLForm.value['ship_to_name'];
          dataToPost['ship_to_addr_1'] = this.bOLForm.value['ship_to_addr_1'];
          dataToPost['ship_to_addr_2'] = this.bOLForm.value['ship_to_addr_2'];
          dataToPost['ship_to_city'] = this.bOLForm.value['ship_to_city'];
          dataToPost['ship_to_state'] = this.bOLForm.value['ship_to_state'];
          dataToPost['ship_to_country'] = this.bOLForm.value['ship_to_country'];
          dataToPost['ship_to_zip'] = this.bOLForm.value['ship_to_zip'];
          dataToPost['tp_id'] = this.ship_to_name_current['tp_id'];
          this._bolService.updateBol2(this._Func.lstGetItem('whs_id'), this.bolID, JSON.stringify(dataToPost))
            .subscribe(
              data => {
                this.showLoadingOverlay = false;
                this.messagesText = this._Func.Messages('success', this._Func.msg('VR109'));
                if(!isSave) {
                  setTimeout(() => {
                    that._router.parent.navigateByUrl('/outbound/bill-of-ladings/' + data.ship_id);
                    window.location.reload();
                  }, 1000);
                }
              },
              err => {
                this.messagesText = this._Func.Messages('danger', this._Func.parseErrorMessageFromServer(err));
                this.showLoadingOverlay = false;
              }
            );
        }


      }else{
        this.messagesText = this._Func.Messages('danger', this._Func.msg('BOL003'));
      }
    }
  }

  private addOrders() {
    this.selectedAll['ListDataPopup']=false;

    let listItemRemove = [];
    this.ordersListForPopupTemp.forEach((item, idx)=>{
        if(item['selected'])
        {
          item['selected']=false;
          this.addedOrders.push(item);
          listItemRemove.push(idx);
        }
      });
      if(listItemRemove.length) {
        for(let i = listItemRemove.length - 1; i >= 0; i--) {
          this.ordersListForPopupTemp.splice(listItemRemove[i],1);
        }
      }
  }

  private removeOrdersFromOrderList(ListName:string,PaginId,IsControlGroupForm=false) {
    // destroy messages
    this.messages=false;
    // create array temp
    let Arr = eval(`this.` + ListName);
    let ArrTemp = Arr.slice();
    /*
     * Check any item has value check
     * */
    let f = 0;
    for (let i = 0; i < Arr.length; i++) {
      if (Arr[i]['selected'] == true) {
        f++;
        break;
      }
    }
    if (f == 0) {
      this.messages = this._Func.Messages('danger', this._Func.msg('VR113'));
    }
    else{
      let n = this;
      let warningPopup = new WarningBoxPopup();
      warningPopup.text = this._Func.msg('BOL002');
      this._boxPopupService.showWarningPopup(warningPopup)
        .then(function (yes) {
          if(!yes){
            return;
          } else{
            n.selectedAll={};
            let i = 0;
            ArrTemp.forEach((item)=> {
              // find item selected and remove it
              if (item['selected'] == true) {
                let el = Arr.indexOf(item);
                var loc = Arr.splice(el, 1);
                // n.listAllFreeLocations.push(loc[0]);
              }
              i++;
            });
          }
        });
    }
  }

  private onPageSizeChanged($event, el = '')
  {
    this.perPage = parseInt($event.target.value);

  }

  private redirectToList(){
    let n = this;
    /*case 1: modify text*/
    // let warningPopup = new WarningBoxPopup();
    // warningPopup.text = "Are you sure to cancel";
    // this._boxPopupService.showWarningPopup(warningPopup)

    /*case 2: default*/
    this._boxPopupService.showWarningPopup()
      .then(function (dm) {
        if(dm)
          n._router.parent.navigateByUrl('/outbound/bill-of-ladings');
      })
  }

  searchOrderNumber(key) {
    this.ordersListForPopup=this._Func.filterbyfieldName(this.ordersListForPopupTemp,'odr_hdr_num',key);
  }

  private printBOL() {
    var that = this;
    var cusID = jQuery('[name="select-customer"]').val();
    let api = that._api.API_ORDERS_V2  + '/whs/' + this._Func.lstGetItem('whs_id') + '/bol/' + this.bolID + '/print?cus_id=' + cusID;
    try{
      let xhr = new XMLHttpRequest();
      xhr.open("GET", api , true);
      xhr.setRequestHeader("Authorization", 'Bearer ' + that._Func.getToken());
      xhr.responseType = 'blob';

      xhr.onreadystatechange = () => {
        if(xhr.readyState == 2) {
          if(xhr.status == 200) {
            xhr.responseType = "blob";
          } else {
            xhr.responseType = "text";
          }
        }

        if(xhr.readyState === 4) {
          if(xhr.status === 200) {
            var fileName = 'BOL-' + that.bolDetailData['bo_label'];
            var blob = new Blob([xhr.response], {type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'});
            saveAs.saveAs(blob, fileName + '.pdf');
          }else{
            let errMsg = '';
            if(xhr.response) {
              try {
                var res = JSON.parse(xhr.response);
                errMsg = res.errors && res.errors.message ? res.errors.message : xhr.statusText;
              }
              catch(err){errMsg = xhr.statusText}
            }
            else {
              errMsg = xhr.statusText;
            }
            if(!errMsg) {
              errMsg = that._Func.msg('VR100');
            }
            this.messagesText = this._Func.Messages('danger', errMsg);
          }
        }
      }
      xhr.send();
    }catch (err){
      this.messagesText = this._Func.Messages('danger', that._Func.msg('VR100'));
    }
  }

  private showMessage(msgStatus, msg) {
    this.messagesText = {
      'status': msgStatus,
      'txt': msg
    }
    jQuery(window).scrollTop(0);
  }

  CommodityForm: ControlGroup;
  private showErrorCommodity = false;

  buildAddCommodityForm() {
    this.CommodityForm =this.fb.group({
      cmd_name: ['',Validators.compose([Validators.required, this._Valid.validateSpace, this._Valid.invalidNameStr])],
      cmd_cls: ['',Validators.compose([Validators.required, this._Valid.validateSpace, this._Valid.invalidCommodityClass])],
      commodity_nmfc: ['',Validators.compose([Validators.required, this._Valid.validateSpace, this._Valid.invalidCommodityNMFC])],
      cmd_des: ['']
    });
  }

  SaveCommodity(){

      this.showErrorCommodity=true;

      // empty messages
      this.addCommodityMessage = false;

      if(this.CommodityForm.valid){
          let data=this.CommodityForm.value;
          for(let x in data) {
            data[x] = data[x].trim()
          }
          let data_json = JSON.stringify(data);
          this.CreateCommodity(data_json);
      }
      else{
      }
  }

  CreateCommodity(data){
        this.showLoadingOverlay=true;
        this._bolService.createCommodity(data).subscribe(
            data => {
                jQuery('#add-commodity').modal('hide');
                // reset form
                this.showErrorCommodity = false;
                (<Control>this.CommodityForm.controls['cmd_name']).updateValue(null);
                (<Control>this.CommodityForm.controls['cmd_cls']).updateValue(null);
                (<Control>this.CommodityForm.controls['commodity_nmfc']).updateValue(null);
                (<Control>this.CommodityForm.controls['cmd_des']).updateValue(null);
                this.addCommodityMessage = false;
                this.showLoadingOverlay = false;
                // this.showMessage('success', this._Func.msg('VR107'));
                var currValue = this.bOLForm.value.cmd_des;
                this._bolService.getCommodityDescList().subscribe(
                  data => {
                    this.commoDesc = data.data;
                    (<Control>this.bOLForm.controls['cmd_des']).updateValue(currValue);
                  }
                );
            },
            err => {
                this.addCommodityMessage = this._Func.Messages('danger', this._Func.parseErrorMessageFromServer(err));
                this.showLoadingOverlay= false;
            },
            () => {}
        );
    }

  private isNumber(evt, int:boolean = false) {
    this._Valid.isNumber(evt, int);
  }

  showErrorWapLPN = false;
  printBOLLPNForm: ControlGroup;
  private checkPrintBOLLPN() {
    this.messages = null;
    this.showErrorWapLPN = false;
    (<Control>this.printBOLLPNForm.controls['num_of_pallet']).updateValue('');
    jQuery('#print-bol-lpn').modal('show');
  }

  buildFormPrintBOLLPN() {
    this.printBOLLPNForm = this.fb.group({
      num_of_pallet: ['', Validators.compose([Validators.required, this._Valid.isZero,this._Valid.inPutOnlyNumber])]
    });
  }

  printBOLLPN() {
    var that = this;
    this.showErrorWapLPN = true;
    if(this.printBOLLPNForm.valid) {
      let api = that._api.API_ORDERS_CORE  + '/orders/print-bol-lpn/' + this.params.get('id') + '?new_lpn=' + (<Control>this.printBOLLPNForm.controls['num_of_pallet']).value;
      try{
        that.showLoadingOverlay = true;
        let xhr = new XMLHttpRequest();
        xhr.open("GET", api , true);
        xhr.setRequestHeader("Authorization", 'Bearer ' + that._Func.getToken());
        xhr.responseType = 'blob';

        xhr.onreadystatechange = function () {
          if(xhr.readyState == 2) {
            if(xhr.status == 200) {
              xhr.responseType = "blob";
            } else {
              xhr.responseType = "text";
            }
          }

          if(xhr.readyState === 4) {
            if(xhr.status === 200) {
              var fileName = `License_Plate_${that.bolDetailData['printBOLNum']}`;
              var blob = new Blob([this.response], {type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'});
              saveAs.saveAs(blob, fileName + '.pdf');
            }else{
              let errMsg = '';
              if(xhr.response) {
                try {
                  var res = JSON.parse(xhr.response);
                  errMsg = res.errors && res.errors.message ? res.errors.message : xhr.statusText;
                }
                catch(err){errMsg = xhr.statusText}
              }
              else {
                errMsg = xhr.statusText;
              }
              if(!errMsg) {
                errMsg = that._Func.msg('VR100');
              }
              that.showMessage('danger', errMsg);
            }
            jQuery('#print-bol-lpn').modal('hide');
            that.showLoadingOverlay = false;
          }
        };
        xhr.send();
      }catch (err){
        that.showMessage('danger', that._Func.msg('VR100'));
        that.showLoadingOverlay = false;
      }
    }
  }

  private setClickedRow(item, ListName:string, state, paginId) {
    item.selected = !item.selected;
    let Array = eval(`this.` + ListName);
    this.selectedAll[ListName] = false;
    if (item.selected) {
      this.checkCheckAll(ListName, state, paginId);
    }
  }

  private openFormThirdParty: boolean=false;
  private _openFormThirdParty() {
    this.openFormThirdParty = true;
    setTimeout(()=>{
      this.openFormThirdParty = false;
    },300);
  }

  private shipping_to_list: Array<any>=[];
  private autoCompleteShippingTo(key) {
    this.Loading['shipping_to']=false;
    this.messages = false;
    let param = "/cus/"+this.bOLForm.value['cus_id']+"/third-party?name="+key+"&limit=20";
    let enCodeSearchQuery = this._Func.FixEncodeURI(param);
    this.ship_to_name_current={};

    if (this.bOLForm['subscribe']) {
      this.bOLForm['subscribe'].unsubscribe();
    }

    if (key) {
      // do Search
      this.Loading['shipping_to']=true;
      this.bOLForm['subscribe'] = this._bolService.searchShippingTo(enCodeSearchQuery).debounceTime(400).distinctUntilChanged().subscribe(
        data => {
          // disable loading icon
          this.Loading['shipping_to']=false;
          this.shipping_to_list = data.data;
          let item = this.findKeyInList(this.shipping_to_list,'name', this._Func.trim(key));
          if(item) {
            this.selectedShippingTo(item);
          } else {
            // this.fbdFunc.setErrorControl(this.bOLForm,'ship_to_name',{'not_existed':true});
          }
        },
        err => {
          this.Loading['shipping_to']=false;
          this.parseError(err);
        },
        () => {}
      );
    }

  }
  
  findKeyInList(Array: Array<any>=[], filename: any, key: any) {
    for(let item of Array) {
      if(this._Func.trim(item[filename]) == key) {
          return item;
      }
    }
    return null;
  }

  // Show error when server die or else
  private parseError(err) {
    this.messages = {'status': 'danger', 'txt': this._Func.parseErrorMessageFromServer(err)};
  }

  private selectedShippingTo(item={}) {
    this.changeShipName(item);
  }

  private changeShipName(item={}) {
    this.setValueShippingInfo(item);
    this.GetState(item['country']);
  }

  GetState(idCountry) {
    this.Loading['state'] = true;
    let param = "?sort[sys_state_name]=asc&limit=500";
    this._bolService.State(idCountry, param).subscribe(
        data=> {
          this.Loading['state'] = false;
          this.StateProvinces = data;
        },
        err=> {
          this.Loading['state'] = false;
        },
        () => {}
    );
  }

  GetCountry() {
    let param = '?sort[ordinal]=asc&sort[sys_country_name]=asc&limit=500';
    this._bolService.getCountry(param).subscribe(
      data=> {
        this.Country = data;
      },
      err=> {},
      () => {}
    );
  }

  ChangeCountry(item) {
    if (item) {
      this.GetState(item);
      this.StateProvinces = [];
      (<Control>this.bOLForm.controls['ship_to_state']).updateValue('');
    } else {
      this.StateProvinces = [];
      (<Control>this.bOLForm.controls['ship_to_state']).updateValue('');
    }
  }

  setValueShippingInfo(item={}) {
    // reset all group shipping info
    this.fbdFunc.setValueControl(this.bOLForm,'ship_to_name',item['name']);
    this.fbdFunc.setValueControl(this.bOLForm,'ship_to_state',item['state']);
    this.fbdFunc.setValueControl(this.bOLForm,'ship_to_zip',item['zip']);
    this.fbdFunc.setValueControl(this.bOLForm,'ship_to_city',item['city']);
    this.fbdFunc.setValueControl(this.bOLForm,'ship_to_country',item['country']);

    item['add_1'] = item['add_1'] ? item['add_1'] : '';
    item['add_2'] = item['add_2'] ? item['add_2'] : '';

    this.fbdFunc.setValueControl(this.bOLForm,'ship_to_addr_1',item['add_1']);
    this.fbdFunc.setValueControl(this.bOLForm,'ship_to_addr_2',item['add_2']);
    this.ship_to_name_current = item;
  }

  selectAddress($event) {
    const key = $event.target.value;
    for(let i =0;i < this.addressList.length; i ++){
      let arrValue = this.addressList[i].key.split(',');
      if(this.addressList[i].key === key) {
        (<Control>this.bOLForm.controls['ship_to_name']).updateValue(this.addressList[i].company);
      }
    }
  }

}
