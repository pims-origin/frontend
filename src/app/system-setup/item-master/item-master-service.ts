import {Component, Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {API_Config, Functions} from '../../common/core/load';

@Injectable()
export class ItemMasterServices {

  public AuthHeader = this._Func.AuthHeader();
  public AuthHeaderPost = this._Func.AuthHeaderPost();


  constructor(private _Func: Functions, private _API: API_Config, private http: Http)
  {

  }

  /*
   * Add new ItemMaster
   * */
  Add_New_ItemMaster(data)
  {
    return this.http.post(this._API.API_ItemMaster, data, { headers: this._Func.AuthHeaderPostJson() })
      .map((res: Response) => res.json().data);
  }

  /*
   * Update ItemMaster
   * */
  Update_ItemMaster(id,data)
  {
    return this.http.put(this._API.API_ItemMaster+'/'+id, data, { headers: this._Func.AuthHeaderPostJson() })
      .map((res: Response) => res.json().data);
  }

  /*
   * Get detail an Item Master
   * */
  Get_UpdatedItemMaster(id)
  {
    return this.http.get(this._API.API_ItemMaster+'/'+id, { headers: this.AuthHeader })
      .map((res: Response) => res.json().data);
  }

  Get_ItemMasterDetail(id)
  {
    return this.http.get(`${this._API.API_ItemMaster}/${id}/detail`, { headers: this.AuthHeader })
        .map((res: Response) => res.json().data);
  }

  /*
   * Get item master List
   *
   * */
  GetItemMasterList(param)
  {
    return this.http.get(this._API.API_ItemMaster+param,{ headers: this.AuthHeader })
      .map((res: Response) => res.json());
  }

  /*
   * Get a detail warehouse
   * */
  GetDetailWareHouse(id)
  {
    return this.http.get(this._API.API_ItemMaster+'/'+id,{ headers: this.AuthHeader })
      .map((res: Response) => res.json());
  }
  /*
   * Delete function
   * */
  DeleteItemMaster(id)
  {
    return this.http.delete(this._API.API_ItemMaster + "/" + id, { headers: this.AuthHeader });
  }

  /*
   *
   * Search function
   * */
  Search(param)
  {
    return this.http.get(this._API.API_ItemMaster+param,{ headers: this.AuthHeader })
      .map((res: Response) => res.json());
  }

  /*
   *
   * Get List custommers
   * */
  ListCustomers()
  {
    return this.http.get(this._API.API_Customer + '?limit=10000&sort[cus_name]=asc', {
        headers: this.AuthHeader
      }).map(res => res.json());
  }

  /*
   * Get UOM
   * */

  getUOM()
  {
    return this.http.get(this._API.API_UOM + '?limit=1000&sort[sys_uom_name]=asc&sys_uom_type=item', {
        headers: this.AuthHeader
      }).map(res => res.json());
  }

  searchUOM_ID(UOM_Name) {
    return this.http.get(this._API.API_UOM + '?limit=1000&sys_uom_name='+UOM_Name, {
        headers: this.AuthHeader
      }).map(res => res.json());  
  }

  getListCustomersByUser($params) {
    return  this.http.get(this._API.API_Customer_User + $params + '&sort[cus_name]=asc' , {headers: this.AuthHeader})
            .map(res => res.json());
  }

  // Autocomplete for search
  public searchItems(params) {
    return this.http.get(this._API.API_ItemMaster +  params, {headers: this.AuthHeader})
      .map((res: Response) => res.json());
  }

  public getItemPacking(itemId) {
    return this.http.get(this._API.API_ITEM_PACKING + '/' + itemId, { headers: this.AuthHeader })
      .map((res: Response) => res.json());
  }

  public saveItemPacking(data) {
    return this.http.post(this._API.API_ITEM_PACKING, data, { headers: this._Func.AuthHeaderPostJson() })
      .map((res: Response) => res.json());
  }

  getPackingLevelDropdown() {
    return  this.http.get(this._API.API_ITEM_SERVICE + '/item-uom', {headers: this.AuthHeader})
            .map(res => res.json());
  }
  
  getPackageItem(itemId) {
    return  this.http.get(this._API.API_ITEM_SERVICE + '/package-item/' + itemId, {headers: this.AuthHeader})
        .map(res => res.json());
  }
  
  public checkExistSKU(cus_id, key) {
    return this.http.get(this._API.API_URL_ROOT + `/core/item-master/v1/items/cus/${cus_id}?sku=${key}`, { headers: this.AuthHeader })
    .map((res: Response) => res.json());
  }
  
  public skuGenerate(cus_id) {
    return this.http.get(this._API.API_URL_ROOT + `/core/item-master/v1/sku-generate/${cus_id}`, { headers: this.AuthHeader })
    .map((res: Response) => res.json());
  }

  /**
   * Approve item master
   *
   * @param itemId
   */
  approveItemMaster(itemId: string) {
    return this.http.post(`${this._API.API_URL_ROOT}/core/item-master/v1/items/${itemId}/approve`, null, { headers: this._Func.AuthHeaderPostJson() })
      .map(res => res.json());
  }

  /**
   * Get name by code
   *
   * @param code
   */
  getNameByCode(code: string, arrStatus: Array<any>) {
    let item = arrStatus.find(item => item.code === code);
    return item ? item.name : '';
  }
}
