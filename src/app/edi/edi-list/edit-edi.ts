import {Component,Input,Directive, Output, EventEmitter, ElementRef} from '@angular/core';
import {Functions,API_Config} from '../../common/core/load';
import {EDIServices} from "./edi-service";
import {WMSMessages} from '../../common/directives/directives';
declare var jQuery:any;
@Component ({
    selector: 'edit-edit-list-directive',
    directives:[WMSMessages],
    providers: [EDIServices],
    templateUrl: 'edit-edit-template.html',
})
export class EditEDIDirective {

    private _EDI_ID;
    private EDI_DETAIL;
    private messages;
    private whs_id;
    private _Action;


    @Input('EDI_ID') set EDI_ID(value) {
        this._EDI_ID=value;
        if(value){
            this.opentModal();
            this.getEDI_Deatail();
        }
    }


    @Input('Action') set Action(value) {
        this._Action=value;
    }

    @Output() updateDoneTrigger  = new EventEmitter();

    constructor(
        private edis:EDIServices,
        private _Func:Functions
    )
    {
        this.whs_id=this._Func.lstGetItem('whs_id');
    }

    opentModal(value='show'){
        this.messages='';
        this.EDI_DETAIL=[];
        jQuery('#myModal').modal(value);
    }

    /*=======================
    * getEDI_Deatail
    * ======================*/
    private getEDI_Deatail(){

        this.edis.EDIT_detail(this.whs_id,this._EDI_ID).subscribe(
                data => {
                    this.EDI_DETAIL = data.data;
                },
                err => {this.parseError(err)},
                () => {}
            );
    }


    /*=======================
     * checkEDITValue
     *===================== */
    checkEDITValue($event,item){
        if($event.target.checked){
            item['edi_sts']="Y";
        }else{
            item['edi_sts']="N";
        }
    }

    private addCusAndWHS(data:Array<any>=[]){

        data.forEach((item)=>{
            item['cus_id']=this._EDI_ID;
            item['whs_id']=this.whs_id;
        })

    }


    /*====================
    * Submit
    * ===================*/
    private Submit(){

        let n=this;
        let data={};
        this.addCusAndWHS(this.EDI_DETAIL);
        data['data']=this.EDI_DETAIL;
        this.edis.updateEDIT(JSON.stringify(data)).subscribe(
            data => {
                this.messages = this._Func.Messages('success', this._Func.msg('VR109'));
                setTimeout(()=>{
                    this.opentModal('hide');
                    this.updateDoneTrigger.emit(true);
                },600)
            },
            err => {
                this.parseError(err);
            },
            () => {}
        );
        
    }

    // Show error when server die or else
    private parseError (err) {
        this.messages = {'status' : 'danger', 'txt' : this._Func.parseErrorMessageFromServer(err)};
    }

}
