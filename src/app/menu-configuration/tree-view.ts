import {Component, Input} from '@angular/core';
import {MenuConfigurationComponent} from "./menu-configuration.component";

@Component({
  selector: 'tree-view',
  templateUrl: 'tree-view.html',
  directives: [TreeView]
})
export class TreeView {

  @Input() ListMenu: Array<MenuConfigurationComponent>;
  children: any;

  /*
   Delete item
   */
  DeleteItem(item) {

    /*
     Handle in ListMenu array
     */
    var index = this.ListMenu.indexOf(item);
    this.ListMenu.splice(index, 1);
    //console.log('deleted item');

  }

  /*====================
   Update Menu Item
   =====================*/

  UpdateItem(item) {

    /*
     Handle in ListMenu array
     */
    //console.log('deleted item');

  }

}
