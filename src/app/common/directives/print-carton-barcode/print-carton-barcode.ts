import {Component, Input, Output, EventEmitter} from '@angular/core';
import { Router ,RouteParams } from '@angular/router-deprecated';
import 'ag-grid-enterprise/main';
import 'rxjs/add/operator/map';
import {API_Config} from "../../../common/core/API_Config";
import {Functions} from "../../../common/core/functions";;
import { FORM_DIRECTIVES,ControlContainer,ControlGroup,FormBuilder, ControlArray ,Control,Validators} from '@angular/common';
import {PrintCartonBarcodeFunctions} from "./function";
import { ValidationService } from '../../../common/core/validator';
declare var jQuery: any;
declare var saveAs: any;

@Component({
	selector: 'print-carton-barcode',
	providers: [PrintCartonBarcodeFunctions],
	templateUrl: 'print-carton-barcode.html',
})

export class PrintCartonBarcode {

	private _id;
	private _ctn_id;
	private _asn_details;
	items:ControlGroup[]=[];
	itemsTable: ControlArray= new ControlArray(this.items);
	ASNForm: ControlGroup;
	private showForm:any = false;
	disablePrint = false;

	@Input('id') set id(value: string) {
		this._id=value;
	}

	@Input('ctn_id') set ctn_id(value: string) {
		this._ctn_id=value;
	}

	@Input('asn_details') set asn_details(value: string) {
		this._asn_details = value;
	}

	@Output() showLoadingOverlay=  new EventEmitter();
	@Input('openBarCode') set openBarCode(value: string) {

		if (value) {

			this.showForm=false;
			setTimeout(()=>{
				this.emptyItemTable();
				this.showForm=true;
				this.formBuilder(this._asn_details);
			});

		}
	}

	constructor(
		private funs:Functions, 
		private printCartonBarcodeFunctions:PrintCartonBarcodeFunctions,
		private _api_Config: API_Config,
		private fb: FormBuilder,
		private _Valid: ValidationService) {
	}

	private formBuilder(asn_details){

		if(asn_details){
			asn_details.forEach((item)=>{
				this.addNewItemBarcode(this.itemsTable, item);
			});

			this.disablePrint = asn_details.length < 1;
		}

		this.ASNForm = this.fb.group({
			details:this.itemsTable
		});
	}

	private emptyItemTable(){
		while (this.itemsTable.controls.length > 0){
			this.itemsTable.removeAt(0);
		}

	}

	private addNewItemBarcode(controlArray:ControlArray,data={}) {

		controlArray.push(
			new ControlGroup({
				pack_barcode: new Control(1 ,Validators.compose([this._Valid.validateInt, this.printCartonBarcodeFunctions.ckLess100, this._Valid.isZero])),
				dtl_gr_dtl_act_ctn_ttl: new Control(data['dtl_gr_dtl_act_ctn_ttl']),
				dtl_ctn_ttl: new Control(data['dtl_ctn_ttl']),
				asn_dtl_id: new Control(data['asn_dtl_id'])
			})
		);

	}

	submitForm = false;
	private printBarCode(form) {

		if (form.valid) {

			this.showLoadingOverlay.emit(true);
			var params = this.printCartonBarcodeFunctions.buildParams(this.itemsTable);

			var that = this;
			let api = that._api_Config.API_GOODS_RECEIPT + '/asns/' + this._id + '/' + this._ctn_id + '/print?' + params;

			try {

				let xhr = new XMLHttpRequest();
				xhr.open("GET", api, true);
				xhr.setRequestHeader("Authorization", 'Bearer ' + that.funs.getToken());
				xhr.responseType = 'blob';

				xhr.onreadystatechange = function () {
					if (xhr.readyState == 2) {
						if (xhr.status == 200) {
							xhr.responseType = "blob";
						} else {
							xhr.responseType = "text";
						}
					}

					if (xhr.readyState === 4) {
						if (xhr.status === 200) {
							var fileName = 'carton-barcode';
							var blob = new Blob([this.response], {type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'});
							saveAs.saveAs(blob, fileName + '.pdf');
							jQuery('#printBarcode').modal('hide');

						} else {
							let errMsg = '';
							if (xhr.response) {
								try {
									var res = JSON.parse(xhr.response);
									errMsg = res.errors && res.errors.message ? res.errors.message : xhr.statusText;
								}
								catch (err) {
									errMsg = xhr.statusText
								}
							}
							else {
								errMsg = xhr.statusText;
							}
							if (!errMsg) {
								errMsg = that.funs.msg('VR100');
							}
							that.showMessage2('danger', errMsg);
						}

						that.showLoadingOverlay.emit(false);
					}
				};
				xhr.send();
			} catch (err) {

				that.showMessage2('danger', that.funs.msg('VR100'));
				that.showLoadingOverlay.emit(false);
			}
		}
	}

	messagesBC;
	private showMessage2(msgStatus, msg) {

		this.messagesBC = {
			'status': msgStatus,
			'txt': msg
		};
		jQuery(window).scrollTop(0);
	}
}
