import {Component} from '@angular/core';
import { FORM_DIRECTIVES,ControlGroup,FormBuilder, ControlArray ,Control,Validators} from '@angular/common';
import {WMSBreadcrumb,DocumentUpload,WMSMessages} from '../common/directives/directives';
import {Router, RouteData, RouteParams } from '@angular/router-deprecated';
import {BoxPopupService} from "../common/popup/box-popup.service";
import {GoBackComponent} from "../common/component/goBack.component";
import { ValidationService } from '../common/core/validator';
import {UserService, Functions,FormBuilderFunctions} from '../common/core/load';
import {ASNTOOL_DIRECTIVES} from './directives/asn-tool-directive-export';
import {ASN_Services} from "./asn-services";
import {AsnToolFunctions} from "./asn-tool-functions";
import {WarningBoxPopup} from "../common/popup/warning-box-popup";
import {CTRLVTABLE_PROVIDERS} from "../common/providers/ctrlv-table/ctrlv-table.providers";
import {CTRLVTable} from "../common/providers/ctrlv-table/ctrlv-table";
import {AutoCompleteProvider} from "./directives/auto_complete"
declare var jQuery: any;
@Component ({
    selector: 'asn-cru-ltool',
    directives:[WMSBreadcrumb,ASNTOOL_DIRECTIVES,GoBackComponent,DocumentUpload,FORM_DIRECTIVES,WMSMessages],
    providers: [BoxPopupService,AutoCompleteProvider,CTRLVTABLE_PROVIDERS,CTRLVTable,ValidationService,FormBuilderFunctions,ASN_Services,UserService,AsnToolFunctions],
    templateUrl: 'asn-tool.html',
    styleUrls: ['asn-tool.scss']
})
export class ANSTool{

    ASNForm: ControlGroup;
    private action = 'new';
    private messages;
    private showLoadingOverlay=false;
    private TitlePage="ASN";
    private IsView=false;
    private Action;
    private whs_id;
    private submitForm=false;
    private cus_id;
    private AnsData:Array<any>=[];
    items:ControlGroup[]=[];
    itemsTable: ControlArray= new ControlArray(this.items);
    private Customers:Array<any>=[];
    private dataListMeasurements:Array<any>=[];
    private dataListUoms:Array<any>=[];
    private selectedAll={};
    private asnID;
    private uom_default_id='';
    private ctn_id;
    private dataListContainersByASN:Array<any>=[];


    constructor(private _router: Router,
                private _boxPopupService:BoxPopupService,
                _RTAction: RouteData,
                private _user:UserService,
                private _ans_service:ASN_Services,
                private fb: FormBuilder,
                private asnFun:AsnToolFunctions,
                private ctrlvData:CTRLVTable,
                private fbdFun:FormBuilderFunctions,
                private _Valid: ValidationService,
                private _Func: Functions,
                private params: RouteParams) {

        this.Action=_RTAction.get('action');
        this.whs_id=this._Func.lstGetItem('whs_id');
        this.checkPermission();

    }


    // Check permission for user using this function page
    private createASN;
    private editASN;
    private viewASN;
    private allowAccess:boolean=false;
    checkPermission(){

        this.showLoadingOverlay = true;
        this._user.GetPermissionUser().subscribe(
            data => {

                this.showLoadingOverlay = false;
                this.createASN = this._user.RequestPermission(data, 'createASN');
                this.editASN = this._user.RequestPermission(data,'editASN');
                this.viewASN=this._user.RequestPermission(data,'viewASN');
                this.getCustomersByWH();
                this.getMeasurements();

              // load detail / init data form
                if(this.Action=='new')
                {
                    if(this.createASN){
                        this.allowAccess=true;
                        this.TitlePage = 'ASN Tool';
                        this.formBuilder();
                    } else{
                        this.redirectDeny();
                    }

                }


                /*
                 * Action router Edit || View
                 * */
                if(this.Action=='edit'||this.Action=='view')
                {
                    if(this.editASN||this.viewASN) {

                        this.allowAccess=true;
                        this.asnID = this.params.get('id');
                        this.ctn_id = this.params.get('containerID');
                        this.getAsnByIdCtnId(this.ctn_id);
                        this.getContainersByASN();
                        if(this.Action=='view'){
                            this.TitlePage = 'View ASN';
                            this.IsView=true;
                        }

                    }
                    else{
                        this.redirectDeny();
                    }
                }

            },
            err => {
                this.parseError(err);
                this.showLoadingOverlay = false;
            }
        );
    }

    redirectDeny(){
        this._router.parent.navigateByUrl('/deny');
    }

    addNewItem(data={}) {

        this.itemsTable.push(
            new ControlGroup({
                dtl_itm_id: new Control(data['dtl_item_id']),
                asn_dtl_id: new Control(data['asn_dtl_id']),
                asn_dtl_cus_upc: new Control(data['asn_dtl_cus_upc'],Validators.compose([this._Valid.checkUPCdigitCode,this._Valid.checkUPClength])),
                dtl_sku: new Control(data['dtl_sku'],Validators.compose([Validators.required,this._Valid.validateSpace])),
                dtl_size: new Control(data['dtl_size'],Validators.compose([Validators.required,this._Valid.validateSpace])),
                dtl_color: new Control(data['dtl_color'],Validators.compose([Validators.required,this._Valid.validateSpace])),
                dtl_lot: new Control(data['asn_dtl_lot']),
                dtl_uom_id: new Control(data['dtl_uom_id'] ? data['dtl_uom_id'] : this.uom_default_id,Validators.compose([Validators.required])),
                dtl_pack: new Control(data['asn_dtl_pack'],Validators.compose([Validators.required,this._Valid.isZero,this._Valid.inPutOnlyNumber])),
                dtl_ctn_ttl: new Control(data['dtl_ctn_ttl'],Validators.compose([Validators.required,this._Valid.isZero,this._Valid.inPutOnlyNumber])),
                dtl_length: new Control(data['dtl_length'],Validators.compose([Validators.required,this._Valid.isZero,this._Valid.isDecimalNumber])),
                dtl_width: new Control(data['dtl_width'],Validators.compose([Validators.required,this._Valid.isZero,this._Valid.isDecimalNumber])),
                dtl_height: new Control(data['dtl_height'],Validators.compose([Validators.required,this._Valid.isZero,this._Valid.isDecimalNumber])),
                dtl_weight: new Control(data['dtl_weight'],Validators.compose([Validators.required,this._Valid.isZero,this._Valid.isDecimalNumber])),
                dtl_crs_doc: new Control(data['dtl_crs_doc'],this._Valid.validateInt),
                dtl_po: new Control(data['dtl_po'],Validators.compose([Validators.required])),
                dtl_po_date: new Control(data['dtl_po_date']),
                expired_dt:new Control(data['expired_dt'])
            })
        );
    }

    /*
    * Formbuilder standard AG 2.0
    * */
    formBuilder(data={}){

        this.ASNForm =this.fb.group({
                cus_id:[data['cus_id'] ? data['cus_id'] : '', Validators.compose([Validators.required])],
                asn_hdr_num:[data['asn_hdr_num']],
                asn_ref:[data['asn_hdr_ref'] ? data['asn_hdr_ref'] : '', Validators.compose([Validators.required])],
                ctnr_num : [data['asn_details'] ? data['asn_details'][0]['ctnr_num'] : '', Validators.compose([Validators.required])],
                ctnr_id : [data['asn_details'] ? data['asn_details'][0]['ctnr_id'] : 0],
                asn_hdr_itm_ttl:[data['asn_hdr_itm_ttl']],
                exp_date:[data['asn_hdr_ept_dt'] ? data['asn_hdr_ept_dt'] : '', Validators.compose([Validators.required])],
                measurement_code: [data['sys_measurement_code'] ? data['sys_measurement_code'] : 'IN'],
                details:this.itemsTable,
                whs_id:[this.whs_id]
        });

        if(data['asn_details']){
            data['asn_details'].forEach((item)=>{
                this.addNewItem(item);
            });
        }else{
            this.addNewItem();
        }

        this.getUoms();

        setTimeout(() => {
            this.fbdFun.handelDropdownDatePicker(this.ASNForm);
        },200);

        // Watch change form value
        this.ASNForm.valueChanges.subscribe(data =>{
                this.fbdFun.checkDuplicatedKey(this.itemsTable,this._ans_service.duplicatedField());
        });

    }

    /*=====================
    * Get id uom default id
    * =====================*/
    uom_default(){
        for(let i in this.dataListUoms){
            if(this.dataListUoms[i]['sys_uom_code']=='EA'){
                this.uom_default_id=this.dataListUoms[i]['sys_uom_id'];
                this.fbdFun.setValueControlItem(this.itemsTable,0,'dtl_uom_id',this.uom_default_id);
                return;
            }
        }
    }
    

    
    /*==============================
    * changeCtnr
    * ============================*/
    
    changeCtnr($event){

        let curentValue=this.ASNForm.value['ctnr_id'];
        if(curentValue&&!this.IsView){
            let that = this;
            let warningPopup = new WarningBoxPopup();
            warningPopup.text = this._Func.msg('M043');
            this._boxPopupService.showWarningPopup(warningPopup)
                .then(function (dm) {
                    if(dm) {
                        that.fbdFun.emptyControlArray(this.itemsTable);
                        that.getAsnByIdCtnId($event.target.value);
                    }
                    else{
                        that.fbdFun.setValueControl(this.ASNForm,'ctnr_id',curentValue);
                    }
                });
        }else{
            if($event.target.value){
                this.fbdFun.emptyControlArray(this.itemsTable);
                this.getAsnByIdCtnId($event.target.value);
            }
        }
        
    }
    
    // Get measurements
    private getMeasurements () {

        this._ans_service.getMeasurements()
            .subscribe(
                data => {
                    this.dataListMeasurements = data.data;
                    for( let i = 0 ; i < this.dataListMeasurements.length ; i ++ ){
                        if(this.dataListMeasurements[i].sys_mea_code == 'CM'){
                            this.dataListMeasurements[i].sys_mea_name = 'Metric System';
                        }
                        if(this.dataListMeasurements[i].sys_mea_code == 'IN'){
                            this.dataListMeasurements[i].sys_mea_name = 'Imperial System';
                        }
                    }
                },
                err => {

                    this.parseError(err);
                },
                () => {}
            );

    }

    // Get containers
    private getContainersByASN () {

        this._ans_service.getContainersByASN(this.asnID)
            .subscribe(
                data => {
                    this.dataListContainersByASN = data.data;
                },
                err => {
                    this.parseError(err);
                },
                () => {}
            );

    }

    // Get uoms
    private getUoms () {

        var params = "?limit=10000";
        this._ans_service.getUoms(params)
            .subscribe(
                data => {
                    this.dataListUoms = data.data;
                    this.uom_default();
                },
                err => {
                    this.parseError(err);
                },
                () => {}
            );

    }

    /*===================================================
     * Handle data like a excel file
     * ================================================== */
    private pasteData($event,row){
      this.ctrlvData.onKeyDown(this.itemsTable,$event,row,this.addNewItem());
    }

    cancel()
    {
        let n = this;
        /*case 1: modify text*/
        // let warningPopup = new WarningBoxPopup();
        // warningPopup.text = "Are you sure to cancel";
        // this._boxPopupService.showWarningPopup(warningPopup)

        /*case 2: default*/
        this._boxPopupService.showWarningPopup()
            .then(function (dm) {
                if(dm)
                    n._router.navigateByUrl('/outbound/work-order');

            });
    }

    private getAsnByIdCtnId (ctn_id) {

        let data;
        this._ans_service.getAsnByIdCtnId(this.asnID, ctn_id)
            .subscribe(
                data => {
                    this.formBuilder(data.data);
                },
                err => {

                    this.parseError(err);
                },
                () => {
                }
            );

    }

    private autoSearchData=[];
    autoComplete(row,key,fieldname){

        // search only if cus_id have seleced
        if(this.ASNForm.value['cus_id']){

            this.items[row]['showLoading']=true;
            let searchkey=this._Func.trim(key);
            let param="?"+fieldname+"="+searchkey+"&cus_id="+this.ASNForm.value['cus_id']+"&limit=20&sort["+fieldname+"]=asc";
            let enCodeSearchQuery=this._Func.FixEncodeURI(param);

            if(this.items[row]['subscribe'])
            {
                this.items[row]['subscribe'].unsubscribe();
            }
            // do Search
            this.items[row]['subscribe']=this._ans_service.searchItems(enCodeSearchQuery).debounceTime(400).distinctUntilChanged().subscribe(
                data => {
                    // disable loading icon
                    this.items[row]['showLoading']=false;
                    this.autoSearchData = data.data;
                    //this.checkKeyExist(row,searchkey,this.autoSearchData,fieldname);
                },
                err =>{
                    this.items[row]['showLoading']=false;
                    this.parseError(err);
                },
                () =>{
                }
            );

        }


    }
    

    /*==============================
    * Autocomplete container
    *==============================*/
    private containerAutoData=[];
    autoCompleteContainer(key){

        let searchkey=this._Func.trim(key);
        let params = "?ctnr_num=" + key + "&limit=20";
        let item=this.ASNForm.controls['ctnr_num'];
        item['showLoading']=true;

        this._ans_service.getContainerByName(this._Func.FixEncodeURI(params)).debounceTime(400).distinctUntilChanged().subscribe(
            data => {
              item['showLoading']=false;
                this.containerAutoData = data;
                this.asnFun.checkExistContainer(this.ASNForm,key,data);
            },
            err =>{
               item['showLoading']=false;
                this.parseError(err);
            },
            () =>{
            }
        );


    }

   


    /*=============================
    * Save
    * ============================*/
    Save(form,saveMoreContainer=false){
            this.submitForm=true;
            this.asnFun.checkTotalExpCarton(this.itemsTable);
            if(form.valid){
                let data = JSON.stringify(form.value);
                this._ans_service.saveASN(this.asnID, data, form.value['ctnr_id'])
                    .subscribe(
                        data => {

                            if(saveMoreContainer){
                                this.messages = this._Func.Messages('success', 'The container ' + this.ASNForm.value['ctnr_num'] + ' is added to ASN ' + data.data['asn_hdr_num'] + ' successfully!');
                                this.Action='edit';
                                // get data for edit
                                this.asnID=data.data['asn_hdr_id'];
                                // empty row
                                this.fbdFun.emptyControlArray(this.itemsTable);
                                this.addNewItem();
                                this.submitForm=false;
                            }

                            if(this.Action=='new'&&!saveMoreContainer) {
                                this.messages = this._Func.Messages('success', 'The ' + data.data['asn_hdr_num'] + ' was added successfully!');
                            }
                            if(this.Action=='edit'&&!saveMoreContainer){
                                this.messages = this._Func.Messages('success', 'The ' + data.data['asn_hdr_num'] + ' was updated successfully!');
                            }

                        },
                        err => {
                            this.parseError(err);
                        },
                        () => {
                            // redirect to asn list
                            if(!saveMoreContainer){
                                setTimeout(()=> {
                                    this._router.parent.navigateByUrl('/asn');
                                }, 1100);
                            }
                        }
                    );
            }
    }
    

    deleteItem(controlArray:ControlArray){

        if(this.fbdFun.getItemchecked(this.itemsTable)>0) {

            // /*case 1: modify text*/
            // let warningPopup = new WarningBoxPopup();
            // warningPopup.text = 'Are you sure to delete';
            // this._boxPopupService.showWarningPopup(warningPopup)
            //     .then(function (dm) {
            //         if (!dm) {
            //             return;
            //         } else {
            //             // create array temp
            //             let Arr = eval(`n.` + ListName);
            //             let ArrTemp = Arr.slice();
            //             if (n.selectedAll[ListName]) {
            //                     n.fbdFun.emptyControlArray(this.itemsTable);
            //                     // auto add one row
            //                     n.selectedAll[ListName] = false;
            //                     n.addNewItem();
            //                     n.selectedAll[ListName] = false;
            //             }
            //             else {
            //                 let i = 0;
            //                 ArrTemp.forEach((item)=> {
            //                     // find item selected and remove it
            //                     if (item['selected'] == true) {
            //                             n.itemsTable.removeAt(i);
            //                             i--;
            //                     }
            //                     i++;
            //                 });
            //             }
            //         }
            //     });

            //this.fbdFun.deleteItem(controlArray,this.addNewItem());
        }
        else{
            this.messages = this._Func.Messages('danger', this._Func.msg('VR113'));
        }
    }
    
    // get list custommer

    private getCustomersByWH () {

        let params='?whs_id=' + this.whs_id + "&limit=10000";
        this._ans_service.getCustomersByWH(params)
            .subscribe(
                data => {
                    this.Customers = data.data;
                },
                err => {
                    this.parseError(err);
                },
                () => {
                }
            );

    }

    

    checkInputNumber($event,int){
        this._Valid.isNumber($event,int);
    }

    // Show error when server die or else
    private parseError (err) {
        this.messages = {'status' : 'danger', 'txt' : this._Func.parseErrorMessageFromServer(err)};
    }
    private parseErrorStandard (err) {
        this.messages = {'status' : 'danger', 'txt' : this._Func.parseErrorMessageFromServerStandard(err)};
    }


    redirectToList(){
        let n = this;
        /*case 2: default*/
        this._boxPopupService.showWarningPopup()
            .then(function (dm) {
                if(dm)
                    n._router.parent.navigateByUrl('/outbound/work-order');
            })
    }

}