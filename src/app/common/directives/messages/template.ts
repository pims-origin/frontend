/**
 */

export const DEFAULT_TEMPLATE = `
    <div *ngIf="_messages" class="col-mg-12">
    <div class="alert alert-{{_messages.status}}">
      <a href="#" class="close internal" (click)="_messages=false" aria-label="close">&times;</a>
      <span [innerHTML]="_messages.txt"></span>
    </div>
  </div>
    `;

export const DEFAULT_STYLES = ``;
