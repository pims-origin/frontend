/*==================
Load.ts
===================*/
'use strict';

import {Functions} from './functions';
import {TableFunctions} from './table-functions';
import {API_Config} from './API_Config';
import {UserService} from '../users/users.service';
import {FormBuilderFunctions} from './formbuilder.functions';

export * from './functions';
export * from './table-functions';
export * from './API_Config';
export * from '../users/users.service';
export * from './formbuilder.functions';

export const SF_PROVIDERS = [
    Functions, TableFunctions,
    API_Config, UserService,FormBuilderFunctions]; /* load service*/
//export const SF_DIRECTIVES: any[] = [];
