
import {Component, ViewChild} from '@angular/core';
import {RouteParams} from '@angular/router-deprecated';
import {API_Config} from '../../../common/core/API_Config';
import {TableFunctions} from '../../../common/core/table-functions';
import {TableStructure} from '../../../common/directives/directives';
import {DateFunctions} from '../../../common/core/functions/date-functions';
import {CountryService} from '../../../master-data/country/country-service';
import {StateService} from '../../../master-data/state/state-service';
import {BaseService} from '../../../common/core/classes_us/base-service';

import {InvoiceIssue} from './invoice-issue';

import {ClientInfoCompoundComponent} from '../client-info/client-info-compound.component';
import {InvoiceHeaderComponent} from './invoice-header.component';
import {InvoiceBodyComponent} from './invoice-body.component';

import {Confirm} from '../../../common/directives/popup-modal_us/confirm/confirm';
import {Alert} from '../../../common/directives/popup-modal_us/alert/alert';

@Component({
    selector: 'invoice-issue',
    directives: [
        TableStructure,
        ClientInfoCompoundComponent,
        InvoiceHeaderComponent,
        InvoiceBodyComponent,
        Confirm,
        Alert
    ],
    providers: [
        TableFunctions,
        CountryService,
        StateService,
        DateFunctions,
        BaseService
    ],
    templateUrl: 'invoice-issue.component.html',
})

export class DisplayInvoiceComponent extends InvoiceIssue
{
    @ViewChild(Confirm) confirm: Confirm;
    @ViewChild(Alert) alert: Alert;

    @ViewChild(InvoiceHeaderComponent) _invoiceHeaderComponent: InvoiceHeaderComponent;
    @ViewChild(InvoiceBodyComponent) _invoiceBodyComponent: InvoiceBodyComponent;

    constructor (
        protected _routeParams: RouteParams,
        protected _api: API_Config,
        protected _tableFunc: TableFunctions,
        protected _dateFunctions: DateFunctions,
        protected _countryService: CountryService,
        protected _stateService: StateService,
        protected _baseService: BaseService
    )
    {
        super(
            _api,
            _dateFunctions,
            _baseService
        );

        let url = this._api.API_Invioce_Process + '/inv/'
                + this._routeParams.get('inv_num');

        this.action = 'print';

        this._baseService.get(url)
            .subscribe(
                data => {

                    this.dateCaption = 'Invoice Date: ' + data.inv_dt;

                    this.cancellable = data.inv_typ == 'o' && data.inv_sts == 'i';

                    this.showInvoiceIssue(data);
                },
                err => {
                },
                () => {
                    this.showLoadingOverlay = false;
                }
        );
    }

    //**************************************************************************

    public cancelInvoice()
    {
        let inv_num = this.invoiceData['inv_num'];

        let message = 'Invoice No ' + inv_num + ' will be canceled. Proceed?';

        this.confirm.show(message, {
            inv_num: inv_num
        });
    }

    //**************************************************************************

    public onConfirm($event)
    {
        if ($event.response) {

            let url = this._api.API_Invoice_Cancel + '/' + $event.data['inv_num'];

            this.showLoadingOverlay = true;

            this._baseService.post(url)
                .subscribe(
                    data => {
                        this.alert.show('Invoice was canceled');
                        this.cancellable = false;
                    },
                    err => {
                        this.alert.show('Error canceling invoice');
                    },
                    () => {
                        this.showLoadingOverlay = false;
                    }
            );
        }
    }

    //**************************************************************************

}


