import {Component,Input, Output,EventEmitter} from '@angular/core';
import {WMSPagination, PaginationService,PDFPrinter} from '../../../common/directives/directives';
import { API_Config,Functions} from '../../../common/core/load';
import {PalletServices} from "./pallet-service";
import { ValidationService } from '../../../common/core/validator';
import { WarningBoxPopup, BoxPopupService } from '../../../common/popup/index';
@Component ({
  selector: 'pallet-list-directive',
  directives:[WMSPagination,PDFPrinter],
  providers: [PaginationService, PalletServices, ValidationService],
  templateUrl: 'pallet.html'
})

export class PalletListDirective {

  private _orderHeaderId = '';
  private PalletList = [];
  private palletLabelAPI:string;
  private cartonLalbelAPI:string;
  public _orderStatus:string;

  @Input('orderHeaderId') set orderHeaderId(value: string) {
    this._orderHeaderId=value;
    if (value) {
      this.cartonLalbelAPI = this._API.API_ORDERS+'/print-carton-shipping/'+this._orderHeaderId;
      this.palletLabelAPI = this._API.API_ORDERS+'/print-pallet-shipping/'+this._orderHeaderId;
      this.getPalletList(1);
    }
  }

  @Input('orderStatus') set orderStatus(value: string) {
    this._orderStatus = value;
  }

  @Input() isFinalBOL: any;

  @Output() messages=  new EventEmitter();
  @Output() showLoadingOverlay=  new EventEmitter();



  constructor(
    private _Func: Functions,
    private _API: API_Config,
    private _Valid: ValidationService,
    private _masterPalletServices:PalletServices,
    private _boxPopupService: BoxPopupService,
  ) {
  }

  private getPalletList(page: any = ''){

    // this.showLoadingOverlay.emit(true);
    var params = "?page=" + page + "&limit=" + this.perPage;
    this._masterPalletServices.getPalletList(this._Func.lstGetItem('whs_id'), this._orderHeaderId, params).subscribe(
        data => {

          this.PalletList = data['data'];
          this.initPagination(data);
          // this.showLoadingOverlay.emit(false);
        },
        err => {

          // this.showLoadingOverlay.emit(false);
        },
        () => {}
    );

  }

  private _addError(item: any, key: string) {
    if (item.errors) {
      const pos = item.errors.indexOf(key);
      if (pos === -1) {
        item.errors.push(key);
      }
    } else {
      item.errors = [key];
    }
  }

  private _removedError(item: any, key: string) {
    if (item.errors) {
      const pos = item.errors.indexOf(key);
      if (pos !== -1) {
        item.errors.splice(pos, 1);
      }
    }
  }

  private _checkRequired(item: any, field: string): boolean {
    const val = item[field];
    const key = 'errorRequired' + field[0].toUpperCase() + field.substr(1);
    item[key] = !val && val !== 0;
    if (item[key]) {
      this._addError(item, key);
    } else {
      this._removedError(item, key);
    }
    return item[key];
  }

  private _setOptional(item: any, field: string) {
    const key = 'errorRequired' + field[0].toUpperCase() + field.substr(1);
    item[key] = false;
    this._removedError(item, key);
  }

  private _checkDecimalNunber(item: any, field: string, length: number) {
    const val = item[field];
    const key = 'errorDecimalNumber' + field[0].toUpperCase() + field.substr(1);
    const valid = length === 15 ? this._Valid.isDecimalNubmerLength15_2(val) : this._Valid.isDecimalNubmerLength5_2(val);
    item[key] = val && !valid;
    if (item[key]) {
      this._addError(item, key);
    } else {
      this._removedError(item, key);
    }
  }

  private _checkZero(item: any, field: string) {
    const val = item[field];
    const key = 'errorZero' + field[0].toUpperCase() + field.substr(1);
    item[key] = parseFloat(val) === 0;
    if (item[key]) {
      this._addError(item, key);
    } else {
      this._removedError(item, key);
    }
  }

  private validdateField(item: any, field: string, inputting=true) {
    let err;
    switch(field) {
      case 'volume':
        err = this._checkRequired(item, field);
        if (!err) {
          this._checkZero(item, field);
          this._checkDecimalNunber(item, field, 15);
        }
        if (inputting) {
          item.inputBy = 'v';
        }
        break;
      case 'weight':
        err = this._checkRequired(item, field);
        if (!err) {
          this._checkZero(item, field);
          this._checkDecimalNunber(item, field, 10);
        }
        break;
      case 'length':
      case 'width':
      case 'height':
        if (item.inputBy === 'd') {
          this._checkRequired(item, field);
        } else {
          this._setOptional(item, field);
        }
        this._checkZero(item, field);
        this._checkDecimalNunber(item, field, 5);
        if (inputting) {
          item.inputBy = 'd';
        }
        break;
    }
  }

  private _calculatedVolume(item) {
    if (item.length && item.width && item.height) {
      item.volume = parseFloat(item.length) * parseFloat(item.width) * parseFloat(item.height);
      item.volume = item.volume.toFixed(2);
    }
  }

  public updateData(item) {
    // reset to input by volume if dimension empty
    if (!item.length && !item.width && !item.height) {
      item.inputBy = 'v';
    }
    // recheck before call API
    ['volume', 'weight', 'length', 'width', 'height'].forEach(field => this.validdateField(item, field, false));
    if (!item['errors'] || item['errors'].length === 0) {
      if (item.inputBy === 'd') {
        this._calculatedVolume(item);
      }
      const data = {
        volume: item.volume,
        weight: item.weight,
        length: item.length,
        width: item.width,
        height: item.height,
      };
      this.showLoadingOverlay.emit(true);
      this._masterPalletServices.updateData(this._Func.lstGetItem('whs_id'), item.plt_id, JSON.stringify(data)).subscribe(
        data => {
          this.showLoadingOverlay.emit(false);
          this.messages.emit(this._Func.Messages('success', this._Func.msg('PLT001')));
        },
        err => {
          this.showLoadingOverlay.emit(false);
          this.parseError(err);
        },
        () => {}
      );
    }
  }

  checkInputFloatNumber(evt, val) {
    this._Valid.validateFloatKeyPress(val, evt);
  }

// Set params for pagination
  private Pagination;
  private currentPage = 1;
  private perPage = 20;
  private initPagination(data) {

    var meta = data.meta;
    this.Pagination = meta['pagination'];
    this.Pagination['numLinks'] = 3;
    this.Pagination['tmpPageCount'] = this._Func.Pagination(this.Pagination);

  }

  private onPageSizeChanged($event, el = '')
  {

    this.perPage = parseInt($event.target.value);

    if ((this.Pagination['current_page'] - 1) * this.perPage >= this.Pagination['total']) {
      this.currentPage = 1;
    }
    else {
      this.currentPage = this.Pagination['current_page'];
    }
    if (el == 'master-pallet') {

      this.getPalletList(this.currentPage);
    }
  }

  private bindMessagePDFPrinter($event){
    this.messages.emit($event);
  }

  private parseError(err){
    this.messages.emit({'status' : 'danger', 'txt' : this._Func.parseErrorMessageFromServer(err)});
  }

  private setClickedRow(arItems, row, item) {
    item.selected = !item.selected;
}

  deleteLPN() {
    const selectedItems = this.PalletList.filter(item => item.selected);
    if (selectedItems.length > 1) {
      this.messages.emit(this._Func.Messages('danger', this._Func.msg('CC004')));
    } else {
      if (selectedItems.length < 1) {
        this.messages.emit(this._Func.Messages('danger', this._Func.msg('CC005')));
      } else {
        const that = this;
        const warningPopup = new WarningBoxPopup();
        warningPopup.text = this._Func.msg('PBAP002');
        this._boxPopupService.showWarningPopup(warningPopup).then(function (dm) {
            if(dm){
              // call api to delete data
              that.showLoadingOverlay.emit(true);
              that._masterPalletServices.deleteLPN(that._Func.lstGetItem('whs_id'), selectedItems[0].plt_id).subscribe(
                data => {
                  that.showLoadingOverlay.emit(false);
                  that.messages.emit(that._Func.Messages('success', that._Func.msg('PLT002')));
                  that.getPalletList(1);
                },
                err => {
                  that.showLoadingOverlay.emit(false);
                  that.parseError(err);
                },
                () => {}
              );
            }
        });

      }
    }
  }
}
