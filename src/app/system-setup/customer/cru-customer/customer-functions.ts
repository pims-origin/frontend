import {Component } from '@angular/core';
import {ControlGroup, Control ,ControlArray} from '@angular/common';
export class CustomerFunctions {
    constructor(){}

    /*===============================
     * changeAddressType
     * ============================*/
    changeAddressType(controlArray:ControlArray){

       this.removeErrorsControlArray(controlArray,['cus_add_type'],'duplicated');
        if(controlArray.value.length==2){
            if(controlArray.value[0]['cus_add_type']==controlArray.value[1]['cus_add_type']){
                this.checkDuplicatedRowByField(controlArray,['cus_add_type']);
            }
        }


    }

    /*=======================================
    * setErrorControlArray
    * =====================================*/
    setErrorControlArray(controlArray:ControlArray,row,control,err={}){
        (<Control>controlArray['controls'][row]['controls'][control]).setErrors(err);
    }

    /*=======================================
     * set value control
     * =====================================*/
    setValueControlArray(controlArray:ControlArray,row,control,value){
        (<Control>controlArray['controls'][row]['controls'][control]).updateValue(value);
    }


    /*=======================================
     * set value control
     * =====================================*/
    setValueControlGroup(controlGroup:ControlGroup,control,value){
        (<Control>controlGroup['controls'][control]).updateValue(value);
    }

    public removeErrorsControlArray(controlArray:ControlArray,controlNeedRemoveError:Array<any>,keyDelete='')
    {
        for(let row in controlArray.controls){
            for(let key in controlNeedRemoveError){
                let control=controlArray.controls[row]['controls'][controlNeedRemoveError[key]];
                if(control.errors){
                    delete control.errors[keyDelete];
                    if(!Object.keys(control.errors).length) {
                        this.setErrorControlArray(controlArray,row,controlNeedRemoveError[key],null);
                    }
                }
            }

        }
    }

    public generationStringData(rowData={},arrayToCheckDuplicated){

        let stringData='';
        let validKey=0;
        for (let key in arrayToCheckDuplicated){
            if(rowData[arrayToCheckDuplicated[key]]) {
                stringData+=rowData[arrayToCheckDuplicated[key]];
                validKey++;
            }
        }
        if(validKey==arrayToCheckDuplicated.length){
            return stringData;
        }else{
            return false;
        }


    }

    /*===================================
     * checkDuplicatedChargeCode
     * =================================*/
    public checkDuplicatedRowByField(controlArray:ControlArray,fieldcheckDup=[]){

            this.removeErrorsControlArray(controlArray,fieldcheckDup,'duplicated');
            let Arr=controlArray.value;
            for(let i=0;i<Arr.length;i++) {
                let items=this.generationStringData(Arr[i],fieldcheckDup);
                for(let j=i+1;j<Arr.length;j++) {
                    let next_item=this.generationStringData(Arr[j],fieldcheckDup);
                    if(i==j) {continue;}
                    if(items==next_item&&(next_item&&items)) {
                        for(let key in fieldcheckDup){
                            this.setErrorControlArray(controlArray,i,fieldcheckDup[key],{'duplicated':true});
                            this.setErrorControlArray(controlArray,j,fieldcheckDup[key],{'duplicated':true});
                            (<Control>controlArray['controls'][i]['controls'][fieldcheckDup[key]]).markAsTouched();
                            (<Control>controlArray['controls'][j]['controls'][fieldcheckDup[key]]).markAsTouched();
                        }
                    }
                }
            }

    }

}