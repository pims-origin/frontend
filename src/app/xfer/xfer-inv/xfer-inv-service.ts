import {Component, Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {API_Config, Functions} from '../../common/core/load';

@Injectable()
export class XferService {

  public AuthHeader = this._Func.AuthHeader();
  public AuthHeaderPost = this._Func.AuthHeaderPost();

  constructor(private _Func: Functions, private _API: API_Config, private http: Http) {

  }

  searchLoc(params){
    return this.http.get(this._API.API_WH_XFER+'/list-act-mz-location'+params,{ headers: this.AuthHeader })
      .map((res: Response) => res.json());
  }

  getCustomersByWH($params) {
    return  this.http.get(this._API.API_CUSTOMER_MASTER + '/customer-warehouses' + $params + '&sort[cus_name]=asc' , {headers: this.AuthHeader})
      .map(res => res.json());
  }


  updateXferRecord(id,data){
    return this.http.put(this._API.API_WH_XFER+'/update-xfer', data, { headers: this._Func.AuthHeaderPostJson() })
      .map((res: Response) => res.json().data);
  }


  geteXferDetail(id,param){
    return  this.http.get(this._API.API_WH_XFER+'/detail-ticket?xfer_ticket_id='+id+param, {headers: this.AuthHeader})
      .map(res => res.json().data);

  }

  getEventracking(XFERNumber){
    return this.http.get(this._API.API_GOODS_RECEIPT_MASTER+'/eventracking?owner='+XFERNumber+'&type=wo', {
      headers: this.AuthHeader
    }).map(res => res.json());
  }

   searchLocation($param) {
    return this.http.get(this._API.API_Warehouse + "/" +$param,{ headers: this.AuthHeader })
      .map((res: Response) => res.json());
  }

  getAvailableQty(item_id,loc_id,cus_id){
    return this.http.get(this._API.API_WH_XFER + "/get-avail-qty/"+item_id+"/"+loc_id+"/"+cus_id,{ headers: this.AuthHeader })
      .map((res: Response) => res.json().data);
  }



}
