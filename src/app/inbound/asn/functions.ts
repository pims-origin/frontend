/*
* ANSToolFunction
* @tien.nguyen
* */
import {Component} from '@angular/core';
import { ControlGroup, ControlArray, Control} from '@angular/common';
import {ASN_Services} from './asn-services';
import {Functions} from '../../common/core/load';
import {FormBuilderFunctions} from '../../common/core/formbuilder.functions';
declare var jQuery: any;
@Component ({
    providers: [ASN_Services]
})
export class ASNFunctions
{
    constructor(private asnService:ASN_Services,private _globalFunc:Functions,private fbdFunc:FormBuilderFunctions) {}

    public  checkTotalExpCarton(controlArray:ControlArray){

        let expcart=controlArray.value;
        let expcartTotal=0;
        for (let i in expcart){
            expcartTotal=expcartTotal+parseInt(expcart[i]['dtl_ctn_ttl']);
        }
        if(expcartTotal>99999){
            for (let i in expcart){
                this.fbdFunc.setErrorsControlItem(controlArray,i,'dtl_ctn_ttl',{'invalidMaxValue':true})
            }
        }else{
           this.fbdFunc.removeErrorsByFieldName(controlArray,['dtl_ctn_ttl'],'invalidMaxValue');
        }

    }

    public getUOMItem(uom_id:any,UOMList:Array<any>=[]){

           for(let item of UOMList){
                if(uom_id == item['uom_id']){
                    return item;
                }
            }

    }

    public checkDuplicatedKey(controlArray:ControlArray,keys){

        this.fbdFunc.removeErrorsByFieldName(controlArray,keys,'duplicated');
        let Arr=controlArray.value;
        for(let i=0;i<Arr.length;i++) {
            let item = this._createItemToCheckDuplicated(Arr[i],keys);
            for(let j=i+1;j<Arr.length;j++) {
                let itemj=this._createItemToCheckDuplicated(Arr[j],keys);
                if(i==j) {continue;}
                if((item&&itemj)&&item==itemj) {
                    this.setDuplicateItem(controlArray,i,j,keys);
                }
            }
        }

    }

    /*===================================
     * Generator string to check duplicated
     *=================================== */
    public _createItemToCheckDuplicated(dataArray:Object,key){

        let string='';
        let validKey=0;
        let anyKey=0;
        for (let i in key){
            string+=dataArray[key[i]];
            validKey++;
            if(dataArray[key[i]]){
                anyKey++;
            }
        }
        if(validKey==key.length && anyKey>0){
            return string;
        }else{
            return false;
        }

    }

    /*============================================================
     * setDuplicateItem
     * ==========================================================*/
    private setDuplicateItem(controlArray:ControlArray,row,next_row,keys){

        for(let i in keys){
            this.fbdFunc.setErrorsControlItem(controlArray,row,keys[i],{duplicated:true});
            this.fbdFunc.setErrorsControlItem(controlArray,next_row,keys[i],{duplicated:true});
            (<Control>controlArray.controls[row]['controls'][keys[i]]).markAsTouched();
            (<Control>controlArray.controls[next_row]['controls'][keys[i]]).markAsTouched();
        }

    }


}
