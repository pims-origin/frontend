
import {Component, ViewChild} from '@angular/core';
import {API_Config} from '../../common/core/API_Config';
import {BaseClass} from '../../common/core/classes_us/base-class';
import {TableFunctions} from '../../common/core/table-functions';
import {DateFunctions} from '../../common/core/functions/date-functions';
import {AdvanceTable_US, WMSMessages, WMSBreadcrumb} from '../../common/directives/directives';
import {OrderBy} from '../../common/pipes/order-by.pipe';
import {BaseService} from '../../common/core/classes_us/base-service';
import {CustomersService} from '../../master-data/customers/customers-service';
import {InvoiceListCustomersComponent} from './invoice-list-customers.component';
import {InvoiceListDetailsComponent} from './invoice-list-details.component';
import {InvoiceInfo} from './invoice-info/invoice-info.component';
import {ClientInfo} from './client-info/client-info.component';

import {Alert} from '../../common/directives/popup-modal_us/alert/alert';

declare var jQuery: any;

@Component({
    selector: 'invoice-list',
    directives: [
        AdvanceTable_US,
        WMSMessages,
        WMSBreadcrumb,
        InvoiceListCustomersComponent,
        InvoiceListDetailsComponent,
        InvoiceInfo,
        ClientInfo,
        Alert
    ],
    providers: [
        TableFunctions,
        DateFunctions,
        BaseService,
        CustomersService
    ],
    pipes: [OrderBy],
    templateUrl: 'invoice-list.component.html'
})

export class InvoiceListComponent extends BaseClass
{
    @ViewChild(InvoiceInfo) invoiceInfo: InvoiceInfo;

    @ViewChild(ClientInfo) clientInfo: ClientInfo;

    @ViewChild(InvoiceListCustomersComponent) _invoiceListCustomersComponent: InvoiceListCustomersComponent;
    @ViewChild(InvoiceListDetailsComponent) _invoiceListDetailsComponent: InvoiceListDetailsComponent;

    @ViewChild(Alert) alert: Alert;

    public headerURL = this._api.API_User_Metas + '/rep';
    public isSummary = true;
    public showLoadingOverlay = false;

    private billedItems = {};

    private invoiceListSearches = {
        whs_id: this.whs_id,
        cus_id: 0,
        inv_sts: '',
        fromDate: '',
        toDate: ''
    };

    private customers = [];

    private chargeTypes = {
        receiving: false,
        storage: false,
        processing: false
    };

    private areCanceledOrders = false;
    private invoiceDetails = false;
    private detailsInfo: any [];
    private processUrl = this._api.API_Invioce_Process;

    constructor(
        private _tableFunc: TableFunctions,
        private _api: API_Config,
        private _baseServices: BaseService,
        private _customersService: CustomersService
    )
    {
        super();

        this.setInvoiceDates();

        this.getCustomers();

        setTimeout(() => {
            this.updateInvoiceListGrid();
        }, 0);
    }

    //**************************************************************************

    private setInvoiceDates()
    {
        let date = new Date();

        date.setDate(1);

        date.setHours(-1);

        let year = date.getFullYear(),
            month = date.getMonth() + 1,
            endDay = date.getDate();

        let monthStr = month.toString();

        monthStr = month < 10 ? '0' + monthStr : monthStr;

        this.invoiceListSearches.fromDate = year + '-' + monthStr + '-01';
        this.invoiceListSearches.toDate = year + '-' + monthStr + '-' + endDay;
    }

    //**************************************************************************

    private getIfInvoiceTypeChecked()
    {
        for (let chargeType in this.chargeTypes) {
            if (this.chargeTypes[chargeType]) {
                return true;
            }
        }

        return false;
    }

    //**************************************************************************

    private getCustomers()
    {
        this._tableFunc.getDropDown({
            dropdown: {
                idField: 'cus_id',
                nameField: 'cus_name',
                service: '_customersService'
            },
            tableComponent: this,
            output: this.customers
        });
    }

    //**************************************************************************

    private selectCustomerTable()
    {
        for (let prop in this.chargeTypes) {
            this.chargeTypes[prop] = false;
        }

        jQuery('.invoiceType').prop('checked', false);

        this.invoiceDetails = false;
    }

    //**************************************************************************

    public onCustomerChange(customerID)
    {
        this.invoiceListSearches.cus_id = customerID;

        this.selectCustomerTable();

        this.updateInvoiceDetails();
        this.updateInvoiceListGrid();
    }

    //**************************************************************************

    public onDateBlur(date, dateType)
    {
        // the 1-st datepicked blur event happens when a user picks a date,
        // the 2-nd datepicked blur event happens after the input box loses focus
        if (this.invoiceListSearches[dateType] != date) {

            this.invoiceListSearches[dateType] = date;

            this.selectCustomerTable();

            this.updateInvoiceDetails();
            this.updateInvoiceListGrid();
        }
    }

    //**************************************************************************

    public onStatusChange($event)
    {
        let status = $event.target.value == 'Display All' ? '' :
                $event.srcElement[$event.target.value].innerHTML;

        this.invoiceListSearches.inv_sts = status;

        this.updateInvoiceListGrid();
    }

    //**************************************************************************

    public selectProcessing(checked)
    {
        this._invoiceListDetailsComponent.selectProcessing(checked);
    }

    //**************************************************************************

    private updateInvoiceDetails()
    {
        let params = '?' + this._tableFunc.getSearchParams(this.invoiceListSearches);

        let url = this._api.API_INVOICE_LIST_DETAILS + params

        this.showLoadingOverlay = true;

        this._baseServices.get(url)
            .subscribe(
                data => {

                    this.detailsInfo = data;

                    this.updateInvoiceDetailstGrid();
                },
                err => {},
                () => {

                    this.billedItems = {};

                    this.showLoadingOverlay = false;
                }
            );
    }

    //**************************************************************************

    private updateInvoiceListGrid()
    {
        this.isSummary = true;

        this._invoiceListCustomersComponent.gridUpdate(this.invoiceListSearches);
    }

    //**************************************************************************

    private updateInvoiceDetailstGrid()
    {
        this.areCanceledOrders = this.checkCanceledOrders();

        jQuery('#selectAll').prop('checked', false);

        this._invoiceListDetailsComponent.gridUpdate(this.detailsInfo,
                this.chargeTypes, this.billedItems);
    }

    //**************************************************************************

    private checkCanceledOrders()
    {
        this.areCanceledOrders = false;

        for (let index in this.detailsInfo) {

            let info = this.detailsInfo[index];

            if (info.cat == 'Order Processing' && info.order == 'Cancel') {
                return true;
            }
        }

        return false;
    }

    //**************************************************************************

    public onChargeTypeChange(isChecked, chargeType)
    {
        this.chargeTypes[chargeType] = isChecked;

        this.invoiceDetails = this.getIfInvoiceTypeChecked();

        this.updateInvoiceDetailstGrid()
    }

    //**************************************************************************

    public editClientProfile()
    {
        if (this.invoiceListSearches.cus_id) {
            this.clientInfo.showClientInfo(this.invoiceListSearches.cus_id);
        }
    }

    //**************************************************************************

    public callInvoiceIssue()
    {
        let params = '',
            openCust = 0;

        if (this.invoiceDetails) {
            if (jQuery('.processing:checked', 'invoice-list-details').length) {
                openCust = this.invoiceListSearches.cus_id;
            }
        } else {
            openCust = jQuery('.selectInvoice:checked', '#invoice-list-customer')
                    .val();
        }

        let cus_id = this.invoiceListSearches.cus_id ?
                        this.invoiceListSearches.cus_id : '',
            items = this.invoiceListSearches.cus_id && this.invoiceDetails ?
                        this.getCheckedBillable() : null;

        if (openCust) {
            params = JSON.stringify({
                whs_id: this.whs_id,
                openCust: openCust,
                items: items,
                custID: cus_id,
                fromDate: this.invoiceListSearches.fromDate,
                toDate: this.invoiceListSearches.toDate,
                selectedCategories: {
                    receivingChecked: jQuery('#recSelected').is(':checked') ?
                        'on' : 'off',
                    storageChecked: jQuery('#storSelected').is(':checked') ?
                        'on' : 'off',
                    processingChecked: jQuery('#orderSelected').is(':checked') ?
                        'on' : 'off'
                }
            });
        }

        if (params) {
            // this showLoadingOverlay will be removed in saveInvoiceData()
            this.showLoadingOverlay = true;

            let url = this.processUrl + '/checkSummaryDate'
                   + '?toDate=' + this.invoiceListSearches.toDate
                   + '&openCust=' + openCust + '&custID=' + cus_id;

            this._baseServices.get(url)
                .subscribe(
                    data => {

                        this.isSummary = data;

                        if (this.isSummary) {
                            this.invoiceIssue(params, items);
                        } else {
                            this.showLoadingOverlay = false;
                        }
                    },
                    err => {
                        this.showLoadingOverlay = false;
                    },
                    () => {}
                );
        } else {
            this.alert.show('No invoice data was selected');
        }
    }

    //**************************************************************************

    private invoiceIssue(params, items)
    {
        this._baseServices.post(this.processUrl, params)
            .subscribe(
                data => {

                    data['cus_name'] = jQuery('#customer-name option:selected')
                            .text();

                    this.saveInvoiceData(data, items);
                },
                err => {
                    this.showLoadingOverlay = false;
                },
                () => {}
            );
    }

    //**************************************************************************

    private saveInvoiceData(data, items)
    {
        let url = this.processUrl + '/saveCreateParams',
            params = JSON.stringify(data),
            cus_id = data.cus_id;

        this._baseServices.post(url, params)
            .subscribe(
                data => {

                    let checked = items ? ':checked' : '';

                    // have to use filter() function cause
                    // jQuery('#invoice-list-customer .selectInvoice [value="' + cus_id + '"]')
                    // does not return any radio

                    jQuery('#invoice-list-customer .selectInvoice').filter(function(){
                        return this.value == cus_id;
                    }).remove();

                    jQuery('#invoice-list-customer .selectInvoice [value="' + cus_id + '"]').remove();
                    jQuery('#invoice-list-details .processing' + checked).remove();

                    if (items && this.billedItems != 'all') {
                        for (let item in items) {
                            for (let key in items[item]) {
                                if (! this.billedItems.hasOwnProperty(item)) {
                                    this.billedItems[item] = {};
                                }

                                this.billedItems[item][key] = items[item][key];
                            }
                        }
                    } else {
                        this.billedItems = 'all';
                    }

                    window.open('/#/invoice/invoice-issue/create/' + data,
                            '_blank');
                },
                err => {},
                () => {
                    this.showLoadingOverlay = false;
                }
            );
    }

    //**************************************************************************

    private getCheckedBillable()
    {
        let results = {};

        jQuery('#invoice-list-details .processing:checked').map(function () {

            let cat = jQuery(this).attr('data-cat'),
                id = jQuery(this).attr('data-id');

            results[cat] = results.hasOwnProperty(cat) ? results[cat] : {};

            results[cat][id] = 'on';
        });

        return results;
    }

    //**************************************************************************

    public printStatementInvoice()
    {
        let cus_id = this.invoiceListSearches.cus_id,
            startDate = this.invoiceListSearches.fromDate,
            endDate =  this.invoiceListSearches.toDate;

        let params = '?whsId=' + this.whs_id + '&cusId=' + cus_id
                   + '&startDate=' + startDate + '&endDate=' + endDate;

        let checkUrl = this._api.API_Get_Print_Statement_Data + params;

        this.showLoadingOverlay = true;

        this._baseServices.get(checkUrl)
            .subscribe(
                data => {
                    if (data.length) {

                        let printUrl = this._api.API_Print_Statement + params,
                            filename = 'Invoice Statement_' + cus_id + '.pdf';

                        this._baseServices.print(printUrl, filename);
                    } else {
                        this.alert.show('No data for output');
                    }
                },
                err => {},
                () => {
                    this.showLoadingOverlay = false;
                }
            );
    }

    //**************************************************************************

}
