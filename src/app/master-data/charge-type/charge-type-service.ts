import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {API_Config, Functions} from '../../common/core/load';

@Injectable()
export class ChargeTypeService {

    public AuthHeader = this._Func.AuthHeader();
    public AuthHeaderPost = this._Func.AuthHeaderPost();

    constructor(private _Func: Functions, private _API: API_Config, private http: Http)
    {

    }

    ChargeType () {
        return this.http.get(this._API.API_CHARGE_TYPE + '?limit=1000',{ headers: this.AuthHeader })
            .map((res: Response) => res.json());
    }

    getChargeType(params) {
        return this.http.get(this._API.API_CHARGE_TYPE + params,{ headers: this.AuthHeader })
            .map((res: Response) => res.json());
    }

    addNewChgType(data) {
        return this.http.post(this._API.API_CHARGE_TYPE, data, { headers: this._Func.AuthHeaderPostJson() })
            .map((res: Response) => res.json().data);
    }

    updateChgType(id, data) {
        return this.http.put(this._API.API_CHARGE_TYPE+'/'+id, data, { headers: this._Func.AuthHeaderPostJson() })
            .map((res: Response) => res.json().data);
    }

    getChgTypeDetail(id) {
        return this.http.get(this._API.API_CHARGE_TYPE+'/'+id, { headers: this.AuthHeader })
            .map((res: Response) => res.json().data);
    }

    search(param) {
        return this.http.get(this._API.API_CHARGE_TYPE+param,{ headers: this.AuthHeader })
            .map((res: Response) => res.json());
    }

    //**************************************************************************

    getDropdown()
    {
        let params = '?sort[chg_type_name]=asc';

        return this.http.get(this._API.API_Charge_Types + '/dropdown' + params, {
            headers: this.AuthHeader
        })
        .map((res: Response) => res.json());
    }

    //**************************************************************************

}
