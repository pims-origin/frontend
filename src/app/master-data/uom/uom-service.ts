import {Component, Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {API_Config, Functions} from '../../common/core/load';

@Injectable()
export class UOMServices{
    public AuthHeader = this._Func.AuthHeader();
    public AuthHeaderPost = this._Func.AuthHeaderPost();

    constructor(private _Func: Functions, private _API: API_Config, private http: Http)
    {

    }
    addNewUOM(data) {
        return this.http.post(this._API.API_UOM, data, { headers: this._Func.AuthHeaderPostJson() })
            .map((res: Response) => res.json().data);
    }

    updateUOM(id, data) {
        return this.http.put(this._API.API_UOM+'/'+id, data, { headers: this._Func.AuthHeaderPostJson() })
            .map((res: Response) => res.json().data);
    }

    getUOMDetail(id) {
        return this.http.get(this._API.API_UOM+'/'+id, { headers: this.AuthHeader })
            .map((res: Response) => res.json().data);
    }
    
    getPackingLevel(){
        return this.http.get(this._API.API_ITEM_SERVICE+'/item-level',{ headers: this.AuthHeader })
            .map((res: Response) => res.json());
    }    

    getUOM(param){
        return this.http.get(this._API.API_UOM+param,{ headers: this.AuthHeader })
            .map((res: Response) => res.json());
    }

    deleteUOM(id){
        return this.http.delete(this._API.API_UOM + "/" + id, { headers: this.AuthHeader });
    }
    search(param)
    {
        return this.http.get(this._API.API_UOM+param,{ headers: this.AuthHeader })
            .map((res: Response) => res.json());
    }

    //**************************************************************************

    getDropdown()
    {
        let params = '?sort[chg_type_name]=asc';

        return this.http.get(this._API.API_System_UOMs + '/dropdown' + params, {
            headers: this.AuthHeader
        })
        .map((res: Response) => res.json());
    }

    //**************************************************************************

}