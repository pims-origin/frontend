import { Component} from '@angular/core';
import { Http } from '@angular/http';
import { Router, RouterLink, ROUTER_DIRECTIVES } from '@angular/router-deprecated';
import 'ag-grid-enterprise/main';
import 'rxjs/add/operator/map';
import {API_Config} from "../../../common/core/API_Config";
import {UserService} from "../../../common/users/users.service";
import {Functions} from "../../../common/core/functions";
import { FORM_DIRECTIVES,ControlGroup,FormBuilder, Control} from '@angular/common';
import {WMSBreadcrumb,WMSMessages, WMSPagination} from '../../../common/directives/directives';
declare var jQuery: any;

@Component ({
    selector: 'location-type-list',
    directives:[WMSBreadcrumb, WMSPagination],
    templateUrl: 'location-type-list.component.html',
})

export class LocationTypeListComponent {

    private dataListLocaType = [];
    private selectedAll = false;
    private viewSystem: boolean = false;
    private headerGet: any;
    private headerPost: any;
    private urlAPILocation: any;
    private urlAPILocationType: any;
    private Pagination;
    private totalCount = 0;
    private tmpPageCount;
    private pageCount=0;
    private ItemOnPage=0;
    private currentPage = 1;
    private perPage = 20;
    private numLinks = 3;
    private messages;
    public loading = [];
    public listSelectedItem = [];
    searchForm: ControlGroup;
    public showLoadingOverlay = false;
    private hasEditPermission = false;
    private hasCreatePermission = false;
    private viewUser = false;
    private searchCriteria = {};

    constructor (
        private _http: Http,
        private _router:Router,
        private _user: UserService,
        private fb: FormBuilder,
        private _func: Functions,
        private _API_Config: API_Config
    ) {
        // Check permission for user using this page
        // this.ck_permisson();
        this.headerGet = this._func.AuthHeader();
        this.headerPost = this._func.AuthHeaderPost();
        this.urlAPILocation = this._API_Config.API_Location;
        this.urlAPILocationType = this._API_Config.API_Location_type;
        this.builderSearchForm();
        this.searchCriteria = this._func.genSearchCriteriaFromForm(this.searchForm);
        this.checkPermission()
    }

    // Check permission for user using this function page
    private checkPermission() {
        this.showLoadingOverlay = true;
        this._user.GetPermissionUser().subscribe(
            data => {
                this.hasEditPermission = this._user.RequestPermission(data, 'editLocationType');
                this.hasCreatePermission = this._user.RequestPermission(data, 'createLocationType');
                this.viewUser = this._user.RequestPermission(data,'viewLocationType');
                /* Check orther permission if View allow */
                if(!this.viewUser) {
                    this._router.parent.navigateByUrl('/deny');
                }
                else {
                    this.getLocaTypeList();
                }
            },
            err => {
                this.showMessage('danger', this._func.parseErrorMessageFromServer(err));
                this.showLoadingOverlay = false;
            }
        );
    }

    private showMessage(msgStatus, msg) {
        this.messages = {
            'status': msgStatus,
            'txt': msg
        }
        jQuery(window).scrollTop(0);
    }

    // Event when user filter form
    private onPageSizeChanged ($event) {

        this.perPage = $event.target.value;
        if((this.Pagination['current_page'] - 1) * this.perPage >= this.Pagination['total']) {
            this.currentPage = 1;
        }
        else {
            this.currentPage = this.Pagination['current_page'];
        }
        this.getLocaTypeList(this.currentPage);

    }

    // Get location
    private getLocaTypeList (page = null) {
        let params="?page="+page+"&limit="+this.perPage;
        if(Object.keys(this.searchCriteria).length !== 0) // check if searchCriteria Object not empty  ( {} )
        {
            params += '&loc_type_code=' + encodeURIComponent(this.searchCriteria['loc_type_code']) +
                '&loc_type_name=' + encodeURIComponent(this.searchCriteria['loc_type_name']);
        }
        if(this.sortQuery != '') {
            params += '&'+this.sortQuery;
        }

        this.loading['getLocationType'] = true;
        this.showLoadingOverlay = true;

        this._http.get(this.urlAPILocationType + params , {headers: this.headerGet})
            .map(res => res.json())
            .subscribe(
                data => {
                    this.loading['searching']=false;
                    this.selectedAll = false;
                    this.listSelectedItem=[];
                    this.dataListLocaType = data.data;
                    //pagin function
                    this.Pagination=data['meta']['pagination'];
                    this.Pagination['numLinks']=3;
                    this.Pagination['tmpPageCount']=this._func.Pagination(this.Pagination);
                    this.showLoadingOverlay = false;

                },
                err => {
                    this.parseError(err);
                },
                () => {
                    this.loading['getLocationType'] = false;
                }
            );

    }

    private getPage(num) {
        let arr=new Array(num);
        return arr;
    }

    Search(data) {
        this.messages = null;
        let param="?page="+1+"&limit="+this.perPage;
        if(this.sortQuery != '') {
            param += '&'+this.sortQuery;
        }
        this.searchCriteria = this._func.genSearchCriteriaFromForm(this.searchForm);
        // let encodeSearchQuery = param + "&loc_type_code=" + encodeURIComponent(this.searchCriteria['loc_type_code'])+
        //                             "&loc_type_name=" + encodeURIComponent(this.searchCriteria['loc_type_name']);
        this.loading['searching']=true;
        this.getLocaTypeList(1);
    }

    // Add location type
    // Navigation to Add page
    private add() {

        this._router.parent.navigateByUrl('/master-data/location-type/new');
    }

    // Edit Role
    // Navigation to edit page
    private edit() {

        if(jQuery('#w-content-per :checkbox:checked:not([id="ck-first"])').length > 1 ) {

            this.messages = this._func.Messages('danger', 'Please choose only one item to edit');
            return false;
        }

        if(jQuery('#w-content-per :checkbox:checked:not([id="ck-first"])').length < 1 ) {

            this.messages = this._func.Messages('danger', 'Please choose one item to edit');
            return false;
        }

        var idLocaType = jQuery('#w-content-per :checkbox:checked:not([id="ck-first"])').val();

        this._router.parent.navigateByUrl('/master-data/location-type/' + idLocaType + "/edit");
    }

    // Event for check all
    public delete() {

        if(jQuery(':checkbox:checked:not([id="ck-first"])').length < 1 ) {

            this.messages = this._func.Messages('danger', 'You must choose one !!!');
            return false;
        }

        if(!confirm("Do you want delete action ?")) {

            return false;
        }

        var tmpDelete = [];
        jQuery(':checkbox:not([id="ck-first"])').each(function(i) {

            if (jQuery(this).context.checked) {

                tmpDelete.push(jQuery(this).val().trim());

            }
        });

        var params = 'ids=' + tmpDelete.join(",");
        this._http.delete(this.urlAPILocationType, {body: params, headers: this.headerPost})
            .subscribe(
                data => {

                    if(data.status == 204) {

                        this.messages = this._func.Messages('success', 'Delete data success !!!');
                        var params = "?page=" + this.currentPage + "&limit=" + this.perPage;
                        this.getLocaTypeList(params);
                    } else {

                        this.parseError(data);
                    }
                },
                err =>  {

                    this.parseError(err);
                },
                () => {}
            );

    }

    private builderSearchForm() {
        this.searchForm =this.fb.group({
            'loc_type_code': [''],
            'loc_type_name': [''],
        });
    }

    resetSearch(data) {
        (<Control>this.searchForm.controls["loc_type_code"]).updateValue("");
        (<Control>this.searchForm.controls["loc_type_name"]).updateValue("");
        this.searchCriteria = this._func.genSearchCriteriaFromForm(this.searchForm);
        this.getLocaTypeList(this.Pagination['current_page']);
    }

    private parseError (err) {

        this.messages = this._func.Messages('danger', this._func.msg('VR100'));

    }

    private filterList(num) {
        this.currentPage = num;
        this.getLocaTypeList(num);
    }

    private FieldsToSortFlag = {
        loc_type_code: false,
        loc_type_name: false,
    };
    private countClick = {
        loc_type_code: 3,
        loc_type_name: 3,
    };
    private sortQuery = 'sort[loc_type_code]=asc';
    sort(field) {
        if(this.countClick[field] == 3) {
            this.countClick[field] = 0;
        }

        this.countClick[field] ++;
        if(this.countClick[field] == 3) {
            this.sortQuery = 'sort[loc_type_code]=asc';
        }
        else {
            this.FieldsToSortFlag[field] = !this.FieldsToSortFlag[field];

            this.sortQuery = 'sort['+field+']';
            if(this.FieldsToSortFlag[field]) {
                this.sortQuery += '=asc';
            } else {
                this.sortQuery += '=desc';
            }
        }

        if(field) {
            for (let sortField in this.FieldsToSortFlag ) {
                if(sortField != field) {
                    this.FieldsToSortFlag[sortField] = false;
                    this.countClick[sortField] = 3;
                }
            }
        }
        this.getLocaTypeList(this.currentPage);
    }

    private checkAll(event) {
        this.selectedAll = !this.selectedAll;
        // set empty array  ListSelectedItem
        this.listSelectedItem=[];
        // Loop
        this.dataListLocaType.forEach((item) => {
            item['selected'] = this.selectedAll;
            if(this.selectedAll) {
                // if check all = true , pushing to ListSelectedItem
                this.listSelectedItem.push(item['loc_type_id']);
            }
        })
    }


    private editSelectedItem() {
        if (this.listSelectedItem.length > 1) {
            this.messages = this._func.Messages('danger',this._func.msg('CF004'));
        }
        else {
            if(this.listSelectedItem.length) {
                this._router.navigateByUrl('/master-data/location-type/' + this.listSelectedItem[0] + '/edit');
            }
            else {
                this.messages = this._func.Messages('danger',this._func.msg('CF005'));
            }
        }
    }

    private checkSelectItem(item) {
        let id = item['loc_type_id'];
        item.selected = !item.selected;
        this.selectedAll = false;
        if (item.selected) {
            this.listSelectedItem.push(id);
            this.setItemListSelected(id, true);

            if(this.listSelectedItem.length==this.dataListLocaType.length)
            {
                this.selectedAll=true;
            }
        }
        else {
            let el = this.listSelectedItem.indexOf(id);
            // remove item out of array
            this.listSelectedItem.splice(el, 1);
            this.setItemListSelected(id, false);
        }
    }

    private setItemListSelected(id,value) {
        this.dataListLocaType.forEach((item) => {
            if(item['loc_type_id'] == id) {
                item['selected'] = value;
            }
        });
    }
}
