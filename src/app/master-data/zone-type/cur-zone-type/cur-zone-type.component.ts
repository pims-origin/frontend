import {Component} from '@angular/core';
import { FORM_DIRECTIVES,ControlGroup,FormBuilder, Control ,Validators} from '@angular/common';
import {RouteParams,Router,RouteData} from '@angular/router-deprecated';
import {API_Config, UserService, Functions} from '../../../common/core/load';
import {ZoneTypeServices} from '../zone-type-service';
import { Http } from '@angular/http';
import {GoBackComponent} from "../../../common/component/goBack.component";
import {ErrorMessageForControlService} from "../../../common/services/error-message-for-control.service";
import { ValidationService } from '../../../common/core/validator';
import {BoxPopup} from "../../../common/popup/box-popup";
import {WarningBoxPopup} from "../../../common/popup/warning-box-popup";
import {BoxPopupService} from "../../../common/popup/box-popup.service";
import {JwtHelper} from "angular2-jwt/angular2-jwt";
import {WMSBreadcrumb,WMSMessages} from '../../../common/directives/directives';
declare var jQuery: any;
@Component({
  selector: 'add-zone-type',
  providers: [ZoneTypeServices,FormBuilder, BoxPopupService, ErrorMessageForControlService, ValidationService],
  directives: [FORM_DIRECTIVES,GoBackComponent,WMSMessages,WMSBreadcrumb],
  templateUrl: 'cur-zone-type.component.html',
})

export class CURZoneTypeComponent {

  public Loading = [];
  private messages;
  private Action;
  private zonetype_id;
  private TitlePage='';
  private IsView:boolean=false;
  private ZoneTypeDetail=[];
  ZoneTypeForm: ControlGroup;
  private showErr:boolean = false;
  public showLoadingOverlay = false;
  private submitted:boolean = false;

  constructor(
    private _http: Http,
    private _Func: Functions,
    private _user: UserService,
    private params: RouteParams,
    private _Valid: ValidationService,
    private _ZTP: ZoneTypeServices,
    private _ErrMsgControlService: ErrorMessageForControlService,
    private _boxPopupService: BoxPopupService,
    private fb: FormBuilder,
    _RTAction: RouteData,
    private _router: Router) {

    this.Action=_RTAction.get('action');
    this.checkPermission();

    this._ErrMsgControlService.errorMsg.invalidSpace = 'is required.';  // set error message for invalid space

    if(this.Action=='view')
    {
      this.IsView=true;
      this.TitlePage='View Zone Type';
    }
    if(this.Action=='edit')
    {
      this.TitlePage='Edit Zone Type';
    }
    if(this.Action=='edit'||this.Action=='view')
    {
      // edit
      this.zonetype_id=this.params.get('id');

      this._ZTP.Get_ZoneTypeDetail(this.zonetype_id).subscribe(
        data => {

          this.ZoneTypeForm =this.fb.group({

            zone_type_name: [data['zone_type_name'], Validators.compose([Validators.required, this._Valid.validateSpace, this._Valid.invalidNameStr])],
            zone_type_code: [data['zone_type_code'], Validators.compose([Validators.required, this._Valid.validateSpace, this._Valid.invalidLength2Or3])],
            zone_type_desc: [data['zone_type_desc']],

          });

        },
        err => {
          this._router.parent.navigateByUrl('/master-data/zone-type');
        },
        () => {}
      );

    }
    else{
      // new
      if(this.Action=='new')
      {
        this.BuilderForm();
        this.TitlePage='Add Zone Type';
      }
      else{
        this._router.parent.navigateByUrl('/master-data/zone-type');
      }
    }


  }

  private checkPermission() {
    this.showLoadingOverlay = true;
    this._user.GetPermissionUser().subscribe(
        data => {
          if((this.Action == 'view' && !this._user.RequestPermission(data,'viewZoneType')) || (this.Action == 'edit' && !this._user.RequestPermission(data,'editZoneType')) || (this.Action == 'new' && !this._user.RequestPermission(data,'createZoneType'))) {
            this._router.parent.navigateByUrl('/deny');
          }
          this.showLoadingOverlay = false;
        },
        err => {
          this.showMessage('danger', this._Func.parseErrorMessageFromServer(err));
          this.showLoadingOverlay = false;
        }
    );
  }

  BuilderForm()
  {
    this.ZoneTypeForm =this.fb.group({

      zone_type_name: [null, Validators.compose([Validators.required, this._Valid.validateSpace, this._Valid.invalidNameStr])],
      zone_type_code: [null, Validators.compose([Validators.required, this._Valid.validateSpace, this._Valid.invalidLength2Or3])],
      zone_type_desc: [''],

    });
  }

  Save(data :Object):void{

    /*check tabs*/

    this._ErrMsgControlService._genErrMsgForMultiControls(this.ZoneTypeForm);
    let n=this;

    this.submitted = true;
    if(this.ZoneTypeForm.valid) {
      let data_json = JSON.stringify(data);

      if (this.Action == 'new') {
        // add new
        this.AddNew(data_json);
      }
      else {
        // edit zone type
        this.Update(this.zonetype_id,data_json);
      }

    }
    else{
      this.showErr = true;
    }


  }

  /*
  *
  * */
  AddNew(data)
  {
    let n=this;
    this.Loading['AddUpdate']=true;
     this._ZTP.Add_New_ZoneType(data).subscribe(
          data => {
            this.Loading['save'] = false;
            this.Loading['AddUpdate']=false;
            //Reset Form
            this.messages = this._Func.Messages('success', this._Func.msg('VR107'));


            if(data['zone_type_id'])
            {
              setTimeout(function(){
                n._router.parent.navigateByUrl('/master-data/zone-type');
              }, 600);

            }
          },
          err => {
            this.Loading['AddUpdate']=false;
            this.showMessage('danger', this._Func.parseErrorMessageFromServer(err));
          },
          () => {}
        );
  }

  Update(id,data)
  {
    let n=this;
    this.Loading['AddUpdate']=true;
        this._ZTP.Update_ZoneType(id,data).subscribe(
          data => {
            this.Loading['update'] = false;
            //Reset Form
            this.messages = this._Func.Messages('success', this._Func.msg('VR109'));
            this.Loading['AddUpdate'] = false;
            if(data['zone_type_id'])
            {
              setTimeout(function(){
                n._router.parent.navigateByUrl('/master-data/zone-type');
              }, 600);

            }
          },
          err => {
            this.Loading['AddUpdate'] = false;
            this.showMessage('danger', this._Func.parseErrorMessageFromServer(err));
          },
          () => {}
        );
    }

    Cancel()
    {
      let n = this;
      /*case 1: modify text*/
      // let warningPopup = new WarningBoxPopup();
      // warningPopup.text = "Are you sure to cancel";
      // this._boxPopupService.showWarningPopup(warningPopup)

      /*case 2: default*/
      this._boxPopupService.showWarningPopup()
          .then(function (dm) {
            if(dm)
              n._router.navigateByUrl('/master-data/zone-type');

          })
    }

  /*
   * obj: object
   * properties: keys of object need to trim
   * */
  _trimObjProperty(obj, properties)
  {
    properties.forEach(function(prop) {
      obj[prop] = obj[prop].trim();
    });
  }

  detectControlErr(control) {
    // this._genErrMsgForControl(control, this.errorMsg);
    this._ErrMsgControlService._genErrMsgForControl(control);
  }

  private showMessage(msgStatus, msg) {
    // if(this.timeoutHideMessage) clearTimeout(this.timeoutHideMessage);
    this.messages = {
      'status': msgStatus,
      'txt': msg
    }
    // this.timeoutHideMessage = setTimeout(() => {
    //     this.messages = null;
    // }, 4000);
    jQuery(window).scrollTop(0);
  }

}
