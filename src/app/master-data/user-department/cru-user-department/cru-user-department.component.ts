/**
 * Created by admin on 12/28/2016.
 */
import {Component} from '@angular/core';
import { FORM_DIRECTIVES,ControlGroup,FormBuilder, Control ,Validators} from '@angular/common';
import {RouteParams,Router,RouteData} from '@angular/router-deprecated';
import {UserService, Functions} from '../../../common/core/load';
import { Http } from '@angular/http';
import {GoBackComponent} from "../../../common/component/goBack.component";
import {ErrorMessageForControlService} from "../../../common/services/error-message-for-control.service";
import { ValidationService } from '../../../common/core/validator';
import {BoxPopupService} from "../../../common/popup/box-popup.service";
import {WMSBreadcrumb,WMSMessages} from '../../../common/directives/directives';
import {UserDepartmentService} from "../user-department-service";
import {PaginationService} from "../../../common/directives/pagination/pagination-service";
import {PaginationControlsCmp} from "../../../common/directives/pagination/pagination-controls-cmp";
import {PaginatePipe} from "../../../common/directives/pagination/paginate-pipe";
declare var jQuery: any;
@Component ({
    selector: 'cru-user-department',
    providers: [UserDepartmentService,FormBuilder, BoxPopupService, ErrorMessageForControlService, ValidationService, PaginationService],
    directives: [FORM_DIRECTIVES,GoBackComponent,WMSMessages,WMSBreadcrumb,PaginationControlsCmp],
    pipes:[PaginatePipe],
    templateUrl: 'cru-user-department.component.html',
})

export class CruUserDepartmentComponent {

    private Loading = [];
    private messages;
    private Action;
    private userDepartmentId;
    private TitlePage='';
    private IsView:boolean=false;
    private ZoneTypeDetail=[];
    userDepartmentForm: ControlGroup;
    private showErr:boolean = false;
    private showLoadingOverlay = false;
    private userList = [];
    private perPage = 20;
    private submitted:boolean = false;

    constructor(
        private _http: Http,
        private _Func: Functions,
        private _user: UserService,
        private params: RouteParams,
        private _Valid: ValidationService,
        private _UDS: UserDepartmentService,
        private _ErrMsgControlService: ErrorMessageForControlService,
        private _boxPopupService: BoxPopupService,
        private fb: FormBuilder,
        _RTAction: RouteData,
        private _router: Router) {

        this.Action=_RTAction.get('action');
        this.checkPermission();

        this._ErrMsgControlService.errorMsg.invalidSpace = 'is required.';  // set error message for invalid space

        if(this.Action=='view') {
            this.IsView=true;
            this.TitlePage='View User Department';
        }
        if(this.Action=='edit')
        {
            this.TitlePage='Edit User Department';
        }
        if(this.Action=='edit'||this.Action=='view')
        {
            // edit
            this.userDepartmentId=this.params.get('id');

            this._UDS.Get_UserDeparmentDetail(this.userDepartmentId).subscribe(
                data => {

                    if(this.Action=='view'){
                        this.userList = data['user_of_department'];
                    }
                    this.userDepartmentForm =this.fb.group({

                        usr_dpm_code: [data['usr_dpm_code'], Validators.compose([Validators.required, this._Valid.validateSpace, this._Valid.invalidLength2Or3])],
                        usr_dpm_name: [data['usr_dpm_name'], Validators.compose([Validators.required, this._Valid.validateSpace, this._Valid.invalidNameStr])],
                        usr_dpm_des: [data['usr_dpm_des']],

                    });

                },
                err => {
                    this._router.parent.navigateByUrl('/master-data/user-department');
                },
                () => {}
            );

        }
        else{
            // new
            if(this.Action=='new')
            {
                this.BuilderForm();
                this.TitlePage='Add User Department';
            }
            else{
                this._router.parent.navigateByUrl('/master-data/user-department');
            }
        }


    }

    private checkPermission() {
        this.showLoadingOverlay = true;
        this._user.GetPermissionUser().subscribe(
            data => {
                if((this.Action == 'view' && !this._user.RequestPermission(data,'viewDepartment')) || (this.Action == 'edit' && !this._user.RequestPermission(data,'editDepartment')) || (this.Action == 'new' && !this._user.RequestPermission(data,'createDepartment'))) {
                    this._router.parent.navigateByUrl('/deny');
                }
                this.showLoadingOverlay = false;
            },
            err => {
                this.showMessage('danger', this._Func.parseErrorMessageFromServer(err));
                this.showLoadingOverlay = false;
            }
        );
    }

    BuilderForm()
    {
        this.userDepartmentForm =this.fb.group({
            usr_dpm_code: [null, Validators.compose([Validators.required, this._Valid.validateSpace, this._Valid.invalidLength2Or3])],
            usr_dpm_name: [null, Validators.compose([Validators.required, this._Valid.validateSpace, this._Valid.invalidNameStr])],
            usr_dpm_des: [''],

        });
    }

    Save(data :Object):void{
        /*check tabs*/
        this._ErrMsgControlService._genErrMsgForMultiControls(this.userDepartmentForm);
        let n=this;

        this.submitted = true;
        if(this.userDepartmentForm.valid) {
            let data_json = JSON.stringify(data);

            if (this.Action == 'new') {
                // add new
                this.AddNew(data_json);
            }
            else {
                // edit zone type
                this.Update(this.userDepartmentId,data_json);
            }

        }
        else{
            this.showErr = true;
        }
    }

    /*
     *
     * */
    AddNew(data)
    {
        let n=this;
        this.Loading['AddUpdate']=true;
        this._UDS.Add_New_UserDepartment(data).subscribe(
            data => {
                this.Loading['save'] = false;
                this.Loading['AddUpdate']=false;
                //Reset Form
                this.messages = this._Func.Messages('success', this._Func.msg('VR107'));
                if(data['usr_dpm_id'])
                {
                    setTimeout(function(){
                        n._router.parent.navigateByUrl('/master-data/user-department');
                    }, 600);

                }
            },
            err => {
                this.Loading['AddUpdate']=false;
                this.showMessage('danger', this._Func.parseErrorMessageFromServer(err));
            },
            () => {}
        );
    }

    Update(id,data)
    {
        let n=this;
        this.Loading['AddUpdate']=true;
        this._UDS.Update_UserDepartment(id,data).subscribe(
            data => {
                this.Loading['update'] = false;
                //Reset Form
                this.messages = this._Func.Messages('success', this._Func.msg('VR109'));
                this.Loading['AddUpdate'] = false;
                if(data['usr_dpm_id'])
                {
                    setTimeout(function(){
                        n._router.parent.navigateByUrl('/master-data/user-department');
                    }, 600);

                }
            },
            err => {
                this.Loading['AddUpdate'] = false;
                this.showMessage('danger', this._Func.parseErrorMessageFromServer(err));
            },
            () => {}
        );
    }

    Cancel()
    {
        let n = this;
        /*case 1: modify text*/
        // let warningPopup = new WarningBoxPopup();
        // warningPopup.text = "Are you sure to cancel";
        // this._boxPopupService.showWarningPopup(warningPopup)

        /*case 2: default*/
        this._boxPopupService.showWarningPopup()
            .then(function (dm) {
                if(dm)
                    n._router.navigateByUrl('/master-data/user-department');

            });
    }

    private fieldsToSortFlag = {
        status: false,
        emp_code: false,
        username: false,
        first_name: false,
        last_name: false,
        email: false,
        created_at: false,
        updated_at: false,
    };

    private countClick = {
        status: 3,
        emp_code: 3,
        username: 3,
        first_name: 3,
        last_name: 3,
        email: 3,
        created_at: 3,
        updated_at: 3,
    };

    private sort(field){
        let sortQuery = '';
        if (this.countClick[field] == 3) {
            this.countClick[field] = 0;
        }

        this.countClick[field]++;
        if (this.countClick[field] == 3) {
            sortQuery = '';
        }
        else {
            this.fieldsToSortFlag[field] = !this.fieldsToSortFlag[field];

            sortQuery = 'sort[' + field + ']';
            if (this.fieldsToSortFlag[field]) {
                sortQuery += '=asc';
            } else {
                sortQuery += '=desc';
            }
        }

        this._UDS.Get_UserDeparmentDetail(this.userDepartmentId,sortQuery).subscribe(
            data => {
                if(this.Action=='view'){
                    this.userList = data['user_of_department'];
                }
            },
            err => {
                /*this._router.parent.navigateByUrl('/master-data/user-department');*/
            },
            () => {}
        );
    }
    /*
     * obj: object
     * properties: keys of object need to trim
     * */
    _trimObjProperty(obj, properties)
    {
        properties.forEach(function(prop) {
            obj[prop] = obj[prop].trim();
        });
    }

    detectControlErr(control) {
        // this._genErrMsgForControl(control, this.errorMsg);
        this._ErrMsgControlService._genErrMsgForControl(control);
    }

    private showMessage(msgStatus, msg) {
        // if(this.timeoutHideMessage) clearTimeout(this.timeoutHideMessage);
        this.messages = {
            'status': msgStatus,
            'txt': msg
        }
        // this.timeoutHideMessage = setTimeout(() => {
        //     this.messages = null;
        // }, 4000);
        jQuery(window).scrollTop(0);
    }

    private onPageSizeChanged($event) {
        this.perPage = parseInt($event.target.value);
    }
}