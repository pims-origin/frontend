/*
 * ANSToolFunction
 * @tien.nguyen
 * */
import {Component} from '@angular/core';
import { ControlGroup, ControlArray, Control} from '@angular/common';
import {Functions} from '../../../common/core/load';
import {FormBuilderFunctions} from '../../../common/core/formbuilder.functions';
declare var jQuery: any;
@Component ({
})
export class PrintCartonBarcodeFunctions
{
    constructor(private _globalFunc:Functions,private fbdFunc:FormBuilderFunctions) {}


    public ckRequireOne(item, itemsTable) {

        let control = (<Control>item['controls']['pack_barcode']);
        var tmp = true;
        this.removeErrorControl(control,'invalidRequireOne');
        for (var i = 0; i < itemsTable['controls'].length; i++) {

            let pack_barcode = itemsTable['controls'][i]['value']['pack_barcode'];

            if (pack_barcode != '') {

                tmp = false;
            }

        }

        if (tmp) {

            control.setErrors({invalidRequireOne: true});
        }
    }

    public removeErrorControl(control:Control,fieldname_to_delete){

        if(control.hasError(fieldname_to_delete)){
            delete  control.errors[fieldname_to_delete];
            if(!Object.keys(control.errors).length) {
                (<Control>control).setErrors(null);
            }
        }

    }

    public buildParams (itemsTable) {

        var params = '';
        for (var i = 0; i < itemsTable['controls'].length; i++) {

            let pack_barcode = itemsTable['controls'][i]['value']['pack_barcode'];

            if (pack_barcode != '') {

                params += 'qty['+ itemsTable['controls'][i]['value']['asn_dtl_id'] + ']='+ pack_barcode + '&';
            }

        }
        params = params.substr(0,params.length - 1);
        return params;
    }

    public checkLessQty(item, index) {

        if (index !== undefined) {

            var pack_barcode = item['value']['pack_barcode'];
            var dtl_gr_dtl_act_ctn_ttl = item['value']['dtl_gr_dtl_act_ctn_ttl'];
            var dtl_ctn_ttl = item['value']['dtl_ctn_ttl'];
            
            let control = (<Control>item['controls']['pack_barcode']);
            this.removeErrorControl(control,'invalidlessQTY');

            if (parseInt(item['value']['pack_barcode']) > 100) {
                return ;
            }

            if (parseInt(item['value']['pack_barcode']) == 0) {
                return ;
            }
            if ((!dtl_gr_dtl_act_ctn_ttl || dtl_gr_dtl_act_ctn_ttl != '') && parseInt(pack_barcode) > parseInt(dtl_gr_dtl_act_ctn_ttl)) {

                item.messageLessQty = 'Label QTY must not greater than Actual Carton QTY';
                control.setErrors({invalidlessQTY: true});

            } else if (dtl_gr_dtl_act_ctn_ttl == '' && dtl_ctn_ttl != ''  && parseInt(pack_barcode) > parseInt(dtl_ctn_ttl)) {

                item.messageLessQty = 'Label QTY must not greater than Expected Carton QTY';
                control.setErrors({invalidlessQTY: true});
            } else if (dtl_ctn_ttl == '' && dtl_gr_dtl_act_ctn_ttl=='') {

                item.messageLessQty = 'Label QTY must not greater than Expected Carton QTY or Actual Carton QTY';
                control.setErrors({invalidlessQTY: true});
            }
        }
    }
    
    public ckLess100(control:Control):{[key:string]:any} {

        if (control.value && control.value > 100) {
            return {'invalidLess100': true};
        }
    }

    private checkInputNumber (evt, flagInt = false) {

        evt = evt || window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;

        if (!evt.ctrlKey && charCode > 31 && (charCode < 48 || charCode > 57)){

            if(flagInt) {

                evt.preventDefault(evt);
            }
            else {

                if(charCode != 46) {

                    evt.preventDefault(evt);
                }
            }
        }
    }
}
