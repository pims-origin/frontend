import {Component} from "@angular/core";
import {Router, ROUTER_DIRECTIVES} from '@angular/router-deprecated';
import {ControlGroup, FormBuilder, Control} from '@angular/common';
import {API_Config, UserService, Functions, FormBuilderFunctions} from '../../common/core/load';
import {BoxPopupService} from "../../common/popup/box-popup.service";
import {CycleCountHistoryReportService} from './cycle-count-history-report.service';
import {WMSPagination, AgGridExtent, WMSMessages, ReportFileExporter, WMSBreadcrumb} from '../../common/directives/directives';
import 'ag-grid-enterprise/main';

declare const $:any;

@Component({
	selector: 'cycle-count-history-report',
	providers: [CycleCountHistoryReportService, BoxPopupService],
	directives: [ROUTER_DIRECTIVES, AgGridExtent, WMSMessages, WMSPagination, ReportFileExporter, WMSBreadcrumb],
	templateUrl: 'cycle-count-history-report.component.html',

})
export class CycleCountHistoryReportComponent {
	private pagination:any;
	private perPage:number = 20;
	private messages:any;
	private searchForm:ControlGroup;
	private sortData = {fieldname: 'updated_at', sort: 'desc'};
	private showLoadingOverlay:boolean = false;
	private cycleCountHisReports:Array<any> = [];
	private headerURL:string = this.apiService.API_User_Metas + '/cch';
	private columnData:any = {
		cus_code: {
			title: 'Customer Code',
			width: 70,
			pin: true,
			ver_table: '123'
		},
		cus_name: {
			title: 'Customer Name',
			width: 120,
			pin: true,
		},
		cycle_num: {
			title: 'Cycle Count Num',
			width: 150,
			pin: true,
		},
		sys_loc_name: {
			title: 'Location',
			width: 120,
			pin: true,
		},
		sku: {
			title: 'SKU',
			pin: true,
			width: 100
		},
		description: {
			title: 'Description',
			width: 100
		},
		lot: {
			title: 'Lot',
			width: 50
		},
		size: {
			title: 'Size',
			width: 50
		},
		color: {
			title: 'Color',
			width: 50
		},
		pack: {
			title: 'Pack Size',
			width: 50
		},
		remain: {
			title: 'Remain QTY',
			width: 50
		},
		act_qty: {
			title: 'Actual QTY',
			width: 70
		},
		count_by: {
			title: 'Count By',
			width: 90
		},
		status: {
			title: 'Status',
			width: 90
		},
		updated_by_name: {
			title: 'Counted By',
			width: 100
		},
		updated_at: {
			title: 'Counted Date',
			width: 100
		}
	};
	private customers:Array<any> = [];
	private cycleNums:Array<any> = [];
	private isLoadingCycleNums:boolean = false;
	private locations:Array<any> = [];
	private isLoadingLocations:boolean = false;
	private SKUs:Array<any> = [];
	private isLoadingSKUs:boolean = false;
	private currentSKU:any = {};
	private statuses:Array<any> = [];
	private isLoadingStatuses:boolean = false;
	private cycleTypes:Array<any> = [];
	private isLoadingCycleTypes:boolean = false;
	private countBys:Array<any> = [];
	private isLoadingCCountBys:boolean = false;

	// Check permission for user using this function page
	private canAccess:boolean = false;

	// Export csv file
	private exportAPIUrl:string = '';
	private fileName:string = 'Cycle-Count-History-Report.csv';

	constructor(private formBuilder:FormBuilder,
	            private cycleCountHisService:CycleCountHistoryReportService,
	            private func:Functions,
	            private fbdFunc:FormBuilderFunctions,
	            private userService:UserService,
	            private apiService:API_Config,
	            private router:Router) {
		this.buildSearchForm();
		this.checkPermission();
		this.initializeCurSKU();
	}

	buildSearchForm() {

		this.searchForm = this.formBuilder.group({
			cus_id: [''],
			cycle_num: [''],
			sys_loc_name: [''],
			sku: [''],
			from: [''],
			to: [''],
			cycle_sts: [''],
			cycle_type: [''],
			count_by: ['']
		});

		const tout = setTimeout(()=>{
			this.fbdFunc.handelDropdownDatePicker(this.searchForm);
			clearTimeout(tout);
		}, 1000);

	}

	initializeCurSKU() {
		this.currentSKU = {size: '', color: '', lot: '', item_id: ''};
	}

	private checkPermission() {
		this.showLoadingOverlay = true;
		this.userService.GetPermissionUser().subscribe(
			data => {
				this.showLoadingOverlay = false;
				this.canAccess = this.userService.RequestPermission(data, 'viewReport');
				if (this.canAccess) {
					this.getCustomers();
					this.getStatuses();
					this.getCycleTypes();
					this.getCountBys();
					this.getCycleCountHisReports();
				} else {
					// redirect to deny page.
					this.router.parent.navigateByUrl('/deny');
				}
			},
			err => {
				this.messages = this.func.Messages('danger', this.func.parseErrorMessageFromServer(err));
				this.showLoadingOverlay = false;
			}
		);
	}

	private changePageSize($event:any) {
		this.perPage = $event.target.value;
		if (this.pagination) {
			if ((this.pagination['current_page'] - 1) * this.perPage >= this.pagination['total']) {
				this.pagination['current_page'] = 1;
			}
			this.getCycleCountHisReports(this.pagination['current_page']);
		}

	}

	private getCycleCountHisReports(pageNum:number = 1) {
		this.searchForm.value['page'] = pageNum;
		this.searchForm.value['limit'] = this.perPage;
		this.searchForm.value['item_id'] = this.currentSKU['item_id'];
		this.searchForm.value['lot'] = this.currentSKU['lot'];
		this.searchForm.value['sort[updated_at]'] = 'desc';
		this.exportAPIUrl = this.cycleCountHisService.getReportAPIUrl(`?` + $.param(this.searchForm.value));

		this.showLoadingOverlay = true;
		this.cycleCountHisService.getReports(`?` + $.param(this.searchForm.value))
			.subscribe(
				data => {
					this.showLoadingOverlay = false;
					this.cycleCountHisReports = data.data;

					// paging
					this.pagination = data['meta']['pagination'];
					this.pagination['numLinks'] = 3;
					this.pagination['tmpPageCount'] = this.func.Pagination(this.pagination);
				},
				err => {
					this.messages = this.func.Messages('danger', err.json().errors.message);
					this.showLoadingOverlay = false;
				},
				() => {}
			);
	}

	private getCustomers() {
		const params:string = '?limit=2000';
		this.cycleCountHisService.getCustomers(params).subscribe(
			data => {
				this.customers = data['data'];
			},
			err => {
				this.messages = this.func.Messages('danger', err.json().errors.message);
			},
			() => {
			}
		);
	}

	private convertMapToList(dataMap:any) {
		let dataList = [];
		for (let key in dataMap) {
			dataList.push({key: key, value: dataMap[key]})
		}
		return dataList;
	}

	private getStatuses() {
		const params:string = '?limit=2000';
		this.cycleCountHisService.getStatues(params).subscribe(
			data => {
				this.statuses = this.convertMapToList(data['data']);
			},
			err => {
				this.messages = this.func.Messages('danger', err.json().errors.message);
			},
			() => {
			}
		);
	}

	private getCycleTypes() {
		const params:string = '?limit=2000';
		this.cycleCountHisService.getCycleTypes(params).subscribe(
			data => {
				this.cycleTypes = this.convertMapToList(data['data']);
			},
			err => {
				this.messages = this.func.Messages('danger', err.json().errors.message);
			},
			() => {
			}
		);
	}

	private getCountBys() {
		const params:string = '?limit=2000';
		this.cycleCountHisService.getCountBys(params).subscribe(
			data => {
				this.countBys = this.convertMapToList(data['data']);
			},
			err => {
				this.messages = this.func.Messages('danger', err.json().errors.message);
			},
			() => {
			}
		);
	}

	loadCycleNums($event:any) {
		if (!this.searchForm.value['cycle_num']) {
			return;
		}

		let queryCycleNumsObj = Object.assign({}, this.searchForm.value);
		queryCycleNumsObj['limit'] = 20;
		this.isLoadingCycleNums = true;
		this.cycleCountHisService.getCycleNums('?' + $.param(queryCycleNumsObj)).debounceTime(400).distinctUntilChanged().subscribe(
			data => {
				this.isLoadingCycleNums = false;
				this.cycleNums = data['data'];
			},
			err => {
				this.isLoadingCycleNums = false;
				this.messages = this.func.Messages('danger', err.json().errors.message);
			},
			() => {
			}
		);
	}

	loadLocations($event:any) {
		if (!this.searchForm.value['sys_loc_name']) {
			return;
		}

		let queryLocationsObj = Object.assign({}, this.searchForm.value);
		queryLocationsObj['limit'] = 20;
		this.isLoadingLocations = true;
		this.cycleCountHisService.getLocations('?' + $.param(queryLocationsObj)).debounceTime(400).distinctUntilChanged().subscribe(
			data => {
				this.isLoadingLocations = false;
				this.locations = data['data'];
			},
			err => {
				this.isLoadingLocations = false;
				this.messages = this.func.Messages('danger', err.json().errors.message);
			},
			() => {
			}
		);
	}

	loadSKUs($event:any) {
		this.initializeCurSKU();
		if (!this.searchForm.value['sku']) {
			return;
		}

		let querySKUsObj = Object.assign({}, this.searchForm.value);
		querySKUsObj['limit'] = 20;

		this.isLoadingSKUs = true;
		this.cycleCountHisService.getSKUs('?' + $.param(querySKUsObj)).debounceTime(400).distinctUntilChanged().subscribe(
			data => {
				this.isLoadingSKUs = false;
				this.SKUs = data['data'];
			},
			err => {
				this.isLoadingSKUs = false;
				this.messages = this.func.Messages('danger', err.json().errors.message);
			},
			() => {
			}
		);
	}

	reset() {
		this.resetSearchForm();
		this.initializeCurSKU();
		this.getCycleCountHisReports();
	}

	search() {
		this.getCycleCountHisReports();
	}

	selectCycleNum($event, cycleNum) {
		(<Control>this.searchForm.controls['cycle_num']).updateValue(cycleNum['cycle_num']);
	}

	selectLocation($event, location) {
		(<Control>this.searchForm.controls['sys_loc_name']).updateValue(location['sys_loc_name']);
	}

	selectSKU($event, sku) {
		this.currentSKU = sku;
		(<Control>this.searchForm.controls['sku']).updateValue(sku['sku']);
	}

	resetSearchForm() {
		(<Control>this.searchForm.controls['cus_id']).updateValue('');
		(<Control>this.searchForm.controls['cycle_num']).updateValue('');
		(<Control>this.searchForm.controls['sys_loc_name']).updateValue('');
		(<Control>this.searchForm.controls['sku']).updateValue('');
		(<Control>this.searchForm.controls['from']).updateValue('');
		(<Control>this.searchForm.controls['to']).updateValue('');
		(<Control>this.searchForm.controls['cycle_sts']).updateValue('');
		(<Control>this.searchForm.controls['cycle_type']).updateValue('');
		(<Control>this.searchForm.controls['count_by']).updateValue('');
	}

	private updateMessages(messages) {
		this.messages = messages;
	}

}
