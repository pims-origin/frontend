import {Component} from '@angular/core';
import {Router, RouterLink, RouteConfig, ROUTER_DIRECTIVES} from '@angular/router-deprecated';
import {DamageTypeListComponent} from "./damage-type-list/damage-type-list.component";
import {CruDamageTypeComponent} from "./cru-damage-type/cru-damage-type.component";

@Component ({
    selector: 'damage-type',
    directives: [ROUTER_DIRECTIVES],
    template: `<router-outlet></router-outlet>`,
})

@RouteConfig([
    { path: '/', component:DamageTypeListComponent , name: 'Damage Type List', useAsDefault: true},
    { path: '/new', component:CruDamageTypeComponent  , name: 'Add Damage Type', data: {action: 'new'}},
    { path: ':id', component:CruDamageTypeComponent  , name: 'View Damage Type',data: {action: 'view'}},
    { path: ':id/edit', component:CruDamageTypeComponent  , name: 'Edit Damage Type', data: {action: 'edit'}}
])
export class DamageTypeComponent {


}
