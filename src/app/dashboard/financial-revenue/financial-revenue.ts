import { Component, Input, Output, EventEmitter } from '@angular/core';
import { WMSMessages } from "../../common/directives/messages/messages";
import { ValidationService } from "../../common/core/validator";
import { Services } from "./service";
import { API_Config, Functions } from '../../common/core/load';
import { PaginatePipe, PaginationControlsCmp, PaginationService } from '../../common/directives/directives';
import { GoBackComponent } from "../../common/component/goBack.component";
declare var $: any;
var moment = require('moment');

@Component({
    selector: 'financial-revenue',
    directives: [GoBackComponent, PaginationControlsCmp, WMSMessages],
    providers: [ValidationService, Services],
    pipes: [PaginatePipe],
    templateUrl: 'financial-revenue.html'
})

export class financialRevenue {


    public Loading = [];
    private showLoadingOverlay: boolean = false;
    private financialDashboardList: Array<any> = [];
    private currentYear = moment().format('YYYY');
    private currentMonth = moment().format('MMMM');
    private whsId: string = '';

    @Input('currentWHSId') set dataExport(value: any) {
        this.whsId = value;
    }
    ngAfterViewInit()
    {
        this.getFinancialDashboard();
    }

    constructor(
        private _Func: Functions,
        private finservice: Services) {

        this.getFinancialDashboard();

    }



    private getFinancialDashboard() {
        this.showLoadingOverlay = true;
        let param = '&limit=9999';
        this.finservice.getFinancialDashboard(this.whsId, param).subscribe(
            data => {
                this.showLoadingOverlay = false;
                this.financialDashboardList = data.data;
            },
            err => {
                this.showLoadingOverlay = false;
            }
        );
    }

    private filter() {

        setTimeout(() => {
            this.getFinancialDashboard();
        }, 200);

    }


    private parseError(err) {
        //this.messages.emit({'status' : 'danger', 'txt' : this._Func.parseErrorMessageFromServer(err)});
    }
}
