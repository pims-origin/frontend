import {Component, Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {Functions, API_Config} from '../../../common/core/load';

@Injectable()
export class AssignMultplePickersService {

    public AuthHeader = this._Func.AuthHeader();
    public AuthHeaderPost = this._Func.AuthHeaderPost();

    constructor(
        private _Func: Functions,
        private _http: Http,
        private _api: API_Config
    ) {

    }

    public getListCRSByCustomer($whs_id = '') {
        return  this._http.get(this._api.API_Authen + '/users/get-picker/' + $whs_id, {headers: this.AuthHeader})
            .map(res => res.json());
    }
    
    public getListPickersAssigned(wakepickid = '') {
        return  this._http.get(this._api.API_Wavepick + `/wave-pick/${wakepickid}/pickers`, {headers: this.AuthHeader})
            .map(res => res.json());
    }


}