import {Component,OnInit} from "@angular/core";
import { Router, RouterLink, ROUTER_DIRECTIVES } from '@angular/router-deprecated';
import { FORM_DIRECTIVES,FormBuilder} from '@angular/common';
import {UserService, Functions} from '../../../common/core/load';
import {WMSBreadcrumb,WMSPagination,PaginationControlsCmp,ColorPickerDirective,WMSMessages,ColorPickerService,PaginationService} from '../../../common/directives/directives';
import {CustomerServices} from "../../../system-setup/customer/customer-service";
import {RF_WavePickServices} from "./wavepick.service";
import {RFGunServices} from "../../rf-gun-service";
import {GoBackComponent} from "../../../common/component/goBack.component";
@Component({
  selector: 'rf-gun',
  providers: [CustomerServices,RF_WavePickServices,RFGunServices,RF_WavePickServices,PaginationService,ColorPickerService,UserService],
  directives: [FORM_DIRECTIVES,WMSPagination,PaginationControlsCmp,ColorPickerDirective,WMSMessages,GoBackComponent,WMSBreadcrumb],
  templateUrl: 'rf-wavepick-list.html',
})
export class RF_WavePickListComponent{

  private show_dashboard_nav=true;
  private page='GR-list-page';
  private WavePickList:Array<any>=[];
  private allowAccess;
  private perPage=10;
  private showLoadingOverlay;
  private messages;
  private Pagination;
  private rfGunPermission;
  private whs_id;
  private grDetail;
  private skuDetail;
  private LocPalletModel={};
  private showMenu:boolean=false;
  private suggestPalletLoc;

  constructor(private rfGunWvService:RF_WavePickServices,
              private _RFGunService:RFGunServices,
              private _router: Router,
              private _user:UserService,
              private _Func: Functions)
  {
    this.checkPermission();
  }

  checkPermission(){

    this.showLoadingOverlay = true;
    this._user.GetPermissionUser().subscribe(
      data => {
        this.showLoadingOverlay = false;
        this.rfGunPermission = this._user.RequestPermission(data, 'viewRFGUN');
        if(this.rfGunPermission) {
          this.allowAccess=true;
          this.whs_id = localStorage.getItem('whs_id');
          this.getWavePickList();
        }
        else{
          this.redirectDeny();
        }
      },
      err => {
        this.parseError(err);
        this.showLoadingOverlay = false;
      }
    );
  }

  redirectDeny(){
    this._router.parent.navigateByUrl('/deny');
  }

  getWavePickList(page='')
  {
    this.showLoadingOverlay=true;
    this.rfGunWvService.RF_GUN_API_GET(this.whs_id,'')
      .subscribe(
        data => {
          this.showLoadingOverlay=false;
          this.WavePickList=data.data;
        },
        err => {
          this.parseError(err);
          this.showLoadingOverlay = false;
        },
        () => {
        }
      );
  }

  viewDetailGR(item)
  {
    this._router.parent.navigateByUrl('/rf-gun/wave-pick/rf-sku-list/'+item['wv_id']);
  }

  // Show error when server die or else
  private parseError (err) {
    err = err.json();
    this.messages = this._Func.Messages('danger', err.errors.message);
  }

}
