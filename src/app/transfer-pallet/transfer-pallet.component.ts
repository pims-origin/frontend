import {Component} from "@angular/core";
import { Router, RouterLink, ROUTER_DIRECTIVES } from '@angular/router-deprecated';
import { FORM_DIRECTIVES,ControlGroup,FormBuilder, Control,Validators} from '@angular/common';
import {API_Config, UserService, Functions} from '../common/core/load';
import { Http } from '@angular/http';
import {WarningBoxPopup} from "../common/popup/warning-box-popup";
import {BoxPopupService} from "../common/popup/box-popup.service";
import {PalletTransferServices} from './service';
import { ValidationService } from '../common/core/validator';
import { FormBuilderFunctions } from '../common/core/formbuilder.functions';
import {WMSPagination,AgGridExtent,WMSMessages,WMSBreadcrumb} from '../common/directives/directives';
import 'ag-grid-enterprise/main';
import {TOOLTIP_DIRECTIVES} from "ng2-bootstrap";
@Component({
    selector: 'transfer-pallet',
    providers:[PalletTransferServices,ValidationService,BoxPopupService],
    directives: [ROUTER_DIRECTIVES,AgGridExtent,WMSMessages,WMSPagination,WMSBreadcrumb,TOOLTIP_DIRECTIVES],
    templateUrl: 'transfer-pallet.component.html',
})
export class TransferPalletComponent{
    private dataForRenderTable:Array<any>=[];
    private headerURL = this.apiService.API_User_Metas+'/trp';
    private columnData = {
        ctn_num:{
            title:'Carton Number',
            width:155,
            pin:true
        },
        item_id: {
            title:'Item ID',
            width:70,
            pin:true,
            ver_table:'30/22234'
        },
        sku:{
            title:'SKU',
            width:120,
            pin:true
        },
        asn_dtl_lot: {
            title:'Lot',
            width:80,
            pin:true
        },
        size : {
            title:'Size',
            width:90,
            pin:true
        },
        color: {
            title:'Color',
            width:90,
            pin:true
        },
        asn_dtl_pack: {
            title:'Pack Size',
            width:90
        },
        uom_name: {
            title:'UOM',
            width:80
        },
        asn_dtl_width: {
            title:'W',
            width:70
        },
        asn_dtl_height: {
            title:'H',
            width:70
        } ,
        asn_dtl_weight: {
            title:'WE',
            width:70
        }
    };
    private whs_code:any;
    private Pagination;
    private perPage=20;
    private whs_id;
    private messages;
    private Loading={};
    private searching={};
    private LocationAutoCompleteData={};
    Form:ControlGroup;
    private locationModel={};
    private sortData={fieldname:'item_id',sort:'asc'};
    private showLoadingOverlay:boolean=false;
    constructor(
        private service:PalletTransferServices,
        private _Func: Functions,
        private _boxPopupService: BoxPopupService,
        private  apiService:API_Config,
        private fb: FormBuilder,
        private fbd:FormBuilderFunctions,
        private _user:UserService,
        private _http: Http,
        private _Valid: ValidationService,
        private _router: Router) {
        this.checkPermission();
        this.buildForm();
        /*
         * Get Warehouse id current
         * */
        if(localStorage.getItem('whs_id'))
        {
            this.whs_id=localStorage.getItem('whs_id');
        }
    }
    // Check permission for user using this function page
    private relocation;
    private allowAccess:boolean=false;
    private checkPermission() {
        this.showLoadingOverlay = true;
        this._user.GetPermissionUser().subscribe(
            data => {
                this.showLoadingOverlay = false;
                this.relocation = this._user.RequestPermission(data, 'relocation');
                if(this.relocation){
                    this.allowAccess=true;
                    // set default to get data
                    this.Form.value['from_loc_id']=0;
                    this.getListItemOfLocation(0);
                    this.getWHScodeById();
                }
                else{
                    this.redirectDeny();
                }
            },
            err => {
                this.messages=this._Func.Messages('danger', this._Func.parseErrorMessageFromServer(err));
                this.showLoadingOverlay = false;
            }
        );
    }

    redirectDeny(){
        this._router.parent.navigateByUrl('/deny');
    }

    private buildForm(){
        this.Form =this.fb.group({
            from_loc_code: ['', Validators.compose([Validators.required, this._Valid.validateSpace])],
            from_loc_id: ['', Validators.required],
            to_loc_code:['',Validators.compose([Validators.pattern('FFFFFFFF[0-9]{16}'), this.isInValidateRFID, this._Valid.validateSpace])]
        });
    }

    private firstSearch:number=0;
    autoSearchLocation(loadingName,ListDataName,controlCode,controlId,key)
    {
        this.exitsLocation[controlCode]=null;
        let keystrim=this._Func.trim(key);
        if(keystrim)
        {
            let n=this;
            // do Search
            let param='?rfid='+encodeURIComponent(keystrim)+'&limit=20';
            n.searching[loadingName]=true;
            n.service.Search(n.whs_id,param).subscribe(
                data => {
                    n.firstSearch++;
                    n.searching[loadingName]=false;
                    n.autoSelectMatchItem(data.data,controlCode,controlId,key,'rfid');
                    n.LocationAutoCompleteData[ListDataName] = data.data;
                    if(n.LocationAutoCompleteData[ListDataName].length==1) {
                        let loccode=n.LocationAutoCompleteData[ListDataName][0];
                        if(loccode['loc_code']==keystrim)
                        {
                            n.selectedItem(controlCode,controlId,loccode);
                        }
                    }
                },
                err =>{
                    n.searching[loadingName]=false;
                },
                () => {}
            );
        }
    }
    private exitsLocation={};
    selectedItem(controlCode,controlId,data)
    {
        this.exitsLocation[controlCode]=true;
        (<Control>this.Form.controls[controlCode]).updateValue(data['rfid']);
        (<Control>this.Form.controls[controlId]).updateValue(data['plt_id']);
        if(controlCode=='from_loc_code'){
            this.getListItemOfLocation();
        }

    }
    checkExitsLocation(controlCode)
    {
        let value=this.Form.value[controlCode];
        if(this.exitsLocation[controlCode]!==true&&value)
        {
            this.exitsLocation[controlCode]=false;
        }
    }
    private onPageSizeChanged($event)
    {
        this.perPage = $event.target.value;
        if(this.Pagination){
            if((this.Pagination['current_page'] - 1) * this.perPage >= this.Pagination['total']) {
                this.Pagination['current_page'] = 1;
            }
            this.getListItemOfLocation(this.Pagination['current_page']);
        }
    }
    /*
     * reLocation
     * */
    private submitForm:boolean=false;
    private listitemLocation:Array<any>=[];
    getListItemOfLocation(page=null)
    {
        this.showLoadingOverlay=true;
        let idLocation=this.Form.value['from_loc_id'];
        let params="?sort["+this.sortData['fieldname']+"]="+this.sortData['sort']+"&page="+page+"&limit="+this.perPage;
        params+='&transfer=1';

        this.Loading['getList'] = true;
        this.service.getItemLocation(idLocation,params)
            .subscribe(
                data => {
                    this.showLoadingOverlay= false;
                    this.listitemLocation = this._Func.formatData(data.data);
                    //pagin function
                    if(typeof  data['meta']!=='undefined')
                    {
                        this.Pagination=data['meta']['pagination'];
                        this.Pagination['numLinks']=3;
                        this.Pagination['tmpPageCount']=this._Func.Pagination(this.Pagination);
                    }
                },
                err => {
                    this.showLoadingOverlay= false;
                    this.parseError(err);
                },
                () => {
                }
            );
    }

    private showForm=true;
    transferPallet()
    {
        let data=this.Form.value;


        if(this.Form.valid)
        {
            let data={
                "plt_id" : this.Form.value['from_loc_id'],
                "rfid": this.Form.value['to_loc_code']
            };

            let params=JSON.stringify(data);
            this.showLoadingOverlay=true;
            this.service.transferPallet(this.whs_id,params)
                .subscribe(
                    data => {
                        this.submitForm=false;
                        this.showForm = false;
                        setTimeout(() => {
                            this.buildForm();
                            this.showForm = true;
                            this.showLoadingOverlay= false;
                            this.messages = this._Func.Messages('success',this._Func.msg('TFPLT001'));
                        });
                        this.listitemLocation=[];
                        this.Pagination={};
                    },
                    err => {
                        this.parseError(err);
                    },
                    () => {}
                );
        }
    }


    autoSelectMatchItem(ListDataName:Array<any>,controlCode,controlId,key,keycompare){

        for(let item in ListDataName){
            if(this._Func.trim(ListDataName[item][keycompare])==this._Func.trim(key))
            {
                this.selectedItem(controlCode,controlId,ListDataName[item]);
                return;
            }
        }

        this.fbd.setErrorControl(this.Form,controlCode,{'not_existed':true})

    }


    private getWHScodeById(){

        // this.whs_code
        let whs_id=this._Func.getWareHouseId();
        this.service.getWhs_code_by_id(whs_id).subscribe(
            data => {
                this.whs_code=data['whs_code'];
            },
            err=>{
                this.parseError(err);
            }
        );

    }


    valid_pl_whs($event){

        let pallet_code=$event.target.value;

        if($event.target.value){

            let whs_code=pallet_code.substring(0,3);
            let regx=/^[0-9A-Za-z-_]{3}-(PLB|PLG|PLF|PL)-[0-9]{6}$/;
            if(this.whs_code!==whs_code||!regx.test(pallet_code)){
                this.fbd.setErrorControl(this.Form,'to_loc_code',{'invalid_whs_code':true});
            }

        }

    }

    /*=====================================================
     setErrorsControl
     *=====================================================*/
    setErrorsControl(control,err){
        (<Control>this.Form.controls[control]).setErrors(err);
    }
    // Show error when server die or else
    private parseError (err) {
        err = err.json();
        this.showLoadingOverlay= false;
        this.messages = this._Func.Messages('danger', err.errors.message);
    }

    private isInValidateRFID(control:Control):{[key:string]:any} {
        const to_loc_code = control.value;
        const whsId = localStorage.getItem('whs_id');

        if (to_loc_code && to_loc_code.length >= 12 && parseInt(to_loc_code.trim().substring(8, 12)) !== parseInt(whsId)) {
            return {invalidRFID: true};
        }
        return null;
    }

}