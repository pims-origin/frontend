
import {Component, Input,Output, EventEmitter, Injectable, ElementRef} from '@angular/core';
import {Http} from '@angular/http';
import {Functions} from '../../core/load';
import {AgGridNg2} from 'ag-grid-ng2/main';
import {GridOptions} from 'ag-grid/main';
import 'ag-grid-enterprise/main';

declare var jQuery: any;

@Component({
  selector: 'advance-table',
  providers:[],
  directives: [AgGridNg2],
  templateUrl: 'advance-table.html',
})

@Injectable()

export class AdvanceTable_US
{
    private columnDefs:Array<any>=[];
    private table;
    private timeoutUpdateHeader;
    private gridOptions: GridOptions;

    @Input() tableID: string;
    @Input() headerURL: string;
    @Input() headerDef: any[];
    @Input() pinCols: number;
    @Input() rowData: any[];

    @Input('rowData') set resetCheckboxAll(value: Array<any>) {

        this.table && this.table.find('.ag-header-cell :checkbox').prop('checked', false);

        let that = this;

        setTimeout(() => {
            that.setTitleforEllipsisText();
        },1000);
    }

    @Input('getSelectedRow') set returnSelectedRows(value) {
        if (value) {
            this.returnSelectedRow.emit({
                data: this.gridOptions.api.getSelectedRows(),
                action: value
            });
        }
    }

    @Input('refreshRowData') set refreshRowData(value) {

        if(value) {

            let selectedsodes = this.gridOptions.api.getSelectedNodes();

            this.gridOptions.api.refreshRows(selectedsodes);
        }
    }

    @Input('expandTable') set expandTable(value) {
        if(value) {

            var allColumnIds = [];

            this.columnDefs.forEach( function(columnDef) {
                allColumnIds.push(columnDef.field);
            });

            this.gridOptions.columnApi.autoSizeColumns(allColumnIds);
            this.gridOptions.api.sizeColumnsToFit();
        }
    }

    @Output() returnSelectedRow  = new EventEmitter();
    @Output() cellClicked  = new EventEmitter();
    @Output() cellValueChanged  = new EventEmitter();
    @Output() sort = new EventEmitter();
    @Output() modelUpdated = new EventEmitter();

    constructor(
        el: ElementRef,
        private _func: Functions,
        private _http: Http
    )
    {
        this.gridOptions = <GridOptions>{};
    }

    ngOnInit() {}

    ngAfterViewInit() {
      this.table = jQuery('#' + this.tableID);
      this.createColumnDefs();
    }

    private checkCheckAll()
    {
        let table = this.table;

        let checkboxAll = table.find('.ag-header-cell :checkbox');

        try {
            if (this.rowData.length) {

               let rowAmount = this.gridOptions.api.getSelectedRows().length;

                let isChecked = rowAmount == this.rowData.length;

                checkboxAll.prop('checked', isChecked);
            }
        }
        catch(err) {}
    }

    private initToggleCheckAll() {
      var that = this,
          table = this.table;
      table.on('change.toggle', '.ag-header-cell :checkbox', function(){
        var checkboxAll = jQuery(this);
        if(checkboxAll.prop('checked')) {
          that.gridOptions.api.selectAll();
        }
        else {
          that.gridOptions.api.deselectAll();
        }
      });
    }
    private initDragDrop() {
      var that = this,
          table = this.table,
          headerCols = table.find('.ag-header-cell'),
          fixedColEdge = headerCols.filter('.column_fixed').last(),
          fixedIndexEdge = headerCols.index(fixedColEdge),
          lastIndex = headerCols.length - 1,
          drag = false,
          dragElm, dragIdx, leftPos, rightPos, lastMousePos;
      table.off('mousedown', '.ag-header-cell-label').on('mousedown', '.ag-header-cell-label', function(event) {
        event.preventDefault();
        dragElm = jQuery(this);
        dragIdx = table.find('.ag-header-cell-label').index(dragElm);
        if(dragIdx > fixedIndexEdge) {
          drag = true;
          leftPos = dragElm.offset().left;
          rightPos = leftPos + dragElm.width();
          lastMousePos = event.pageX;
        }
      }).off('mousemove').on('mousemove', function(event) {
        event.preventDefault();
        if(drag) {
          var currPos = event.pageX;
          if(currPos > lastMousePos && currPos > rightPos && dragIdx < lastIndex) {
            that.gridOptions.columnApi.moveColumn(dragIdx, dragIdx+1);
            dragIdx++;
            dragElm = table.find('.ag-header-cell-label').eq(dragIdx);
            leftPos = dragElm.offset().left;
            rightPos = leftPos + dragElm.width();
          }
          if(currPos < lastMousePos && currPos < leftPos && dragIdx > fixedIndexEdge + 1) {
            that.gridOptions.columnApi.moveColumn(dragIdx, dragIdx-1);
            dragIdx--;
            dragElm = table.find('.ag-header-cell-label').eq(dragIdx);
            leftPos = dragElm.offset().left;
            rightPos = leftPos + dragElm.width();
          }
          lastMousePos = currPos;
        }
      }).off('mouseup mouseleave').on('mouseup mouseleave', function() {
        drag = false;
      });
    }
    private initSortable() {
      var _this = this,
          allCols = this.table.find('.sorting');
      this.table.on('click.sort', '.sortable .ag-header-cell-label', function() {
        var col = jQuery(this).closest('.sortable'),
            sortType, sortClass, sortField = col.attr('colid');
        if(col.hasClass('sorting')) { sortType = 'asc'; sortClass='sorting-asc'; };
        if(col.hasClass('sorting-asc')) { sortType = 'desc';  sortClass='sorting-desc';};
        if(col.hasClass('sorting-desc')) { sortType = 'none';  sortClass='sorting';};
        allCols.removeClass('sorting sorting-asc sorting-desc').addClass('sorting');
        col.removeClass('sorting sorting-asc sorting-desc').addClass(sortClass);
        _this.sort.emit({sort_type: sortType, sort_field: sortField});
      });
    }
    private createColumnDefs() {
      var tmp_table_header;
      this._http.get(this.headerURL,{ headers: this._func.AuthHeader()})
          .map((res) => res.json())
          .subscribe(
              data => {
                if(data && data.data) {
                  tmp_table_header = JSON.parse(data.data);
                  if(!jQuery.isArray(tmp_table_header) || tmp_table_header[0].id != 'ver_table' || this.headerDef[0].id != 'ver_table' || tmp_table_header[0].value != this.headerDef[0].value) {
                    tmp_table_header = null;
                  }
                }
              },
              err => {
                this.generateHeader(tmp_table_header);
              },
              () => {
                this.generateHeader(tmp_table_header);
              }
          )
    }
    private generateHeader(tmp_table_header) {
      var fullScreen = false;
      if(!tmp_table_header) {
        fullScreen = true;
        tmp_table_header = this.headerDef;
        this._http.put(this.headerURL, JSON.stringify({'value': JSON.stringify(this.headerDef)}), { headers: this._func.AuthHeaderPostJson()})
            .map((res) => res.json().data).subscribe();
      }
      this.columnDefs = [];
      for(let i = 0, l = tmp_table_header.length; i < l; i++) {
        if(tmp_table_header[i].id != 'ver_table') {

          let dataDefs = {
            headerName: tmp_table_header[i].name,
            headerTooltip : tmp_table_header[i].name,
            field: tmp_table_header[i].id,
            width: tmp_table_header[i].width,
            headerClass: i <= this.pinCols + 1 ? 'column_fixed' : '',
            cellClass: i <= this.pinCols + 1 ? 'row_column_fixed' : '',

            suppressMovable: true,
            suppressMenu: false,
            suppressSizeToFit: true,

            template: tmp_table_header[i].hasOwnProperty('template') ?
                tmp_table_header[i].template : '',

            // templateUrl: tmp_table_header[i].hasOwnProperty('templateUrl') ?
            //     tmp_table_header[i].templateUrl : '',

            hide: tmp_table_header[i].hasOwnProperty('hide') ?
                tmp_table_header[i].hide : false,

            editable: tmp_table_header[i].hasOwnProperty('editable') ?
                tmp_table_header[i].editable : false,

            cellRenderer: tmp_table_header[i].hasOwnProperty('cellRenderer') ?
                tmp_table_header[i].cellRenderer : '',

            cellEditorParams: tmp_table_header[i].hasOwnProperty('cellEditorParams') ?
                tmp_table_header[i].cellEditorParams : '',

            cellEditor: tmp_table_header[i].hasOwnProperty('cellEditor') ?
                tmp_table_header[i].cellEditor : '',

            cellClassRules: tmp_table_header[i].hasOwnProperty('cellClassRules') ?
                tmp_table_header[i].cellClassRules : null
          };

          if(tmp_table_header[i].id != 'ck') {
            for(let j = 0, k = this.headerDef.length; j < k; j++) {
              if(this.headerDef[j].id == tmp_table_header[i].id && !this.headerDef[j]['unsortable']) {
                dataDefs['headerClass'] += ' sorting sortable';
              }
            }
          }
          if(tmp_table_header[i].id == 'ck') {
            dataDefs['headerName'] = '';
            dataDefs['sortingOrder'] = [null];
            dataDefs['checkboxSelection'] = true;
            dataDefs['headerCellTemplate']= function(){
              var eCell = document.createElement('div');
              eCell.innerHTML = '<div id="agHeaderCellLabel" class="ag-header-cell-label"><input type="checkbox"></div>';
              return eCell;
            };
            dataDefs['suppressSizeToFit'] = true;
          }
          this.columnDefs.push(dataDefs)
        }
      }
      setTimeout(() => {
        if(fullScreen) {
          this.gridOptions.api.sizeColumnsToFit();
        }
        this.initDragDrop();
        this.initToggleCheckAll();
        this.initSortable();
      }, 400)
    }

   //***************************************************************************

    private updateHeader()
    {
        let colApi = this.gridOptions.columnApi;

        var table_state = colApi.getColumnState();

        if (this.timeoutUpdateHeader) {
            clearTimeout(this.timeoutUpdateHeader);
        }

        this.timeoutUpdateHeader = setTimeout(() => {

            let tmp = [];

            tmp.push({
                id: this.headerDef[0].id,
                value: this.headerDef[0].value
            });

            for (let count=0; count < table_state.length; count++){

                let colId = table_state[count].colId;

                tmp.push({
                    id: colId,
                    width: table_state[count].width,
                    name: count ? colApi.getColumn(colId)['colDef'].headerName : ''
                })
            }

            let param = JSON.stringify({
                value: JSON.stringify(tmp)
            });

            this._http.put(this.headerURL, param, {
                    headers: this._func.AuthHeaderPostJson()
                })
                .map((res) => res.json().data).subscribe(
                    data => {
                    },
                    err => {
                    }
            );
        }, 400);
    }

   //***************************************************************************

    private onColumnMoved($event)
    {
        this.updateHeader();

        if (this.rowData.length) {
            this.checkCheckAll();
        } else {
            this.table.find('.ag-header-cell :checkbox').prop('checked', false);
        }
    }

   //***************************************************************************

    private onColumnResized()
    {
        this.updateHeader();
        this.checkCheckAll();
        this.setTitleforEllipsisText();
    }

   //***************************************************************************

    private onSelectionChanged()
    {
        this.checkCheckAll();
    }

   //***************************************************************************

    private onCellClicked($event)
    {
        this.cellClicked.emit($event);
    }

   //***************************************************************************

    private onCellValueChanged($event)
    {
        this.cellValueChanged.emit($event);
    }

   //***************************************************************************

    private onModelUpdated($event)
    {
        this.modelUpdated.emit($event);
    }

   //***************************************************************************

    private setTitleforEllipsisText()
    {
        let table = this.table;

        table.find('.ag-cell').each(function() {

            let spanChild = jQuery(this).find('span').children(),
                anchorChild = jQuery(this).find('a').children();

            if (! spanChild && anchorChild) {

                let tilte = anchorChild.attr('title') || anchorChild.text();

                anchorChild.attr('title', tilte);
            } else {
//                jQuery(this).wrapInner("<span title='"+jQuery(this).text()+"'></span>")
            }
        });
    }

   //***************************************************************************

}