import {Component} from '@angular/core';
import { FORM_DIRECTIVES,ControlGroup,FormBuilder, Control,Validators} from '@angular/common';
import {WMSBreadcrumb,WMSMessages,WMSPagination} from '../../common/directives/directives';
import {Router, RouteData, RouteParams } from '@angular/router-deprecated';
import {BoxPopupService} from "../../common/popup/box-popup.service";
import {GoBackComponent} from "../../common/component/goBack.component";
import {WarningBoxPopup} from "../../common/popup/warning-box-popup";
import { ValidationService } from '../../common/core/validator';
import {UserService, Functions} from '../../common/core/load';
import {API_Config} from "../../common/core/API_Config";
import {Services} from "./services";
import {DeliveryServiceForm} from "./delivery-form";
declare var saveAs: any;
declare var jQuery: any;
@Component ({
    selector: 'location-reason',
    directives:[WMSBreadcrumb,WMSPagination,DeliveryServiceForm,GoBackComponent,FORM_DIRECTIVES,WMSMessages],
    providers: [BoxPopupService,ValidationService,Services,UserService],
    templateUrl: 'template.html'
})

export class DeliveryService {

    Form: ControlGroup;
    private showLoadingOverlay=false;
    private TitlePage="Create Delivery Service";
    private submitForm=false;
    private loading={};
    private dataListDelivery=[];
    private messages;
    private Pagination={};
    private perPage=20;
    private currentPage = 1;
    private numLinks = 3;

    constructor(private _router: Router,
                private _boxPopupService:BoxPopupService,
                _RTAction: RouteData,
                private ser:Services,
                private _user:UserService,
                private fb: FormBuilder,
                private _Valid: ValidationService,
                private _Func: Functions,
                private _api_Config: API_Config,
                private params: RouteParams) {

        this.checkPermission();

    }

    private canCreate;
    private canView:any;
    private canEdit:any;
    private showForm=true;
    private checkPermission(){
        this.showLoadingOverlay = true;
        this._user.GetPermissionUser().subscribe(
            data => {

                this.canCreate = this._user.RequestPermission(data, 'createDeliveryService');
                this.canView = this._user.RequestPermission(data, 'viewDeliveryService');

                if(!this.canView){
                    this._router.parent.navigateByUrl('/deny');
                }

                if(!this.canCreate){
                    this.TitlePage='Delivery Service List';
                }

                this.getDeliveryService();
            },
            err => {
                this.parseError(err);
                this.showLoadingOverlay = false;
            }
        );
    }



    // Get Asn by ID
    private getDeliveryService(page = 1) {

        this.showLoadingOverlay = true;
       let param= "?page=" + page + "&limit=" + this.perPage;

        this.ser.getDeliveryService(param)
            .subscribe(
                data => {
                    this.showLoadingOverlay = false;
                    this.dataListDelivery = data.data;
                    if(data['meta']){
                        this.Pagination = data['meta']['pagination'];
                        this.Pagination['numLinks'] = 3;
                        this.Pagination['tmpPageCount'] = this._Func.Pagination(this.Pagination);
                    }
                },
                err => {
                    this.showLoadingOverlay = false;
                    this.parseError(err);
                },
                () => {}
            );
    }


    // Event when user filter form
    private onPageSizeChanged ($event,el) {

        this.perPage = $event.target.value;

        if((this.Pagination['current_page'] - 1) * this.perPage >= this.Pagination['total']) {
            this.currentPage = 1;
        }
        else {
            this.currentPage = this.Pagination['current_page'];
        }
        this.getDeliveryService(this.currentPage);
    }

    // Show error when server die or else
    private parseError(err) {
        this.messages = {'status': 'danger', 'txt': this._Func.parseErrorMessageFromServer(err)};
    }



}