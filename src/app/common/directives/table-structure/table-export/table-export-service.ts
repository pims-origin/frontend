
import {Injectable} from '@angular/core';
import {Functions} from '../../../core/load';
declare var jQuery: any;
declare var jsPDF: any;
declare var saveAs: any;

@Injectable()

export class TableExportService {

    private messagesText;

    constructor(private _Func: Functions)
    {

    }

    //**************************************************************************

    public exportPdf(tableComponent: any)
    {
        this.checkHeader(tableComponent);

        let dataExport = tableComponent.get('dataExport'),
            headerExport = tableComponent.get('headerExport'),
            downloadName = this.getDownloadName(tableComponent),
            //pdf = new jsPDF('p', 'pt'),
            url = tableComponent.get('printURL');

        var that = tableComponent;
        let api = url + '&print=pdf';

        try{
            let xhr = new XMLHttpRequest();
            xhr.open("GET", api , true);
            xhr.setRequestHeader("Authorization", 'Bearer ' + this._Func.getToken());
            xhr.responseType = 'blob';

            xhr.onreadystatechange = () => {
                if(xhr.readyState == 2) {
                    if(xhr.status == 200) {
                        xhr.responseType = "blob";
                    } else {
                        xhr.responseType = "text";
                    }
                }

                if(xhr.readyState === 4) {
                    if(xhr.status === 200) {
                        var fileName = downloadName;
                        var blob = new Blob([xhr.response], {type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'});
                        saveAs.saveAs(blob, fileName + '.pdf');
                    }else{
                        let errMsg = '';
                        if(xhr.response) {
                            try {
                                var res = JSON.parse(xhr.response);
                                errMsg = res.errors && res.errors.message ? res.errors.message : xhr.statusText;
                            }
                            catch(err){errMsg = xhr.statusText}
                        }
                        else {
                            errMsg = xhr.statusText;
                        }
                        if(!errMsg) {
                            errMsg = this._Func.msg('VR100');
                        }
                        this.messagesText = this._Func.Messages('danger', errMsg);
                    }
                }
            }
            xhr.send();
        }catch (err){
            this.messagesText = this._Func.Messages('danger', this._Func.msg('VR100'));
        }

        // pdf.autoTable(headerExport, dataExport, {
        //     theme: 'grid'
        // });
        //
        // pdf.save(downloadName + '.pdf');
    }

    //**************************************************************************

    public exportCsv(tableComponent: any)
    {
        this.checkHeader(tableComponent);

        let dataExport = tableComponent.get('dataExport'),
            headerExport = tableComponent.get('headerExport'),
            headerDef = tableComponent.get('headerDef'),
            downloadName = this.getDownloadName(tableComponent);
        
        headerDef.shift();

        let finalVal = headerExport.join(',') + '\n';

        for (let row=0; row<dataExport.length; row++) {

            var value = dataExport[row];

            let valueLength = Object.keys(value).length;
            for (var column=0; column<valueLength; column++) {

                if (this.outputColumn(headerDef[column])) {

                    let field = headerDef[column].id;

                    let result = typeof value[column] === 'undefined'
                              || value[column] === null ?
                        '' : value[column].toString().replace(/"/g, '""');

                    finalVal += column ? ',' : '';
                   
                    finalVal += result.search(/("|,|\n)/g) >= 0 ?
                        '"' + result + '"' : result;
                }
            }

            finalVal += '\n';
        }

        if (navigator.msSaveBlob) { // IE 10+

            let blob = new Blob([finalVal], {
                type: 'text/csv;charset=utf-8;'
            });

            navigator.msSaveBlob(blob, finalVal);
        } else {

            let encoded = encodeURIComponent(finalVal);

            let download = jQuery('<a>')
                .attr('href', 'data:text/csv;charset=utf-8,' + encoded)
                .attr('download', downloadName + '.csv');

            download[0].click();

            download.remove();
        }
    }

    //**************************************************************************

    private checkHeader(tableComponent: any)
    {
        if (! tableComponent.get('headerExport').length) {

            tableComponent.set('headerExport', []);

            let headerDef = tableComponent.get('headerDef');

            for (let prop in headerDef) {

                let colInfo = headerDef[prop];

                if (this.outputColumn(colInfo)) {
                    if (colInfo.name) {
                        tableComponent.get('headerExport').push(colInfo.name);
                    }
                }
            }
        }
    }

    //**************************************************************************

    private outputColumn(colInfo)
    {
        return colInfo.hasOwnProperty('name') &&
            ! (colInfo.hasOwnProperty('hide') && colInfo.hide) &&
            ! (colInfo.hasOwnProperty('skipExport') && colInfo.skipExport);
    }

    //**************************************************************************

    private getDownloadName(tableComponent: any)
    {
        return tableComponent.hasOwnProperty('downloadName') ?
            tableComponent.get('downloadName') : tableComponent.get('title');
    }

    //**************************************************************************

}