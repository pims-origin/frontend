import { Component,Input } from '@angular/core';
import { CycleCountServices } from '../cycle-count-service';
import { Router } from '@angular/router-deprecated';
import { API_Config, UserService, Functions } from '../../common/core/load';
import { FORM_DIRECTIVES,ControlGroup,FormBuilder, Control } from '@angular/common';
import { Http } from '@angular/http';
import { WMSBreadcrumb,PDFPrinter, PopupLocations, WMSPagination } from '../../common/directives/directives';
import {BoxPopupService} from "../../common/popup/box-popup.service";
import {WarningBoxPopup} from "../../common/popup/warning-box-popup";
import {CRUUserService} from "../../user-management/cru-user/cru-user-service";
import { DataShareService } from '../../common/services/data-share.service';
import { setTimeout } from 'timers';
declare var jQuery: any;

@Component ({
  selector: 'cycle-count-list',
  providers: [CycleCountServices, FormBuilder, BoxPopupService],
  directives: [WMSBreadcrumb, PDFPrinter, PopupLocations, WMSPagination],
  templateUrl: 'cycle-count-list.component.html',
})

export class CycleCountListComponent {
  private whs_id;
  private cycleCountList: Array<any> = [];
  private selectedAll = false;
  private cycleCountStatus: any = [];
  private cycleCountTypes: any = [];
  private userList: any = [];
  private Pagination;
  private totalCount = 0;
  private tmpPageCount;
  private pageCount=0;
  private ItemOnPage=0;
  private currentPage = 1;
  private perPage = 20;
  private numLinks = 3;
  private messages;
  searchForm: ControlGroup;
  public loading = [];
  public showLoadingOverlay = false;
  private hasEditPermission = false;
  private hasCreatePermission = false;
  private hasUnsetRfidOnLocationPermission = false;
  private hasViewCC = false;
  private hasDeleteCC = false;
  private eventCkEdit = true;
  private eventCkDelete = true;
  private eventCkReview = true;
  private currentUserID = {};
  private sortQuery = '';
  private searchParams = '';
  public cycleCountUserList: any = [];
  private page_current = 1;
  private changeAssinged:any ;
  private countClick = {
    cycle_hdr_id: 3,
    cycle_name: 3,
    cycle_num: 3,
    due_dt: 3,
    cycle_type: 3,
    created_by: 3,
    assignee_id: 3,
    cycle_sts: 3,
    cc_type: 3,
    completed_at: 3,
    created_at: 3
  };
  private FieldsToSortFlag = {
    cycle_hdr_id: false,
    cycle_name: false,
    cycle_num: false,
    due_dt: false,
    cycle_type: false,
    created_by: false,
    assignee_id: false,
    cycle_sts: false,
    cc_type: false,
    completed_at: false,
    created_at: false

  };
  private searchCriteria = {};
  private hasChangeAssignee = false;
  public listSelectedItemArr = [];
  private locations:Array<any>=[];
  private listNull = {};

  // print PDF
  private printerAPI:string = '';
  private fileName:string='';
  private listDataAutocomplete = {};
  private queryParams;

  constructor(private _http: Http,
              private _Func: Functions,
              private _user: UserService,
              private fb: FormBuilder,
              private _cycleCountService: CycleCountServices,
              private _boxPopupService:BoxPopupService,
              private _router: Router,
              private dataShareService: DataShareService
              )
  {
    this.checkPermission();
    this.whs_id = localStorage.getItem('whs_id');
    this.currentUserID = JSON.parse(localStorage.getItem('logedUser'))['user_id'];
    this.builderSearchForm();
    this.getCycleCountStatus();
    this.getCycleCountType();
    this.getCycleCountUsers();
  }

  /*
   *********************************************************************************************************************
   */

  private checkPermission() {
    this.showLoadingOverlay = true;
    this._user.GetPermissionUser().subscribe(
      data => {

        this.hasEditPermission = this._user.RequestPermission(data, 'editCycleCount');
        this.hasCreatePermission = this._user.RequestPermission(data, 'createCycleCount');
        this.hasViewCC = this._user.RequestPermission(data,'viewCycleCount');
        this.hasDeleteCC = this._user.RequestPermission(data,'deleteCycleCount');
        this.hasChangeAssignee = this._user.RequestPermission(data,'changeAssignee');
        this.hasUnsetRfidOnLocationPermission = this._user.RequestPermission(data,'unsetRfidOnLocation');

        if (!this.hasViewCC) {

          this._router.parent.navigateByUrl('/deny');
        } else {
          this._getQueryParams();
          this.getCycleCountList(1);
        }
      },
      err => {
        this.showMessage('danger', this._Func.parseErrorMessageFromServer(err));
        this.showLoadingOverlay = false;
      }
    );
  }

  /*
   *********************************************************************************************************************
   */

  private builderSearchForm() {
    this.searchForm = this.fb.group({
      'cycle_name': [''],
      'due_dt': [''],
      'cycle_type': [''],
      'created_by': [''],
      'assignee_id': [''],
      'cycle_sts': [''],
      'sku':[''],
      'size':[''],
      'color':[''],
      'lot':['']
    });
  }

  /*
   *********************************************************************************************************************
   */

  private getCycleCountList(page=null) {

    this.page_current = page;
    let param = "?page=" + page + "&limit=" + this.perPage + '&whs_id=' + this.whs_id + this.searchParams;

    if(this.sortQuery != '') {
      param += '&'+this.sortQuery;
    }

    if(Object.keys(this.searchCriteria).length !== 0)
    {
      param += '&cycle_name=' + this.searchCriteria['cycle_name'] +
        '&due_dt=' + this.searchCriteria['due_dt'] +
        '&cycle_type=' + this.searchCriteria['cycle_type'] +
        '&created_by=' + this.searchCriteria['created_by'] +
        '&assignee_id=' + this.searchCriteria['assignee_id'] +
        '&cycle_sts=' + this.searchCriteria['cycle_sts'];
    }
    
    this.loading['getCycleCount'] = true;
    this.showLoadingOverlay = true;
    this._cycleCountService.getCycleCount(param).subscribe(
      data =>{
        this.selectedAll=false;
        this.cycleCountList=data.data;
        this.printerAPI = this._cycleCountService.getPrinterAPI(this.whs_id);
        this.fileName="Tally";

        this.Pagination=data['meta']['pagination'];
        this.Pagination['numLinks']=3;
        this.Pagination['tmpPageCount']=this._Func.Pagination(this.Pagination);

        this.loading['getCycleCount']=false;
        this.showLoadingOverlay = false;
      },
      err => {}
    );
  }

  /*
   *********************************************************************************************************************
   */

  private showMessage(msgStatus, msg) {
    this.messages = {
      'status': msgStatus,
      'txt': msg
    };
    jQuery(window).scrollTop(0);
  }

  /*
   *********************************************************************************************************************
   */

  private cycleCountUserListCA = [];

  /*
   *********************************************************************************************************************
   */

  private setItemListSelected(id, value) {
    this.cycleCountList.forEach((item) => {
      if(item['cycle_hdr_id'] == id) {
        item['selected'] = value;
      }
    });
  }

  /*
   *********************************************************************************************************************
   */

  private checkAll(event) {
    this.selectedAll = !this.selectedAll;
    // set empty array  ListSelectedItem
    this.listSelectedItemArr = [];
    // Loop
    this.cycleCountList.forEach((item) => {
      item['selected'] = this.selectedAll;
      if(this.selectedAll) {
        // if check all = true , pushing to ListSelectedItem
        this.listSelectedItemArr.push(item);
      }
    })
    this.checkEventForPermission();
  }

  /*
   *********************************************************************************************************************
   */

  private editSelectedItem() {
    if (this.listSelectedItemArr.length > 1) {
      this.messages = this._Func.Messages('danger',this._Func.msg('CC007'));
      return false;
    }
      
    if(this.listSelectedItemArr.length === 1) {

      const arValidStatus = ['AS', 'RC'];
      const status = this.listSelectedItemArr[0]['cycle_sts'];
      const id = this.listSelectedItemArr[0]['cycle_hdr_id'];

      if (arValidStatus.indexOf(status) !== -1) {
        this._router.navigateByUrl('/cycle-count/' + id + '/edit');
      } else {
        this.messages = this._Func.Messages('danger',this._Func.msg('CC008'));
      }

    } else {
      this.messages = this._Func.Messages('danger',this._Func.msg('CC009'));
    }
    
  }

  /*
   *********************************************************************************************************************
   */

  private reviewSelectedItem() {

    if (this.listSelectedItemArr.length > 1) {
      this.messages = this._Func.Messages('danger',this._Func.msg('CC002'));
      return false;
    }

    if(this.listSelectedItemArr.length === 1) {
      
      const arValidStatus = ['CC', 'RC', 'CP'];
      const status = this.listSelectedItemArr[0]['cycle_sts'];
      const id = this.listSelectedItemArr[0]['cycle_hdr_id'];

      if (arValidStatus.indexOf(status) !== -1) {
        this._router.navigateByUrl('/cycle-count/' + id + '/review');
      } else {
        this.messages = this._Func.Messages('danger',this._Func.msg('CC008'));
      }

    } else {
      this.messages = this._Func.Messages('danger',this._Func.msg('CC003'));
    }
  }

  /*
   *********************************************************************************************************************
   */

  private deleteSelectedItem() {

    if (this.listSelectedItemArr.length < 1) {

      this.messages = this._Func.Messages('danger',this._Func.msg('CC006'));
      return false;
    }

    for (var i = 0; i < this.listSelectedItemArr.length; i++) {

      if (this.listSelectedItemArr[i]['cycle_sts'] != 'AS') {

        this.messages = this._Func.Messages('danger',this._Func.msg('CC008'));
        return false;
      }
    }

    this.messages = false;
    var target = this;
    let warningPopup = new WarningBoxPopup();
    warningPopup.text = "Do you want delete action ?";

    this._boxPopupService.showWarningPopup(warningPopup)
        .then(function (dm) {

          if(dm) {

            target.actionDeleteSelectedItem();
          }
        })
  }

  /*
   *********************************************************************************************************************
   */

  private actionDeleteSelectedItem() {

    try {

      if(this.listSelectedItemArr.length) {
        const selectedItems = this.listSelectedItemArr.map(item => item['cycle_hdr_id']);
        const params = JSON.stringify({"cycle_hdr_ids": selectedItems});
        
        this._cycleCountService.deleteCycleCount(params).subscribe(
            data =>{

            },
            err => {

              this.showMessage('danger', this._Func.parseErrorMessageFromServer(err));

            },
            () => {

              this.listSelectedItemArr = [];
              this.getCycleCountList(this.page_current);
            }
        );

      }
      else {

        this.messages = this._Func.Messages('danger',this._Func.msg('CC005'));
      }

    } catch (err) {

    }

  }

  /*
   *********************************************************************************************************************
   */

  // Delete item in cycle lisst
  private deleteCCList(arr_hdr_id = []) {

    arr_hdr_id.forEach((item, index) => {

      this.cycleCountList.forEach((item2, index2) => {

        if(item2['cycle_hdr_id'] == item) {

          this.cycleCountList.splice(index2, 1);

        }
      });
     });

  }

  /*
   *********************************************************************************************************************
   */

  private onPageSizeChanged($event) {
    this.perPage = $event.target.value;
    if((this.Pagination['current_page'] - 1) * this.perPage >= this.Pagination['total']) {
      this.currentPage = 1;
    }
    else {
      this.currentPage = this.Pagination['current_page'];
    }
    this.getCycleCountList(this.currentPage);
  }

  /*
   *********************************************************************************************************************
   */

  private filterList(num) {
    this.getCycleCountList(num);
  }

  /*
   *********************************************************************************************************************
   */

  private getPage(num) {
    let arr=new Array(num);
    return arr;
  }

  /*
   *********************************************************************************************************************
   */

  private getCycleCountStatus () {

     this._cycleCountService.getCycleCountStatus()
       .subscribe(
         data => {

           this.cycleCountStatus = data;

         },
         err => {}
       );
  }

  /*
   *********************************************************************************************************************
   */

  private getCycleCountType () {

    this._cycleCountService.getCycleCountType()
      .subscribe(
        data => {
          this.cycleCountTypes = data;
        },
        err => {}
      );
  }

  /*
   *********************************************************************************************************************
   */

  private reset() {

    jQuery("#form-filter input, #form-filter select").each(function( index ) {
      jQuery(this).val("");
    });
    (<Control>this.searchForm.controls["cycle_name"]).updateValue("");
    (<Control>this.searchForm.controls["due_dt"]).updateValue("");
    (<Control>this.searchForm.controls["cycle_type"]).updateValue("");
    (<Control>this.searchForm.controls["created_by"]).updateValue("");
    (<Control>this.searchForm.controls["assignee_id"]).updateValue("");
    (<Control>this.searchForm.controls["cycle_sts"]).updateValue("");
    this._resetQueryParams();
    this.searchCriteria = this._Func.genSearchCriteriaFromForm(this.searchForm);

    this.searchParams = '';
    this.getCycleCountList(this.Pagination['current_page']);

  }

  /*
   *********************************************************************************************************************
   */

  private sort(field) {

    this.listSelectedItemArr = [];
    if(this.countClick[field] == 3) {
      this.countClick[field] = 0;
    }

    this.countClick[field] ++;
    if(this.countClick[field] == 3) {
      this.sortQuery = 'sort[cycle_name]=asc';
    }
    else {
      this.FieldsToSortFlag[field] = !this.FieldsToSortFlag[field];

      this.sortQuery = 'sort['+field+']';
      if(this.FieldsToSortFlag[field]) {
        this.sortQuery += '=asc';
      } else {
        this.sortQuery += '=desc';
      }
    }

    if(field) {
      for (let sortField in this.FieldsToSortFlag ) {
        if(sortField != field) {
          this.FieldsToSortFlag[sortField] = false;
          this.countClick[sortField] = 3;
        }
      }
    }
    this.getCycleCountList(this.currentPage);
  }

  /*
   *********************************************************************************************************************
   */

  search(data) {
    let date: Array<string>;
    this.messages = null;
    this.loading['searching']=true;
    this.showLoadingOverlay = true;

    let param="?whs_id=" + this.whs_id + "&page=" + 1 + "&limit=" + this.perPage;
    this.searchCriteria = this._Func.genSearchCriteriaFromForm(this.searchForm);

    date = jQuery('input#due-date').val().split("/");
    if (date.length > 1) {
      this.searchCriteria['due_dt'] = (date[2] + '-' + date[0] + '-' + date[1]).toString();
    }

    if (! this.hasCreatePermission) {
      param += '&assignee_id=' + this.currentUserID;
    }

    let encodeSearchQuery = param+"&cycle_name="+encodeURIComponent(this.searchCriteria['cycle_name'])+
      "&due_dt="+encodeURIComponent(this.searchCriteria['due_dt'])+
      "&cycle_type="+encodeURIComponent(this.searchCriteria['cycle_type'])+
      "&created_by="+encodeURIComponent(this.searchCriteria['created_by'])+
      "&assignee_id="+encodeURIComponent(this.searchCriteria['assignee_id'])+
      "&cycle_sts="+encodeURIComponent(this.searchCriteria['cycle_sts'])+
      "&sku="+encodeURIComponent(this.searchCriteria['sku'])+
      "&size="+encodeURIComponent(this.searchCriteria['size'])+
      "&color="+encodeURIComponent(this.searchCriteria['color'])+
      "&lot="+encodeURIComponent(this.searchCriteria['lot']);

    if(this.sortQuery != '') {
      param += '&'+this.sortQuery;
    }
    this._cycleCountService.search(encodeSearchQuery).subscribe(
      data => {
        // disable loading icon
        this.loading['searching']=false;
        this.showLoadingOverlay = false;
        this.selectedAll = false;

        this.cycleCountList = data.data;
        //pagin function
        this.Pagination=data['meta']['pagination'];
        this.Pagination['numLinks']=3;
        this.Pagination['tmpPageCount']=this._Func.Pagination(this.Pagination);
      },
      err =>{
        this.messages = this._Func.Messages('danger','Not found');
      }
    );
  }

  /*
   ********************************************************************************************************************
   */


  private getCycleCountUsers() {

    this._cycleCountService.getCycleCountUsers()
      .subscribe(
        data => {
          this.cycleCountUserList = data.data;
        },
        err => {
          this.showMessage('danger', this._Func.parseErrorMessageFromServer(err));
        },
        () => {
        }
      );
  }
  private changeAssignee() {

    this.changeAssinged = null;
    this.messages = null;
    if (this.listSelectedItemArr.length > 1) {

      this.messages = this._Func.Messages('danger',this._Func.msg('CC0012'));
      return false;
    } else if (this.listSelectedItemArr.length < 1) {

      this.messages = this._Func.Messages('danger',this._Func.msg('CC0012'));
      return false;
    }

    if (this.listSelectedItemArr[0]['cycle_sts'] === 'CP') {
      this.messages = this._Func.Messages('danger',this._Func.msg('CC0014'));
      return false;
    }

    this.cycleCountUserListCA = [];
    for(var i = 0; i < this.cycleCountUserList.length; i++) {

      if (this.cycleCountUserList[i]['user_id'] == this.listSelectedItemArr[0]['created_by']) {

        continue;
      }

      if (this.cycleCountUserList[i]['user_id'] == this.listSelectedItemArr[0]['assignee_id']) {

        continue;
      }

      this.cycleCountUserListCA.push(this.cycleCountUserList[i]);
    }
    
    jQuery('#change-assignee').modal('show');
  }
  private parseError(err) {

    this.messages = {'status': 'danger', 'txt': this._Func.parseErrorMessageFromServer(err)};
  }

  selectChangeAssignee(item) {
    for(var i = 0, l = this.cycleCountUserList.length; i < l; i++) {
      this.cycleCountUserList[i].selected = false;
    }
    item.selected = true;
    this.changeAssinged = item;

  }

  saveChangeAssignee() {

    this._cycleCountService.saveChangeAssignee(this.listSelectedItemArr[0]['cycle_hdr_id'], this.changeAssinged['user_id'])
        .subscribe(
            data => {

              this.messages = this._Func.Messages('success',this._Func.msg('CC0013'));
              this.getCycleCountList(this.page_current);
              this.listSelectedItemArr = [];
              this.changeAssinged = null;

              for(var i = 0, l = this.cycleCountUserList.length; i < l; i++) {
                this.cycleCountUserList[i].selected = false;
              }
            },
            err => {
              this.showMessage('danger', this._Func.parseErrorMessageFromServer(err));
            },
            () => {
              jQuery('#change-assignee').modal('hide');

            }
        );
  }

  updateModalDate(value) {
    (<Control>this.searchForm.controls["due_dt"]).updateValue(value);
  }

  //  by Nhan
  private bindMessagePDFPrinter($event){
    this.messages = $event;
  }

  private setClickedRow(item) {
    item.selected = !item.selected;
    this.selectedAll = false;
    if (item.selected) {
      this._selectedItem(item['cycle_hdr_id'], item);
    } else {
      this._unselectedItem(item['cycle_hdr_id'], item);
    }
    this.checkEventForPermission();
  }

  private _selectedItem(id, item) {
    this.listSelectedItemArr.push(item);
    this.setItemListSelected(id, true);

    if (this.listSelectedItemArr.length == this.cycleCountList.length)
    {
      this.selectedAll = true;
    }
  }

  private _unselectedItem(id, item) {
    // remove item out of array
    this.listSelectedItemArr = this.listSelectedItemArr.filter(item => {
      return item['cycle_hdr_id'] != id;
    })
    this.setItemListSelected(id, false);
  }

  private checkEventForPermission() {
    var target = this;
    this.eventCkEdit = this.eventCkDelete = this.eventCkReview = true;

    this.cycleCountList.forEach((item) => {
      if(item['selected']) {

        var status = item['cycle_sts'];
        var assignTo = item['assignee_id'];
        var createBy = item['created_by'];
  
        // For page Update
        if (!((status == 'AS' || status == 'RC') && target.currentUserID == assignTo)) {
          target.eventCkEdit = false;
        }
  
        // For page delete
        if (!(status == 'AS' && target.currentUserID == createBy)) {
          target.eventCkDelete = false;
        }
  
        // For page review
        if (!((status == 'CC' || status == 'RC') && target.currentUserID == createBy)) {
          target.eventCkReview = false;
        }
      }
    });
  }

  unsetRfidOnLocation() {
    if (this.locations.length) {
      const info = {
        data: this.locations
      }
      this.showLoadingOverlay = true;
      this._cycleCountService.unsetRfidOnLocation(JSON.stringify(info)).subscribe(
        data => {
          this.showLoadingOverlay = false;
          this.locations = [];
          this.messages = this._Func.Messages('success',this._Func.msg('CC013'));
        },
        err => {
          this.showLoadingOverlay = false;
          this.showMessage('danger', this._Func.parseErrorMessageFromServer(err));
        });
    } else {
      this.messages = this._Func.Messages('danger',this._Func.msg('CC012'));
    }
  }

  private addLocation($event: any[] = []){

    if (this.locations.length === 0) {
      this.locations = $event;
    } else {
      this.locations.push(...this.filteredDuplicatedItem(this.locations, $event));
    }

    if (this.locations.length) {
      this.listNull['locations'] = false;
    }

  }

  private filteredDuplicatedItem(ArrayDes:Array<any>=[],array_push:Array<any>=[]){

    let arr_temp:Array<any> = [];
    for (let new_item of array_push){
      let is_existed = 0;
      for (let old_item of ArrayDes){
        if(new_item['loc_id'] == old_item['loc_id'] ){
          is_existed++;
        }
      }
      if(is_existed==0){
        arr_temp.push(new_item);
      }
    }
    return arr_temp;

  }

  private autoCompleteSku($nameField, $e) {
    this.listDataAutocomplete[$nameField] = null;
    (<Control>this.searchForm.controls['size']).updateValue('');
    (<Control>this.searchForm.controls['color']).updateValue('');
    (<Control>this.searchForm.controls['lot']).updateValue('');
    
    if ($e.target.value) {
      this.listDataAutocomplete['loading-' + $nameField] = true;

      // do Search
      var param = `?sku=` +this._Func.trim($e.target.value)+ `&limit=20`;

      this._cycleCountService.getSKUsSearch(this.whs_id, param).debounceTime(400).distinctUntilChanged()
          .subscribe(
              data => {

                this.listDataAutocomplete[$nameField] = $e.target.value ? data.data : null;
                this.listDataAutocomplete['loading-' + $nameField] = false;
              },
              err => {

                this.listDataAutocomplete['loading-' + $nameField] = false;
                this.parseError(err);
              },
              () => {
              }
          );
    }
  }

  private selectedItem($item){
      this.listDataAutocomplete['sku'] = null;
      (<Control>this.searchForm.controls['size']).updateValue($item.size);
      (<Control>this.searchForm.controls['color']).updateValue($item.color);
      (<Control>this.searchForm.controls['lot']).updateValue($item.lot);
      (<Control>this.searchForm.controls['sku']).updateValue($item.sku);
  }

  _getQueryParams() {
      const queryString = this.dataShareService.data;
      this.queryParams = this._Func.parseQueryString(queryString);

      if(queryString.length) {
          if(this.searchParams) {
              this.searchParams += "&" + queryString;
          } else {
              this.searchParams = "&" + queryString;
          }
      }
    }

  ngOnDestroy() {
    this._resetQueryParams();
  }

  private _resetQueryParams() {
      this.searchParams = '';
      this.dataShareService.data = '';
  }
}
