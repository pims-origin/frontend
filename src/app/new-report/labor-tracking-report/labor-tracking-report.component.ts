import { Component } from '@angular/core';
import { Router, ROUTER_DIRECTIVES } from '@angular/router-deprecated';
import { ControlGroup, FormBuilder, Control } from '@angular/common';
import { API_Config, UserService, Functions } from '../../common/core/load';
import { BoxPopupService } from '../../common/popup/box-popup.service';
import { LaborTrackingReportService } from './labor-tracking-report.service';
import { WMSPagination, AgGridExtent, WMSMessages, ReportFileExporter } from '../../common/directives/directives';
import 'ag-grid-enterprise/main';

declare const $: any;

@Component({
  selector: 'labor-tracking-report',
  providers: [LaborTrackingReportService, BoxPopupService],
  directives: [ROUTER_DIRECTIVES, AgGridExtent, WMSMessages, WMSPagination, ReportFileExporter],
  templateUrl: 'labor-tracking-report.component.html'
})
export class LaborTrackingReportComponent {
  pagination: any;
  perPage: number = 20;
  whs_id: string;
  messages: any;
  searchForm: ControlGroup;
  sortData = { fieldname: 'updated_at', sort: 'desc' };
  showLoadingOverlay: boolean = false;
  grReports: Array<any> = [];
  headerURL: string = this.apiService.API_User_Metas + '/ltr';
  columnData: any = {
    fullname: {
      title: 'User',
      width: 190,
      pin: true,
      sort: true
    },
    customer: {
      title: 'Customer',
      width: 190,
      pin: true,
      sort: true
    },
    owner: {
      title: 'Owner',
      width: 190,
      pin: true,
      sort: true
    },
    trans_num: {
      title: 'Trans Number',
      width: 200,
      pin: true,
      sort: true
    },
    start_time: {
      title: 'Start Time',
      width: 170,
      pin: true,
      sort: true
    },
    end_time: {
      title: 'End Time',
      width: 170,
      pin: true,
      sort: true
    },
    total_hours: {
      title: 'Total Hours',
      width: 100,
      pin: true,
      sort: true
    },
    type: {
      title: 'Type',
      width: 130,
      pin: true,
      sort: true
    }
  };
  customers: any[] = [];
  private listType = [];
  public isLoading: any[] = [];

  // Check permission for user using this function page
  canAccess: boolean = false;

  // Export csv file
  exportAPIUrl: string = '';
  fileName: string = 'Labor-Tracking-Report.csv';

  constructor(
    private formBuilder: FormBuilder,
    private service: LaborTrackingReportService,
    private func: Functions,
    private userService: UserService,
    private apiService: API_Config,
    private router: Router
  ) {
    this.buildSearchForm();
    this.whs_id = localStorage.getItem('whs_id');
    this.checkPermission();
  }

  buildSearchForm() {
    this.searchForm = this.formBuilder.group({
      cus_id: [''],
      username: [''],
      owner: [''],
      type: ['']
    });
  }

  checkPermission() {
    this.showLoadingOverlay = true;
    this.userService.GetPermissionUser().subscribe(
      (data) => {
        this.showLoadingOverlay = false;
        this.canAccess = this.userService.RequestPermission(data, 'viewReport');
        if (this.canAccess) {
          this.getCustomers();
          this.getReports();
          this.getListType();
        } else {
          // redirect to deny page.
          this.router.parent.navigateByUrl('/deny');
        }
      },
      (err) => {
        this.messages = this.func.Messages('danger', this.func.parseErrorMessageFromServer(err));
        this.showLoadingOverlay = false;
      }
    );
  }

  changePageSize($event: any) {
    this.perPage = $event.target.value;
    if (this.pagination) {
      if ((this.pagination['current_page'] - 1) * this.perPage >= this.pagination['total']) {
        this.pagination['current_page'] = 1;
      }
      this.getReports(this.pagination['current_page']);
    }
  }

  getReports(pageNum: number = 1) {
    const sortField = this.sortData['fieldname'] ? this.sortData['fieldname'] : 'updated_at';
    const sortValue = this.sortData['sort'] ? this.sortData['sort'] : 'desc';
    this.searchForm.value['page'] = pageNum;
    this.searchForm.value['limit'] = this.perPage;
    this.searchForm.value['start_date'] = $('#start_time').val();
    this.searchForm.value['end_date'] = $('#end_time').val();
    const params = '?' + $.param(this.searchForm.value) + '&sort[' + sortField + ']=' + sortValue;
    this.exportAPIUrl = this.service.getReportAPIUrl(params);
    this.showLoadingOverlay = true;
    this.service.getReports(params).subscribe(
      (data) => {
        this.showLoadingOverlay = false;
        this.grReports = data.data;
        for (var i =0; i < data.data.length; i++) {
          if (data.data[i].type == "IB") {
            data.data[i].type = "INBOUND";
          }
          else if (data.data[i].type == "OB") {
            data.data[i].type = "OUTBOUND";
          }
        }
        // paging
        this.pagination = data['meta']['pagination'];
        this.pagination['numLinks'] = 3;
        this.pagination['tmpPageCount'] = this.func.Pagination(this.pagination);
      },
      (err) => {
        this.messages = this.func.Messages('danger', err.json().errors.message);
        this.showLoadingOverlay = false;
      },
      () => {}
    );
  }

  getCustomers() {
    const params: string = '?limit=2000';
    this.service.getCustomers(params).subscribe(
      (data) => {
        this.customers = data['data'];
      },
      (err) => {
        this.messages = this.func.Messages('danger', err.json().errors.message);
      },
      () => {}
    );
  }

  reset() {
    this.resetSearchForm();
    this.getReports();
  }

  search() {
    this.getReports();
  }

  resetSearchForm() {
    this.func.ResetForm(this.searchForm);
    $('#start_time').val('');
    $('#end_time').val('');
  }

  updateMessages(messages) {
    this.messages = messages;
  }

  getListType() {
    this.listType = [
      {type: 'IB', name: 'INBOUND'},
      {type: 'OB', name: 'OUTBOUND'}
    ];
  }
}

