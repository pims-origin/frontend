/*==========================================================
* @Author : Tien.Nguyen
* ASN CELL
* Version : 1.0
* ==========================================================*/
import { Component,Directive,Output, ElementRef, EventEmitter , HostListener, Input } from '@angular/core';
@Directive({ selector: '[input-value-cell]'})
export class ASNCellDirective{

    @Output() paseteData  = new EventEmitter();

    constructor(private el: ElementRef) {}

}

/*======================================
* Popup Show modal
*================================= */

declare var jQuery: any;
@Component ({
    selector: 'progess-bar',
    directives:[],
    providers: [],
    templateUrl: 'progressBar.html'
})
export class ProGressBarDirective{

    private _progressValue=0;
    private progressBarDiv;

    @Input('progressValue') set progress(value) {
        this._progressValue=value;
        this.handleProgressBar(value);
    }

    constructor(){

    }

    ngAfterViewInit() {
        this.progressBarDiv=jQuery('#progressBar');
    }

    showProgressBar(){
        this.progressBarDiv.modal('show');
    }

    hideProgressBar() {
        this.progressBarDiv.modal('hide');
    }

    handleProgressBar(progressValue){
        
        if(this.progressBarDiv){
            if(progressValue>0){
                this.showProgressBar();
            }else{
                if(progressValue==0){
                    this.hideProgressBar();
                }
            }
        }
    }
    
}