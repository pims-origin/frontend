import { Pipe, PipeTransform, Injectable } from '@angular/core';

/**
 *
 *  <ul>
 * 		<li *ngFor="let uom of uoms | greaterLevel:level" >{{uom.name}}</li>
 *  </ul>
 */

@Pipe({
  name: 'greaterLevel',
  pure: false
})
@Injectable()
export class greaterLevel implements PipeTransform {
  transform(items:any[], field: string, level:any):any[] {
    level = parseInt(level, 10);
    return items.filter(item => item[field] > level);
  }
}
