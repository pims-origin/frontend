import {Component, Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {API_Config, Functions} from '../common/core/load';

@Injectable()
export class RelocationServices {

  public AuthHeader = this._Func.AuthHeader();
  public AuthHeaderPost = this._Func.AuthHeaderPost();
  private apiService = this._API.API_GOODS_RECEIPT_MASTER+'/relocations';

  constructor(private _Func: Functions, private _API: API_Config, private http: Http)
  {
  }

  /*
   *
   * Search function
   * */
  Search(whsId,param)
  {
    return this.http.get(this._API.API_Warehouse + "/" + whsId + "/locations" + param,{ headers: this.AuthHeader })
      .map((res: Response) => res.json());
  }

  getItemLocation(idLocations,params)
  {
    return this.http.get(this.apiService+"/"+idLocations+params,{ headers: this.AuthHeader })
      .map((res: Response) => res.json());
  }

  reLocation(params)
  {
    return this.http.put(this.apiService,params,{ headers: this._Func.AuthHeaderPostJson() })
      .map((res: Response) => res.json().data);
  }

  activeLoc(loc_id){
    // # /v1/locations/{locationId}/changeLocationStatus
    return this.http.put(this._API.API_GOODS_RECEIPT_MASTER+'/locations/'+loc_id+'/changeLocationStatus','',{ headers: this._Func.AuthHeaderPostJson() })
      .map((res: Response) => res.json().data);
  }

  overWriteNewLocation(locId,params){
    return this.http.put(this._API.API_GOODS_RECEIPT_MASTER+'/locations/'+locId+'/overWriteNewLocation',params,{ headers: this._Func.AuthHeaderPostJson() })
      .map((res: Response) => res.json().data);
  }
}
