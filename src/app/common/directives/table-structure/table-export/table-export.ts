
import {Component, Input, Injectable} from '@angular/core';
import {TableExportService} from './table-export-service';
import {BaseService} from '../../../core/classes_us/base-service';
import {TableFunctions} from '../../../core/load';

@Component ({
    selector: 'table-export',
    providers: [
        TableExportService
    ],
    templateUrl: 'table-export.html',
})

@Injectable()

export class TableExport {

    @Input() parentComponent = <any> {};
    @Input() headerDef: any [];
    @Input() rowData: any [];
    @Input() dataExport: any [];
    @Input() downloadName: string = 'exportTableData';

    private headerExport: any [];

    constructor(
        private _tableExportService: TableExportService,
        private _baseServices: BaseService,
        private _tableFunc: TableFunctions
    )
    {

    }

    //**************************************************************************

    public exportPdf()
    {
        this._tableExportService.exportPdf(this.parentComponent);
    }

    //**************************************************************************

    public exportCsv()
    {
        const useExport = this.parentComponent.get('useExport');
        if (useExport) {
            const printUrl = this.parentComponent.get('currentURL') + '&export=1';
            const filename = this.parentComponent.hasOwnProperty('downloadName') ?
                this.parentComponent.get('downloadName') : this.parentComponent.get('title');
            const exportMethod = this.parentComponent.get('exportMethod');
            this.parentComponent.set('showLoadingOverlay', true);
            this._baseServices.print(printUrl, filename + '.csv', exportMethod, msg => {
                this.parentComponent.set('showLoadingOverlay', false);
                if (msg) {
                    this._tableFunc.showMessage(this.parentComponent, 'danger', msg);
                }
            });
        } else {
            this._tableExportService.exportCsv(this.parentComponent);
        }
    }

    //**************************************************************************

}
