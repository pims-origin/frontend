import {Component} from "@angular/core";
import {RoleService} from "./role.services";
import {SearchPipe} from "./search.pipe";
import {ControlGroup, FORM_DIRECTIVES, FormBuilder, Validators, Control} from "@angular/common";
import {ValidationService} from "../common/core/validator";
import {BoxPopupService} from "../common/popup/box-popup.service";
import {WMSMessages} from "../common/directives/messages/messages";
import {Functions} from "../common/core/functions";
import {UppercaseToSpaceLowerPipe} from "./string.pipe";

declare const jQuery:any;

@Component({
    selector: 'role',
    providers: [RoleService, FormBuilder, ValidationService, BoxPopupService],
    templateUrl: 'role.html',
    directives: [FORM_DIRECTIVES, WMSMessages],
    pipes: [SearchPipe, UppercaseToSpaceLowerPipe]

})
export class RoleComponent {
    private roleList:Array<any> = [];
    private permissionFilterKey = '';
    private allGroupPermission:any[] = [];
    private messages;
    private popupMessages:any;
    private submitted = false;
    private activeRole:any = {};
    private curRoleName:string = '';
    private isCheckedAll:boolean = false;
    roleForm:ControlGroup;
    private showForm=true;

    constructor(private roleService:RoleService,
                private fb:FormBuilder,
                private validationService:ValidationService,
                private boxPopupService:BoxPopupService,
                private funcs:Functions) {
    }

    ngOnInit() {
        this.formBuilder();
        this.getRoleList();
        this.getAllPermission();
    }

    private formBuilder() {
        this.roleForm = this.fb.group({
            code: ['', Validators.compose([this.validationService.customRequired, this.validationService.invalidCodeStr])],
            name: ['', Validators.compose([this.validationService.customRequired, this.validationService.invalidNameStr])],
            description: ['']
        });
    }

    private resetRoleForm() {
        this.popupMessages = null;
        this.showForm=false;
        this.submitted = false;

        setTimeout(()=>{
            this.showForm=true;
            this.formBuilder();
        })
    }

    private showMessage(msgStatus, msg) {
        this.messages = {
            'status': msgStatus,
            'txt': msg
        };
        jQuery(window).scrollTop(0);
    }

    private updateActiveRole(role:any) {
        this.activeRole['is_active'] = false;
        role['is_active'] = true;
        this.activeRole = role;
    }

    private getRoleList() {
        var params = "?limit=10000";
        this.roleService.getRole(params)
            .subscribe(
                data => {
                    this.roleList = data.data;
                    this.setActiveRole(this.activeRole['name']);
                },
                err => {
                    this.showMessage('danger', this.funcs.parseErrorMessageFromServer(err));
                }
            );
    }

    private getAllPermission() {
        this.roleService.getPermission()
            .subscribe(
                data => {
                    if (data.data.group_permissions) {
                        this.allGroupPermission = data.data.group_permissions;
                        this.upgradeGroupsPermission();
                        this.setIsCheckedAll();
                    }
                },
                err => {
                    this.showMessage('danger', this.funcs.parseErrorMessageFromServer(err));
                }
            )
    }

    private checkAll() {
        this.isCheckedAll = !this.isCheckedAll;

        if (this.isCheckedAll) {
            this.allGroupPermission = this.allGroupPermission.map(groupPermission => {
                for (const permissionObj of groupPermission['extended_permissions']) {
                    permissionObj['checked'] = true;
                }
                groupPermission['checked'] = true;

                return groupPermission;
            });
        } else {
            this.allGroupPermission = this.allGroupPermission.map(groupPermission => {
                for (const permissionObj of groupPermission['extended_permissions']) {
                    permissionObj['checked'] = false;
                }
                groupPermission['checked'] = false;

                return groupPermission;
            });
        }
    }

    private checkGroupPermission(groupPermission:any = {}) {
        groupPermission['checked'] = !groupPermission['checked'];

        if (groupPermission['checked']) {
            for (const exPermission of groupPermission['extended_permissions']) {
                exPermission['checked'] = true;
            }
        } else {
            for (const exPermission of groupPermission['extended_permissions']) {
                exPermission['checked'] = false;
            }
        }

        this.setIsCheckedAll();
    }

    private checkPermission(permission:any = {}, groupPermission:any = {}) {
        permission['checked'] = !permission['checked'];

        if (!permission['checked']) {
            groupPermission['checked'] = false;
        } else {
            for (const per of groupPermission['extended_permissions']) {
                if (!per['checked']) {
                    groupPermission['checked'] = false;
                    return;
                }
            }
            groupPermission['checked'] = true;
        }

        this.setIsCheckedAll();
    }

    private upgradeGroupsPermission() {
        this.allGroupPermission =  this.allGroupPermission.map(groupPermission => {
            groupPermission['extended_permissions'] = [];
            for (const permissionName of groupPermission['permissions']) {
                groupPermission['extended_permissions'].push({name: permissionName, checked: false});
            }
            groupPermission['checked'] = false;

            return groupPermission;
        });
    }

    private processAllGroupPermission(groupsPermission:any[] = []) {
        this.allGroupPermission = this.allGroupPermission.map(groupPermission => {
            for (const groupPerTemp of groupsPermission) {
                if (groupPermission['group'] === groupPerTemp['group']) {
                    let countPermission = 0;
                    for (const exPermission of groupPermission['extended_permissions']) {
                        if (groupPerTemp['permissions'].includes(exPermission['name'])) {
                            exPermission['checked'] = true;
                            countPermission += 1;
                        }
                    }
                    if (countPermission === groupPermission['extended_permissions'].length) {
                        groupPermission['checked'] = true;
                    }
                    continue;
                }
            }
            return groupPermission;
        });
    }

    private setIsCheckedAll() {
        for (const groupPermission of this.allGroupPermission) {
            if (!groupPermission['checked']) {
                this.isCheckedAll = false;
                return;
            }
        }
        this.isCheckedAll = true;
    }

    private getPermissionsByRoleName(roleName:string = '') {
        // prevent call api again
        if (roleName === this.curRoleName) {
            return;
        }
        this.curRoleName = roleName;

        this.roleService.getPermissionsByRoleName(roleName).subscribe(
            data => {
                this.upgradeGroupsPermission();
                this.processAllGroupPermission(data['group_permissions']);
                this.setIsCheckedAll();
            },
            err => {
                this.showMessage('danger', this.funcs.parseErrorMessageFromServer(err));
            }
        );
    }

    private setActiveRole(roleName:string = '') {
        for (let role of this.roleList) {
            if (role['name'] === roleName) {
                this.activeRole['is_active'] = false;
                role['is_active'] = true;
                this.activeRole = role;
                return;
            }
        }
    }

    private submit():void {
        this.submitted = true;
        if (this.roleForm.valid) {
            this.roleForm.value['permissions'] = this.getGroupPermission('Dashboard')['permissions'];

            this.roleService.addRole(JSON.stringify(this.roleForm.value)).subscribe(
                data => {
                    jQuery('#addRole').modal('hide');
                    this.showMessage('success', this.funcs.msg('ROLE001'));
                    this.activeRole = this.roleForm.value;
                    this.getRoleList();
                    this.getPermissionsByRoleName(this.activeRole['name']);
                },
                err => {
                    this.showMessage('danger', this.funcs.parseErrorMessageFromServer(err));

                    this.popupMessages = {
                        'status': 'danger',
                        'txt': this.funcs.parseErrorMessageFromServer(err)
                    };
                }
            );
        }
    }

    private update() {
        // Prepare updated data for role
        let updatedRole = {code: this.activeRole['code'], name: this.activeRole['name'], update_name: this.activeRole['name'], description: this.activeRole['description']};
        let permissions = [];

        for (const groupPermission of this.allGroupPermission) {
            for (const exPermissions of groupPermission['extended_permissions']) {
                if (exPermissions['checked']) {
                    permissions.push(exPermissions['name']);
                }
            }
        }
        updatedRole['permissions'] = permissions;

        this.roleService.updateRole(updatedRole['name'], JSON.stringify(updatedRole)).subscribe(
            data => {
                this.showMessage('success', this.funcs.msg('ROLE002'));
            },
            err => {
                this.showMessage('danger', this.funcs.parseErrorMessageFromServer(err));
            }
        );
    }

    private getGroupPermission(groupName:string = '') {
        for (const groupPermission of this.allGroupPermission) {
            if (groupPermission['group'] === groupName) {
                return groupPermission;
            }
        }
    }

    private cancel() {
        this.boxPopupService.showWarningPopup()
            .then((ok) =>{
                if (ok) {
                    jQuery('#addRole').modal('hide');
                }
            });
    }
}