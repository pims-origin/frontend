import {Component, Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {API_Config, Functions} from '../common/core/load';

@Injectable()
export class PalletTransferServices {

    public AuthHeader = this._Func.AuthHeader();
    public AuthHeaderPost = this._Func.AuthHeaderPost();
    private apiService = this._API.API_GOODS_RECEIPT_MASTER+'/consolidation_plt';

    constructor(private _Func: Functions, private _API: API_Config, private http: Http)
    {
    }

    /*
     *
     * Search function
     * */
    Search(whs_id:any,param)
    {
        return this.http.get(this._API.API_GOODS_RECEIPT_MASTER+'/palletrfid/'+whs_id+ param,{ headers: this.AuthHeader })
            .map((res: Response) => res.json());
    }

    getItemLocation(idLocations,params)
    {
        return this.http.get(this.apiService+"/"+idLocations+params,{ headers: this.AuthHeader })
            .map((res: Response) => res.json());
    }

    transferPallet(whs_id:any,data:any)
    {
        return this.http.post(this._API.API_GOODS_RECEIPT_MASTER+'/transfer-pallet/'+whs_id,data,{ headers: this._Func.AuthHeaderPostJson() })
            .map((res: Response) => res.json().data);
    }


    getWhs_code_by_id($id){

        return this.http.get(this._API.API_Warehouse+"/"+$id,{ headers: this.AuthHeader })
            .map((res: Response) => res.json());

    }
}