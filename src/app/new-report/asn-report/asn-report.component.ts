import {Component,OnInit} from "@angular/core";
import { Router, RouterLink, ROUTER_DIRECTIVES } from '@angular/router-deprecated';
import { FORM_DIRECTIVES,ControlGroup,FormBuilder, Control,Validators} from '@angular/common';
import {API_Config, UserService, Functions} from '../../common/core/load';
import { Http } from '@angular/http';
import {BoxPopupService} from "../../common/popup/box-popup.service";
import {ASN_REPORT_Services} from "./ans-report-service";
import {WMSPagination,AgGridExtent,WMSMessages, ReportFileExporter} from '../../common/directives/directives';
import 'ag-grid-enterprise/main';
declare var jQuery: any;

@Component({
  selector: 'asn-report',
  providers:[ASN_REPORT_Services],
  directives: [ROUTER_DIRECTIVES,AgGridExtent,WMSMessages,WMSPagination, ReportFileExporter],
  templateUrl: 'asn-report.component.html',

})
export class ASN_REPORT_Component{

  private headerURL = this.apiService.API_User_Metas+'/asr';
  private columnData = {
    asn_sts_name: {
      title:'Status',
      width:50,
      pin:true,
      ver_table:'9',
      sort:true
    },
    asn_hdr_num: {
      title:'ASN Number',
      width:90,
      pin:true,
      sort:true
    },
    asn_hdr_ref: {
      title:'Ref Code',
      width:170,
      pin:true,
      sort:true
    },
    asn_date:{
      title:'Created Date',
      width:120,
      pin:true
    },
    cus_name: {
      title:'Customer',
      width:170,
      pin:true,
      sort:true
    },
    item_ttl: {
      title:'# of SKUs',
      width:70
    },
    ctnr_ttl: {
      title:'# of CNTRs',
      width:120
    },
    ctn_ttl: {
      title:'# of Pack',
      width:150
    },
    asn_hdr_ept_dt: {
      title:'Expected Date',
      width:130
    },
    asn_hdr_act_dt: {
      title:'Goods Receipt Date',
      width:100
    },
    user: {
      title:'User',
      width:130
    }
  };

  private Pagination;
  private perPage=20;
  private whs_id;
  private messages;
  private Loading={};
  private searching={};
  searchForm:ControlGroup;
  private isSearch:boolean=false;
  private workOrderList:Array<any>=[];
  private sortData={fieldname:'created_at',sort:'desc'};
  private showLoadingOverlay:boolean=false;
  private ListSelectedItem:Array<any>=[];
  private Customers:Array<any>=[];
  private asnStatus:Array<any>=[];
  private viewTime=1;
  private cus_id='';
  private ans_status='';

  // Export csv file by API
  private exportAPIUrl:string = '';
  private fileName:string = 'ASN-report.csv';

  constructor(
    private asnReportService:ASN_REPORT_Services,
    private _Func: Functions,
    private _boxPopupService: BoxPopupService,
    private  apiService:API_Config,
    private _user:UserService,
    private fb: FormBuilder,
    private _http: Http,
    private _router: Router) {

    this.checkPermission();
    this.whs_id = localStorage.getItem('whs_id');


  }

  // Check permission for user using this function page
  private viewReport;
  private allowAccess:boolean=false;
  private checkPermission() {
    this.showLoadingOverlay = true;
    this._user.GetPermissionUser().subscribe(
      data => {
        this.viewReport = this._user.RequestPermission(data, 'viewReport');
        /* Check orther permission if View allow */
        if(!this.viewReport) {
          this.redirectDeny();
        }
        else {
          this.allowAccess=true;
          this.getAsnReportList();
          this.getASNStatus();
          this.getCustomersByWH();
        }
      },
      err => {
        this.messages=this._Func.Messages('danger', this._Func.parseErrorMessageFromServer(err));
        this.showLoadingOverlay = false;
      }
    );
  }


  // get list custommer

  private getCustomersByWH () {

    let params='?whs_id=' + this.whs_id + "&limit=10000";
    this.asnReportService.getCustomersByWH(params)
      .subscribe(
        data => {
          this.Customers = data.data;
        },
        err => {
          this.parseError(err);
        },
        () => {
        }
      );

  }

  private changeTimeView(time)
  {
    this.viewTime=time;
    this.getAsnReportList(this);
  }


  redirectDeny(){
    this._router.parent.navigateByUrl('/deny');
  }


  private onPageSizeChanged($event)
  {
    this.perPage = $event.target.value;
    if(this.Pagination){
      if((this.Pagination['current_page'] - 1) * this.perPage >= this.Pagination['total']) {
        this.Pagination['current_page'] = 1;
      }
      this.getAsnReportList(this.Pagination['current_page']);
    }

  }

  private getAsnReportList(page=null){

    let param="?sort["+this.sortData['fieldname']+"]="+this.sortData['sort']+"&page="+page+"&limit="+this.perPage;
    param+='&day='+this.viewTime+'&cus_id='+this.cus_id+"&asn_sts="+this.ans_status;
    this.exportAPIUrl = this.asnReportService.getReportAPIUrl(param);
    this.showLoadingOverlay=true;
    this.asnReportService.getListAsn(param).subscribe(
      data => {
        this.showLoadingOverlay=false;
        this.workOrderList = this._Func.formatData(data.data);
        //pagin function
        this.Pagination=data['meta']['pagination'];
        this.Pagination['numLinks']=3;
        this.Pagination['tmpPageCount']=this._Func.Pagination(this.Pagination);
      },
      err => {
        this.parseError(err);
        this.showLoadingOverlay=false;
      },
      () => {}
    );

  }

  // Get ASN All status
  private getASNStatus () {

    this.asnReportService.getAsnStatus()
      .subscribe(
        data => {

          this.asnStatus = data.data;

        },
        err => {

          this.parseError(err);
        },
        () => {}
      );

  }

  scolltoTop(){
    jQuery('html, body').animate({
      scrollTop: 0
    }, 700);
  }

  private updateMessages(messages) {
    this.messages = messages;
  }

  // Show error when server die or else
  private parseError (err) {
    err = err.json();
    this.messages = this._Func.Messages('danger', err.errors.message);
  }
}
