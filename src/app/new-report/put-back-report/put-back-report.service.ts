import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {API_Config, Functions} from '../../common/core/load';

@Injectable()
export class PutBackReportService {
  private authHeader:any;

  constructor(private func:Functions, private api:API_Config, private http:Http) {
    this.authHeader = func.AuthHeader();
  }

  getReports(params:string = '') {
    return this.http.get(`${this.api.API_PUT_BACK_REPORT}${params}`, {headers: this.authHeader})
        .map(res => res.json());
  }

  getReportAPIUrl(params:string = '') {
    return `${this.api.API_PUT_BACK_REPORT}${params}&export=1`;
  }

}
