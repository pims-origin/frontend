import {Component, Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {API_Config, Functions} from '../../common/core/load';

@Injectable()
export class ASN_REPORT_Services {

  public AuthHeader = this._Func.AuthHeader();
  public AuthHeaderPost = this._Func.AuthHeaderPost();

  constructor(private _Func: Functions, private _API: API_Config, private http: Http) {

  }


  // Get list asn
  public getListAsn ($params = '') {

    return this.http.get(this._API.API_Asns +  $params, {headers: this.AuthHeader})
      .map(res => res.json());

  }

  getReportAPIUrl(params:string = '') {
    return `${this._API.API_Asns}${params}&export=1`;
  }

  getCustomersByWH($params) {
    return  this.http.get(this._API.API_CUSTOMER_MASTER + '/customer-warehouses' + $params + '&sort[cus_name]=asc' , {headers: this.AuthHeader})
      .map(res => res.json());
  }

  // Get ASN All status by Huy
  public getAsnStatus() {

    return this.http.get(this._API.API_ASN_Status + '?sort[asn_sts_name]=asc', {headers: this.AuthHeader})
      .map(res => res.json());

  }


}
