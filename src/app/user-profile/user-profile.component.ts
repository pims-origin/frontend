import {Component} from '@angular/core';
import {Router, RouterLink, RouteConfig, ROUTER_DIRECTIVES} from '@angular/router-deprecated';
import {UserProfileDetailComponent} from "./user-profile-detail/user-profile-detail.component";
import {EditProfileComponent} from "./edit-profile/edit-profile.component";
import {ChangePasswordComponent} from "./change-password/change-password.component";
//import {HeaderComponent} from "../layout/header/header.component";

@Component ({
  selector: 'user-profile',
  directives: [ROUTER_DIRECTIVES],
  //providers: [HeaderComponent],
  templateUrl: 'user-profile.component.html'
})

@RouteConfig([
  { path: '/', component: UserProfileDetailComponent, name: 'User Profile Detail' },
  { path: '/edit', component: EditProfileComponent , name: 'Edit Profile',useAsDefault: true },
  { path: '/change-password', component: ChangePasswordComponent , name: 'Change Password' }


])
export class UserProfileComponent {
  //UserID: any;
  //constructor(public userID: HeaderComponent){
  //  this.UserID = this.userID.decodedJwt;
  //}

}
