import {Component, ViewEncapsulation} from '@angular/core';
import {Router, RouterLink, RouteConfig, ROUTER_DIRECTIVES} from '@angular/router-deprecated';
import {Home} from "./home/home.component";
import {LoginComponent} from "./authenticate/login/login.component";
import {ForgotPasswordComponent} from "./authenticate/forgot-password/components/forgot-password.component";
import {ForgotPasswordNoticeComponent} from "./authenticate/forgot-password/components/forgot-password-notice.component";
import {ResetPasswordComponent} from "./authenticate/forgot-password/components/reset-password.component";
import {LoggedInRouterOutlet} from "./authenticate/LoggedInOutlet";
import {PageNotFoundComponent} from "./common/page404/page404.component";
import {SetupPasswordComponent} from "./authenticate/setup-password/setup-password.component";
import {RFGUNComponent} from "./rf-gun/rf-gun.component";
import {RFGUNDEMOComponent} from "./rf-gun/demo/rf-gun-demo.component";

declare var jQuery: any;

@Component({
  selector: 'main-app',
  providers: [],
  pipes: [],
  directives: [ROUTER_DIRECTIVES, LoggedInRouterOutlet],
  template: `
        <secured-router-outlet></secured-router-outlet>        
    `,
  encapsulation: ViewEncapsulation.None,
  styleUrls: [
    '../../bower_components/jquery-ui/themes/smoothness/jquery-ui.min.css',
    '../../bower_components/bootstrap/dist/css/bootstrap.min.css',
    '../../bower_components/font-awesome/css/font-awesome.min.css',
    '../../bower_components/animate.css/animate.min.css',
    '../../bower_components/simple-line-icons/css/simple-line-icons.css',
    '../../node_modules/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css',
    '../../node_modules/bootstrap-multiselect/dist/css/bootstrap-multiselect.css',
    '../../bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
    '../../node_modules/npm-font-open-sans/open-sans.scss',
    '../../node_modules/raleway-webfont/raleway.min.css',
    '../../node_modules/sweetalert2/dist/sweetalert2.min.css',
    '../assets/scss/styles.scss',
  ]
})
@RouteConfig([
  { path: '/rf-gun/...', component: RFGUNComponent, name: 'RF Gun' },
  { path: '/rf-gun/demo', component: RFGUNDEMOComponent, name: 'RF Gun demo' },
  { path: '/login', component: LoginComponent, name: 'Login', useAsDefault: true },
  { path: '/forgot-password', component: ForgotPasswordComponent, as: 'Forgot Password' },
  { path: '/forgot-password-notice', component: ForgotPasswordNoticeComponent, as: 'Forgot Password Notice' },
  { path: '/reset-password', component: ResetPasswordComponent, as: 'Reset Password' },
  { path: '/setup-password', component: SetupPasswordComponent, as: 'Setup Password'},
  { path: '...', component: Home }
  // { path: '**',    component: PageNotFoundComponent },
])
export class MainApp{

  constructor(private router: Router) {
    window.addEventListener("popstate", function(e) {
      setTimeout(function() {
        var modal = jQuery('modal');
        var modalBackdrop = jQuery('.modal-backdrop');
        var body = jQuery('body');
        if(modalBackdrop.length && modalBackdrop.is(':visible')) {
          if(!modal.length) {
            modalBackdrop.remove();
            body.removeAttr('style').removeClass('modal-open');
          }
          else {
            if(!modal.is(':visible')) {
              modalBackdrop.remove();
              body.removeAttr('style').removeClass('modal-open');
            }
          }
        }
      });
    });
  }
  // ngOnInit(){
  //   this.router.parent.navigateByUrl('/login');
  // }
}
