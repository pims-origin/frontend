import {Component, Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {API_Config, Functions} from '../../../common/core/load';

@Injectable()
export class CRUOrderServices {

  public AuthHeader = this._Func.AuthHeader();
  public AuthHeaderPost = this._Func.AuthHeaderPost();

  constructor(private _Func: Functions, private _API: API_Config, private http: Http) {

  }

  getCountry(param) {
    return this.http.get(this._API.API_Country + param,{ headers: this.AuthHeader })
      .map((res: Response) => res.json().data);
  }

  /*
   * State
   * */
  State(idCountry,param)
  {
    return this.http.get(this._API.API_Country+'/'+idCountry+'/states'+param,{ headers: this.AuthHeader })
      .map((res: Response) => res.json().data);
  }

  // getCustomersByWH($params) {
  //   return  this.http.get(this._API.API_CUSTOMER_MASTER + '/customer-warehouses' + $params + '&sort[cus_name]=asc' , {headers: this.AuthHeader})
  //     .map(res => res.json());
  // }
  getCustomersByUser($params) {
    return  this.http.get(this._API.API_Customer_User + $params + '&sort[cus_name]=asc' , {headers: this.AuthHeader})
      .map(res => res.json());
  }

  searchItems(params){
    return this.http.get(this._API.API_SEARCH_ITEMS+params,{ headers: this.AuthHeader })
      .map((res: Response) => res.json());
  }

  addNewOrders(data){
    return this.http.post(this._API.API_ORDERS, data, { headers: this._Func.AuthHeaderPostJson() })
      .map((res: Response) => res.json().data);
  }

  addNewOrdersTransferWarehouse(data){
    return this.http.post(`${this._API.API_ORDERS_CORE}/transfer-warehouse/orders`, data, { headers: this._Func.AuthHeaderPostJson() })
      .map((res: Response) => res.json().data);
  }

  updateOrder(id,data){
    return this.http.put(this._API.API_ORDERS+'/'+id, data, { headers: this._Func.AuthHeaderPostJson() })
      .map((res: Response) => res.json().data);
  }

  getOrderType(){
    return this.http.get(this._API.API_ORDER_TYPES,{ headers: this.AuthHeader })
      .map((res: Response) => res.json());
  }

  getOrderDetail(orderId){
    return this.http.get(this._API.API_ORDERS+'/'+orderId,{ headers: this.AuthHeader })
      .map((res: Response) => res.json().data);
  }

  getCarton(orderId, $params){
    return this.http.get(this._API.API_ORDERS+'/'+ orderId + '/cartons' + $params,{ headers: this.AuthHeader })
        .map((res: Response) => res.json());
  }

  getUOM()
  {
    return this.http.get(this._API.API_UOM + '?limit=1000&sort[sys_uom_name]=asc&sys_uom_type=item', {
      headers: this.AuthHeader
    }).map(res => res.json());
  }

  getEventracking(param){
    return this.http.get(this._API.API_GOODS_RECEIPT_MASTER+'/eventracking'+param, {
      headers: this.AuthHeader
    }).map(res => res.json());
  }

  /*
   * saveOrderFlow
   * */
  saveOrderFlow(ord_id, data)
  {
    //console.log('__update item master id > ' +id,data);
    if(ord_id){
      return this.http.post(this._API.API_ORDERS+'/order-flows/' + ord_id, data, { headers: this._Func.AuthHeaderPostJson() })
          .map((res: Response) => res.json().data);
    }else{
      return this.http.post(this._API.API_Customers+'/order-flows/', data, { headers: this._Func.AuthHeaderPostJson() })
          .map((res: Response) => res.json().data);
    }

  }

  searchShippingTo(params){
    return this.http.get(this._API.API_SHIPPING+params,{ headers: this.AuthHeader })
        .map((res: Response) => res.json());
  }

  updateScheduleRouteDate(orderId, data){
    return this.http.put(this._API.API_ORDERS + '/edit-schedule-ship-and-routed-date/' + orderId, data, { headers: this._Func.AuthHeaderPostJson() })
        .map((res: Response) => res.json());
  }

  updateComment(id,data){
    return this.http.put(this._API.API_ORDERS+'/update-comments/'+id, data, { headers: this._Func.AuthHeaderPostJson() })
        .map((res: Response) => res.json().data);
  }

  getCarrierList($param: any) {
    return this.http.get(this._API.MASTERDATA + '/search' + $param, { headers: this.AuthHeader })
      .map(res => res.json());
  }

  getDeliveryService($params: any) {
    return this.http.get(this._API.MASTERDATA + '/search' + $params, { headers: this.AuthHeader })
      .map((res: Response) => res.json());
  }

  undoPackCartons(id) {
    return this.http.put(this._API.API_Pack + '/packs/' + id + '/undo', null, { headers: this._Func.AuthHeaderPostJson() })
        .map((res: Response) => res.json().data);
  }

  getCarrierTypeOther($params: any) {
    return this.http.get(this._API.API_ORDERS_CORE + '/carrier/autocomplete' + $params, { headers: this.AuthHeader })
      .map((res: Response) => res.json());
  }

  updateTrakingNumber(orderId, data) {
    return this.http.put(this._API.API_ORDERS_CORE + '/orders/'+ orderId +'/tracking-num', data, { headers: this._Func.AuthHeaderPostJson() })
      .map((res: Response) => res.json().data);
  }

  updateOrderTrackingNumber(orderId, data) {
    return this.http.put(this._API.API_ORDERS_CORE + '/orders/track-num/' + orderId, data, { headers: this._Func.AuthHeaderPostJson() })
      .map((res: Response) => res.json().data);
  }

}
