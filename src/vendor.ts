// Vendors

// Angular 2
import '@angular/platform-browser-dynamic';
import '@angular/platform-browser';
import '@angular/core';
import '@angular/http';
import '@angular/router-deprecated';
import 'ng2-sweetalert2';


// RxJS 5
// import 'rxjs/Rx';


// For vendors for example jQuery, Lodash, angular2-jwt import them here
// Also see src/typings.d.ts as you also need to run `typings install x` where `x` is your module

import '../bower_components/jquery-ui/jquery-ui.min.js';
import '../bower_components/bootstrap/dist/js/bootstrap.min.js';
import '../bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js';
import '../bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js';
import '../bower_components/ag-grid/dist/ag-grid.min.js';
import '../node_modules/jquery-slimscroll/jquery.slimscroll.min.js'
import '../node_modules/chart.js/dist/Chart.min.js'
import './assets/js/jquery.nestable.js';
import '../node_modules/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js';
import '../node_modules/bootstrap-multiselect/dist/js/bootstrap-multiselect.js';
import './assets/js/click.action.js';
