/**
 * Created by admin on 6/16/2016.
 */
import {Input, Output, EventEmitter, Component} from "@angular/core";
@Component({
  selector: 'breadCrumbs',
  template: `
<ol class="breadcrumb">
  <li *ngFor="let item of road"><a href="{{item.url}}">{{item.text}}</a></li>
</ol>
`
})
export class breadCrumbs {
  @Input() road;
  
  constructor(){

  }
}
