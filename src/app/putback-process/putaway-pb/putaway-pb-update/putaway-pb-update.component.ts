import {Component} from "@angular/core";
import {CruPaPbListComponent} from "../cru-pa-pb-list.component";

@Component ({
    selector: 'putaway-pb-update',
    directives: [CruPaPbListComponent],
    templateUrl: 'putaway-pb-update.component.html',
})

export class PutawayPbUpdateComponent{
    constructor(){

    }
}