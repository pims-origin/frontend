import {Component, Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {API_Config, Functions} from '../../../../common/core/load';

@Injectable()
export class Services {

    public AuthHeader = this._Func.AuthHeader();
    public AuthHeaderPost = this._Func.AuthHeaderPost();

    constructor(private _Func: Functions, private _API: API_Config, private http: Http) {

    }

    searchCourierName(params){
        return this.http.get(this._API.MASTERDATA+params,{ headers: this.AuthHeader })
            .map((res: Response) => res.json());
    }

    getCourierByCusId(cus_id:any){

        return this.http.get(this._API.API_CUSTOMER_MASTER+'/customers/account-number/' + cus_id,{ headers: this._Func.AuthHeader() })
            .map((res: Response) => res.json());

    }

    saveCourier(cus_id:any,data:any){

        return this.http.post(this._API.API_CUSTOMER_MASTER+'/customers/account-number/' + cus_id, data, { headers: this._Func.AuthHeaderPostJson() })
            .map((res: Response) => res.json().data);

    }

}
