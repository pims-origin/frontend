import {Component, Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {API_Config, Functions} from '../../common/core/load';
import {AsyncPipe} from "@angular/common";

@Injectable()
export class BillOfLadingService {

  public AuthHeader = this._Func.AuthHeader();
  public AuthHeaderPost = this._Func.AuthHeaderPost();

  constructor(private _Func:Functions,
              private _API:API_Config,
              private http:Http) {
  }

  public getBillOfLadingList($param) {
    return this.http.get(this._API.API_ORDERS_ORG + '/bol-list' + $param, {headers: this.AuthHeader})
      .map(res => res.json());
  }

  public getCustomersByWH($params) {
    return this.http.get(this._API.API_CUSTOMER_MASTER + '/customer-warehouses' + $params + '&sort[cus_name]=asc', {headers: this.AuthHeader})
      .map(res => res.json());
  }

  public getFullAddressList(cus_id, ship_ready, orderId) {
    return this.http.get(this._API.API_ORDERS_CORE + '/bol/address?ship_ready=' + ship_ready + '&cus_id=' + cus_id + '&odr_id=' + orderId, {headers: this.AuthHeader})
      .map((res:Response) => res.json());
  }

  public getCommodityDescList() {
    return this.http.get(this._API.API_ORDERS_CORE + '/carrier/commodityList', {headers: this.AuthHeader})
      .map((res:Response) => res.json());
  }

  public getDeliveryService(param='') {
    return this.http.get(this._API.API_ORDERS_CORE + '/delivery-service' + param, {headers: this.AuthHeader})
      .map((res:Response) => res.json());
  }

  public getTrailerLoad() {
    return this.http.get(this._API.API_ORDERS_CORE + '/carrier/show-status-drop-down?objects=trailer-load', {headers: this.AuthHeader})
      .map((res:Response) => res.json());
  }

  public getFreCounted() {
    return this.http.get(this._API.API_ORDERS_CORE + '/carrier/show-status-drop-down?objects=freight-counted', {headers: this.AuthHeader})
      .map((res:Response) => res.json());
  }

  public getFreChTerms() {
    return this.http.get(this._API.API_ORDERS_CORE + '/carrier/show-status-drop-down?objects=freight-charge-terms', {headers: this.AuthHeader})
      .map((res:Response) => res.json());
  }

  public getFeeTerms() {
    return this.http.get(this._API.API_ORDERS_CORE + '/carrier/show-status-drop-down?objects=fee-terms', {headers: this.AuthHeader})
      .map((res:Response) => res.json());
  }

  public getShipMethod() {
    return this.http.get(this._API.API_ORDERS_CORE + '/carrier/show-status-drop-down?objects=shipment-method', {headers: this.AuthHeader})
      .map((res:Response) => res.json());
  }

  public getListCustomersByUser($params) {
    return  this.http.get(this._API.API_Customer_Warehouses + $params + '&sort[cus_name]=asc' , {headers: this.AuthHeader})
      .map(res => res.json());
  }

  public getOrdersList(cusID,ordID){
    return  this.http.get(this._API.API_ORDERS_CORE + '/bol-orders/' + cusID + '/' + ordID, {headers: this.AuthHeader})
      .map(res => res.json());
  }

  public getCarrierByOrderId(cusID,ordID){
    return  this.http.get(this._API.API_ORDERS_CORE + '/bol-orders/' + cusID + '/' + ordID + '/carrier', {headers: this.AuthHeader})
      .map(res => res.json());
  }

  public getOrdersListByBol(cusID,bolID){
    return  this.http.get(this._API.API_ORDERS_CORE + '/orders-by-bol/' + cusID + '/' + bolID, {headers: this.AuthHeader})
      .map(res => res.json());
  }

  public createBol(data){
    return this.http.post(this._API.API_ORDERS_CORE + '/bol', data, { headers: this._Func.AuthHeaderPostJson() })
      .map((res: Response) => res.json().data);
  }

  public updateBol(ship_id, data){
    return this.http.put(this._API.API_ORDERS_CORE+'/bol/' + ship_id, data, { headers: this._Func.AuthHeaderPostJson() })
      .map((res: Response) => res.json().data);
  }

  public loadUSACountry(idCountry,param){
    return this.http.get(this._API.API_Country+'/'+idCountry+'/states'+param,{ headers: this.AuthHeader })
      .map((res: Response) => res.json().data);
  }

  public getBOLItem(id){
    return this.http.get(this._API.API_ORDERS_CORE + '/bol/' + id,{ headers: this.AuthHeader })
      .map((res: Response) => res.json().data);
  }

  createCommodity(data){
      return this.http.post(this._API.API_Commodity, data, { headers: this._Func.AuthHeaderPostJson() })
          .map((res: Response) => res.json().data);
  }

  public createBolV2(wh_id, data){
    return this.http.post(this._API.API_ORDERS_V2+ '/whs/' + wh_id + '/bol/create' , data, { headers: this._Func.AuthHeaderPostJson() })
        .map((res: Response) => res.json().data);
  }

  public getMasterPalletList(wh_id, id, params){
    return this.http.get(this._API.API_ORDERS_V2 + '/whs/' + wh_id +'/bol/master-pallet-list/' + id + params,{ headers: this.AuthHeader })
        .map((res: Response) => res.json());
  }

  public updateBol2(whs_id, ship_id, data){
    return this.http.put(this._API.API_ORDERS_V2 + '/whs/' + whs_id +'/bol/' + ship_id, data, { headers: this._Func.AuthHeaderPostJson() })
        .map((res: Response) => res.json().data);
  }

  getCountry(param) {
    return this.http.get(this._API.API_Country + param,{ headers: this.AuthHeader })
      .map((res: Response) => res.json().data);
  }

  State(idCountry,param)
  {
    return this.http.get(this._API.API_Country+'/'+idCountry+'/states'+param,{ headers: this.AuthHeader })
      .map((res: Response) => res.json().data);
  }

  searchShippingTo(params){
    return this.http.get(this._API.API_SHIPPING+params,{ headers: this.AuthHeader })
        .map((res: Response) => res.json());
  }

  public getOrdersListSameCustomer(cusID,ordID){
    return  this.http.get(this._API.API_ORDERS_CORE + '/bol-orders-same-customer/' + cusID + '/' + ordID, {headers: this.AuthHeader})
      .map(res => res.json());
  }

}
