import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Functions } from '../../../common/core/load';
import { ProGressBarDirective } from '../progress-bar/progress-bar';

declare let $: any;
declare let saveAs: any;

@Component({
	selector: 'progress-exporter',
	templateUrl: 'progress-exporter.html',
	directives: [ProGressBarDirective]
})

export class ProgressExporter {
	private exportedReportAPIUrl: string;
	private namedFile: string;
	private totalPart: string = '';
	private showLoadingOverlay: boolean = false;
	private contentType = '';
	private progress: number = 0;
	private limit: any;

	@Input('exportAPIUrl') set exportAPIUrl(value: any) {
		this.exportedReportAPIUrl = value;
	}

	@Input('fileName') set fileName(value: any) {
		this.namedFile = value;
	}

	@Input('limitRow') set limitRow(value: any) {
		this.limit = value;
	}

	@Input('total') set total(value: any) {
		if (value) {
			this.totalPart = String(Math.ceil(value/this.limit));
		}
	}


	@Output() bindMessages = new EventEmitter();

	constructor(private _func: Functions, private _http: Http) {
	}
//+ '&file_name=' + fileName
	private exportReportCSV(part: number = 1, fileName: string = '', ref = '') {
		$('html').scrollTop(0);
		let url = this.exportedReportAPIUrl + '&part=' + part + '&limitRow=' + this.limit + '&totalPart=' + this.totalPart;
		if (ref) {
			url += '&ref=' + ref;
		}
		this.progress = !this.progress ? 1 : this.progress;

		this._http.get(url, { headers: this._func.AuthHeader() })
			.map((res: Response) => {
				const contentType = res.headers.get('content-type');
				if (contentType.indexOf('/csv') !== -1) {// text/csv
					this.contentType = 'blob';
				} else {
					this.contentType = 'json';
					return res.json();
				}
				return res;
			})
			.subscribe(
				(res: Response) => {
					if (this.contentType === 'blob') {
						this.progress = 100;
						this.downloadFile(res['_body']);
						this.progress = 0;
					} else {
						const progress = Math.ceil(part * 100 / Number(this.totalPart));
						this.progress = progress < 100 ? progress : this.progress;
						part += 1;
						if (this.totalPart) {
							ref = res['data']['md5'];
						}
						if (res['status']) {
							this.exportReportCSV(part, fileName, ref);
						}
					}
				},
				(error) => {
					console.log(error);
				}
			);
	}

	private downloadFile(data) {
		let blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
		saveAs.saveAs(blob, this.namedFile);
	}
}
