import {Component} from '@angular/core';
import {Router, RouterLink, RouteConfig, ROUTER_DIRECTIVES} from '@angular/router-deprecated';
import {OrderListComponent} from "../order/order-list/order-list.component";
import {PalletAssignmentOutComponent} from "./pallet-assignment/pallet-assignment.component";
import {PalletAssignmentListComponent} from "./pallet-assignment-list/pallet-assignment-list.component";

@Component ({
    selector: 'pallet-assignment-management',
    directives: [ROUTER_DIRECTIVES],
    template: `<router-outlet></router-outlet>`,
})

@RouteConfig([
    { path: '/', component: PalletAssignmentListComponent , name: 'PalletAssignment List', data:{page:'pallet-assignment'}, useAsDefault: true },
    { path: '/:id', component: PalletAssignmentOutComponent , name: 'PalletAssignment Detail', data:{action:'edit'}}
])
export class PalletAssignmentManagementComponent {

}
