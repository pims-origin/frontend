import {Component, Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {API_Config, Functions} from '../../common/core/load';

@Injectable()
export class ASN_Services {

    public AuthHeader = this._Func.AuthHeader();
    public AuthHeaderPost = this._Func.AuthHeaderPostJson();

    constructor(private _Func: Functions, private _API: API_Config, private http: Http) {}

    // Get list asn
    public getListAsn ($params = '') {

        return this.http.get(this._API.API_Asns +  $params, {headers: this.AuthHeader})
            .map(res => res.json());

    }

    getCustomersByWH($params) {
        return  this.http.get(this._API.API_CUSTOMER_MASTER + '/customers-user' + $params + '&sort[cus_name]=asc' , {headers: this.AuthHeader})
            .map(res => res.json());
    }

    // Get uoms
    public getUoms ($params) {

        return  this.http.get(this._API.API_UOM + $params, {headers: this.AuthHeader})
            .map(res => res.json());

    }

    // Autocomplete for search by name
    public getContainerByName ($params) {

        return this.http.get(this._API.API_Container + $params, {headers: this.AuthHeader})
            .map(res => res.json().data);


    }

    searchItems(params){
        return this.http.get(this._API.API_ItemMaster+params,{ headers: this.AuthHeader })
            .map((res: Response) => res.json());
    }

    getMeasurements()
    {
        return  this.http.get(this._API.API_Measurements, {headers: this.AuthHeader})
            .map(res => res.json());
    }

    saveASN(asnId, data, ctnId = 0) {
        if(!asnId){
            return this.http.post(this._API.API_Asns, data , {headers: this.AuthHeaderPost})
                .map(res => res.json())
        }
        else{
            return this.http.put(this._API.API_Asns + "/" + asnId + "/containers/" + ctnId, data , {headers: this.AuthHeaderPost})
                .map(res => res.json())
        }
    }

    public getAsnByIdCtnId ($id, $ctn_id) {
        return  this.http.get(this._API.API_Asns + "/" + $id + '/containers/' + $ctn_id, {headers: this.AuthHeader})
            .map(res => res.json());
    }

    // Get containers
    public getContainersByASN (asnId) {

        return this.http.get(this._API.API_Asns + '/' + asnId + '/list-containers', {headers: this.AuthHeader})
            .map(res => res.json());

    }

    public duplicatedField(){

        return ['dtl_sku','dtl_size','dtl_color','dtl_pack','dtl_po','dtl_lot'];

    }

    public getAsnStatus() {

        return this.http.get(this._API.API_ASN_Status + '?sort[asn_sts_name]=asc', {headers: this.AuthHeader})
            .map(res => res.json());

    }

    public saveGoodReceipt(params) {

        return this.http.post(this._API.API_Goods_Receipt, params , {headers: this.AuthHeaderPost})
            .map(res => res.json())
    }

    public getEventTrackingByAsn($params) {

        return this.http.get(this._API.API_GOODS_RECEIPT_MASTER+'/eventracking' + $params,{ headers: this.AuthHeader })
            .map(res => res.json());

    }

    // Cancel ASN
    public cancelAsn(asnId) {
        return this.http.get(`${this._API.API_Asns}/${asnId}/cancel`, {headers: this.AuthHeader}).map(res => res.json());
    }
    
    autocomplete(params){
        return this.http.get(this._API.API_GOODS_RECEIPT + '/asn/rma-orders' + params,{ headers: this.AuthHeader })
            .map((res: Response) => res.json());
    }

    saveRMA(params) {
        return this.http.post(this._API.API_GOODS_RECEIPT + '/asn/rma-orders', params , {headers: this.AuthHeaderPost})
            .map(res => res.json())
    }

    completeReceiving(asnId, data) {
        return this.http.put(this._API.API_GOODS_RECEIPT + '/asns/' + asnId + '/complete_receiving_asn', data , {headers: this.AuthHeaderPost})
            .map(res => res.json())
    }

    undoComplete(asnId, data) {
        return this.http.put(this._API.API_GOODS_RECEIPT + '/asns/' + asnId + '/revert_receiving_asn', data , {headers: this.AuthHeaderPost})
            .map(res => res.json())
    }

    getLPN() {
        return this.http.get(this._API.API_GOODS_RECEIPT +'/warehouse/lpn/show', {headers: this.AuthHeader})
            .map(res => res.json());
    }

    /**
     * Get list of ASN Type
     *
     * @param data
     */
    getASNTypeList() {
        return this.http.get(`${this._API.API_GOODS_RECEIPT}/asn-types`, { headers: this.AuthHeader })
            .map(res => res.json());
    }
}
