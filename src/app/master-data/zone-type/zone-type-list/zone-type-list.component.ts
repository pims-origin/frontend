import {Component} from '@angular/core';
import { Router } from '@angular/router-deprecated';
import {API_Config, UserService, Functions} from '../../../common/core/load';
import {ZoneTypeServices} from '../zone-type-service';
import { FORM_DIRECTIVES,ControlGroup,FormBuilder, Control} from '@angular/common';
import { Http } from '@angular/http';
import {WMSBreadcrumb} from '../../../common/directives/directives';
declare var jQuery: any;
@Component({
	selector: 'zone-type-list',
	providers: [ZoneTypeServices,FormBuilder],
	directives: [FORM_DIRECTIVES,WMSBreadcrumb],
	templateUrl: 'zone-type-list.component.html',
})

export class ZoneTypeListComponent {
	private ZoneTypeList: Array<any> = [];
	private Pagination = [];
	public Loading = [];
	public ListSelectedItem: Array<any> = [];
	public selectedAll: boolean = false;
	private messages;
    private currentPage;
    SearchForm: ControlGroup;
    public showLoadingOverlay = false;
    private hasEditPermission = false;
    private hasCreatePermission = false;
    private viewUser = false;
    private searchCriteria = {};
	constructor(
		private _http: Http,
		private _Func: Functions,
		private _user: UserService,
        private fb: FormBuilder,
		private _ZTP: ZoneTypeServices,
		private _router: Router) {

		// /*check permission*/
		// /*this._user.GetPermissionUser().subscribe(
		//  data => {
		//   this.ListPermission = data;
		//   this.viewMenuWarehouse = this._user.RequestPermission(data, 'viewMenuWarehouse');
		//   /* Check orther permission if View allow */
		//   if (this.viewMenuWarehouse) {
		// 	  this.createMenu = this._user.RequestPermission(data, 'createMenu');
		//   }
		//   else {
		// 	  this.router.parent.navigateByUrl('/deny');
		//   }
		//  },
		//  err => console.log(err),
		//  () => console.log('GetPermissionUser Complete')
		//    );

		/*
		* Get List WareHouse
		* */
    // builder form search


    this.BuilderSearchForm();
    this.searchCriteria = this._Func.genSearchCriteriaFromForm(this.SearchForm);
    this.checkPermission();

    // get country

  }

    // Check permission for user using this function page
    private checkPermission() {
        this.showLoadingOverlay = true;
        this._user.GetPermissionUser().subscribe(
            data => {
                this.hasEditPermission = this._user.RequestPermission(data, 'editZoneType');
                this.hasCreatePermission = this._user.RequestPermission(data, 'createZoneType');
                this.viewUser = this._user.RequestPermission(data,'viewZoneType');
                /* Check orther permission if View allow */
                if(!this.viewUser) {
                    this._router.parent.navigateByUrl('/deny');
                }
                else {
                    this.GetZoneTypeList();
                }
            },
            err => {
                this.showMessage('danger', this._Func.parseErrorMessageFromServer(err));
                this.showLoadingOverlay = false;
            }
        );
    }

    private showMessage(msgStatus, msg) {
        this.messages = {
            'status': msgStatus,
            'txt': msg
        }
        jQuery(window).scrollTop(0);
    }

  /*
  * Builder form
  * */
  BuilderSearchForm()
  {
    this.SearchForm =this.fb.group({
      zone_type_code: [''],
      zone_type_name: ['']
    });
  }

  /*
  * Get GetZoneTypeList
  * */
  private  perPage=20;
	private GetZoneTypeList(page = null) {
		let param = "?page=" + page + "&limit=" + this.perPage;
        if(Object.keys(this.searchCriteria).length !== 0) // check if searchCriteria Object not empty  ( {} )
        {
            param += '&zone_type_code=' + this.searchCriteria['zone_type_code'] + '&zone_type_name='
                + this.searchCriteria['zone_type_name'];
        }
        this.Loading['getList'] = true;
        this.showLoadingOverlay = true;
        if(this.sortQuery != '') {
            param += '&'+this.sortQuery;
        }

		this._ZTP.GetZoneTypeList(param).subscribe(
			data => {

				this.ZoneTypeList = data.data;
                this.selectedAll = false;
                this.ListSelectedItem = [];
                //pagin function
                this.Pagination=data['meta']['pagination'];
                this.Pagination['numLinks']=3;
				this.Pagination['tmpPageCount']=this._Func.Pagination(this.Pagination);


				this.Loading['getList'] = false;
                this.showLoadingOverlay = false;
			},
			err => {},
			() => {}
		);
	}



	private filterList(num) {
		this.GetZoneTypeList(num);
        // this.
        //remove list selected and checkall
        this.selectedAll=false;
        this.ListSelectedItem=[];
        this.currentPage = num;
	}

  private onPageSizeChanged($event)
  {
    this.perPage = $event.target.value;
    if((this.Pagination['current_page'] - 1) * this.perPage >= this.Pagination['total']) {
      this.Pagination['current_page'] = 1;
    }
    this.GetZoneTypeList(this.Pagination['current_page']);
  }


	/*
	* Check All/Off
	* */
	private CheckAll(event) {
		if (event) {
			this.selectedAll = true;
		}
		else {
			this.selectedAll = false;
		}

    // set empty array  ListSelectedItem
    this.ListSelectedItem=[];
		// Loop
		this.ZoneTypeList.forEach((item) => {
			 item['selected'] = this.selectedAll;
       if(this.selectedAll)
       {
          // if check all = true , pushing to ListSelectedItem
          this.ListSelectedItem.push(item['zone_type_id']);
       }
		});
	}

	/*
	* Selected
	* */
	private checkSelectItem(item) {
    let id = item['zone_type_id'];
    item.selected = !item.selected;
    this.selectedAll = false;
    if (item.selected) {
        this.ListSelectedItem.push(id);
        this.SetItemListSelected(id,true);
          if(this.ListSelectedItem.length==this.ZoneTypeList.length)
          {
              this.selectedAll=true;
          }
      }
      else {
        let el = this.ListSelectedItem.indexOf(id);
        // remove item out of array
        this.ListSelectedItem.splice(el, 1);
        this.SetItemListSelected(id,false);

      }

	}

  /*
  * Set select item
  * */
  SetItemListSelected(id,value)
  {
    this.ZoneTypeList.forEach((item) => {
      if(item['zone_type_id']==id)
      {
        item['selected']=value;
      }
    });
  }

	/*
	* Router edit
	* */
	Edit() {
		if (this.ListSelectedItem.length > 1) {
      this.messages = this._Func.Messages('danger','Please choose only one item to edit');
		}
		else {
      if(this.ListSelectedItem.length)
      {
        this._router.navigateByUrl('/master-data/zone-type/' + this.ListSelectedItem[0] + '/edit');
      }
      else{
        this.messages = this._Func.Messages('danger','Please choose one item to edit .');
      }

		}
	}

  /*
   * Delete
   * */
  private Delete()
  {
      if(!this.ListSelectedItem.length)
      {
        // if no select any item to delete
        return;
      }

     if(confirm(this._Func.msg('PBAP002'))&&this.ListSelectedItem.length) {
       // remove item local
       this.RemoveItemLocal();

       this.ListSelectedItem.forEach((item)=> {
         // the fisrt , remove item in list data

         this._ZTP.DeleteZoneType(item).subscribe(
           data => {
             this.selectedAll = false;


             // reload data new
             if (this.ZoneTypeList.length == 0) {
               this.GetZoneTypeList(this.Pagination['current_page']);
             }

           },
           err => {},
           () => {}
         );

       });

       // set empty selected
       this.ListSelectedItem = [];
     }

  }

  RemoveItemLocal()
  {
    let sumItemDelet=0;

    this.ListSelectedItem.forEach((item)=>{

      for(let el of this.ZoneTypeList) {

        if (item == el['zone_type_id']) {
          // found item to delete
          let index = this.ZoneTypeList.indexOf(el);
          // remove item out of array
          this.ZoneTypeList.splice(index, 1);
          sumItemDelet++;

           break;
        }
        else{
        }
      }

    });

  }

  Search(data : Object):void
  {
    this.messages = null;
    this.ListSelectedItem=[];
    this.selectedAll=false;
    let param="?page="+1+"&limit="+this.perPage;
    this.searchCriteria = this._Func.genSearchCriteriaFromForm(this.SearchForm);
    let enCodeSearchQuery = param + "&zone_type_code=" + encodeURIComponent(this.searchCriteria['zone_type_code'])
                            +"&zone_type_name=" + encodeURIComponent(this.searchCriteria['zone_type_name']);
      if(this.sortQuery != '') {
          enCodeSearchQuery += '&'+this.sortQuery;
      }

    // let enCodeSearchQuery=this._Func.FixEncodeURI(new_query);
    //let new_query="?whs_code="+data['whs_code'];
    //whs_name[]=name1&whs_short_name[]=short_name&whs_status[]=ok

    this.Loading['searching']=true;
    this.showLoadingOverlay = true;

    this._ZTP.Search(enCodeSearchQuery).subscribe(
      data => {
        // disable loading icon
        this.Loading['searching']=false;
          this.showLoadingOverlay = false;

        this.ZoneTypeList = data.data;
        //pagin function
        this.Pagination=data['meta']['pagination'];
        this.Pagination['numLinks']=3;
        this.Pagination['tmpPageCount']=this._Func.Pagination(this.Pagination);

      },
      err =>{
        this.messages = this._Func.Messages('danger','Not found');
      },
      () => {}
    );

  }

  /*
   * Reset Form Builder
   * */
  ResetSearch()
  {
    (<Control>this.SearchForm.controls["zone_type_code"]).updateValue("");
    (<Control>this.SearchForm.controls["zone_type_name"]).updateValue("");
    this.searchCriteria = this._Func.genSearchCriteriaFromForm(this.SearchForm);
    this.GetZoneTypeList(this.Pagination['current_page']);
  }

    private FieldsToSortFlag = {
        zone_type_code: false,
        zone_type_name: false,
    };
    private countClick = {
        zone_type_code: 3,
        zone_type_name: 3,
    }
    private sortQuery = 'sort[zone_type_code]=asc';
    sort(field) {
        if(this.countClick[field] == 3) {
            this.countClick[field] = 0;
        }

        this.countClick[field] ++;
        if(this.countClick[field] == 3) {
            this.sortQuery = 'sort[zone_type_code]=asc';
        }
        else {
            this.FieldsToSortFlag[field] = !this.FieldsToSortFlag[field];

            this.sortQuery = 'sort['+field+']';
            if(this.FieldsToSortFlag[field]) {
                this.sortQuery += '=asc';
            } else {
                this.sortQuery += '=desc';
            }
        }

        if(field) {
            for (let sortField in this.FieldsToSortFlag ) {
                if(sortField != field) {
                    this.FieldsToSortFlag[sortField] = false;
                    this.countClick[sortField] = 3;
                }
            }
        }
        this.GetZoneTypeList(this.Pagination['current_page']);
    }

}
