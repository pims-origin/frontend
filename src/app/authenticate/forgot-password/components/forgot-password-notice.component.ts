import {Component, OnInit} from "@angular/core";
import { Router, RouterLink, ROUTER_DIRECTIVES } from '@angular/router-deprecated';
import { CORE_DIRECTIVES, FORM_DIRECTIVES } from '@angular/common';

@Component({
    selector: 'forgot-password-notice',
    directives: [CORE_DIRECTIVES, FORM_DIRECTIVES],
    templateUrl: '../views/forgot-password-notice.component.html',
    // styleUrls:['assets/css/page/forgot-password.css']


})
export class ForgotPasswordNoticeComponent{
    constructor(public router: Router){}
    goBack() {
    	this.router.parent.navigateByUrl('/login');
	}
}

