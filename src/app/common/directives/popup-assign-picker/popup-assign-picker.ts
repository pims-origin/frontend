import { Component, Input, Output, EventEmitter, Injectable } from '@angular/core';
import {
    WMSPagination, PaginationControlsCmp, PaginationService, PaginatePipe
} from '../../../common/directives/directives';
import { Functions } from '../../../common/core/load';
import { Services } from './services';
declare const jQuery: any;
@Injectable()
@Component({
    templateUrl: 'template.html',
    styleUrls: ['./style.css'],
    selector: 'assign-picker-popup',
    directives: [WMSPagination, PaginationControlsCmp],
    pipes: [PaginatePipe],
    providers: [Services]
})

export class PopupAssignPicker {
    public infoSearchUser = {};
    public oldDataCSRUser = [];
    public dataCSRUser = [];
    private showLoadingOverlay = false;
    private listWp = [];
    private type = {
        csr: 'odr_id',
        multiPicker: 'wv_dtl_ids',
        picker: 'wv_id'
    }
    @Input('typePopup') typePopup: string = '';
    @Input('urlGET') urlGET: string = '';
    @Input('urlAssign') urlAssign: string = '';

    @Input('listSelectedItemID') set listSelectedItemID(value) {
        if (value.length > 0) {
            this.infoSearchUser = {};
            this.listWp = value;
            this.assignPicker();
        }
    }
    @Output() messages = new EventEmitter();
    @Output() isAssigned = new EventEmitter();
    constructor(private _func: Functions,
        private service: Services) { }

    private assignPicker() {

        this.showLoadingOverlay = true;
        var tmp = localStorage.getItem('whs_id');
        this.service.getListCRSByCustomer(this.urlGET, tmp)
            .subscribe(
                data => {
                    if (data.data && data.data.length) {
                        jQuery('#assign-csr2').modal('show');
                        this.dataCSRUser = data.data;
                        this.oldDataCSRUser = data.data;
                    }
                    else {
                        this.showMessage('danger', this._func.msg('WPS02'));
                    }
                    this.showLoadingOverlay = false;
                },
                err => {

                    this.showMessage('danger', this._func.parseErrorMessageFromServer(err));
                    this.showLoadingOverlay = false;
                }
            );

    }

    selectCSR(item) {
        for (var i = 0, l = this.dataCSRUser.length; i < l; i++) {
            this.dataCSRUser[i].selected = false;
        }
        item.selected = true;
        this.assignUser = item;
    }

    private assignUser: any;
    saveAssignPutter() {
        const data = {
            'user_id': this.assignUser.user_id
        };
        data[this.type[this.typePopup]] = this.listWp;
        this.showLoadingOverlay = true;
        this.service.assignPicker(JSON.stringify(data), this.urlAssign)
            .subscribe(
                data => {
                    jQuery('#assign-csr').modal('hide');
                    if (this.typePopup !== 'csr') {
                        this.showMessage('success', this._func.msg('WP02322'));
                    }
                    this.isAssigned.emit(true);
                    this.showLoadingOverlay = false;
                },
                err => {
                    jQuery('#assign-csr').modal('hide');
                    this.showMessage('danger', this._func.parseErrorMessageFromServer(err));
                    this.showLoadingOverlay = false;
                }
            );
    }

    filterUser() {
        let data = this.oldDataCSRUser;
        if (this.infoSearchUser['first_name']) {
            data = this.oldDataCSRUser.filter(x => x.first_name.toLowerCase().includes(this.infoSearchUser['first_name'].toLowerCase()));
        }
        if (this.infoSearchUser['last_name']) {
            data = data.filter(x => x.last_name.toLowerCase().includes(this.infoSearchUser['last_name'].toLowerCase()));
        }
        if (this.infoSearchUser['email']) {
            data = data.filter(x => x.email.toLowerCase().includes(this.infoSearchUser['email'].toLowerCase()));
        }
        this.dataCSRUser = data;
    }

    resetFilterUser() {
        this.infoSearchUser['first_name'] = '';
        this.infoSearchUser['last_name'] = '';
        this.infoSearchUser['email'] = '';
        this.dataCSRUser = this.oldDataCSRUser;
    }

    private showMessage(msgStatus, msg) {
        this.messages.emit({
            'status': msgStatus,
            'txt': msg
        })
        jQuery(window).scrollTop(0);
    }

}