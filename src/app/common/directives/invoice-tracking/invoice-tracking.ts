import {Component, Input, Injectable} from '@angular/core';
import {API_Config} from '../../../common/core/API_Config';

@Component({
	selector: 'invoice-tracking',
	template: `<iframe height="350px" width="100%" [attr.src]="invoiceIFrameURL"></iframe>`
})

@Injectable()
export class InvoiceTracking {
	private invoiceIFrameURL:string = '';

	@Input('ref_num') set setRefNum(refNum:string) {
		if (this.invoiceIFrameURL.indexOf('&ref_num') > -1) {
			this.invoiceIFrameURL = this.invoiceIFrameURL.substring(0, this.invoiceIFrameURL.indexOf('&ref_num'));
		}
		this.invoiceIFrameURL += `&ref_num=${refNum}`;
	}

	constructor(private _API: API_Config,) {
		// this.invoiceIFrameURL = `${window.location.origin}/invoice-tracking?token=${localStorage.getItem('jwt')}`;
		this.invoiceIFrameURL = `${this._API.PAGE_URL}/invoice-tracking/index.html?token=${localStorage.getItem('jwt')}`;
	}
}
