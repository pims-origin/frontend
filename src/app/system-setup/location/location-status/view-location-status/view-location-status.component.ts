import { Component, Input, OnChanges, SimpleChange} from '@angular/core';
import { FORM_DIRECTIVES,ControlGroup,FormBuilder, Control ,Validators} from '@angular/common';
import { Http } from '@angular/http';
import { Router, RouterLink, ROUTER_DIRECTIVES } from '@angular/router-deprecated';
import 'ag-grid-enterprise/main';
import 'rxjs/add/operator/map';
import {API_Config} from "../../../../common/core/API_Config";
import {UserService} from "../../../../common/users/users.service";
import {Functions} from "../../../../common/core/functions";
import {DateFormat} from "../date.pipe";
import { JwtHelper } from 'angular2-jwt/angular2-jwt';
declare var jQuery: any;

@Component ({
    selector: 'view-location-status',
    pipes: [DateFormat],
    templateUrl: 'view-location-status.component.html',
})

export class ViewLocationStatusComponent {

    private headerGet: any;
    private headerPost: any;
    private headerPostJson: any;
    private urlAPILocation: any;
    private urlAPILocationType: any;
    private urlAPIZoneType: any;
    private urlWarehouse: any;
    private loc_whs_id: any;
    @Input('dataItemLocaView') dataItemLocaView: any;
    private dataStausLocation = {};
    private messages = {};
    private status = {
        'AC': 'Active',
        'IA': 'InActive',
        'LK': 'Locked',
        'RS': 'Reserved',

    };

    constructor (
        private _http: Http,
        private _router:Router,
        private _user: UserService,
        private _Func: Functions,
        private _API_Config: API_Config,
        public jwtHelper: JwtHelper
    ) {
        let that = this;
        this.loc_whs_id = localStorage.getItem('whs_id');
        this.headerGet = this._Func.AuthHeader();
        this.headerPost = this._Func.AuthHeaderPost();
        this.headerPostJson = this._Func.AuthHeaderPostJson();
        this.urlWarehouse = this._API_Config.API_Warehouse;
        this.urlAPILocation = this._API_Config.API_Warehouse + "/" + this.loc_whs_id + "/locations";
        this.urlAPILocationType = this._API_Config.API_Location_type;
        this.urlAPIZoneType = this._API_Config.API_Zone_Type;
        jQuery('body').on('viewLocationStatus', function(){
            if (that.dataItemLocaView.loc_id != undefined)
                that.getLocaStatus();
        });
    }

    ngOnChanges(changes: {[propKey: string]: SimpleChange}) {
        // console.log('change');
        // if (this.dataItemLocaView.loc_id != undefined)
        //     this.getLocaStatus();

    }

    private getLocaStatus () {

        this.dataStausLocation = {};
        this._http.get(this.urlWarehouse + "/" + this.loc_whs_id + "/locations/" + this.dataItemLocaView.loc_id + "/change-status", {headers: this.headerGet})
            .map(res => res.json())
            .subscribe(
                data => {

                    this.dataStausLocation = data.data;

                },
                err => {

                    this.parseError(err);

                },
                () => {}
            );
    }

    private parseError (err) {

        this.messages = this._Func.Messages('danger', this._Func.msg('VR100'));

    }

}
