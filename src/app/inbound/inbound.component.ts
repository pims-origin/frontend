import {Component} from '@angular/core';
import {RouteConfig, ROUTER_DIRECTIVES} from '@angular/router-deprecated';
import {InboundASNComponent} from "./asn/asn.component";

@Component ({
    selector: 'inbound',
    directives: [ROUTER_DIRECTIVES],
    template: `<router-outlet></router-outlet>`
})

@RouteConfig([
    { path: 'asn/...', component:InboundASNComponent , name: 'InBound ASN', data: {page: 'ASN'} },
])
export class InboundComponent {

}
