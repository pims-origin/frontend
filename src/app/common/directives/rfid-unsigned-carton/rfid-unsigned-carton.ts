import {Component, Input, Output, EventEmitter} from '@angular/core';

import {WMSPagination, AdvanceTable} from '../directives'
import {Functions} from "../../../common/core/functions";
import {Http, Response} from '@angular/http';
import {API_Config} from "../../../common/core/API_Config";

declare var $:any;

@Component({
	selector: 'rfid-unsigned-carton',
	templateUrl: 'rfid-unsigned-carton.html',
	directives: [WMSPagination, AdvanceTable]
})

export class RFIDUnsignedCarton {

	private data = [];
	private _flag;
	private messages;
	private Pagination={};
	private perPage=20;
	private currentPage = 1;
	private numLinks = 3;
	private _wv_id;

	@Output() bindMessages  = new EventEmitter();
	@Output() autoAssign  = new EventEmitter();
	@Output() warning  = new EventEmitter();
	@Input('wv_id') set wv_id(value:any) {
		this._wv_id = value;
	}

	@Input('rfid') set rfid(value:any) {
		this._flag = value;
		this._getRFIDUnsignedCarton();
	}

	constructor(
		private _Func:Functions,
		private _http:Http,
		private _api_Config: API_Config
	) {
	}
	private _getRFIDUnsignedCarton (page = 1) {

		var param = "?rfid=" + this._flag;
		param += "&page=" + page + "&limit=" + this.perPage + "&wv_id=" + this._wv_id;
		try {

			this.getRFIDUnsignedCarton(param)
				.subscribe(
					data => {
						this.data = data.data;
						this.Pagination = data['meta']['pagination'];
						this.Pagination['numLinks'] = 3;
						this.Pagination['tmpPageCount'] = this._Func.Pagination(this.Pagination);
						this.autoAssign.emit(this.data.length && this._flag === '1');
						this.warning.emit(this.data && this.data.length > 0);
					},
					err => {
						this.parseError(err);
					},
					() => {}
				);

		} catch (err) {

			this.bindMessages.emit(this._Func.Messages('danger', this._Func.msg('VR100')));
		}

	}


	public getRFIDUnsignedCarton($params) {

		return this._http.get(this._api_Config.API_ORDERS+'/cartons-not-assign'+ $params,{ headers: this._Func.AuthHeader() })
			.map(res => res.json());

	}

	// Event when user filter form
	private onPageSizeChanged ($event,el) {

		this.perPage = $event.target.value;

		if((this.Pagination['current_page'] - 1) * this.perPage >= this.Pagination['total']) {
			this.currentPage = 1;
		}
		else {
			this.currentPage = this.Pagination['current_page'];
		}
		this._getRFIDUnsignedCarton(this.currentPage);
	}

	// Show error when server die or else
	private parseError (err) {

		this.bindMessages.emit(this._Func.Messages('danger', this._Func.parseErrorMessageFromServer(err)));
	}
}