import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {API_Config, Functions} from '../../../common/core/load';

@Injectable()
export class OrderOutBoundPalletServices {
    private authHeader:any = this.funcs.AuthHeader();
    private authHeaderPostJson:any = this.funcs.AuthHeaderPostJson();
    private outboundPalletAPIURL:string = '';

    constructor(private funcs:Functions,
                private _api:API_Config,
                private http:Http) {
        const whsId = localStorage.getItem('whs_id');
        this.outboundPalletAPIURL = `${this._api.API_ORDERS_CORE}/${whsId}/out-pallet`;
    }

    getOutboundPallets(ordHdrId:string = '') {
        return this.http.get(`${this.outboundPalletAPIURL}/${ordHdrId}`, {headers: this.authHeader})
            .map(res => res.json().data);
    }

    getLocations(params:string = '') {
        return this.http.get(`${this.outboundPalletAPIURL}/auto-location${params}`, {headers: this.authHeader})
            .map(res => res.json().data);
    }

    updateOutboundPallets(data) {
        return this.http.put(`${this.outboundPalletAPIURL}/edit`, data, {headers: this.authHeaderPostJson})
            .map((res: Response) => res.json().data);
    }
}
