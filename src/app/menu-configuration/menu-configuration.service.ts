import {Component, Injectable} from '@angular/core';
import {Http, Response, JSONP_PROVIDERS, Jsonp} from '@angular/http';
import {API_Config, Functions} from '../common/core/load';

@Component({
    providers: [JSONP_PROVIDERS]
})

@Injectable()
export class MenuConfigurationServices {

  public ListMenu;
  public GroupMenu;
  public AuthHeader = this._Func.AuthHeader();
  public AuthHeaderPost = this._Func.AuthHeaderPost();
  public loading:boolean=false;

  constructor(private _Func: Functions, private _API: API_Config, private http: Http)
  {
  }

  ngOnInit() {


  }

  /*
    Get Group Menu
  */
  getGroupMenu()
  {
    return this.http.get(this._API.API_GroupMenu,{ headers: this.AuthHeader })
    //return this.http.get(this._API.API_GroupMenu)
    .map((res: Response) => res.json().data);  
  }

  /*
  Get List Menu Data
  */
  GetListMenu(id_group)
  {
    //, { headers: this.AuthHeader }
    //return this.http.get(this._API.API_GroupMenu + id_group, { headers: this.AuthHeader })
    // console.log("__menuconfiguration.service idmenu group >>" + id_group)
    return this.http.get(this._API.API_ListMenu+id_group, { headers: this.AuthHeader })
    .map((res: Response) => res.json().data);
  }

  UpdateMenu(menu_group_id,data)
  {
      // console.log("_config_service" + this._API.API_ListMenu + menu_group_id);
      // console.log(data);  

      return this.http.post(this._API.API_ListMenu + menu_group_id, data, { headers: this.AuthHeaderPost });

  }

  UpdateMenuGroup(menu_group_id,data)
  {
      return this.http.post(this._API.API_GroupMenu + menu_group_id, data, { headers: this.AuthHeaderPost })
      .map((res: Response) => res.json().data);  
  }

  CreateNewMenuGroup(param)
  {
    // using API_GroupMenu to create new menu group
    return this.http.post(this._API.API_GroupMenu,param, { headers:this.AuthHeaderPost })
      .map((res: Response) => res.json().data); 
  }

  DeleteGroup(menu_group_id) {
    // using API_GroupMenu to create new menu group
    return this.http.delete(this._API.API_GroupMenu + "/" + menu_group_id, { headers: this.AuthHeaderPost })
      .map((res: Response) => res.json().data);
  }

}
