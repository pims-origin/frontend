import {Component, Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {API_Config, Functions} from '../../common/core/load';

@Injectable()
export class ChargeCodeService {
    public AuthHeader = this._Func.AuthHeader();
    public AuthHeaderPost = this._Func.AuthHeaderPost();

    constructor(private _Func: Functions, private _API: API_Config, private http: Http)
    {

    }

    ChargeCode () {
        return this.http.get(this._API.API_CHARGE_CODE + '?limit=1000',{ headers: this.AuthHeader })
            .map((res: Response) => res.json());
    }

    getChargeCode(params) {
        return this.http.get(this._API.API_CHARGE_CODE + params,{ headers: this.AuthHeader })
            .map((res: Response) => res.json());
    }

    getChargeCodeDetail(id) {
        return this.http.get(this._API.API_CHARGE_CODE+'/'+id, { headers: this.AuthHeader })
            .map((res: Response) => res.json().data);
    }

    addNewChgCode(data) {
        return this.http.post(this._API.API_CHARGE_CODE, data, { headers: this._Func.AuthHeaderPostJson() })
            .map((res: Response) => res.json().data);
    }

    updateChgCode(id, data) {
        return this.http.put(this._API.API_CHARGE_CODE+'/'+id, data, { headers: this._Func.AuthHeaderPostJson() })
            .map((res: Response) => res.json().data);
    }


    search(param) {
        return this.http.get(this._API.API_CHARGE_CODE+param,{ headers: this.AuthHeader })
            .map((res: Response) => res.json());
    }

    //**************************************************************************

    getDropdown()
    {
        let params = '?sort[chg_code_name]=asc';

        return this.http.get(this._API.API_Charge_Codes + '/dropdown' + params, {
            headers: this.AuthHeader
        })
        .map((res: Response) => res.json());
    }

    //**************************************************************************

}