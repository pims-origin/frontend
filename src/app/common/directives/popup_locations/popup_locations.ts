import { Component, Input, Output, EventEmitter, Injectable } from '@angular/core';
import { 
    WMSPagination,PaginationControlsCmp, PaginationService, PaginatePipe
} from '../../../common/directives/directives';
import { BoxPopupService } from '../../../common/popup/box-popup.service';
import { Functions, FormBuilderFunctions } from '../../../common/core/load';
import { Services } from './services';
import { CheckedListPagingFunctions } from './checked-functions';
import { SearchPipe } from './search.pipe';
import { WarningBoxPopup } from "../../../common/popup/warning-box-popup";
declare const jQuery: any;
@Injectable()
@Component({
    templateUrl: 'template.html',
    styleUrls: ['./style.css'],
    selector: 'locations-popup',
    directives: [WMSPagination, PaginationControlsCmp],
    pipes: [SearchPipe, PaginatePipe],
    providers: [Services, CheckedListPagingFunctions, BoxPopupService, PaginationService]
})

export class PopupLocations {
    private is_page: string = '';
    private messages: any;
    
    @Input('data_used') data_used: any[] = [];
    @Input('style') style = 'default';
    @Input('isdisabled') disabled: boolean = false;
    @Input('isPage') set isPage(value) {
        if (value) {
            this.is_page = value;
        }
    }
    @Output() selectedLoc = new EventEmitter();
    
    private selectedItemList: any[] = [];
    
    Pagination: any;
    perPage = 20;

    locationCodeFilter = '';
    aisleFilter = '';
    levelFilter = '';
    rowFilter = '';
    locationList = {};
    action = 'add';
    isLoading: any[] = [];
    
    constructor(private _Func: Functions,
                private checkedListPaging: CheckedListPagingFunctions,
                private service: Services,
                private _boxPopupService: BoxPopupService,
                private fbdFunc: FormBuilderFunctions) {}


    private showPopupLocForm(array: any[] = [], action = 'add') {
        this.action = action;  

        if (this.disabled) return;

        if (action === 'view') {
            this.locationList['data'] = this.data_used;

        } else if(action === 'add') {
            this.getLocation();
        }

        jQuery('#locations_popup').modal('show');
    }

    private loading: any;
    private getLocation(page = 1) {
        this.loading = true;
        let params = `?page=${page}&limit=${this.perPage}&status=AC&loc_type_code=rac`;
        params += `&loc_code=${this.locationCodeFilter}&aisle=${this.aisleFilter}&row=${this.rowFilter}&level=${this.levelFilter}`;
        
        if (this.is_page == 'CC') {
            params += '&has_pallet=1';
        }

        if (this.sortQuery !== '') {
            params += '&' + this.sortQuery;
        }

        let whs_id = localStorage.getItem('whs_id');
        this.service.getLocationByWH(whs_id,params).subscribe(
            data => {
                this.loading = false;
                this.buildList(data);

                // pagin function
                if (typeof  data['meta'] !== 'undefined') {
                    this.Pagination = data['meta']['pagination'];
                    this.Pagination['tmpPageCount'] = this._Func.Pagination(this.Pagination);
                }

            }, 
            err => {
                this.parseError(err);
                this.loading = false;
            }, 
            () => {}
        );

    }

    Selected(state, paginId) {
        if (this.action === 'view') {
            this.checkCheckAll(state, paginId);
        }
    }

    private selectedAllPage: any;

    checkCheckAll(StatePagin, objName) {
        setTimeout(() => {
            if (StatePagin) {
                this.selectedAllPage = this.checkedListPaging.checkCheckAll(StatePagin, objName, this.locationList['data']);
            }
        });
    }

    private CheckAll(checked, StatePagin, objName) {
        this.checkedListPaging.checkAll(checked, StatePagin, objName, this.locationList['data']);
    }

    private checkedItem(item, $event) {
        this._Func.checkedItem(this.locationList, item, $event);
    }

    private checkedAll($event) {
        this._Func.checkedAll(this.locationList, $event);
    }

    private buildList(data) {
        this.locationList = data;
        // this.selectedItemList = [];
        //
        // this.data_used.forEach((item) => {
        //     this.selectedItemList.push(item['loc_id']);
        // });
        //
        // this.locationList['data'] = data['data'].filter((loc) => {
        //
        //     if (this.selectedItemList.indexOf(loc['loc_id']) !== -1) {
        //         loc['selected'] = true;
        //     }
        //     return true;
        //
        // });
    }

    private addLocationToLocationFrom() {

        let _selectedLoc: any[] = [];
        let tmp: any[] = JSON.parse(JSON.stringify(this.locationList['data']));
        tmp.forEach((item) => {
            if (item['selected']) {
                item['selected'] = false;
                _selectedLoc.push(item);
            }
        });

        if (_selectedLoc.length) {
            this.selectedLoc.emit(_selectedLoc);
        }

    }

    private _un_selected_item(item_id: any) {

        for (let item of this.locationList['data']) {
            if (item['loc_id'] == item_id) {
                item['selected'] = false;
                return;
            }
        }

    }

    /*
    * Delete Function
    **/
    private messagesRemove: any = false;
    Delete(ListName: any[] = [], PaginId) {

        this.messagesRemove = false;
        let Arr = ListName;
        let ArrTemp = Arr.slice();
        
        let f = 0;
        for (let i = 0; i < Arr.length; i++) {
            if (Arr[i]['selected'] === true) {
                f++;
                break;
            }
        }
        if (f === 0) {
            this.messagesRemove = this._Func.Messages('danger', this._Func.msg('VR113'));
        } else {
    
            let n = this;
            let warningPopup = new WarningBoxPopup();
            warningPopup.text = this._Func.msg('VR115');
            this._boxPopupService.showWarningPopup(warningPopup).then(function(yes) {
                if (!yes) {
                    return;
                } else {
                    let i = 0;
                    ArrTemp.forEach((item) => {
                        if (item['selected'] === true) {
                            const el = Arr.indexOf(item);
                            const loc = Arr.splice(el, 1);
                        }
                        i++;
                    });
                    n.selectedLoc.emit(n.locationList['data']);
                }
            });
        }
    }


    // Show error when server die or else
    private parseError(err) {
        this.messages = {'status': 'danger', 'txt': this._Func.parseErrorMessageFromServer(err)};
    }

    private setClickedRow(item) {
        item.selected = !item.selected;
    }

    private onPageSizeChanged($event: any) {
        this.perPage = $event.target.value;
		if (this.Pagination && this.action === 'add') {
			if ((this.Pagination['current_page'] - 1) * this.perPage >= this.Pagination['total']) {
				this.Pagination['current_page'] = 1;
			}
			this.getLocation(this.Pagination['current_page']);
		}

    }

    FieldsToSortFlag = {
        loc_code: false
    };
    countClick = {
        loc_code: 3
    };

    sortQuery = 'sort[loc_code]=asc';

    sort(field) {
        let sortType: string;
        if (this.countClick[field] === 3) {
            this.countClick[field] = 0;
        }

        this.countClick[field]++;
        if (this.countClick[field] === 3) {
            sortType = 'asc';
        } else {
            this.FieldsToSortFlag[field] = !this.FieldsToSortFlag[field];
            sortType = this.FieldsToSortFlag[field] ? 'asc' : 'desc';
        }

        this.sortQuery = 'sort[' + field + ']=' + sortType;

        if (field) {
            for (const sortField in this.FieldsToSortFlag) {
                if (sortField !== field) {
                    this.FieldsToSortFlag[sortField] = false;
                    this.countClick[sortField] = 3;
                }
            }
        }

        if (this.action === 'add') { // sort by server
            this.getLocation(this.Pagination['current_page']);
        } else { // sort by client
            if (!this.locationList['data']) {
                return;
            }
            
            this.locationList['data'].sort(function(a, b) {
                if (sortType === 'asc') {
                    if (a[field].toLowerCase() > b[field].toLowerCase()) {
                        return 1;
                    }
                    if (a[field].toLowerCase() < b[field].toLowerCase()) {
                        return -1;
                    }
                    return 0;
                }

                if (sortType === 'desc') {
                    if (a[field].toLowerCase() > b[field].toLowerCase()) {
                        return -1;
                    }
                    if (a[field].toLowerCase() < b[field].toLowerCase()) {
                        return 1;
                    }
                    return 0;
                }
            });
        }
    }
    
    // for Autocomplete aisles
    autocompleteData: any = {};
	
	autocompleteLocation($event:any, fieldSearch: string, field: string) {
        const key = $event.target.value.trim();
		const params = `/${fieldSearch}?page=1&limit=10&${field}=${key}`;
		const enCodeSearchQuery = this._Func.FixEncodeURI(params);

		if (!key) {
			return;
		}
		this.isLoading[field] = true;
		this.service.getAutocompleteLocation(enCodeSearchQuery).debounceTime(400).distinctUntilChanged().subscribe(
			data => {
				this.isLoading[field] = false;
				this.autocompleteData[field] = data['data'];
			},
			err => {
				this.isLoading[field] = false;
				this.messages = this._Func.Messages('danger', err.json().errors.message);
			},
			() => {
			}
		);
	}
	
	selectItem(field: string, item: any) {
        switch(field) {
            case 'aisle': 
                this.aisleFilter = item[field];
            break;
            case 'row': 
                this.rowFilter = item[field];
            break;
            default: 
                this.levelFilter = item[field];
        }
        this.getLocation();
    }
    
    reset() {
        this.locationCodeFilter = '';
        this.aisleFilter = '';
        this.levelFilter = '';
        this.rowFilter = '';
        this.getLocation();
    }

    private timeout = 1000;
    private autocompleteForLocation() {
        if (this.loading) {
            return;
        }
        this.loading = true;
        setTimeout(() => {
            const locationLast = JSON.parse(JSON.stringify(this.locationCodeFilter));
            let params = `?page=1&limit=${this.perPage}&status=AC&loc_type_code=rac`;
            params += `&loc_code=${this.locationCodeFilter}&aisle=${this.aisleFilter}&row=${this.rowFilter}&level=${this.levelFilter}`;
            
            if (this.is_page == 'CC') {
                params += '&has_pallet=1';
            }
    
            if (this.sortQuery !== '') {
                params += '&' + this.sortQuery;
            }
            this.service.getLocationByWH(this._Func.getWareHouseId(),params).subscribe(
                async data => {
                    this.locationList = data;
                    this.loading = false;
                    if (this.locationCodeFilter !== locationLast) {
                        this.autocompleteForLocation();
                    }
                    // pagin function
                    if (typeof  data['meta'] !== 'undefined') {
                        this.Pagination = data['meta']['pagination'];
                        this.Pagination['tmpPageCount'] = this._Func.Pagination(this.Pagination);
                    }
    
                }, 
                err => {
                    this.parseError(err);
                    this.loading = false;
                }, 
                () => {}
            );

        }, this.timeout);
    }
}