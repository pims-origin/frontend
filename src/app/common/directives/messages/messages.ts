import {Component, ViewChild, Input, Output, EventEmitter, ChangeDetectionStrategy, ChangeDetectorRef} from '@angular/core'
import {Subscription} from 'rxjs';
import {DEFAULT_TEMPLATE} from './template';

@Component({
  selector: 'wms-messages',
  template: DEFAULT_TEMPLATE,
})
export class WMSMessages {

  private _timeout:number;
  private timeout_default:number=4000;
  private _messages;

  @Input('messages') set messages(value:any) {
    this._messages=value;
    let timeout=this._timeout;
    if(typeof  timeout=='undefined'){
      timeout=this.timeout_default;
    }
    /*let n=this;
    setTimeout(function() {
      n._messages=false;
    }, timeout);*/
  }
  @Input('timeout') set timeout(value:number) {
    this._timeout=value;
  }

  constructor() {}
}
