import {Component} from '@angular/core';
import {API_Config} from "../../../common/core/API_Config";
import {UserService} from "../../../common/users/users.service";
import {Functions} from "../../../common/core/functions";
import {Http,Response} from "@angular/http";
import { Router,RouteData, RouteParams } from '@angular/router-deprecated';
import { FORM_DIRECTIVES,ControlGroup,FormBuilder, Control ,Validators} from '@angular/common';
import {WarningBoxPopup} from "../../../common/popup/warning-box-popup";
import {BoxPopupService} from "../../../common/popup/box-popup.service";
import {ErrorMessageForControlService} from "../../../common/services/error-message-for-control.service";
import { OrderBy } from "../../../common/pipes/order-by.pipe";
declare var jQuery: any;
import {AutoCompleteComponent} from "../../../common/component/inputAutocomplete.component";
import {search} from "./locationCode.pipe";
import { ValidationService} from "../../../common/core/validator";
import {GoBackComponent} from "../../../common/component/goBack.component";
import {Services} from "./services";
import {WMSPagination,PaginatePipe,WMSMessages,AddPopExFunction, AddPopupLocations,ProGressBarDirective,PaginationControlsCmp, PaginationService,WMSBreadcrumb} from '../../../common/directives/directives';
import {FormBuilderFunctions} from "../../../common/core/formbuilder.functions";
@Component ({
    selector: 'add-zone',
    providers: [ValidationService,Services,AddPopExFunction,PaginationService,BoxPopupService, ErrorMessageForControlService],
    pipes: [search,PaginatePipe, OrderBy],
    directives: [PaginationControlsCmp,WMSPagination,
        AddPopupLocations,WMSMessages,
        ProGressBarDirective,GoBackComponent,AutoCompleteComponent,WMSBreadcrumb, FORM_DIRECTIVES],
    templateUrl: 'cru-zone.component.html',
    styles: [`
    .red-border {
    border:1px solid red;
    }
  `],
})
export class CruZoneComponent {
    ZoneForm: ControlGroup;
    private addedLocations: any = [];
    private authHeader = this._Func.AuthHeader();
    private messages;
    private Loading={};
    private warehouseName: any;
    private warehouse_id: string = localStorage.getItem('whs_id');
    private authHeaderPost = this._Func.AuthHeaderPost();
    private zoneTypeList: any = [];
    private zoneId: any;
    private url;
    public ListSelectedItem:Array<any>=[];
    public selectedAll={};
    public checkAll: boolean=false;
    private showError:boolean=false;
    private Action;
    private TitlePage;
    private IsView:boolean=false;
    private zoneData: any;
    public AuthHeader = this._Func.AuthHeader();
    private  perPage=20;
    private showLoadingOverlay:boolean=false;
    private allowAccess:boolean=false;
    private Pagination={};
    private getZoneListrPogress=0;
    private submited:boolean = false;
    private defaultZoneColor = "#fff";
    private isFixedZone:boolean = false;

    constructor(
        private  fb: FormBuilder,
        private params: RouteParams,
        private _http: Http,
        private _router:Router,
        private _user: UserService,
        private services:Services,
        private _valid: ValidationService,
        private _boxPopupService: BoxPopupService,
        private _ErrMsgControlService: ErrorMessageForControlService,
        private _Valid: ValidationService,
        private _Func: Functions,
        private add_pop_sv:AddPopExFunction,
        private fbFunc:FormBuilderFunctions,
        private _API_Config: API_Config,
        private _RTAction: RouteData) {

        this._ErrMsgControlService.errorMsg.invalidSpace = 'is required.';  // set error message for invalid space
        this.Action=this._RTAction.get('Action');

        if(this.Action=='New'){
            this.defaultZoneColor =  this.radomHex();
        }

        this.checkPermission();
    }

    private radomHex(){
       return  '#'+Math.floor(Math.random()*16777215).toString(16);
    }

    private FormBuilder()
    {
        this.ZoneForm = this.fb.group({
            zone_code: ['', Validators.compose([Validators.required, this._Valid.validateSpace, this._Valid.invalidLength3])],
            zone_name: ['', Validators.compose([Validators.required, this._Valid.validateSpace, this._Valid.invalidNameStr])],
            zone_type_id: ['', Validators.required],
            zone_min_count: [null,this._valid.validateInt],
            zone_max_count: [null,this._valid.validateInt],
            customer_name: [null],
            zone_description: [''],
            zone_color:[this.defaultZoneColor, Validators.compose([Validators.required])],
            zone_num_of_active_loc: [''],
            zone_num_of_inactive_loc: ['']
        }); //,{validator: numberCompare('zone_min_count', 'zone_max_count')}

        setTimeout(()=>{
            this.initColorPicker();
        },500)
    }

    private createZone;
    private viewZone;
    private editZone;
    private checkPermission() {
        this.showLoadingOverlay = true;
        this._user.GetPermissionUser().subscribe(
            data => {
                this.createZone = this._user.RequestPermission(data, 'createZone');
                this.viewZone = this._user.RequestPermission(data,'viewZone');
                this.editZone=this._user.RequestPermission(data,'editZone');
                /* Check orther permission if View allow */
                if(!this.viewZone) {
                    this._router.parent.navigateByUrl('/deny');
                }
                else {

                    this.getWarehouseName();
                    this.getZoneType();
                    this.FormBuilder();
                    this.url = this.params.get("whs_id");

                    if(this.Action=='View')
                    {
                        this.IsView=true;
                        this.TitlePage='View Zone';
                        if(!this.viewZone){
                            this.redirectDeny();
                        }
                        else{
                            this.allowAccess=true;
                        }
                    }

                    if(this.Action=='New')
                    {
                        this.TitlePage='Add Zone';
                        if(!this.createZone){
                            this.redirectDeny();
                        }
                        else{
                            this.allowAccess=true;
                        }
                    }

                    if(this.Action=='Edit')
                    {
                        this.TitlePage='Edit Zone';
                        if(!this.editZone){
                            this.redirectDeny();
                        }
                        else{
                            this.allowAccess=true;
                        }
                    }

                    if(this.Action=='Edit'||this.Action=='View')
                    {
                        this.zoneId=this.params.get('id');
                        this.getLocationByZone();
                        this.getZoneDetail(this.zoneId);
                    }
                }
            },
            err => {
                this.messages=this._Func.Messages('danger', this._Func.parseErrorMessageFromServer(err));
                this.showLoadingOverlay = false;
            }
        );
    }
    redirectDeny(){
        this._router.parent.navigateByUrl('/deny');
    }

    private getZoneDetail(zoneId:any){

        this._http.get(this._API_Config.API_Warehouse + '/' + this.warehouse_id + '/zone/' +  zoneId, {
                headers: this.authHeader
            })
            .map(res => res.json().data)
            .subscribe(
                data => {
                    this.showLoadingOverlay = false;
                    this.zoneData = data;
                    this.isFixedZone = ['SHP', 'XDK-D'].indexOf(this.zoneData['zone_code']) !== -1;
                    (<Control>this.ZoneForm.controls['zone_code']).updateValue(data['zone_code']);
                    (<Control>this.ZoneForm.controls['zone_name']).updateValue(data['zone_name']);
                    (<Control>this.ZoneForm.controls['zone_type_id']).updateValue(data['zone_type_id']);
                    (<Control>this.ZoneForm.controls['zone_min_count']).updateValue(data['zone_min_count']);
                    (<Control>this.ZoneForm.controls['zone_max_count']).updateValue(data['zone_max_count']);
                    (<Control>this.ZoneForm.controls['zone_description']).updateValue(data['zone_description']);
                    (<Control>this.ZoneForm.controls['zone_num_of_active_loc']).updateValue(data['zone_num_of_loc']);
                    (<Control>this.ZoneForm.controls['zone_num_of_inactive_loc']).updateValue(data['zone_num_of_inactive_loc']);
                    this.initColorPicker(data['zone_color']);
                },
                err => {
                    this.showLoadingOverlay = false;
                },
                () => {}
            )


    }


    private initColorPicker(color=''){

        let that=this;

        jQuery('.xcolorpicker').colorpicker({format: 'hex'}).on('changeColor', function(e) {
            that.fbFunc.setValueControl(that.ZoneForm,'zone_color',e.color.toString('hex'));
        });

        if(color){
            jQuery('.xcolorpicker').colorpicker('setValue', color);
        }

    }
    // Get Warehouse Name
    private getWarehouseName() {
        this.messages = false;
        this._http.get(this._API_Config.API_Warehouse + '/' + this.warehouse_id, {
                headers: this.authHeader
            })
            .map(res => res.json())
            .subscribe(
                data => {
                    this.warehouseName = data.whs_name;
                    //this.warehouseFilter(data);
                },
                err => {

                    if (this.Action=='New') {

                        this.showLoadingOverlay = false;
                    }
                },
                () => {

                    if (this.Action=='New') {

                        this.showLoadingOverlay = false;
                    }
                }
            );
    }
    // Get zone type
    private getZoneType() {
        this._http.get(this._API_Config.API_Zone_Type + '?limit=1000', {
                headers: this.authHeader
            })
            .map(res => res.json())
            .subscribe(
                data => {
                    this.zoneTypeList = data.data;
                },
                err => {
                    if (this.Action=='New') {

                        this.showLoadingOverlay = false;
                    }
                },
                () => {

                    if (this.Action=='New') {

                        this.showLoadingOverlay = false;
                    }
                }
            );
    }
    private beforeSaveZone(){
        this.showError=true;

        jQuery('html, body').animate({
            scrollTop: 0
        }, 700);
        this._ErrMsgControlService._genErrMsgForMultiControls(this.ZoneForm);
        this.submited = true;
        /*
         * Only Form valid
         * */
        if(this.ZoneForm.valid&&this.checkVaildMinCountMaxCount())
        {
            if(!this.addedLocations.length && this.Action!=='New'){
                let n = this;
                /*case 1: modify text*/
                let warningPopup = new WarningBoxPopup();
                warningPopup.text = this._Func.msg('BZ002');
                this._boxPopupService.showWarningPopup(warningPopup)
                    .then(function (dm) {
                        if(dm)
                            n.saveZone();
                    });
            }else{
                this.saveZone();
            }
        }
        else{
        }
    }

    checkVaildMinCountMaxCount()
    {
        if(this.ZoneForm.value['zone_min_count']&&this.ZoneForm.value['zone_max_count'])
        {
            if(parseInt(this.ZoneForm.value['zone_min_count'])>=parseInt(this.ZoneForm.value['zone_max_count']))
            {
                this.messages = this._Func.Messages('danger', this._Func.msg('VR013'));
                return false;
            }
        }
        return true;
    }

    private saveZone() {

        this.showError=true;
        let n=this;
        var params = jQuery("form").serialize();
        if(this.Action=='New')
        {
            this.addNew(params);
        }
        else{
            // edit
            this.Edit(params);
        }

    }

    private Edit(params){
        let n=this;
        this.showLoadingOverlay=true;
        this._http.put(this._API_Config.API_Warehouse + '/' + this.warehouse_id + '/zone/'+this.zoneId,
            params , {
                headers: this.authHeaderPost
            })
            .map(res => res.json().data)
            .subscribe(
                data => {
                    this.showLoadingOverlay=false;
                    this.messages = this._Func.Messages('success', this._Func.msg('BZ006'));
                    setTimeout(function(){
                        n._router.parent.navigateByUrl('/system-setup/zone');
                    }, 1100);
                },
                err => {
                    this.parseError(err);
                },
                () => {}
            );
    }

    private addNew(params){
        this.showLoadingOverlay=true;
        this._http.post(this._API_Config.API_Warehouse + '/' + this.warehouse_id + '/zone',
            params , {
                headers: this.authHeaderPost
            })
            .map(res => res.json().data)
            .subscribe(
                data => {
                    this.showLoadingOverlay=false;
                    this.messages = this._Func.Messages('success', this._Func.msg('BZ001'));
                    setTimeout(()=>{
                        this._router.parent.navigateByUrl('/system-setup/zone/'+data['zone_id']+'/edit');
                    }, 3000);
                },
                err => {
                    this.parseError(err);
                },
                () => {}
            );
    }

    private onPageSizeChanged($event)
    {
        this.perPage = parseInt($event.target.value);
        this.getLocationByZone();
    }
    /*=====================================
     * getLocationByZone
     * ====================================*/
    private addedLocationsList={};
    private loc_code='';
    private getLocationByZone(page=1){
        // to show progressbar
        this.getZoneListrPogress=1;
        let params = "?page="+page+"&limit="+this.perPage+'&loc_code='+this.loc_code;
        let wh_id = this.warehouse_id;
        let zone_id = this.zoneId;
        this.Loading['addedLocations']=true;
        this.getLocation(wh_id,zone_id,params).subscribe(
            data => {

                this.addedLocationsList = data;
                this.Loading['addedLocations']=false;
                this.addedLocations=data.data;
                //pagin function
                this.Pagination=data['meta']['pagination'];
                this.Pagination['tmpPageCount']=this._Func.Pagination(this.Pagination);

            },
            err =>{
                this.Loading['addedLocations']=false;
            },
            () => {}
        )


    }
    
    private resetSearchLoc(){
        this.loc_code='';
        this.getLocationByZone();
    }

    public getLocation(wh_id,zone_id,param)
    {
        return this._http.get(this._API_Config.API_Warehouse+'/' + wh_id + '/zone/' + zone_id + '/locations' + param,{
                headers: this.AuthHeader })
            .map((res: Response) => res.json());
    }


    checkInputNumber(event)
    {
        this._valid.isNumber(event);
    }

    private Cancel()
    {
        let n = this;
        this._boxPopupService.showWarningPopup()
            .then(function (dm) {
                if(dm)
                    n._router.parent.navigateByUrl('/system-setup/zone/');
            })
    }

    private checkedAll($event){
        this._Func.checkedAll(this.addedLocationsList,$event);
    }

    private checkedItem(item,$event) {

        if(typeof item['has_item']=='undefined' || item['has_item']==0) {
            this._Func.checkedItem(this.addedLocationsList,item,$event);
        }
        else{

            item['errors']=true;
            let n = this;
            let warningPopup = new WarningBoxPopup();
            warningPopup.text = "Location cannot be removed because it has inventories!";
            warningPopup.showCancelButton=false;
            warningPopup.confirmButtonText='Close';
            this._boxPopupService.showWarningPopup(warningPopup)
                .then(function (dm) {
                    if(dm||!dm)
                    {
                        // has item
                        $event.target.checked=false;
                        item['errors']=false;
                        item['selected'] = false;
                        return;
                    }
                })

        }

    }

    checkAnyItemHasEventories(listItem:Array<any>){

        for (let i in listItem){
            if(listItem[i]['has_item']==1){
                return true;
            }
        }
        return false;
    }

    /*
     * Delete Function
     * */
    Delete() {

        // destroy messages
        this.messages=false;
        let loc_ids = this._Func.getValueArray(this.addedLocations,'loc_id',true);
        if (!loc_ids.length) {
            this.messages = this._Func.Messages('danger', this._Func.msg('VR113'));
        }
        else{
            let n = this;
            let warningPopup = new WarningBoxPopup();
            warningPopup.text = this._Func.msg('VR114');
            this._boxPopupService.showWarningPopup(warningPopup)
                .then(function (yes) {
                    if(!yes){
                        return;
                    } else{
                        n.selectedAll={};
                        n.submitLocToZone(loc_ids,'delete');
                    }
                });
        }



    }

    detectControlErr(control) {
        this._ErrMsgControlService._genErrMsgForControl(control);
    }

    private addLocation($event:Array<any>=[]){
        let loc_ids=this._Func.getValueArray($event,'loc_id');
        this.submitLocToZone(loc_ids,'add');
    }

    private submitLocToZone(loc_ids:Array<any>=[],action='add'){


        if(loc_ids.length){
            this.showLoadingOverlay=true;
            let data = {};
            data['loc_id'] = loc_ids;
            data['action'] = action;
            this.services.Add_Loc_To_Zone(this.zoneId,this.warehouse_id,JSON.stringify(data)).subscribe((res)=>{
                this.showLoadingOverlay=false;
                this.messages= this._Func.Messages('success',res['data']);
                this.getLocationByZone();
            },err=>{
                this.parseError(err);
            });
        }

    }

    private parseError (err) {
        this.showLoadingOverlay=false;
        this.messages = {'status' : 'danger', 'txt' : this._Func.parseErrorMessageFromServer(err)};
    }

    private setClickedRow(item, $event) {
        if (item.selected) {
            item.selected = !item.selected;
            return;
        }

        if(typeof item['has_item']=='undefined' || item['has_item']==0) {
            item.selected = !item.selected;
        } else {
            item['errors'] = true;
            let n = this;
            let warningPopup = new WarningBoxPopup();
            warningPopup.text = "Location cannot be removed because it has inventories!";
            warningPopup.showCancelButton = false;
            warningPopup.confirmButtonText = 'Close';
            this._boxPopupService.showWarningPopup(warningPopup)
                .then(function (dm) {
                    // has item
                    item['errors'] = false;
                    item['selected'] = false;
                    $event.target.checked = false;
                    return;
                })

        }
    }

}
