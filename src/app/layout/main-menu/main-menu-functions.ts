import { Component } from '@angular/core';
import {Functions} from '../../common/core/load';
declare var jQuery: any ;
@Component({
    providers: [Functions],
})
export class MainMenuFunctions{

    constructor(private func:Functions){}
    public resetActiveMenu(MenuData:Array<any>=[]){

        MenuData.forEach((menuParent)=>{
            menuParent['active']=false;
            menuParent['open']=false;
            menuParent['nodes'].forEach((menuchild)=>{
                menuchild['active']=false;
                menuParent['open']=false;
            })
        });

    }

    /*
     * Handle external menu
     *
     * */

   public handleHrefMenuLink(data:Array<any>=[])
    {
        data.forEach((item)=>{
            if(item['url']!=='#') {
                if(item['target']=='external') {
                    item['url']='#/'+item['url'];
                }
                else{
                    item['url']='#/'+item['url'];
                }
            }
        });
        return data;
    }

    public getComponentType(ins){
        // ins['child']['component']['componentType']['name']
        if(ins['child']!==null){
            let cpm_name;
            if(ins['child']['child']!==null){
                if(ins['child']['child']['child']!=null)
                {
                    // level 3
                    cpm_name=ins['child']['child']['child']['component']['componentType']['name'];
                }else{
                    // level 2
                    cpm_name=ins['child']['child']['component']['componentType']['name'];
                }
            }
            else{
                //console.log('item level 1');
                cpm_name=ins['child']['component']['componentType']['name'];
            }
            //console.log('cpm_name', cpm_name);
            return cpm_name;
        }
    }

    /*=====================
     * Router
     * ===================*/
    public getRouterName(child){
        if(child.child){
            this.getRouterName(child.child);
        }else{
            if( typeof child.component.routeData.data.titlePage !=='undefined' ){
                return child.component.routeData.data.titlePage;
            }
        }

        return null;
    }

    /*
     * Function get data from loca
     * */
    public getMenuLocation(menuname)
    {
        let menulocal=localStorage.getItem(menuname);
        if(typeof menulocal!=='undefined'){
            return localStorage.getItem(menuname);
        }
        return '';
    }

    public createMenuLocation(menuname,data)
    {
        localStorage.setItem(menuname,data);
    }

    private cpnrArray(router:string){
        return router.split(",");
    }

    public isParrentComponent(menuCpn,CPNActive)
    {
        let pageComponent=CPNActive;
        if(typeof pageComponent!=='undefined'){
            let menuComponent=this.cpnrArray(menuCpn);
            for(let i in menuComponent){
                if(this.func.trim(menuComponent[i])==this.func.trim(pageComponent)){
                    return true;
                }
            }
            return false;
        }
    }

    //  by Nhan
    slimScroll(){

        jQuery('#menu-scroll').slimScroll({
            height:'90vh',
            size: '6px',
            position: 'right',
            color:'#ddd',
            wheelStep:3
        });

        jQuery('.toogle-menu-icon').click(function(){
            jQuery('main-body').toggleClass('larger');
        });


        jQuery('#menu-scroll').mouseenter(function() {
            jQuery('.side-bar-collapse').addClass("width220");
        }).mouseleave(function() {
                jQuery('.side-bar-collapse').removeClass("width220");
            });


    }

}
