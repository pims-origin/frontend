import {Component, Input} from '@angular/core';

import {WMSPagination} from '../../common/directives/directives';
import {DashboardServices} from '../dashboard.service';
import {Functions} from '../../common/core/load';
import {PaginatePipe,PaginationControlsCmp} from '../../common/directives/directives';
import {WMSNumberFormatter} from '../../common/pipes/wms-number-formatter';

declare var Chart:any;

@Component({
	selector: 'wavepick-dashboard',
	templateUrl: 'wavepick-dashboard.html',
	directives: [WMSPagination, PaginationControlsCmp],
	providers: [DashboardServices],
	pipes: [PaginatePipe, WMSNumberFormatter]
})

export class WavepickDashboard {
	// https://www.w3schools.com/colors/colors_picker.asp?colorhex=005400
	private CHART_COLORS = ['#36c6d3', 'rgb(0, 128, 0)'];
	private wavePicks:any;
	private cLabels:Array<string> = [];
	private pickedArr:Array<any> = [];
	private totalArr:Array<any> = [];
	private barChart:any;
	private whsId:string = '';

	@Input('currentWHSId') set updateWHSId(value:any) {
		this.whsId = value;
		if (value) {
			this.getWavePicks();
		}
	}

	constructor(private dashboardServices:DashboardServices, private funcs:Functions) {
	}

	ngAfterViewInit()
	{
		// this.getWavePicks();
	}

	public drawBarChart() {
		const chartData = {
			labels: this.cLabels,
			datasets: [
				{
					label: 'Picked QTY',
					data: this.pickedArr,
					backgroundColor: this.CHART_COLORS[0]
				},
				{
					label: 'Total QTY',
					data: this.totalArr,
					backgroundColor: this.CHART_COLORS[1]
				}
			]
		};

		const ctx = document.getElementById('wavepick-bar-chart');
		if (ctx == null) {
			return true;
		}
		const that = this;
		if (this.barChart) this.barChart.destroy();
		this.barChart = new Chart(ctx, {
			type: 'bar',
			data: chartData,
			maintainAspectRatio: false,
			options: {
				responsive: true,
				tooltips: {
					mode: 'label',
					intersect: true,
					callbacks: {
						title: function(tooltipItems, data) {
							return tooltipItems[0].xLabel.split('&');
						},
						label: (tooltipItem, data) => {
							return data.datasets[tooltipItem.datasetIndex].label + ": " + this.funcs.formatSWISNumber(tooltipItem.yLabel);
						}
					}
				},
				scales: {
					xAxes: [{
						barPercentage: 1,
						categoryPercentage: 0.6,
						display: false
					}],
					yAxes: [{
						scaleLabel: {
							display: false
						},
						ticks: {
							beginAtZero: true,
							callback: (value, index, values) => {
								if (value.toString().indexOf(".") !== -1) {
									return this.funcs.formatSWISNumber(value.toFixed(1));
								}
								return this.funcs.formatSWISNumber(value);
							}
						}
					}]
				}
			}
		});
	}

	private prepareDataForChart() {
		this.cLabels = this.wavePicks.map(wp => {
			return `${wp['wv_num']}&Picker: ${wp['picker']}`;
		});

		this.pickedArr = this.wavePicks.map(wp => {
			return wp['picked'];
		});

		this.totalArr = this.wavePicks.map(wp => {
			return wp['total'];
		});
	}

	private getWavePicks() {
		this.dashboardServices.getWavepicks().subscribe(
			data => {
				this.wavePicks = data.data;

				this.prepareDataForChart();
				this.drawBarChart();
			},
			err => {
			},
			() => {}
		)

	}
}
