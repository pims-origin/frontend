import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {API_Config, Functions} from '../../common/core/load';

@Injectable()
export class ForecastReportService {
	public AuthHeader = this._Func.AuthHeader();
	public AuthHeaderPost = this._Func.AuthHeaderPost();

	constructor(private _Func:Functions,
	            private _API:API_Config,
	            private http:Http) {
	}

	getForecastReports(params) {
		return this.http.get(`${this._API.API_REPORTS_CORE}/forecast/show` + params, {headers: this.AuthHeader})
			.map(res => res.json());
	}

	getCustomersByWH(params) {
		return this.http.get(`${this._API.API_CUSTOMER_MASTER}/customer-warehouses${params}`, {headers: this.AuthHeader})
			.map(res => res.json());
	}

	getData(cusId) {
		return this.http.get(`${this._API.API_REPORTS_CORE}/forecast/get-year-month-have-data/${cusId}`, {headers: this.AuthHeader})
			.map(res => res.json());
	}

}
