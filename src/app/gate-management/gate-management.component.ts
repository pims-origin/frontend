import {Component, OnInit} from "@angular/core";
import {Http, Headers} from "@angular/http";
import { Router, RouteParams, RouterLink, ROUTER_DIRECTIVES } from '@angular/router-deprecated';
import {CORE_DIRECTIVES, FORM_DIRECTIVES,ControlGroup,FormBuilder,Control,ControlArray,Validators} from '@angular/common';
import {API_Config} from "../common/core/API_Config";
import {UserService} from "../common/users/users.service";
import {Functions} from "../common/core/functions";
import {WMSMessages} from "../common/directives/messages/messages";
import {WMSBreadcrumb} from '../common/directives/directives';
import {TOOLTIP_DIRECTIVES} from "ng2-bootstrap/ng2-bootstrap";
import {GateManagementServices} from '../gate-management/gate-management-service';
import {ValidationService } from '../common/core/validator';
declare var jQuery:any;

@Component({
  selector: 'gate-management',
  directives: [TOOLTIP_DIRECTIVES, CORE_DIRECTIVES, FORM_DIRECTIVES, WMSMessages, WMSBreadcrumb],
  providers : [FormBuilder,ValidationService,GateManagementServices],
  templateUrl: 'gate-management.component.html',
})
export class GateManagementComponent{

  private messages;
  private showLoadingOverlay = false;
  private gateList = [];
  private showError = false;
  form: ControlGroup;
  private popupGateDetail;
  private datetimePickerStart;
  private datetimePickerEnd;
  private inputStart;
  private inputEnd;
  private listCustomer = [];
  private listContainer = [];
  private whs_id;
  private containerId;
  private gateId;

  constructor(
    private _func: Functions,
    private _API: API_Config,
    private _gateManagementServices: GateManagementServices,
    private _Valid: ValidationService,
    private fb: FormBuilder,
    private http: Http
  ){
    this.whs_id = localStorage.getItem('whs_id');
    this.getCustomersByWH();
    // this.getListContainer();
    this.getGateList();
    this.buildForm();
  }

  ngAfterViewInit () {
    var that = this;
    this.datetimePickerStart = jQuery('#datetimepicker1');
    this.datetimePickerEnd = jQuery('#datetimepicker2');
    this.inputStart = this.datetimePickerStart.find('input');
    this.inputEnd = this.datetimePickerEnd.find('input');

    this.datetimePickerStart.datetimepicker({format: 'YYYY-MM-DD HH:mm', collapse: false, sideBySide: true, allowInputToggle: true, showClose: true})
      .on('dp.change', function() {
        var startVal = that.inputStart.val();
        (<Control>that.form.controls['open']).updateValue(startVal);
        that.calculateDuration();
      })

    this.datetimePickerEnd.datetimepicker({format: 'YYYY-MM-DD HH:mm', collapse: false, sideBySide: true, allowInputToggle: true, showClose: true})
      .on('dp.change', function() {
        var endVal = that.inputEnd.val();
        (<Control>that.form.controls['close']).updateValue(endVal);
        that.calculateDuration();
      })

    this.popupGateDetail = jQuery('#gate-detail');
  }

  private getCustomersByWH () {
    let params='?whs_id=' + this.whs_id + "&limit=1000";
    this._gateManagementServices.getCustomersByWH(params)
      .subscribe(
        data => {
          this.listCustomer = data.data;
        },
        err => {
        }
      );
  }

  private getListContainer() {
    this._gateManagementServices.getListContainer()
      .subscribe(
        data => {
          this.listContainer = data.data;
        },
        err => {
        }
      );
  }

  private showMessage(msgStatus, msg) {
      this.messages = {
          'status': msgStatus,
          'txt': msg
      }
      jQuery(window).scrollTop(0);
  }

  private calculateDuration() {
    var min = '',
        startVal, endVal, startTime, endTime, duration;
    startVal = this.inputStart.val();
    endVal = this.inputEnd.val();
    if(this.gateAction == 'update') {
      if(!endVal) {
        (<Control>this.form.controls['close']).setErrors({'required': true});
      }
    }
    else {
      (<Control>this.form.controls['close']).setErrors(null);
    }
    if(startVal && endVal) {
      (<Control>this.form.controls['close']).setErrors(null);
      startTime = new Date(startVal).getTime();
      endTime = new Date(endVal).getTime();
      duration = endTime - startTime;
      if(duration > 0) {
        min = (duration/1000/60).toString();
      }
      else {
        (<Control>this.form.controls['close']).setErrors({'invalid': true});
      }
    }
    (<Control>this.form.controls['duration']).updateValue(min);
  }

  private getGateList() {
    this.showLoadingOverlay = true;
    this._gateManagementServices.getGateList().subscribe(
      data => {
        for(let key in data.data) {
          this.gateList.push(data.data[key]);
        }
        this.showLoadingOverlay = false;
      },
      err => {
        this.showMessage('danger', this._func.parseErrorMessageFromServer(err));
        this.showLoadingOverlay = false;
      }
    );
  }

  private popupTitle;
  private gateAction;
  showGateDetail(gate) {

    this.containerId = '';
    this.gateId = gate.gate_id;
    if(gate.gate_sts == 'A') { // Available
      this.popupTitle = 'New gate';
      this.gateAction = 'new';
    }
    else {
      this.popupTitle = 'Update gate';
      this.gateAction = 'update';
    }

    // reset form
    this.showError = false;
    (<Control>this.form.controls['ctnr_num']).updateValue('');
    (<Control>this.form.controls['cus_id']).updateValue('');

    this.datetimePickerStart.data("DateTimePicker").date(null);
    this.datetimePickerEnd.data("DateTimePicker").date(null);
    (<Control>this.form.controls['open']).updateValue('');
    (<Control>this.form.controls['close']).updateValue('');

    // show gate detail
    (<Control>this.form.controls['gate_dtl_sts']).updateValue('O');
    if(gate.container) {
      this.containerId = gate.container.cc_id;
      (<Control>this.form.controls['ctnr_num']).updateValue((gate.container.ctnr_num || '').toString());
      (<Control>this.form.controls['cus_id']).updateValue((gate.container.cus_id || '').toString());
      if(gate.container.open) {
        (<Control>this.form.controls['open']).updateValue(gate.container.open);
        this.datetimePickerStart.data("DateTimePicker").date(gate.container.open);
      }
      if(gate.container.close) {
        (<Control>this.form.controls['close']).updateValue(gate.container.close);
        this.datetimePickerEnd.data("DateTimePicker").date(gate.container.close);
      }
      this.calculateDuration();
    }
    this.popupGateDetail.modal('show');
  }

  private buildForm() {
      this.form = this.fb.group({
          ctnr_num: ['', this._Valid.customRequired],
          cus_id: ['', this._Valid.customRequired],
          open: ['', this._Valid.customRequired],
          close: [''],
          duration: [''],
          gate_dtl_sts: ['O'],
      });
  }

  onSubmit(form) {
    this.showError = true;
    if (this.form.valid) {
      if(this.containerId) {
        this.update(this.containerId, JSON.stringify(this.form.value));
      }
      else {
        this.addNew(this.gateId, JSON.stringify(this.form.value));
      }
    }
  }

  addNew(gateId, stringData) {
      let that = this;
      this.showLoadingOverlay = true;
      this._gateManagementServices.addNew(gateId, stringData).subscribe(
          data => {
              this.showLoadingOverlay = false;
              // window.location.reload();
              this.gateList = [];
              this.getGateList();
              this.popupGateDetail.modal('hide');
          },
          err => {
              this.showLoadingOverlay = false;
              this.showMessage('danger', this._func.parseErrorMessageFromServer(err));
          }
      );
  }

  update(containerID, stringData) {
      let that = this;
      this.showLoadingOverlay = true;
      this._gateManagementServices.update(containerID, stringData).subscribe(
          data => {
              this.showLoadingOverlay = false;
              // window.location.reload();
              this.gateList = [];
              this.getGateList();
              this.popupGateDetail.modal('hide');
          },
          err => {
              this.showLoadingOverlay = false;
              this.showMessage('danger', this._func.parseErrorMessageFromServer(err));
          }
      );
  }

  private dataListCtnr = [];
  private getContainerByName ($event) {
      var ctnr_num = $event.target.value.trim();

      if (ctnr_num == '') {

          return false;
      }

      var params = "?ctnr_num=" + encodeURIComponent(ctnr_num) + "&limit=20";

      jQuery("#wrap-ctnr-list .create-item").html('Create <b>' + ctnr_num + '</b>');

      this._gateManagementServices.getContainerByName(params)
          .subscribe(
              data => {
                  this.dataListCtnr = data;
                  this.ckCtnrForShowList(ctnr_num);
              }
          );

      jQuery("body").off('click.hideAutocompleteContainer').on("click.hideAutocompleteContainer", function(e) {

          if(e.target.id !== "wrap-ctnr-list" && e.target.id !== "ctnr_num")
          {
              jQuery("#wrap-ctnr-list").hide();
          }
      });

  }

  private ckCtnrForShowList(ctnr_num) {

      jQuery("#wrap-ctnr-list .create-item").show();

      for (var i = 0; i < this.dataListCtnr.length; i++) {

          if (this.dataListCtnr[i]['ctnr_num'].toLowerCase() == ctnr_num.toLowerCase()) {

              jQuery("#wrap-ctnr-list .create-item").hide();
          }
      }

      jQuery("#wrap-ctnr-list").show();
  }

  private getDetailCtnr($event, createNew = false) {
      var ctnr_num = createNew ? jQuery($event.target).closest('.create-item').find('b').text() : jQuery($event.target).text();

      (<Control>this.form.controls['ctnr_num']).updateValue(ctnr_num);
      jQuery("#wrap-ctnr-list").hide();
  }
}
