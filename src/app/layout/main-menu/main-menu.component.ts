import { Component,ElementRef,OnInit, Renderer } from '@angular/core';
import {RouteConfig, RouterLink, Router,RouteData, RouteParams} from '@angular/router-deprecated';
import {Http, Response, Headers } from '@angular/http';
import { contentHeaders } from '../../common/api/headers';
import {DND_DIRECTIVES} from 'ng2-dnd/ng2-dnd';
import {MainMenuService} from "./main-menu.service";
import {FavoriteMenu} from "./favorite.menu";
import {TOOLTIP_DIRECTIVES} from "ng2-bootstrap/ng2-bootstrap";
import {Functions} from '../../common/core/load';
import { Title }     from '@angular/platform-browser';
import { MainMenuFunctions }     from './main-menu-functions';
import { MenuCustom }     from './menu-custom';
// import 'ng2-dnd/ng2-dnd.css';
import {
  EventTrackerDirective
} from '../../common/directives/directives';
declare var jQuery: any ;
@Component({
  selector: 'main-menu',
  directives: [RouterLink, FavoriteMenu, DND_DIRECTIVES,TOOLTIP_DIRECTIVES, EventTrackerDirective],
  providers: [MainMenuService,MainMenuFunctions,Title,MenuCustom],
  templateUrl: 'main-menu.component.html'
})
export class MainMenuComponent {
  appendStyle: boolean = true;
  ShowAllFavoriteMenu: boolean = false;
  elementRef: ElementRef;
  private whs_id:any;
  private menuActive;
  public MenuData:Array<any>=[];
  public FavoriteMenu: Array<any> = [];
  public isloading: boolean = false;
  public active: boolean = false;
  public Loading={};
  private CPNActive;
  private routerTitlePage;

  constructor(
    public el: ElementRef,
    private titleService: Title,
    public renderer: Renderer,
    elementRef: ElementRef,
    public _router: Router,
    private menuCustom:MenuCustom,
    private menuFunc:MainMenuFunctions,
    public _MenuService: MainMenuService,
    private _params: RouteParams,
    private _Func:Functions
  ) {

    this.elementRef = elementRef;
    this.ListFavoriteMenu();
    this.ListLeftMenu();
    this.whs_id = this._params.get("whs_id");
    this.routerReDefined(this._router.root);

    this._router.root.subscribe((router)=>{
      if(typeof router!=='undefined'){
        this.routerReDefined(router);
      }
    });

  }
  public setTitle( newTitle: string) {
    this.titleService.setTitle(newTitle);
  }

  private addCustomeMenuToMainMenu(){
      this.MenuData.push(this.menuCustom.MenuListCustom[0]); // the number zero is Dasboard Menu
  }

  private routerReDefined(router){
    this.menuActive=router;
    let ins= this._router.root.currentInstruction;
    this.routerTitlePage=this.menuFunc.getRouterName(ins);
    this.CPNActive=this.menuFunc.getComponentType(ins);
    this.setActiveMenuParent( this.menuActive);
  }

  // set active parent menu
  private setActiveMenuParent(roterActiveName){

    if(roterActiveName!==null){
      // set default menu
      this.menuFunc.resetActiveMenu(this.MenuData);
      let Menu=this.MenuData;
      let menuComponent;
      for(let i in Menu){
        let subMenu=Menu[i]['nodes'];
        if(subMenu.length){
          for(let sub in subMenu) {
            menuComponent= subMenu[sub]['description'];
            if (this.menuFunc.isParrentComponent(menuComponent,this.CPNActive)) {
              Menu[i]['active'] = true;
              Menu[i]['open'] = true;
              subMenu[sub]['active'] = true;
              if(this.routerTitlePage){
                this.setTitle(this.routerTitlePage);
              }
              else{
                this.setTitle(subMenu[sub]['name']);
              }

            }
          }
        }
        else{
          menuComponent = Menu[i]['description'];
          if(this.menuFunc.isParrentComponent(menuComponent,this.CPNActive)){

            Menu[i]['active']=true;
            Menu[i]['open'] = true;

            if(this.routerTitlePage){
              this.setTitle(this.routerTitlePage);
            }
            else{
              this.setTitle(Menu[i]['name']);
            }

          }
        }
      }
    }

  }
  
  


  private checkToken()
  {
    this._MenuService.getLeftMenu().subscribe(
      data=>{
      },
      err => {
        if(err.json().errors.status==401)
        {
          this.logout();
        }
      },
      ()=>{}
    );
  }

  private getWidth() {
    if (this.appendStyle) {
      //this.renderer.setElementStyle(this.el.nativeElement, 'width', '240px');
      this.renderer.setElementClass(this.el.nativeElement, 'side-bar-expand', true);
      this.renderer.setElementClass(this.el.nativeElement, 'side-bar-collapse', false);
    } else {
      //this.renderer.setElementStyle(this.el.nativeElement, 'width', '40px');
      this.renderer.setElementClass(this.el.nativeElement, 'side-bar-expand', false);
      this.renderer.setElementClass(this.el.nativeElement, 'side-bar-collapse', true);
    }
  }
  /*
  * List main-menu
  *
  * */
  private ListLeftMenu() {

    if(this.menuFunc.getMenuLocation('leftmenu'))
    {
      this.addCustomeMenuToMainMenu();

      this.Loading['menuLeft'] = true;
      this._MenuService.getLeftMenu().subscribe(
        data => {
          this.Loading['menuLeft'] = false;
          // update menu data
          this.updateMenuData(data);
          if(typeof this.menuActive!=='undefined'){
          this.setActiveMenuParent( this.menuActive);
          }
          this.menuFunc.slimScroll();
        },
        err => {
          this.Loading['menuLeft'] = false;
          this.updateMenuData([]);
        },
           () => {}
      );
    }
    else{
      this.Loading['menuLeft'] = true;
      this._MenuService.getLeftMenu().subscribe(
        data => {
          this.Loading['menuLeft'] = false;
          this.updateMenuData(data);
          this.menuFunc.slimScroll();
        },
        err => {
          this.Loading['menuLeft'] = false;
          this.updateMenuData([]);
        },
        () => {}
      );
    }
  }

  /* ==============================
  *updateMenuData
  * =============================*/
 private updateMenuData(data:Array<any>=[]){

   this.MenuData=[];
   this.addCustomeMenuToMainMenu();
   this.MenuData.push(...data);
   this.menuFunc.createMenuLocation('leftmenu',JSON.stringify(this.MenuData ));

 }

  /*List main-menu*/
  private ListFavoriteMenu() {
    if(this.menuFunc.getMenuLocation('favoritemenu'))
    {
      this.FavoriteMenu=JSON.parse(this.menuFunc.getMenuLocation('favoritemenu'));
      this._MenuService.getFavoriteMenu()
        .subscribe(
          data => {
            this.FavoriteMenu = data;
            this.menuFunc.createMenuLocation('favoritemenu',JSON.stringify(data));
          },
          err =>{
          },
          () => {}
        );
    }
    else{
      this.Loading['menuFavorite'] = true;
      this._MenuService.getFavoriteMenu()
        .subscribe(
          data => {
            this.Loading['menuFavorite'] = false;
            this.FavoriteMenu = data;
            this.menuFunc.createMenuLocation('favoritemenu',JSON.stringify(data));
          },
          err =>{
            this.Loading['menuFavorite'] = false;
          },
          () => {}
        );
    }
  }

  private handleError(error: any) {
    return Promise.reject(error.message || error);
  }
 
  /*=====
   Update Favorite menu
   ======*/
  private UpdateMenuFavorite($event) {
    let item = $event.dragData;
    let found = 0;

    let position = this.FavoriteMenu.length;

    this.isloading = true;
    if (this.FavoriteMenu.length == 0) {
      this.FavoriteMenu.push(item); /* push to array*/
      this._MenuService.Add_to_FavoriteMenu(item.menu_id, position + 1)
        .subscribe(data => {
        }, error => {});
    }
    else {
      for (let i in this.FavoriteMenu) {
        let idFav = this.FavoriteMenu[i].menu_id;
        if (item.menu_id == idFav) {
          found++;
        }
      }
      if (found == 0) {
        this.FavoriteMenu.push(item);
        this._MenuService.Add_to_FavoriteMenu(item.menu_id, position + 1)
          .subscribe(data => {
            this.isloading = false;
            this.menuFunc.createMenuLocation('favoritemenu',JSON.stringify(this.FavoriteMenu));
          }, error => {
            
          });
        return;
      }
    }  /*end if*/
  }



  /*
   Delete item menu favorite
   */
  private DeleteItemFavoriteMenu($event) {
    let item = $event.dragData;
    this.isloading = true;
    let index = this.FavoriteMenu.indexOf(item);
    this.FavoriteMenu.splice(index, 1);
    this._MenuService.DeleteItemMenuFavorite(item.menu_id)
      .subscribe(
        (res: Response) => {
          return res.status === 204 ? '' : alert('Error delete!');
        },
        err => this.handleError(err)
      );
  }
  /*
   Remove by click button
   */
  removeItemFavorite(item) {

    this.isloading = true;
    let index = this.FavoriteMenu.indexOf(item);
    this.FavoriteMenu.splice(index, 1);
    this._MenuService.DeleteItemMenuFavorite(item.menu_id)
      .subscribe(
        (res: Response) => {
          return res.status === 204 ? '' : alert('Error delete!');
        },
        err => this.handleError(err)
      );
  }
  msgErrors(err)
  {
    if(err.json().errors)
    {
      let  stt_code=err.json().errors.status;
      switch (stt_code)
      {
        case 401 :
          this.logout();
          break;
      }
    }
    else{
    }
  }
  logout() {
    localStorage.removeItem('jwt');
    this._router.parent.navigateByUrl('/login');
  }
  
  private checkRedirectPage(url) {
    if(url) {
      var splitURL = url.split('/#/');
      if(splitURL.length > 1) {
        return window.location.origin + '/' + url;
      }
      else {
        return window.location.origin + '/#/' + url;
      }
    }
  }
  
}
class Menu {
  constructor(public menu_id: number, public name: string) { }
}
