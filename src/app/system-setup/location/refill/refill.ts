import {Component,Input,Output,EventEmitter} from '@angular/core';
import { FORM_DIRECTIVES,ControlGroup,ControlArray,FormBuilder, Control,Validators} from '@angular/common';
import { ValidationService } from '../../../common/core/validator';
import { InfiniteScrollAutocomplete } from '../../../common/directives/directives';
import {UserService, Functions} from '../../../common/core/load';
import {RefillServices} from "./refill-services";
import {BoxPopupService} from "../../../common/popup/box-popup.service";
import {WarningBoxPopup} from "../../../common/popup/warning-box-popup";
declare var jQuery: any;
@Component ({
    selector: 'refill-directive',
    directives:[FORM_DIRECTIVES, InfiniteScrollAutocomplete],
    providers: [RefillServices,ValidationService,BoxPopupService,UserService],
    templateUrl: 'refill.html'
})

export class ReFillDirective{

    RefillForm: ControlGroup;
    RefillItems:ControlGroup[]=[];
    RefillItemsArray: ControlArray= new ControlArray(this.RefillItems);
    private _whs_id;
    private _action;
    private _loc_id;
    private _LocationData:Array<any>=[];
    private selectedAll={};
    private loading={};

    @Input('LocationData') set LocationData(value) {
        this._LocationData=value;
    }

    @Input('action') set action(value) {
       this._action=value;
    }

    @Input('whs_id') set whs_id(value) {
        this._whs_id=value;
    }

    @Input('loc_id') set loc_id(value) {
        this._loc_id=value;
    }

    private _submitForm=false;
    @Input('submitForm') set submitForm(value) {
        if(value==true){
            this.Save(this.RefillForm.value);
        }
    }

    @Output() updateCompleted = new EventEmitter();
    @Output() messages = new EventEmitter();

    constructor(
        private fb: FormBuilder,
        private _refService:RefillServices,
        private _Valid: ValidationService,
        private _boxPopupService: BoxPopupService,
        private _Func: Functions,
        private _user:UserService
    ) {
        this.checkPermission();
    }

    private viewLocation;
    private editLocation;
    private allowAccess:boolean=false;
    private checkPermission() {
        this._user.GetPermissionUser().subscribe(
            data => {
                this.viewLocation = this._user.RequestPermission(data,'viewLocation');
                this.editLocation = this._user.RequestPermission(data,'editLocation');
                /* Check orther permission if View allow */
                if(!this.editLocation||this.viewLocation) {
                    if(this._action=='view'||this._action=='edit') {
                        this.getRefillList();
                        this.getCustomersByWH();
                    }
                }
                else {
                 // fail
                }
            },
            err => {
                this.messages.emit(this._Func.Messages('danger', this._Func.parseErrorMessageFromServer(err)));
            }
        );
    }

    // get list custommer
    private Customers:Array<any>=[];
    private getCustomersByWH () {

        let params="?limit=10000";
        this._refService.getCustomersByWH(params)
            .subscribe(
                data => {
                    this.Customers = data.data;
                },
                err => {
                    this.parseError(err);
                },
                () => {
                }
            );

    }

    /*Save*/
    Save(data){
        this._submitForm=true;
        if(this.RefillForm.valid) {
            data = JSON.stringify(data);
            this._refService.updateRefill(this._LocationData['loc_id'], data).subscribe(
                data => {
                    this.updateCompleted.emit(true);
                },
                err => {
                    this.parseError(err);
                },
                () => {}
            );
        }
    }

    /*====================================
     * Check Min Count Max Count
     * ===================================*/
    private validMinMaxCount(row,a,b)
    {
        if(a&&b) {
            if(parseInt(a)>parseInt(b)||parseInt(a)==parseInt(b)){
                this.setErrorsControlItem(row,'min',{'invalidMin':true});
                this.setErrorsControlItem(row,'max',{'invalidMax':true});
            }
        }
    }
    
    /*=====================================
    * FormBuilder
    *=================================== */
    FormBuilder(data:any=[])
    {
        this.RefillForm =this.fb.group({
            data:this.RefillItemsArray,
        });

        if(data.length){
            let i=0;
            data.forEach((item)=>{
                this.AddRefillItem(item);
                this.RefillItems[i]['sku_valid']=true;
                i++;
            });
        }else{
            this.AddRefillItem();
        }

        this.RefillForm.valueChanges.subscribe(data =>{
            this.checkDuplicatedKey();
        });

    }

    /*=====================================
    * AddRefillItem
    * ===================================*/
    AddRefillItem(data={}){

        // loc_id	loc_code	item_id	Cus_id	SKU	Color 	Size	UPC	Lot	Min	Max

        this.RefillItemsArray.push(
            new ControlGroup({
                'rf_id':new Control(data['rf_id']),
                'loc_id':new Control(this._LocationData['loc_id']),
                'loc_code' :new Control(this._LocationData['loc_code']),
                'item_id':new Control(data['item_id']),
                'cus_id':new Control(data['cus_id'] ? data['cus_id'] : '', Validators.compose([Validators.required])),
                'sku':new Control(data['sku'] ? data['sku'] : '',Validators.compose([Validators.required])),
                'color':new Control(data['color']),
                'size':new Control(data['size']),
                'upc':new Control(data['cus_upc']),
                'lot':new Control(data['lot']),
                'type':new Control(data['type']),
                'min':new Control(data['min'],Validators.compose([Validators.required,this._Valid.validateInt])),
                'max':new Control(data['max'],Validators.compose([Validators.required,this._Valid.isZero,this._Valid.validateInt]))
            })
        );
    }

    autoComplete(row,key,fieldname,page=1,reset=true){
        // reset row
        this.resetRow(row,false);

        let searchkey=this._Func.trim(key);
        if (!searchkey) return;

        // search only if cus_id have seleced
        this.RefillItems[row]['sku_valid']=false;
        this.RefillItems[row]['showLoading']=true;
        this.RefillItems[row]['current_page'] = page;

        let param = `?${fieldname}=${searchkey}&cus_id=${this.RefillItems[row]['controls']['cus_id'].value}&limit=20&sort[sku]=asc&page=${page}`;
        if (fieldname === 'sku') {
            param += '&pack=1';
        }
        let enCodeSearchQuery=this._Func.FixEncodeURI(param);

        if(this.RefillItems[row]['subscribe']) {
            this.RefillItems[row]['subscribe'].unsubscribe();
        }
        // do Search
        this.RefillItems[row]['subscribe']=this._refService.searchItems(enCodeSearchQuery).debounceTime(400).distinctUntilChanged().subscribe(
            data => {
                // disable loading icon
                this.RefillItems[row]['showLoading']= false;
                this.RefillItems[row]['paginData']= data.meta;
                if(!this.RefillItems[row]['autoSearchData'] || reset){
                    this.RefillItems[row]['autoSearchData'] = [];
                }
                this.RefillItems[row]['autoSearchData'].push(...data.data);
                this.checkKeyExist(row,searchkey,this.RefillItems[row]['autoSearchData'],fieldname);
            },
            err =>{
                this.RefillItems[row]['showLoading']=false;
                this.parseError(err);
            },
            () =>{
            }
        );


    }
    /*
    * Check Key exist
    * */
    checkKeyExist(row,str:string,arr:Array<any>,fieldname)
    {
        let string=str;
        arr.forEach((item)=>{
            if(string==this._Func.trim(item[fieldname]))
            {
                this.selectedItem(row,item);
            }
        });


    }

    /*
     * Auto select item if match
     * */
    selectedItem(row,dataItem)
    {
        this.RefillItems[row]['disabled']=true;
        this.RefillItems[row]['item_code_valid']=true;
        this.RefillItems[row]['sku_valid']=true;
        this.setErrorsControlItem(row,'sku',null);
        this.setValueForItem(row,'item_id',dataItem['item_id']);
        this.setValueForItem(row,'sku',dataItem['sku']);
        this.setValueForItem(row,'color',dataItem['color']);
        this.setValueForItem(row,'size',dataItem['size']);
        this.setValueForItem(row,'upc',dataItem['cus_upc']);
        this.setValueForItem(row,'lot',dataItem['lot']);
        this.setValueForItem(row,'type',dataItem['type']);
        this.setValueForItem(row,'min','');
        this.setValueForItem(row,'max','');
    }

    /*==================================
    * getRefillList
    *================================== */
    getRefillList()
    {
        this.loading['refill']=true;
        this._refService.getRefillList(this._loc_id).subscribe(
            data => {
                this.loading['refill']=false;
                this.loading['refilldone']=true;
                if((this._action=='view'&&data.data.length)||this._action!=='view'){
                    this.FormBuilder(data.data);
                }
            },
            err => {
                this.loading['refill']=false;
                this.loading['refilldone']=true;
            },
            () => {}
        );
    }

    /*==================================
     * showKeyNotExit
     * ===============================*/
    showKeyNotExit(row){
        this.RefillItems[row]['mouseout']=true;
        setTimeout(() => {
            if(!this.RefillItems[row]['sku_valid']&&this.RefillItems[row].value['sku']){
                this.setErrorsControlItem(row,'sku',{invaildValue:true});
            }
        },150);
    }

    /*==================================
    * checkDuplicatedValue
    * ===============================*/
    checkDuplicatedKey(){

        // reset
        let error=0;

        for(let i=0;i<this.RefillItems.length;i++)
        {
            let skuControl=this.RefillItemsArray['controls'][i]['controls']['sku'];
            if(skuControl.value){
                if(skuControl.hasError('duplicated')){
                    delete  skuControl.errors['duplicated'];
                    if(!Object.keys(skuControl.errors).length)
                    {
                        this.setErrorsControlItem(i,'sku',null);
                    }
                }
            }
        }

        let Arr=this.RefillItemsArray.value;
        for(let i=0;i<Arr.length;i++)
        {
            let item=Arr[i]['item_id'];
            for(let j=i+1;j<Arr.length;j++)
            {
                let itemj=Arr[j]['item_id'];
                if(i==j) {
                    continue;
                }
                if((item&&itemj)&&item==itemj)
                {
                    this.setErrorsControlItem(i,'sku',{duplicated:true});
                    this.setErrorsControlItem(j,'sku',{duplicated:true});
                    (<Control>this.RefillItems[i]['controls']['sku']).markAsTouched();
                    (<Control>this.RefillItems[j]['controls']['sku']).markAsTouched();

                }
            }
        } // end for


    }

    deleteItem(ListName:string,IsControlGroupForm=false){

        let n = this;
        if(this.getItemchecked()>0) {

            /*case 1: modify text*/
            let warningPopup = new WarningBoxPopup();
            warningPopup.text = this._Func.msg('PBAP002');
            this._boxPopupService.showWarningPopup(warningPopup)
                .then(function (dm) {
                    if (!dm) {
                        return;
                    } else {
                        // create array temp
                        let Arr = eval(`n.` + ListName);
                        let ArrTemp = Arr.slice();
                        if (n.selectedAll[ListName]) {
                            // check delete form control group
                            if (IsControlGroupForm) {
                                while (n.RefillItemsArray.length) {
                                    n.RefillItemsArray.removeAt(0);
                                }
                                // auto add one row
                                n.selectedAll[ListName] = false;
                                n.AddRefillItem();
                            }
                            else {
                                n.selectedAll[ListName] = false;

                                let i = 0;
                                ArrTemp.forEach((item)=> {
                                    // find item selected and remove it

                                    if (item['selected'] == true) {
                                        let el = Arr.indexOf(item);
                                        Arr.splice(el, 1);
                                    }
                                    i++;

                                });

                            }
                            n.selectedAll[ListName] = false;
                        }
                        else {

                            let i = 0;
                            ArrTemp.forEach((item)=> {
                                // find item selected and remove it

                                if (item['selected'] == true) {
                                    if (IsControlGroupForm) {
                                        n.RefillItemsArray.removeAt(i);
                                        i--;
                                    }
                                    else {
                                        let el = Arr.indexOf(item);
                                        Arr.splice(el, 1);
                                    }

                                }
                                i++;

                            });

                        }
                    }
                });
        }
        else{

            this.messages.emit(this._Func.Messages('danger', this._Func.msg('VR113')));

        }
    }

    /*
     * Selected
     * */

    private Selected($event,item,ListName:string) {

        let Array=eval(`this.`+ListName);
        this.selectedAll[ListName]=false;

        if ($event.target.checked) {
            item['selected']=true;

            if(this.getItemchecked()==this.RefillItems.length){
                this.selectedAll['RefillItems']=true;
            }

        }
        else {
            item['selected']=false;
        }

    }

    /*
    * getItemchecked
    * */

    getItemchecked(){

        let selected=0;
        this.RefillItems.forEach((item)=>{
            if(item['selected']==true)
            {
                selected++;
            }
        });

        return selected;

    }

    /*
     * Check All/Off
     * */
    private CheckAll(event,ListName) {
        let Array=eval(`this.`+ListName);

        if(!Array.length)
        {
            this.selectedAll[ListName] = false;
            return;
        }
        if (event) {
            this.selectedAll[ListName] = true;

            this.setListItemsSelected(true);

        }
        else {
            this.selectedAll[ListName]= false;
            this.setListItemsSelected(false);
        }

    }

    setListItemsSelected(value)
    {
        this.RefillItems.forEach((item) => {
            item['selected']=value;
        });
    }

    private currentCus;
    changeCustomer(row,$event){

        // set currentCus
        this.currentCus=this.RefillItems[row]['controls']['cus_id'].value;
        let that = this;
        let warningPopup = new WarningBoxPopup();
        warningPopup.text = this._Func.msg('M043');

        if(this.RefillItems[row]['controls']['cus_id'].value&&$event.target.value){
            this._boxPopupService.showWarningPopup(warningPopup)
                .then(function (yes) {
                    if(yes) {
                        that.resetRow(row);
                    }
                    else{
                      that.setValueForItem(row,'cus_id',that.currentCus);
                    }
                });
        }else{
            this.resetRow(row);
        }

    }
    
    /*
    * resetRow
    * */
    resetRow(row,ext=true){
        this.setValueForItem(row,'item_id','');
        if(ext){
            this.setValueForItem(row,'sku','');
        }
        this.setValueForItem(row,'color','');
        this.setValueForItem(row,'size','');
        this.setValueForItem(row,'upc','');
        this.setValueForItem(row,'lot','');
        this.setValueForItem(row,'type','');
        this.setValueForItem(row,'min','');
        this.setValueForItem(row,'max','');
    }

    private checkInputNumber(event,int=false)
    {
        this._Valid.isNumber(event,int);
    }

    /*
    * setErrorsControlItem
    * */
    setErrorsControlItem(row,control,err){
        (<Control>this.RefillItems[row]['controls'][control]).setErrors(err);
    }

    /*
     * setValueForItem
     * */
    setValueForItem(row,control,value){
        (<Control>this.RefillItems[row]['controls'][control]).updateValue(value);
    }

    private parseError(err){
        this.messages.emit(this._Func.Messages('danger',this._Func.parseErrorMessageFromServerStandard(err)));
    }

}
