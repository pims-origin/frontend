import { Component} from '@angular/core';
import { Http } from '@angular/http';
import { Router, RouterLink, ROUTER_DIRECTIVES, RouteParams } from '@angular/router-deprecated';
import 'ag-grid-enterprise/main';
import 'rxjs/add/operator/map';
import {API_Config} from "../../../common/core/API_Config";
import {UserService} from "../../../common/users/users.service";
import {Functions} from "../../../common/core/functions";
import {GoBackComponent} from "../../../common/component/goBack.component";
import {WMSBreadcrumb} from '../../../common/directives/directives';
declare var jQuery: any;

@Component ({
    selector: 'view-location-type',
    directives:[GoBackComponent,WMSBreadcrumb],
    templateUrl: 'view-location-type.component.html',
})

export class ViewLocationTypeComponent {
    private id: any;
    private flagViewLocation = false;
    private headerGet: any;
    private headerPost: any;
    private urlAPILocation: any;
    private urlAPILocationType: any;
    private messages = {};
    private dataLocaType: any = {};

    constructor (
        private _http: Http,
        private _router:Router,
        private _user: UserService,
        private _Func: Functions,
        private _API_Config: API_Config,
        params: RouteParams) {

        this.id = params.get('id');

        if(this.id == '') {

            this._router.parent.navigateByUrl('/deny');
        }
        // Check permission for user using this page
        // this.ck_permisson();

        this.headerGet = this._Func.AuthHeader();
        this.headerPost = this._Func.AuthHeaderPost();
        this.urlAPILocation = this._API_Config.API_Location;
        this.urlAPILocationType = this._API_Config.API_Location_type;
        this.getLocaTypeDetail();

    }

    // Check permission for user using this function page
    private ck_permisson() {

        this._user.GetPermissionUser().subscribe(
            data => {

                this.flagViewLocation = this._user.RequestPermission(data,'viewSystem');

                /* Check orther permission if View allow */
                if(!this.flagViewLocation) {

                    this._router.parent.navigateByUrl('/deny');
                }
            },
            err => {this._router.parent.navigateByUrl('/deny'); },
            () => {}
        );
    }

    private getLocaTypeDetail () {

        this._http.get (this.urlAPILocationType + "/" + this.id, {headers: this.headerGet})
            .map(res => res.json())
            .subscribe(
                data => {

                    this.dataLocaType = data.data;

                },
                error => {

                    this.parseError(error);
                },
                () => {}
            );
    }

    // Calcel action
    private cancel() {

        this._router.parent.navigateByUrl('/master-data/location-type');
    }

    private parseError (err) {

        this.messages = this._Func.Messages('danger', this._Func.msg('VR100'));

    }
}
