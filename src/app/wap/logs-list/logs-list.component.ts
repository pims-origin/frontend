import {Component, OnInit} from "@angular/core";
import {Http, Headers} from "@angular/http";
import { Router, RouterLink, ROUTER_DIRECTIVES } from '@angular/router-deprecated';
import { CORE_DIRECTIVES, FORM_DIRECTIVES } from '@angular/common';
import {API_Config} from "../../common/core/API_Config";
import {UserService} from "../../common/users/users.service";
import {Functions} from "../../common/core/functions";
import {LogsListService} from "../logs-list/logs-list-service";
import {WMSPagination, AdvanceTable, WMSBreadcrumb, WMSMessages} from '../../common/directives/directives';
import {BoxPopupService} from "../../common/popup/box-popup.service";
import {Title} from '@angular/platform-browser';
declare var jQuery: any;
declare var swal: any;

@Component({
    selector: 'logs-list',
    directives: [CORE_DIRECTIVES, FORM_DIRECTIVES, WMSMessages, WMSBreadcrumb, AdvanceTable, WMSPagination],
    templateUrl: 'logs-list.html',
    providers: [LogsListService,Title,BoxPopupService],
})
export class LogsListComponent{

		// permission
	private hasViewPermission = false;

  // advance table
  private tableID = 'advance-table-list';
  private headerURL = this._API_Config.API_User_Metas + '/log';
  private headerDef = [{id: 'ver_table', value: 3},
                    {id: 'evt_code', name: 'Error Code', width: 100},
                    {id: 'des', name: 'Error', width: 100},
                    {id: 'owner', name: 'Owner', width: 100},
                    {id: 'transaction', name: 'Transaction', width: 100},
                    {id: 'message', name: 'Message', width: 100},
                    {id: 'created_by', name: 'Created By', width: 100},
                    {id: 'created_at', name: 'Created At', width: 60},
                    {id: 'header', name: 'Header Data', width: 100, unsortable: true},
                    {id: 'request_data', name: 'Request Data', width: 100, unsortable: true},
                    {id: 'response_data', name: 'Response Data', width: 100, unsortable: true},
                    {id: 'url_endpoint', name: 'API Endpoint', width: 100},
                    ];
  private pinCols = 5;
  private rowData: any[];
  private getSelectedRow;
  private objSort;
  private searchParams;

  // pagination
  private Pagination;
  private totalCount = 0;
  private tmpPageCount;
  private pageCount=0;
  private ItemOnPage=0;
  private currentPage = 1;
  private perPage = 20;
  private numLinks = 3;

  private messages;
  private showLoadingOverlay = false;

  private warehouseId:string = '';

  // private defaultSortParams = 'sort[updated_at]=desc';
  private defaultSortParams = '';
  private logTypeData;
  private evtCodeData;

  constructor (private _boxPopupService: BoxPopupService, private _http: Http, private _router:Router, private _user: UserService, private _func: Functions, private _API_Config: API_Config, _title: Title, private _logsListService: LogsListService) {
    _title.setTitle('Log');
    this.warehouseId = localStorage.getItem('whs_id');
    this.getLogType();
    this.getEventCode();
    this.checkPermission();
    this.initModalEvent();    
  }

  private viewData;
  private modalTitle;
  private isTextData;

  private initModalEvent() {
    var _this = this;
    jQuery('body').on('click.showDetail', '[data-modal-content]', function(e) {
      var elm = jQuery(this),
          modalData = _this.data[elm.data('index')][elm.data('modal-content')],
          arrData = [];

      try {
        modalData = modalData ? JSON.parse(modalData) : {};
      }
      catch(err) {
      }
      if(modalData instanceof Object && !(modalData instanceof Array)) { 
        for(var x in modalData){
          let value = modalData[x];
          if(value instanceof Object && !(value instanceof Array)) {
            value = JSON.stringify(value);
          }
          arrData.push({
            key: x,
            value: value
          })
        }
        _this.isTextData = false;
        _this.viewData = arrData;
      }
      else {
        _this.isTextData = true;
        _this.viewData = modalData;
      }
      _this.modalTitle = elm.data('title');
      jQuery('#modal-content').modal('show');
      // console.log('arr data', arrData);
    });
  }

  private getLogType() {
    this._logsListService.getLogType(this.warehouseId)
      .subscribe(
        data => {
          this.logTypeData = data.data;
        },
        err => {
        }
      );
  }

  private getEventCode() {
    this._logsListService.getEventCode(this.warehouseId)
      .subscribe(
        data => {
          this.evtCodeData = data.data;
        },
        err => {
        }
      );
  }

  private checkPermission() {
      this.showLoadingOverlay = true;
      this._user.GetPermissionUser().subscribe(
          data => {
              // this.hasViewPermission = this._user.RequestPermission(data,'viewLog');
              this.hasViewPermission = true;
              if(!this.hasViewPermission) {
                  this._router.parent.navigateByUrl('/deny');
              }
              else {
                this.getList();
              }
          },
          err => {
              this.showMessage('danger', this._func.parseErrorMessageFromServer(err));
              this.showLoadingOverlay = false;
          }
      );
  }

  private showMessage(msgStatus, msg) {
      this.messages = {
          'status': msgStatus,
          'txt': msg
      }
      jQuery(window).scrollTop(0);
  }

  private filterList(pageNumber) {
      this.getList(pageNumber);
  }

  private getPage(pageNumber) {
      let arr=new Array(pageNumber);
      return arr;
  }

  private initPagination(data){
    let meta = data['meta'];
    if(meta['pagination']) {
      this.Pagination=meta['pagination'];
    }
    else {
      let perPage = meta['perPage'],
          curPage = meta['currentPage'],
          total = meta['totalCount'];
      let count = perPage * curPage > total ? perPage * curPage - total : perPage;
      this.Pagination = {
        "total": total,
        "count": count,
        "per_page": perPage,
        "current_page": curPage,
        "total_pages": meta['pageCount'],
        "links": {
          "next": data['link']['next']
        }
      }
    }
    this.Pagination['numLinks']=3;
    this.Pagination['tmpPageCount']=this._func.Pagination(this.Pagination);
  }

  private getList(page = null){
    this.showLoadingOverlay = true;
    if(!page) page = 1;
    let params="?page="+page+"&limit="+this.perPage;
    if(this.objSort && this.objSort['sort_type'] != 'none') {
      params += '&sort['+ this.objSort['sort_field']+ ']='+ this.objSort['sort_type'];
    }
    else {
      params += '&' + this.defaultSortParams;
    }
    if(this.searchParams) {
      params += this.searchParams;
    }

    this._logsListService.getList(params, this.warehouseId)
        .subscribe(
          data => {
            this.createRowData(data);
            this.showLoadingOverlay = false;
          },
          err => {
          	this.rowData = [];
      			this.Pagination = null;
            this.showMessage('danger', this._func.parseErrorMessageFromServer(err));
            this.showLoadingOverlay = false;
          }
        );
  }

  private onPageSizeChanged($event) {
      this.perPage = $event.target.value;
      if(this.Pagination && (this.Pagination['current_page'] - 1) * this.perPage >= this.Pagination['total']) {
          this.currentPage = 1;
      }
      else {
          this.currentPage = this.Pagination['current_page'];
      }
      this.getList(this.currentPage);
  }

    private onErrorTypeChanged($e) {
        var type = $e.target.value;
        var param = (type!='')?('?type='+type):'';
        this._logsListService.getEventCode(this.warehouseId, param)
          .subscribe(
            data => {
              this.evtCodeData = data.data;
            },
            err => {
            }
          );

    }

  private search() {
    this.searchParams = '';
    let params_arr = jQuery("#form-filter").serializeArray() ;
    for (let i in params_arr) {
        if (params_arr[i].value == "") continue;
        this.searchParams +="&" +  params_arr[i].name.trim() + "=" + encodeURIComponent(params_arr[i].value.trim());
    }
      this.getList(1);
  }
  // Format data for ag-grid
  private data;
  private createRowData(data) {
    var rowData: any[] = [];
    // Check data
    if (typeof data.data != 'undefined') {
      this.initPagination(data);

      data = this._func.formatData(data.data);
      this.data = data;
      for (var i = 0; i < data.length; i++) {
        rowData.push(({
            type: data[i].type,
            evt_code: data[i].evt_code,
            des: data[i].des,
            owner: data[i].owner,
            transaction: data[i].transaction,
            message: data[i].message,
            created_by: data[i].created_by,
            created_at: data[i].created_at,
            request_data: `<a href="javascript:;" data-title="Request Data" data-modal-content="request_data" data-index="${i}">View</a>`,
            header: `<a href="javascript:;" data-title="Header Data" data-modal-content="header" data-index="${i}">View</a>`,
            response_data: `<a href="javascript:;" data-title="Response Data" data-modal-content="response_data" data-index="${i}">View</a>`,
            url_endpoint: data[i].url_endpoint,
        }));
      }
    }
    else {
      this.rowData = [];
      this.Pagination = null;
    }

    this.rowData = rowData;    
  }

  private doSort(objSort) {
    this.objSort = objSort;
    this.getList(this.Pagination.current_page);
  }

  private reset() {
      jQuery("#form-filter input, #form-filter select").each(function( index ) {
          jQuery(this).val("");
      });
      this.searchParams = '';
      this.getList(1);
  }

  private expandTable = false;
  private viewListFullScreen() {
    this.expandTable = true;
    setTimeout(() => {
      this.expandTable = false;
    }, 500);
  }
}
