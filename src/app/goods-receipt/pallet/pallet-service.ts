import {Component, Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {API_Config,Functions} from '../../common/core/load';

@Injectable()
export class PalletServices {

    constructor(private _Func: Functions,
                private _API: API_Config,
                private http: Http) {
    }

    getGoodReceiptList(whsID, gdId:any,param:any){
        return this.http.get(this._API.API_GOODS_RECEIPT+'/whs/' + whsID + '/pallet-cartons/'+gdId +param, { headers: this._Func.AuthHeader() })
            .map((res: Response) => res.json());
    }

    updateCtns(whsID, gdId, data:any){
        return this.http.put(this._API.API_GOODS_RECEIPT+'/whs/' + whsID + '/pallet-cartons/'+gdId ,data, { headers: this._Func.AuthHeaderPostJson() })
            .map((res: Response) => res.json());
    };

    updateExpireDate(whsID, gdDetailId, palletID, data:any){
        return this.http.put(this._API.API_GOODS_RECEIPT+'/whs/' + whsID + '/pallet-cartons/'+palletID + '/' + gdDetailId + '/update-expired-date' ,data, { headers: this._Func.AuthHeaderPostJson() })
            .map((res: Response) => res.json());
    };

    updateQty(whsID, gdId, data:any){
        return this.http.put(this._API.API_GOODS_RECEIPT+'/whs/' + whsID + '/pallet-cartons-qty/' + gdId, data, { headers: this._Func.AuthHeaderPostJson() })
            .map((res: Response) => res.json());
    };

    autoSearchLocation(whsID, param: string){
        return this.http.get(this._API.API_GOODS_RECEIPT+'/whs/' + whsID + '/auto-avail-location' +param, { headers: this._Func.AuthHeader() })
            .map((res: Response) => res.json());
    }

    autoSearchLocationOrderIdNotNull(whsID, param: string){
        return this.http.get(this._API.API_GOODS_RECEIPT+'/whs/' + whsID + '/auto-avail-location-xdock'+ param, { headers: this._Func.AuthHeader() })
            .map((res: Response) => res.json());
    }

    updateLocation(whsID, gdId, data:any){
        return this.http.put(this._API.API_GOODS_RECEIPT + '/whs/' + whsID + '/pallet-cartons/' + gdId + '/location' ,data, { headers: this._Func.AuthHeaderPostJson() })
            .map((res: Response) => res.json());
    };
    
    updateLocationXDock(whsID, gdId, data:any){
        return this.http.put(this._API.API_GOODS_RECEIPT + '/whs/' + whsID + '/pallet-cartons/' + gdId + '/location-xdock' ,data, { headers: this._Func.AuthHeaderPostJson() })
            .map((res: Response) => res.json());
    };

}
