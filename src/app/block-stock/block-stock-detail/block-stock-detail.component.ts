import {Component, DynamicComponentLoader, ElementRef, Injector} from '@angular/core';
import { FORM_DIRECTIVES,ControlGroup,FormBuilder, Control ,Validators} from '@angular/common';
import { Http } from '@angular/http';
import { Router, RouterLink, ROUTER_DIRECTIVES, RouteParams, RouteData } from '@angular/router-deprecated';
import 'ag-grid-enterprise/main';
import 'rxjs/add/operator/map';
import {API_Config} from "../../common/core/API_Config";
import {Functions} from "../../common/core/functions";
import {BlockStockServices} from "../block-stock-service";
import {JwtHelper} from "angular2-jwt/angular2-jwt";
import {WMSMessages, WMSBreadcrumb} from '../../common/directives/directives';
import {GoBackComponent} from "../../common/component/goBack.component";
import {UserService} from "../../common/users/users.service";
declare var jQuery: any;
declare var saveAs: any;

@Component({
  selector: 'cycle-count-detail',
  directives: [
    WMSBreadcrumb,
    GoBackComponent,
    WMSMessages
  ],
  providers: [BlockStockServices],
  templateUrl: 'block-stock-detail.component.html',
})

export class BlockStockDetailComponent {
  private BlockID: any;
  private messages: any;
  private messagesAddSKU: any;
  private cycleDetail = {};
  public listSelectedItem = [];
  private isUpdate = false;
  private action;
  private perPage = 20;
  private currentPage = 1;
  private sortQuery = '';
  public loading = [];
  public showLoadingOverlay = false;
  private selectedAll = false;
  private countItemList: Array<any> = [];
  private urlAPICC = '';
  private whs_id = '';
  private currentUserID = {};
  private hasPrintPermission = false;
  private hasAddSkuPermission = false;
  private hasUpdateItem = false;
  private disabled = true;
  private blockDetail = {};

  private Pagination = {
    count: 0,
    current_page: this.currentPage,
    per_page: this.perPage,
    total: 0,
    total_pages: 1,
    links: {
      next: ""
    }
  };
  searchForm: ControlGroup;

  constructor(private _router:Router,
              private _Func: Functions,
              private _blockStockServices: BlockStockServices,
              params: RouteParams,
              _RTAction: RouteData,
              private _API_Config: API_Config,
              private _user: UserService) {

    this.checkPermission();

    if (localStorage.getItem('whs_id')) {

      this.whs_id = localStorage.getItem('whs_id');
    }

    this.BlockID = params.get('id');

    this.getCountItemByBlockID(1);
    this.getDetailBlockStock();

  }

  /*
   *********************************************************************************************************************
   */
  private checkPermission() {

    this._user.GetPermissionUser().subscribe(

      data => {

        var isView = this._user.RequestPermission(data,'viewBlockStock');

        if(!isView) {
          this._router.parent.navigateByUrl('/deny');
        }

      },
      err => {

        this.showMessage('danger', this._Func.parseErrorMessageFromServer(err));

      }
    );
  }



  /*
   *********************************************************************************************************************
   */

  private showMessage(msgStatus, msg) {
    this.messages = {
      'status': msgStatus,
      'txt': msg
    };
    jQuery(window).scrollTop(0);
  }

  /*
   *********************************************************************************************************************
   */

  private getCountItemByBlockID(page=null) {

    let param = "?page=" + page + "&limit=" + this.perPage;

    if(this.sortQuery != '') {
      param += '&'+this.sortQuery;
    }

    this.loading['getCycleCount'] = true;
    this.showLoadingOverlay = true;

    this._blockStockServices.getCountItemByBlockID(this.BlockID, param).subscribe(
      data =>{

        this.selectedAll = false;
        this.countItemList = data.data;

        this.Pagination = data['meta']['pagination'];
        this.Pagination['numLinks'] = 3;
        this.Pagination['tmpPageCount'] = this._Func.Pagination(this.Pagination);

        this.loading['getCycleCount'] = false;
        this.showLoadingOverlay = false;
      },
      err => {

        this.showLoadingOverlay = false;
        this.showMessage('danger', this._Func.parseErrorMessageFromServer(err));
      },
      () => {

        this.showLoadingOverlay = false;
      }
    );
  }

  /*
   *********************************************************************************************************************
   */

  private onPageSizeChanged($event) {
    this.perPage = $event.target.value;

    if((this.Pagination['current_page'] - 1) * this.perPage >= this.Pagination['total']) {
      this.currentPage = 1;
    }
    else {
      this.currentPage = this.Pagination['current_page'];
    }
    this.getCountItemByBlockID(this.currentPage);
  }

  /*
   *********************************************************************************************************************
   */

  private checkSelectItem($event) {
    let id = $event.target.value;
    this.selectedAll=false;
    if ($event.target.checked) {
      this.listSelectedItem.push(id);
      this.setItemListSelected(id, true);

      if(this.listSelectedItem.length==this.countItemList.length)
      {
        this.selectedAll=true;
      }
    }
    else {
      let el = this.listSelectedItem.indexOf(id);
      // remove item out of array
      this.listSelectedItem.splice(el, 1);
      this.setItemListSelected(id, false);
    }
  }

  /*
   *********************************************************************************************************************
   */

  private filterList(num) {
    this.getCountItemByBlockID(num);
  }

  /*
   *********************************************************************************************************************
   */

  private editSelectedCountItem() {
    if (this.listSelectedItem.length > 1) {
      this.messages = this._Func.Messages('danger',this._Func.msg('CC006'));
    }
    else {
      if(this.listSelectedItem.length) {
        this._router.navigateByUrl('/cycle-count/' + this.BlockID + '/edit/' + this.listSelectedItem[0]);
      }
      else {
        this.messages = this._Func.Messages('danger',this._Func.msg('CC007'));
      }
    }
  }

  /*
   *********************************************************************************************************************
   */

  private setItemListSelected(id, value) {

    this.countItemList.forEach((item) => {

      if(item['block_dtl_id'] == id) {
        item['selected'] = value;
      }
    });
  }

  /*
   *********************************************************************************************************************
   */

  private checkAll(event) {
    this.selectedAll = !this.selectedAll;
    // set empty array  ListSelectedItem
    this.listSelectedItem=[];
    // Loop
    this.countItemList.forEach((item) => {
      item['selected'] = this.selectedAll;
      if(this.selectedAll) {
        // if check all = true , pushing to ListSelectedItem
        this.listSelectedItem.push(item['block_dtl_id']);
      }
    })
  }

  private parseError(err) {

    this.messages = {'status': 'danger', 'txt': this._Func.parseErrorMessageFromServer(err)};
  }

  private getDetailBlockStock() {

    this._blockStockServices.getDetailBlockStock(this.BlockID).subscribe(
        data =>{

          this.blockDetail = data;

        },
        err => {

          this.showMessage('danger', this._Func.parseErrorMessageFromServer(err));
        },
        () => {}
    );
  }

  private unblock() {

    try {

      if (this.listSelectedItem.length < 1) {

        this.messages = this._Func.Messages('danger',this._Func.msg('WMS001'));
        return false;
      }

      var tar_get = this;
      var params = JSON.stringify({"block_dtl_ids" : this.listSelectedItem}) ;
      this.showLoadingOverlay = true;
      this.messages = false;
      
      this._blockStockServices.addUnblockBlockStock(this.BlockID, params)
          .subscribe(

              data => {

                this.messages = this._Func.Messages('success', this._Func.msg('VR109'));
              },
              err => {

                this.parseError(err);
                this.showLoadingOverlay = false;
              },
              () => {

                setTimeout(function(){

                  tar_get._router.parent.navigateByUrl('/block-stock/create');
                }, 1100);

                tar_get.showLoadingOverlay = false;

              }
          );

      } catch (err) {

    }

  }

  private cancel() {

    this._router.parent.navigateByUrl('/block-stock/create');
  }


}
