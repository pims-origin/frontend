import {Component, Input, Output, EventEmitter} from '@angular/core';

import {Functions} from '../../../common/core/load';

declare var $:any;
declare var saveAs:any;

@Component({
	selector: 'report-file-exporter',
	templateUrl: 'report-file-exporter.html',
})

export class ReportFileExporter {
	private exportedReportAPIUrl:string;
	private namedFile:string;
	private showLoadingOverlay =false;

	@Input('exportAPIUrl') set exportAPIUrl(value:any) {
		this.exportedReportAPIUrl = value;
	}

	@Input('fileName') set fileName(value:any) {
		this.namedFile = value;
	}

	@Output() bindMessages  = new EventEmitter();

	constructor(private funs:Functions) {
	}

	private exportReportCSV() {
		try {
			const that = this;

			let xhr = new XMLHttpRequest();
			xhr.open("GET", this.exportedReportAPIUrl, true);
			xhr.setRequestHeader("Authorization", 'Bearer ' + this.funs.getToken());
			xhr.responseType = 'blob';

			that.showLoadingOverlay = true;
			xhr.onreadystatechange = function () {
				if (xhr.readyState == 2) {
					if (xhr.status == 200) {
						xhr.responseType = "blob";
					} else {
						xhr.responseType = "text";
					}
				}
				// If we get an HTTP status OK (200), save the file using fileSaver
				if (xhr.readyState === 4) {
					if (xhr.status === 200) {
						let blob = new Blob([this.response], {type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'});
						saveAs.saveAs(blob, that.namedFile);

					} else {
						let errMsg = '';
						if (xhr.response) {
							try {
								var res = JSON.parse(xhr.response);
								errMsg = res.errors && res.errors.message ? res.errors.message : xhr.statusText;
							}
							catch (err) {
								errMsg = xhr.statusText
							}
						}
						else {
							errMsg = xhr.statusText;
						}
						if (!errMsg) {
							errMsg = that.funs.msg('VR100');
						}
						that.bindMessages.emit(that.funs.Messages('danger', errMsg));
					}
					that.showLoadingOverlay = false;
				}
			};

			xhr.send();
		} catch (err) {
			this.bindMessages.emit(this.funs.Messages('danger', this.funs.msg('VR100')));
			this.showLoadingOverlay = false;
		}
	}
}
