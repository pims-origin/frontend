export class CRUUserMessages{
  public messages = {
  	"remove_none": "Please select one item to remove",
  	"remove_confirm": "Do you want to remove these items?",
  	"remove_warehouse_confirm": "Do you want to remove warehouse/warehouses?",
  	"remove_customer_confirm": "Do you want to remove customer/customers?",
  	"remove_role_confirm": "Do you want to remove role/roles?",
    "connection": "Sorry, there's a problem with the connection.",
    "add_new_success": "Add new successfully",
    "update_success": "Update successfully"
  }
}
