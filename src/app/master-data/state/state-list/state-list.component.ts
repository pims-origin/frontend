import {Component} from '@angular/core';
import { Router } from '@angular/router-deprecated';
import {API_Config, UserService, Functions} from '../../../common/core/load';
import {StateService} from '../state-service';
import {CountryService} from '../../country/country-service';
import { FORM_DIRECTIVES,ControlGroup,FormBuilder, Control} from '@angular/common';
import { OrderBy } from "../../../common/pipes/order-by.pipe";
import { Http } from '@angular/http';
import {WMSBreadcrumb, WMSPagination} from '../../../common/directives/directives';
declare var jQuery: any;
@Component ({
    selector: 'state-list',
    providers: [StateService, CountryService, FormBuilder],
    directives: [FORM_DIRECTIVES,WMSBreadcrumb, WMSPagination],
    pipes: [OrderBy],
    templateUrl: 'state-list.component.html',

})

export class StateListComponent {
    private stateList: Array<any> = [];
    private countryList: Array<any> = [];
    private selectedAll = false;
    private Pagination = [];
    private totalCount = 0;
	private tmpPageCount;
	private pageCount=0;
	private ItemOnPage=0;
	private currentPage = 1;
	private perPage = 20;
	private numLinks = 3;
    public listSelectedItem = [];
    private messages;
    searchForm: ControlGroup;
    public loading = [];
    public showLoadingOverlay = false;
    private hasEditPermission = false;
    private hasCreatePermission = false;
    private viewUser = false;
    private searchCriteria = {};
    constructor(
        private _http: Http,
        private _func: Functions,
        private _user: UserService,
        private fb: FormBuilder,
        private _stateService: StateService,
        private _countryService: CountryService,
        private _router: Router) {

        this.builderSearchForm();
        this.searchCriteria = this._func.genSearchCriteriaFromForm(this.searchForm);
        this.checkPermission();
    }

    // Check permission for user using this function page
    private checkPermission() {
        this.showLoadingOverlay = true;
        this._user.GetPermissionUser().subscribe(
            data => {
                this.hasEditPermission = this._user.RequestPermission(data, 'editState');
                this.hasCreatePermission = this._user.RequestPermission(data, 'createState');
                this.viewUser = this._user.RequestPermission(data,'viewState');
                /* Check orther permission if View allow */
                if(!this.viewUser) {
                    this._router.parent.navigateByUrl('/deny');
                }
                else {
                    this.getStateList();
                    this.getCountryList();
                }
            },
            err => {
                this.showMessage('danger', this._func.parseErrorMessageFromServer(err));
                this.showLoadingOverlay = false;
            }
        );
    }

    private showMessage(msgStatus, msg) {
        this.messages = {
            'status': msgStatus,
            'txt': msg
        }
        jQuery(window).scrollTop(0);
    }

    private getCountryList(page = null) {
        this.showLoadingOverlay = true;
        this._countryService.Country().subscribe(
            data => {
                this.countryList = data.data;
                this.showLoadingOverlay = false;
            },
            err => {},
            () => {}
        );
    }

    private getStateList(page = null) {
        let param="?page="+page+"&limit="+this.perPage;
        if(Object.keys(this.searchCriteria).length !== 0) // check if searchCriteria Object not empty  ( {} )
        {
            param += '&sys_state_code=' + this.searchCriteria['sys_state_code'] +
                '&sys_state_name=' + this.searchCriteria['sys_state_name'];
        }
        if(this.searchCriteria['sys_state_country_id']) {
            param += '&sys_state_country_id=' + this.searchCriteria['sys_state_country_id'];
        }

        if(this.sortQuery != '') {
            param += '&'+this.sortQuery;
        }

        this.loading['getState'] = true;
        this.showLoadingOverlay = true;
        this._stateService.getStateList(param).subscribe(
            data => {
                // console.log('load states success', data.data);
                this.selectedAll = false;
                this.listSelectedItem=[];
                this.stateList = data.data;
                //pagin function
		        this.Pagination=data['meta']['pagination'];
		        this.Pagination['numLinks']=3;
		        this.Pagination['tmpPageCount']=this._func.Pagination(this.Pagination);
                this.loading['getState'] = false;
                this.showLoadingOverlay = false;
            },
            err => {},
            () => {}
        );
    }

    private checkSelectItem(item) {
        let id = item['sys_state_id'];
        item.selected = !item.selected;
        this.selectedAll = false;
        if (item.selected) {
            this.listSelectedItem.push(id);
            this.setItemListSelected(id, true);

            if(this.listSelectedItem.length==this.stateList.length)
            {
                this.selectedAll=true;
            }
        }
        else {
            let el = this.listSelectedItem.indexOf(id);
            // remove item out of array
            this.listSelectedItem.splice(el, 1);
            this.setItemListSelected(id, false);
        }
    }

    private setItemListSelected(id,value) {
        this.stateList.forEach((item) => {
            if(item['sys_state_id'] == id) {
                item['selected'] = value;
            }
        });
    }

    private checkAll(event) {
        this.selectedAll = !this.selectedAll;
        // set empty array  ListSelectedItem
        this.listSelectedItem=[];
        // Loop
        this.stateList.forEach((item) => {
            item['selected'] = this.selectedAll;
            if(this.selectedAll) {
                // if check all = true , pushing to ListSelectedItem
                this.listSelectedItem.push(item['sys_state_id']);
            }
        })
    }

    private editSelectedItem() {
        if (this.listSelectedItem.length > 1) {
            this.messages = this._func.Messages('danger',this._func.msg('CF004'));
        }
        else {
            if(this.listSelectedItem.length) {
                this._router.navigateByUrl('/master-data/state/' + this.listSelectedItem[0] + '/edit');
            }
            else {
                this.messages = this._func.Messages('danger',this._func.msg('CF005'));
            }
        }
    }

    private onPageSizeChanged($event) {
	    this.perPage = $event.target.value;
        if((this.Pagination['current_page'] - 1) * this.perPage >= this.Pagination['total']) {
            this.currentPage = 1;
        }
        else {
            this.currentPage = this.Pagination['current_page'];
        }
	    this.getStateList(this.currentPage);
	}

	private filterList(num) {
        this.currentPage = num;
		this.getStateList(num);
	}

	private getPage(num) {
		let arr=new Array(num);
		return arr;
	}

    private builderSearchForm() {
        this.searchForm =this.fb.group({
          'sys_state_code': [''],
          'sys_state_name': [''],
          'sys_state_country_id': ['']
        });
    }

    search(data) {

        this.messages = null;
        let param="?page="+1+"&limit="+this.perPage;
        this.searchCriteria = this._func.genSearchCriteriaFromForm(this.searchForm);
        let encodeSearchQuery = param + "&sys_state_code="+encodeURIComponent(this.searchCriteria['sys_state_code'])+
                        "&sys_state_name="+encodeURIComponent(this.searchCriteria['sys_state_name']);
        if(this.searchCriteria['sys_state_country_id']) {
            encodeSearchQuery += "&sys_state_country_id="+this.searchCriteria['sys_state_country_id'];
        }
        if(this.sortQuery != '') {
            encodeSearchQuery += '&'+this.sortQuery;
        }
        this.loading['searching']=true;
        this.showLoadingOverlay = true;
        this._stateService.search(encodeSearchQuery).subscribe(
          data => {
            // disable loading icon
            this.loading['searching']=false;
            this.selectedAll = false;
            this.listSelectedItem=[];
            this.stateList = data.data;
            //pagin function
            this.Pagination=data['meta']['pagination'];
            this.Pagination['numLinks']=3;
            this.Pagination['tmpPageCount']=this._func.Pagination(this.Pagination);
            this.showLoadingOverlay = false;
          },
          err =>{
            this.messages = this._func.Messages('danger','Not found');
          },
          () => {}
        );
    }

    resetSearch(data) {
        // if(data.sys_state_code || data.sys_state_name || data.sys_country_id) {
            (<Control>this.searchForm.controls["sys_state_code"]).updateValue("");
            (<Control>this.searchForm.controls["sys_state_name"]).updateValue("");
            (<Control>this.searchForm.controls["sys_state_country_id"]).updateValue("");
            this.searchCriteria = this._func.genSearchCriteriaFromForm(this.searchForm);
            this.getStateList(this.Pagination['current_page']);
        // }
    }

    private FieldsToSortFlag = {
                                sys_state_code: false,
                                sys_state_name: false,
                                sys_country_name: false,
                            };
    private countClick = {
        sys_state_code: 3,
        sys_state_name: 3,
        sys_country_name: 3,
    };
    private sortQuery = 'sort[sys_state_code]=asc';
    sort(field) {
        if(this.countClick[field] == 3) {
            this.countClick[field] = 0;
        }

        this.countClick[field] ++;
        if(this.countClick[field] == 3) {
            this.sortQuery = 'sort[sys_state_code]=asc';
        }
        else {
            this.FieldsToSortFlag[field] = !this.FieldsToSortFlag[field];

            this.sortQuery = 'sort['+field+']';
            if(this.FieldsToSortFlag[field]) {
                this.sortQuery += '=asc';
            } else {
                this.sortQuery += '=desc';
            }
        }

        if(field) {
            for (let sortField in this.FieldsToSortFlag ) {
                if(sortField != field) {
                    this.FieldsToSortFlag[sortField] = false;
                    this.countClick[sortField] = 3;
                }
            }
        }
        this.getStateList(this.currentPage);
    }
}
