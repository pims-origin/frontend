import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { API_Config, Functions } from '../../common/core/load';

@Injectable()
export class LPNReportService {
  authHeader: any;
  authHeaderPost: any;
  whsId: string;

  constructor(private func: Functions, private api: API_Config, private http: Http) {
    this.authHeader = func.AuthHeader();
    this.authHeaderPost = func.AuthHeaderPost();
    this.whsId = localStorage.getItem('whs_id');
  }

  getReports(params: string = '') {
    return this.http
      .get(`${this.api.API_REPORTS_CORE}/lpn/reports/${this.whsId}` + params, { headers: this.authHeader })
      .map((res) => res.json());
  }

  getReportAPIUrl(params: string = '') {
    return `${this.api.API_REPORTS_CORE}/lpn/reports/${this.whsId}${params}&export=1`;
  }

  getCustomers(params: string = '') {
    return this.http
      .get(`${this.api.API_REPORTS_CORE}/pallet/customers/${this.whsId}` + params, { headers: this.authHeader })
      .map((res) => res.json());
  }

  getGRNums(params: string = '') {
    return this.http
      .get(`${this.api.API_GOODS_RECEIPT_REPORT}/auto-gr-num/${this.whsId}` + params, { headers: this.authHeader })
      .map((res) => res.json());
  }

  getRefCodes(params: string = '') {
    return this.http
      .get(`${this.api.API_GOODS_RECEIPT_REPORT}/auto-ref-code/${this.whsId}` + params, { headers: this.authHeader })
      .map((res) => res.json());
  }

  getSKUs(params: string = '') {
    return this.http
      .get(`${this.api.API_REPORTS}/pallet/auto-sku/${this.whsId}` + params, { headers: this.authHeader })
      .map((res) => res.json());
  }

  getCartonsList(palletID, params: string = '') {
    return this.http
      .get(`${this.api.API_GOODS_RECEIPT_MASTER}/pallet/reports/${palletID}/cartons-list` + params, { headers: this.authHeader })
      .map((res) => res.json());
  }

  getAutocompleteLocation(params: string = '') {
    return this.http.get(`${this.api.API_REPORTS_CORE}/auto-complete` + params, { headers: this.authHeader }).map((res) => res.json());
  }
}
