
import {Component, Output, EventEmitter} from '@angular/core';
import {API_Config} from '../../../common/core/load';
import {ModalForm} from '../../../common/core/classes_us/modal-form';
import {WMSMessages} from '../../../common/directives/directives';
import {TableFunctions} from '../../../common/core/table-functions';
import {BaseService} from '../../../common/core/classes_us/base-service';
import {PopupServices} from '../../../common/directives/popup-modal_us/add/add-service';
import {OrderBy} from '../../../common/pipes/order-by.pipe';

@Component({
    selector: 'receive-payment',
    directives: [
        WMSMessages
    ],
    providers: [
        PopupServices
    ],
    pipes: [OrderBy],
    templateUrl: '../../../common/directives/popup-modal_us/add/add.html'
})

export class ReceivePayment extends ModalForm
{
    @Output() notify: EventEmitter<any> = new EventEmitter<string>();

    private inv_num: number;
    private index: number;

    private data = {
        id: 'receive-paymen',
        title: 'Receive Payment',
        class_name: 'receive-payment-modal-form',
        caption_class: 'receive-payment-caption',
        value_class: 'receive-payment-value',
        items: [
            {
                caption: 'Paid Date',
                name: 'inv_paid_dt',
                class_name: 'receive-payment-date',
                value: '',
                datePicker: true,
                mandatory: true
            },
            {
                caption: 'Paid Type',
                name: 'inv_paid_typ',
                class_name: 'receive-payment-item',
                value: '',
                mandatory: true
            },
            {
                caption: 'Paid Reference #',
                name: 'inv_paid_ref',
                class_name: 'receive-payment-item',
                value: '',
                mandatory: true
            }
        ]
    };

    //**************************************************************************

    constructor(
        private _api: API_Config,
        private _tableFunc: TableFunctions,
        private _popupServices: PopupServices,
        private _baseService: BaseService
    )
    {
        super();
    }

    //**************************************************************************

    public showInfo(tableComponent, inv_num: number, index: number)
    {
        this.inv_num = inv_num;
        this.index = index;
        this.parentComponent = tableComponent;

        for(let key in this.data.items) {
            this.data.items[key].value = '';
        }

        this.isVisible = true;
    }

    //**************************************************************************

    public submitInfo()
    {
        let params = this._popupServices.checkValues(this);

        if (params) {

            params['whs_id'] = this.parentComponent.get('whs_id');

            let url = this._api.API_Invioce_Receive_Payment + '/' + this.inv_num,
                data = JSON.stringify(params);

            this.showLoadingOverlay = true;

            this._baseService.post(url, data)
                .subscribe(
                    data => {

                        this.isVisible = false;

                        this.notify.emit({
                            pmnt_rcv_dt: params['inv_paid_dt'],
                            check_num: params['inv_paid_ref'],
                            index: this.index
                        });
                    },
                    err => {
                        this.messages = this._tableFunc.parseError(err);
                        this.notify.emit({
                            pmnt_rcv_dt: 'error',
                            check_num: 'error',
                            index: this.index
                        });
                        this.isVisible = false;
                    },
                    () => {
                        this.showLoadingOverlay = false;
                    }
            );
        }
    }

    //**************************************************************************

}
