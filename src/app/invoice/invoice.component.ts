
import {Component} from '@angular/core';
import {RouteConfig, ROUTER_DIRECTIVES} from '@angular/router-deprecated';
import {ChargeCodeListComponent} from '../master-data/charge-code/charge-code-list/charge-code-list.component';
import {InvoiceListComponent} from './invoice-list/invoice-list.component';
import {CreateInvoiceComponent} from './invoice-list/invoice-issue/create-invoice.component';
import {DisplayInvoiceComponent} from './invoice-list/invoice-issue/display-invoice.component';
import {InvoiceCostsComponent} from './invoice-costs/invoice-costs.component';
import {ReceivingDetailsComponent} from './invoice-list/invoice-issue/view-details/receiving-details.component';
import {OrderProcessingDetailsComponent} from './invoice-list/invoice-issue/view-details/order-processing-details.component';

@Component ({
    selector: 'invoice-management',
    directives: [ROUTER_DIRECTIVES],
    template: `<router-outlet></router-outlet>`,
})

@RouteConfig([
    {   path: '/charge-code',
        component: ChargeCodeListComponent,
        name: 'Charge Code List', data: {
            action:'view'
        }
    },
    {
        path: '/invoice-list',
        component: InvoiceListComponent,
        name: 'Invoice List'
    },
    {
        path: '/invoice-issue/create/:inv_cr_id',
        component: CreateInvoiceComponent,
        name: 'Create Invoice'
    },
    {
        path: '/invoice-issue/display/:inv_num',
        component: DisplayInvoiceComponent,
        name: 'Display Invoice'
    },
    {
        path: '/invoice-costs/:cus_id',
        component: InvoiceCostsComponent,
        name: 'Invoice Costs'
    },
    {
        path: '/view-details/receiving/:gr_hdr_id',
        component: ReceivingDetailsComponent,
        name: 'View Container Details'
    },
    {
        path: '/view-details/order-processing/:odr_id',
        component: OrderProcessingDetailsComponent,
        name: 'View Order Details'
    }
])

export class InvoicesManagementComponent
{

}
