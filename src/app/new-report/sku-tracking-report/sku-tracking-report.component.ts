import { Component, NgZone } from '@angular/core';
import { Router, ROUTER_DIRECTIVES } from '@angular/router-deprecated';
import { ControlGroup, FormBuilder, Control } from '@angular/common';
import { API_Config, UserService, Functions } from '../../common/core/load';
import { WMSPagination, AgGridExtent, WMSMessages, ReportFileExporter } from '../../common/directives/directives';
import 'ag-grid-enterprise/main';
import { SKUTrackingService } from './sku-tracking-report.service';
import { BoxPopupService } from '../../common/popup/box-popup.service';

declare const $: any;

@Component({
	selector: 'sku-tracking-report',
	providers: [SKUTrackingService, BoxPopupService],
	directives: [ROUTER_DIRECTIVES, AgGridExtent, WMSMessages, WMSPagination, ReportFileExporter],
	templateUrl: 'sku-tracking-report.component.html',

})
export class SKUTrackingReportComponent {

	headerURL = `${this.apiService.API_User_Metas}/str`;
	columnData = {
		cus_name: {
			title: 'Customer Name',
			id: true,
			ver_table: '8',
			pin: true
		},
		trans_num: {
			title: 'Trans Num',
			pin: true
		},
		ref_cus_order: {
			title: 'Ref/Cus Order #',
			pin: true
		},
		po_ctnr: {
			title: 'PO/Carrier',
			pin: true
		},
		actual_date: {
			title: 'Actual Date',
			pin: true,
			sort: true
		},
		sku: {
			title: 'SKU',
			pin: true
		},
		size: {
			title: 'Size'
		},
		color: {
			title: 'Color'
		},
		lot: {
			title: 'Lot'
		},
		pack: {
			title: 'Pack'
		},
		ctns: {
			title: 'CTNS'
		},
		qty: {
			title: 'QTY'
		},
		cube: {
			title: 'Cube',
			sort: true
		}
	};

	Pagination;
	perPage = 20;
	whs_id;
	messages;
	searchForm: ControlGroup;
	listActivity: any[] = [];
	showLoadingOverlay: boolean = false;
	customers: any[] = [];
	currentSKU: any = {};
	isLoadingSKUs: boolean = false;
	SKUs: string[] = [];
	sortData = {fieldname: 'actual_date', sort: 'desc'};

	// Check permission for user using this function page
	canAccess: boolean = false;

	// Export csv file
	exportAPIUrl: string = '';
	fileName:string = 'Sku-tracking-report.csv';

	constructor(private formBuilder:FormBuilder,
	            private _service: SKUTrackingService,
	            private _Func:Functions,
	            private _user:UserService,
	            private _router:Router,
	            private apiService:API_Config,
	            public lc:NgZone) {
		this.whs_id = localStorage.getItem('whs_id');
		this.buildSearchForm();
		this.checkPermission();
		this.initializeCurSKU();

	}

	private buildSearchForm() {
		this.searchForm = this.formBuilder.group({
			cus_id: [''],
			sku: [''],
			item_id: [''],
			trans_num: [''],
			ref_cus_order: [''],
			po_ctnr: [''],
			actual_date_from: [''],
			actual_date_to: [''],
		});
	}

	private initializeCurSKU() {
		this.currentSKU = {size: '', color: '', lot: ''};
	}

	private checkPermission() {
		this.showLoadingOverlay = true;
		this._user.GetPermissionUser().subscribe(
			data => {
				this.showLoadingOverlay = false;
				this.canAccess = this._user.RequestPermission(data, 'viewPerformanceActivity');
				if (this.canAccess) {
					this.getCustomers();
					this.getReport();
				}
				else {
					this._router.parent.navigateByUrl('/deny');
				}
			},
			err => {
				this.messages = this._Func.Messages('danger', this._Func.parseErrorMessageFromServer(err));
				this.showLoadingOverlay = false;
			}
		);
	}

	onPageSizeChanged($event) {
		this.perPage = $event.target.value;
		if (this.Pagination) {
			if ((this.Pagination['current_page'] - 1) * this.perPage >= this.Pagination['total']) {
				this.Pagination['current_page'] = 1;
			}
			this.getReport(this.Pagination['current_page']);
		}

	}


	getReport(page:number = 1) {
		const sortField = this.sortData['fieldname'] ? this.sortData['fieldname'] : 'actual_date';
		const sortValue = this.sortData['sort'] ? this.sortData['sort'] : 'desc';
		this.searchForm.value['lot'] = this.currentSKU['lot'];
		this.searchForm.value['page'] = page;
		this.searchForm.value['limit'] = this.perPage;
		this.searchForm.value['actual_date_from'] = $("#actual_date_from").val();
		this.searchForm.value['actual_date_to'] = $("#actual_date_to").val();

		const params = '?' + $.param(this.searchForm.value) + '&sort['+sortField+']=' + sortValue;
		this.exportAPIUrl = this._service.getReportAPIUrl(this.whs_id, params);
		
		this.showLoadingOverlay = true;
		this._service.getReport(this.whs_id, params).subscribe(
			data => {
				this.showLoadingOverlay = false;
				this.listActivity = data.data;

				//paging function
				this.Pagination = data['meta']['pagination'];
				this.Pagination['numLinks'] = 3;
				this.Pagination['tmpPageCount'] = this._Func.Pagination(this.Pagination);
			},
			err => {
				this.parseError(err);
				this.showLoadingOverlay = false;
			},
			() => {}
		);

	}

	getCustomers() {
		// const params = `/${this.whs_id}?limit=100`;
		const params = `?limit=100`;

		this._service.getCustomersByUser(params).subscribe(
			data => {
				this.customers = data.data;
			},
			err => {
				this.parseError(err);
			},
			() => {}
		);
	}

	loadSKUs($event:any) {
		const cusId = this.searchForm.value['cus_id'];
		const skuKeySearch = this.searchForm.value['sku'].trim();
		const params = `/${this.whs_id}?cus_id=${cusId}&sku=${skuKeySearch}&limit=20`;
		const enCodeSearchQuery = this._Func.FixEncodeURI(params);

		this.initializeCurSKU();
		if (!skuKeySearch) {
			return;
		}
		this.isLoadingSKUs = true;
		this._service.getSKUs(enCodeSearchQuery).debounceTime(400).distinctUntilChanged().subscribe(
			data => {
				this.isLoadingSKUs = false;
				this.SKUs = data['data'];
			},
			err => {
				this.isLoadingSKUs = false;
				this.messages = this._Func.Messages('danger', err.json().errors.message);
			},
			() => {
			}
		);
	}

	selectSKU($event, sku) {
		this.currentSKU = sku;
		(<Control>this.searchForm.controls['sku']).updateValue(sku.sku);
		(<Control>this.searchForm.controls['item_id']).updateValue(sku.item_id);
	}

	resetSearchForm() {
		(<Control>this.searchForm.controls['cus_id']).updateValue('');
		(<Control>this.searchForm.controls['sku']).updateValue('');
		(<Control>this.searchForm.controls['item_id']).updateValue('');
		(<Control>this.searchForm.controls['trans_num']).updateValue('');
		(<Control>this.searchForm.controls['ref_cus_order']).updateValue('');
		(<Control>this.searchForm.controls['po_ctnr']).updateValue('');
		(<Control>this.searchForm.controls['actual_date_from']).updateValue('');
		(<Control>this.searchForm.controls['actual_date_to']).updateValue('');
		$('#actual_date_from').val('');
		$('#actual_date_to').val('');
	}

	reset() {
		this.resetSearchForm();
		this.initializeCurSKU();
		this.getReport();
	}

	search() {
		if (!this.searchForm.value['sku']) {
			(<Control>this.searchForm.controls['item_id']).updateValue('');
		}
		this.getReport();
	}

	updateMessages(messages) {
		this.messages = messages;
	}

	private parseError(err) {
		err = err.json();
		this.messages = this._Func.Messages('danger', err.errors.message);
	}
}
