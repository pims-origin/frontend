export const DEFAULT_TEMPLATE = `
   <iframe height="350px" *ngIf = "uploadDocumentData['completed']" width="100%" src="{{uploadDocumentData['api_string']}}"></iframe>
   <div class="error-dms" *ngIf = "!uploadDocumentData['completed']"><i class="icon-ban"></i>DMS server error: {{uploadDocumentData['messages']}}</div>
    `;

export const DEFAULT_STYLES = ` `;
