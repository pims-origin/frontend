import {Component} from "@angular/core";
import {Router} from '@angular/router-deprecated';
import {ControlGroup, FormBuilder, Control} from '@angular/common';
import {API_Config, UserService, Functions} from '../../common/core/load';
import {BoxPopupService} from "../../common/popup/box-popup.service";
import {DamagedCartonReportService} from './damaged-carton-report-service';
import {WMSPagination, AgGridExtent, WMSMessages, WMSBreadcrumb, ReportFileExporter} from '../../common/directives/directives';
import 'ag-grid-enterprise/main';

declare const $:any;

@Component({
	selector: 'damaged-carton-report',
	providers: [DamagedCartonReportService, BoxPopupService],
	directives: [AgGridExtent, WMSMessages, WMSPagination, WMSBreadcrumb, ReportFileExporter],
	templateUrl: 'damaged-carton-report.component.html',

})
export class DamagedCartonReportComponent {
	private pagination:any;
	private perPage:number = 20;
	private messages:any;
	private searchForm:ControlGroup;
	private sortData = {fieldname: 'updated_at', sort: 'desc'};
	private showLoadingOverlay:boolean = false;
	private damagedCartonReports:Array<any> = [];
	private headerURL:string = this.apiService.API_User_Metas + '/shr';
	private columnData:any = {
		cus_code: {
			title: 'Customer Code',
			width: 120,
			pin: true,
			ver_table: '113'
		},
		cus_name: {
			title: 'Customer Name',
			width: 120,
			pin: true,
		},
		gr_hdr_num: {
			title: 'GR Num',
			width: 170,
			pin: true,
			id: true
		},
		ctnr_num: {
			title: 'Carrier Num',
			width: 50,
			pin: true
		},
		ref_code: {
			title: 'Ref Code',
			width: 50,
			pin: true
		},
		po: {
			title: 'PO',
			width: 50
		},
		dmg_name: {
			title: 'Damaged Type',
			width: 100
		},
		dmg_qty: {
			title: 'Damaged QTY',
			width: 50
		},
		item_id: {
			title: 'Item ID',
			width: 50
		},
		sku: {
			title: 'SKU',
			width: 50
		},
		size: {
			title: 'Size',
			width: 50
		},
		color: {
			title: 'Color',
			width: 50
		},
		lot: {
			title: 'Lot',
			width: 50
		}
		,
		ctn_pack_size: {
			title: 'Pack Size',
			width: 50
		},
		des: {
			title: 'Description',
			width: 150
		}
	};
	private customers:Array<any> = [];
	private ctnrNums:Array<any> = [];
	private isLoadingCTNRNums:boolean = false;
	private damagedTypes:Array<any> = [];
	private grNums:Array<any> = [];
	private isLoadingGRNums:boolean = false;
	private SKUs:Array<any> = [];
	private isLoadingSKUs:boolean = false;
	private currentSKU:any = {};

	// Check permission for user using this function page
	private canAccess:boolean = false;

	// Export csv file by API
	private exportAPIUrl:string = '';
	private fileName:string = 'Damaged-Carton-Report.csv';

	constructor(private formBuilder:FormBuilder,
	            private dmgCartonReportService:DamagedCartonReportService,
	            private func:Functions,
	            private userService:UserService,
	            private apiService:API_Config,
	            private router:Router) {
		this.buildSearchForm();
		this.checkPermission();
		this.initializeCurSKU();
	}

	buildSearchForm() {
		this.searchForm = this.formBuilder.group({
			cus_id: [''],
			ctnr_num: [''],
			dmg_id: [''],
			gr_hdr_num: [''],
			sku: ['']
		});
	}

	initializeCurSKU() {
		this.currentSKU = {size: '', color: '', lot: '', item_id: ''};
	}

	private checkPermission() {
		this.showLoadingOverlay = true;
		this.userService.GetPermissionUser().subscribe(
			data => {
				this.showLoadingOverlay = false;
				this.canAccess = this.userService.RequestPermission(data, 'viewReport');
				if (this.canAccess) {
					this.getCustomers();
					this.loadDamagedTypes();
					this.getDamagedCartonReports();
				} else {
					// redirect to deny page.
					this.router.parent.navigateByUrl('/deny');
				}
			},
			err => {
				this.messages = this.func.Messages('danger', this.func.parseErrorMessageFromServer(err));
				this.showLoadingOverlay = false;
			}
		);
	}

	private changePageSize($event:any) {
		this.perPage = $event.target.value;
		if (this.pagination) {
			if ((this.pagination['current_page'] - 1) * this.perPage >= this.pagination['total']) {
				this.pagination['current_page'] = 1;
			}
			this.getDamagedCartonReports(this.pagination['current_page']);
		}

	}

	private getDamagedCartonReports(pageNum:number = 1) {
		this.searchForm.value['page'] = pageNum;
		this.searchForm.value['limit'] = this.perPage;
		this.searchForm.value['item_id'] = this.currentSKU['item_id'];
		this.searchForm.value['lot'] = this.currentSKU['lot'];
		this.searchForm.value['sort[updated_at]'] = 'desc';
		this.exportAPIUrl = this.dmgCartonReportService.getReportAPIUrl(`&` + $.param(this.searchForm.value));

		this.showLoadingOverlay = true;
		this.dmgCartonReportService.getReports(`&` + $.param(this.searchForm.value))
			.subscribe(
				data => {
					this.showLoadingOverlay = false;
					this.damagedCartonReports = data.data;

					// paging
					this.pagination = data['meta']['pagination'];
					this.pagination['numLinks'] = 3;
					this.pagination['tmpPageCount'] = this.func.Pagination(this.pagination);
				},
				err => {
					this.messages = this.func.Messages('danger', err.json().errors.message);
					this.showLoadingOverlay = false;
				},
				() => {}
			);
	}

	private getCustomers() {
		const params:string = '?limit=200';
		this.dmgCartonReportService.getCustomers(params).subscribe(
			data => {
				this.customers = data['data'];
			},
			err => {
				this.messages = this.func.Messages('danger', err.json().errors.message);
			},
			() => {
			}
		);
	}

	loadCTNRNums($event:any) {
		const cusId = this.searchForm.value['cus_id'];
		const ctnrNumKeySearch = this.searchForm.value['ctnr_num'].trim();
		const params = `&cus_id=${cusId}&ctnr_num=${ctnrNumKeySearch}&limit=200`;
		const enCodeSearchQuery = this.func.FixEncodeURI(params);

		if (!ctnrNumKeySearch) {
			return;
		}
		this.isLoadingCTNRNums = true;
		this.dmgCartonReportService.getCTNRNums(enCodeSearchQuery).debounceTime(400).distinctUntilChanged().subscribe(
			data => {
				this.isLoadingCTNRNums = false;
				this.ctnrNums = data['data'];
			},
			err => {
				this.isLoadingCTNRNums = false;
				this.messages = this.func.Messages('danger', err.json().errors.message);
			},
			() => {
			}
		);
	}

	loadDamagedTypes() {
		const cusId = this.searchForm.value['cus_id'];
		const params = `&cus_id=${cusId}&limit=200`;
		const enCodeSearchQuery = this.func.FixEncodeURI(params);

		this.dmgCartonReportService.getDamagedTypes(enCodeSearchQuery).subscribe(
			data => {
				this.damagedTypes = data['data'];
			},
			err => {
				this.messages = this.func.Messages('danger', err.json().errors.message);
			},
			() => {
			}
		);
	}

	loadGRNums($event:any) {
		const cusId = this.searchForm.value['cus_id'];
		const ctnrNum = this.searchForm.value['ctnr_num'];
		const grNumsKeySearch = this.searchForm.value['gr_hdr_num'].trim();
		const params = `&cus_id=${cusId}&ctnr_num=${ctnrNum}&gr_hdr_num=${grNumsKeySearch}&limit=200`;
		const enCodeSearchQuery = this.func.FixEncodeURI(params);

		if (!grNumsKeySearch) {
			return;
		}
		this.isLoadingGRNums = true;
		this.dmgCartonReportService.getGRNums(enCodeSearchQuery).debounceTime(400).distinctUntilChanged().subscribe(
			data => {
				this.isLoadingGRNums = false;
				this.grNums = data['data'];
			},
			err => {
				this.isLoadingGRNums = false;
				this.messages = this.func.Messages('danger', err.json().errors.message);
			},
			() => {
			}
		);
	}

	loadSKUs($event:any) {
		const cusId = this.searchForm.value['cus_id'];
		const ctnrNum = this.searchForm.value['ctnr_num'];
		const grhdrNum = this.searchForm.value['gr_hdr_num'];
		const skuKeySearch = this.searchForm.value['sku'].trim();
		const params = `&cus_id=${cusId}&ctnr_num=${ctnrNum}&gr_hdr_num=${grhdrNum}&sku=${skuKeySearch}&limit=200`;
		const enCodeSearchQuery = this.func.FixEncodeURI(params);

		this.initializeCurSKU();
		if (!skuKeySearch) {
			return;
		}

		this.isLoadingSKUs = true;
		this.dmgCartonReportService.getSKUs(params).debounceTime(400).distinctUntilChanged().subscribe(
			data => {
				this.isLoadingSKUs = false;
				this.SKUs = data['data'];
			},
			err => {
				this.isLoadingSKUs = false;
				this.messages = this.func.Messages('danger', err.json().errors.message);
			},
			() => {
			}
		);
	}

	reset() {
		this.resetSearchForm();
		this.initializeCurSKU();
		this.getDamagedCartonReports();
	}

	search() {
		this.getDamagedCartonReports();
	}

	selectCTNRNum($event, ctnrNum) {
		(<Control>this.searchForm.controls['ctnr_num']).updateValue(ctnrNum['ctnr_num']);
	}

	selectGRNum($event, grNum) {
		(<Control>this.searchForm.controls['gr_hdr_num']).updateValue(grNum['gr_hdr_num']);
	}

	selectSKU($event, sku) {
		this.currentSKU = sku;
		(<Control>this.searchForm.controls['sku']).updateValue(sku['sku']);
	}

	resetSearchForm() {
		(<Control>this.searchForm.controls['cus_id']).updateValue('');
		(<Control>this.searchForm.controls['ctnr_num']).updateValue('');
		(<Control>this.searchForm.controls['dmg_id']).updateValue('');
		(<Control>this.searchForm.controls['gr_hdr_num']).updateValue('');
		(<Control>this.searchForm.controls['sku']).updateValue('');
	}

	private updateMessages(messages) {
		this.messages = messages;
	}
}
