import { Directive, ElementRef, HostListener, SkipSelf, Host, Optional, OnInit } from '@angular/core';
import { AbstractControl, NgControlGroup, ControlContainer, ControlGroup, Control } from '@angular/common';
declare const $: any;

@Directive({
    selector: '[wms-datetimepicker]'
})

export class WMSDateTimePickerDirective implements OnInit {

    formControl: AbstractControl;
    // follow format of Moment.js
    defaultDateTimeFormat = 'MM/DD/YYYY HH:mm';

    constructor(
        private el: ElementRef,
        @Optional() @Host() @SkipSelf() private parentForm: ControlContainer
    ) {}

    ngOnInit() {
        const that = this;
        const el = this.el.nativeElement;
        const formatDate = el.getAttribute('data-date-format');
        $(el).parent().datetimepicker({
            format: formatDate ? formatDate : this.defaultDateTimeFormat,
            sideBySide: false,
            collapse: false,
            allowInputToggle: true,
            defaultDate: el.value
        }).on('dp.change', function (e) {
            that.inputChanged(e);
        });

        let ngControlName = el.getAttribute('ngControl');
        if (this.parentForm && ngControlName) {
            if (this.parentForm['form']) {
                this.formControl = (<ControlGroup>this.parentForm['form']).find(ngControlName);
            } else if (this.parentForm instanceof NgControlGroup) {
                this.formControl = (<NgControlGroup>this.parentForm).control.controls[ngControlName];
            }
        }
    }

    // THIS WILL BE FIRED IF SOMEONE CHANGES THE INPUT
    @HostListener('focusout', ['$event'])
    inputChanged(event) {
        if (event.target.value) {
            (<Control>this.formControl).updateValue(event.target.value);
        }
    }

}

// template
// <div class="form-group">
//     <label>Created Date</label>
//     <div class="input-group date">
//         <input class="form-control" wms-datetimepicker formControlName="created_at" data-date-format="YYYY-MM-DD HH:mm" />
//         <span class="input-group-addon">
//             <span class="glyphicon glyphicon-calendar"></span>
//         </span>
//     </div>
// </div>