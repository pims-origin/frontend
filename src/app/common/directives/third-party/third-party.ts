import {Component,Input, Output,EventEmitter} from '@angular/core';
import {ControlGroup, FORM_DIRECTIVES, FormBuilder, Validators, Control, ControlArray} from "@angular/common";
import {WMSMessages} from "../../../common/directives/messages/messages";
import {ValidationService} from "../../../common/core/validator";
import {thirdPartyServices} from "./third-party-service";
import { API_Config,Functions} from '../../../common/core/load';
import {PaginatePipe,PaginationControlsCmp,PaginationService} from '../../../common/directives/directives';
import {GoBackComponent} from "../../../common/component/goBack.component";
declare var $:any;

@Component ({
  selector: 'third-party-directive',
  directives:[GoBackComponent,PaginationControlsCmp,WMSMessages],
  providers: [ValidationService, FormBuilder, thirdPartyServices],
  pipes: [PaginatePipe],
  templateUrl: 'third-party.html'
})

export class ThirdPartyDirective {

  private cusId;
  private perPage=20;
  private perPage2=20;
  private ListThirdPartysByCustomer=[];
  private cruThirdPartyTitle;
  private thirdPartyAction;
  private currentItem;

  public Loading = [];
  private selectedAll={
    'ListThirdPartysByCustomer':false
  };

  private StatePaginThirdParty;
  private StatePaginItems;
  private whs_id = localStorage.getItem('whs_id');

  @Input() IsView: string;

  @Input('cusId') set getThirdPartyList(value: string) {
    if(value){
      this.cusId = value;
      this.getThirdParty();
    }
  }

  @Output() messages=  new EventEmitter();
  @Output() showLoadingOverlay=  new EventEmitter();

  constructor(
              private fb:FormBuilder, private _Valid:ValidationService,
              private _Func: Functions,
              private _API: API_Config,
              private _ODFSerivce:thirdPartyServices) {
    this.getListType();
    this.GetCountry();
    this.buildPriceForm();
  }

  private getThirdParty(){
    let param='?limit=10000';
    this.Loading['getListThirdPartyByCustomer']=true;

    this._ODFSerivce.getThirdParty(this.cusId, param).subscribe(
        data => {
          if(data.data) {
            this.ListThirdPartysByCustomer = data.data;
          }
          this.Loading['getListThirdPartyByCustomer'] = false;
        },
        err => {
          this.Loading['getListThirdPartyByCustomer'] = false;
          this.parseError(err);
        }
    );
  }

  priceForm:ControlGroup;
  items:ControlGroup[] = [];
  itemsArray:ControlArray = new ControlArray(this.items);
  private tempAllItems;
  private updatePriceMessage;
  private itemIdFilter:string = '';

  searchItems(key) {
      this.clearItemList();
      var currItems=this._Func.filterbyfieldName(this.tempAllItems,'item_id',key);
      currItems.forEach((item)=> {
          this.itemDetail(item);
      });
  }

  clearItemList() {
    while(this.itemsArray.length) {
      this.itemsArray.removeAt(0);
    }
  }

  buildPriceForm() {
    this.priceForm = this.fb.group({
        items: this.itemsArray
    });
  }

  private getItems(){
    let param='?limit=10000';
    this.showLoadingOverlay.emit(true);
    this.clearItemList();
    this.updatePriceMessage = null;
    this._ODFSerivce.getItems(this.whs_id, this.cusId, this.currentItem.tp_id, param).subscribe(
        data => {
          this.showLoadingOverlay.emit(false);
          if(data.data) {
            this.tempAllItems = data.data;
            data['data'].forEach((item)=> {
                this.itemDetail(item);
            });
          }
        },
        err => {
          this.updatePriceMessage = this._Func.Messages('danger', this._Func.parseErrorMessageFromServer(err));
          this.showLoadingOverlay.emit(false);
        }
    );
  }

  itemDetail(item) {
      this.itemsArray.push(
          new ControlGroup({
              'item_id': new Control(item['item_id']),
              'sku': new Control(item['sku']),
              'size': new Control(item['size']),
              'color': new Control(item['color']),
              'price': new Control(item['price'],Validators.compose([this._Valid.isDecimalNumberMore])),
              'curr_price': new Control(item['price'],Validators.compose([this._Valid.isDecimalNumberMore])),
              'description': new Control(item['description']),
          })
      );
  }

  updatePrice(item: ControlGroup) {
    if(item.valid) {
      if(item.value['curr_price'] == item.value['price'].trim()) return;
      var dataUpdate = {
        price: item.value['price'].trim(),
      };
      this.showLoadingOverlay.emit(true);
      this.updatePriceMessage = null;
      this._ODFSerivce.updateItemPrice(this.whs_id, this.cusId, this.currentItem.tp_id, item.value['item_id'], JSON.stringify(dataUpdate)).subscribe(
        data=>{
            this.showLoadingOverlay.emit(false);
            this.updatePriceMessage = this._Func.Messages('success', this._Func.msg('VR109'));
            (<Control>item.controls['curr_price']).updateValue(item.value['price'].trim());
            (<Control>item.controls['price']).updateValue(item.value['price'].trim());
            for(let i = 0; i < this.tempAllItems.length; i++) {
              if(this.tempAllItems[i].item_id == item.value['item_id']) {
                this.tempAllItems[i].price = item.value['price'].trim();
              }
            }
        },err=>{
            this.showLoadingOverlay.emit(false);
            this.updatePriceMessage = this._Func.Messages('danger', this._Func.parseErrorMessageFromServer(err));
            (<Control>item.controls['price']).updateValue(item.value['curr_price']);
        });
    }
  }

  showThirdPartyPopup(action, item = null) {
    this.resetForm(item);
    this.thirdPartyAction = action;
    switch (action) {
      case "add":
        this.cruThirdPartyTitle = 'Add Third Party';
        break;
      case "view":
        this.cruThirdPartyTitle = 'View Third Party';
        break;
      case "edit":
        this.cruThirdPartyTitle = 'Edit Third Party';
        break;
    }

    $('#add-thirdParty').modal('show');
  }

  edit(ListName:string) {
    let n = this;
    let Arr=eval(`n.`+ListName);
    let ArrTemp=Arr.slice();
    let ListSelectedItem=this.getSelected(Arr);

    if (ListSelectedItem.length > 1) {
      this.messages.emit(this._Func.Messages('danger',this._Func.msg('CF004')));
    }
    else {
      if(ListSelectedItem.length)
      {
        this.messages.emit(null);
        this.showThirdPartyPopup('edit', ListSelectedItem[0]);
      }
      else{
        this.messages.emit(this._Func.Messages('danger',this._Func.msg('CF005')));
      }
    }
  }

  setPrice(ListName:string) {
    let n = this;
    let Arr=eval(`n.`+ListName);
    let ArrTemp=Arr.slice();
    let ListSelectedItem=this.getSelected(Arr);

    if (ListSelectedItem.length > 1) {
      this.messages.emit(this._Func.Messages('danger',`Please choose only one item to ${this.IsView?'view':'set'} price!`));
    }
    else {
      if(ListSelectedItem.length)
      {
        this.messages.emit(null);
        this.currentItem = ListSelectedItem[0];
        this.getItems();
        $('#third-party').modal('show');
      }
      else{
        this.messages.emit(this._Func.Messages('danger',`Please choose only one item to ${this.IsView?'view':'set'} price!`));
      }
    }
  }

  getSelected(list:Array<any>){
    var listSelected = [];
    list.forEach((item, idx)=>{
      if(item['selected']==true) {
        listSelected.push(item);
      }
    });
    return listSelected;
  }

  ThirdPartyForm: ControlGroup;
  private showErrorThirdParty = false;
  private cruThirdPartyMessage;
  private listType = [];
  private Country = [];
  private StateProvinces = [];
  private showForm = false;

  getListType() {
    this.listType = [
      {code: 'CUS', name: 'End Customer'},
      {code: 'SHP', name: 'Shipper'}
    ];
  }

  GetCountry() {
    let param='?sort[sys_country_name]=asc&limit=500';
    this._ODFSerivce.getCountry(param).subscribe(
      data=>{
        this.Country=data;
      }
    );
  }

  GetState(idCountry) {
    let param="?sort[sys_state_name]=asc&limit=500";
    this._ODFSerivce.State(idCountry,param).subscribe(
      data=>{
        this.StateProvinces = data;
      }
    );
  }

  ChangeCountry(idCountry) {
    if(idCountry) {
      this.GetState(idCountry);
    }
    else{
      this.StateProvinces= [];
    }
    (<Control>this.ThirdPartyForm.controls['state']).updateValue('');
  }

  resetForm(item) {
    this.showForm = false;
    setTimeout(() => {
      this.showForm = true;
      this.buildThirdPartyForm(item);
      this.showErrorThirdParty = false;
      this.cruThirdPartyMessage = false;
    });
  }

  buildThirdPartyForm(item) {
    if(!item) {
      this.ThirdPartyForm =this.fb.group({
          type: ['',Validators.compose([Validators.required])],
          name: ['',Validators.compose([Validators.required, this._Valid.validateSpace, this._Valid.invalidNameStr])],
          phone: [''],
          mobile: [''],
          add_1: ['',Validators.compose([Validators.required, this._Valid.validateSpace])],
          add_2: [''],
          country: ['',Validators.compose([Validators.required])],
          state: ['',Validators.compose([Validators.required])],
          city: ['',Validators.compose([Validators.required, this._Valid.validateSpace])],
          zip: ['',Validators.compose([Validators.required, this._Valid.validateSpace])],
          ct_first_name: ['',this._Valid.invalidNameStr],
          ct_last_name: ['',this._Valid.invalidNameStr],
          des: [''],
      });
    }
    else {
      this.currentItem = item;
      this.ThirdPartyForm =this.fb.group({
          type: [item.type,Validators.compose([Validators.required])],
          name: [item.name,Validators.compose([Validators.required, this._Valid.validateSpace, this._Valid.invalidNameStr])],
          phone: [item.phone],
          mobile: [item.mobile],
          add_1: [item.add_1,Validators.compose([Validators.required, this._Valid.validateSpace])],
          add_2: [item.add_2],
          country: [item.country,Validators.compose([Validators.required])],
          state: [item['state'] ? item['state'] : '',Validators.compose([Validators.required])],
          city: [item.city,Validators.compose([Validators.required, this._Valid.validateSpace])],
          zip: [item.zip,Validators.compose([Validators.required, this._Valid.validateSpace])],
          ct_first_name: [item.ct_first_name,this._Valid.invalidNameStr],
          ct_last_name: [item.ct_last_name,this._Valid.invalidNameStr],
          des: [item.des],
      });

        this.GetState(item.country);

    }
  }

  SaveThirdParty(){

      this.showErrorThirdParty=true;
      this.cruThirdPartyMessage = false;

      if(this.ThirdPartyForm.valid){
          let data=this.ThirdPartyForm.value;
          for(let x in data) {
              data[x] = data[x] ? data[x].trim() : data[x];
          }
          let data_json = JSON.stringify(data);
          if(this.thirdPartyAction == 'edit') {
            this.updateThirdParty(data_json);
          }
          else {
            this.createThirdParty(data_json);
          }
      }
  }


  createThirdParty(data){
    this.showLoadingOverlay.emit(true);
    this._ODFSerivce.createThirdParty(this.cusId, data).subscribe(
        data => {
            this.showLoadingOverlay.emit(false);
            this.messages.emit(this._Func.Messages('success', this._Func.msg('VR107')));
            this.getThirdParty();
            $('#add-thirdParty').modal('hide');
        },
        err => {
            this.cruThirdPartyMessage = this._Func.Messages('danger', this._Func.parseErrorMessageFromServer(err));
            this.showLoadingOverlay.emit(false);
        }
    );
  }

  updateThirdParty(data) {
    this.showLoadingOverlay.emit(true);
    this._ODFSerivce.updateThirdParty(this.cusId, this.currentItem.tp_id, data).subscribe(
        data => {
            this.showLoadingOverlay.emit(false);
            this.messages.emit(this._Func.Messages('success', this._Func.msg('VR109')));
            this.getThirdParty();
            $('#add-thirdParty').modal('hide');
        },
        err => {
            this.cruThirdPartyMessage = this._Func.Messages('danger', this._Func.parseErrorMessageFromServer(err));
            this.showLoadingOverlay.emit(false);
        }
    );
  }

  private isNumber(evt, int:boolean = false) {
      this._Valid.isNumber(evt, int);
  }

  private CheckAll(event,ListName:string,StatePagin,objName) {
    let Array=eval(`this.`+ListName);
    if(!Array.length)
    {
      this.selectedAll[ListName] = false;
      return;
    }
    if (event) {
      this.selectedAll[ListName] = true;
    }
    else {
      this.selectedAll[ListName]= false;
    }
    let end=StatePagin[objName]['end'];
    if(StatePagin[objName]['size']<StatePagin[objName]['end'])
    {
      end=StatePagin[objName]['size'];
    }
    for(let i=StatePagin[objName]['start'];i<end;i++)
    {
      Array[i]['selected']=this.selectedAll[ListName];
    }
  }

  private Selected($event,item,ListName:string,state,paginId) {
    let Array=eval(`this.`+ListName);
    this.selectedAll[ListName]=false;
    if ($event.target.checked) {
      item['selected']=true;
      this.checkCheckAll(ListName,state,paginId);
    }
    else {
      item['selected']=false;
    }
  }

  checkCheckAll(ListName:string,StatePagin,objName) {
    let Array=eval(`this.`+ListName);
    let el=StatePagin[objName];
    let end=el['end'];
    let chk=0;
    if(el['size'])
    {
      if(el['size']<el['end'])
      {
        end=el['size'];
      }
      for(let i=el['start'];i<end;i++)
      {
        if(Array[i]['selected'])
        {
          chk++;
        }
      }
      if(chk==el['slice'].length)
      {
        this.selectedAll[ListName]=true;
      }
      else{
        this.selectedAll[ListName]=false;
      }
    } // end if size
  }

  private onPageSizeChanged($event,funcGetList,currentPage) {
    this.perPage =  parseInt($event.target.value);
  }

  private parseError(err){
    this.messages.emit({'status' : 'danger', 'txt' : this._Func.parseErrorMessageFromServer(err)});
  }

  private setClickedRow(item) {
    item.selected = !item.selected;
  }
}
