/*
*
* <!-- <list-table
 [listTableHead]="ListTableHead"
 [listItemShow]="listItemShow"
 [fieldId]="'cus_id'"
 [urlCRU]="'/system-setup/customer'"
 (searchFunction)="Search(param)"
 [apiUrl]="apiUrl"
 [isSearch]="isSearch"
 [reloadList]="reloadList"
 [searchQuery]="searchQuery"
 (mgs)="messages=$event"
 (loading)="LoadingSearch=$event"
 >
 </list-table>-->
* */

import {Component,Input,Directive, Output, EventEmitter, ElementRef} from '@angular/core';
import {RouteParams,Router ,RouteData} from '@angular/router-deprecated';
import {Functions,API_Config} from '../../../common/core/load';
import {Http, Response} from '@angular/http';
@Component ({
  selector: 'list-table',
  directives:[],
  providers: [],
  templateUrl: 'list-directive.html',
})

export class ListDirective {
  public AuthHeader = this._Func.AuthHeader();
  public AuthHeaderPost = this._Func.AuthHeaderPost();
  private ListPermission=[];
  private _apiUrl:string;
  private _listTableData:Array<any>=[];
  private _listTableHead:Array<any>=[];
  private _listItemShow:Array<any>=[];
  private _urlCRU:string;
  private _fieldId:string;
  private _isSearch:boolean=false;
  private _pagination:Array<any>=[];
  private _link:string='';
  private Pagination = [];
  public Loading=[];
  private errMessages=[];
  private totalCount = 0;
  private tmpPageCount;
  private pageCount=0;
  private ItemOnPage=0;
  private currentPage = 1;
  private perPage = 20;
  private numLinks = 3;
  private start = 1;
  private end = 1;
  public ListSelectedItem:Array<any>=[];
  public selectedAll:boolean=false;
  private messages;

  /*
   * Method get data any
   * */
  @Input('listTableData') set listTableData(value: Array<any>) {
    if(value)
    {
      if(value['data'])
      {
        this._listTableData = value['data'];
      }
      //pagin function
      if(typeof value['meta']!=='undefined')
      {
        this._pagination=value['meta']['pagination'];
        this._pagination['numLinks']=3;
        this._pagination['tmpPageCount']=this._Func.Pagination(this._pagination);
      }
      //
    }
  }

  @Input('listItemShow') set listItemShow(value: Array<any>) {
    this._listItemShow=value;
  }

  @Input('fieldId') set fieldId(value: string) {
    this._fieldId=value;
  }

  /*
   * urlViewEdit
   * */
  @Input('urlCRU') set urlCRU(value: string) {
    this._urlCRU=value;
  }

  @Input('listTableHead') set listTableHead(value: Array<any>) {
    this._listTableHead = value;
  }

  @Input('link') set link(value: string) {
    this._link = value;
  }

  @Input('apiUrl') set apiUrl(value: string) {
    this._apiUrl = value;
    this.getListData();
  }

  @Input('reloadList') set reloadList(value: boolean) {
    if(value==true)
    {
      this._isSearch=false; // disable search
      this.getListData();
    }
  }

  @Input('isSearch') set isSearch(value: boolean) {
    if(value==true)
    {
      this._isSearch=value;
    }
  }

  private _searchQuery:string=null;
  @Input('searchQuery') set searchQuery(value: string) {
    if(value)
    {
      this._searchQuery=value;
      this.getListData();
    }
  }

  @Output() functionPagin  = new EventEmitter();

  @Output() mgs  = new EventEmitter();

  @Output() loading  = new EventEmitter();


  constructor(
    private http: Http,
    private _API: API_Config,
    private _Func: Functions,
    private _router:Router)
  {

  }
  private createDetail(url)
  {
    this._router.parent.navigateByUrl(this._urlCRU+'/new');
  }

  private onPageSizeChanged($event)
  {
    this.perPage = $event.target.value;
    if((this._pagination['current_page'] - 1) * this.perPage >= this._pagination['total']) {
      this._pagination['current_page'] = 1;
    }
    this.getListData(this._pagination['current_page']);
  }
  private getPage(num)
  {
    let arr=new Array(num);
    return arr;
  }

  private filterList(num)
  {
    this.selectedAll=false;
    this.functionPagin.emit(num);
    this.getListData(num);
  }

  /*
   * Check All/Off
   * */
  private CheckAll(event) {
    if (event) {
      this.selectedAll = true;
    }
    else {
      this.selectedAll = false;
    }

    // set empty array  ListSelectedItem
    this.ListSelectedItem=[];
    // Loop
    this._listTableData.forEach((item) => {
      item['selected'] = this.selectedAll;
      if(this.selectedAll)
      {
        // if check all = true , pushing to ListSelectedItem
        this.ListSelectedItem.push(item['item_id']);
      }
    });
  }

  /*
   * Selected
   * */
  private Selected($event) {


    let item = $event.target.value;

    this.selectedAll=false;


    if ($event.target.checked) {
      this.ListSelectedItem.push(item);
      this.SetItemListSelected(item,true);

      if(this.ListSelectedItem.length==this._listTableData.length)
      {
        this.selectedAll=true;
      }
    }
    else {
      let el = this.ListSelectedItem.indexOf(item);
      // remove item out of array
      this.ListSelectedItem.splice(el, 1);
      this.SetItemListSelected(item,false);

    }


  }

  /*
   * Set select item
   * */
  SetItemListSelected(id,value)
  {
    this._listTableData.forEach((item) => {
      if(item[this._fieldId]==id)
      {
        item['selected']=value;
      }
    });
  }

  /*
   * Function get list template
   * */
  private getListData(page=null)
  {
    let param="?page="+page+"&limit="+this.perPage;
    this.selectedAll=false;
    this.ListSelectedItem=[];

    if(this._isSearch)
    {
      this.loading.emit({searchLoading:true});
      param=this._searchQuery+'&limit='+this.perPage+'&page='+page;
    }
    this.Loading['getList']=true;
    this.GetListDataService(param).subscribe(
      data => {
        this._listTableData = data.data;
        if(typeof data['meta']!=='undefined')
        {
          this._pagination=data['meta']['pagination'];
          this._pagination['numLinks']=3;
          this._pagination['tmpPageCount']=this._Func.Pagination(this._pagination);
        }
        this.Loading['getList'] = false;
        this.loading.emit({searchLoading:false});

      },
      err => {},
      () => {}
    );



  }

  /*
   * Router edit
   * */
  Edit() {
    if (this.ListSelectedItem.length > 1) {
      this.mgs.emit(this._Func.Messages('danger',this._Func.msg('CF004')));
    }
    else {
      if(this.ListSelectedItem.length)
      {
        this._router.parent.navigateByUrl(this._urlCRU+'/'+this.ListSelectedItem[0]+'/edit');
      }
      else{
        this.mgs.emit(this._Func.Messages('danger',this._Func.msg('CF005')));
      }
    }
  }

  private Delete(id)
  {

    if(!this.ListSelectedItem.length)
    {
      // if no select any item to delete
      return;
    }
    if(confirm(this._Func.msg('PBAP002'))&&this.ListSelectedItem.length) {
      // remove item local
      this.RemoveItemLocal();

      this.ListSelectedItem.forEach((item)=> {

        this.deleteServiceById(item).subscribe(
          data => {
            this.selectedAll = false;
            // reload data new
            if (this._listTableData.length <10) {
              this.getListData(this.Pagination['current_page']);
            }
          },
          err => {},
          () => {}
        );

      });

      // set empty selected
      this.ListSelectedItem = [];
    };

  }

  /*
   * Service
   * */
  private deleteServiceById(id)
  {
    return this.http.delete(this._apiUrl+'/'+id,{ headers: this.AuthHeader });
  }
  private GetListDataService(param)
  {
    return this.http.get(this._apiUrl+param,{ headers: this.AuthHeader }).map((res: Response) => res.json());
  }

  RemoveItemLocal()
  {
    let sumItemDelet=0;

    this.ListSelectedItem.forEach((item)=>{

      for(let el of this._listTableData) {
        let els=parseInt(el);
        if (item == els['item_id']) {
          let index = this._listTableData.indexOf(item);
          // remove item out of array
          this._listTableData.splice(index, 1);
          sumItemDelet++;
        }
        else{
        }
      }

    });

  }

  private CheckArray2D(e)
  {
    if(e.charAt(0)=='2')
    {
      return true;
    }
    return false;
  }

  private showArray2D(str)
  {
    return str.split(".");
  }



}
