import {Component, Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {API_Config,Functions} from '../../common/core/load';

@Injectable()
export class UnsetDamageServices {

    constructor(private _Func: Functions,
                private _API: API_Config,
                private http: Http) {
    }

    getGoodReceiptList(gdId:any,param:any){
        return this.http.get(this._API.API_Damage_Carton+'/'+gdId+'/list'+param, { headers: this._Func.AuthHeader() })
            .map((res: Response) => res.json());
    }

    getDamagedCartonList(param:any){
        return this.http.get(this._API.API_DAMAGE_TYPE+param, { headers: this._Func.AuthHeader() })
            .map((res: Response) => res.json().data);
    }

    updateDamageCarton(data:any){
        return this.http.put(this._API.API_Damage_Carton+'/update',data, { headers: this._Func.AuthHeaderPostJson() })
            .map((res: Response) => res.json());
    }

    unsetDamaged(data:any){
        return this.http.put(this._API.API_Damage_Carton+'/unset',data, { headers: this._Func.AuthHeaderPostJson() })
            .map((res: Response) => res.json());
    }



}
