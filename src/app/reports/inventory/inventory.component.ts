
import {Component} from '@angular/core';
import {API_Config} from '../../common/core/API_Config';
import {BaseTable} from '../../common/core/classes_us/base-table';
import {TableFunctions} from '../../common/core/table-functions';
import {DateFunctions} from '../../common/core/functions/date-functions';
import {BaseService} from '../../common/core/classes_us/base-service';
import {CustomersService} from '../../master-data/customers/customers-service';
import {TableStructure} from '../../common/directives/directives';

@Component ({
    selector: 'inventory',
    directives: [
        TableStructure
    ],
    templateUrl: '../../common/directives/table-structure/table.component.html',
    providers: [
        BaseService,
        TableFunctions,
        DateFunctions,
        CustomersService
    ]
})

export class InventoryComponent extends BaseTable
{
    public tableID = 'inventory';
    public url = this._api.API_INVENTORY_REPORTS;
    public headerUrlSuffix = 'rep';
    public title = 'Search Inventory';
    public permissionPage = 'viewReport';

    public headerDef = [
        {
            id: 'ver_table',
            value: 1 + Math.random() * 10000
        },
        this.columnList.customer,
        this.columnList.container,
        this.columnList.receivingNumber,
        this.columnList.measurementSystem,
        this.columnList.createdAt,
        this.columnList.pieceRemain,
        this.columnList.upc,
        this.columnList.sku,
        this.columnList.color,
        this.columnList.size,
        this.columnList.po,
        this.columnList.lot,
        this.columnList.length,
        this.columnList.width,
        this.columnList.height,
        this.columnList.weight,
        this.columnList.cartonPackSize,
        this.columnList.cartonNumber,
        this.columnList.plateNumber,
        this.columnList.location,
        this.columnList.cartonStatus
    ];

    public unsortableColumns = [
        'customer',
        'receivingNumber',
        'measurementSystem',
        'plateNumber'
    ];

    public searches = [
        this.searchList.customer,
        this.searchList.container,
        this.searchList.carton,
        this.searchList.location
    ];

    /*
    ****************************************************************************
    */

    constructor (
        public _api: API_Config,
        public _tableFunc: TableFunctions,
        public _tableService: BaseService,
        public _customersService: CustomersService
    )
    {
        super();

        this._tableFunc.permissionCall(this);
    }

    //**************************************************************************

}
