import {Component,Input, Output,EventEmitter} from '@angular/core';
import {WMSMessages,WMSPagination} from '../../../common/directives/directives';
import {API_Config} from "../../../common/core/API_Config";
import {Functions} from "../../../common/core/functions";
import {Http, Response} from '@angular/http';
declare var jQuery: any;
@Component ({
    selector: 'event-tracking-direcitve',
    directives:[WMSMessages,WMSPagination],
    providers: [],
    templateUrl: 'event-tracking.html'
})

export class EventTrackingDirective {

    private messages;
    private Pagination={};
    private perPage=20;
    private currentPage = 1;
    private numLinks = 3;
    private dataEventTracking:Array<any>=[];
    private _owner:any;

    @Input('owner') set owner(value:any) {
        if(value){
            this._owner=value;
            this._getEventTrackingByAsn(1);
        }
    }

    @Input('reload') set reloadData(value:any) {
        if(value){
            this._getEventTrackingByAsn(1);
        }
    }

    constructor(
        private _Func:Functions,
        private _http:Http,
        private _api_Config: API_Config
    ){}

    // Get Asn by ID
    private _getEventTrackingByAsn (page = 1) {

        var param = "?owner=" + this._owner;
        param += "&page=" + page + "&limit=" + this.perPage;
        // reset Pagination
        // this.Pagination={};

        try {

            this.getEventTrackingByAsn(param)
                .subscribe(
                    data => {
                        this.dataEventTracking = data.data;
                        this.Pagination = data['meta']['pagination'];
                        this.Pagination['numLinks'] = 3;
                        this.Pagination['tmpPageCount'] = this._Func.Pagination(this.Pagination);
                    },
                    err => {
                        this.parseError(err);
                    },
                    () => {}
                );

        } catch (err) {

            this.messages = this._Func.Messages('danger', this._Func.msg('VR100'));
        }

    }

    public getEventTrackingByAsn($params) {

        return this._http.get(this._api_Config.API_GOODS_RECEIPT_MASTER+'/eventracking'+ $params,{ headers: this._Func.AuthHeader() })
            .map(res => res.json());

    }

    // Event when user filter form
    private onPageSizeChanged ($event,el) {

        this.perPage = $event.target.value;

        if((this.Pagination['current_page'] - 1) * this.perPage >= this.Pagination['total']) {
            this.currentPage = 1;
        }
        else {
            this.currentPage = this.Pagination['current_page'];
        }
        this._getEventTrackingByAsn(this.currentPage);
    }

    // Show error when server die or else
    private parseError (err) {

        this.messages = {'status' : 'danger', 'txt' : this._Func.parseErrorMessageFromServer(err)};
    }


}
