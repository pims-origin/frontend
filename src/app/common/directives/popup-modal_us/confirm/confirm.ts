
import {Component, Output, EventEmitter} from '@angular/core';
import {ModalForm} from '../../../core/classes_us/modal-form';

@Component ({
    selector: 'confirm-popup',
    templateUrl: 'confirm.html',
})

export class Confirm extends ModalForm
{
    public content;
    public data;

    @Output() onConfirm: EventEmitter<any> = new EventEmitter<boolean>();

    //**************************************************************************

    public submit(value: boolean)
    {
        this.onConfirm.emit({
            response: value,
            data: this.data
        });

        this.isVisible = false;
    }

    //**************************************************************************

    public show(content: string, data: any)
    {
        this.content = content;
        this.data = data;

        this.isVisible = true;
    }

    //**************************************************************************

}
