import {Component} from '@angular/core';
import {Router, RouterLink, RouteConfig, ROUTER_DIRECTIVES} from '@angular/router-deprecated';
import {PackingListComponent} from "./packing-list/packing-list.component";
import {PackingUpdateComponent} from "./packing-update/packing-update.component";
import {PackCartonComponent} from "./pack-carton/pack-carton.component";

@Component ({
    selector: 'packing-management',
    directives: [ROUTER_DIRECTIVES],
    template: `<router-outlet></router-outlet>`,
})

@RouteConfig([
    { path: '/', component: PackingListComponent , name: 'Packing List', data:{page:'packing'}, useAsDefault: true },
    { path: '/:id', component: PackingUpdateComponent , name: 'Packing Inventory Update', data:{action:'view'}},
    { path: '/pack-carton/:id/:item', component: PackCartonComponent , name: 'Pack Carton'}

])
export class PackingManagementComponent {

}
