import {Component} from '@angular/core';
import { FORM_DIRECTIVES,ControlGroup,FormBuilder, Control,Validators} from '@angular/common';
import {WMSBreadcrumb,DocumentUpload,WMSMessages,LaborCostDirective, DmsIn} from '../../common/directives/directives';
import {Router, RouteData, RouteParams } from '@angular/router-deprecated';
import {BoxPopupService} from "../../common/popup/box-popup.service";
import {WorkOrderServices} from "./work-order-service";
import {GoBackComponent} from "../../common/component/goBack.component";
import {WarningBoxPopup} from "../../common/popup/warning-box-popup";
import { ValidationService } from '../../common/core/validator';
import {UserService, Functions} from '../../common/core/load';
import {API_Config} from "../../common/core/API_Config";
declare var saveAs: any;
declare var jQuery: any;
@Component ({
    selector: 'work-order-list',
    directives:[WMSBreadcrumb, GoBackComponent,DocumentUpload,FORM_DIRECTIVES,WMSMessages,LaborCostDirective, DmsIn],
    providers: [BoxPopupService,WorkOrderServices,ValidationService,UserService],
    templateUrl: 'work-order-cru.html'
})

export class CRUWorkOrderCC {

    WODForm: ControlGroup;
    private workOrd = null;
    private action = 'new';
    private title = 'Create';
    private _wordOrderby = "chargecode";
    private _byOrder=true;
    private messages;
    private showLoadingOverlay=false;
    private TitlePage="Create Work Order";
    private IsView=false;
    private Action;
    private skusArray:Array<any>=[];
    private chargecodesArray:Array<any>=[];
    private Customers:Array<any>=[];
    private whs_id;
    private wodr_id;
    private submitForm=false;
    private listChargCode:Array<any>=[];
    private listSkus:Array<any>=[];
    private order_info={};
    private selectedAllChargeCode=false;
    private selectedAllItems=false;
    private selectChargCode=[];
    private cus_id = '';
    private loading={};
    private invalidDetail=false;
    private changeCustomernumber=0;
    private workOrderDetail;
    private flagFinalForNew = false;
    private isNewWO:boolean = false;
    private isProcessingWO:boolean = false;
    private isFinalWO:boolean = false;


    constructor(private _router: Router,
                private _boxPopupService:BoxPopupService,
                _RTAction: RouteData,
                private _user:UserService,
                private fb: FormBuilder,
                private _Valid: ValidationService,
                private _Func: Functions,
                private workODService:WorkOrderServices,
                private _api_Config: API_Config,
                private params: RouteParams) {

      this.Action=_RTAction.get('action');
      this.whs_id=this._Func.lstGetItem('whs_id');
      this.checkPermission();

    }

  wordOrderby($event,orderby, type){

    this._wordOrderby=orderby;
    (<Control>this.WODForm.controls['type']).updateValue(type);

  }

  byOrder($event){

    let that = this;
    /*case 1: modify text*/
    let warningPopup = new WarningBoxPopup();
    warningPopup.text = this._Func.msg('M043');
    this._boxPopupService.showWarningPopup(warningPopup)
      .then(function (dm) {
        if(dm) {
          if ($event.target.checked) {
            // set require for control odr_num

            // reset chargcode list and skus
            that.chargecodesArray = [];
            that.skusArray = [];
            that.listSkus=[];
            that.addNewSku();
            that.addRowChargcode();
            that._byOrder = true;
            // set required
            (<Control>that.WODForm.controls['odr_num']).setErrors({required: true});
          }
          else {
            that._byOrder = false;
            that.resetDataChange();
            let cusId=that.WODForm.value['cus_id'];
            that.getChageCodeList(cusId);
            that.getListSkus(cusId);
          }
        }
        else{

            if($event.target.checked)
            {
              that._byOrder=false;
            }
          else{
              that._byOrder=true;
            }

        }

      });

  }

  private autoSearchData=[];
  autoComplete(key,fieldname){

    // search only if cus_id have seleced
    let item=this.WODForm.controls['odr_num'];
    item['showLoading']=true;
    item['invaildValue']=false;

    let searchkey=this._Func.trim(key);
    let param="?"+fieldname+"="+searchkey+"&limit=20";
    if(this.cus_id != ''){
      param += '&cus_id=' + this.cus_id;
    }
    let enCodeSearchQuery=this._Func.FixEncodeURI(param);

    if(item['subscribe']) {
      item['subscribe'].unsubscribe();
    }
    // do Search
    item['subscribe']=this.workODService.getOrderList(enCodeSearchQuery).debounceTime(400).distinctUntilChanged().subscribe(
        data => {
          // disable loading icon
          item['showLoading']=false;
          this.autoSearchData = data.data;
          this.checkKeyExist(searchkey,this.autoSearchData,fieldname);
        },
        err =>{
          item['showLoading']=false;
          this.parseError(err);
        },
        () =>{
        }
    );


  }
  /*
   * Check Key exist
   * */
  checkKeyExist(str:string,arr:Array<any>,fieldname)
  {
    let string=str;
    arr.forEach((item)=>{
      if(string==this._Func.trim(item[fieldname]))
      {
        this.WODForm.controls['odr_num']['invaildValue']=true;
        this.selectedItem(item);
      }
    });

  }

  /*
   * Auto select item if match
   * */
  selectedItem(dataItem)
  {
    this.order_info={};
    this.skusArray=[];
    this.listSkus=[];
    this.chargecodesArray=[];
    this.addNewSku();
    this.addRowChargcode();
   this.WODForm.controls['odr_num']['invaildValue']=true;
    let param = this.WODForm.value['cus_id'] + '/' + dataItem['odr_num'];
    this.getListSkus(param,true);
    (<Control>this.WODForm.controls['odr_num']).updateValue(dataItem['odr_num']);
  }

  /*==================================
   * showKeyNotExit
   * ===============================*/
  showKeyNotExit(){
    let item=this.WODForm.controls['odr_num'];
    setTimeout(() => {
      if(!item['invaildValue']&&item.value){
        (<Control>this.WODForm.controls['odr_num']).setErrors({invaildValue:true});
      }
    },150);
  }


  resetDataChange(){
    // reset chargcode list and skus
    this.chargecodesArray = [];
    this.skusArray = [];
    this.listSkus=[];
    this.order_info={};
    this.addNewSku();
    this.addRowChargcode();
    (<Control>this.WODForm.controls['odr_num']).updateValue('');
    /*
    * If not by order , set null errors for control odr_number
    * */
    if(!this._byOrder){
      setTimeout(() => {
        (<Control>this.WODForm.controls['odr_num']).setErrors(null);
      });
    }

  }

  // Check permission for user using this function page
  private createWOrder;
  private viewWOrder;
  private editWOrder;
  private allowAccess:boolean=false;
  checkPermission(){

    this.showLoadingOverlay = true;
    this._user.GetPermissionUser().subscribe(
      data => {
        this.showLoadingOverlay = false;
        this.createWOrder = this._user.RequestPermission(data, 'createWorkOrder');
        this.viewWOrder = this._user.RequestPermission(data,'viewWorkOrder');
        this.editWOrder = this._user.RequestPermission(data,'editWorkOrder');

        this.getCustomersByWH();

        /*
         * Action router View detail
         * */
        if(this.Action=='view')
        {
          if(this.viewWOrder) {
            this.IsView = true;
            this.TitlePage = 'View Work Order';
            this.allowAccess=true;
          }
          else{
            this.redirectDeny();
          }
        }

        // Action new
        if(this.Action=='new')
        {
          if(this.viewWOrder) {
            this.allowAccess=true;

            this.TitlePage = 'Create Work Order';

            this.formBuilder();
          }
          else{
            this.redirectDeny();
          }
        }

        // load detail / init data form
        if(this.Action=='edit'||this.Action=='view')
        {
          this.getWorkOrderDetail();
        }

        /*
         * Action router Edit
         * */
        if(this.Action=='edit')
        {
          if(this.editWOrder) {
            this.allowAccess=true;
            this.TitlePage = 'Edit Work Order';
          }
          else{
            this.redirectDeny();
          }
        }

      },
      err => {
        this.parseError(err);
        this.showLoadingOverlay = false;
      }
    );
  }

  redirectDeny(){
    this._router.parent.navigateByUrl('/deny');
  }



  changeCustomer($event){

      let cus_id=$event.target.value;
      if((cus_id&&this.WODForm.value['cus_id']) || (this.WODForm.value['cus_id']&&!cus_id)){
          let that = this;
          /*case 1: modify text*/
          let warningPopup = new WarningBoxPopup();
          warningPopup.text = this._Func.msg('M043');
          this._boxPopupService.showWarningPopup(warningPopup)
              .then(function (dm) {
                  if(dm) {
                      that.cus_id=cus_id;
                      that.resetDataChange();
                      if(cus_id){
                          that.getChageCodeList(cus_id);
                      }

                      if(!that._byOrder){
                          if(cus_id){
                              that.getListSkus(cus_id);
                          }
                      }
                  }
                  else{
                      (<Control>that.WODForm.controls['cus_id']).updateValue(that.cus_id);
                  }
              });

    }
    else{
      this.cus_id=cus_id;
      this.resetDataChange();
      this.getChageCodeList(cus_id);
      if(!this._byOrder){
        this.getListSkus(cus_id);
      }
    }

  }

  getChageCodeList($param){
    this.workODService.getChargeCode($param).subscribe(
      data => {
        this.listChargCode=data.data;
      },
      err => {
        this.parseError(err);
      },
      () => {}
    );
  }


  formBuilder(){

    let orderData={};
    orderData['cus_id']=this.params.get('cus_id');
    orderData['ord_num']=this.params.get('ord_num');


    if(this.Action=='new'){

      if(orderData['cus_id']&&orderData['ord_num'])
      {
        let param = orderData['cus_id'] + '/' + orderData['ord_num'];
        this.getListSkus(param,true);
        this.getChageCodeList(orderData['cus_id']);
      }

    }

    this.addNewSku();
    this.addRowChargcode();

    this.WODForm =this.fb.group({
      cus_id: [orderData['cus_id'] ? orderData['cus_id'] : '',Validators.required],
      odr_num:[orderData['ord_num'] ? orderData['ord_num'] : '',Validators.compose([Validators.required, this._Valid.validateSpace])],
      wo_hdr_num:[''],
      wo_sts:['New'],
      wo_ex_notes:[null],
      wo_in_notes:[null],
      type: [0],
      details:['']
    });

    if(orderData['ord_num']){
      // check code
      this.autoComplete(orderData['ord_num'],'odr_num');
    }

  }

  formBuilderViewEdit(data){

    // set 1 for changeCustomernumber
    this.changeCustomernumber=1;

    this.WODForm =this.fb.group({
      cus_id: [data['cus_id'],Validators.required],
      wo_hdr_id:[this.wodr_id],
      odr_num:[data['odr_num'],Validators.compose([Validators.required, this._Valid.validateSpace])],
      wo_hdr_num:[data['wo_hdr_num']],
      wo_sts:[data['wo_sts'] ? data['wo_sts'] : 'New'],
      wo_ex_notes:[data['wo_ex_notes']],
      wo_in_notes:[data['wo_in_notes']],
      type: [data['type']],
      details:['']
    });

    this.cus_id=data['cus_id'];
    // get chagecode
    this.getChageCodeList(this.cus_id);

    // check button by order
    if(!data['odr_num']){
      this._byOrder=false;
      this.getListSkus(this.cus_id);
    }
    else{
      // get detail order number
      let param =this.cus_id + '/' + data['odr_num'];
      this.getListSkus(param,true);
    }

    /*
     *  */
    if(data['type']==0){

        this._wordOrderby='chargecode';
        this.chargecodesArray=data['details'];
        // createValueSelected
    }
    else{
      this._wordOrderby='sku';
      this.skusArray=data['details'];
    }

    // remove require if odr_num is empty
    if(!data['odr_num']){
      let that=this;
      setTimeout(() => {
        (<Control>that.WODForm.controls['odr_num']).setErrors(null);
      }, 1000);

    }




  }



  addNewSku(){

    // init the first row for this.itemsArray
    this.skusArray.push({
      chg_code_id:'',
      skus:[],
      }
    );

  }

  addRowChargcode(data=null){

    this.chargecodesArray.push({
      item_id:'',
      chargecodes:[],
    });

  }

  getWorkOrderDetail(){

    this.wodr_id=this.params.get('id');
    this.workODService.getWorkOrderDetail(this.wodr_id).subscribe(
      data => {
        this.formBuilderViewEdit(data);
        this.workOrderDetail=data;
        const woStatus = this.workOrderDetail['wo_sts'];
        this.isNewWO = woStatus ? woStatus.toLowerCase() === 'new' : false;
        this.isProcessingWO = woStatus ? woStatus.toLowerCase() === 'processing' : false;
        this.isFinalWO = woStatus ? woStatus.toLowerCase() === 'final' : false;
      },
      err => {
        this.parseError(err);
      },
      () => {}
    );
    //let data=this.workODService.getWorkOrderDetail(this.wodr_id);
     // this.formBuilderViewEdit(data);
  }


  changeSKU(value,row){

    this.chargecodesArray[row]['item_id'] = value;
    this.createValueSelected(row,value);
    // check duplicated
    this.checkDuplicatedValueRow(this.chargecodesArray,'item_id');

    if(value) {
      this.chargecodesArray[row]['chargecodes'] = JSON.parse(JSON.stringify(this.listChargCode));
    }
    else{
      // reset
      this.chargecodesArray[row]={item_id:''};
    }

  }

  checkDuplicatedValueRow(array,control){

    // listData -> list get by api
    let dataCheck = this._Func.checkDuplicatedValueRow(array,control);
    array = dataCheck['data'];
    let dup = dataCheck['duplicated'];
    if (!dup) {
      (<Control>this.WODForm.controls['details']).setErrors(null);
    }
    else {
      // set invalid form WODForm
      (<Control>this.WODForm.controls['details']).setErrors({duplicated: true});
    }

  }

  createValueSelected(row,ItemId){
    // create attribute selectValue
    for(let i in this.listSkus){
      if(this.listSkus[i]['item_id']==ItemId)
      {
        this.chargecodesArray[row]['value_selected']=this.listSkus[i];
        return;
      }
    }
  }


  changeChargcode(value,row){

    this.skusArray[row]['chg_code_id']=value;
    // check duplicated
    this.checkDuplicatedValueRow(this.skusArray,'chg_code_id');

    if(value){
      this.skusArray[row]['skus']=JSON.parse(JSON.stringify(this.listSkus));
    }
    else{
      //reset
      this.skusArray[row]={chg_code_id:''};
    }

  }


  Save(){

    this.submitForm=true;
    let validValue=false;
    let validQtyDetail=false;

    // empty messages
    this.messages=false;

    // scroll to top
    jQuery('html, body').animate({
      scrollTop: 0
    }, 700);

    if(this._wordOrderby=='chargecode'){
      // remove empty row;
      this.deleteEmptyValueRow(this.chargecodesArray,'item_id');
      this.WODForm.value['details']=this.chargecodesArray;
      validQtyDetail=this.checkVaildQTy(this.chargecodesArray,'chargecodes');
      validValue=this.checkAtLeastonInputValue(this.chargecodesArray,'chargecodes','wo_qty');
    }
    else {
      this.deleteEmptyValueRow(this.skusArray,'chg_code_id');
      this.WODForm.value['details']=this.skusArray;
      validQtyDetail=this.checkVaildQTy(this.skusArray,'skus');
      validValue=this.checkAtLeastonInputValue(this.skusArray,'skus','wo_qty');
    }

    if(this.WODForm.valid&&validValue&&validQtyDetail){
      let data=this.WODForm.value;
      // check by order , if no by order -> remove order number
      if(!this._byOrder){
        data['odr_num']='';
      }
      let data_json = JSON.stringify(data);


      if (this.Action == 'new') {
        // add new
        this.CreateWorkOrder(data_json);
      }
      else {
        // Update
        this.Update(data_json);
      }

    }
    else{
        if(!validValue)
        {
          this.messages= this._Func.Messages('danger', this._Func.msg('BM044'));
        }
    }

  }

  CreateWorkOrder(data){
    let n=this;
    this.showLoadingOverlay=true;
    this.workODService.createWorkOrder(data).subscribe(
      data => {

        this.showLoadingOverlay = false;

        if (this.flagFinalForNew) {

          n.wodr_id = data['wo_hdr_id'];
          n.finalForNew();

        } else {

          //Reset Form
          this.messages = this._Func.Messages('success', this._Func.msg('VR107'));
          setTimeout(function(){
            n._router.parent.navigateByUrl('/outbound/work-order');
          }, 600);
        }

      },
      err => {
        this.showLoadingOverlay= false;
        this.parseErrorStandard(err);
      },
      () => {}
    );
  }

  Update(data){

    //
    let n=this;
    this.showLoadingOverlay=true;
    this.workODService.updateWorkOrder(data).subscribe(
      data => {
        this.showLoadingOverlay=false;
        this.messages = this._Func.Messages('success', this._Func.msg('VR109'));
        setTimeout(function(){
          n._router.parent.navigateByUrl('/outbound/work-order');
        }, 600);
      },
      err => {
        this.showLoadingOverlay=false;
        this.parseError(err);
      },
      () => {}
    );

  }


  changeWOQtyChargeCode(value, item, parentRow) {
    item['invaild_qty'] = false;
    item['zero_qty'] = false;
    if (value && this._byOrder && this._wordOrderby === 'chargecode') {
      const woQTY = parseInt(value);
      const orderedQTY = parseInt(this.chargecodesArray[parentRow]['value_selected']['qty']);
      if (woQTY > orderedQTY) {
        item['invaild_qty'] = true;
      }
      if (woQTY === 0) {
        item['zero_qty'] = true;
      }
    }
  }

  changeWOQtySku(value,item) {
    item['invaild_qty'] = false;
    item['zero_qty'] = false;
    if (value && this._byOrder && this._wordOrderby === 'sku') {
      const woQTY = parseInt(value);
      const orderedQTY = parseInt(item['qty']);
      if (woQTY > orderedQTY) {
        item['invaild_qty'] = true;
      }

      if (woQTY === 0) {
        item['zero_qty'] = true;
      }
    }
  }

  checkVaildQTy(array:Array<any>,controlGroup){
    if(this._byOrder){
      for(let i in array){
        for(let j in array[i][controlGroup]){
          if(array[i][controlGroup][j]['invaild_qty']){
            return false;
          }
        }
      }
      return true;
    }
    else{
      // return true if not by order
      return true;
    }
  }


  getListSkus($param,endableLoading=false){

    if(this.Action=='edit'||this.IsView){
      // add param
      $param+='?update=true';
    }

    if(endableLoading){
      this.loading['loadchargecode']=true;
    }
    this.workODService.getODinfo($param).subscribe(
      data => {
        this.listSkus = data['skus']; // skus
        this.loading['loadchargecode']=false;
        this.order_info=data['order_info'];

        // function for edit page
        if(this.Action=='edit'){
          for(let i in this.chargecodesArray){
            this.createValueSelected(i,this.chargecodesArray[i]['item_id']);
          }

        }

      },
      err => {
        this.loading['loadchargecode']=false;
        this.parseError(err);
      },
      () => {}
    );
  }

  checkInputNumber(event)
  {
    this._Valid.isNumber(event);
  }

  // get list custommer

  private getCustomersByWH () {

    let params='?whs_id=' + this.whs_id + "&limit=10000";
    this.workODService.getCustomersByWH(params)
      .subscribe(
        data => {
          this.Customers = data.data;
        },
        err => {
          this.parseError(err);
        },
        () => {
        }
      );

  }


  Selected(array,row,$event,selectedAllModel){

      let checked=$event.target.checked;
      let selectData=this._Func.Selected(array,row,checked);
      if(this._wordOrderby=='chargecode'){
        this.chargecodesArray=selectData['array_data'];
        this.selectedAllChargeCode=selectData['full_selected'];
      }
      else{
        this.skusArray=selectData['array_data'];
        this.selectedAllItems=selectData['full_selected'];
      }

  }

  hasSelected(){

    let array=[];

    if(this._wordOrderby=='chargecode'){
      array=this.chargecodesArray;
    }
    else{
      array=this.skusArray;
    }

    for(let i in array){
        if(array[i]['selected'])
        {
          return true;
        }
    }
    return false;

  }

  setSelectedAll(array,$event){
    this._Func.setSelectedAll(array,$event.target.checked);
  }

  delete(){

   if(!this.hasSelected())
   {
     this.messages = this._Func.Messages('danger', this._Func.msg('VR113'));
     return;
   }

    let n = this;
    /*case 1: modify text*/
    let warningPopup = new WarningBoxPopup();
    warningPopup.text = this._Func.msg('PBAP002');
    this._boxPopupService.showWarningPopup(warningPopup)
      .then(function (dm) {
        if(!dm){
          return;
        }else {
          if (n._wordOrderby == 'chargecode') {
            // delete chargecode
            n._Func.deleteItemList(n.chargecodesArray);
            if(n.chargecodesArray.length==0){
              n.selectedAllChargeCode=false;
              n.addRowChargcode();
            }
          }
          else {
            // delete items
            n._Func.deleteItemList(n.skusArray);
            if(n.skusArray.length==0){
              n.selectedAllItems=false;
              n.addNewSku();
            }
          }


          // after delete done , check duplicated
          if(n._wordOrderby=='chargecode'){
            n.checkDuplicatedValueRow(n.chargecodesArray,'item_id');
          }else{
            n.checkDuplicatedValueRow(n.skusArray,'chg_code_id');
          }

        }
      });
  }

  cancel()
  {
      let n = this;
      /*case 1: modify text*/
      // let warningPopup = new WarningBoxPopup();
      // warningPopup.text = "Are you sure to cancel";
      // this._boxPopupService.showWarningPopup(warningPopup)

      /*case 2: default*/
      this._boxPopupService.showWarningPopup()
          .then(function (dm) {
              if(dm)
                  n._router.navigateByUrl('/outbound/work-order');

          });
  }

  checkAtLeastonInputValue(array:Array<any>,fieldgroup,field){

   for(let i in array){
     for(let j in array[i][fieldgroup]) {
        if(array[i][fieldgroup][j][field]&&parseInt(array[i][fieldgroup][j][field])>0) {
          return true;
        }
     }
   }
    return false;
  }

  // Show error when server die or else
  private parseError (err) {
    this.messages = {'status' : 'danger', 'txt' : this._Func.parseErrorMessageFromServer(err)};
  }
  private parseErrorStandard (err) {
    this.messages = {'status' : 'danger', 'txt' : this._Func.parseErrorMessageFromServerStandard(err)};
  }

  deleteEmptyValueRow(listArray:Array<any>,field){

    let arayTmp=listArray.slice();

    arayTmp.forEach((item)=>{
      if(!item[field]){
        let index=listArray.indexOf(item);
        listArray.splice(index, 1);
      }
    });

  }

  redirectToList(){
    let n = this;
    /*case 2: default*/
    this._boxPopupService.showWarningPopup()
      .then(function (dm) {
        if(dm)
          n._router.parent.navigateByUrl('/outbound/work-order');
      })
  }

  private print() {

    var that = this;
    let api = that._api_Config.API_WORK_ORDER + '/' + this.wodr_id + '/print';

    try {

      let xhr = new XMLHttpRequest();
      xhr.open("GET", api, true);
      xhr.setRequestHeader("Authorization", 'Bearer ' + that._Func.getToken());
      xhr.responseType = 'blob';

      xhr.onreadystatechange = function () {
        if (xhr.readyState == 2) {
          if (xhr.status == 200) {
            xhr.responseType = "blob";
          } else {
            xhr.responseType = "text";
          }
        }

        if (xhr.readyState === 4) {
          if (xhr.status === 200) {
            var fileName = that.workOrderDetail['wo_hdr_num'];
            var blob = new Blob([this.response], {type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'});
            saveAs.saveAs(blob, fileName + '.pdf');
            jQuery('#printBarcode').modal('hide');

          } else {
            let errMsg = '';
            if (xhr.response) {
              try {
                var res = JSON.parse(xhr.response);
                errMsg = res.errors && res.errors.message ? res.errors.message : xhr.statusText;
              }
              catch (err) {
                errMsg = xhr.statusText
              }
            }
            else {
              errMsg = xhr.statusText;
            }
            if (!errMsg) {
              errMsg = that._Func.msg('VR100');
            }
            that.parseError(errMsg);
          }

          that.showLoadingOverlay = false;
        }
      };
      xhr.send();
    } catch (err) {

      that.parseError(that._Func.msg('VR100'));
      that.showLoadingOverlay = false;
    }
  }

  private final() {

    if (this.Action != 'new') {

      this.changeStatusFinal(this.wodr_id);
    } else {

      this.flagFinalForNew = true;
      this.Save();
    }
  }

  private finalForNew() {

    let that = this;
    let warningPopup = new WarningBoxPopup();
    warningPopup.text = 'Do you want to final the Work Order?';
    this._boxPopupService.showWarningPopup(warningPopup)
        .then(function (dm) {
          if(!dm){
            that._router.parent.navigateByUrl('/outbound/work-order');
            return;
          }else {

            that.changeStatusFinal(that.wodr_id);

          }
        });

  }

  private changeStatusFinal (wodr_id) {

    var that = this;
    this.workODService.changeStatusFinal(wodr_id)
        .subscribe(
            data => {

            },
            err => {

              that.parseError(err);
            },
            () => {
              that.isFinalWO = true;
              that.messages= that._Func.Messages('success', that._Func.msg('WO001'));

              setTimeout(function(){
                that._router.parent.navigateByUrl('/outbound/work-order');
              }, 600);

            }
        );

  }

  private updateMessages(messages) {
      this.messages = messages;
  }
}
