import {Component, DynamicComponentLoader, ElementRef, Injector} from '@angular/core';
import { Control, ControlGroup, FormBuilder, Validators } from '@angular/common';
import { Http } from '@angular/http';
import { Router, RouterLink, ROUTER_DIRECTIVES, RouteParams } from '@angular/router-deprecated';
import 'rxjs/add/operator/map';
import {API_Config} from "../../../common/core/API_Config";
import {UserService, Functions, FormBuilderFunctions} from "../../../common/core/load";
import {ASN_Services} from "../asn-services";
import {JwtHelper} from "angular2-jwt/angular2-jwt";
import {WMSPagination, AdvanceTable, WMSMessages, WMSBreadcrumb, ReportFileExporter, WMSMultiSelectComponent} from '../../../common/directives/directives';
import { OrderBy } from "../../../common/pipes/order-by.pipe";
import {BoxPopupService} from "../../../common/popup/box-popup.service";
import {WarningBoxPopup} from "../../../common/popup/warning-box-popup";
import {DataShareService} from "../../../common/services/data-share.service";
import { ValidationService } from '../../../common/core/validator';
declare var jQuery: any;
declare var jsPDF: any;
declare var saveAs: any;

@Component ({
    selector: 'asn-list',
    directives: [AdvanceTable, WMSMessages, WMSBreadcrumb, ReportFileExporter, WMSPagination, WMSMultiSelectComponent],
    templateUrl: 'asn-list.component.html',
    pipes: [OrderBy],
    providers: [ASN_Services],
})

export class ASNListComponent {
    private tableID = 'asn-list';
    private headerURL = this._API_Config.API_User_Metas + '/asn';
    private headerDef = [{id: 'ver_table', value: 5},
                      {id: 'ck', name: '', width: 25},
                     {id: 'icon', name: '', width: 50, unsortable: true},
                      {id: 'asn_sts', name: 'Status', width: 90},
                      {id: 'asn_hdr_num', name: 'ASN Number', width: 120},
                      {id: 'ref_code', name: 'Ref Code', width: 130, unsortable: true},
                      {id: 'created_at', name: 'Created Date', width: 90},
                      {id: 'customer', name: 'Customer', width: 130, unsortable: true},
                      {id: 'container_number', name: 'Carrier', width: 110, unsortable: true},
                      {id: 'asn_hdr_ept_dt', name: 'Expected Date', width: 110, unsortable: true},
                      {id: 'asn_hdr_act_dt', name: 'Goods Receipt Date', width: 110, unsortable: true},
        {id: 'item_ttl', name: '# of SKUs', width: 90, unsortable: true},
        {id: 'ctnr_ttl', name: '# of CNTRs', width: 90, unsortable: true},
        {id: 'ctn_ttl', name: '# of Cartons', width: 90, unsortable: true},
                      // {id: 'po', name: 'PO', width: 130},
                      // {id: 'po_date', name: 'PO Date', width: 130},
                      {id: 'user', name: 'User', width: 130, unsortable: true}];
    private pinCols = 5;
    private rowData: any[];
    private getSelectedRow;
    private objSort;
    private searchParams;
    private defaultSearchParams;

    private headerExport = ['ASN Number', 'Ref Code', 'Date', 'Customer', 'Container', 'X-Dock', 'Expected Date', '# of SKUs', '# of CNTRs', '# of CTNs', 'PO', 'PO Date', 'Status', 'User'];
    private dataExport: any[];
    private fileName = 'dataASN.csv';
    private exportAPIUrl: string = '';

    private whs_id: any;

    private Pagination;
    private totalCount = 0;
    private tmpPageCount;
    private pageCount=0;
    private ItemOnPage=0;
    private currentPage = 1;
    private perPage = 20;
    private numLinks = 3;
    private messages:any;
    // private flagLoading = false;
    // private flagLoading2 = false;
    private dataListAsn = [];
    private dataListCustomer = [];
    private asnStatus: any = [];
    private showLoadingOverlay = false;
    private hasEditBtn:any;
    private hasCreateBtn:any;
    private queryParams;
    SearchForm: ControlGroup;
    RMAForm: ControlGroup;
    private cancelASN = false;
    public showError = false;
    public loading = [];
    public refreshMultiSelect = false;
    public dataLPN = [];
    formLPN: ControlGroup;

    // Construct
    constructor (
        private _asnServices: ASN_Services,
        private _func: Functions,
        private fbdFunc: FormBuilderFunctions,
        private _API_Config: API_Config,
        private _Valid: ValidationService,
        private _http: Http,
        public jwtHelper: JwtHelper,
        private _router:Router,
        private fb: FormBuilder,
        private _user: UserService,
        private _boxPopupService: BoxPopupService,
        private dataShareService: DataShareService,
    ) {
        this.whs_id = localStorage.getItem('whs_id');
        this.checkPermission();
    }

    _parseQueryString(queryString) {
        var params = {}, queries = [], temp, i, l;
        queries = queryString.split("&");
        for ( i = 0, l = queries.length; i < l; i++ ) {
            temp = queries[i].split('=');
            params[temp[0]] = temp[1];
        }
        return params;
    };

    _getQueryParams() {
        // let url = window.location.href;
        // let queryString = url.indexOf('?') != -1 ? url.substring( url.indexOf('?') + 1 ) : '';

        // if (queryString == '') {

        //     queryString = this.dataShareService.data;
        // }
        const queryString = this.dataShareService.data;
        this.queryParams = this._parseQueryString(queryString);
        if(queryString.length) {
            if(this.defaultSearchParams) {
                this.defaultSearchParams += "&" + queryString;
            } else {
                this.defaultSearchParams = "&" + queryString;
            }
        }
    }

    FormBuilder() {
        this.SearchForm =this.fb.group({
            asn_hdr_num: this.queryParams['asn_hdr_num'] ? this.queryParams['asn_hdr_num'] : [''],
            cus_id: this.queryParams['cus_id'] ? [this.queryParams['cus_id']] : [''],
            ctnr_num: this.queryParams['ctnr_num'] ? [this.queryParams['ctnr_num']] : [''],
            sku: this.queryParams['sku'] ? [this.queryParams['sku']] : [''],
            asn_hdr_ept_dt: this.queryParams['asn_hdr_ept_dt'] ? [this.queryParams['asn_hdr_ept_dt']] : [''],
            asn_sts: this.queryParams['asn_sts'] ? [this.queryParams['asn_sts']] : [''],
            asn_type: this.queryParams['asn_type'] ? [this.queryParams['asn_type']] : [''],
        });
    }

    private checkPermission() {

        this._user.GetPermissionUser().subscribe(

            data => {

                this.hasEditBtn = this._user.RequestPermission(data, 'editASN');
                this.hasCreateBtn = this._user.RequestPermission(data, 'createASN');
                var flagView = this._user.RequestPermission(data,'viewASN');
                this.cancelASN = this._user.RequestPermission(data, 'cancelASN');

                if(!flagView) {
                    this._router.parent.navigateByUrl('/deny');
                } else {
                    // pass
                    this._getQueryParams();
                    this.FormBuilder();
                    this.getCustomers();
                    this.getASNStatus();
                    this.getASNTypes();

                    setTimeout(() => {
                      jQuery('#asn-list').on('mouseover', '.hover-value', function() {
                        var ul = jQuery(this).find('ul'),
                            aTag = ul.prev();
                        ul.css({
                          display: 'block',
                          position: 'fixed',
                          top: aTag.offset().top - jQuery(window).scrollTop() + 16,
                          left: aTag.offset().left,
                            zIndex: 9999
                        })
                      }).on('mouseout', '.hover-value', function() {
                        var ul = jQuery(this).find('ul');
                        ul.css({
                          display: 'none'
                        })
                      });
                    }, 400);
                }
            },
            err => {

                this.showMessage('danger', this._func.parseErrorMessageFromServer(err));

            }
        );
    }

    private showMessage(msgStatus, msg) {

        this.messages = {
            'status': msgStatus,
            'txt': msg
        }
        jQuery(window).scrollTop(0);
    }

    private requestPermission(permission, permission_req) {

        if (permission && permission.length) {

            for (var per of permission) {

                if (per.name == permission_req) {

                    return true;
                }
            }

        }

        return false;

    }

    // Format data for ag-grid
    private createRowData(data) {
        var rowData: any[] = [];
        // Check data
        if (typeof data.data != 'undefined') {
          data = this._func.formatData(data.data);
          this.dataExport = [];
            for (var i = 0; i < data.length; i++) {
                if (data[i]['containers'].length > 0 && data[i]['containers'][0]) {
                    this.dataExport.push([
                        data[i].asn_hdr_num,
                        data[i].asn_hdr_ref,
                        data[i].asn_date,
                        data[i].cus_name,
						data[i]['containers'][0]['ctnr_num'],
                        data[i].dtl_crs_doc,
                        data[i].asn_hdr_ept_dt,
                        data[i].item_ttl,
                        data[i].ctnr_ttl,
                        data[i].ctn_ttl,
                        data[i].dtl_po,
                        data[i].dtl_po_date,
                        data[i].asn_sts_name,
                        data[i].user
                    ]);

                    var containerList = '',
                        asnId = data[i]['asn_hdr_id'],
                        containers = data[i]['containers'];

                    try {
                        for (let j = 0, l = containers.length; j < l; j++) {
                            containerList += '<li><a href="#/inbound/asn/' + asnId + '/' + containers[j].ctnr_id + '">' + containers[j]['ctnr_num'] + '</a></li>';
                        }
                    }
                    catch (err) { console.log(err) };

                    let iconHTML = "";
                    if (data[i]['is_damaged'] == '1') {
                        iconHTML += "<div class='img-style'><img src='assets/images/damage.png' alt='Damaged' class='img-responsive' title='Damaged'/></div>"
                    }
                    if (data[i]['is_discrepancy'] == '1') {
                        iconHTML += "<div class='img-style'><img src='assets/images/desc.png' alt='Discrepancy' class='img-responsive' title='Discrepancy'/></div>"
                    }

                    rowData.push({
                        asn_hdr_num: '<a href="#/inbound/asn/' + data[i]['asn_hdr_id'] + '/' + data[i]['containers'][0]['ctnr_id'] + '">' + data[i].asn_hdr_num + '</a>',
                        icon: iconHTML,
                        ref_code: data[i].asn_hdr_ref,
                        created_at: data[i].asn_date,
                        customer: data[i].cus_code,
                        container_number: '<span class="hover-value">' +
                            '<a>' + data[i]['containers'][0]['ctnr_num'] + '</a>' +
                            '<ul class="nav">' + containerList + '</ul></span>',
                        x_dock: data[i].dtl_crs_doc,
                        asn_hdr_ept_dt: data[i].asn_hdr_ept_dt,
                        item_ttl: data[i].item_ttl,
                        ctnr_ttl: data[i].ctnr_ttl,
                        ctn_ttl: data[i].ctn_ttl,
                        po: data[i].dtl_po,
                        po_date: data[i].dtl_po_date,
                        asn_sts: data[i].asn_sts_name,
                        user: data[i].user,
                        asn_hdr_id: data[i].asn_hdr_id,
                        ctnr_id: data[i]['containers'][0]['ctnr_id'],
                        create_from_wap: data[i].create_from_wap
                    })
                }
            }
        }

        this.rowData = rowData;
    }

    // Get ASN
    private getListAsn(page = null) {
        this.showLoadingOverlay = true;
        if(!page) page = 1;
        var params="?page="+page+"&limit="+this.perPage+"&type=asn";
        params += "&whs_id=" + this.whs_id;
        if(this.objSort && this.objSort['sort_type'] != 'none') {
          params += '&sort['+ this.objSort['sort_field']+ ']='+ this.objSort['sort_type'];
        }
        else {
          params += '&sort[created_at]=desc';
        }
        if(this.searchParams) {
          params += this.searchParams;
        }
        if(this.defaultSearchParams) {
            params += this.defaultSearchParams;
        }

        // update exportAPIUrl
        this.exportAPIUrl = this._API_Config.API_Asns + params + '&export=1';

        this._asnServices.getListAsn(params)
            .subscribe(
                data => {
                    this.dataListAsn = data.data;
                    this.initPagination(data);
                    this.createRowData(data);
                    this.showLoadingOverlay = false;
                },
                err => {

                    this.parseError(err);
                    this.showLoadingOverlay = false;
                },
                () => {}
            );
    }

    // Get customers
    private getCustomers() {
        var $params = '?limit=10000';
        this._asnServices.getCustomersByWH($params)
            .subscribe(
                data => {
                    this.dataListCustomer = data.data;
                },
                err => {
                    this.parseError(err);
                },
                () => { }
            );
    }

    arrASNType = [];
    getASNTypes() {
        this._asnServices.getASNTypeList().subscribe(
            res => {
                this.arrASNType = res.data;
            },
            err => {
                this.parseError(err);
            });
    }

    // Get ASN All status
    private getASNStatus () {

        this._asnServices.getAsnStatus().subscribe(
            data => {
                this.asnStatus = data.data;
                let preSelectStatus = false;
                for (let i = 0; i < this.asnStatus.length; i++) {
                    const status = this.asnStatus[i];
                    status['key'] = status['asn_sts'];
                    status['value'] = status['asn_sts_name'];
                    status['checked'] = status['asn_checked'];
                    if (status.checked) {
                        preSelectStatus = true;
                    }
                }
                if (preSelectStatus) {
                    setTimeout(() => {
                        this.search();
                    }, 500);
                } else {
                    this.getListAsn(1);
                }
            },
            err => {
                this.parseError(err);
            },
            () => {}
        );

    }

    private filterList(pageNumber) {
        this.getListAsn(pageNumber);
    }

    private getPage(pageNumber) {
        let arr=new Array(pageNumber);
        return arr;
    }

    // Set params for pagination
    private initPagination(data){
        var meta = data.meta;
        this.Pagination=meta['pagination'];
        this.Pagination['numLinks']=3;
        this.Pagination['tmpPageCount']=this._func.Pagination(this.Pagination);

    }

    // Event when user filter form
    private onPageSizeChanged ($event) {
        this.perPage = $event.target.value;
        if((this.Pagination['current_page'] - 1) * this.perPage >= this.Pagination['total']) {
            this.currentPage = 1;
        }
        else {
            this.currentPage = this.Pagination['current_page'];
        }
        this.getListAsn(this.currentPage);
    }

    // Export data to PDF format
    private exportPdf(){
        var doc = new jsPDF('p', 'pt');
        doc.autoTable(this.headerExport, this.dataExport, {
              theme: 'grid'
        });
        doc.save('asn_list.pdf');
    }

    private exportCsv(){
        var content = this.dataExport;
        var finalVal = this.headerExport.join(',') + '\n';

        for (var i = 0; i < content.length; i++) {
            var value = content[i];

            for (var j = 0; j < value.length; j++) {
                var innerValue =  value[j]===null?'':value[j].toString();
                var result = innerValue.replace(/"/g, '""');
                if (result.search(/("|,|\n)/g) >= 0)
                    result = '"' + result + '"';
                if (j > 0)
                    finalVal += ',';
                finalVal += result;
            }

            finalVal += '\n';
        }
        var blob = new Blob([finalVal], { type: 'text/csv;charset=utf-8;' });

        if (navigator.msSaveBlob) { // IE 10+
            navigator.msSaveBlob(blob, finalVal);
        } else {
            var downloadLink = jQuery('<a style="visibility:hidden;" href="data:text/csv;charset=utf-8,' + encodeURIComponent(finalVal) +'" download="dataASN.csv"></a>').appendTo('body');
            downloadLink[0].click();
            downloadLink.remove();
        }
    }

    // Edit
    // Navigation to edit page
    private edit() {
        this.getSelectedRow = 'edit';
    }

    // Cancel asn
    private cancel() {
        this.getSelectedRow = 'cancel';
    }

    afterGetSelectedRow($event) {
      var listSelectedItem = $event.data;
      switch ($event.action) {
        case 'edit':
          if (listSelectedItem.length > 1) {
            this.messages = this._func.Messages('danger', this._func.msg('CF004'));
          }
          else {
            if (listSelectedItem.length < 1) {
              this.messages = this._func.Messages('danger', this._func.msg('CF008'));
            }
            else {
                // if(listSelectedItem[0].create_from_wap === 1) {
                //     this.messages = this._func.Messages('danger', 'ASN has been created from WAP cannot be updated!');
                // } else {
                    if(listSelectedItem[0].asn_sts != 'New' && listSelectedItem[0].asn_sts.toLowerCase() != 'receiving') {
                        this.messages = this._func.Messages('danger', 'Only New and Receiving ASNs can be updated!');
                    }
                    else {
                        this._router.parent.navigateByUrl('/inbound/asn/' + listSelectedItem[0].asn_hdr_id + "/" + listSelectedItem[0].ctnr_id + "/edit");
                    }
                // }
            }
          }
          break;

          case 'cancel':
              if (listSelectedItem.length > 1) {
                  this.messages = this._func.Messages('danger', this._func.msg('CF004'));
              }
              else {
                  if (listSelectedItem.length < 1) {
                      this.messages = this._func.Messages('danger', this._func.msg('CF008'));
                  }
                  else {
                      const asnObj = listSelectedItem[0];
                      if(asnObj['asn_sts'].toLowerCase() !== 'new') {

                          if (asnObj['asn_sts'].toLowerCase() == 'receiving') {

                              this._router.parent.navigateByUrl('/inbound/asn/' + listSelectedItem[0].asn_hdr_id + "/" + listSelectedItem[0].ctnr_id + "/edit");
                              break;
                          }

                          this.messages = this._func.Messages('danger', this._func.msg('INB001'));
                      } else {
                          let warningPopup = new WarningBoxPopup();
                          warningPopup.text = "Are you sure you want to cancel the ASN?";
                          const that = this;

                          that._boxPopupService.showWarningPopup(warningPopup).then(function (dm) {
                              if (dm) {
                                  that.showLoadingOverlay = true;
                                  that._asnServices.cancelAsn(asnObj['asn_hdr_id']).subscribe(
                                      data => {
                                          that.messages = that._func.Messages('success', that._func.msg('INB002'));
                                          that.getListAsn(that.Pagination['current_page']);
                                          that.showLoadingOverlay = false;
                                      },
                                      err => {
                                          that.parseError(err);
                                          that.showLoadingOverlay = false;
                                      }
                                  );
                              }
                          });
                      }
                  }
              }
              break;
              case 'print-lpn':
                this.getLPN();
                this.showError = false;
                this.formBuilderLPN();

        case 'delete':
          break;
      }

      this.getSelectedRow = false;
    }

    private doSort(objSort) {
      this.objSort = objSort;
      this.getListAsn(this.Pagination.current_page);
    }

    // Search
    private search () {
        this.searchParams = '';
        let params_arr = jQuery("#form-filter").serializeArray() ;
        for (let i in params_arr) {
            if (params_arr[i].value == "") continue;
            this.searchParams +="&" +  encodeURIComponent(params_arr[i].name.trim()) + "=" + encodeURIComponent(params_arr[i].value.trim());
        }
        this.getListAsn(1);
    }

    // Reset form
    private reset() {

        jQuery("#form-filter input[type=text], #form-filter select").each(function( index ) {

            jQuery(this).val("");
        });
        this.refreshMultiSelect = true;
        this.searchParams = '';
        this.defaultSearchParams = '';
        this.getListAsn(1);

    }

    // Show error when server die or else
    private parseError (err) {

        this.messages = {'status' : 'danger', 'txt' : this._func.parseErrorMessageFromServer(err)};
    }

    private expandTable = false;
    private viewListFullScreen() {
      this.expandTable = true;
      setTimeout(() => {
        this.expandTable = false;
      }, 500);
    }

    private updateMessages(messages) {
        this.messages = messages;
    }

    addRMA() {
        jQuery('#addRMA').modal('show');
        this.showError = false;
        if (!this.RMAForm) {
            this.RMAForm = this.fb.group({
                'odr_num': ['', Validators.required],
                'cus_odr_num': [''],
                'cus_po': [''],
            });
        } else {// reset odr_num
            (<Control>this.RMAForm.controls['odr_num']).updateValue('');
            (<Control>this.RMAForm.controls['cus_odr_num']).updateValue('');
            (<Control>this.RMAForm.controls['cus_po']).updateValue('');
        }
    }

    // Add
    // Navigation to Add page
    private add() {
        this._router.parent.navigateByUrl('/inbound/asn/new');
    }


    saveRMA() {
        if (this.RMAForm.valid) {
            const data = this.RMAForm.value;
            let data_json = JSON.stringify(data);
            this.showLoadingOverlay = true;
            this._asnServices.saveRMA(data_json).subscribe(
                data => {
                    this.showLoadingOverlay = false;
                    this.messages = this._func.Messages('success', this._func.msg('VR107'));
                    jQuery('#addRMA').modal('hide');
                    this.getListAsn(1);
                },
                err => {
                    this.showLoadingOverlay = false;
                    jQuery('#addRMA').modal('hide');
                    this.parseError(err);
                },
                () => {}
            )
        } else {
            this.showError = true;
        }
    }

    private order_list: any[] = [];
    private autoComplete($event) {
        const key = $event.target.value;
        const param = '?odr_num=' + key + '&limit=20&page=1';
        const enCodeSearchQuery = this._func.FixEncodeURI(param);
        const that = this;
        that.order_list = [];

        if (key) {
            // do Search
            this.loading['odr_num'] = true;
            this._asnServices.autocomplete(enCodeSearchQuery).debounceTime(400).distinctUntilChanged().subscribe(
                data => {
                    that.loading['odr_num'] = false;
                    that.order_list = data.data;
                    let item = that.findKeyInList(that.order_list, 'odr_num', that._func.trim(key));
                    if (item) {
                        that.selectedOrder(item);
                    }
                },
                err => {
                    that.loading['odr_num'] = false;
                    that.parseError(err);
                },
                () => {
                }
           );
        }
    }

    findKeyInList(Array: any[] = [],filename: any, key: any) {
        for (let item of Array) {
            if (this._func.trim(item[filename]) == key) {
                return item;
            }
        }

        return null;
    }

    private selectedOrder(item={}){
        this.fbdFunc.setValueControl(this.RMAForm, 'odr_num', item['odr_num']);
    }

    private cus_order_list: any[] = [];
    private autoCompleteCusOrderNum($event) {
        const key = $event.target.value;
        const param = '?cus_odr_num=' + key + '&limit=20&page=1';
        const enCodeSearchQuery = this._func.FixEncodeURI(param);
        const that = this;
        that.cus_order_list = [];

        if (key) {
            // do Search
            this.loading['cus_odr_num'] = true;
            this._asnServices.autocomplete(enCodeSearchQuery).debounceTime(400).distinctUntilChanged().subscribe(
                data => {
                    that.loading['cus_odr_num'] = false;
                    that.cus_order_list = data.data;
                    let item = that.findKeyInList(that.cus_order_list, 'cus_odr_num', that._func.trim(key));
                    if (item) {
                        that.selectedCusOrder(item);
                    }
                },
                err => {
                    that.loading['cus_odr_num'] = false;
                    that.parseError(err);
                },
                () => {
                }
           );
        }
    }

    private selectedCusOrder(item={}){
        this.fbdFunc.setValueControl(this.RMAForm, 'cus_odr_num', item['cus_odr_num']);
        this.fbdFunc.setValueControl(this.RMAForm, 'odr_num', item['odr_num']);
        this.fbdFunc.setValueControl(this.RMAForm, 'cus_po', '');
    }

    private po_list: any[] = [];
    private autoCompletePO($event) {
        const key = $event.target.value;
        const param = '?cus_po=' + key + '&limit=20&page=1';
        const enCodeSearchQuery = this._func.FixEncodeURI(param);
        const that = this;
        that.po_list = [];

        if (key) {
            // do Search
            this.loading['cus_po'] = true;
            this._asnServices.autocomplete(enCodeSearchQuery).debounceTime(400).distinctUntilChanged().subscribe(
                data => {
                    that.loading['cus_po'] = false;
                    that.po_list = data.data;
                    let item = that.findKeyInList(that.cus_order_list, 'cus_po', that._func.trim(key));
                    if (item) {
                        that.selectedPO(item);
                    }
                },
                err => {
                    that.loading['cus_po'] = false;
                    that.parseError(err);
                },
                () => {
                }
           );
        }
    }

    private selectedPO(item={}){
        this.fbdFunc.setValueControl(this.RMAForm, 'cus_po', item['cus_po']);
        this.fbdFunc.setValueControl(this.RMAForm, 'odr_num', item['odr_num']);
        this.fbdFunc.setValueControl(this.RMAForm, 'cus_odr_num', '');
    }
    private printLPN() {
        this.getSelectedRow = 'print-lpn';
    }

    private getLPN() {
        this.limitLPN['end'] = '';
        this._asnServices.getLPN().subscribe(
            data => {
                this.dataLPN = data.data;
                this.limitLPN['start'] = this.dataLPN['to'] + 1;
                jQuery('#print-lpn').modal('show');
                setTimeout(() => jQuery('#Lpn_field').focus(), 500);
            },
            err => { }
        )
    }

    private limitLPN = [];
    private inputLPN(event:any) {
        this.limitLPN['start'] = parseInt(this.dataLPN['to']) + 1 ;
        if (event != '') {
            this.limitLPN['end'] = parseInt(this.dataLPN['to']) + parseInt(event);
            if (this.limitLPN['end'] >= 1000000) {
                this.fbdFunc.setErrorControl(this.formLPN, 'lpn_field', { max:true });
            } else {
                this.fbdFunc.setErrorControl(this.formLPN, 'lpn_field',null);
            }
        } else {
            this.limitLPN['end'] = '';
        }
    }

    private formBuilderLPN() {
        if (!this.formLPN){
            this.formLPN = this.fb.group({
                'lpn_field': ['', Validators.required]
            });

        } else {
            (<Control>this.formLPN.controls['lpn_field']).updateValue('');
        }
    }

    private printLPNData() {
        let lpn_qty = this.formLPN.value.lpn_field;

        if (this.formLPN.valid) {
            var that = this;
            let api = this._API_Config.API_GOODS_RECEIPT +'/warehouse/print-lpn?lpn_to_print='+ lpn_qty;
            try{
            let xhr = new XMLHttpRequest();
            xhr.open("GET", api , true);
            xhr.setRequestHeader("Authorization", 'Bearer ' + that._func.getToken());
            xhr.responseType = 'blob';

            xhr.onreadystatechange = function () {
                if(xhr.readyState == 2) {
                if(xhr.status == 200) {
                    xhr.responseType = "blob";
                } else {
                    xhr.responseType = "text";
                }
                }

                if(xhr.readyState === 4) {
                if(xhr.status === 200) {
                    var today = new Date();
                    var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
                    "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
                    ];
                    var day = today.getDate();
                    var month = monthNames[today.getMonth()];
                    var year = today.getFullYear().toString().substr(2,2);
                    var fileName = 'LPN_' + month +'_'+ day + '_' + year;
                    var blob = new Blob([this.response], {type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'});
                    saveAs.saveAs(blob, fileName + '.pdf');
                } else {
                    let errMsg = '';
                    if(xhr.response) {
                        try {
                            var res = JSON.parse(xhr.response);
                            errMsg = res.errors && res.errors.message ? res.errors.message : xhr.statusText;
                        } catch(err){errMsg = xhr.statusText}
                        }
                        else {
                            errMsg = xhr.statusText;
                        }
                        if(!errMsg) {
                            errMsg = that._func.msg('VR100');
                        }
                        that.showMessage('danger', errMsg);
                    }
                }
            }
            xhr.send();
            jQuery('#print-lpn').modal('hide');
            } catch (err){
                that.showMessage('danger', that._func.msg('VR100'));
            }
        } else {
            this.showError = true;
        }
    }

    private checkInputNumber(evt, int=true) {
        this._Valid.isNumber(evt, int);
    }
}
