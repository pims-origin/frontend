import {Component} from '@angular/core';
import {Functions} from "../common/core/functions";
import {Http} from "@angular/http";
import {API_Config} from "../common/core/API_Config";
import {WMSBreadcrumb} from "../common/directives/wms-breadcrumb/wms-breadcrumb";
import {JwtHelper} from "angular2-jwt/angular2-jwt";
import {UserService} from "../common/users/users.service";
import {WMSPagination, PaginationControlsCmp, PaginationService} from "../common/directives/directives";
import {WavepickDashboard} from './wavepick-dashboard/wavepick-dashboard';
import {OccupancyPCTDashboard} from './occupancy-pct-dashboard/occupancy-pct-dashboard';
import {ZoneCapacityDashboard} from './zone-capacity-dashboard/zone-capacity-dashboard';
import {RoomCapacityDashboard} from './room-capacity-dashboard/room-capacity-dashboard';
import {HistogramCapacityChart} from './histogram-capacity-chart/histogram-capacity-chart';
import { financialRevenue } from './financial-revenue/financial-revenue';
import {AlertBarDashboard} from './alert-bar-dashboard/alert-bar-dashboard';
import { Router } from '@angular/router-deprecated';
import { DataShareService } from '../common/services/data-share.service';
import { CycleCountDashboard } from './cycle-count-dashboard/cycle-count-dashboard';
import { StorageAgingSKUDashboard } from './storage-aging-dashboard/storage-aging-sku-dashboard/storage-aging-sku-dashboard';
import { StorageAgingCustomerDashboard } from './storage-aging-dashboard/storage-aging-cus-dashboard/storage-aging-cus-dashboard';

declare var jQuery:any;

@Component({
	selector: 'dashboard',
	directives: [
		WMSBreadcrumb, WavepickDashboard,
		WMSPagination, PaginationControlsCmp,
		OccupancyPCTDashboard, ZoneCapacityDashboard,
		RoomCapacityDashboard, HistogramCapacityChart,
		financialRevenue, AlertBarDashboard, CycleCountDashboard,
		StorageAgingSKUDashboard, StorageAgingCustomerDashboard
	],
	providers: [UserService, PaginationService],
	templateUrl: 'dashboard.component.html',
})
export class DashboardComponent {
	private dashboardData = {};
	private showLoadingOverlay:boolean = false;
	private userRole = '';
	private isShowDashboard = true;
	private messages:any;

	// Check permissions
	private canAccessReceiving:boolean = false;
	private canAccessPutaway:boolean = false;
	private canAccessOrder:boolean = false;
	private canAccessInprogress:boolean = false;
	private isShownDashboard:boolean = false;
	private canAccessRoom = false;
	private canAccessProduct = false;
	private canOccupancyPCT = false;
	private canZoneCapacity = false;
	private canCycleCount = false;

	// Current whsId
	private whsId:string = '';
	private filterDay:string = '0';

	constructor(
		private _Func:Functions,
		private _API:API_Config,
		private jwtHelper:JwtHelper,
		private http:Http,
		private userService:UserService,
		private dataShareService: DataShareService,
		private _router: Router
	) {
		this.whsId = localStorage.getItem('whs_id');
		this.checkPermission();
	}

	private checkPermission() {
		this.showLoadingOverlay = true;
		this.userService.GetPermissionUser().subscribe(
			(data:any) => {
				this.canAccessReceiving = this.userService.RequestPermission(data, 'dashboardReceiving');
				this.canAccessPutaway = this.userService.RequestPermission(data, 'dashboardPutaway');
				this.canAccessOrder = this.userService.RequestPermission(data, 'dashboardOrder');
				this.canAccessInprogress = this.userService.RequestPermission(data, 'dashboardInprogress');
				this.canAccessRoom = this.userService.RequestPermission(data, 'dashboardWarehouseCapacity');
				this.canAccessProduct = this.userService.RequestPermission(data, 'dashboardProduct');
				this.canOccupancyPCT = this.userService.RequestPermission(data, 'dashboardWarehouseCapacity');
				this.canZoneCapacity = this.userService.RequestPermission(data, 'dashboardWarehouseCapacity');
				this.canCycleCount = this.userService.RequestPermission(data, 'dashboardWarehouseCapacity');
				this.isShownDashboard = this.canAccessReceiving || this.canAccessPutaway || this.canAccessOrder || this.canAccessInprogress;
				this.loadDashboard('0', false);
			},
			err => {
				this.messages = this._Func.Messages('danger', this._Func.parseErrorMessageFromServer(err));
				this.showLoadingOverlay = false;
			},
			() => {
				this.triggerWarehouseDropdown();
			}
		);
	}

	private triggerWarehouseDropdown() {
		let that = this;
		jQuery('.warehouse-selected').click(function () {
			that.whsId = localStorage.getItem('whs_id');
			that.loadDashboard('0', true);
		});
	}

	private getAsnInfo(day = '', isAdmin = false) {
		let param = 'asns';
		if (isAdmin) {
			param = 'asn-admin';
		}
		let api = this._API.API_DashBoard + '/' + param;
		if (day != '') {
			api += '?day=' + day;
		}
		this.http.get(api, {headers: this._Func.AuthHeader()})
			.map(res => res.json().data).subscribe(
			data => {
				this.dashboardData['asns'] = data;
			},
			err => {
				this.showLoadingOverlay = false;
			}
		);
	}

	private getOrderInfo(day = '', isAdmin = false) {
		let param = 'orders';
		if (isAdmin) {
			param = 'order-admin';
		}
		let api1 = this._API.API_DashBoard + '/' + param;
		if (day != '') {
			api1 += '?day=' + day;
		}
		this.http.get(api1, {headers: this._Func.AuthHeader()})
			.map(res => res.json().data).subscribe(
			data => {
				this.dashboardData['orders'] = data;
				this.showLoadingOverlay = false;
			},
			err => {
				this.showLoadingOverlay = false;
			}
		);
	}

	private getInprocess(day = '') {
		let api2 = this._API.API_DashBoard + '/order-inprocess-admin';
		if (day != '') {
			api2 += '?day=' + day;
		}
		this.http.get(api2, {headers: this._Func.AuthHeader()})
			.map(res => res.json().data).subscribe(
			data => {
				this.showLoadingOverlay = false;
				this.dashboardData['inprocess'] = data;
			},
			err => {
				this.showLoadingOverlay = false;
			}
		);
	}

	private  getPutAwayAdmin(day = '') {
		let api = this._API.API_DashBoard + '/putaway-admin';
		if (day != '') {
			api += '?day=' + day;
		} else {
			api += '?day=0';
		}
		this.http.get(api, {headers: this._Func.AuthHeader()})
			.map(res => res.json()).subscribe(
			data => {
				this.dashboardData['putawayAadmin'] = data;
				this.showLoadingOverlay = false;
			},
			err => {
				this.showLoadingOverlay = false;
			}
		);
	}

	private loadDashboard(day = '', changeWHS = false) {

		// if change whs then check first radio
		if (changeWHS) {
			jQuery('#inlineRadio0').prop('checked', true);
		}
		this.filterDay = day;
		this.canAccessReceiving && this.getAsnInfo(day, true);
		this.canAccessPutaway && this.getPutAwayAdmin(day);
		this.canAccessOrder && this.getOrderInfo(day, true);
		this.canAccessInprogress && this.getInprocess(day);

		this.showLoadingOverlay = false;
	}

	private redirectPage(url, queryString) {
		this.dataShareService.data = queryString ? queryString + '&day=' + this.filterDay : 'day=' + this.filterDay;
		this._router.parent.navigateByUrl(url);
	}

	showLoading($event) {
		this.showLoadingOverlay = $event;
	}

}
