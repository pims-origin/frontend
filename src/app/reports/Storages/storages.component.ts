
import {Component} from '@angular/core';
import {API_Config} from '../../common/core/API_Config';
import {BaseTable} from '../../common/core/classes_us/base-table';
import {Functions} from '../../common/core/functions';
import {TableFunctions} from '../../common/core/table-functions';
import {DateFunctions} from '../../common/core/functions/date-functions';
import {BaseService} from '../../common/core/classes_us/base-service';
import {CustomersService} from '../../master-data/customers/customers-service';
import {TableStructure} from '../../common/directives/directives';

@Component ({
    selector: 'Storages',
    directives: [
        TableStructure
    ],
    templateUrl: '../../common/directives/table-structure/table.component.html',
    providers: [
        BaseService,
        CustomersService,
        TableFunctions,
        DateFunctions
    ]
})

export class StoragesComponent extends BaseTable
{
    public tableID = 'storages';
    public url = this._api.API_STORAGES_REPORTS;
    public headerUrlSuffix = 'rep';
    public title = 'Storages';
    public permissionPage = 'viewReport';
    public useExport = true;
    public exportMethod = 'GET';

    public headerDef = [
        {
            id: 'ver_table',
            value: 27
        },
        this.columnList.warehouse,
        this.columnList.customer,      
       //  this.columnList.sku,
        // this.columnList.plateNumber,
        this.columnList.location_name,
        this.columnList.upc,
        this.columnList.sku,
        this.columnList.cartonStatus,
        this.columnList.lot,
        this.columnList.color,
        this.columnList.size,
        this.columnList.length,
        this.columnList.width,
        this.columnList.height,
        this.columnList.cupercarton,
        this.columnList.weight,
        this.columnList.packSize,
        this.columnList.uom,
        this.columnList.ctns,
        this.columnList.qty,
        this.columnList.totalcu,
    ];

     public unsortableColumns = [
        'ctns',
        'packSize',
        'qty',
        'customer',
        'upc',
        'uom',
        'sku',
        'color',
        'size',
        'length',
        'width',
        'height',
        'cupercarton',
        'totalcu',
        'cartonStatus',
     ];

     public searches = [
        this.searchList.customer,
        this.searchList.from,
        this.searchList.to,
        this.searchList.sku
     ];

    /*
    ****************************************************************************
    */

    constructor (
        public _api: API_Config,
        public _func: Functions,
        public _tableFunc: TableFunctions,
        public _tableService: BaseService,
        public _customersService: CustomersService
    )
    {
        super();

        this._tableFunc.permissionCall(this);
    }

    //**************************************************************************

}
