import { Component, Input, Output, EventEmitter } from '@angular/core';
import { CSRListService } from './csr-list.service';
import { Functions } from '../../../common/core/load';
import { PaginationControlsCmp, PaginationService, PaginatePipe, PopupAssignMultiCSR } from '../../../common/directives/directives';
import { OrderBy } from '../../../common/pipes/order-by.pipe';
import { WarningBoxPopup, BoxPopupService } from '../../../common/popup/index';
declare const jQuery: any;

@Component({
    selector: 'csr-list',
    providers: [PaginationService, CSRListService],
    directives: [PaginationControlsCmp, PopupAssignMultiCSR],
    pipes: [OrderBy, PaginatePipe],
    templateUrl: 'csr-list.html',
})

export class CSRListComponent {
    public _order;
    public _orderId;
    public dataCSR = [];
    public listCSR = [];
    public allCSR = [];
    public tempAllCSR = [];
    public selectedItems = [];
    public selectedAll = {
        'popupCSR': false,
        'listCSR': false
    };
    public allowAssignCSR = false;

    @Output() showLoading  = new EventEmitter();

    @Output() showMessage = new EventEmitter();

    @Input('order') set order(value:any) {
        if(value){
            this._order = value;
            this.allowAssignCSR = ['CC', 'SH'].indexOf(this._order['odr_sts_key']) === -1;
        }
    }

    @Input('orderId') set orderId(value:any) {
        if(value){
            this._orderId = value;
            this.getList();
            this.getDataCSR();
        }
    }

    public infoSearchUser = {};
    public oldDataCSRUser = [];
    constructor(
        private _service: CSRListService,
        private _func: Functions,
        private _boxPopupService: BoxPopupService,
    ) {
    
    }

    public getDataCSR() {
        // this.showLoading.emit(true);
        const whs = localStorage.getItem('whs_id');
        this._service.getListCRSByWarehouse(whs)
            .subscribe(
                data => {
                    if(data.data && data.data.length) {
                        this.dataCSR = data.data;
                    }
                    else {
                        const msg = {'status': 'danger', 'msg': this._func.msg('VR129')};
                        this.showMessage.emit(msg);
                    }
                    // this.showLoading.emit(false);
                },
                err => {
                    const msg = {'status': 'danger', 'msg':  this._func.parseErrorMessageFromServer(err)};
                    this.showMessage.emit(msg);
                    // this.showLoading.emit(false);
                }
            );
    }

    // Get list
    public getList() {
        // this.showLoading.emit(true);
        this._service.getListCSR(this._orderId).subscribe(
            data => {
                // this.showLoading.emit(false);
                this.listCSR = data.data;
                this.tempAllCSR = this.listCSR.slice();
            },
            err => {
                // this.showLoading.emit(false);
            },
            () => {}
        );

    }

    public checkAll(event, fieldSelectedAll, listItems) {
        this.selectedAll[fieldSelectedAll] = !this.selectedAll[fieldSelectedAll];
        // set empty array  ListSelectedItem
        this.selectedItems = [];
        // Loop
        listItems.forEach((item) => {
            item['selected'] = this.selectedAll[fieldSelectedAll];
            item['odr_id'] = parseInt(this._orderId, 10);
            item['csr'] = parseInt(item['user_id'], 10);
            if(this.selectedAll[fieldSelectedAll]) {
                // if check all = true , pushing to ListSelectedItem
                this.selectedItems.push(item);
            }
        })
    }

    public checkSelectItem(item, fieldSelectedAll, listItems) {
        item['selected'] = !item['selected'];
        this.selectedAll[fieldSelectedAll] = false;
        // reset this.assignUser
        this.selectedItems = [];
        for (let i = 0, l = listItems.length; i < l; i++) {
            item['odr_id'] = parseInt(this._orderId, 10);
            item['csr'] = parseInt(item['user_id'], 10);
            if (listItems[i]['selected']) {
                this.selectedItems.push(listItems[i]);
            }
        }
        if (this.selectedItems.length === listItems.length) {
            this.selectedAll[fieldSelectedAll] = true;
        }
    }

    private _resetSelectedItems() {
        this.selectedAll['listCSR'] = false;
        this.selectedAll['popupCSR'] = false;
        this.selectedItems = [];
    }

    public delete() {
        if (this.selectedItems.length > 0) {
            const that = this;
            const warningPopup = new WarningBoxPopup();
            warningPopup.text = this._func.msg('PBAP002');
            this._boxPopupService.showWarningPopup(warningPopup).then(function (dm) {
                if(dm){
                    // call api to update data
                    const CSRs = that.filterArrayByField(that.listCSR.slice(), that.selectedItems, 'user_id');
                    that._saveCSR(CSRs);
                }
            });
        } else {
            const msg = {'status': 'danger', 'msg': this._func.msg('CF005')};
            this.showMessage.emit(msg);
        }
    }

    private filterArrayByField(array_data, listFilter, field) {
        if (listFilter.length) {
            for (let i in listFilter) {
                for (let j = 0; j < array_data.length; j++) {
                    if (listFilter[i][field] == array_data[j][field]) {
                        let el = array_data.indexOf(array_data[j]);
                        array_data.splice(el, 1);
                    }
                }
            }
        }
        return array_data;
    }

    private getPopupCSR() {
        this.tempAllCSR = this.filterArrayByField(this.dataCSR.slice(), this.listCSR, 'user_id');
        for (var i = 0, l = this.tempAllCSR.length; i < l; i++) {
          this.tempAllCSR[i]['selected'] = false;
        }
        this.allCSR = this.tempAllCSR.slice();
        this.oldDataCSRUser =  this.tempAllCSR.slice();
        this.infoSearchUser = {};
    }

    public assignCSR() {
        const CSRs = this.listCSR.concat(this.selectedItems);
        this._saveCSR(CSRs);
    }

    public _saveCSR(CSRs) {
        const data = {
            'data': CSRs
        }
        this.showLoading.emit(true);
        this._service.saveListCSR(this._orderId, JSON.stringify(data)).subscribe(
            data => {
                jQuery('#assign-csr').modal('hide');
                this.getList();
                this.showLoading.emit(false);
                this._resetSelectedItems();
            },
            err => {
                jQuery('#assign-csr').modal('hide');
                const msg = {'status': 'danger', 'msg': this._func.parseErrorMessageFromServer(err)};
                this.showMessage.emit(msg);
                this.showLoading.emit(false);
            }
        );
    }

    filterUser() {
        let data = this.oldDataCSRUser;
        if (this.infoSearchUser['first_name']) {
            data = this.oldDataCSRUser.filter(x => x.first_name.toLowerCase().includes(this.infoSearchUser['first_name'].toLowerCase()));
        }
        if (this.infoSearchUser['last_name']) {
            data = data.filter(x => x.last_name.toLowerCase().includes(this.infoSearchUser['last_name'].toLowerCase()));
        }
        if (this.infoSearchUser['email']) {
            data = data.filter(x => x.email.toLowerCase().includes(this.infoSearchUser['email'].toLowerCase()));
        }
        this.allCSR = data;
    }

    resetFilterUser() {
        this.infoSearchUser['first_name'] = '';
        this.infoSearchUser['last_name'] = '';
        this.infoSearchUser['email'] = '';
        this.allCSR = this.oldDataCSRUser;
    }
}
