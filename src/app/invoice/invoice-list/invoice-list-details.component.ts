
import {Component, ViewChild} from '@angular/core';
import {API_Config} from '../../common/core/API_Config';
import {BaseTable} from '../../common/core/classes_us/base-table';
import {TableFunctions} from '../../common/core/table-functions';
import {TableStructure} from '../../common/directives/directives';
import {InvoiceListDetailsServices} from './invoice-list-details-service';
import {InvoiceInfo} from './invoice-info/invoice-info.component';

declare var jQuery: any;

@Component({
    selector: 'invoice-list-details',
    directives: [
        TableStructure,
        InvoiceInfo
    ],
    providers: [
        TableFunctions,
        InvoiceListDetailsServices
    ],
    templateUrl: 'invoice-list-details.component.html'
})

export class InvoiceListDetailsComponent extends BaseTable
{
    @ViewChild(InvoiceInfo) invoiceInfo: InvoiceInfo;

    public tableID = 'invoice-list-details';
    public headerURL = this._API_Config.API_User_Metas + '/rep';

    public headerDef = [
        {
            id: 'ver_table',
            value: 1 + Math.random() * 10000
        },
        {
            id: 'radio',
            name: '',
            width: 20,
            unsortable: true
        },
        {
            id: 'info',
            name: '',
            width: 30,
            template: '<i class="icon-info"></i>',
            unsortable: true
        },
        {
            id: 'display_cat',
            name: 'CTG',
            width: 300,
            unsortable: true
        },
        {
            id: 'display_name',
            name: 'ID',
            width: 300,
            unsortable: true
        },
        {
            id: 'display_dt',
            name: 'DT',
            width: 300,
            unsortable: true
        }
    ];

    public skipPageSize = true;
    public skipExport = true;

    public permissionPage = 'viewInvoice';

    constructor (
        public _API_Config: API_Config,
        public _tableFunc: TableFunctions,
        public _tableService: InvoiceListDetailsServices
    )
    {
        super();

        this.tableComponent = this;
    }

    //**************************************************************************

    public gridUpdate(detailsInfo, chargeTypes, billedItems)
    {
        this.rowData = [];

        for (let count=0; count<detailsInfo.length; count++) {

            let value = detailsInfo[count];

            if (value.cat == 'Receiving' && chargeTypes.receiving
             || value.cat == 'Order Processing' && chargeTypes.processing
             || value.cat == 'Storage' && chargeTypes.storage) {

                value.display_cat = value.cat;

                if (value.cat == 'Receiving') {
                    value.display_name = value.ext.name + ' / ' + value.ext.refCode;
                } else {
                    value.display_name = value.hasOwnProperty('ext') ?
                        value.ext.name : value.id;
                }

                value.radio = billedItems == 'all'
                           || billedItems.hasOwnProperty(value.cat)
                           && billedItems[value.cat].hasOwnProperty(value.id) ?
                        '' : `<input type="checkbox" class="processing"
                               data-cat="` + value.cat + `"
                               data-id="` + value.id + `">`;

                value.display_dt = value.dt;

                if (value.cat == 'Order Processing' && chargeTypes.processing
                 && value.order == 'Cancel') {

                    value.display_cat = `
                        <span class="red">` + value.cat + `</span>
                    `;

                    value.display_name =  `
                        <span class="red">` + value.display_name + `</span>
                    `;

                    value.display_dt =  `
                        <span class="red">` + value.dt + `</span>
                    `;
                }

                this.rowData.push(value);
            }
        }

        this.Pagination = {
            current_page: 1,
            total: this.rowData.length
        };
    }

    //**************************************************************************

    public selectProcessing(checked: boolean)
    {
        jQuery('#invoice-list-details .processing').prop('checked', checked);
    }

    //**************************************************************************

    public onCellClicked($event)
    {
        let cellClicked = $event.colDef.field,
            index = $event.rowIndex;

        if (! ~jQuery.inArray(cellClicked, ['info'])) {
            return;
        }

        this.showDetailsInfo(this.rowData[index]);
    }

    //**************************************************************************

    private showDetailsInfo(info: any)
    {
        let infoParams = [
                {
                    caption: 'ID',
                    value: info.id
                },
                {
                    caption: 'Category',
                    value: info.cat
                }
            ];

        switch (info.cat) {
            case 'Receiving':

                infoParams = infoParams.concat([
                    {
                        caption: 'Client',
                        value: info.cust
                    },
                    {
                        caption: 'Container Name',
                        value: info.ext.name + ' / ' + info.ext.refCode
                    },
                    {
                        caption: 'Measurement',
                        value: info.ext.meas
                    },
                    {
                        caption: 'Date',
                        value: info.dt
                    }
                ]);

                break;

            case 'Storage':

                infoParams = infoParams.concat([
                    {
                        caption: 'qty',
                        value: info.qty
                    },
                    {
                        caption: 'uom',
                        value: info.uom
                    },
                    {
                        caption: 'Date',
                        value: info.dt
                    }
                ]);

                break;

            case 'Order Processing':

                infoParams = infoParams.concat([
                    {
                        caption: 'Client',
                        value: info.cust
                    },
                    {
                        caption: 'Customer Name',
                        value: info.ext.custName
                    },
                    {
                        caption: 'Order Number',
                        value: info.ext.name
                    },
                    {
                        caption: 'Customer Order Number',
                        value: info.ext.custNum
                    },
                    {
                        caption: 'Client Order Number',
                        value: info.ext.clientOrdNum
                    },
                    {
                        caption: 'Carton Quantity',
                        value: info.ext.cq
                    },
                    {
                        caption: 'Pieces Quantity',
                        value: info.ext.pq
                    },
                    {
                        caption: 'Date',
                        value: info.dt
                    },
                    {
                        caption: 'Order Type',
                        value: info.order
                    }
                ]);

                break;

            default:

                infoParams = [];

                break;
        }

        this.invoiceInfo.showInvoiceInfo(info.cat, infoParams);
    }

    //**************************************************************************

}