import {Component, DynamicComponentLoader, ElementRef, Injector} from '@angular/core';
import { FORM_DIRECTIVES,ControlGroup,FormBuilder, Control ,Validators} from '@angular/common';
import { Http } from '@angular/http';
import { Router, RouterLink, ROUTER_DIRECTIVES, RouteParams, RouteData } from '@angular/router-deprecated';
import 'ag-grid-enterprise/main';
import 'rxjs/add/operator/map';
import {API_Config} from "../../common/core/API_Config";
import {Functions} from "../../common/core/functions";
import {CycleCountServices} from "../cycle-count-service";
import {JwtHelper} from "angular2-jwt/angular2-jwt";
import {WMSMessages, EventTrackingDirective,WMSBreadcrumb} from '../../common/directives/directives';
import {GoBackComponent} from "../../common/component/goBack.component";
import {UserService} from "../../common/users/users.service";
declare var jQuery: any;
declare var saveAs: any;

@Component({
  selector: 'cycle-count-detail',
  directives: [
    WMSBreadcrumb,
    GoBackComponent,
    WMSMessages,
    EventTrackingDirective
  ],
  providers: [CycleCountServices],
  templateUrl: 'cycle-count-detail.component.html',
})

export class CycleCountDetailComponent {
  private cycleID: any;
  private messages: any;
  private messagesAddSKU: any;
  private cycleDetail = {};
  private timeoutHideMessage;
  private hasCreateBtn: any;
  private dataEventTracking = [];
  public listSelectedItem = [];
  private isView;
  private isUpdate = false;
  private action;
  private perPage = 20;
  private currentPage = 1;
  private sortQuery = '';
  public loading = [];
  public showLoadingOverlay = false;
  private selectedAll = false;
  private countItemList: Array<any> = [];
  private flagHasCus = true;
  private isAddSKU = false;
  private isSaveCycle = false;
  private isCheckAll = false;
  private dataListCustomer = [];
  private dataListSku = [];
  private dataListLoca = [];
  private listNull = {};
  private urlAPICC = '';
  private whs_id = '';
  private qty_old_value = '';
  private loc_old_value = '';
  private s_loading = false;
  private dataLocation = [];
  private s_loading_list = {};
  private currentUserID = {};
  private isUpdateItem: boolean;
  private hasPrintPermission = false;
  private hasAddSkuPermission = false;
  private hasAddSkuButton = false;
  private hasSaveButton = false;
  private hasUpdateItem = false;
  private disabled = true;
  private listNull2 = {};
  private dataListPackSize = [];
  private itemselectedItemSku;
  private flagPack = true;
  private flagHiddenAQTY = {};
  private isCountedByCarton:boolean = false;
  private selectedActualLocaltionItem = {};

  private Pagination = {
    count: 0,
    current_page: this.currentPage,
    per_page: this.perPage,
    total: 0,
    total_pages: 1,
    links: {
      next: ""
    }
  };
  searchForm: ControlGroup;
  private flagHiddenRTT = false;

  constructor(private _router:Router,
              private _Func: Functions,
              private _cycleCountServices: CycleCountServices,
              params: RouteParams,
              _RTAction: RouteData,
              private _API_Config: API_Config,
              private _user: UserService) {

    this.action = _RTAction.get('action');
    this.checkPermission();

    if (this.action == 'update') {

      this.isUpdate = true;
    }

    this._user.checkAuthentication().subscribe(data => {

      this.currentUserID = data;
    });

    if (localStorage.getItem('whs_id')) {

      this.whs_id = localStorage.getItem('whs_id');
    }

    this.urlAPICC = this._API_Config.API_Cycle_Count;
    this.cycleID = params.get('id');

    this.getCycleCountDetail(this.cycleID);
    this.getWHScodeById();

    jQuery(document)
        .on('focus', '[id="act_loc"]', function() {
          var input = jQuery(this),
              ul = input.next();
          ul.css({
            position: 'fixed',
            width: input.outerWidth(),
            //top: input.offset().top - jQuery(window).scrollTop() + input.outerHeight(),
            //left: input.offset().left
          })
        })
        .on('scroll.reposition resize.reposition', function() {
          jQuery('[id="act_loc"]').each(function() {
            var input = jQuery(this),
                ul = input.next();
            ul.css({
              position: 'fixed',
              width: input.outerWidth(),
              //top: input.offset().top - jQuery(window).scrollTop() + input.outerHeight(),
              //left: input.offset().left
            })
          });
        });
  }

  /*
   *********************************************************************************************************************
   */
  private checkPermission() {

    this._user.GetPermissionUser().subscribe(

      data => {

        var isView = this._user.RequestPermission(data,'viewCycleCount');
        this.hasPrintPermission = this._user.RequestPermission(data,'printCycleCount');
        this.hasAddSkuPermission = this._user.RequestPermission(data,'addSKUCycleCount');

        if(!isView) {
          this._router.parent.navigateByUrl('/deny');
        }

        // Check edit permission
        if (this.isUpdate) {

          var isEdit = this._user.RequestPermission(data,'editCycleCount');

          if(!isEdit) {

            this._router.parent.navigateByUrl('/deny');
          }

        }

      },
      err => {

        this.showMessage('danger', this._Func.parseErrorMessageFromServer(err));

      }
    );
  }

  /*
   *********************************************************************************************************************
   */

  private getCycleCountDetail(cycleID) {

    this._cycleCountServices.getCycleCountDetail(cycleID).subscribe(
      data => {

        this.cycleDetail = data;
        this.isCountedByCarton = this.cycleDetail['count_by'] === 'CARTON' ? true : false;

        if (this.currentUserID['user_id'] == this.cycleDetail['assignee_id']) {
          this.flagHiddenRTT = true;
        }

        if ((data['cycle_sts'] == 'AS' || data['cycle_sts'] == 'RC') && this.currentUserID['user_id'] == this.cycleDetail['assignee_id']) {
          this.hasSaveButton = true;
          this.hasAddSkuButton = true;
          this.hasUpdateItem = true;
        }

        if (data.cycle_sts === 'AS') {
          this.isAddSKU = true;
          this.isSaveCycle = true;
        }

        this.isUpdateItem =  this.currentUserID['user_id'] != this.cycleDetail['assignee_id']
          || this.cycleDetail['cycle_sts'] != 'AS';

        if (this.cycleDetail['cycle_type'].trim() != 'CS') {

          this.flagHasCus = false;

          this.getCustomersByWH();
        }

        if (data.cycle_type === 'CS') {
          this.eventForDisableLoad(false);
        }

        this.getCountItemByCycleID(1);
      },
      err => {
        this.showMessage('danger', this._Func.parseErrorMessageFromServer(err));
      }
    );
  }

  /*
   *********************************************************************************************************************
   */

  private showMessage(msgStatus, msg) {
    this.messages = {
      'status': msgStatus,
      'txt': msg
    };
    jQuery(window).scrollTop(0);
  }

  /*
   *********************************************************************************************************************
   */

  private getCountItemByCycleID(page=null) {

    let param = "?page=" + page + "&limit=" + this.perPage;

    if(this.sortQuery != '') {
      param += '&'+this.sortQuery;
    }

    this.loading['getCycleCount'] = true;
    this.showLoadingOverlay = true;

    this._cycleCountServices.getCountItemByCycleID(this.cycleID, param).subscribe(
      data =>{
        this.selectedAll = false;
        this.countItemList = data.data;


        this.countItemList.forEach((item, index) => {
          if(item['cycle_dtl_sts'] === 'NW' || item['cycle_dtl_sts'] === 'RC') {
            this.isCheckAll = true;
          }
          
          if (this.currentUserID['user_id'] == this.cycleDetail['assignee_id']) {

            this.flagHiddenAQTY[index] = true;
          }

          if ((this.currentUserID['user_id'] == this.cycleDetail['assignee_id']) && item['cycle_dtl_sts'] != 'RC') {
            this.flagHiddenAQTY[index] = false;
          }
        });

        this.Pagination = data['meta']['pagination'];
        this.Pagination['numLinks'] = 3;
        this.Pagination['tmpPageCount'] = this._Func.Pagination(this.Pagination);

        this.loading['getCycleCount'] = false;
        this.showLoadingOverlay = false;
      },
      err => {

        this.showMessage('danger', this._Func.parseErrorMessageFromServer(err));
      },
      () => {}
    );
  }

  /*
   *********************************************************************************************************************
   */

  private onPageSizeChanged($event) {
    this.perPage = $event.target.value;

    if((this.Pagination['current_page'] - 1) * this.perPage >= this.Pagination['total']) {
      this.currentPage = 1;
    }
    else {
      this.currentPage = this.Pagination['current_page'];
    }
    this.getCountItemByCycleID(this.currentPage);
  }

  /*
   *********************************************************************************************************************
   */

  private checkSelectItem($event) {
    let id = $event.target.value;
    this.selectedAll=false;
    if ($event.target.checked) {
      this.listSelectedItem.push(id);
      this.setItemListSelected(id, true);

      if(this.listSelectedItem.length==this.countItemList.length)
      {
        this.selectedAll=true;
      }
    }
    else {
      let el = this.listSelectedItem.indexOf(id);
      // remove item out of array
      this.listSelectedItem.splice(el, 1);
      this.setItemListSelected(id, false);
    }
  }

  /*
   *********************************************************************************************************************
   */

  private filterList(num) {
    this.getCountItemByCycleID(num);
  }

  /*
   *********************************************************************************************************************
   */

  private editSelectedCountItem() {
    if (this.listSelectedItem.length > 1) {
      this.messages = this._Func.Messages('danger',this._Func.msg('CC006'));
    }
    else {
      if(this.listSelectedItem.length) {
        this._router.navigateByUrl('/cycle-count/' + this.cycleID + '/edit/' + this.listSelectedItem[0]);
      }
      else {
        this.messages = this._Func.Messages('danger',this._Func.msg('CC007'));
      }
    }
  }

  /*
   *********************************************************************************************************************
   */

  private setItemListSelected(id, value) {
    this.countItemList.forEach((item) => {
      if(item['cycle_dtl_id'] == id) {
        item['selected'] = value;
      }
    });
  }

  /*
   *********************************************************************************************************************
   */

  private checkAll(event) {
    this.selectedAll = !this.selectedAll;
    // set empty array  ListSelectedItem
    this.listSelectedItem=[];
    // Loop
    this.countItemList.forEach((item) => {
      item['selected'] = this.selectedAll;
      if(this.selectedAll) {
        // if check all = true , pushing to ListSelectedItem
        this.listSelectedItem.push(item['cycle_hdr_id']);
      }
    })
  }

  // Save CC
  private save() {

    try {

      var $cc_id = this.cycleDetail['cycle_hdr_id'];
      var tar_get = this;
      this.showLoadingOverlay = true;
      this._cycleCountServices.save($cc_id).subscribe(
          data => {

            this.messages = this._Func.Messages('success', this._Func.msg('VR109'));
          },
          err => {
            this.parseError(err);
            this.showLoadingOverlay = false;
          },
          () => {

            setTimeout(function(){

              tar_get._router.parent.navigateByUrl('/cycle-count');
            }, 1100);
          }
      );
    } catch (err) {

      this.showLoadingOverlay = false;
    }

  }

  /*
   *********************************************************************************************************************
   */

  private autoCompleteSku($event) {

    try {
      var sku = $event.target.value;

      if (sku == '') {

        return false;
      }

      var cus_id = '';
      if (this.flagHasCus) {

        cus_id = this.countItemList[0]['cus_id'];
      } else {

        cus_id = jQuery("#customer").val();
      }

      if (cus_id == '') {

        return false;
      }

      var params = `/${this.whs_id}?cus_id=${cus_id}&sku=${encodeURIComponent(sku)}&limit=20`

      jQuery("#list-sku").show();
     // jQuery("#list-sku").find("a:first-child").html('<b></b>').find('b').text('Create ' + sku);

      let target = this;
      // do Search
      this._cycleCountServices.getSKUs(params).debounceTime(400).distinctUntilChanged().subscribe(
          data => {

            this.dataListSku = data.data;
          },
          err => {

            jQuery("#list-sku").hide();

          },
          () => {

            jQuery("body").off("click.sku").on("click.sku", function (e) {

              if (e.target.id != 'sku' && e.target.id != 'list-sku') {

                jQuery("#list-sku").hide();

              }

            });
          }
      );

    } catch (err) {

      return false;
    }

  }

  private printPdf() {

    try {

      var tar_get = this;
      var xhrl = new XMLHttpRequest();

      xhrl.open("GET", this.urlAPICC + "/print-pdf/" + this.cycleDetail['cycle_hdr_id'], true);
      xhrl.setRequestHeader("Authorization", 'Bearer ' +this._Func.getToken());
      xhrl.responseType = 'blob';  

      xhrl.onreadystatechange = function () {

        // If we get an HTTP status OK (200), save the file using fileSaver
        if (xhrl.readyState === 4) {
          if (xhrl.status === 200) {
            var blob = new Blob([this.response], {type: 'application/pdf'});
            saveAs.saveAs(blob, 'Cycle_count_' + tar_get.cycleID +'.pdf');
          } else {
            var fr = new FileReader();
            fr.onload = function(e){
                const responseText = JSON.parse(e.target['result']);
                tar_get.messages = tar_get._Func.Messages('danger', responseText.errors.message);
            }
            fr.readAsText(xhrl.response);
          }
        }
      };

      xhrl.send();
    } catch (err) {

      this.messages = this._Func.Messages('danger', this._Func.msg('VR100'));
    }

  }

  // select one item sku list
  private selectedItemSku($event) {

    var index = parseInt($event.target.id.replace("list-sku-", ""));
    var dataDetailSku = this.dataListSku[index];

    this.itemselectedItemSku = this.dataListSku[index];
    this.listNull['sku_required'] = false;
    this.listNull['pack_required'] = false;
    this.listNull['remain_required'] = false;
    jQuery("#sku").val(dataDetailSku['sku']);
    jQuery("#size").val(dataDetailSku['size']);
    jQuery("#color").val(dataDetailSku['color']);
    jQuery("#lot").val(dataDetailSku['lot']);
    jQuery("#pack").val(dataDetailSku['ctn_pack_size']);
    jQuery("#remain").val(dataDetailSku['piece_remain']);
    jQuery("body").off("click.sku");
    jQuery("#list-sku").hide();
    this.getPackSize();

  }
// select one item loca list
  private selectedItemLoca($event, item) {

    this.listNull['act_loc_not_exist'] = false;
    jQuery("#act_loc").val(jQuery($event.target).text());
    jQuery("#list-location").hide();
    jQuery("body").off("click.loca");
    this.selectedActualLocaltionItem = item;
    this.getPalletByLocation(item['loc_id']);

  }

  // Get Customer
  private getCustomersByWH() {
    const params = `/${this.whs_id}?limit=100`;
    this._cycleCountServices.getCustomersByWH(params)
        .subscribe(
            data => {
              this.dataListCustomer = data.data;
            },
            err => {

              this.showMessage('danger', this._Func.parseErrorMessageFromServer(err));
            },
            () => {
            }
        );
  }

  // Save Sku
  private saveSku($event) {

    try {

      this.messages = false;
      var tar_get = this;
      var submit = true;
      var params = {};
      // Validate Customer
      if (!this.flagHasCus) {

        // Validate fields Top
        jQuery('#customer').each(function (i, object) {
          tar_get.filterForm(object);
        });

        params['cus_id'] = jQuery('#customer').val();
      } else {

        params['cus_id'] = this.countItemList[0]['cus_id'];

      }

      params['cycle_hdr_id'] = this.cycleDetail['cycle_hdr_id'];
      params['whs_id'] = this.cycleDetail['whs_id'];
      params['cycle_type'] = this.cycleDetail['cycle_type'];
      params['loc_id'] = this.selectedActualLocaltionItem['loc_id'];
      let selectedActualLocaltionItem = this.selectedActualLocaltionItem;
      const optionalFields = ['plt_rfid', 'size', 'color'];
      // Validate fields Top
      jQuery('#sku, #pack, #size, #color, #lot, #act_qty, #act_loc, #remain, #plt_rfid').each(function (i, object) {
        if (optionalFields.indexOf(object.id) === -1) {
          tar_get.filterForm(object);
        } 
        
        if (object.id === 'plt_rfid' && selectedActualLocaltionItem['is_floor'] === 1){
          tar_get.filterForm(object);
        }
        params[object.id] = object.value.trim();
      });

      jQuery.each(this.listNull, function (key, value) {

        if (value) {
          submit = false;
          return false;
        }
      });

      if (! submit) {
        return false;
      }

      this._cycleCountServices.saveSku(this.cycleID, JSON.stringify(params))
          .subscribe(
              data => {

                this.messages = this._Func.Messages('success', 'SKU was added successfully!');
                jQuery("[data-dismiss=modal]").trigger({ type: "click" });
              },
              err => {

                this.parseError2(err);
              },
              () => {

                jQuery("[data-dismiss=modal]").trigger({ type: "click" });
                tar_get.resetFormSku();

                setTimeout(function(){
                  tar_get.getCountItemByCycleID(1)
                }, 1100);
              }
          );

    }catch (err) {


    }
    return false;

  }

  private parseError2(err) {

    this.messagesAddSKU = {'status': 'danger', 'txt': this._Func.parseErrorMessageFromServer(err)};

  }
  private parseError(err) {

    this.messages = {'status': 'danger', 'txt': this._Func.parseErrorMessageFromServer(err)};
  }

  // Filter form only show choose one
  private filterForm($target, flag = 'required') {

      try {

        switch (flag) {

          case 'required_int':
            this.listNull[$target.id + "_int"] = false;
            this.listNull[$target.id + "_required"] = false;
            if ($target.value == "") {

              this.listNull[$target.id + "_required"] = true;

            }

            if (!this.validateInt($target.value)) {

              this.listNull[$target.id + "_int"] = true;

            }
            break;

          case 'required':

            this.listNull[$target.id + "_required"] = false;

            if ($target.value == "") {

              this.listNull[$target.id + "_required"] = true;

            }
            break;
          case 'required_int_lessP_other0_lessAct':

            this.listNull[$target.id + "_int"] = false;
            this.listNull[$target.id + "_required"] = false;
            this.listNull[$target.id + "_lessP"] = false;
            this.listNull[$target.id + "_other0"] = false;
            this.listNull[$target.id + "_lessAct"] = false;
            this.listNull["act_qty_eachLessPR"] = false;
            this.listNull["pack_greaterPR"] = false;
            if ($target.value == "") {

              this.listNull[$target.id + "_required"] = true;

            } else if ($target.value == "0") {

              this.listNull[$target.id + "_other0"] = true;
            }else if (!this.validateInt($target.value)) {

              this.listNull[$target.id + "_int"] = true;

            } else if ((parseInt($target.value) > parseInt(jQuery("#pack").val())) && jQuery("#pack").val()) {

              this.listNull[$target.id + "_lessP"] = true;
              this.listNull["pack_greaterPR"] = true;
            } else if ((parseInt(jQuery("#act_qty").val()) % parseInt($target.value) != 0) && jQuery("#act_qty").val() && this.cycleDetail['count_by'] != 'CARTON') {

              this.listNull[$target.id + "_lessAct"] = true;
              this.listNull["act_qty_eachLessPR"] = true;
            }
            break;

          case 'required_greaterPR':

            this.listNull[$target.id + "_required"] = false;
            this.listNull[$target.id + "_greaterPR"] = false;
            this.listNull["remain_lessP"] = false;

            if ($target.value == "") {

              this.listNull[$target.id + "_required"] = true;

            } else if (parseInt($target.value) < parseInt(jQuery("#remain").val())) {

              this.listNull[$target.id + "_greaterPR"] = true;
              this.listNull["remain_lessP"] = true;
            }
            break;
          case 'required_int_eachLessPR':

            this.listNull[$target.id + "_int"] = false;
            this.listNull[$target.id + "_required"] = false;
            this.listNull[$target.id + "_eachLessPR"] = false;
            this.listNull["remain_lessAct"] = false;
            if ($target.value == "") {

              this.listNull[$target.id + "_required"] = true;

            }else if (!this.validateInt($target.value)) {

              this.listNull[$target.id + "_int"] = true;

            } else if ((parseInt($target.value) % parseInt(jQuery("#remain").val())) != 0 && jQuery("#remain").val() && this.cycleDetail['count_by'] != 'CARTON') {

              this.listNull[$target.id + "_eachLessPR"] = true;
              this.listNull["remain_lessAct"] = true;
            }

            break;
        }
          
      } catch (err) {

      return false;
    }
  }

  // Validate Int
  private validateInt(number = '') {

    var re = /^([0-9]*)$/;
    var re2 = /^0([0-9]*)$/;
    if (re.test(number)) {

      if (number.length > 1 && re2.test(number)) {

        return false;

      }

      return true;
    }

    return false;
  }

  private autoSearchLocation($event) {

    this.listNull['act_loc_not_exist'] = false;
    if ($event.type == 'keyup') {
      jQuery("#plt_rfid").prop("disabled", false);
      jQuery("#plt_rfid").val("");
    }

    try {

      let target = this;
      var loca = $event.target.value;
      if (loca == '') {

        return false;
      }
      jQuery("#list-location").show();
     // jQuery("#list-location").find("a:first-child").html('<b></b>').find('b').text('Create ' + loca);

      // do Search
      let param = '?loc_code=' + encodeURIComponent(loca) + '&limit=20';


      this._cycleCountServices.getLocationByWH(target.cycleDetail['whs_id'], param).subscribe(
          data => {

            jQuery("#list-location").show();

            // disable loading icon
            this.dataListLoca = data.data;

          },
          err => {

            jQuery("#list-location").hide();
            this.parseError(err);
          },
          () => {

            jQuery("body").off("click.loca").on("click.loca", function (e) {

              if (e.target.id != 'act_loc' && e.target.id != 'list-location') {

                jQuery("#list-location").hide();

              }

            });
          }
      );

    } catch (err) {

      return false;
    }
  }

  /*
   * Disable input text
   * */
  private checkInputNumberQty (evt, flagInt = false) {

    var charCode = (evt.which) ? evt.which : evt.keyCode;

    // Save data
    if (charCode == 13) {
      
      var index = evt.target.id.replace("act-qty-", "");
      var cycle_dtl_id = evt.target.alt;
      var params = {'act_qty': evt.target.value};
      this.update(cycle_dtl_id, index,params);

    }

    if (charCode > 31 && (charCode < 48 || charCode > 57)){

      if(flagInt) {

        evt.preventDefault();
      }
      else {

        if(charCode != 46) {

          evt.preventDefault();
        }
      }
    }
  }

  /*
   * Disable input text
   * */
  private checkInputNumber (evt, flagInt = false) {

    var charCode = (evt.which) ? evt.which : evt.keyCode;

    if (charCode > 31 && (charCode < 48 || charCode > 57)){

      if(flagInt) {

        evt.preventDefault();
      }
      else {

        if(charCode != 46) {

          evt.preventDefault();
        }
      }
    }
  }

  // Auto Add Input Qty
  private autoAddQty($event) {

    if(!this.isUpdate) {
      return false;
    }

    var str_id = $event.target.id;
    var index = str_id.replace("q-td-", "");
    index = str_id.replace("q-span-", "");

    var cycle_dtl_sts = this.countItemList[index]['cycle_dtl_sts'];

    if (!((cycle_dtl_sts == 'RC' || cycle_dtl_sts == 'NW') && this.hasUpdateItem)) {

      return false;
    }

    var value = jQuery("#q-span-" + index).text();

    this.qty_old_value = value;
    jQuery("#q-span-" + index).hide();
    jQuery("#act-qty-" + index).show();
    jQuery("#act-qty-" + index).focus();
    jQuery("#act-qty-" + index).val(value.trim());

    // jQuery("body").off("click.q-span-" + index).on("click.q-span-" + index, function (e) {
    //
    //   if (e.target.id !== "q-span-" + index && e.target.id !== "act-qty-" + index) {
    //
    //     jQuery("#q-span-" + index).show();
    //     jQuery("#act-qty-" + index).hide();
    //     jQuery("#q-span-" + index).text(value);
    //
    //   }
    // });

  }

  private validateQTY($event) {

    // this.listNull2[$event.target.id +'-greater0'] = false;
    this.listNull2[$event.target.id +'-int'] = false;
    if ($event.target.value == '0') {

      // this.listNull2[$event.target.id +'-greater0'] = true;
    }

    var re = /^(0[0-9]+)$/;
    if (re.test($event.target.value)) {

      this.listNull2[$event.target.id +'-int'] = true;
    }
    
  }

  private updateValueQty($event) {

    this.validateQTY($event);

    if ( this.listNull2[$event.target.id +'-int'] || this.listNull2[$event.target.id +'-greater0']) {

      return false;
    }

    var str_id = $event.target.id;
    var index = str_id.replace("act-qty-", "");

    jQuery("#q-span-" + index).show();
    jQuery("#act-qty-" + index).hide();
    jQuery("#q-span-" + index).text($event.target.value);

    // Save data
    var cycle_dtl_id = $event.target.alt;
    var params = {'act_qty': $event.target.value};

    if ($event.target.value != '') {

      this.update(cycle_dtl_id, index,params);
    }

  }

  // Auto Add Input LOC
  private autoAddLoc($event, item) {
    
    if (item['is_new_sku'] == '1') {
      return false;
    }

    if (!this.isUpdate) {

      return false;
    }

    var str_id = $event.target.id;
    var index = str_id.replace("loc-span-", "");
    var value = jQuery("#loc-span-" + index).text();
    var cycle_dtl_sts = this.countItemList[index]['cycle_dtl_sts'];

    if (!((cycle_dtl_sts == 'RC' || cycle_dtl_sts == 'NW') && this.hasUpdateItem)) {

      return false;
    }

    this.loc_old_value = value;
    jQuery("#loc-span-" + index).hide();
    jQuery("#act-loc-" + index).show();
    jQuery("#act-loc-" + index).focus();
    jQuery("#act-loc-" + index).val(value.trim());

    jQuery("body").off("click.loc-span-" + index).on("click.loc-span-" + index, function (e) {

      var re = new RegExp("^location-from");

      if (e.target.id !== "loc-span-" + index && e.target.id !== "act-loc-" + index && !re.test(e.target.id)) {

        jQuery("#loc-span-" + index).show();
        jQuery("#act-loc-" + index).hide();
        jQuery("#loc-span" + index).text(value);

      }
    });

  }

  private update(cycle_dtl_id, index,  params = {}) {

    try {

      this.messages = false;
      params['cycle_hdr_id'] = this.cycleDetail['cycle_hdr_id'];
      params['whs_id'] = this.whs_id;
      var paramPut = JSON.stringify(params);
      this.showLoadingOverlay = true;
      this._cycleCountServices.updateItemOfCycleCount(cycle_dtl_id, paramPut)
          .subscribe(
              data => {

                if (params['act_qty'] !== undefined) {

                  jQuery("#q-span-" + index).show();
                  jQuery("#act-qty-" + index).hide();
                  jQuery("#q-span-" + index).text(params['act_qty']);
                  jQuery("body").off("click.q-span-" + index);

                }

                if (params['act_loc_name'] !== undefined) {

                  jQuery("#ul-location-" + index).hide();
                  jQuery("#loc-span-" + index).show();
                  jQuery("#act-loc-" + index).hide();
                  jQuery("#loc-span-" + index).text(params['act_loc_name']);
                  jQuery("body").off("click.loc-span-" + index);
                  jQuery("body").off("click.alocation" + index);
                }


              },
              err => {

                if (params['act_qty'] !== undefined) {

                  jQuery("#q-span-" + index).show();
                  jQuery("#act-qty-" + index).hide();
                  jQuery("#q-span-" + index).text(this.qty_old_value);

                }

                if (params['act_loc_name'] !== undefined) {

                  jQuery("#ul-location-" + index).hide();
                  jQuery("#loc-span-" + index).show();
                  jQuery("#act-loc-" + index).hide();
                  jQuery("#loc-span-" + index).text(this.loc_old_value);
                }

                this.showMessage('danger', this._Func.parseErrorMessageFromServerStandard(err));
                this.showLoadingOverlay = false;
              },
              () => {

                this.showLoadingOverlay = false;
              }
          );

    } catch (err) {

    }
  }

  /*
   ********************************************************************************************************************
   */

  private autoSearchLocationList($event) {

    try {

      if ($event.type == "keyup" && $event.which == 13) {

        return false;
      }

      var str_id = $event.target.id;
      var index = str_id.replace("act-loc-", "");

      let n = this;
      // do Search
      let param = '?loc_code=' + encodeURIComponent($event.target.value) + '&limit=20';
      this.s_loading_list[index] = true;
      jQuery("#ul-location-" + index).show();

      this._cycleCountServices.getLocationByWH(this.whs_id, param).subscribe(
          data => {

            jQuery("#ul-location-" + index).show();

            // disable loading icon
            this.s_loading_list[index] = false;
            this.dataLocation = data.data;

          },
          err => {

            jQuery("#ul-location-" + index).hide();
            this.s_loading_list[index] = false;
          },
          () => {
            jQuery("body").off("click.alocation" + index).on("click.alocation" + index, function (e) {

              var re = new RegExp("^act-loc-" + index);

              if (!re.test(e.target.id)) {

                jQuery("#ul-location-" + index).hide();

              }

            });
          }
      );

    } catch (err) {

      return false;
    }
  }

  /*
   ********************************************************************************************************************
   */

  private selectedItem($event) {

    var index =  jQuery($event.target).parent("div")[0].id.replace("ul-location-", "");
    jQuery("body").off("click.loc-span-" + index);
    var code = jQuery($event.target).text().trim();
    jQuery("#ul-location-" + index).hide();
    jQuery("#act-loc-" + index).val(code);
    jQuery("#act-loc-" + index).focus();
  }

  /*
   * Disable input text
   * */
  private saveLocation (evt) {

    var charCode = (evt.which) ? evt.which : evt.keyCode;

    // Save data
    if (charCode == 13) {

      this.showLoadingOverlay = true;
      var index = evt.target.id.replace("act-loc-", "");
      var cycle_dtl_id = evt.target.alt;
      var params = {'act_loc_name': evt.target.value};
      this.update(cycle_dtl_id, index,params);

    }

  }

  /**
   *
   * @returns {boolean}
     */

  private checkIsUpdateCountItem() {

    return true;

  }

  private eventForDisableLoad($flag = true) {

    var fields = [
      'sku',
      'remain',
      'act_qty',
      'act_loc',
      'plt_rfid'
    ];

    for (var i = 0; i < fields.length; i++) {

      jQuery("#" + fields[i]).prop("disabled", $flag);

    }

  }

  private enableOtherFields($event) {
    this.listNull = {};
    var flag = $event.target.value.trim() == '';
    this.eventForDisableLoad(flag);
    this.removeValue();
  }

  private removeValue() {

    var fields = [
      'size',
      'color',
      'lot',
      'sku',
      'pack',
      'remain',
      'act_qty',
      'act_loc',
      'plt_rfid'
    ];

    for (var i = 0; i < fields.length; i++) {

      jQuery("#" + fields[i]).val("");

    }

  }

  private getPackSize() {

    try {

      jQuery("#remain").val('');
      var params = this.itemselectedItemSku['item_id']+'?lot='+this.itemselectedItemSku['lot'];
      this.flagPack = true;
      this._cycleCountServices.getPackSize(params).subscribe(
          data => {

            this.dataListPackSize = data.data;
            this.flagPack = false;

            if (this.cycleDetail['count_by'] == 'CARTON') {
              jQuery("#remain").val(this.dataListPackSize['0']['pack_size']);
            }

          },
          err => {
            this.flagPack = true;
          },
          () => {
          }
      );

    } catch (err) {

      return false;
    }
  }

  private resetFormSku () {

    jQuery("#addSKU").modal("hide");

    if (!this.flagHasCus) {
      // Validate fields Top
      jQuery('#customer').each(function (i, object) {
        jQuery(object).val('');
      });

      this.eventForDisableLoad(true);
      this.flagPack = true;

    }
    jQuery('#sku, #pack, #size, #color, #lot, #act_qty, #act_loc, #remain, #plt_rfid').each(function (i, object) {
      jQuery(object).val('');
    });

    this.listNull = {};
  }

  private whs_code = '';
  private getWHScodeById(){

    // this.whs_code
    let whs_id= this._Func.getWareHouseId();
    this._cycleCountServices.getWhs_code_by_id(whs_id).subscribe(
        data => {
          this.whs_code=data['whs_code'];
        },
        err=>{
          this.parseError(err);
        }
    );

  }

  private valid_pl_whs(pallet_code){

    this.listNull['plt_rfid_invalid'] = false;
    //let regx=/^[0-9A-Za-z-_]{3}-(PLB|PLG|PLF|PL)-[0-9]{6}$/;
    if(pallet_code){

      this.listNull['plt_rfid_required'] = false;
      let tmp = 4 - this._Func.getWareHouseId().toString().length;
      let tmp_ck_whs = '';

      for (let i = 0; i < tmp; i ++) {
        tmp_ck_whs += '0';
      }

      tmp_ck_whs = tmp_ck_whs + '' + this._Func.getWareHouseId();
      let regx = /^P-([0-9]{2})(0[1-9]|1[0-2])-([0-9]{6})$/;
    
      if(!regx.test(pallet_code)){
        this.listNull['plt_rfid_invalid'] = true;
      }

    }

  }

  private dataPallet: any;
  private getPalletByLocation($loc_id) {

    jQuery("#plt_rfid").prop("disabled", true);
    jQuery("#plt_rfid").val('');
    let whs_id=this._Func.getWareHouseId();
    this._cycleCountServices.getPalletByLocation(whs_id, $loc_id).subscribe(
        data => {

          jQuery("#plt_rfid").prop("disabled", false);
          if (data['plt_rfid'] != null) {

            jQuery("#plt_rfid").val(data['plt_rfid']['plt_rfid']);
            jQuery("#plt_rfid").prop("disabled", true);
            this.valid_pl_whs(data['plt_rfid']['plt_rfid']);
          }
        },
        err=>{

          jQuery("#plt_rfid").prop("disabled", false);
          this.parseError(err);
        }
    );
  }

  private ckExistLocationSKU($e) {

    this.listNull['act_loc_not_exist'] = false;

    var target = this;
    setTimeout(function(){

      var tmp = $e.target.value.trim();
      var flag = false;
      for (var i = 0; i < target.dataListLoca.length; i++) {

        if (target.dataListLoca[i]['loc_code'] == tmp) {

          flag = true;
          break;
        }
      }
      if (!flag) {

        target.listNull['act_loc_not_exist'] = true;
      }

    }, 200);
  }

  private assignRemain($e) {
    jQuery("#remain").val($e.target.value);
  }

}
