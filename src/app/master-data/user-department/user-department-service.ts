/**
 * Created by admin on 12/28/2016.
 */
import {Component, Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {API_Config, Functions} from '../../common/core/load';

@Injectable()
export class UserDepartmentService {

    public AuthHeader = this._Func.AuthHeader();
    public AuthHeaderPost = this._Func.AuthHeaderPost();

    constructor(private _Func: Functions, private _API: API_Config, private http: Http)
    {

    }

    /*
     * Add new Zone Type
     * */
    Add_New_UserDepartment(data)
    {
        return this.http.post(this._API.API_USER_DEPARTMENT, data, { headers: this._Func.AuthHeaderPostJson() })
            .map((res: Response) => res.json().data);
    }

    /*
     * Update
     * */Update_ZoneType
    Update_UserDepartment(id,data) {
        return this.http.put(this._API.API_USER_DEPARTMENT+'/'+id, data, { headers: this._Func.AuthHeaderPostJson() })
            .map((res: Response) => res.json().data);
    }

    /*
     * Get detail an Zone Type
     * */
    Get_UserDeparmentDetail(id,sortQuery ='') {
        let paramAPI = this._API.API_USER_DEPARTMENT + '/' + id;
        if(sortQuery != ''){
            paramAPI += '?' + sortQuery;
        }
        return this.http.get(paramAPI, { headers: this.AuthHeader })
            .map((res: Response) => res.json().data);
    }

    /*
     * Get zone-type List
     *
     * */
    GetUserDepartmentList(param)
    {
        return this.http.get(this._API.API_USER_DEPARTMENT  + param,{ headers: this.AuthHeader })
            .map((res: Response) => res.json());
    }

    /*
     * Get a detail warehouse
     * */
    GetDetailWareHouse(id)
    {
        return this.http.get(this._API.API_Warehouse+'/'+id,{ headers: this.AuthHeader })
            .map((res: Response) => res.json());
    }
    /*
     * Delete function
     * */
    DeleteUserDepartment(id)
    {
        return this.http.delete(this._API.API_USER_DEPARTMENT + "/" + id, { headers: this.AuthHeader });
    }

    /*
     *
     * Search function
     * */
    Search(param)
    {
        return this.http.get(this._API.API_USER_DEPARTMENT+param,{ headers: this.AuthHeader })
            .map((res: Response) => res.json());
    }


}
