import {Component} from "@angular/core";
import { CORE_DIRECTIVES, FORM_DIRECTIVES } from '@angular/common';
import {StylesheetService} from "../../common/services/stylesheet.service";
import {TimeService} from "../../common/services/time.service";
import {SystemConfigurationServices} from "../system-configuration.service";
import {ErrorComponent} from "../../common/component/error.component";

@Component({
    selector: 'general',
    directives: [CORE_DIRECTIVES, FORM_DIRECTIVES, ErrorComponent],
    providers: [TimeService,SystemConfigurationServices,StylesheetService],
    templateUrl: 'general.component.html',
})
export class GeneralComponent{
  public timeZones: Array<string>     = [];
  public timeFormats: Array<string>   = [];
  public dayOfWeek: Array<string>     = [];

  public timezoneSelect = '(UTC+00:00) London , Europe/London';
  public timeformatSelect = 'yyyy-mm-dd';
  public dayOfWeekSelect = 'Monday';
  public measurement = 'Metric System';
  public modeSelect = false;

  public SuccessMsg:string;
  public ErrMsg: Array<string>     = [];
  constructor(private _timeService:TimeService, private _sysService:SystemConfigurationServices, private _styleService:StylesheetService){
    this.timeZones = this._timeService.getListTimeZones();
    this.timeFormats = this._timeService.getDateFormat2();
    this.dayOfWeek = this._timeService.getDayOfWeek();
    this.SuccessMsg = '';
    this.ErrMsg = [];
    this._styleService.addLoading();
    this._sysService.getDatabyTabName('general').subscribe(
      data => {
        if(data.length > 0){

          for(let i = 0; i < data.length ;i++ ){
            if(data[i].setting_key === "TZ" && data[i].setting_value != null){
              this.timezoneSelect = data[i].setting_value;
            }
            if(data[i].setting_key === "MM" && data[i].setting_value != null){
              this.modeSelect = this.convertNumberToBool(data[i].setting_value);
            }
            if(data[i].setting_key === "SW" && data[i].setting_value != null){
              this.dayOfWeekSelect = this.convertNumberToDay(data[i].setting_value);
            }
            if(data[i].setting_key === "DF" && data[i].setting_value != null){
              this.timeformatSelect = data[i].setting_value;
            }
            if(data[i].setting_key === "MS" && data[i].setting_value != null){
              this.measurement = data[i].setting_value;
            }
          }


        }
        this._styleService.removeLoadding();
      },

      err => {
        let msg = 'loading data failed:' + err.status;
        this.ErrMsg.push(msg);
        this._styleService.removeLoadding();
      },
      () => {}

    )
  }

  saveGeneral(){
    this.SuccessMsg = '';
    var params = {
      TZ:  this.timezoneSelect,
      DF:  this.timeformatSelect,
      SW: this.convertDayToNumber(this.dayOfWeekSelect),
      MM:  this.convertBooleanToNumber(this.modeSelect),
      MS:  this.measurement

    };

    this._styleService.addLoading();
    this._sysService.saveDatabyTabName('general',JSON.stringify(params)).subscribe(
      response => {
        this.SuccessMsg = 'Submit successfully.';
        this._styleService.removeLoadding();
      },
      err => {
        console.error(err);
        this._styleService.removeLoadding();
      },
      () => {}
    )
  }

  convertDayToNumber(day){
    return this._timeService.getNumberDayOfWeek(day);
  }

  convertNumberToDay(numb){
    return this._timeService.getDayOfWeekFromNumber(numb);
  }
  convertBooleanToNumber(bool){
    if(bool == true){
      return '1';
    }
    return '0';
  }

  convertNumberToBool(numb){
    if(numb == '1'){
      return true;
    }
    return false;
  }
}
