import {Component,OnInit} from "@angular/core";
import { Router, RouterLink, ROUTER_DIRECTIVES } from '@angular/router-deprecated';
import { FORM_DIRECTIVES,ControlGroup,FormBuilder, Control,Validators} from '@angular/common';
import {API_Config, UserService, Functions} from '../../common/core/load';
import { Http } from '@angular/http';
import {BoxPopupService} from "../../common/popup/box-popup.service";
import {GateManagement_REPORT_Services} from "./gate-management-report-service";
import {WMSPagination,AgGridExtent,WMSMessages, FileExporter, WMSBreadcrumb} from '../../common/directives/directives';
import 'ag-grid-enterprise/main';
declare var jQuery: any;

@Component({
  selector: 'gate-management-report',
  providers:[GateManagement_REPORT_Services],
  directives: [ROUTER_DIRECTIVES,AgGridExtent,WMSMessages,WMSPagination, FileExporter, WMSBreadcrumb],
  templateUrl: 'gate-management-report.component.html',

})
export class GateManagement_REPORT_Component{

  private headerURL = this.apiService.API_User_Metas+'/gat';
  private columnData = {
    gate_dtl_sts_name: {
      title:'Status',
      width:100,
      pin:true,
      ver_table:'4'
    },
    gate_name: {
      title:'Gate Name',
      width:70,
      pin:true,
      sort:true
    },
    ctnr_num: {
      title:'CTNR #',
      width:100,
      pin:true,
      sort:true
    },
    cus_name: {
      title:'Customer',
      width:170,
      pin:true,
      sort:true
    },
    open:{
      title:'Open',
      width:120,
      pin:true
    },
    close:{
      title:'Close',
      width:120,
      pin:true
    },
    duration: {
      title:'Duration',
      width:170,
      sort:true
    },
    created_at: {
      title:'Created at',
      width:120
    }
  };

  private Pagination;
  private perPage=20;
  private whs_id;
  private messages;
  private Loading={};
  private searching={};
  searchForm:ControlGroup;
  private isSearch:boolean=false;
  private gateManagementList:Array<any>=[];
  private sortData={fieldname:'gate',sort:'desc'};
  private showLoadingOverlay:boolean=false;
  private Customers:Array<any>=[];
  private viewTime=1;
  private cus_id='';
  private searchParams;
  private listContainers = [];
  private listGates = [];

  // Export csv file
  private dataExport: any[];
  private headerExport = ['Status','Gate Name', 'CTNR #', 'Customer', 'Open', 'Close', 'Duration', 'Created at'];
  private fileName:string = 'Gate-Management-Report.csv';

  constructor(
    private gateManagementReportService:GateManagement_REPORT_Services,
    private _Func: Functions,
    private _boxPopupService: BoxPopupService,
    private  apiService:API_Config,
    private _user:UserService,
    private fb: FormBuilder,
    private _http: Http,
    private _router: Router) {

    this.checkPermission();
    this.whs_id = localStorage.getItem('whs_id');
  }

  private viewReport;
  private allowAccess:boolean=false;
  private checkPermission() {
    this.showLoadingOverlay = true;
    this._user.GetPermissionUser().subscribe(
      data => {
        this.viewReport = this._user.RequestPermission(data, 'gateManagement');
        if(!this.viewReport) {
          this.redirectDeny();
        }
        else {
          this.allowAccess=true;
          this.getGateManagementReportList();
          this.getCustomersByWH();
          this.getListCTN();
          this.getListGate();
        }
      },
      err => {
        this.messages=this._Func.Messages('danger', this._Func.parseErrorMessageFromServer(err));
        this.showLoadingOverlay = false;
      }
    );
  }


  private getCustomersByWH () {
    let params='?whs_id=' + this.whs_id + "&limit=10000";
    this.gateManagementReportService.getCustomersByWH(params)
      .subscribe(
        data => {
          this.Customers = data.data;
        },
        err => {
        }
      );

  }

  private getListCTN(){
    this.gateManagementReportService.getListContainer()
      .subscribe(
        data => {
          this.listContainers = data.data;
        },
        err => {
        }
      );
  }

  private getListGate(){
    this.gateManagementReportService.getListGate()
      .subscribe(
        data => {
          this.listGates = data.data;
        },
        err => {
        }
      );
  }

  redirectDeny(){
    this._router.parent.navigateByUrl('/deny');
  }


  private onPageSizeChanged($event)
  {
    this.perPage = $event.target.value;
    if(this.Pagination){
      if((this.Pagination['current_page'] - 1) * this.perPage >= this.Pagination['total']) {
        this.Pagination['current_page'] = 1;
      }
      this.getGateManagementReportList(this.Pagination['current_page']);
    }

  }

  private getGateManagementReportList(page=null){

    if(!page) page = 1;
    let params="?page="+page+"&limit="+this.perPage;
    params += "&sort["+this.sortData['fieldname']+"]="+this.sortData['sort'];
    if(this.searchParams) {
      params += this.searchParams;
    }
    this.showLoadingOverlay=true;
    this.gateManagementReportService.getListGateManagement(params).subscribe(
      data => {
        this.showLoadingOverlay=false;
        this.gateManagementList = this._Func.formatData(data.data)
        this.setDataExport(this.gateManagementList);

        this.Pagination=data['meta']['pagination'];
        this.Pagination['numLinks']=3;
        this.Pagination['tmpPageCount']=this._Func.Pagination(this.Pagination);
      },
      err => {
        this.messages=this._Func.Messages('danger', this._Func.parseErrorMessageFromServer(err));
        this.showLoadingOverlay=false;
      }
    );

  }

  private reset() {
      jQuery("#form-filter input, #form-filter select").each(function( index ) {
          jQuery(this).val("");
      });
      this.searchParams = '';
      this.getGateManagementReportList(1);
  }

  private search() {
    this.searchParams = '';
    let params_arr = jQuery("#form-filter").serializeArray() ;
    for (let i in params_arr) {
        if (params_arr[i].value == "") continue;
        this.searchParams +="&" +  params_arr[i].name.trim() + "=" + encodeURIComponent(params_arr[i].value.trim());
    }
      this.getGateManagementReportList(1);
  }

  private setDataExport(data:Array<any> = []) {
    this.dataExport = [];
    for (let i in data) {
      this.dataExport.push([
        data[i].gate_dtl_sts_name,
        data[i].gate_name,
        data[i].ctnr_num,
        data[i].cus_name,
        data[i].open,
        data[i].close,
        data[i].duration,
        data[i].created_at
      ]);
    }
  }
}
