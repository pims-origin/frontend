import {Component} from "@angular/core";
import { Router, RouterLink, RouteParams,  ROUTER_DIRECTIVES } from '@angular/router-deprecated';
import { CORE_DIRECTIVES, FORM_DIRECTIVES,ControlGroup, FormBuilder, Validators } from '@angular/common';
import {Http, Response, JSONP_PROVIDERS, Jsonp} from '@angular/http';
import {API_Config, Functions} from '../../common/core/load';
import { ValidationService, matchingPasswords } from '../../common/core/validator';
@Component({
    selector: 'reset-password',
    directives: [CORE_DIRECTIVES, FORM_DIRECTIVES],
    providers:[ValidationService],
    templateUrl: 'setup-password.component.html',
    // styleUrls:['assets/css/page/forgot-password.css']

})
export class SetupPasswordComponent{

    public AuthHeader = this._Func.AuthHeaderNoTK();
    public messages;
    public loading:boolean=false;
    private token;
    private newPass = '';
    private repeatNewPass = '';
    private resetPasswordMsgs = {};
    constructor(public router: Router,
                private _Func: Functions,
                private _API: API_Config,
                private _Valid: ValidationService,
                private params: RouteParams,
                private http: Http) {
        this.resetPasswordMsgs['err'] = [];
    }

    GoToLoginPage(){
        this.router.parent.navigateByUrl('/login');
    }


    createPass(): void {
        if(true){
            this.loading = true;

            let param = "new_password=" + encodeURIComponent(this.newPass) + "&repeat_new_password=" +
                encodeURIComponent(this.repeatNewPass) + "&token="+this.params.get('token');

            this.setupPassService(param).subscribe(
                data=>{
                    this.loading=true;
                    this.messages = this._Func.Messages('success', this._Func.msg('VR104'));
                },
                err => {
                    this.loading=false;
                    this.messages=this._Func.Messages('danger',this._Func.parseErrorMessageFromServer(err));
                }
            );
        }
    }

    setupPassService(param)
    {
        return  this.http.post(this._API.API_SETUP_PASSWORD, param,
            { headers: this.AuthHeader}).map((res: Response) => res.json().data);
    }
    /*
     * Redirect to
     * */
    private checkPasswordPolicy(){
        this.messages = false;
        this.resetPasswordMsgs['err'] = this._Valid.validatePasswordByPolicy(this.newPass,this.repeatNewPass);
        if(this.resetPasswordMsgs['err'].length > 0){
            return false;
        }
        return true;
    }
    private removeMsgs(msg){
        let index = this.resetPasswordMsgs['err'].indexOf(msg);
        this.resetPasswordMsgs['err'].splice(index,1);
    }
}
