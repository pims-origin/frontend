import {Component, Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {API_Config, Functions} from '../../../app/common/core/load';

@Injectable()
export class EditCartonService {

    public AuthHeader = this._Func.AuthHeader();
    public AuthHeaderPost = this._Func.AuthHeaderPost();

    constructor(private _Func: Functions, private _API: API_Config, private http: Http) {
    }
    
    getCartonNum(ctn_num) {
        return this.http.get(this._API.API_GOODS_RECEIPT_MASTER + '/list-carton-num?ctn_num=' + ctn_num, {headers: this.AuthHeader})
            .map((res:Response)=> res.json());
    }

    
    getPallets(gr_id) {
        return this.http.get(this._API.API_GOODS_RECEIPT_MASTER + '/pallet-list?gr_id=' + gr_id, {headers: this.AuthHeader})
            .map((res:Response)=> res.json());
    }

    getCartonList(gr_id) {
        return this.http.get(this._API.API_GOODS_RECEIPT_MASTER + '/carton-list?gr_id=' + gr_id, {headers: this.AuthHeader})
            .map((res:Response)=> res.json());
    }
    
    getCartonDetail(ctn_id) {
        return this.http.get(this._API.API_GOODS_RECEIPT_MASTER + '/detail-carton?ctn_id=' + ctn_id, {headers: this.AuthHeader})
            .map((res:Response)=> res.json());
    }
}