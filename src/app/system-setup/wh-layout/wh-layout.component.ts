import {Component} from '@angular/core';
import {Router, RouterLink, RouteConfig, ROUTER_DIRECTIVES} from '@angular/router-deprecated';
import {WarehouseLayout} from "./wh-layout/wh-layout.component";
import {AisleDetail} from "./aisle-detail/aisle-detail.component";

@Component ({
    selector: 'wh-management',
    directives: [ROUTER_DIRECTIVES],
    template: `<router-outlet></router-outlet>`,
})

@RouteConfig([
    { path: '/', component: WarehouseLayout , name: 'Warehouse Layout', useAsDefault: true },
    { path: '/:id', component: AisleDetail , name: 'View Aisle', data:{action:'view'}}

])
export class WarehouseLayoutComponent {


}
