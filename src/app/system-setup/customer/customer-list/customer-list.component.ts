import {Component,OnInit} from '@angular/core';
import {RouteParams,Router ,RouteData} from '@angular/router-deprecated';
import {CustomerServices} from '../customer-service';
import { FORM_DIRECTIVES,ControlGroup,FormBuilder, Control} from '@angular/common';
import {Functions} from '../../../common/core/load';
import { CountryService } from '../../../master-data/country/country-service';
import { Http } from '@angular/http';
import {API_Config} from '../../../common/core/load';
import {WMSPagination,WMSBreadcrumb,AgGridExtent} from '../../../common/directives/directives';
import { OrderBy } from "../../../common/pipes/order-by.pipe";
import {UserService} from "../../../common/users/users.service";
@Component ({
  selector: 'customer-list',
  directives:[FORM_DIRECTIVES,AgGridExtent,WMSPagination,WMSBreadcrumb],
  pipes: [OrderBy],
  providers: [CustomerServices,CountryService,FormBuilder,UserService],
  templateUrl: 'customer-list.component.html',
})

export class CustomerListComponent {
  private sortData={fieldname:'cus_code',sort:'asc'};
  private showLoadingOverlay:boolean=false;
  private CustomerList:Array<any>=[];
  private ListPermission=[];
  private headerURL = this._API.API_User_Metas+'/cus';
  private messages;
  private Pagination;
  private perPage=20;
  public Loading=[];
  private errMessages=[];
  private reloadList:boolean=false;
  private Country=[];
  private State:Array<any>=[];
  public ListSelectedItem:Array<any>=[];
  public selectedAll:boolean=false;
  SearchForm: ControlGroup;
  private isSearch:boolean=false;
  private userPermission:Array<any>=[];



  private ListCustomer=null;

  private columnData = {
    cus_id:{
      attr:'checkbox',
      id:true,
      title:'#',
      width:25,
      pin:true,
      ver_table:1
    },
    cus_code:{
      title:'Customer Code',
      width:120,
      url:'#/system-setup/customer/',
      field_get_id:'cus_id',
      pin:true,
      sort:true
    },
    cus_billing_account: {
      title:'Billing Account Code',
      width:140,
      pin:true,
      sort:true
    },
    cus_name: {
      title:'Customer Name',
      width:140,
      pin:true,
      sort:true
    },
    cus_add_city_name:{
      title: 'City',
      parent :'addresses',
      width:120,
      pin:true
    },
    cus_add_state_name: {
      title: 'State',
      parent :'addresses',
      width:120,
      pin:true
    },
    cus_add_postal_code:{
      title: 'Zipcode',
      parent :'addresses',
      width:60
    },
    cus_ctt_full_name:{
      title: 'Contact',
      width:140
    },
    cus_ctt_phone:{
      title: 'Phone',
      width:120
    },
    cus_ctt_email: {
      title: 'Email',
      width:160
    }
  };

  constructor(
    private _Func: Functions,
    private fb: FormBuilder,
    private _API: API_Config,
    private _user:UserService,
    private _CTService: CustomerServices,
    private _router:Router)
  {

    // builder form search

    /*
     * Get List customer
     * */
    this.checkPermission();

    this.FormBuilder();
    this.GetWareHouseList();
    this.GetCountry();

  }

  // Check permission for user using this function page
  private createCustomer;
  private viewCustomer;
  private editCustomer;
  private allowAccess:boolean=false;
  private checkPermission() {
    this.showLoadingOverlay = true;
    this._user.GetPermissionUser().subscribe(
      data => {
        this.createCustomer = this._user.RequestPermission(data, 'createCustomer');
        this.viewCustomer = this._user.RequestPermission(data,'viewCustomer');
        this.editCustomer=this._user.RequestPermission(data,'editCustomer');
        /* Check orther permission if View allow */
        if(!this.viewCustomer) {
          this._router.parent.navigateByUrl('/deny');
        }
        else {
          this.allowAccess=true;
          this.getCustomerList();
        }
      },
      err => {
        this.messages=this._Func.Messages('danger', this._Func.parseErrorMessageFromServer(err));
        this.showLoadingOverlay = false;
      }
    );
  }

  FormBuilder()
  {
    this.SearchForm =this.fb.group({
      whs_id: [''],
      cus_code: [''],
      cus_billing_account: [''],
      cus_name: [''],
      cus_country_id: [''],
      cus_state_id: [''],
      cus_city_name: [''],
      cus_postal_code: ['']
    });
  }

  private searchParam;
  Search()
  {
    this.isSearch=true;
    let data=this.SearchForm.value;
    // set status search is true
    // assign to SearchQuery
    // whs_id=&cus_code=&cus_city_name=&cus_name=&cus_billing_account=&cus_country_id=&cus_state_id=&cus_postal_code=
    this.searchParam="&whs_id="+data['whs_id']+"&cus_code="+encodeURIComponent(data['cus_code'])+"&cus_billing_account="
      +encodeURIComponent(data['cus_billing_account'])+"&cus_name="+encodeURIComponent(data['cus_name'])+"&cus_country_id="+data['cus_country_id']
      +"&cus_state_id="+data['cus_state_id']+"&cus_city_name="+encodeURIComponent(data['cus_city_name'])
      +"&cus_postal_code="+encodeURIComponent(data['cus_postal_code']);
    this.getCustomerList(1);
  }

  /*
   * Load state of country when change
   * */
  ChangeCountry(item)
  {
    if(item){
      this.GetState(item);
    }
    else{
      this.StateProvinces=[];
      (<Control>this.SearchForm.controls['cus_state_id']).updateValue('');
    }
  }

  /*
   * List list warehouse to get  whs code
   * */
  private WareHouseList:Array<any>=[];
  private GetWareHouseList(page=null)
  {
    let param="?page="+page+"&limit=500";
    this.Loading['getList']=true;
    this._CTService.GetWareHouseList(param).subscribe(
      data => {
        this.WareHouseList = data.data;
        this.Loading['getList'] = false;
      },
      err => {},
      () => {}
    );
  }

  /*
   * Get Country
   * */
  GetCountry()
  {
    let param="?limit=500";
    this._CTService.getCountry(param).subscribe(
      data=>{
        this.Country=data;
      },
      err=>{
        this.errMessages.push('Could not load country.');
      }
    );
  }

  private StateProvinces=[];
  GetState(idCountry)
  {
    let param="?sort[sys_state_name]=asc&limit=500";
    this.Loading['state']=true;
    this._CTService.State(idCountry,param).subscribe(
      data=>{
        this.Loading['state']=false;
        this.StateProvinces=data;
      },
      err=>{
        this.Loading['state']=false;
        this.errMessages.push('Could not load State.');
      }
    );
  }

  private onPageSizeChanged($event)
  {
    this.perPage = $event.target.value;
    if((this.Pagination['current_page'] - 1) * this.perPage >= this.Pagination['total']) {
      this.Pagination['current_page'] = 1;
    }
    this.getCustomerList(this.Pagination['current_page']);
  }

  private filterList(num)
  {
    this.getCustomerList(num);
  }

  private getCustomerList(page=null)
  {
    let param="?sort["+this.sortData['fieldname']+"]="+this.sortData['sort']+"&page="+page+"&limit="+this.perPage;
    if(this.isSearch){
     param=param+this.searchParam;
      this.Loading['searching']=true;
    }


    this.showLoadingOverlay=true;

    this._CTService.ListCustomers(param).subscribe(
      data => {
        this.Loading['searching']=false;
        this.ListCustomer = this._Func.formatData(data.data);
        this.showLoadingOverlay=false;
        //pagin function
        this.Pagination=data['meta']['pagination'];
        this.Pagination['numLinks']=3;
        this.Pagination['tmpPageCount']=this._Func.Pagination(this.Pagination);
      },
      err => {
        this.showLoadingOverlay=false;
        this.Loading['searching']=false;
      },
      () => {}
    );
  }

  /*
   * Router edit
   * */
  Edit() {
    if (this.ListSelectedItem.length > 1) {
      this.messages = this._Func.Messages('danger','Please choose only one item to edit');
    }
    else {
      if(this.ListSelectedItem.length)
      {
        this._router.navigateByUrl('/system-setup/customer/' + this.ListSelectedItem[0]['cus_id'] + '/edit');
      }
      else{
        this.messages = this._Func.Messages('danger','Please choose one item to edit');
      }
    }
  }

  /*
   * Reset Form Builder
   * */
  ResetSearch()
  {
    this.isSearch=false; // remove action search
    this.State=[];
    this._Func.ResetForm(this.SearchForm);
    this.getCustomerList(this.Pagination['current_page']);
  }


}
