import {JwtHelper} from "angular2-jwt/angular2-jwt";
import {BoxPopup} from "./box-popup";
declare var swal: any;
export class WarningBoxPopup extends BoxPopup<string>{
    type:string = 'warning';
    // title = 'Are you sure?';
    text:string = 'Are you sure you want to cancel?';
    confirmButtonText:string = 'Yes';
    cancelButtonText:string = 'No';
    confirmButtonClass = "btn btn-primary";
    cancelButtonClass = "btn btn-default"
    width = '280';
    showCancelButton:boolean = true;
    // showConfirmButton:boolean = true;

    constructor(options: {} = {}) {
        super(options);
        this.text = options['text'] || this.text;
        this.confirmButtonText = options['confirmButtonText'] || this.confirmButtonText;
        this.cancelButtonText = options['cancelButtonText'] || this.cancelButtonText;
    }

     yesAction () {
    }

    noAction (dismiss) {
    }
}