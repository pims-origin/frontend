import {Directive, ElementRef, HostListener, SkipSelf, Host, Optional, OnInit} from '@angular/core';
import {AbstractControl, NgControlGroup, ControlContainer, ControlGroup, Control} from '@angular/common';
@Directive({
    selector: '[wms-datepicker]'
})
export class WMSDatepicker implements OnInit {

    formControl:AbstractControl;

    constructor(private el:ElementRef,
                @Optional() @Host() @SkipSelf() private parentForm:ControlContainer) {
    }

    ngOnInit():void {
        let ngControlName = this.el.nativeElement.getAttribute('ngControl');
        if (this.parentForm && ngControlName) {
            if (this.parentForm['form']) {
                this.formControl = (<ControlGroup>this.parentForm['form']).find(ngControlName);
            } else if (this.parentForm instanceof NgControlGroup) {
                this.formControl = (<NgControlGroup>this.parentForm).control.controls[ngControlName];
            }
        }

    }

    // THIS WILL BE FIRED IF SOMEONE CHANGES THE INPUT
    @HostListener('focus', ['$event'])
    inputChanged(event) {
        if (event.target.value) {
            (<Control>this.formControl).updateValue(event.target.value);
        }
    }
}
