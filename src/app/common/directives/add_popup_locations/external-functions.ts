import { Injectable } from '@angular/core';
@Injectable()
export class AddPopExFunction{

    constructor() { }

    public addLocation(currentList:Array<any>=[],newArray:Array<any>=[]){

        if(currentList.length==0){
            currentList = newArray;
        }else{
            currentList.push(...this.filteredDuplicatedItem(currentList,newArray));
        }

        return currentList;

    }

    public filteredDuplicatedItem(ArrayDes:Array<any>=[],array_push:Array<any>=[]){

        let arr_temp:Array<any> = [];
        for (let new_item of array_push){
            let is_existed = 0;
            for (let old_item of ArrayDes){
                if(new_item['loc_id'] == old_item['loc_id'] ){
                    is_existed++;
                }
            }
            if(is_existed==0){
                arr_temp.push(new_item);
            }
        }
        return arr_temp;

    }


}