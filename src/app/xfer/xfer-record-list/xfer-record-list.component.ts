import {Component,Input} from '@angular/core';
import {XFERListService} from "./xfer-list.service";
import {Functions} from "../../common/core/functions";
import {AgGridExtent} from "../../common/directives/ag-grid-extent/ag-grid-extent";
import {WMSPagination} from "../../common/directives/wms-pagination/wms-pagination";
import {API_Config} from "../../common/core/API_Config";
import {WorkOrderServices} from "../../outbound-process/work-order/work-order-service";
import { FORM_DIRECTIVES,ControlGroup,FormBuilder, Control,Validators} from '@angular/common';
import {WMSMessages} from "../../common/directives/messages/messages";
import { Router } from '@angular/router-deprecated';
import {WMSBreadcrumb} from "../../common/directives/wms-breadcrumb/wms-breadcrumb";
// import {UserServices} from "../../common/services/user.services";
import {UserService} from "../../common/users/users.service";
declare var jQuery: any;
declare var saveAs: any;
@Component ({
    selector: 'xfer-list',
    templateUrl: 'xfer-record-list.component.html',
    providers: [XFERListService, WorkOrderServices, UserService],
    directives: [AgGridExtent, WMSPagination, WMSMessages, WMSBreadcrumb]
})

export class XFERListComponent {

    private headerURL = this.apiService.API_User_Metas+'/xfe';
    private xferList = [];
    private messages;
    private columnData = {
        xfer_hdr_id:{
            attr:'checkbox',
            id:true,
            title:'#',
            width:25,
            pin:true,
            ver_table:'1k97sd'
        },
        xfer_sts_name: {
            title:'Status',
            width:100,
            pin:true,
            sort:true,
            cellStyle: function(params) {
                if (params.value=='Completed') {
                    //mark police cells as red
                    return {color: '#fff', backgroundColor: 'green'};
                }if (params.value=='Pending') {
                    //mark police cells as red
                    return {color: '#fff', backgroundColor: 'orange'};
                }if (params.value=='Picking') {
                    //mark police cells as red
                    return {color: '#fff', backgroundColor: 'blue'};
                } else {
                    return null;
                }
            }
        },
        xfer_hdr_num: {
            title:'XFER Number',
            width:150,
            url:'#/xfer/',
            field_get_id:'xfer_hdr_id',
            pin:true,
            sort:true
        },
        cus_name:{
            title:'Customer',
            width:120,
            pin:true
        },
        item_id: {
            title:'Item ID',
            width:80,
            pin:true,
            sort:true
        },
        cus_upc: {
            title:'UPC',
            width:80,
            pin:true,
            sort: true
        },
        sku: {
            title:'SKU',
            width:130,
            pin:true,
            sort:true
        },
        size: {
            title:'Size',
            width:70,
            pin:true,
        },
        color: {
            title:'Color',
            width:70,
            pin:true,
        },
        pieces: {
            title:'Requested QTY',
            width:90,
            pin:true,
        },
        nw_loc:{
            title:'To Location',
            width:120,
            pin:true
        }
    };
    private whs_id;
    private ListSelectedItem:Array<any>=[];
    private Customers:Array<any>=[];
    searchForm:ControlGroup;
    private sortData={fieldname:'created_at',sort:'desc'};
    private perPage = 20;
    private isSearch = false;
    private showLoadingOverlay:boolean;
    private Pagination;
    constructor(
        private wodService:WorkOrderServices,
        private xferListService: XFERListService,
        private _Func: Functions,
        private  apiService:API_Config,
        private fb: FormBuilder,
        private _router: Router,
        private _user:UserService
    ){
        this.whs_id = localStorage.getItem('whs_id');
        this.showLoadingOverlay = true;
        this.checkPermission();
        // this.getXferList(1);
        // this.getCustomersByWH();
        this.buildSearchForm();
    }

    private viewXFer = false;
    private createXFer = false;
    private editXFer = false;
    private createXFerTicket = false;
    private updateXferInv = false;
    private checkPermission() {
        this._user.GetPermissionUser().subscribe(
           permissions => {
               this.viewXFer = this._user.RequestPermission(permissions, 'viewXFER');
               this.createXFer = this._user.RequestPermission(permissions, 'createXFER');
               this.editXFer = this._user.RequestPermission(permissions, 'editXFER');
               this.updateXferInv = this._user.RequestPermission(permissions, 'updateXferInv');
               this.createXFerTicket = this._user.RequestPermission(permissions, 'createXferTicket');

               // console.log(this.viewXFer, this.createXFer, this.updateXferInv, this.createXFerTicket);

               if(this.viewXFer) {
                   this.getXferList(1);
                   this.getCustomersByWH();
               } else {
                   this.redirectDeny();
               }
           },
            err => {
                this.showMessage('danger', this._Func.parseErrorMessageFromServer(err));
                // this.messages=this._Func.Messages('danger', this._Func.parseErrorMessageFromServer(err));
                this.showLoadingOverlay = false;
            }


        );
    }

    private showMessage(msgStatus, msg) {
        this.messages = {
            'status': msgStatus,
            'txt': msg
        }
        jQuery(window).scrollTop(0);
    }

    redirectDeny(){
        this._router.parent.navigateByUrl('/deny');
    }

    // Show error when server die or else
    private parseError (err) {
        err = err.json();
        this.messages = this._Func.Messages('danger', err.errors.message);
    }

    // get list custommer

    private getCustomersByWH () {

        let params='?whs_id=' + this.whs_id + "&limit=10000";
        this.wodService.getCustomersByWH(params)
            .subscribe(
                data => {
                    this.Customers = data.data;
                },
                err => {
                    // this.parseError(err);
                    this.showMessage('danger', this._Func.parseErrorMessageFromServer(err));
                },
                () => {
                }
            );

    }

    buildSearchForm()
    {
        this.searchForm = this.fb.group({
            cus_id: [''],
            xfer_sts: [''],
            sku: [''],
            item_id: [''],
            cus_upc: [''],
            xfer_hdr_num: [''],
        });
    }

    /*
     * Reset Form Builder
     * */
    ResetSearch()
    {
        this.showLoadingOverlay = true;
        this.isSearch=false; // remove action search
        this._Func.ResetForm(this.searchForm);
        // this.getWorkOrderList(this.Pagination['current_page']);
        this.getXferList(1);
    }

    private searchParam;
    Search()
    {
        this.showLoadingOverlay = true;
        this.isSearch = true;
        let data = this.searchForm.value;
        // set status search is true
        // assign to SearchQuery
        // whs_id=&cus_code=&cus_city_name=&cus_name=&cus_billing_account=&cus_country_id=&cus_state_id=&cus_postal_code=
        this.searchParam="&cus_id="+data['cus_id']+"&xfer_sts="+encodeURIComponent(data['xfer_sts'])+"&sku="
            +encodeURIComponent(data['sku'])+"&item_id="+data['item_id']+"&cus_upc="+data['cus_upc']+"&xfer_hdr_num="+data['xfer_hdr_num'];

        this.getXferList(1);
    }

    getXferList(page) {
        this.showLoadingOverlay = true;
        // let param = "?page=" + page + "&limit=" + this.perPage;
        let param = '';
        if(this.sortData['fieldname'] == 'xfer_sts_name') {
            param="?sort[xfer_sts]="+this.sortData['sort']+"&page="+page+"&limit="+this.perPage;
        } else {
            param="?sort["+this.sortData['fieldname']+"]="+this.sortData['sort']+"&page="+page+"&limit="+this.perPage;
        }
        if(this.isSearch){
            param = param + this.searchParam;
        }
        this.xferListService.getXferList(param).subscribe(
            data => {
                this.xferList = this._Func.formatData(data.data);
                this.Pagination = data['meta']['pagination'];
                this.Pagination['numLinks']=3;
                this.Pagination['tmpPageCount']=this._Func.Pagination(this.Pagination);
                this.showLoadingOverlay = false;
            },
            err => {
                // this.parseError(err);
                this.showMessage('danger', this._Func.parseErrorMessageFromServer(err));
                this.showLoadingOverlay = false;
            }
        )
    }

    creatMultiXferTicket () {

    }
    createXferTicket() {
        // let id = this.ListSelectedItem[0]['wv_tf_id'];
        if(this.ListSelectedItem.length == 0)
        {
            this.messages = this._Func.Messages('danger',this._Func.msg('XF008'));
            this.scolltoTop();
        }  else {
            let that = this;

            let fileName = 'XFer_Ticket_PDf'
            let ids = [];
            let flagErr = false;
            this.ListSelectedItem.forEach(function (item, index) {
                if(!that.checkOnSelecting(index)) {
                    let id = that.ListSelectedItem[index]['xfer_hdr_id'];
                    // let fileName = jQuery(that.ListSelectedItem[index]['wv_tf_num']).text();
                    ids.push(id);
                } else {
                    flagErr = true;
                }

            });
            if(flagErr === false)
                that.xferListService.printXFerTicket(ids, fileName, that);
        }
    }

    scolltoTop(){
        jQuery('html, body').animate({
            scrollTop: 0
        }, 700);
    }

    checkOnSelecting(index):boolean {
        let Errflag = false;
         if (this.ListSelectedItem[index]['xfer_sts_name'] != 'Pending') {
            this.messages = this._Func.Messages('danger', this._Func.msg('BM056'));
            this.scolltoTop();
            Errflag = true;
        }
        return Errflag ;
    }

    /*
     * Router edit
     * */
    Edit() {
        // if (this.ListSelectedItem.length > 1) {
        //     this.messages = this._Func.Messages('danger',this._Func.msg('XF003'));
        //     this.scolltoTop();
        // }
        if(this.ListSelectedItem.length == 0)
        {
            this.messages = this._Func.Messages('danger',this._Func.msg('XF010'));
            this.scolltoTop();
            // this._router.navigateByUrl('/xfer/' + this.ListSelectedItem[0]['wv_tf_id'] + '/update-inv');
        }
        else if (this.ListSelectedItem[0]['xfer_sts_name'] != 'Pending') {
            this.messages = this._Func.Messages('danger', this._Func.msg('XF007'));
            this.scolltoTop();
        }
        else {
            this._router.navigateByUrl('/xfer/' + this.ListSelectedItem[0]['xfer_hdr_id'] + '/edit');
        }

    }

    EditXfer() {
        if (this.ListSelectedItem.length > 1) {
            this.messages = this._Func.Messages('danger',this._Func.msg('XF003'));
            this.scolltoTop();
        }
        else if(this.ListSelectedItem.length == 0)
        {
            this.messages = this._Func.Messages('danger',this._Func.msg('XF003'));
            this.scolltoTop();
            // this._router.navigateByUrl('/xfer/' + this.ListSelectedItem[0]['wv_tf_id'] + '/update-inv');
        }
        else {
            this._router.navigateByUrl('/xfer/' + this.ListSelectedItem[0]['xfer_hdr_id'] + '/edit');
        }

    }

    private onPageSizeChanged($event) {
        let currentPage = this.Pagination['current_page'];
        this.perPage = $event.target.value;
        if((this.Pagination['current_page'] - 1) * this.perPage >= this.Pagination['total']) {
            currentPage = 1;
        }
        // else {
        //     this.currentPage = this.Pagination['current_page'];
        // }
        this.getXferList(currentPage);
    }
}
