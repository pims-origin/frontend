
import {Component, Input, Injectable} from '@angular/core';
import {TableFunctions} from '../../../core/table-functions';
import {
    AdvanceTable_US,
    PageSize,
    TablePagination,
    TableExport
} from '../../directives';

@Component ({
    selector: 'table-block',
    directives:[
        AdvanceTable_US,
        PageSize,
        TablePagination,
        TableExport
    ],
    providers: [
        TableFunctions
    ],
    templateUrl: 'table-block.html',
})

@Injectable()

export class TableBlock
{
    @Input() tableID;
    @Input() headerURL = '';
    @Input() headerDef = [];
    @Input() pinCols = 0;
    @Input() rowData: any [];
    @Input() Pagination;
    @Input() perPage = 20;
    @Input() parentComponent;
    @Input() dataExport: any [];
    @Input() downloadName: string = 'exportTableData';

    public objSort;

    constructor(
        private _tableFunc: TableFunctions
    )
    {

    }

    /*
    ****************************************************************************
    */

    public doSort(objSort)
    {
        this.parentComponent.objSort = objSort;

        this._tableFunc.callGetTableData({
            parentComponent: this.parentComponent,
            page: this.parentComponent.Pagination.current_page
        });
    }

    //**************************************************************************

    public onCellClicked($event)
    {
        if (typeof this.parentComponent.onCellClicked === 'function') {
            this.parentComponent.onCellClicked($event);
        }
    }

    //**************************************************************************

    public afterGetSelectedRow($event)
    {
        if (typeof this.parentComponent.afterGetSelectedRow === 'function') {
            this.parentComponent.afterGetSelectedRow($event);
        }
    }

    //**************************************************************************

    public onCellValueChanged($event)
    {
        if (typeof this.parentComponent.onCellValueChanged === 'function') {
            this.parentComponent.onCellValueChanged($event);
        }
    }

    //**************************************************************************

}
