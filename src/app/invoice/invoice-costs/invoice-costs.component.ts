
import {Component, ViewChild} from '@angular/core';
import {RouteParams} from '@angular/router-deprecated';
import {API_Config} from '../../common/core/API_Config';
import {BaseTable} from '../../common/core/classes_us/base-table';
import {TableFunctions} from '../../common/core/table-functions';
import {DateFunctions} from '../../common/core/functions/date-functions';
import {BaseService} from '../../common/core/classes_us/base-service';
import {CustomersService} from '../../master-data/customers/customers-service';
import {ChargeTypeService} from '../../master-data/charge-type/charge-type-service';
import {ChargeCodeService} from '../../master-data/charge-code/charge-code-service';
import {UOMServices} from '../../master-data/uom/uom-service';
import {TableStructure} from '../../common/directives/directives';
import {AddCosts} from './add-costs.component';
import {Alert} from '../../common/directives/popup-modal_us/alert/alert';
import {Confirm} from '../../common/directives/popup-modal_us/confirm/confirm';

declare var jQuery: any;

@Component ({
    selector: 'invoice-costs',
    directives: [
        TableStructure,
        AddCosts,
        Confirm,
        Alert
    ],
    providers: [
        TableFunctions,
        DateFunctions,
        BaseService,
        CustomersService,
        ChargeCodeService,
        ChargeTypeService,
        UOMServices
    ],
    templateUrl: '../../common/directives/table-structure/table.component.html'
})

export class InvoiceCostsComponent extends BaseTable
{
    @ViewChild(AddCosts) addCosts: AddCosts;
    @ViewChild(Confirm) confirm: Confirm;
    @ViewChild(Alert) alert: Alert;

    public tableID = 'customer-rates';
    public url = this._api.API_Invoice_Costs + '/get';
    public headerUrlSuffix = 'rep';
    public title = 'Customer Rates';
    public permissionPage = 'viewInvoice';

    public headerDef = [
        {
            id: 'ver_table',
            value: 1 + Math.random() * 10000
        },
        {
            id: 'cus_name',
            name: 'Customer',
            width: 150,
            unsortable: true
        },
        {
            id: 'chg_type_name',
            name: 'Charge Type',
            width: 150,
            unsortable: true
        },
        {
            id: 'chg_code_name',
            name: 'Charge Code Name',
            width: 150,
            unsortable: true
        },
        {
            id: 'chg_code_des',
            name: 'Charge Code Description',
            width: 180,
            unsortable: true
        },
        {
            id: 'sys_uom_name',
            name: 'UOM',
            width: 150,
            unsortable: true
        },
        {
            id: 'chg_cd_cur',
            name: 'Currency',
            width: 80
        },
        {
            id: 'chg_cd_price',
            name: 'Price',
            width: 60,
            editable: true
        },
        {
            id: 'update',
            name: 'Update',
            width: 100,
            template: `
                <button class="btn btn-outline blue update costs-button">
                    <i class="icon-note"></i>Update
                </button>
            `,
            skipExport: true
        },
        {
            id: 'remove',
            name: 'Remove',
            width: 100,
            template: `
                <button class="btn btn-outline red remove costs-button">
                    <i class="icon-trash"></i>Remove
                </button>
            `,
            skipExport: true
        },
        {
            id: 'cus_id',
            name: '',
            hide: true
        },
        {
            id: 'chg_code_id',
            name: '',
            hide: true
        }
    ];

    public searches = [
        {
            caption: 'Charge Type',
            name: 'charge_type',
            field: 'chg_type_id',
            dropdown: {
                source: [],
                idField: 'chg_type_id',
                nameField: 'chg_type_name',
                service: '_chargeTypeService'
            }
        },
        {
            caption: 'Charge Code Name',
            name: 'charge_code_name',
            field: 'chg_code_id',
            dropdown: {
                source: [],
                idField: 'chg_code_id',
                nameField: 'chg_code_name',
                service: '_chargeCodeService'
            }
        },
        {
            caption: 'System UOM',
            name: 'system_uom',
            field: 'sys_uom_id',
            dropdown: {
                source: [],
                idField: 'sys_uom_id',
                nameField: 'sys_uom_name',
                service: '_uomService'
            }
        }
    ];

    public actions = [
        {
            id: 'add',
            title: 'Add',
            icon: 'icon-plus',
            action: 'add'
        }
    ];

    //**************************************************************************

    constructor (
        private _routeParams: RouteParams,
        private _api: API_Config,
        public _tableFunc: TableFunctions,
        public _tableService: BaseService,
        public _dateFunctions: DateFunctions,
        public _customersService: CustomersService,
        public _chargeCodeService: ChargeCodeService,
        public _chargeTypeService: ChargeTypeService,
        public _uomService: UOMServices
    )
    {
        super();

        this.controlSearch += '&cus_id=' + this._routeParams.get('cus_id');

        this._tableFunc.permissionCall(this);
    }

    //**************************************************************************

    public createRowData(data: any)
    {
        for (let key in data) {
            data[key].chg_cd_price = data[key].chg_cd_price.toFixed(2);
        }

        return data;
    }

    //**************************************************************************

    public add()
    {
        this.addCosts.showInfo(this);
    }

    //**************************************************************************

    public onCellClicked($event)
    {
        let action = $event.colDef.field;

        if (! ~jQuery.inArray(action, ['update', 'remove'])) {
            return false;
        }

        let $table = jQuery('#customer-rates'),
            index = $event.rowIndex;

        let rowData = this.rowData[index],
            $cell = jQuery('[colid="chg_cd_price"]', $table).eq(index + 1);

        let cost = action == 'update' ? Math.round($cell.text() * 100) / 100 : 0;

        if (action == 'update') {
            if (this.originalData[index]['chg_cd_price'] == cost) {

                this.alert.show('The price is already set to '
                              + rowData['chg_cd_cur'] + ' ' + cost + '.<br>'
                              + 'Enter a different value for the price');

                return;
            }

            if (cost <= 0) {

                this.alert.show('The price entered must be a positive number');

                return;
            }
        } else {
            if (! this.originalData[index]['chg_cd_price']) {

                this.alert.show('The price is already removed');

                return;
            }
        }

        let confirmData = {
            action: action,
            cell: $cell,
            cost: cost,
            index: index,
            params: JSON.stringify({
                cus_id: rowData['cus_id'],
                whs_id: this.whs_id,
                chg_code_id: rowData['chg_code_id'],
                chg_cd_cur: rowData['chg_cd_cur'],
                chg_cd_price: cost
            })
        }

        let actionMessage = action == 'remove' ? 'removed' :
                'updated to ' + rowData['chg_cd_cur'] + ' ' + cost;

        let message = 'Price will be ' + actionMessage + '.<br>Proceed?';

        this.confirm.show(message, confirmData);
    }

    //**************************************************************************

    public onConfirm($event)
    {
        let confirmData = $event.data,
            index = confirmData.index;

        if ($event.response) {

            let url = this._api.API_Invoice_Costs + '/update';

            this._tableService.post(url, confirmData.params)
                .subscribe(
                    data => {

                        let value = confirmData.action == 'remove' ?
                                '0.00' : confirmData.cost.toFixed(2);

                        this.originalData[index]['chg_cd_price'] = value;

                        confirmData.cell.text(value);

                        this.alert.show('Price was successfully '
                                      + confirmData.action + 'd');
                    },
                    err => {
                        this.messages = this._tableFunc.parseError(err);
                    },
                    () => {}
            );
        } else {

            let value = this.originalData[index]['chg_cd_price'];

            confirmData.cell.text(value);

            this.rowData[index]['chg_cd_price'] = value;
        }

        confirmData.action == 'remove' ?
            setTimeout(() => {
                this._tableFunc.getTableData({
                        tableComponent: this
                });
           }, 500) : null;
    }

    //**************************************************************************

}
