import {Component, Injector} from '@angular/core';
import {RouteConfig, Router, RouterLink, RouteParams, RouteData, ROUTER_DIRECTIVES} from '@angular/router-deprecated';
import { Http, Headers } from '@angular/http';
import {JwtHelper} from 'angular2-jwt/angular2-jwt';
import {API_Config, Functions} from '../../common/core/load';
import 'rxjs/Rx';
@Component ({

})
export class AuthorizationService {
    jwt: string = '';
    decodedJwt: any = '';
    public userRole: any = [];
    active: string = '';

    private AuthHeader = this._Func.AuthHeader();
    api_user_role = this._API.API_User_Roles;

    constructor(
      public router: Router,
      public http: Http,
      private _Func: Functions,
      private _API: API_Config
    ) {
        var jwtHelper = new JwtHelper();
        if(localStorage.getItem('jwt')) {
          this.jwt = localStorage.getItem('jwt');
          this.decodedJwt = jwtHelper.decodeToken(this.jwt);
        }

    }
    roleData() {
      // this.http.get(this.api_user_role + this.decodedJwt.jti + '/roles', {
      this.http.get(this.api_user_role, {
          headers: this.AuthHeader
        })
        .map(res => res.json().data)
        .subscribe(
          data => this.isAuthorized(data),
          err => {},
          () => {}
        )
    }
    isAuthorized(data) {

      for(var d of data)
      {
        this.userRole = d.name;
        // console.log(this.userRole);
        if (this.userRole == 'Admin') {
          this.active = '111456789';
          break;
        }
        else{
          this.active = '000123456';
        }
      }
      localStorage.setItem('access', this.active);
    }
}
