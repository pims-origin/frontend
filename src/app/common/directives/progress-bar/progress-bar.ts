/*==========================================================
 * @Author : Tien.Nguyen
 * ASN CELL
 * Version : 1.0
 * ==========================================================*/
import { Component,Directive,Output, ElementRef, EventEmitter , HostListener, Input } from '@angular/core';
@Component ({
    selector: 'wms2-progressBar',
    directives:[],
    providers: [],
    templateUrl: 'progressBar.html'
})
export class ProGressBarDirective{
    private _progressValue=0;
    private _progressText='';
    @Input('progressText') set progressText(value) {
        this._progressText=value;
    }
    @Input('progressValue') set progress(value) {
        this._progressValue=value;
    }
    constructor(){}
}