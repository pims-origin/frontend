import {Component, Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {API_Config, Functions} from '../../common/core/load';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class CommodityServices {

    public AuthHeader = this._Func.AuthHeader();
    public AuthHeaderPost = this._Func.AuthHeaderPost();

    constructor(private _Func: Functions,
                private _API: API_Config,
                private http: Http) {

    }

    createCommodity(data){
        return this.http.post(this._API.API_Commodity, data, { headers: this._Func.AuthHeaderPostJson() })
            .map((res: Response) => res.json().data);
    }

    updateCommodity(data,id){
        return this.http.put(this._API.API_Commodity+'/'+id, data, { headers: this._Func.AuthHeaderPostJson() })
            .map((res: Response) => res.json().data);
    }

    getCommoditylist($param){
        return  this.http.get(this._API.API_Commodity + '/commoditylist'+$param, {headers: this.AuthHeader})
            .map(res => res.json());
    }

    getCommodityDetail(cmd_id){
        return  this.http.get(this._API.API_Commodity+'/'+cmd_id, {headers: this.AuthHeader})
            .map(res => res.json().data);
    }


}
