import {Component} from '@angular/core';
import {Router, RouterLink, RouteConfig, ROUTER_DIRECTIVES} from '@angular/router-deprecated';
import {WavePickAssignmentComponent} from "./wave-pick/wave-pick-assignment.component";



@Component ({
    selector: 'picking-management',
    directives: [ROUTER_DIRECTIVES],
    template: `<router-outlet></router-outlet>`,
})

@RouteConfig([
    { path: '/', component: WavePickAssignmentComponent , name: 'Picking List', useAsDefault: true },

])
export class WavepickAssignmentManagementComponent {

}
