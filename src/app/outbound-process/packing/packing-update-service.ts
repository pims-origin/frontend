import {Component, Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {Functions, API_Config} from '../../common/core/load';

@Injectable()
export class PackingUpdateServices {

    public AuthHeader = this._Func.AuthHeader();
    public AuthHeaderPost = this._Func.AuthHeaderPost();

    constructor(
        private _Func: Functions,
        private _http: Http,
        private _api: API_Config) {
    }

    getOrderPackingDetail(order_id) {
        return this._http.get(this._api.API_Pack + '/packs/' + order_id,{ headers: this.AuthHeader })
            .map((res: Response) => res.json());
    }

    getPackDimensions() {
        return this._http.get(this._api.API_Pack + '/pack-ref',{ headers: this.AuthHeader })
            .map((res: Response) => res.json());
    }

    getPackTypes() {
        return this._http.get(this._api.API_COMMON + '/packtype/dropdown',{ headers: this.AuthHeader })
            .map((res: Response) => res.json());
    }

    getNewCarton(order_id, item_id) {
        return this._http.get(this._api.API_Pack_v2 + '/packs/' + order_id + '/details?odr_dtl_id=' + item_id,{ headers: this.AuthHeader })
            .map((res: Response) => res.json());
    }

    getCartonDetail(order_id, carton_id) {
        return this._http.get(this._api.API_Pack + '/packs/' + order_id + '/details?pack_hdr_id=' + carton_id,{ headers: this.AuthHeader })
            .map((res: Response) => res.json());
    }

    createCarton(order_id, stringData) {
        return this._http.post(this._api.API_Pack_v2 + '/packs/' + order_id, stringData, { headers: this._Func.AuthHeaderPostJson() })
            .map((res: Response) => res.json());
    }

    editCarton(order_id, carton_id, stringData) {
        return this._http.put(this._api.API_Pack_v2 + '/packs/' + order_id + '/details/' + carton_id, stringData, { headers: this._Func.AuthHeaderPostJson() })
            .map((res: Response) => res.json());
    }

    addCartonDimension(data) {
        return this._http.post(this._api.API_COMMON + '/packedcartondimension', data, { headers: this._Func.AuthHeaderPostJson() })
            .map((res: Response) => res.json().data);
    }

    autoPack(orderID, data) {
        return this._http.post(this._api.API_Pack + '/packs/auto/' + orderID, data, { headers: this._Func.AuthHeaderPostJson() })
            .map((res: Response) => res.json());
    }

  autoPackXDock(orderID, data) {
    return this._http.post(this._api.API_Pack + '/packs/validate-auto/' + orderID, data, { headers: this._Func.AuthHeaderPostJson() })
      .map((res: Response) => res.json());
  }

  getUOM() {
    return this._http.get(this._api.API_UOM + '?limit=1000&sort[sys_uom_name]=asc&sys_uom_type=item', {
        headers: this.AuthHeader
      }).map(res => res.json());
  }

  checkExistSKU(data) {
    return this._http.post(this._api.API_Pack_v2 + '/packs/get-sku', data, { headers: this._Func.AuthHeaderPostJson() })
      .map((res: Response) => res.json().data);
  }

  getCarrierList($param: any) {
    return this._http.get(this._api.MASTERDATA + '/search' + $param, { headers: this.AuthHeader })
      .map(res => res.json());
  }
}
