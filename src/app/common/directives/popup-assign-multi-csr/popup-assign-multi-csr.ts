import { Component, Input, Output, EventEmitter, Injectable } from '@angular/core';
import {
    WMSPagination, PaginationControlsCmp, PaginationService, PaginatePipe
} from '../../../common/directives/directives';
import { Functions } from '../../../common/core/load';
import { Services } from './services';
declare const jQuery: any;
@Injectable()
@Component({
    templateUrl: 'template.html',
    styleUrls: ['./style.css'],
    selector: 'popup-assign-multi-csr',
    directives: [WMSPagination, PaginationControlsCmp],
    pipes: [PaginatePipe],
    providers: [Services]
})

export class PopupAssignMultiCSR {
    public infoSearchUser = {};
    public oldDataCSRUser = [];
    public allCSR = [];
    public _orderId = '';
    public selectedAll = false;
    public selectedItems = [];
    public usersExisted = [];
    @Input('orderId') set orderId(value) {
        if (value) {
            this._orderId = value;
        }
    }
    @Input('listSelectedItem') set listSelectedItem(value) {
        if (value) {
            jQuery('#assign-csr2').modal('show');
            this.infoSearchUser = {};
            this.allCSR = value;
            this.oldDataCSRUser = value;
        }
    }
    @Input('listCSRExisted') set listCSRExisted(value) {
        if (value) {
            this.usersExisted = value;
        }
    }
    @Output() messages = new EventEmitter();
    @Output() isAssigned = new EventEmitter();
    public showLoadingOverlay = false;
    constructor(private _func: Functions,
        private service: Services) { }
    public checkSelectItem(item) {
        item['selected'] = !item['selected'];
        this.selectedAll = false;
        // reset this.assignUser
        this.selectedItems = [];
        for (let i = 0, l = this.allCSR.length; i < l; i++) {
            item['odr_id'] = parseInt(this._orderId, 10);
            item['csr'] = parseInt(item['user_id'], 10);
            if (this.allCSR[i]['selected']) {
                this.selectedItems.push(this.allCSR[i]);
            }
        }
        if (this.selectedItems.length === this.allCSR.length) {
            this.selectedAll = true;
        }
    }

    private assignUser: any;
    public assignCSR() {
        const CSRs = this.usersExisted.concat(this.selectedItems);
        this._saveCSR(CSRs);
    }

    public _saveCSR(CSRs) {
        const data = {
            'data': CSRs
        }
        this.showLoadingOverlay = true;
        this.service.saveListCSR(this._orderId, JSON.stringify(data)).subscribe(
            data => {
                jQuery('#assign-csr').modal('hide');
                this.showLoadingOverlay = false;
                this.isAssigned.emit(true);
            },
            err => {
                jQuery('#assign-csr').modal('hide');
                this.showMessage('danger', this._func.parseErrorMessageFromServer(err));
                this.showLoadingOverlay = false;
            }
        );
    }

    filterUser() {
        let data = this.oldDataCSRUser;
        if (this.infoSearchUser['first_name']) {
            data = this.oldDataCSRUser.filter(x => x.first_name.toLowerCase().includes(this.infoSearchUser['first_name'].toLowerCase()));
        }
        if (this.infoSearchUser['last_name']) {
            data = data.filter(x => x.last_name.toLowerCase().includes(this.infoSearchUser['last_name'].toLowerCase()));
        }
        if (this.infoSearchUser['email']) {
            data = data.filter(x => x.email.toLowerCase().includes(this.infoSearchUser['email'].toLowerCase()));
        }
        this.allCSR = data;
    }

    resetFilterUser() {
        this.infoSearchUser['first_name'] = '';
        this.infoSearchUser['last_name'] = '';
        this.infoSearchUser['email'] = '';
        this.allCSR = this.oldDataCSRUser;
    }

    private showMessage(msgStatus, msg) {
        this.messages.emit({
            'status': msgStatus,
            'txt': msg
        })
        jQuery(window).scrollTop(0);
    }
    private _resetSelectedItems() {
        this.selectedAll = false;
        this.selectedItems = [];
    }

    public checkAll(event) {
        this.selectedAll = !this.selectedAll;
        // set empty array  ListSelectedItem
        this.selectedItems = [];
        // Loop
        this.allCSR.forEach((item) => {
            item['selected'] = this.selectedAll;
            item['odr_id'] = parseInt(this._orderId, 10);
            item['csr'] = parseInt(item['user_id'], 10);
            if(this.selectedAll) {
                // if check all = true , pushing to ListSelectedItem
                this.selectedItems.push(item);
            }
        })
    }

}