import {Component, Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {Functions, API_Config} from '../../../common/core/load';

@Injectable()
export class PickingListServices {

  public AuthHeader = this._Func.AuthHeader();
  public AuthHeaderPost = this._Func.AuthHeaderPost();

  constructor(
    private _Func: Functions, 
    private http: Http, 
    private _api: API_Config) {
  }

  getPickingList(params) {
  	return this.http.get(this._api.API_Wavepick + '/order-picking-list' + params,{ headers: this.AuthHeader })
      .map((res: Response) => res.json());
  }

  getExportAPIUrl(params) {
  	return this._api.API_Wavepick + '/order-picking-list' + params;
  }

  getWavepickStatusList() {
    return this.http.get(this._api.API_Wavepick + '/wave-pick-statuses?type=wv_update',{ headers: this.AuthHeader })
      .map((res: Response) => res.json());
  }

  runInsertOrderCartonQueue(whsId, wavepickId) {
    return this.http.post(`${this._api.API_Wavepick}/${whsId}/update-wave-pick/${wavepickId}/queue`, '', { headers: this.AuthHeader })
      .map((res: Response) => res.json());
  }

  
}
