import {Component, Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {API_Config, Functions} from '../common/core/load';

@Injectable()

export class CycleCountServices {
    public AuthHeader = this._Func.AuthHeader();
    public AuthHeaderPost = this._Func.AuthHeaderPost();
    public AuthHeaderPostJSON = this._Func.AuthHeaderPostJson();
    private urlAPIItem = this._API.API_ItemMaster;

    constructor(private _Func:Functions, private _API:API_Config, private http:Http) {

    }

    /*
     ********************************************************************************************************************
     */

    addNewCycleCount(data) {

        return this.http.post(this._API.API_Cycle_Count, data , {headers: this.AuthHeaderPostJSON})
            .map(res => res.json());
    }

    /*
     ********************************************************************************************************************
     */

    checkSameCusAssign(data) {

        return this.http.post(this._API.API_Cycle_Count + '/check-cus-in-user', data , {headers: this.AuthHeaderPostJSON})
            .map(res => res.json());
    }

    // Autocomplete for search
    public getItemBySKU (sku, cus_id = '') {

        return this.http.get(this.urlAPIItem +  '?sku=' + sku + '&cus_id=' + cus_id, {headers: this.AuthHeader})
            .map(res => res.json().data);


    }

    // Autocomplete for search
    public getItemBySKUCC (sku, cus_id = '') {

        return this.http.get(this._API.API_Cycle_Count_Item +  '?sku=' + sku + '&cus_id=' + cus_id, {headers: this.AuthHeader})
            .map(res => res.json().data);


    }

    /*
     ********************************************************************************************************************
     */

    updateCycleCount(id, data) {

        return this.http.put(this._API.API_Cycle_Count + '/' + id, data, {headers: this._Func.AuthHeaderPostJson()})
            .map(res => res.json());
    }

    /*
     ********************************************************************************************************************
     */

    updateItemOfCycleCount(id, data) {

        return this.http.put(this._API.API_Count_Item_List + id, data, {headers: this._Func.AuthHeaderPostJson()})
            .map(res => res.json());
    }

    /*
     ********************************************************************************************************************
     */

    getCycleCountDetail(id) {
        return this.http.get(this._API.API_Cycle_Count + '/' + id, {headers: this.AuthHeader})
            .map((res:Response) => res.json());
    }

    /*
   ********************************************************************************************************************
   */

    getCycleCount(param) {

        return this.http.get(this._API.API_Cycle_Count + param, {headers: this.AuthHeader})
            .map((res:Response) => res.json());
    }

    /*
     ********************************************************************************************************************
     */

    getCycleCountItemDetail(cc_id, cc_item_id) {

        return this.http.get(this._API.API_Count_Item_List + cc_id + '/item/' + cc_item_id, {headers: this.AuthHeader})
            .map((res:Response) => res.json());
    }

    /*
     ********************************************************************************************************************
     */

    deleteCycleCount($params) {

        return this.http.post(this._API.API_Cycle_Count + "/delete", $params ,{headers: this.AuthHeaderPostJSON})
            .map((res:Response) => res.json());
    }

    /*
     ********************************************************************************************************************
     * Save CC
     */

    save($cc_id) {

        return this.http.post(this._API.API_Cycle_Count + "/saveCycled/" + $cc_id , '' ,{headers: this.AuthHeaderPostJSON})
            .map((res:Response) => res.json());
    }

    /*
     ********************************************************************************************************************
     * Print CC
     */

    printPdf($cc_id) {

        return this.http.get(this._API.API_Cycle_Count + "/print-pdf/" + $cc_id  ,{headers: this.AuthHeader})
            .map((res:Response) => res.json());
    }


    /*
     ********************************************************************************************************************
     * Recount CC
     */

    recount($params) {

        return this.http.post(this._API.API_Cycle_Count + "/recount", $params ,{headers: this.AuthHeaderPostJSON})
            .map((res:Response) => res.json());
    }


    /*
     ********************************************************************************************************************
     * accept CC
     */

    accept($params) {

        return this.http.post(this._API.API_Cycle_Count + "/approve", $params ,{headers: this.AuthHeaderPostJSON})
            .map((res:Response) => res.json());
    }


    //Save SKU
    saveSku(cc_id, $params) {

        return this.http.post(this._API.API_Cycle_Count + "/" + cc_id + '/add-sku', $params ,{headers: this.AuthHeaderPostJSON})
            .map((res:Response) => res.json());
    }

    /*
     ********************************************************************************************************************
     */

    search(param) {
        return this.http.get(this._API.API_Cycle_Count + param, {headers: this.AuthHeader})
            .map((res:Response) => res.json());
    }

    /*
     ********************************************************************************************************************
     */

    getCycleCountStatus() {
        return this.http.get(this._API.API_Cycle_Status, {headers: this.AuthHeader})
            .map(res => res.json());
    }

    /*
     ********************************************************************************************************************
     */

    public getCustomersByWH($params) {

        return this.http.get(`${this._API.API_INVENTORY_NEW_REPORTS}/customers${$params}&sort[cus_name]=asc`, {headers: this.AuthHeader})
            .map(res => res.json());

    }

    /*
     ********************************************************************************************************************
     */

    getLocationByWH(whsId, param) {
        return this.http.get(this._API.API_Warehouse + "/" + whsId + "/locations" + param + '&is_cc=1', {headers: this.AuthHeader})
            .map((res:Response) => res.json());
    }

    /*
     ********************************************************************************************************************
     */

    getItemLocation(idLocations, params) {
        return this.http.get(this._API.API_GOODS_RECEIPT_MASTER + '/relocations' + "/" + idLocations + params, {headers: this.AuthHeader})
            .map((res:Response) => res.json());
    }

    /*
     ********************************************************************************************************************
     */

    public saveCycleCount(params) {

        return this.http.post(this._API.API_Cycle_Count, params, {headers: this.AuthHeader})
            .map(res => res.json());

    }

    /*
     ********************************************************************************************************************
     */

    public getCountItemByCycleID(cycleID, param) {

        return this.http.get(this._API.API_Count_Item_List + cycleID + param, {headers: this.AuthHeader})
            .map(res => res.json());

    }

    /*
     ********************************************************************************************************************
     */

    getCycleCountUsers() {

        return this.http.get(this._API.API_Cycle_Count_User, {headers: this.AuthHeader})
            .map(res => res.json());

    }

    /*
     ********************************************************************************************************************
     */

    getCycleCountType() {
        return this.http.get(this._API.API_Cycle_Type, {headers: this.AuthHeader})
            .map(res => res.json());
    }

    public getSKUs(params) {
        return this.http.get(`${this._API.API_INVENTORY_NEW_REPORTS}/auto-sku${params}`, {headers: this.AuthHeader})
            .map(res => res.json());

    }

    public getPackSize(params = '') {
        return this.http.get(`${this._API.API_ORDERS_ORG}/itemPackSizes/${params}`, {headers: this.AuthHeader})
            .map(res => res.json());

    }

    /*
     ********************************************************************************************************************
     */

    public saveChangeAssignee(ccId, asg_id) {

        return this.http.put(this._API.API_Cycle_Count + '/' + ccId + '/update-assignee/' + asg_id , '', {headers: this.AuthHeader})
            .map(res => res.json());

    }

    getPrinterAPI($wh_id) {
        return `${this._API.API_Cycle_Count_V2}/whs/${$wh_id}/location/tally-sheet/print`;
    }

    getWhs_code_by_id($id){

        return this.http.get(this._API.API_Warehouse+"/"+$id,{ headers: this.AuthHeader })
            .map((res: Response) => res.json());

    }
    getPalletByLocation($w_id, $id){

        return this.http.get(this._API.API_Cycle_Count_Root +"/warehouses/" + $w_id + "/location" +"/"+$id + "/get-pallet",{ headers: this.AuthHeader })
            .map((res: Response) => res.json());

    }

    public checkStatusReduce(params) {

        return this.http.post(this._API.API_Cycle_Count + '/check-orders', params, {headers: this._Func.AuthHeaderPostJson()})
            .map(res => res.json());

    }

    getFreeLocation($w_id, $params){

        return this.http.get(this._API.API_Warehouse + "/" + $w_id + "/free-locations" + $params,{ headers: this.AuthHeader })
            .map((res: Response) => res.json());

    }

    public getSKUAdd(params) {
        return this.http.get(`${this._API.API_Cycle_Count}/auto-sku${params}`, {headers: this.AuthHeader})
            .map(res => res.json());

    }

    getListReason(params = '') {

        return this.http.get(this._API.API_Cycle_Count + '/reason-list', {headers: this.AuthHeader})
            .map((res:Response) => res.json());
    }

    addReason(data) {

        return this.http.post(this._API.API_Cycle_Count + '/add-reason', data , {headers: this.AuthHeaderPostJSON})
            .map(res => res.json());
    }

    unsetRfidOnLocation(data) {
        return this.http.post(this._API.API_Cycle_Count_Root + '/location/unset-cartons-rfid', data , {headers: this.AuthHeaderPostJSON})
            .map(res => res.json());
    }

    
    public getSKUsSearch(whs_id,params) {
        return this.http.get(`${this._API.API_Cycle_Count_Root}/ccn/${whs_id}/auto-sku` + params, {headers: this.AuthHeader})
            .map(res => res.json());

    }

}
