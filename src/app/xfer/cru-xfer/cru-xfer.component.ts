import {Component } from '@angular/core';
import { FORM_DIRECTIVES,ControlGroup,FormBuilder, Control ,ControlArray,Validators} from '@angular/common';
import {RouteParams,Router ,RouteData} from '@angular/router-deprecated';
import {UserService, Functions} from '../../common/core/load';
import {XferService} from './xfer-service';
import { ValidationService } from '../../common/core/validator';
import {WMSBreadcrumb,PaginatePipe,WMSMessages, PaginationControlsCmp, PaginationService, InvoiceTracking} from '../../common/directives/directives';
import { Http } from '@angular/http';
import {GoBackComponent} from "../../common/component/goBack.component";
import {BoxPopupService} from "../../common/popup/box-popup.service";
import {WarningBoxPopup} from "../../common/popup/warning-box-popup";
declare var jQuery: any;
@Component({
  selector: 'cru-xfer',
  providers: [XferService,PaginationService,ValidationService,FormBuilder, BoxPopupService,UserService],
  directives: [FORM_DIRECTIVES,PaginationControlsCmp,WMSMessages,GoBackComponent,WMSBreadcrumb, InvoiceTracking],
  pipes:[PaginatePipe],
  templateUrl: 'cru-xfer.component.html',
})

export class CruXferComponent {

  public Loading = [];
  private messages;
  private perPage=100;
  private Action;
  private TitlePage='View Customer';
  private IsView:boolean=false;
  private ListSelectedItem:Array<any>=[];
  XFForm: ControlGroup;
  private cusId;
  private showLoadingOverlay:boolean=false;
  private Customers:Array<any>=[];
  private selectedAll={};
  private whs_id;
  showForm:boolean = true;
  private items:Array<any>=[];
  private cus_id;
  private autoSearchData={};
  private _xferBy='sku';
  private itemSelected:Array<any>=[];
  parseInt=parseInt;
  private validLoc;
  private totalPieces=0;
  private evenTracking:Array<any>=[];
  private xferNum:string = '';



  constructor(
    private _http: Http,
    private _Func: Functions,
    private params: RouteParams,
    private fb: FormBuilder,
    private _Valid: ValidationService,
    private _boxPopupService: BoxPopupService,
    private XFService:XferService,
    _RTAction: RouteData,
    private _user:UserService,
    private _router: Router) {


    this.Action=_RTAction.get('action');
    this.whs_id=this._Func.lstGetItem('whs_id');

    this.checkPermission();

  }

  // Check permission for user using this function page
  private createXFER;
  private viewXFER;
  private editXFER;
  private allowAccess:boolean=false;
  checkPermission(){

    this.showLoadingOverlay = true;
    this._user.GetPermissionUser().subscribe(
      data => {

        this.showLoadingOverlay = false;
        this.createXFER = this._user.RequestPermission(data, 'createXFER');
        this.editXFER = this._user.RequestPermission(data, 'editXFER');
        this.viewXFER = this._user.RequestPermission(data, 'viewXFER');

        // load default
        this.getCustomersByWH();

        // Action new
        if(this.Action=='new')
        {
          if(this.createXFER) {
            this.allowAccess=true;
            this.TitlePage = 'New Xfer';
            this.formBuilder();
          }
          else{
            this.redirectDeny();
          }
        }

        // Action edit
        if(this.Action=='edit')
        {
          if(this.editXFER) {
            this.allowAccess=true;
            this.TitlePage = 'Edit Xfer';
            this.geteXferDetail();
          }
          else{
            this.redirectDeny();
          }
        }

        // Action edit
        if(this.Action=='view')
        {
          if(this.viewXFER) {
            this.allowAccess=true;
            this.IsView=true;
            this.TitlePage = 'View Xfer';
            this.geteXferDetail();
          }
          else{
            this.redirectDeny();
          }
        }

      },
      err => {
        this.parseError(err);
        this.showLoadingOverlay = false;
      }
    );
  }

  redirectDeny(){
    this._router.parent.navigateByUrl('/deny');
  }

  /***
   *
   * Order forrm
   * **/
  private xfer_hdr_id='';
  formBuilder(data={}){

    if(this.Action!=='new'){
      this.xfer_hdr_id=this.params.get('id');
    }

    this.XFForm =this.fb.group({
      xfer_hdr_id:[data['xfer_hdr_id'] ? data['xfer_hdr_id'] : this.xfer_hdr_id],
      xfer_hdr_num: [data['xfer_hdr_num'] ? data['xfer_hdr_num'] : ''],
      cus_id: [data['cus_id'] ? data['cus_id'] : '',Validators.required],
      request_qty: [data['request_qty'],Validators.compose([Validators.required,this._Valid.validateInt,this._Valid.isZero,this._Valid.inPutOnlyNumber])],
      loc_code: [data['loc_code'],Validators.compose([Validators.required, this._Valid.validateSpace])],
      loc_id: [data['loc_id']],
      sku: [data['sku'],Validators.compose([Validators.required, this._Valid.validateSpace])],
      item_id: [data['item_id'],Validators.compose([Validators.required,this._Valid.validateSpace])],
      cus_upc: [data['cus_upc'],Validators.compose([Validators.required,this._Valid.validateSpace])],
      lot:[data['lot']],
      size: [data['size']],
      color: [data['color']],
      xfer_sts:[data['xfer_hdr_sts_name'] ? data['xfer_hdr_sts_name'] : 'Pending'],
      items:[null,Validators.required]
    });

    this.cus_id=data['cus_id'];

    // default required only sku field
    if(this.Action=='new'){
      this.change_XFER_BY('sku');
    }
    else{
      this.items=data['items'];
      this.itemSelected=data['items'].slice();
      this.setSelectedItem(this.items,true);
    }

  }

  private xferId;
  geteXferDetail(){

    this.xferId=this.params.get('id');
    this.XFService.geteXferDetail(this.xferId).subscribe(
      data => {
        this.xferNum = data['xfer_hdr_num'];
        this.formBuilder(data);
        if(this.IsView){
          this.getEventracking(this.xferNum);
        }
      },
      err => {
        this.parseError(err);
      },
      () => {}
    );
   // let data=this.XFService.geteXferDetail(this.xferId);
  //  this.formBuilder(data);

  }


  selectedLoc(item,$event){

    let checked=$event.target.checked;
    let request_qty=this.XFForm.controls['request_qty'].value;

    if(checked){

      item['selected']=true;

      this.itemSelected.push(item);
      // check valid
      if(request_qty) {
        if (parseInt(item['ttl_pieces']) < parseInt(request_qty)) {
          this.scropllTop();
          this.setRequestQtyControl({invalidPieces: true});
        }
        else {
          // if valid
          this.setRequestQtyControl(null);
        }
      }
    }
    else{

      this.removeItem(item,this.itemSelected);
      item['selected']=false;
      if(request_qty){
        (<Control>this.XFForm.controls['request_qty']).setErrors(null);
      }
    }

    // check Enogh number total
    this.checkEnoughNumber();

  }

  getEventracking(XferNumber){

    this.XFService.getEventracking(XferNumber).subscribe(
      data => {
        this.evenTracking = data.data;
      },
      err => {
        this.parseError(err);
      },
      () => {}
    );

  }

  /**======================================
   * setRequestQtyControl
   *====================================**/
  setRequestQtyControl(error){
    (<Control>this.XFForm.controls['request_qty']).setErrors(error);
  }

  /**======================================
   * removeItem
   *====================================**/
  removeItem(item,array:Array<any>){

    let index=array.indexOf(item);
    if(index>=0){
      array.splice(index,1);
    }else{
    }

  }

  /**======================================
   * checkEnoughNumber
   *====================================**/
  checkEnoughNumber(){

    if(this.itemSelected.length) {
      let number_request = parseInt(this.XFForm.value['request_qty']);
      let array = this.itemSelected;

      // reset this.totalPieces
      this.totalPieces = 0;

      if (number_request) {
        let total = 0;
        for (let i in array) {
          total = total + parseInt(array[i]['ttl_pieces']);
        }
        this.totalPieces = total;

        if (total >= number_request) {
          this.setDisableItem(this.items,true);
          this.setRequestQtyControl(null);
        }
        else {
          this.setDisableItem(this.items,false);
          this.setRequestQtyControl({invalidPieces: true});
        }
      }
      else {
        this.setDisableItem(this.items,false);
      }
    }
    else{
      this.setDisableItem(this.items,false);
    }

  }

  /*
  * Reset Item
  * */
  resetCheckedItem($event){
      this.itemSelected = [];
      this.setSelectedItem(this.items,false);
      (<Control>this.XFForm.controls['items']).updateValue(null);
  }

  /**======================================
   * set checked for item
   *====================================**/
  setSelectedItem(array:Array<any>,value:boolean){

    array.forEach((item)=> {
      item['selected'] = value;
    });

  }

  /**======================================
   * set Disable Item
   *====================================**/
  setDisableItem(array:Array<any>,value:boolean)
  {
    array.forEach((item)=>{
      if(!item['selected']){
        item['disabled']=value;
      }
    });

  }

  /**======================================
   * changeRequestPiece
   *====================================**/
  changeRequestPiece(){
    this.checkEnoughNumber();
  }


  changeCustomer($event){

    let cus_id=$event.target.value;

      // if customer id is null then without show popup
      if(!this.cus_id) {
        this.cus_id=cus_id;
      }
      else{

        let that = this;
        let warningPopup = new WarningBoxPopup();
        warningPopup.text = this._Func.msg('M043');

        this._boxPopupService.showWarningPopup(warningPopup)
          .then(function (dm) {
            if(dm) {
              that.resetFormData();
              that.cus_id=cus_id;
            }
            else{
              (<Control>that.XFForm.controls['cus_id']).updateValue(that.cus_id);
            }
          });

      }

  }

  resetFormData(){
    Object.keys(this.XFForm.controls).map(
      key =>{
        if(key!=='cus_id'&&key!=='xfer_hdr_id' && key !== 'xfer_sts') {
          (<Control>this.XFForm.controls[key]).updateValue('');
          //(<Control>this.XFForm.controls[key]).setErrors(null);
        }
      }
    );
    this.items=[];
  }

  change_XFER_BY(e){

    let groupAu=['sku','item_id','cus_upc'];
    this._xferBy=e;

    if(this.XFForm.value[e]){

      if(!this.items.length){
        // only autoComplete if item length equal 0
        this.autoComplete(this.XFForm.value[e],e);
      }

    }

  }

  setErrorForControl(control,err){
    (<Control>this.XFForm.controls[control]).setErrors(err);
  }

  getControlValue(control){
    return (<Control>this.XFForm.controls[control]).value;
  }

  autoComplete(key,fieldname){

    let searchbyAPI='searchItems';
    let searchkey=this._Func.trim(key);

    if(fieldname=='loc_code'){
      // autocomplete by to loc
      searchbyAPI='searchLoc';
    }
    else{
      this.resetItemsData();
    }

    // if empty field
    if(!searchkey.length){
      this.XFForm.controls[fieldname]['subscribe'].unsubscribe();
      this.XFForm.controls[fieldname]['showLoading']=false;
      return false;
    }

    // search only if cus_id have seleced
    if(this.XFForm.value['cus_id']){

      let param="?"+fieldname+"="+searchkey+"&cus_id="+this.XFForm.value['cus_id']+"&limit=20&sort["+fieldname+"]=asc";
      let enCodeSearchQuery=this._Func.FixEncodeURI(param);

      this.XFForm.controls[fieldname]['showLoading']=true;

      if(this.XFForm.controls[fieldname]['subscribe'])
      {
        this.XFForm.controls[fieldname]['subscribe'].unsubscribe();
      }


      // do Search
      eval(`this.XFForm.controls[fieldname]['subscribe']=this.XFService.`+searchbyAPI+`(enCodeSearchQuery).debounceTime(400).distinctUntilChanged().subscribe(
        data => {
          // disable loading icon
          this.XFForm.controls[fieldname]['showLoading']=false;
          this.autoSearchData[fieldname] = data.data;
          this.checkKeyExist(searchkey,this.autoSearchData[fieldname],fieldname);
        },
        err =>{
          this.XFForm.controls[fieldname]['showLoading']=false;
          this.parseError(err);
        },
        () =>{
         
        }
      );`); // end eval function

    }

  }

  resetItemsData(){
    this.itemSelected=[];
    this.items=[];
    (<Control>this.XFForm.controls['items']).updateValue(null);
  }

  getItem(){

    this.showLoadingOverlay=true;
    let params = '?limit=9999';
    
    params += this.XFForm.value['cus_id'] ? "&cus_id="+this.XFForm.value['cus_id']: '';
    params += this.XFForm.value['sku']  ? "&sku="+this.XFForm.value['sku']: '';
    params += this.XFForm.value['item_id']  ? "&item_id="+this.XFForm.value['item_id']: '';
    params += this.XFForm.value['cus_upc'] ? "&cus_upc="+this.XFForm.value['cus_upc']: '';
    
    this.XFService.getListItemLocation(params)
      .subscribe(
        data => {
          this.showLoadingOverlay=false;
          this.items = data;
        },
        err => {
          this.showLoadingOverlay=false;
          this.parseError(err);
        },
        () => {
        }
      );
  }

  getItemUpc($event,fieldcontrol){

    if(this.XFForm.value[fieldcontrol]){

      this.resetItemsData();

      let searchkey=this._Func.trim($event.target.value);
      let param="?cus_id="+this.XFForm.value['cus_id']+'&'+fieldcontrol+"="+searchkey;
      let enCodeSearchQuery=this._Func.FixEncodeURI(param);

      this.XFForm.controls[fieldcontrol]['showLoading']=true;

      // do Search
      this.XFService.searchItems(enCodeSearchQuery).subscribe(
        data => {
          this.XFForm.controls[fieldcontrol]['showLoading']=false;
          this.checkKeyExist(searchkey,data.data,fieldcontrol);
        },
        err =>{
          this.XFForm.controls[fieldcontrol]['showLoading']=false;
          this.parseError(err);
        },
        () =>{
        }
      );

    }
  }

  // get list custommer

  private getCustomersByWH () {

    let params='?whs_id=' + this.whs_id + "&limit=10000";
    this.XFService.getCustomersByWH(params)
      .subscribe(
        data => {
          this.Customers = data.data;
        },
        err => {
          this.parseError(err);
        },
        () => {
        }
      );

  }

  private onPageSizeChanged($event)
  {
    this.perPage = parseInt($event.target.value);
  }

  /*
   * Add for pagination
   * */
  /*
   * Selected
   * */
  private Selected($event,item,ListName:string,state,paginId) {

    let Array=eval(`this.`+ListName);
    this.selectedAll[ListName]=false;

    if ($event.target.checked) {
      item['selected']=true;
      this.checkCheckAll(ListName,state,paginId);
    }
    else {
      item['selected']=false;
    }
  }

  checkCheckAll(ListName:string,StatePagin,objName)
  {
    let Array=eval(`this.`+ListName);

    let el=StatePagin[objName];
    let end=el['end'];
    let chk=0;
    if(el['size']){
      if(el['size']<el['end']) { end=el['size']; }
      for(let i=el['start'];i<end;i++) {
        if(Array[i]['selected']){chk++; }
      }
      if(chk==el['slice'].length) {this.selectedAll[ListName]=true; }
      else{this.selectedAll[ListName]=false; }

    } // end if size

  }

  /*
   * Check All/Off
   * */
  private CheckAll(event,ListName) {
    let Array=eval(`this.`+ListName);

    if(!Array.length)
    {
      this.selectedAll[ListName] = false;
      return;
    }
    if (event) {
      this.selectedAll[ListName] = true;
      this.setListItemsSelected(true);
    }
    else {
      this.selectedAll[ListName]= false;
      this.setListItemsSelected(false);
    }

  }

  setListItemsSelected(value)
  {
    this.items.forEach((item) => {
      item['selected']=value;
    });
  }




  checkKeyExist(str:string,arr:Array<any>,fieldname)
  {
    for(let i in arr) {
      if (str == this._Func.trim(arr[i][fieldname])) {// auto select item if match
        if(fieldname=='loc_code') {
          this.selectedLocItem(arr[i]);
        }
        else{
          this.selectedItem(arr[i]);
        }
        this.setErrorsControl(fieldname, null);
        return;
      }
    }
    // if not found code -> set invalid
    this.setErrorsControl(fieldname,{'invalid':true});
    return

  }

  setErrorsControl(control,err){
    (<Control>this.XFForm.controls[control]).setErrors(err);
  }

  /*
   * Auto select item if match
   * */
  selectedItem(dataItem)
  {
    this.resetItemsData();

    (<Control>this.XFForm.controls['sku']).updateValue(dataItem['sku']);
    (<Control>this.XFForm.controls['item_id']).updateValue(dataItem['item_id']);
    (<Control>this.XFForm.controls['cus_upc']).updateValue(dataItem['cus_upc']);
    (<Control>this.XFForm.controls['size']).updateValue(dataItem['size']);
    (<Control>this.XFForm.controls['color']).updateValue(dataItem['color']);
    (<Control>this.XFForm.controls['lot']).updateValue(dataItem['lot']);

    // after selected done , getItem
    this.getItem();

  }

  selectedLocItem(item){
    (<Control>this.XFForm.controls['loc_id']).updateValue(item['loc_id']);
    (<Control>this.XFForm.controls['loc_code']).updateValue(item['loc_code']);
  }

  /*
   * Save
   * */
  private submitForm:boolean=false;
  private printXFER=false;
  Save():void{

    this.scropllTop();
    if(this.itemSelected.length){
      (<Control>this.XFForm.controls['items']).updateValue(this.itemSelected);
    }
    else{
      this.messages = this._Func.Messages('danger',`Please choose one location to do the transfer!`);
      return;
    }

    let data=this.XFForm.value;
    this.submitForm=true;
    this.handelValidForm();

    if(this.XFForm.valid) {

      let data_json = JSON.stringify(data);

      if (this.Action == 'new') {
        // add new
        this.AddNew(data_json);
      }
      else {
        // edit zone type
        this.xferId=this.params.get('id');
        this.Update(this.xferId,data_json);
      }


    }
    else{
    }


  }

  handelValidForm(){

    let groupAu=['sku','item_id','cus_upc'];
    let xf=this._xferBy;
    for(let i in groupAu)
    {
      let control=groupAu[i];
      if(xf!==control){
        (<Control>this.XFForm.controls[control]).setErrors(null);
      }
    }
  }

  /*
   *
   * */
  AddNew(data)
  {
    let n=this;
    this.showLoadingOverlay=true;
    this.XFService.createXferRecord(data).subscribe(
      data => {
        this.showLoadingOverlay = false;
        //Reset Form
        this.messages = this._Func.Messages('success', this._Func.msg('VR107'));
        setTimeout(function(){
          n._router.parent.navigateByUrl('/xfer');
        }, 600);
      },
      err => {
        this.showLoadingOverlay= false;
        if(err.json().errors.message)
        {
          this.messages = this._Func.Messages('danger',err.json().errors.message);
        }
      },
      () => {}
    );
  }

  Update(id,data)
  {

    let n=this;
    this.showLoadingOverlay=true;
    this.XFService.updateXferRecord(data).subscribe(
      data => {
        this.showLoadingOverlay=false;
        this.messages = this._Func.Messages('success', this._Func.msg('VR109'));
        setTimeout(function(){
          n._router.parent.navigateByUrl('/xfer');
        }, 600);
      },
      err => {
        this.showLoadingOverlay=false;
        this.parseError(err);
      },
      () => {}
    );

  }

  checkInputNumber(evt){
    this._Valid.isNumber(event);
  }

  // Show error when server die or else
  private parseError (err) {
    this.messages = {'status' : 'danger', 'txt' : this._Func.parseErrorMessageFromServer(err)};
  }

  redirectToList(){
    let n = this;
    this._boxPopupService.showWarningPopup()
      .then(function (dm) {
        if(dm)
          n._router.parent.navigateByUrl('/xfer');
      })
  }

  scropllTop()
  {
    // scroll to top
    jQuery('html, body').animate({
      scrollTop: 0
    }, 700);
  }


}





