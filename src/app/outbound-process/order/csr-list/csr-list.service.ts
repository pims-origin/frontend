import {Component, Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {Functions, API_Config} from '../../../common/core/load';

@Injectable()
export class CSRListService {

    public AuthHeader = this._Func.AuthHeader();
    public AuthHeaderPost = this._Func.AuthHeaderPost();
    public AuthHeaderPostJson = this._Func.AuthHeaderPostJson();

    constructor(
        private _Func: Functions,
        private http: Http,
        private _api: API_Config
    ) {
        
    }

    getListCRSByCustomer($params, $wh_id = '') {
        return  this.http.get(this._api.API_Authen + '/users/get-csr/' + $params + '/' + $wh_id, { headers: this.AuthHeader })
            .map((res: Response) => res.json());
    }

    getListCRSByWarehouse($wh_id) {
        return  this.http.get(this._api.API_Authen + '/csr-warehouse/' + $wh_id, {headers: this.AuthHeader})
            .map(res => res.json());
    }
    
    getListCSR(orderId) {
        return this.http.get(this._api.API_ORDERS_CORE + '/assign-multi-csr/' + orderId, { headers: this.AuthHeader })
            .map((res: Response) => res.json());
    }

    saveListCSR(orderId, data) {
        return this.http.post(this._api.API_ORDERS_CORE + '/assign-multi-csr/' + orderId, data, { headers: this.AuthHeaderPostJson })
            .map((res: Response) => res.json());
    }
}