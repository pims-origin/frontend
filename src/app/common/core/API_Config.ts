/*=================================
LIST API CONFIG
===================================*/
import {Injectable} from '@angular/core';
@Injectable()
export class API_Config {

    constructor(){}
    /* CONFIG return from environment variable */
    public API_URL_ROOT = process.env.CONFIG['API_URL_ROOT'];
    public PAGE_URL = process.env.CONFIG['PAGE_URL'];
    public SERVICE_DOCUMENTS = process.env.CONFIG['SERVICE_DOCUMENTS'];
    /*API Origin menus*/
    public API_MASTER_URL = this.API_URL_ROOT+ '/core/master-service/v1';

    public API_WAREHOUSE_URL = this.API_URL_ROOT+ '/core/warehouse-service/v1';

    /* API URL RESET PASSWORD */
    public API_URL_RESET_PASS = this.PAGE_URL+ '/#/reset-password';

    public API_COMMON = this.API_URL_ROOT + '/core/common-service/v1';

    public API_ITEM_SERVICE = this.API_URL_ROOT + '/core/item-service/v1';

    public API_ITEM_MASTER = this.API_URL_ROOT + '/core/item-master/v1';

    public API_CUSTOMER_CORE = this.API_URL_ROOT + '/core/customer/v1';

    public API_GOODS_RECEIPT = this.API_URL_ROOT + '/core/goods-receipt/v1';

    public API_Data_Layer = this.API_URL_ROOT + '/core/data-layer/v1';

    public API_Authen = this.API_URL_ROOT + '/core/authentication';

    public API_ORDERS_CORE = this.API_URL_ROOT + '/core/orders/v1';

    public API_Wavepick = this.API_URL_ROOT + '/core/wave-pick/v1';

    public API_Wavepick_2 = this.API_URL_ROOT + '/core/wave-pick/v2';

    public API_REPORTS_CORE = this.API_URL_ROOT + '/core/report/v1';

    public API_Pack = this.API_URL_ROOT + '/core/pack/v1';

    public API_Pack_v2 = this.API_URL_ROOT + '/core/pack/v2';

    public API_WH_XFER = this.API_URL_ROOT + '/core/transfer/v1';

    public API_INVOICE = this.API_URL_ROOT+'/core/invoice/v1';

    public API_WORK_ORDER = this.API_ORDERS_CORE+'/work-orders';

    public API_Cycle_Count_Root = this.API_URL_ROOT + '/core/cycle-count/v1';

    public API_Block_Stock_Root = this.API_Cycle_Count_Root + '/block-stock';

    public API_RF_GUN = this.API_URL_ROOT + '/core/rf-gun/v1';

    public API_Container_Plug = this.API_URL_ROOT + '/core/rf-gun';

    public API_ORDERS_V2 = this.API_URL_ROOT+'/core/orders/v2';

    public API_Cycle_Count_V2 = this.API_URL_ROOT + '/core/cycle-count/v2';

    public API_SHIPPING = this.API_URL_ROOT + '/core/shipping/v1';

    /* return left menu admin*/
    public API_LeftMenu = this.API_MASTER_URL + '/menu/left-menu';

    /* API Favorite menu */
    public API_FavoriteMenu = this.API_MASTER_URL + '/menu/favourite-menu';

    public API_ListMenu = this.API_MASTER_URL + '/menus/';

    /* API return list group menu */
    public API_GroupMenu = this.API_MASTER_URL + '/menus';

    public API_Users = this.API_MASTER_URL + '/users';

    public API_RESET_PASS = this.API_MASTER_URL+ '/reset-password';

    public API_CHECK_TOKEN = this.API_MASTER_URL+ '/check-reset-password-token';

    public API_Roles = this.API_MASTER_URL + '/roles';

    public API_User_Roles = this.API_MASTER_URL + '/users/roles';

    public API_URL_LOGIN = this.API_MASTER_URL+ '/login';

    public API_Permission = this.API_MASTER_URL + '/permissions';

    public API_SYSTEM_CONFIGURATION = this.API_MASTER_URL + '/system/';

    public API_USER_Permission = this.API_MASTER_URL + '/users/permissions';

    public API_Logout = this.API_MASTER_URL + '/logout';

    public API_Warehouse = this.API_WAREHOUSE_URL + '/warehouses';

    public API_Zone_Type = this.API_WAREHOUSE_URL + '/zone-type';

    public API_Zone = this.API_WAREHOUSE_URL + '/zone';

    public API_Location = this.API_WAREHOUSE_URL + '/locations';

    public API_Location_type = this.API_WAREHOUSE_URL + '/location-type';

    public API_Country = this.API_COMMON + '/countries';

    public API_State = this.API_COMMON + '/states';

    public API_Customers = this.API_COMMON + '/customers';

    public API_Customer_Statuses = this.API_COMMON + '/customer-statuses';

    public API_Charge_Codes = this.API_COMMON + '/charge-codes';

    public API_Charge_Types = this.API_COMMON + '/charge-types';

    public API_System_UOMs = this.API_COMMON + '/system-uoms';

    public API_USER_DEPARTMENT = this.API_COMMON + '/user-department';

    public API_ItemMaster =this.API_ITEM_SERVICE+ '/items';

    public API_ItemMaster_Exporter =this.API_ITEM_SERVICE+'/item-export-excel';

    public API_CUSTOMER_MASTER = this.API_CUSTOMER_CORE;

    public API_Customer = this.API_CUSTOMER_CORE + '/customers';

    public API_Container = this.API_GOODS_RECEIPT + '/containers';

    public API_Measurements = this.API_Data_Layer + '/system-measurements';

    public API_Asns = this.API_GOODS_RECEIPT + '/asns';

    public API_UOM = this.API_Data_Layer + '/system-uoms';

    public API_Goods_Receipt = this.API_GOODS_RECEIPT + '/goods-receipts';

    public API_GOODS_RECEIPT_REPORT = this.API_REPORTS_CORE + '/goods-receipt';

    public API_CHARGE_TYPE = this.API_CUSTOMER_CORE + '/charge-types';

    public API_CHARGE_CODE = this.API_CUSTOMER_CORE + '/charge-codes';

    public API_ASN_Status = this.API_GOODS_RECEIPT + '/asn-statuses';

    public API_Put_Away = this.API_GOODS_RECEIPT + '/putaway';

    public API_DAMAGE_TYPE = this.API_GOODS_RECEIPT + '/damage-type';

    public API_User_Metas = this.API_Authen + '/user-metas';

    public API_GR_Status = this.API_GOODS_RECEIPT + '/gr-statuses';

    public API_Damage_Carton = this.API_GOODS_RECEIPT + '/damage-carton';

    public API_User_Info = this.API_Authen + '/users/info';

    public API_User_Token = this.API_Authen + '/users/token';

    public API_GOODS_RECEIPT_MASTER = this.API_GOODS_RECEIPT;

    public API_URL_SETUP_PASSWORD =  this.PAGE_URL+ '/#/setup-password';

    public API_WarehouseId_Meta = this.API_Authen + '/user-metas/wid';

    public API_Customer_List_All = this.API_CUSTOMER_CORE + '/customers-all';

    public API_Customer_User = this.API_CUSTOMER_CORE + '/customers-user';

    public API_Customer_Warehouses = this.API_CUSTOMER_CORE + '/customer-warehouses';

    public API_SETUP_PASSWORD = this.API_Authen + '/users/setup-password';

    public API_ORDERS=this.API_ORDERS_CORE+ '/orders';

    public API_ALLOCATE=this.API_ORDERS_CORE+ '/allocates';

    public API_SEARCH_ITEMS=this.API_ORDERS_CORE+ '/invt-smr';

    public API_ORDER_TYPES=this.API_ORDERS_CORE+ '/order-types';

    public API_ORDERS_CANCEL = this.API_ORDERS + '/order-cancel-lists';


    public API_REPORTS = this.API_REPORTS_CORE;

    public API_INVENTORY_REPORTS = this.API_REPORTS_CORE + '/inventory';

    public API_AVAILABLE_INVENTORY_REPORTS = this.API_REPORTS_CORE + '/available-inventory';

    public API_STYLE_LOCATIONS_REPORTS = this.API_REPORTS_CORE + '/style-locations';

    public API_STORAGES_REPORTS = this.API_REPORTS_CORE + '/storages';

    public API_LOCATIONUTILIZATION_REPORTS = this.API_REPORTS_CORE + '/locationutilization';

    public API_DIMENSIONS_REPORTS = this.API_REPORTS_CORE + '/dimensions';


    public API_WAREHOUSE_LOCATIONS_REPORTS = this.API_REPORTS_CORE + '/warehouse-locations';

    public API_CUSTOMERS_REPORTS = this.API_REPORTS_CORE + '/customers';

    public API_SHIPPING_NEW_REPORTS = this.API_REPORTS_CORE + '/shipping-report';

    public API_INVENTORY_NEW_REPORTS = this.API_REPORTS_CORE + '/inventory';

    public API_INVENTORY_HISTORY_REPORTS = this.API_REPORTS_CORE + '/inventory-history';

    public API_ORDER_REPORTS = this.API_REPORTS_CORE + '/orders';

    public API_ONLINE_ORDER_REPORTS = this.API_REPORTS_CORE + '/online-orders';

    public API_OPEN_ORDER_REPORTS = this.API_REPORTS_CORE + '/open-orders';

    public API_WAVEPICKS_REPORTS = this.API_REPORTS_CORE + '/wavePicks';

    public API_CARTONS_REPORTS = this.API_REPORTS_CORE + '/carton';

    //Leon Thanikal 13/7/2017
    public API_RECEIVING_REPORTS = this.API_REPORTS_CORE + '/receiving-report';

    //Leon Thanikal 18/7/2017
    public API_ACTUAL_RECEIVING = this.API_REPORTS_CORE + '/actual-receiving';

    //Leon Thanikal 24/7/2017
    public API_RECEIVING_CONTAINER = this.API_REPORTS_CORE + '/receiving-container'

    //Leon Thanikal 27/7/2017
    public API_OUTBOUND_ORDER = this.API_REPORTS_CORE + '/outbound-order'

    public API_LOCATION_SATSUSES = this.API_COMMON + '/location-statuses';

    public API_CARTON_SATSUSES = this.API_COMMON + '/carton-statuses';

    public API_WAREHOUSE_NAMES = this.API_COMMON + '/warehouse-names';

    public API_CUSTOMER_NAMES = this.API_COMMON + '/customer-names';

    public API_CUSTOMER_STATUSES = this.API_COMMON + '/customer-statuses';

    public API_INVOICE_LIST = this.API_INVOICE + '/list-invoice';

    public API_INVOICE_LIST_DETAILS = this.API_INVOICE + '/invoice-details';

    public API_CUSTOMER_CONTACTS = this.API_INVOICE + '/client-contacts';

    public API_CLIENT_INFO = this.API_INVOICE + '/client-info';

    public API_Check_Chagre_Code = this.API_INVOICE + '/check-charge-code';

    public API_Invoice_Costs = this.API_INVOICE + '/invoice-costs';

    public API_Invioce_Process = this.API_INVOICE + '/invoice-process';

    public API_Print_Invoice = this.API_INVOICE + '/print-invoice';

    public API_Get_Print_Statement_Data = this.API_INVOICE + '/get-print-statement-data';

    public API_Print_Statement = this.API_INVOICE + '/print-statement';

    public API_View_Detail = this.API_INVOICE + '/view-detail';

    public API_Invoice_Receiving_Details = this.API_INVOICE + '/view-details/receiving';

    public API_Invoice_Order_Processing_Details = this.API_INVOICE + '/view-details/order-processing';

    public API_Invioce_Receive_Payment = this.API_INVOICE + '/receive-payment';

    public API_Invoice_Cancel = this.API_INVOICE + '/cancel-invoice';

    public API_Store_Invoice = this.API_INVOICE + '/store-invoice';

    public API_WH_XFER_LOC = this.API_WH_XFER + '/list-mz-location';

    public API_WH_XFER_TICKET_LIST = this.API_WH_XFER + '/list-all-ticket';

    public API_WH_XFER_CREATE_TICKET=this.API_WH_XFER+ '/create-xfer-ticket';

    public API_WH_XFER_PRINT_TICKET=this.API_WH_XFER+ '/download-ticket';

    public API_ORDERS_ORG=this.API_ORDERS_CORE;

    public API_Cycle_Count = this.API_Cycle_Count_Root + '/cycle-count';

    public API_Count_Item_List = this.API_Cycle_Count_Root + '/cycle-detail/';

    public API_Cycle_Type = this.API_Cycle_Count_Root + '/cycle/type';

    public API_Cycle_Status = this.API_Cycle_Count_Root + '/cycle/status';

    public API_Cycle_Count_Item = this.API_Cycle_Count_Root + '/cycle/items';

    public API_Cycle_Count_User = this.API_Cycle_Count_Root + '/cycle-count/get-users-in-whs';


    public DOCUMENT_API_UPLOAD = this.SERVICE_DOCUMENTS;

    public API_REASONS = this.API_WAREHOUSE_URL + '/reasons';

    public API_DashBoard=this.API_WAREHOUSE_URL+ '/dashboard';

    public API_PUTBACK_ORDER = this.API_ORDERS + '/canceled-orders-search';

    public API_CREATE_PB_RECEIPT = this.API_ORDERS_CORE + '/return';

    public API_ORDER_DETAIL_PB = this.API_ORDERS_CORE + '/putback';

    public API_ASSIGN_PALLET_PB =this.API_ORDERS_CORE+ '/putback/pal-assign';

    public API_Refill=this.API_WAREHOUSE_URL+ '/refill';

    public API_EDI= this.API_COMMON + '/edi';

    public API_Commodity= this.API_COMMON + '/commodity';

    public API_Dimension= this.API_COMMON + '/packedcartondimension';

    public API_Packtype= this.API_COMMON + '/packtype/dropdown';

    public API_Labor= this.API_COMMON + '/labor';

    public API_CUSTOMER_DAMAGED = this.API_CUSTOMER_CORE + '/customer-damage';

    public API_DAMAGED_CARTON_REPORT = this.API_GOODS_RECEIPT + '/cartons-damage';

    public API_PUT_BACK_REPORT = `${this.API_ORDERS_CORE}/put-back/reports`;

    public API_MASTER_DATA = `${this.API_URL_ROOT}/core/master-data/v1`;

    public API_MIGRATION = `${this.API_URL_ROOT}/core/migration/v1`;

    public API_CYCLE_COUNT_HIS_REPORT = this.API_REPORTS_CORE + '/cycle-count-history';

    public MASTERDATA = this.API_MASTER_DATA + '/master-data';

    public API_ITEM_PACKING = this.API_ITEM_SERVICE+ '/item-packing';

  public API_WAREHOUSE_CORE = this.API_URL_ROOT+ '/core/warehouse/v1';

}
