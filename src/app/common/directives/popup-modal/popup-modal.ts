import {Component,Input,Directive, Output, EventEmitter, ElementRef} from '@angular/core';
import {Functions,API_Config} from '../../../common/core/load';

@Component ({
  selector: 'popup-modal',
  directives:[],
  providers: [],
  templateUrl: 'popup-modal.html',
})
export class ModalPopupDirective {
  private _openModal:boolean=false;
  private modal={};
  @Input('openModal') set openPopup(value:boolean) {
    if(value==true)
    {
    }
  }
  @Input('modalContent') set modalContent(value) {
    this.modal=value;
  }
  @Output() dataClick = new EventEmitter();


  /*
  * okAction
  * */
  ClickButtonOk(value:boolean)
  {
    this.dataClick.emit(value);
  }
}
