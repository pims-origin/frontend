import {Component } from '@angular/core';
import { FORM_DIRECTIVES,ControlGroup,FormBuilder, Control ,ControlArray,Validators} from '@angular/common';
import {RouteParams,Router ,RouteData} from '@angular/router-deprecated';
import {UserService,FormBuilderFunctions, Functions} from '../../common/core/load';
import {XferService} from './xfer-inv-service';
import { ValidationService } from '../../common/core/validator';
import {WMSBreadcrumb,PaginatePipe,WMSMessages, PaginationControlsCmp, PaginationService} from '../../common/directives/directives';
import { Http } from '@angular/http';
import {GoBackComponent} from "../../common/component/goBack.component";
import {BoxPopupService} from "../../common/popup/box-popup.service";
import {WarningBoxPopup} from "../../common/popup/warning-box-popup";
declare var jQuery: any;
@Component({
  selector: 'xfer-inv',
  providers: [XferService,PaginationService,ValidationService,FormBuilder, BoxPopupService,UserService],
  directives: [FORM_DIRECTIVES,PaginationControlsCmp,WMSMessages,GoBackComponent,WMSBreadcrumb],
  pipes:[PaginatePipe],
  templateUrl: 'xfer-inv.component.html',
})

export class XferINVComponent {

  public Loading = [];
  private messages;
  private perPage=20;
  private Action;
  private TitlePage='';
  private IsView:boolean=false;
  private ListSelectedItem:Array<any>=[];
  XFForm: ControlGroup;
  private cusId;
  private showLoadingOverlay:boolean=false;
  private Customers:Array<any>=[];
  private selectedAll={};
  private whs_id;
  showForm:boolean = true;
  private items:Array<any>=[];
  private cus_id;
  private autoSearchData={};
  private _xferBy='sku';
  private itemSelected:Array<any>=[];
  parseInt=parseInt;
  private xferDetail={};
  private chkFromLoc=true;
  private chkToLoc=true;
  private evenTracking:Array<any>=[];

  xfer_tickets:ControlGroup[]=[];
  xferTicketArray: ControlArray= new ControlArray(this.xfer_tickets);

  constructor(
    private _http: Http,
    private _Func: Functions,
    private params: RouteParams,
    private fb: FormBuilder,
    private _Valid: ValidationService,
    private _boxPopupService: BoxPopupService,
    private XFService:XferService,
    _RTAction: RouteData,
    private fbFunc:FormBuilderFunctions,
    private _user:UserService,
    private _router: Router) {


    this.Action=_RTAction.get('action');
    this.whs_id=this._Func.lstGetItem('whs_id');

    this.checkPermission();

  }

  // Check permission for user using this function page

  private viewXfer;
  private editXfer;
  private allowAccess:boolean=false;
  checkPermission(){

    this.showLoadingOverlay = true;
    this._user.GetPermissionUser().subscribe(
      data => {
        this.showLoadingOverlay = false;
        this.viewXfer = this._user.RequestPermission(data,'createXFER');
        this.editXfer = this._user.RequestPermission(data,'editXFER');

        this.getCustomersByWH();
        /*
         * Action router View detail
         * */
        if(this.Action=='view')
        {
          if(this.viewXfer) {
            this.IsView = true;
            this.TitlePage = 'VIEW XFER TICKET';
            this.allowAccess=true;
            this.geteXferDetail();
          }
          else{
            this.redirectDeny();
          }
        }

        /*
         * Action router Edit
         * */
        if(this.Action=='update')
        {
          if(this.editXfer) {
            this.allowAccess=true;
            this.TitlePage = 'XFER TICKET UPDATE INV';
             this.geteXferDetail();
          }
          else{
            this.redirectDeny();
          }
        }

      },
      err => {
        this.parseError(err);
        this.showLoadingOverlay = false;
      }
    );
  }


  redirectDeny(){
    this._router.parent.navigateByUrl('/deny');
  }

  /***
   *
   * XFER forrm
   * **/
  formBuilder(data={}) {

    if (data) {

    this.XFForm = this.fb.group({
      xfer_ticket_id: [data[0]['xfer_ticket_id'] ? data[0]['xfer_ticket_id'] : ''],
      xfer_ticket_num: [data[0]['xfer_ticket_num'] ? data[0]['xfer_ticket_num'] : ''],
      xfer_tickets: this.xferTicketArray
    });

    for (let i in data) {
      this.creat_xferTicketItem(data[i]);
    }


    for (let i in data) {

      this.xfer_tickets[i]['req_loc'] = {};
      this.xfer_tickets[i]['mez_loc'] = {};
      this.xfer_tickets[i]['req_loc']['checked'] = true;
      this.xfer_tickets[i]['mez_loc']['checked'] = true;

      this.setChecked(this.xfer_tickets[i],'act_req_loc','req_loc');
      this.setChecked(this.xfer_tickets[i],'act_mez_loc','mez_loc');

    }
  }

  }

  /*=============================================================
  * setChecked
  *============================================================= */
  setChecked(item,fieldname1,filesetcheck){
    if (item.value[fieldname1] == item.value[filesetcheck]) {
      item[filesetcheck]['checked'] = true;
    }
    else {
      if (item.value[fieldname1] && (item.value[fieldname1] !== item.value[filesetcheck])) {
        item[filesetcheck]['checked'] = false;
      }
    }
  }


  creat_xferTicketItem(data={}){

    this.xferTicketArray.push(
      new ControlGroup({
        'xfer_dtl_id':new Control(data['xfer_dtl_id']),
        'xfer_hdr_id':new Control(data['xfer_hdr_id']),
        'xfer_hdr_num':new Control(data['xfer_hdr_num']),
        'item_id':new Control(data['item_id']),
        'cus_id':new Control(data['cus_id']),
        'req_loc_id':new Control(data['req_loc_id']),
        'req_loc':new Control(data['req_loc']),
        'act_req_loc_sts': new Control(data['act_req_loc_sts']),
        'act_req_loc_id' :new Control(data['act_req_loc_id']),
        'piece_remain' :new Control(data['piece_remain']),
        'act_piece_qty' :new Control(data['act_piece_qty'],Validators.compose([Validators.required, this._Valid.validateSpace])),
        'act_req_loc':new Control(data['act_req_loc'],Validators.compose([Validators.required, this._Valid.validateSpace])),
        'mez_loc_id':new Control(data['mez_loc_id']),
        'mez_loc':new Control(data['mez_loc']),
        'act_mez_loc_id':new Control(data['act_mez_loc_id']),
        'act_mez_loc':new Control(data['act_mez_loc'],Validators.compose([Validators.required, this._Valid.validateSpace])),
        'act_mez_loc_sts': new Control(data['act_mez_loc_sts']),
      })
    );

  }

  // get list custommer

  private getCustomersByWH () {

    let params='?whs_id=' + this.whs_id + "&limit=10000";
    this.XFService.getCustomersByWH(params)
      .subscribe(
        data => {
          this.Customers = data.data;
        },
        err => {
          this.parseError(err);
        },
        () => {
        }
      );

  }

  private xferId;
  private xferDetailOrigin:Array<any>;
  geteXferDetail(){


    this.xferId=this.params.get('id');
    this.showLoadingOverlay=true;
    let param = '&limit=100';
    this.XFService.geteXferDetail(this.xferId,param).subscribe(
      data => {
        this.showLoadingOverlay=false;
        this.formBuilder(data);
        this.xferDetail=data;
        // save to stogre
        this.xferDetailOrigin=JSON.parse(JSON.stringify(data));
        if(this.IsView){
          this.getEventracking(this.xferDetail[0]['xfer_ticket_num']);
        }
      },
      err => {
        this.showLoadingOverlay=false;
        this.parseError(err);
      },
      () => {}
    );


  }

  autoComplete(key,row,control,fieldname=''){

    let searchbyAPI='searchLoc';
    let searchkey = this._Func.trim(key);

    let controlshap=this.XFForm.controls['xfer_tickets']['controls'][row]['controls'][control];

    // check duplicated with from localtion
    this.checkDuplicatedWithFromLocation();

    /*
    If before has subscribe , then unsubscribe
    * */
    if (controlshap['subscribe']) {
      controlshap['subscribe'].unsubscribe();
    }

    if(!searchkey.length){
      this.setRequiredControl(row,control);
      return false;
    }
    else{

    // search only if cus_id have seleced
    if(controlshap.value){
        let param = "?" + fieldname + "=" + searchkey + "&cus_id=" + this.XFForm.value['xfer_tickets'][row]['cus_id'] + "&limit=20&sort[" + fieldname + "]=asc";

        controlshap['showLoading']=true;


        if (control == 'act_req_loc') {
          searchbyAPI = 'searchLocation';
          param = this.whs_id + "/locations?loc_code=" + key + "&without=ECO&limit=20";
        }

        let enCodeSearchQuery = this._Func.FixEncodeURI(param + '&item_id=' + this.xferDetail[row]['item_id']);

        // do Search
        eval(`controlshap['subscribe']=this.XFService.` + searchbyAPI + `(enCodeSearchQuery).debounceTime(400).distinctUntilChanged().subscribe(
        data => {
          // disable loading icon
          controlshap['showLoading']=false;
          this.autoSearchData[control] = data.data;
          this.checkKeyExist(searchkey,row,this.autoSearchData[control],control,fieldname);
        },
        err =>{
          controlshap['showLoading']=false;
          this.parseError(err);
        },
        () =>{
          console.log('Search Complete');
        }
      );`); // end eval function

    }

    }

  }

  checkDuplicatedWithFromLocation(){

    this.fbFunc.removeErrorsByFieldName(this.xferTicketArray,['act_req_loc'],'duplicated_from');
    for (let actualLocationItem of this.xferTicketArray.controls){
      actualLocationItem = <ControlGroup>actualLocationItem;
      for (let fromLocationItem of this.xferTicketArray.controls){
            if((actualLocationItem.value['act_req_loc'] == fromLocationItem.value['req_loc'])
                && fromLocationItem['req_loc']['checked']){
                (<Control>actualLocationItem['controls']['act_req_loc']).setErrors({duplicated_from:true});
            }
      }
    }

    setTimeout(()=>{
      this.fbFunc.checkDuplicatedKey(this.xferTicketArray,['act_req_loc']);
    })

  }

  private onPageSizeChanged($event)
  {
    this.perPage = parseInt($event.target.value);
  }

  setListItemsSelected(value)
  {
    this.items.forEach((item) => {
      item['selected']=value;
    });
  }

  setRequiredControl(row,control){
    this.setErrorsControl(row,control,null);
    this.setErrorsControl(row,control,{'required':true});
  }

  checkKeyExist(str:string,row,arr:Array<any>,control,fieldname) {
    if (str){
        for (let i in arr) {
          if (str == this._Func.trim(arr[i][fieldname])) {// auto select item if match
            this.selectedLocItem(row,control,arr[i]);
            return;
          }
        }
        this.setErrorsControl(row,control,{'invalid':true});
        return;
    }
  }

  /*======================================
   * Checked button
   * =====================================*/
  ckLoc(row,item,$event,fielname=''){

      if(fielname=='act_req_loc'){
        this.resetValueItem(item,'act_req_loc');
        this.resetValueItem(item,'act_req_loc_id');
        this.resetValueItem(item,'act_req_loc_sts');
        this.updateValueItem(item,'piece_remain',this.xferDetailOrigin[row]['piece_remain']);
        this.checkQunatityAqt(item);
      }
      else{
        this.findSameXferHDRToSetChecked(item,$event);
      }

      setTimeout(()=>{
        this.checkDuplicatedWithFromLocation();
      },200);

  }

  /*======================================
  * Reset using the item instead of the row
  * =====================================*/
  resetValueItem(item,fieldname){
    (<Control>item.controls[fieldname]).updateValue('');
  }

  /*========================================
   * Update value using the item instead of the row
   * =======================================*/
  updateValueItem(item,fieldname,value){
    (<Control>item.controls[fieldname]).updateValue(value);
  }

  /*========================================================
  * Find same xfer id
  * ======================================================*/
  findSameXferHDRToSetChecked(item,checked){

    let xfer=this.xfer_tickets;

    xfer.forEach((itm)=>{

      if(item.value['xfer_hdr_num']==itm.value['xfer_hdr_num']){
        itm['mez_loc']['checked']=checked;
        this.resetValueItem(itm,'act_mez_loc');
        this.resetValueItem(itm,'act_mez_loc_id');
        this.resetValueItem(itm,'act_mez_loc_sts');
      }

    });

  }

  /*========================================================*
   * setErrorsControl
   * *=====================================================*/
  setErrorsControl(row,control,err){
    (<Control>this.XFForm.controls['xfer_tickets']['controls'][row]['controls'][control]).setErrors(err);
  }

  /*========================================================*
   * setErrorsControlItem
   * *=====================================================*/
  setErrorsControlItem(item,filename,errs){
    (<Control>item['controls'][filename]).setErrors(errs);
  }


  resetValueControlLv1(control){
    (<Control>this.XFForm.controls[control]).updateValue('');
  }

  resetValueControlLv2(row,control){
    (<Control>this.XFForm.controls['xfer_tickets']['controls'][row]['controls'][control]).updateValue('');
  }

  updateControl(row,control,value){
    (<Control>this.XFForm.controls['xfer_tickets']['controls'][row]['controls'][control]).updateValue(value);
  }

  selectedLocItem(row,control,item){

    let xfer_hdr_id=this.XFForm.value['xfer_tickets'][row]['xfer_hdr_id'];

    if(control=='act_req_loc'){
      this.updateControl_ATC_REQ_LOC(row,item);
      this.getAvailableQty(row,item['loc_id']);
    }
    else{
      this.updateControlACT_MEZ_LOC(row,item);
      // update other control group
      this.updateGroupXfer(row,xfer_hdr_id,item);
    }
    //this.setErrorsControl(row,control, null);

    let that=this;
    setTimeout(function(){
      that.checkLoc_Status(row,control,item);
      that.checkDuplicatedWithFromLocation();
    }, 100);


  }

  checkLoc_Status(row,control,loc_item)
  {
      if(loc_item['loc_sts_code']=='LK')
      {
        this.setErrorsControl(row,control,{'loc_status_is_locked':true});
      }
      if(loc_item['loc_sts_code']=='IA')
      {
        // show popup change stt
      }
      if(loc_item['loc_sts_code']=='CL')
      {
        this.setErrorsControl(row,control,{'loc_status_is_locked_cycle':true});
      }
  }

  /*=============================
  * Get Available Qty
  * =============================*/

  getAvailableQty(row,loc_id){
    let item=this.xferDetail[row];
    this.XFService.getAvailableQty(item['item_id'],loc_id,item['cus_id']).subscribe(
      data => {
        this.updateControl(row,'piece_remain',data['piece_remain']);
        this.checkQunatityAqt(this.xfer_tickets[row]);
      },
      err => {
        this.parseError(err);
      },
      () => {}
    );

  }

  /*
  * Update Group
  * */
  updateGroupXfer(row,xfer_hdr_id,valueData){

    let xfForm=this.XFForm.value['xfer_tickets'];
    for(let i in xfForm){
      if(xfForm[i]['xfer_hdr_id']==xfer_hdr_id){
        this.updateControlACT_MEZ_LOC(i,valueData);
      }
    }

  }

  /*============================
  * updateControl_ATC_MEZ_LOC
  * ==========================*/
  updateControl_ATC_REQ_LOC(row,item){
    this.updateControl(row,'act_req_loc',item['loc_code']);
    this.updateControl(row,'act_req_loc_id',item['loc_id']);
    this.updateControl(row,'act_req_loc_sts',item['loc_sts_code']);
  }

  /*============================
   * updateControl_ATC_MEZ_LOC
   * ==========================*/
  updateControlACT_MEZ_LOC(row,item){
    this.updateControl(row,'act_mez_loc',item['loc_code']);
    this.updateControl(row,'act_mez_loc_id',item['loc_id']);
    this.updateControl(row,'act_mez_loc_sts',item['loc_sts_code']);
  }

  /*
   * Save
   * */
  private submitForm:boolean=false;
  private printXFER=false;
  Save():void{

    this.scropllTop();

    let data=this.XFForm.value;
    this.submitForm=true;
    this.handelValidForm();

    if(this.XFForm.valid&&this.checkAstleastItemGroupHasValueGreaterThanZero(data['xfer_tickets'])) {

      let data_json = JSON.stringify(data);

        this.xferId=this.params.get('id');
        this.Update(this.xferId,data_json);

    }
    else{
    }


  }



  Update(id,data)
  {

    let n=this;
    this.showLoadingOverlay=true;
    this.XFService.updateXferRecord(id,data).subscribe(
      data => {
        this.showLoadingOverlay=false;
        this.messages = this._Func.Messages('success', this._Func.msg('VR109'));
        setTimeout(function(){
          n._router.parent.navigateByUrl('/xfer');
        }, 600);
      },
      err => {
        this.showLoadingOverlay=false;
        this.parseError(err);
      },
      () => {}
    );

  }


  handelValidForm(){

    let rowItem=this.xfer_tickets;
    for(let i in rowItem){

      if(rowItem[i]['req_loc']['checked']){
         this.setErrorsControl(i,'act_req_loc',null);
      }
      if(rowItem[i]['mez_loc']['checked']){
        this.setErrorsControl(i,'act_mez_loc',null);
      }

    }

  }

  checkInputNumber(evt){
    this._Valid.isNumber(evt);
  }

  /*==============================================================
  * check Actual picked quantity must be less than atual quantity!
  * =============================================================*/
  checkQunatityAqt(item){

      let piece_remain =item.value['piece_remain'] ? item.value['piece_remain'] : 0;
      if (parseInt(item.value['act_piece_qty']) > parseInt(piece_remain)) {
        this.setErrorsControlItem(item, 'act_piece_qty', {invaildAqt: true});
      }
      else {
        //this.setErrorsControlItem(item, 'act_piece_qty', null);
      }
  }

  /*
  * Check at least one item has value greater than 0
  * */

  private checkAstleastItemGroupHasValueGreaterThanZero(listItemGroup:Array<any>){

    let xtickets=JSON.parse(JSON.stringify(listItemGroup));
    let in_xfer_hdr_num=[];

    for(var i=0;i<xtickets.length;i++){


      if(typeof xtickets[i]['checked']=='undefined') {
        let sum_num = 0;
        sum_num = sum_num + parseInt(xtickets[i]['act_piece_qty']);
        let xfer_hdr_num_i = xtickets[i]['xfer_hdr_num'];
        var j = 0;
        for (j = i + 1; j < xtickets.length; j++) {
          let xfer_hdr_num_j = xtickets[j]['xfer_hdr_num'];

          if (typeof xtickets[j]['checked'] == 'undefined' && (xfer_hdr_num_i == xfer_hdr_num_j)) {

            sum_num = sum_num + parseInt(xtickets[j]['act_piece_qty']);
            // stick the flag to item
            xtickets[j]['checked'] = true;


          }
        } // end For child


        if (sum_num == 0) {

          let index = in_xfer_hdr_num.indexOf(xfer_hdr_num_i);
          if (index < 0) {
            in_xfer_hdr_num.push(xfer_hdr_num_i);
          }
        }
      }

    } // end for master


    if(in_xfer_hdr_num.length){

      this.messages = {'status' : 'danger', 'txt' :this._Func.msg('XF009') + in_xfer_hdr_num.join()};
      return false;

    }

    return true;

  }


  // Show error when server die or else
  private parseError (err) {
    this.messages = {'status' : 'danger', 'txt' : this._Func.parseErrorMessageFromServer(err)};
  }

  redirectToList(){
    let n = this;
    this._boxPopupService.showWarningPopup()
      .then(function (dm) {
        if(dm)
          n._router.parent.navigateByUrl('/xfer');
      })
  }

  getEventracking(XferNumber){

    this.XFService.getEventracking(XferNumber).subscribe(
      data => {
        this.evenTracking = data.data;
      },
      err => {
        this.parseError(err);
      },
      () => {}
    );

  }

  scropllTop()
  {
    this._Func.scrollToTop();
  }


}





