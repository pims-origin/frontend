import {Component} from '@angular/core';
import {Router, RouterLink, RouteConfig, ROUTER_DIRECTIVES} from '@angular/router-deprecated';
import {CustomerListComponent} from "./customer-list/customer-list.component";
import {CruCustomerComponent} from "./cru-customer/cru-customer.component";

@Component ({
    selector: 'customer',
    directives: [ROUTER_DIRECTIVES],
    template: `<router-outlet></router-outlet>`,
})

@RouteConfig([
    { path: '/', component:CustomerListComponent  , name: 'Customer', useAsDefault: true},
    { path: '/new', component:CruCustomerComponent  , name: 'Add Customer',data: {Action: 'New'}},
    { path: '/:id', component:CruCustomerComponent , name: 'View Customer',data: {Action: 'View'}},
    { path: '/:id/edit', component:CruCustomerComponent , name: 'Edit Customer',data: {Action: 'Edit'}},

])
export class CustomerComponent {

}
