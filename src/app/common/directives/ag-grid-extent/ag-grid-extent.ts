import {Component,Input,Output,EventEmitter,ElementRef, Injectable} from '@angular/core';
import {Http, Response, Headers} from '@angular/http';
import {ControlGroup,Control} from '@angular/common';
import {API_Config, Functions} from '../../../common/core/load';
import { Router} from '@angular/router-deprecated'
import {AgGridNg2} from 'ag-grid-ng2/main';
import {GridOptions} from 'ag-grid/main';
import 'ag-grid-enterprise/main';
import {DEFAULT_STYLES} from './styles';
declare var jQuery: any;
declare var jsPDF: any;
@Component({
  selector: 'ag-grid-ng2-extent',
  providers:[],
  directives: [AgGridNg2],
  templateUrl: 'ag-grid-extent.html',
  styles:[DEFAULT_STYLES]
})
@Injectable()
export class AgGridExtent {

  private selectedAll:boolean=false;
  private _headerURL;
  private _urlViewDetail:string='';
  private _activeViewDetail:boolean=false;
  private _columnData:any[];
  private fieldNameTable:any[];
  private rowData=null;
  private columnDefs:Array<any>=[];
  private _unCheckall:boolean=false;
  private _dataForRenderTable:any[];
  private columnId;
  private timeoutUpdateHeader;
  private _sortDefault;
  private tableName='#tableList';
  private sortVaribleGolbal;
  private expandTable = false;


  @Input('headerURL') set headerURL(value: string) {
    this._headerURL=value;
  }

  @Input('columnData') set columnData(value: Array<any>) {
    this._columnData=value;
    this.fieldNameTable=this.createTempTableColumData(this._columnData);
    this.createColumnDefs();
  }

  @Input('unCheckall') set unCheckall(value: boolean) {
    this._unCheckall=value;
    if(value===true)
    {
      this.resetSelected();
    }
  }

  @Input('activeViewDetail') set activeViewDetail(value: boolean) {
    this._activeViewDetail=value;
  }

  @Input('sortDefault') set sortDefault(value) {
    if(!this._sortDefault){
      this._sortDefault=value;
    }
  }

  @Input('urlViewDetail') set urlViewDetail(value: string) {
    this._urlViewDetail=value;
  }

  @Input('dataForRenderTable') set dataForRenderTable(value) {

    if(value !== null){
      this._dataForRenderTable=value;
      this.createRowData(this._dataForRenderTable);
      if(typeof this.gridOptions.api!=='undefined'){
        this.resetSelected();
      }
    }else{
      // set null to show loading
      this.rowData=null;
    }

  }

  @Output() dataSelected  = new EventEmitter();

  @Output() sortData  = new EventEmitter();

  private gridOptions:GridOptions;
  private inputClipboard;
  constructor(
    private _Func: Functions,
    private _router:Router,
    private elementRef: ElementRef,
    private _http: Http){
    var that = this;
    this.gridOptions = <GridOptions>{
      onClipboardPaste: (event) => {
        try {
          var focusCell = that.gridOptions.api.getFocusedCell();
          var cellValue = that.rowData[focusCell['rowIndex']][focusCell['column']['colId']].toString();
          var jValue = jQuery(cellValue);
          if(jValue.length) {
            var selectedNoteText = jValue.hasClass('hover-value') ? jValue.find(' > a').text() : jValue.text();
            if(selectedNoteText == '') selectedNoteText = ' ';
            if(!that.inputClipboard) {
              that.inputClipboard = jQuery('<textarea class="input-clipboard"></textarea>').appendTo('body');
            }
            that.inputClipboard.text(selectedNoteText);
            that.inputClipboard.select();
            document.execCommand('copy');
          }
        }
        catch(err) {}
      }
    };
  }
  private initDragDrop(tableName) {
    var that = this,
      headerCols = jQuery(tableName + ' .ag-header-cell'),
      fixedColEdge = headerCols.filter('.column_fixed').last(),
      fixedIndexEdge = headerCols.index(fixedColEdge),
      lastIndex = headerCols.length - 1,
      drag = false,
      dragElm, dragIdx, leftPos, rightPos, lastMousePos;
    var table = jQuery(tableName);
    table.off('mousedown', '.ag-header-cell-label').on('mousedown', '.ag-header-cell-label', function(event) {
      event.preventDefault();
      dragElm = jQuery(this);
      dragIdx = table.find('.ag-header-cell-label').index(dragElm);
      if(dragIdx > fixedIndexEdge) {
        drag = true;
        leftPos = dragElm.offset().left;
        rightPos = leftPos + dragElm.width();
        lastMousePos = event.pageX;
      }
    }).off('mousemove').on('mousemove', function(event) {
      event.preventDefault();
      if(drag) {
        var currPos = event.pageX;
        if(currPos > lastMousePos && currPos > rightPos && dragIdx < lastIndex) {
          that.gridOptions.columnApi.moveColumn(dragIdx, dragIdx+1);
          dragIdx++;
          dragElm = jQuery(tableName + ' .ag-header-cell-label').eq(dragIdx);
          leftPos = dragElm.offset().left;
          rightPos = leftPos + dragElm.width();
        }

        if(currPos < lastMousePos && currPos < leftPos && dragIdx > fixedIndexEdge + 1) {
          that.gridOptions.columnApi.moveColumn(dragIdx, dragIdx-1);
          dragIdx--;
          dragElm = jQuery(tableName + ' .ag-header-cell-label').eq(dragIdx);
          leftPos = dragElm.offset().left;
          rightPos = leftPos + dragElm.width();
        }
        lastMousePos = currPos;
      }
    }).off('mouseup mouseleave').on('mouseup mouseleave', function() {
      drag = false;
    });
  }

  private createColumnDefs() {
    var that = this;

    var tmp_list_table;

    this._http.get(this._headerURL,{ headers: this._Func.AuthHeader()})
      .map((res) => res.json())
      .subscribe(
        data => {
          if(data && data.data) {
            tmp_list_table = JSON.parse(data.data);
          }
        },
        err => {
          this.prcolumnDefs(tmp_list_table);
        },
        () => {
          this.prcolumnDefs(tmp_list_table);
        }
      )
  }

  private theFirstAccess=false;
  prcolumnDefs(tmp_list_table)
  {
    let that=this;

    if(!tmp_list_table || !this.checkDiffcolumnData(this._columnData,tmp_list_table)) {
      this.theFirstAccess=true;
      tmp_list_table=this._columnData;
    }
    this.columnDefs = [];


    for (var column in tmp_list_table) {

      let columnData=tmp_list_table[column];
      let headerName=this._columnData[column]['title']!=='undefined' ? this._columnData[column]['title'] : '';
      let pin= typeof this._columnData[column]['pin']!=='undefined' ? typeof this._columnData[column]['pin'] : false;

      if(typeof this._columnData[column]['attr']!=='undefined'){
        this.columnDefs.push({
          headerName: "",
          suppressMovable: true,
          field:column,
          width: columnData['width'],
          sortingOrder: [null],
          headerCellTemplate:this.createCheckBoxAll(),
          checkboxSelection: true, suppressMenu: false, suppressSizeToFit: true,
          cellStyle : this._columnData[column]['cellStyle']
        })
      } else {

        this.columnDefs.push({
          headerName: headerName, suppressMovable: true,
          field: column,
          headerTooltip : headerName,
          hide: typeof this._columnData[column]['hidden']!=='undefined',
          editable: false,
          headerClass: this.headerClassAdd(column),
          width: columnData['width'],
          cellClass: typeof this._columnData[column]['pin']!=='undefined' ? 'row_column_fixed' : '',
          cellStyle : this._columnData[column]['cellStyle']
        });

      }


    }
    setTimeout(function() {
      that.initDragDrop('#tableList');
      that.customSortHeader();
      if(that.theFirstAccess)
      {
        that.changeSizeTable();
      }
    },1000)



  }

  private headerClassAdd(columnHeader){
    let classStr='';
    if(typeof  this._columnData[columnHeader]['pin']!=='undefined'){
      classStr='column_fixed';
    }
    if(typeof this._columnData[columnHeader]['sort']=='undefined'){
      classStr+=' no-sort';
      //console.log('merge string ',classStr );
    }
    //console.log('classStr for field ',columnHeader,  classStr , this._columnData[columnHeader]['sort']);
    return classStr;
  }
  private  createCheckBoxAll(checkbox=false){

    let that=this;
    var eCell = document.createElement('div');


    var agResizeBar = document.createElement('div');
    agResizeBar.setAttribute('id','agResizeBar');
    agResizeBar.className="ag-header-cell-resize";

    var agHeaderCellLabel = document.createElement('div');
    agHeaderCellLabel.setAttribute('id','agHeaderCellLabel');
    agHeaderCellLabel.className="ag-header-cell-label";

    var agText = document.createElement('div');
    agText.setAttribute('id','agText');
    agText.className="ag-header-cell-text";

    var headerCheckbox = document.createElement('input');
    headerCheckbox.type='checkbox';
    headerCheckbox.setAttribute("id", "selectedAll");
    headerCheckbox.addEventListener('click', function(e) {
      that.selectAll(e);
    });

    agHeaderCellLabel.appendChild(headerCheckbox);
    eCell.appendChild(agHeaderCellLabel);
    eCell.appendChild(agResizeBar);
    eCell.className="ag-header-cell ag-header-cell-sorted-none no-sort column_fixed";
    return eCell;
  }


  private sortTemp={};
  customSortHeader(){

      let table=jQuery(this.tableName);

      let n=this;
      let sortData;
      table.find('.ag-header-cell:not(.no-sort) .ag-header-cell-label').click(function(e){

          let parentClass=jQuery(this).parent('.ag-header-cell');
          let key=parentClass.attr('colid');

          n.resetSortKey(key);


          if(typeof  n.sortTemp[key]=='undefined'){
            n.sortTemp[key]='asc';
            parentClass.addClass('asc').removeClass('desc none');
          }else{
            if(n.sortTemp[key]=='asc'){
              n.sortTemp[key]='desc';
              parentClass.addClass('desc').removeClass('asc none');
            }
            else {
              if(n.sortTemp[key]=='desc'){
                n.sortTemp[key]='none';
                parentClass.addClass('none').removeClass('asc desc');
              }
              else{
                n.sortTemp[key]='asc';
                parentClass.addClass('asc').removeClass('none desc');
              }
            }
          }

        if(n.sortTemp[key]=='none'){
          sortData= n._sortDefault;
        }
        else{
          sortData= {fieldname:  key , sort:  n.sortTemp[key] };
        }
        n.sortVaribleGolbal=sortData;
         n.sortData.emit(sortData);
      });

  }

  resortAfterMoveColumn(){
    if(this.sortVaribleGolbal){
      let sortData=this.sortVaribleGolbal;
      let table=jQuery(this.tableName);
      table.find('.ag-header-cell[colid='+sortData['fieldname']+']').addClass(sortData['sort']);
    }

  }

  resetSortKey(excerptKey){

    Object.keys(this.sortTemp).map(
      key =>{
        if(excerptKey!==key){
          this.sortTemp[key]='none';
        }
      }
    );

    var table=jQuery('#tableList');
    table.find('.ag-header-cell:not(.ag-header-cell-sorted-none)').each(function(e){

        jQuery(this).removeClass('asc desc').addClass('none');

    });

  }


  private createTempTableColumData(data)
  {
    let tmp_list_table=[];
    Object.keys(data).map(
      key =>{
          tmp_list_table.push(key);
      }
    );

    return tmp_list_table;
  }

  private convectObjectvaluetoArray(data)
  {
    let headerColumnData=[];
    Object.keys(data).map(
      key =>{
        headerColumnData.push(data[key]);
      }
    );
    return headerColumnData;
  }

  // Format data for ag-grid
  private createRowData(data) {
    let tempTable=this.fieldNameTable;
    var rowData: any[] = [];


    if (typeof data != 'undefined') {

      data = data;
      for (var i = 0; i < data.length; i++) {

        let obj={};

        for(let j in tempTable) {

          let itemOriginObjectData=this._columnData[tempTable[j]];
          let value_field;
          // get column Id
          if(typeof itemOriginObjectData['id']!=='undefined')
          {
            this.columnId=tempTable[j];
          }

          // settup value_field
          if(typeof itemOriginObjectData['parent']!=='undefined'){
            if(typeof itemOriginObjectData['groupfield']!=='undefined'){
              let dataParent=itemOriginObjectData['groupfield'];
              value_field=data[i][itemOriginObjectData['parent']][dataParent[0]]+' '+data[i][itemOriginObjectData['parent']][dataParent[1]];
            }else{
              value_field=data[i][itemOriginObjectData['parent']]!==null ? data[i][itemOriginObjectData['parent']][tempTable[j]] : '';
            }
          }else{
            value_field=data[i][tempTable[j]]==null ? '' : data[i][tempTable[j]];
          }

          // check has url
          if(typeof itemOriginObjectData['url']!=='undefined')
          {
            let id=itemOriginObjectData['field_get_id'];
            value_field=`<a title="`+value_field+`" href="`+itemOriginObjectData['url']+data[i][id]+`">`+ value_field +`</a>`;
          }

          obj[tempTable[j]] = value_field;
        }
        rowData.push(obj);
      }
    }
    this.rowData = rowData;

    // fixed bug title
    let that=this;
    setTimeout(function() {
      that.setTitleforEllipsisText();
    },1000)


  }

  private checkDiffcolumnData(columnData:any[],dataHeaderServe:any[])
  {
    // arr1 is _columnData
    // arr2 is get from serve ver_table

      /*
      * Check key not undefined
      * */
    for(let key in columnData){
      if(typeof columnData[key]['ver_table']!=='undefined'){
        // start check key on serve
        let key_sv=typeof dataHeaderServe[key]!=='undefined' ? dataHeaderServe[key] : 'undefined';
        if(key_sv!=='undefined'){
            // if key_sv is not undefined
          if(key_sv['ver_table']==columnData[key]['ver_table']){
            //console.log('compare key ', key_sv['ver_table'],' == ',columnData[key]['ver_table']);
            return true
          }
        }
      }
    }

    return false;
  }

  selectAll($event)
  {
    if($event.target.checked)
    {
      this.gridOptions.api.selectAll();
    }else{
      this.gridOptions.api.deselectAll();
    }
    this.selectedAll=$event.target.checked;
    this.checkAllCheckBox(this.selectedAll);
  }

  private listDataSelected:Array<any>=[];
  rowSelected(event)
  {
    let item = event.node.data;
    let el=this.listDataSelected.indexOf(item);
    if(event.node.selected){
      if(el<0){
        this.listDataSelected.push(item)
      }
      if(this.listDataSelected.length==this._dataForRenderTable.length){
        this.selectedAll=true;
        this.checkAllCheckBox(true);
        this.gridOptions.api.selectAll();
      }
    }else{
      this.selectedAll=false;
      this.checkAllCheckBox(false);
      if(el>=0){
        this.listDataSelected.splice(el,1);
      }
    }
    //console.log('checked item ', item);
    this.dataSelected.emit(this.listDataSelected);
  }


  checkAllCheckBox(value){
    jQuery("#selectedAll").prop("checked", value);
  }


  private updateHeader() {

    var table_state = this.gridOptions.columnApi.getColumnState();
    if(this.timeoutUpdateHeader) {
      clearTimeout(this.timeoutUpdateHeader);
    }
    this.timeoutUpdateHeader = setTimeout(() => {

      let obj = {};

      table_state.forEach((item)=>{
        // get ver
        let ver=typeof this._columnData[item['colId']]['ver_table']!=='undefined' ? this._columnData[item['colId']]['ver_table'] : '';
        obj[item['colId']]={};
        obj[item['colId']]['width']=item['width'];
        if(ver){
          obj[item['colId']]['ver_table']=ver;
        }
      });

      this._http.put(this._headerURL, JSON.stringify({'value': JSON.stringify(obj)}), { headers: this._Func.AuthHeaderPostJson() })
        .map((res) => res.json().data).subscribe(
        data => {
        },
        err => {
        }
      );
    }, 400);

  }

  private checkCheckAll() {
    if(this.listDataSelected.length==this.rowData.length&&this.rowData.length>0){
      this.checkAllCheckBox(true);
    }
  }

  private onColumnMoved($event) {
    this.updateHeader();
    this.checkCheckAll();
    this.customSortHeader();
    this.resortAfterMoveColumn();
  }

  private onColumnResized($event) {
    this.updateHeader();
    this.checkCheckAll();
  }

  resetSelected(){
    //console.log('reset check');
    this.gridOptions.api.deselectAll();
    this.selectedAll=false;
    this.checkAllCheckBox(false);
    this.listDataSelected=[];
  }

  changeSizeTable(){
      // this.autoSizeAll();
      this.sizeToFit();
      this.updateHeader();
  }

  private sizeToFit() {
    this.gridOptions.api.sizeColumnsToFit();
  }

  private autoSizeAll() {
    this.gridOptions.columnApi.autoSizeColumns(this.fieldNameTable);
  }

  private setTitleforEllipsisText(){

    let that=this;
    let table=jQuery(this.tableName);
    table.find('.ag-cell').each(function(i,e){
      var cell = jQuery(this);
      if(cell.attr('id') != 'ck') {
        var children = cell.children();
        if(!children.length) {
          let text = cell.text().replace(/"/g, '&#34;').replace(/</g, '&lt;');
          cell.html(`<span title="${text}">${text}</span>`);
        }
        else {
          if(children[0].tagName == 'A') {
            let text = children.text().replace(/"/g, '&#34;').replace(/</g, '&lt;');
            children.html(`<span title="${text}">${text}</span>`);
          }
        }
      }
    });
  }


}
