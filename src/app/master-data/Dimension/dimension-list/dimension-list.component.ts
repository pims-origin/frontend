import {Component} from '@angular/core';
import {Http,} from '@angular/http';
import { Router } from '@angular/router-deprecated';
import {API_Config, UserService, Functions} from '../../../common/core/load';
import {DimensionServices} from "../dimension-services"
import {AgGridExtent} from '../../../common/directives/directives'
import { FORM_DIRECTIVES,ControlGroup,FormBuilder, Control,Validators} from '@angular/common';
import {WMSPagination,WMSBreadcrumb,WMSMessages, ReportFileExporter} from '../../../common/directives/directives';

import 'ag-grid-enterprise/main';
import { ValidationService } from '../../../common/core/validator';
declare var jQuery: any;
declare var saveAs: any;

@Component ({
    selector: 'dimension-list',
    providers:[DimensionServices, ValidationService],
    directives:[AgGridExtent,WMSBreadcrumb,WMSMessages,WMSPagination,ReportFileExporter],
    templateUrl: 'dimension-list.component.html',

})

export class DimensionListComponent {

    private headerURL = this.API.API_User_Metas+'/dim';
    private columnData = {
        dms_id:{
            attr:'checkbox',
            id:true,
            title:'#',
            width:25,
            pin:true,
            ver_table:'8'
        },
        pack_ref_id:{
            title:'ID',
            url:'#/master-data/dimension/',
            field_get_id:'pack_ref_id',
            width:50,
            pin:true,
        },
        length:{
            title:'Length',
            width:70,
            sort:true
        },
        width:{
            title:'Width',
            width:70,
            sort:true
        },
        height:{
            title:'Height',
            width:70,
            sort:true
        },
        dimension:{
            title:'Dimension (LxWxH)',
            width:120,
            sort:true
        },
        pack_type:{
            title:'Type',
            width:120,
            hidden:true
        },
        pack_name:{
            title:'Type',
            width:150,
        }
    };
    private DimesionList:Array<any>=[];
    //pagination
    private Pagination;
    private perPage = 20;
    private totalCount = 0;
    private tmpPageCount;
    private pageCount=0;
    private ItemOnPage=0;
    private currentPage = 1;
    private numLinks = 3;
    private exportAPIUrl: string = '';
    private fileName: string = '';

    private sortData={fieldname:'pack_ref_id',sort:'desc'};
    private showLoadingOverlay:boolean=false;
    private messages;
    private errMessages=[];
    private packTypeList:any = [];
    //search
    private authHeader = this._Func.AuthHeader();
    private isSearch:boolean=false;
    searchForm:ControlGroup;
    //router edit
    private ListSelectedItem:Array<any>=[];

    private printForm: ControlGroup;
    private showForm = false;
    constructor(
        private dmsServices: DimensionServices,
        private API:API_Config,
        private _Func: Functions,
        private fb:FormBuilder,
        private _http: Http,
        private _router: Router,
        private _Valid: ValidationService,
        private _user:UserService
    ){
        this.getDimensionList();
        this.buildSearchForm();
        this.getPackType();
        this.checkPermission();
    }
    //search
    private  searchParam;
    Search(){
        this.isSearch=true;
        let data=this.searchForm.value;
        this.searchParam="&length="+data['length']+"&width="+encodeURIComponent(data['width'])+"&height="
            +encodeURIComponent(data['height'])+"&pack_type=" +encodeURIComponent(data['pack_type']);
        this.getDimensionList();
    }
    buildSearchForm()
    {
        this.searchForm =this.fb.group({
            length: [''],
            width: [''],
            height: [''],
            pack_type:[''],
            pack_name:['']
        });
    }
    ResetSearch()
    {
        this.isSearch=false; // remove action search
        this._Func.ResetForm(this.searchForm);
        this.getDimensionList(this.Pagination['current_page']);
    }
    
    private getPackType(){
        let params = "";
        this._http.get(this.API.API_Packtype+ params, {headers: this.authHeader
        })
            .map(res => res.json())
            .subscribe(
                data => {
                    this.packTypeList = data['pack_types'];
                },
                err => {
                },
                () => {}
            );
    }
    
    private updateMessages(messages) {
        this.messages = messages;
    }

    private createNameFile() {
        let today = new Date();
        let monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
        "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
        ];
        let day = today.getDate();
        let month = monthNames[today.getMonth()];
        let year = today.getFullYear().toString().substr(2,2);
        this.fileName = 'CartonDimension _'+ month + day + year +'.csv';
    }

    private getDimensionList(page=null){
        let param="?sort["+this.sortData['fieldname']+"]="+this.sortData['sort']+"&page="+page+"&limit="+this.perPage;
        //search
        if(this.isSearch){
            param=param+this.searchParam;
        }
        this.createNameFile();
        this.exportAPIUrl = this.dmsServices.getDimensionlistURL() + param + '&export=1';

        this.showLoadingOverlay=true;
        this.dmsServices.getDimensionlist(param).subscribe(
            data => {
                this.showLoadingOverlay = false;
                this.DimesionList = this._Func.formatData(data.data);
                //pagin function
                if(data['meta']){
                    this.Pagination=data['meta']['pagination'];
                    this.Pagination['numLinks']=3;
                    this.Pagination['tmpPageCount']=this._Func.Pagination(this.Pagination);
                }
            },
            err => {
                this.parseError(err);
                this.showLoadingOverlay=false;
            },
            () => {}
        );

        //test data without API
        // this.DimesionList=this.dmsServices.getDimensionlist(param);

    }

    private onPageSizeChanged($event){
        this.perPage=$event.target.value;
        if((this.Pagination['current_page'] - 1) * this.perPage >= this.Pagination['total']) {
            this.Pagination['current_page'] = 1;
        }
        this.getDimensionList(this.Pagination['current_page']);
    }
    Edit() {
        // console.log('list:',this.ListSelectedItem[0]);
        if (this.ListSelectedItem.length > 1) {
            this.messages = this._Func.Messages('danger',this._Func.msg('WMS002'));
            this.scolltoTop();
        }
        else {
            if(this.ListSelectedItem.length)
            {
                this._router.navigateByUrl('/master-data/dimension/' + jQuery(this.ListSelectedItem[0]['pack_ref_id']).attr('title') + '/edit');
            }
            else{
                this.messages = this._Func.Messages('danger',this._Func.msg('WMS001'));
                this.scolltoTop();
            }
        }
    }
    redirectDeny(){
        this._router.parent.navigateByUrl('/deny');
    }
    private createDimension;
    private viewDimension;
    private editDimension;
    private allowAccess:boolean=false;
    private checkPermission() {
        this.showLoadingOverlay = true;
        this._user.GetPermissionUser().subscribe(
            data => {
                this.createDimension = this._user.RequestPermission(data, 'createCartonDimension');
                this.viewDimension = this._user.RequestPermission(data,'viewCartonDimension');
                this.editDimension=this._user.RequestPermission(data,'editCartonDimension');
                /* Check orther permission if View allow */
                if(!this.viewDimension) {
                    this.redirectDeny();
                }
                else {
                    this.allowAccess=true;
                    this.getDimensionList();
                }
            },
            err => {
                this.messages=this._Func.Messages('danger', this._Func.parseErrorMessageFromServer(err));
                this.showLoadingOverlay = false;
            }
        );
    }
    scolltoTop(){
        jQuery('html, body').animate({
            scrollTop: 0
        }, 700);
    }
    // Show error when server die or else
    private parseError(err){
        err = err.json();
        this.messages = this._Func.Messages('danger',err.errors.message);
    }

//    pagination 
    private filterList(num) {
        this.currentPage = num;
        this.getDimensionList(num);
    }

    
    private printDimension() {
        this.showForm = false;
        if (this.ListSelectedItem.length > 1) {
            this.messages = this._Func.Messages('danger',this._Func.msg('PRI002'));
        }
        else {
            if(this.ListSelectedItem.length)
            {
                setTimeout(() => {
                    jQuery('#print-dimension-barcode').modal('show');
                    this.buildPrintForm();
                }, 0);
            }
            else{
              this.messages = this._Func.Messages('danger',this._Func.msg('PRI001'));
            }
        }
    }

    printDimensionBarcode() {
        const that = this;
        if(this.printForm.valid) {      
            const qty = parseInt(this.printForm.value['qty']);
            let api = that.API.API_URL_ROOT + '/core/ecom-orders/v1/carton-dimention/print-barcode';
            const param = {
                'quantity': qty,
                'pack_ref_id': Number(jQuery(this.ListSelectedItem[0]['pack_ref_id']).attr('title'))
            }
            try {
                that.showLoadingOverlay = true;
                let xhr = new XMLHttpRequest();
                xhr.open("POST", api , true);
                xhr.setRequestHeader("Authorization", 'Bearer ' + that._Func.getToken());
                xhr.setRequestHeader('Content-Type', 'application/json');
                xhr.responseType = 'blob';
                xhr.onreadystatechange = function () {
                    if (xhr.readyState == 2) {
                        if (xhr.status == 200) {
                            xhr.responseType = "blob";
                        } else {
                            xhr.responseType = "text";
                        }
                    }
        
                    if (xhr.readyState === 4) {
                        if(xhr.status === 200) {
                            const fileName = `Dimension-Barcode`;
                            const blob = new Blob([this.response], {type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'});
                            saveAs.saveAs(blob, fileName + '.pdf');
                        } else {
                            let errMsg = '';
                            if (xhr.response) {
                                try {
                                    var res = JSON.parse(xhr.response);
                                    errMsg = res.errors && res.errors.message ? res.errors.message : xhr.statusText;
                                } catch(err) {errMsg = xhr.statusText}
                            } else {
                                errMsg = xhr.statusText;
                            }
                            if (!errMsg) {
                                errMsg = that._Func.msg('VR100');
                            }
                            that.messages = that._Func.Messages('danger',errMsg);
                        }
                        jQuery('#print-dimension-barcode').modal('hide');
                        that.showForm = false;
                        that.showLoadingOverlay = false;
                    }
                }
                xhr.send(JSON.stringify(param));
            } catch(err) {
                that.messages = that._Func.Messages('danger',that._Func.msg('VR100'));
                that.showLoadingOverlay = false;
            }
        }
    }

    buildPrintForm() {
        this.printForm = this.fb.group({
            qty: ['', Validators.required]
        });
        
        this.showForm = true;
    }
    
    checkInputNumber(evt, int=true) {
        this._Valid.isNumber(evt, int);
    }

}