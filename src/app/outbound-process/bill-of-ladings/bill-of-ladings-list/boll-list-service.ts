import {Component, Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {API_Config, Functions} from '../../../common/core/load';
import {AsyncPipe} from "@angular/common";

@Injectable()
export class BillOfLadingListService {

    public AuthHeader = this._Func.AuthHeader();
    public AuthHeaderPost = this._Func.AuthHeaderPost();

    constructor(private _Func:Functions,
                private _API:API_Config,
                private http:Http) {
    }

    public getBillOfLadingList($param) {
        return this.http.get(this._API.API_ORDERS_ORG + '/bol-list' + $param, {headers: this.AuthHeader})
            .map(res => res.json());
    }

    public getCustomersByWH($params) {
        return this.http.get(this._API.API_CUSTOMER_MASTER + '/customer-warehouses' + $params + '&sort[cus_name]=asc', {headers: this.AuthHeader})
            .map(res => res.json());
    }

    public getFullAddressList($params) {
        return this.http.get(this._API.API_ORDERS_CORE + '/bol/address'+$params, {headers: this.AuthHeader})
            .map((res:Response) => res.json());
    }

    public getCommodityDescList() {
        return this.http.get(this._API.API_ORDERS_CORE + '/carrier/commodityList', {headers: this.AuthHeader})
            .map((res:Response) => res.json());
    }

    public getShipMethod() {
        return this.http.get(this._API.API_ORDERS_CORE + '/carrier/show-status-drop-down?objects=shipment-method', {headers: this.AuthHeader})
            .map((res:Response) => res.json());
    }

    public getListCustomersByUser($params) {
        return  this.http.get(this._API.API_Customer_Warehouses + $params + '&sort[cus_name]=asc' , {headers: this.AuthHeader})
            .map(res => res.json());
    }



}
