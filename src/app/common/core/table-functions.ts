
import {Injectable} from '@angular/core';
import {API_Config} from './API_Config';
import {Router} from '@angular/router-deprecated';
import {Functions} from './functions';
import {DateFunctions} from './functions/date-functions';
import {UserService} from '../users/users.service';

declare var jQuery: any;

@Injectable()

export class TableFunctions
{
    constructor(
        private _api: API_Config,
        private _func: Functions,
        private _dateFunctions: DateFunctions,
        private _router: Router,
        private _user: UserService
    )
    {

    }

    //**************************************************************************

    public permissionCall(callObject)
    {
        callObject.tableComponent = callObject;

        this.setUnsortableColumns(callObject);
        this.setSearchFields(callObject);

        let headerURL = this._api.API_User_Metas + '/' +
                callObject.get('headerUrlSuffix');

        callObject.set('headerURL', headerURL);

        let customCall = typeof callObject.getTableData === 'function';

        this.callbackCall([
            {
                tableComponent: this,
                callback: 'checkPermission',
                params: {
                    tableComponent: callObject,
                    page: callObject.permissionPage
                }
            },
            {
                tableComponent: customCall ? callObject : this,
                callback: 'getTableData',
                params: {
                    tableComponent: callObject,
                    firstCall: true,
                }
            }
        ]);
    }

    //**************************************************************************

    public getTableData(data)
    {
        var tableComponent = data.tableComponent,
            page = data.page || 1,
            callback = data.hasOwnProperty('callback') ? data.callback : '',
            autocomplete = data.hasOwnProperty('autocomplete') ?
                    data.autocomplete : false;

        if (data.hasOwnProperty('firstCall') && data.firstCall
         && tableComponent.hasOwnProperty('searches')
         && tableComponent.get('searches').length) {

            this.getDropDowns({
                tableComponent: tableComponent,
                sourceInfo: tableComponent.get('searches')
            });
        }

        tableComponent.set('showLoadingOverlay', true);

        let searchParams = tableComponent.get('searchParams'),
            controlSearch = tableComponent.get('controlSearch'),
            searches = tableComponent.hasOwnProperty('searches') ?
                    tableComponent.get('searches') : [],
            objSort = tableComponent.hasOwnProperty('objSort') ?
                tableComponent.get('objSort') : null;

        var params = '?page=' + page + '&limit=' + tableComponent.get('perPage'),
            sortType = objSort ? objSort.sort_type : null,
            sortField = objSort ? objSort.sort_field : null;

        params += sortType === null || sortType == 'none' ?
            '&sort[created_at]=desc' : '&sort[' + sortField + ']=' + sortType;

        params += searchParams ? '&' + searchParams : '';
        params += controlSearch ? '&' + controlSearch : '';

        let tableService = tableComponent.get('_tableService'),
            url = tableComponent.get('url');

        let service = typeof tableService.getTableData == 'function' ?
                tableService.getTableData(params, searches) :
                tableService.get(url + params);
        tableComponent.set('currentURL', url + params);
        service
            .subscribe(
                data => {
                    if (autocomplete) {
                        this.setAutcomplete(tableComponent, autocomplete, data);
                    } else {
                        this.processTableData(data, tableComponent, callback);
                    }
                },
                err => {
                    tableComponent.set('messages', this.parseError(err));
                },
                () => {
                    tableComponent.set('showLoadingOverlay', false);
                }
            );
    }

    //**************************************************************************

    public processTableData(data, tableComponent, callback)
    {
        let skipExport = tableComponent.hasOwnProperty('skipExport') ?
                tableComponent.get('skipExport') : false;

        let results = this.getColumnData(
                data, tableComponent.get('headerDef'), skipExport
            ),
            paginationValue = this.initPagination(
                data, tableComponent.get('Pagination'), tableComponent.get('numLinks')
            );

        tableComponent.set('Pagination', paginationValue);
        tableComponent.set('dataExport', results.dataExport);
        tableComponent.set('rowData', results.rowData);
        tableComponent.set('originalData', results.rowData);

        if (typeof tableComponent.createRowData == 'function') {
            tableComponent.set('rowData', tableComponent.createRowData(data.data));
        }

        if (callback && typeof tableComponent.get(callback) == 'function') {
            tableComponent.get(callback)(data.data);
        }
    }

    //**************************************************************************

    public checkPermission(data, callbacks=[])
    {
        let tableComponent = data.tableComponent,
            page = data.page;

        this._user.GetPermissionUser().subscribe(
            data => {

//                let flagView = this._user.RequestPermission(data, page);
let flagView = true;
                if (flagView) {
                    this.callbackCall(callbacks);
                } else {
                    this._router.parent.navigateByUrl('/deny');
                }
            },
            err => {

                let errMgs = tableComponent._func.parseErrorMessageFromServer(err);

                this.showMessage(tableComponent, 'danger', errMgs);
            }
        );
    }

    //**************************************************************************

    public showMessage(tableComponent, msgStatus, msg)
    {
        tableComponent.set('messages', {
            'status': msgStatus,
            'txt': msg
        });

        jQuery(window).scrollTop(0);
    }

    //**************************************************************************

    private getColumnData(tableData, headerDef, skipExport=false)
    {
        var results = {
            dataExport: [],
            rowData: []
        };

        if (typeof tableData.data != 'undefined') {

            var data = this._func.formatData(tableData.data);

            for (let row = 0; row < data.length; row++) {
                // loop through row columns
                let exportData = [],
                    rowValues = {};

                for (let prop in headerDef) {
                    // loop through row columns
                    if (! headerDef.hasOwnProperty(prop)
                     || ! headerDef[prop].hasOwnProperty('name')) {

                        continue;
                    }

                    let field = headerDef[prop].id;

                    if (! skipExport) {
                        exportData.push(data[row][field]);
                    }

                    rowValues[field] = data[row][field];
                }

                if (! skipExport) {
                    results.dataExport.push(exportData);
                }

                results.rowData.push(rowValues);
            }
        }

        return results;
    }

    //**************************************************************************

    public parseError(err)
    {
        return {
            status : 'danger',
            txt : this._func.parseErrorMessageFromServer(err)
        };
    }

    //**************************************************************************

    public getSearchParams(searches)
    {
        let params = '';

        jQuery.each(searches, function (key, value) {
            if (value) {
                params += '&' + key + '=' + value;
            }
        });

        return params;
    }

    //**************************************************************************

    public initPagination(data, pagination, numLinks: string)
    {
        pagination = data.meta['pagination'];
        pagination['numLinks'] = numLinks;
        pagination['tmpPageCount'] = this._func.Pagination(pagination);

        return pagination;
    }

    //**************************************************************************

    public callbackCall(callbacks: any[])
    {
        if (Object.keys(callbacks).length) {

            let callback = callbacks[0];

            let callbackObject = callback.tableComponent,
                callbackFunction = callback.callback,
                callbackParams = callback.hasOwnProperty('params') ?
                        callback.params : {};

            if (Object.keys(callbacks).length) {
                callbacks.shift();
            }

            callbackObject[callbackFunction](callbackParams, callbacks);
        }
    }

    //**************************************************************************

    public callGetTableData(data)
    {
        let callObject = typeof data.parentComponent.get('getTableData') === 'function' ?
                data.parentComponent : this,
            page = data.hasOwnProperty('page') ? data.page : 1,
            autocomplete = data.hasOwnProperty('autocomplete') ?
                    data.autocomplete : false;

        return callObject.getTableData({
            tableComponent: data.parentComponent,
            page: page,
            autocomplete: autocomplete
        });
    }

    //**************************************************************************

    public getDropDowns(data)
    {
        let tableComponent = data.tableComponent,
            info = data.sourceInfo;

        for (let count=0; count<info.length; count++) {

            let source = info[count];

            if (source.hasOwnProperty('dropdown') && source.dropdown) {
                this.getDropDown({
                    dropdown: source.dropdown,
                    tableComponent: tableComponent,
                    output: info[count].dropdown.source
                });
            }
        }
    }

    //**************************************************************************

    public getDropDown(data)
    {
        let dropdown = data.dropdown,
            tableComponent = data.tableComponent,
            output = data.output;

        let idField = dropdown.idField,
            nameField = dropdown.nameField;

        setTimeout(() => {
            tableComponent.get(dropdown.service).getDropdown()
                .subscribe(
                    data => {
                        for (var key in data) {
                            output.push({
                                key: data[key][idField],
                                value: data[key][nameField]
                            });
                        }
                    },
                    err => {
                        if (tableComponent.hasOwnProperty('messages')) {
                            tableComponent.set('messages', this.parseError(err));
                        }
                    },
                    () => {}
                );
        }, 0);
    }

    //**************************************************************************

    private setUnsortableColumns(callObject)
    {
        for (let count=0; count<callObject.get('unsortableColumns').length; count++) {

            let key = callObject.get('unsortableColumns')[count];

            let id = callObject.get('columnList')[key].id;

            for (let index=1; index<callObject.get('headerDef').length; index++) {

                if (callObject.get('headerDef')[index].id == id) {

                    let headerDef = callObject.get('headerDef');

                    headerDef[index]['unsortable'] = true;
                }
            }
        }
    }

    //**************************************************************************

    private setSearchFields(callObject)
    {
        if (callObject.hasOwnProperty('searchFields')
         && Object.keys(callObject.get('searchFields')).length) {

            for (let count=0; count<callObject.get('searches').length; count++) {

                let key = callObject.get('searches')[count].caption;

                if (callObject.get('searchFields').hasOwnProperty(key)) {

                    let field = callObject.get('searchFields')[key];

                    let searches = callObject.get('searches');

                    searches[count]['field'] = field;
                }
            }
        }
    }

    //**************************************************************************


    public setAutcomplete(tableComponent, autocomplete, data)
    {
        for (let count=0; count<tableComponent.searches.length; count++) {

            let search = tableComponent.get('searches')[count];

            if (search.name == autocomplete) {

                search.autocomplete = data;

                break;
            }
        }
    }

    //**************************************************************************

}
