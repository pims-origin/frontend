import {Component, Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {Functions, API_Config} from '../../../common/core/load';

@Injectable()
export class WavePickServices {

  public AuthHeader = this._Func.AuthHeader();
  public AuthHeaderPost = this._Func.AuthHeaderPost();
  public headerPostJson = this._Func.AuthHeaderPostJson();

  constructor(
    private _Func: Functions, 
    private http: Http, 
    private _api: API_Config) {
  }

  getPickingList(params) {
  	return this.http.get(this._api.API_Wavepick + '/order-picking-list' + params,{ headers: this.AuthHeader })
      .map((res: Response) => res.json());
  }

  getWavepickStatusList() {
    return this.http.get(this._api.API_Wavepick + '/wave-pick-statuses?type=wv_assign',{ headers: this.AuthHeader })
      .map((res: Response) => res.json());
  }
}
