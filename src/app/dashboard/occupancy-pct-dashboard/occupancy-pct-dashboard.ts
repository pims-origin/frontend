import { Component, Input, Output, EventEmitter } from '@angular/core';
import { DecimalPipe } from '@angular/common';

import { WMSPagination } from '../../common/directives/directives';
import { DashboardServices } from '../dashboard.service';
import { Functions } from '../../common/core/load';
import { PaginatePipe, PaginationControlsCmp } from '../../common/directives/directives';
import { WMSNumberFormatter } from '../../common/pipes/wms-number-formatter';
import { KeysPipe } from '../../common/pipes/keys.pipe';

declare var Chart:any;

@Component({
	selector: 'occupancy-pct-dashboard',
	templateUrl: 'occupancy-pct-dashboard.html',
	directives: [WMSPagination, PaginationControlsCmp],
	providers: [DashboardServices],
	pipes: [PaginatePipe, DecimalPipe, WMSNumberFormatter, KeysPipe]
})

export class OccupancyPCTDashboard {
	private listOccupancyPCT = {};
	private whsId:string = '';
	isType: boolean = false;
	curType = '';
	total = 0;
	listOccupancyPCTStatus = [];

	@Input('currentWHSId') set updateWHSId(value:any) {
		this.whsId = value;
		if (value) {
			this.switchMode(this.isType);
		}
	}

	@Output() showLoading = new EventEmitter();

	constructor(private dashboardServices:DashboardServices, private funcs:Functions) {
	}

	private defaultPieChartOpts = {
		responsive: true,
		tooltips: {
			callbacks: {
				label: (tooltipItem, data) => {
					let index = tooltipItem.index;
					return data.labels[index] + ': ' + Number(data.datasets[0].data[index]).toFixed(2) + '%';
				},
			},
		},
		legend: {
			labels: {
				boxWidth: 30,
			}
		},
		layout: {
            padding: {
				top: 5,
			}
		}
	};

	private dataLocatype = [];

	private getLocaType() {
		this.dashboardServices.getLocationTypes().subscribe(
			data => {
				this.dataLocatype = data.data;
			},
			err => {
			}
		);
	}

	arrCustomer = [];
	getCustomers() {
		this.dashboardServices.getCustomers(this.whsId).subscribe(
			data => {
				this.arrCustomer = data.data;
			}
		);
	}

	private changeLocationType($e) {
		let params = '';
		this.curType = $e.target.value;
		if (this.curType != '') {
			params = this.isType ? '?loc_sts_code=' + this.curType : '?loc_type=' + this.curType;
		}

		if (!this.isType) {
			this.getOccupancyPCT(params);
		} else {
			this.getOccupancyPCTStatus(params);
		}
	}

	private changeCustomer(event) {
		let cusId = '';
		if (event.target.value) {
			cusId = event.target.value;
		}

		this.getOccupancyPCTbyCus(cusId);
	}

	getOccupancyPCTbyCus(cusId: string) {
		this.showLoading.emit(true);
		this.dashboardServices.getOccupancyPCTbyCus(cusId).subscribe(
			data => {
				this.showLoading.emit(false);
				this.listOccupancyPCT = data.data;
				this.drawPiesChart();
				this.drawBarChart();
			}
		);
	}

	private getOccupancyPCT(params:string = '') {
		this.showLoading.emit(true);
		this.dashboardServices.getOccupancyPCT(params).subscribe(
			data => {
				this.showLoading.emit(false);
				this.listOccupancyPCT = data.data;
				this.drawPiesChart();
				this.drawBarChart();
			},
			err => {
			}
		);
	}

	private chart:any;
	private drawBarChart() {
		let barChartData = {
			datasets: [
				{
					data: [],
					backgroundColor: [],
					label: ['']
				}
			],
			labels: []
		};

		if (!this.isType) {
			const data = this.listOccupancyPCT;
			barChartData['labels'] = ['Available', 'Locked', 'Inactive', 'Occupied', 'Un-zone'];
			barChartData['datasets'][0]['data'] = [data['avail_loc'], data['locked_loc'], data['inactive_loc'], data['total_pallet'], data['unzone_loc']];
			barChartData['datasets'][0]['backgroundColor'] = ['#008000', '#d91e18', '#ddd', '#36c6d3'];
		} else {
			this.listOccupancyPCTStatus.forEach(item => {
				barChartData['labels'].push(item.loc_type_name);
				barChartData['datasets'][0]['data'].push(item.quantity);
			});
			barChartData['datasets'][0]['backgroundColor'] = ['#36c6d3', '#d97328', '#012147', '#74859a', '#FFB60D'];
		}

		const ctx = document.getElementById('chart-occ');
		if (ctx == null) {
			return true;
		}

		if (this.chart) {
			this.chart.data.datasets[0].data = barChartData['datasets'][0]['data'];
			this.chart.data.datasets[0].backgroundColor = barChartData['datasets'][0]['backgroundColor'];
			this.chart.data.labels = barChartData['labels'];
			this.chart.update();
		} else {
			  this.chart = new Chart(ctx, {
				type: 'bar',
				data: barChartData,
				options: {
					title: {
						display: false
					},
					legend: false,
					tooltips: {
						mode: 'single',
						callbacks: {
							label: (tooltipItem, data) => {
								return ': ' + this.funcs.formatSWISNumber(tooltipItem.yLabel);
							},
						},
					},
					responsive: true,
					scales: {
						yAxes: [{
							stacked: true,
							ticks: {
								beginAtZero: true,
								callback: (value, index, values) => {
									if (value.toString().indexOf(".") !== -1) {
										return this.funcs.formatSWISNumber(value.toFixed(1));
									}
									return this.funcs.formatSWISNumber(value);
								}
							}

						}]
					},
					layout: {
						padding: {
							top: 20,
						}
					}
				}
			});
		}
	}

	private occupancyChart;

	drawPiesChart() {
		let pieChartData = {
			datasets: [
				{
					data: [],
					backgroundColor: [],
					label: 'Dataset 1'
				}
			],
			labels: []
		};
		if (!this.isType) {
			const data = this.listOccupancyPCT;
			pieChartData['labels'] = ['Available', 'Locked', 'Inactive', 'Occupied', 'Un-zone'];
			pieChartData['datasets'][0]['data'] = [data['percent_avail_loc'], data['percent_locked_loc'], data['percent_inactive_loc'], data['percent_total_pallet'], data['percent_unzone_loc']];
			pieChartData['datasets'][0]['backgroundColor'] = ['#008000', '#d91e18', '#ddd', '#36c6d3'];
		} else {
			this.listOccupancyPCTStatus.forEach(item => {
				pieChartData['labels'].push(item.loc_type_name);
				pieChartData['datasets'][0]['data'].push(item.percent);
			});
			pieChartData['datasets'][0]['backgroundColor'] = ['#36c6d3', '#d97328', '#012147', '#74859a', '#FFB60D'];
		}

		const ctx = document.getElementById('occupancy-chart-wrapper');
		if (ctx == null) {
			return true;
		}
		ctx.style.height = "200px";
		if (this.occupancyChart) this.occupancyChart.destroy();
		this.occupancyChart = new Chart(ctx, {
			type: 'pie',
			data: pieChartData,
			options: this.defaultPieChartOpts
		});
	}

	switchMode(val) {
		this.isType = val;
		if (this.isType === true) {
			this.getOccupancyPCTStatus();
		} else {
			this.getLocaType();
			this.getCustomers();
			this.getOccupancyPCT();
		}
	}

	private getOccupancyPCTStatus(params:string = '') {
		this.showLoading.emit(true);
		this.dashboardServices.getOccupancyPCTStatus(params).subscribe(
			data => {
				this.showLoading.emit(false);
				this.listOccupancyPCTStatus = data['data']['items'];
				this.total = data['data']['total'];
				this.dataLocatype = [];
				const obj = data['data']['status'];
				for (let key in obj) {
					this.dataLocatype.push({loc_type_id: key, loc_type_name: obj[key]});
				}
				this.drawPiesChart();
				this.drawBarChart();
			},
			err => {
			}
		);
	}
}
