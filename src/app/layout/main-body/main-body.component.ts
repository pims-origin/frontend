import { Component } from '@angular/core';
import {RouteConfig, RouterLink, Router, RouteParams} from '@angular/router-deprecated';
import {LoggedInRouterOutlet} from "../../authenticate/LoggedInOutlet";



@Component({
    selector: 'main-body',
    directives: [LoggedInRouterOutlet],
    templateUrl: 'main-body.component.html'
})
export class MainBodyComponent {
  
}
