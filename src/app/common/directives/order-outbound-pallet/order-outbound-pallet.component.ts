import {Component, Input, Output, EventEmitter} from '@angular/core';
import {FORM_DIRECTIVES, ControlGroup, Control, ControlArray, FormBuilder, Validators} from '@angular/common';
import {OrderOutBoundPalletServices} from "./order-outbound-pallet.service";
import {Functions} from '../../../common/core/load';
declare var jQuery:any;

@Component({
	selector: 'order-outbound-pallet',
	directives: [FORM_DIRECTIVES],
	providers: [OrderOutBoundPalletServices],
	templateUrl: 'order-outbound-pallet.template.html'
})

export class OrderOutboundPallet {
	private outboundPalletForm:ControlGroup;
	private submitted:boolean = false;
	private items:ControlGroup[] = [];
	private itemsArray:ControlArray = new ControlArray(this.items);
	private pallets:any[] = [];
	private showLoadingLocations:any = {};
	private locations:any[] = [];
	private ordHdrId:string = '';
	private canUpdate:boolean = true;

	@Input('orderHeaderId') set setOrderId(value:string) {
		this.ordHdrId = value;
		this.getOutboundPallets();
	}

	@Input('outPalletTabShow') set checkTabShowing(value:boolean) {
		if (value) {
			this.getOutboundPallets();
			this.resetOutPalletShow.emit(false);
		}
	}

	@Input('orderStatusKey') set setCanUpdate(value:string) {
		if (value === 'SH') {
			this.canUpdate = false;
		}
	}

	// @Input('isFinalBOL') set setUpdate(value:string) {
	// 	if (!value) {
	// 		this.canUpdate = false;
	// 	}
	// }

	@Output() outboundPalletMessages = new EventEmitter();
	@Output() resetOutPalletShow = new EventEmitter();
	@Output() reloadOrder = new EventEmitter();

	constructor(private formBuilder:FormBuilder,
	            private funcs:Functions,
	            private ordOutboundPalletService:OrderOutBoundPalletServices) {
		this.buildForm();
	}

	private buildForm() {
		this.outboundPalletForm = this.formBuilder.group({
			out_pallets: this.itemsArray,
			odr_id: this.ordHdrId
		});
	}

	private buildItem() {
		for (let pallet of this.pallets) {
			this.itemsArray.push(
				new ControlGroup({
					'plt_id': new Control(pallet['plt_id'] ? pallet['plt_id'] : '', Validators.required),
					'loc_id': new Control(pallet['loc_id'] ? pallet['loc_id'] : '', Validators.required),
					'loc_code': new Control(pallet['loc_code'] ? pallet['loc_code'] : '', Validators.required)
				})
			)
		}
		(<Control>this.outboundPalletForm.controls['odr_id']).updateValue(this.ordHdrId);
	}

	private submit() {
		if (this.outboundPalletForm.valid) {
			const dataJson = JSON.stringify(this.outboundPalletForm.value);
	
			this.ordOutboundPalletService.updateOutboundPallets(dataJson).subscribe(
				data => {
					this.getOutboundPallets();
					this.outboundPalletMessages.emit(this.funcs.Messages('success', data));
					this.reloadOrder.emit(true);
				},
				err => {
					this.emitError(err);
				}
			)
		} else {
			this.submitted = true;
		}
		
	}

	private getOutboundPallets() {
		this.ordOutboundPalletService.getOutboundPallets(this.ordHdrId).subscribe(
			data => {
				this.resetItemsArray();
				this.pallets = data;
				this.buildItem();
			},
			err => {
				this.emitError(err);
			},
			() => {
			}
		);
	}

	private loadLocations($event, itemIndex) {
		const params = `?loc_code=${$event}`;
		
		this.showLoadingLocations[itemIndex] = true;
		this.ordOutboundPalletService.getLocations(params).subscribe(
			data => {
				this.locations = data;
				(<Control>this.outboundPalletForm.controls['out_pallets']['controls'][itemIndex]['controls']['loc_id']).updateValue(this.findLocationId($event));
				this.showLoadingLocations[itemIndex] = false;
			},
			err => {
				this.emitError(err);
				this.showLoadingLocations[itemIndex] = false;
			},
			() => {
				this.showLoadingLocations[itemIndex] = false;
			}
		);
	}

	private findLocationId(locCode:string = '') {
		for (let loc of this.locations) {
			if (locCode.trim() === loc['loc_code']) {
				return loc['loc_id'];
			}
		}
		return locCode;
	}

	private resetItemsArray() {
		while (this.itemsArray.controls.length) {
			this.itemsArray.removeAt(0);
		}
	}

	private selectLocation(location, itemIndex) {
		(<Control>this.outboundPalletForm.controls['out_pallets']['controls'][itemIndex]['controls']['loc_code']).updateValue(location['loc_code']);
		(<Control>this.outboundPalletForm.controls['out_pallets']['controls'][itemIndex]['controls']['loc_id']).updateValue(location['loc_id']);
	}

	private emitError(err:any) {
		this.outboundPalletMessages.emit({'status': 'danger', 'txt': this.funcs.parseErrorMessageFromServer(err)});
	}

}
