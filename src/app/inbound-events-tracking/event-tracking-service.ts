import {Component, Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {API_Config, Functions} from '../common/core/load';

@Injectable()
export class EventTrackingService {

  public AuthHeader = this._Func.AuthHeader();
  public AuthHeaderPost = this._Func.AuthHeaderPost();
  private apiService = this._API.API_GOODS_RECEIPT_MASTER+'/eventracking';
  private urlAPICustomerMaster = this._API.API_CUSTOMER_MASTER;

  constructor(private _Func: Functions, private _API: API_Config, private http: Http)
  {
  }

  getListEventTracking(params)
  {
    return this.http.get(this.apiService+params,{ headers: this.AuthHeader })
      .map((res: Response) => res.json());
  }

  searchOwner(params)
  {
    return this.http.get(this.apiService+'/auto-owner'+params,{ headers: this.AuthHeader })
        .map((res: Response) => res.json());
  }


  // Get customers by WH
  public getCustomersByWH ($params) {

    return  this.http.get(this.urlAPICustomerMaster + '/customer-warehouses' + $params + '&sort[cus_name]=asc' , {headers: this.AuthHeader})
      .map(res => res.json());

  }
  public indicatoList ($params) {

    return  this.http.get(this.apiService + '/indicator-drop-downs' + $params + '&sort[cus_name]=asc' , {headers: this.AuthHeader})
        .map(res => res.json());

  }
}
