import {Component} from '@angular/core';
import {Router, RouterLink, RouteConfig, ROUTER_DIRECTIVES} from '@angular/router-deprecated';
import {OrderListComponent} from "./order-list/order-list.component";
import {CRUOrderComponent} from "./cru-order/cru-order.component";



@Component ({
    selector: 'order-management',
    directives: [ROUTER_DIRECTIVES],
    template: `<router-outlet></router-outlet>`,
})

@RouteConfig([
    { path: '/', component: OrderListComponent , name: 'Order List', data:{page:'order'}, useAsDefault: true },
    { path: '/new', component: CRUOrderComponent , name: 'Create Order', data:{action:'new'}},
    { path: '/:id', component: CRUOrderComponent , name: 'View Order', data:{action:'view'}},
    { path: '/:id/edit', component: CRUOrderComponent , name: 'Edit Order', data:{action:'edit'}},
    { path: '/:id/comment', component: CRUOrderComponent , name: 'Edit Order', data:{action:'comment'}}
])
export class OrderManagementComponent {

}
