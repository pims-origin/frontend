import {Component } from '@angular/core';
import { FORM_DIRECTIVES,ControlGroup,FormBuilder, Control ,ControlArray,Validators} from '@angular/common';
import {RouteParams,Router ,RouteData} from '@angular/router-deprecated';
import {UserService, Functions} from '../../../common/core/load';
import {PalletAssignmentServices} from './pallet-assignment-service';
import { ValidationService } from '../../../common/core/validator';
import {WMSBreadcrumb,PaginatePipe,WMSMessages, PaginationControlsCmp, PaginationService} from '../../../common/directives/directives';
import { Http } from '@angular/http';
import {GoBackComponent} from "../../../common/component/goBack.component";
import {BoxPopupService} from "../../../common/popup/box-popup.service";
import {WarningBoxPopup} from "../../../common/popup/warning-box-popup";
import {API_Config} from "../../../common/core/API_Config";
declare var jQuery: any;
declare var saveAs: any;
declare var swal: any;

@Component({
  selector: 'pallet-assignment',
  providers: [PalletAssignmentServices,PaginationService,ValidationService,FormBuilder, BoxPopupService,UserService],
  directives: [FORM_DIRECTIVES,PaginationControlsCmp,WMSMessages,GoBackComponent,WMSBreadcrumb],
  pipes:[PaginatePipe],
  templateUrl: 'pallet-assignment.component.html',
})

export class PalletAssignmentOutComponent {

  public Loading = [];
  private messages;
  private Action;
  private showLoadingOverlay = false;
  private hasAssignPalletPermission;

  private orderIDs;
  private listOrders;
  private pallet_carton_template = {
            pallet_num: '',
            carton_num: '',
            checked: false
          };
  private listErrMsg = {
    'AP1': 'Assigned Cartons must be equal Total!',
    'AP2': 'Packed Cartons of {{listOrderName}} are assigned to pallets successfully!'
  };

  constructor(
    private _http: Http,
    private _Func: Functions,
    private params: RouteParams,
    private fb: FormBuilder,
    private _Valid: ValidationService,
    private _boxPopupService: BoxPopupService,
    private _palletAssignmentServices:PalletAssignmentServices,
    _RTAction: RouteData,
    private _user:UserService,
    private _API: API_Config,
    private _router: Router) {

    this.orderIDs = this.params.get('id');

    this.checkPermission();
  }

  private showMessage(msgStatus, msg) {
      this.messages = {
          'status': msgStatus,
          'txt': msg
      }
      jQuery(window).scrollTop(0);
  }

  private checkInputNumber (evt) {
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)){
      evt.preventDefault();
    }
  }

  private formatValue(name, value, pallet_carton, carton, notCheckEqual = null) {
    if(!notCheckEqual) {
      value = value.replace(/ /g,'').replace(/-/g,'');
      pallet_carton[name + 'HasErrRequired'] = false;
      pallet_carton[name + 'HasErrInt'] = false;
      pallet_carton[name + 'HasErrZero'] = false;
      this.removeErrEqual(carton);
    }

    // format value & check validation
    if(value == null || value == '') {
      pallet_carton[name + 'HasErrRequired'] = true;
      carton.hasError = true;
    }
    else {
      if(isNaN(value)) {
        pallet_carton[name + 'HasErrInt'] = true;
        carton.hasError = true;
      }
      else {
        value = parseInt(value);
        if(value == 0) {
          pallet_carton[name + 'HasErrZero'] = true;
          carton.hasError = true;
        }
      }
    }
    pallet_carton[name + '_num'] = value;
    if(!notCheckEqual) {
      this.checkEqual(carton, pallet_carton, name);
    }
  }

  private removeErrEqual(carton) {
    for(var i = 0; i < carton.pallet_carton.length; i++) {
      carton.pallet_carton[i]['palletHasErrEqual'] = false;
      carton.pallet_carton[i]['cartonHasErrEqual'] = false;
    }
  }

  private checkEqual(carton, pallet_carton = null, name = null) {
    var total = carton.total,
        assigned = 0;
    for(let i = 0; i < carton.pallet_carton.length; i++) {
      let palletNum = carton.pallet_carton[i].pallet_num,
          cartonNum = carton.pallet_carton[i].carton_num;
      if(!isNaN(palletNum) && !isNaN(cartonNum)) {
        assigned += palletNum * cartonNum;
      }
    }
    if(assigned != total) {
      carton.hasErrorEqual = true;
      if(pallet_carton) pallet_carton[name + 'HasErrEqual'] = true;
    }
    else {
      this.removeErrEqual(carton);
    }
    // update carton assigned & order assigned
    carton.assigned = assigned;
    this.updateOrderAssigned();
  }

  private setPalletNum(carton, index) {
    const total = carton.total;
    const currentCartonNum = carton.pallet_carton[index].carton_num;
    let assigned = 0;
    let remaining = 0;
    let palletNum = 0;

    for(let i = 0; i < carton.pallet_carton.length; i++) {
      let palletNum = carton.pallet_carton[i].pallet_num,
          cartonNum = carton.pallet_carton[i].carton_num;
      if(!isNaN(palletNum) && !isNaN(cartonNum) && i !== index) {
        assigned += palletNum * cartonNum;
      }
    }

    remaining = total - assigned;
    if (remaining > 0 && currentCartonNum) {
      palletNum = Math.floor(remaining/currentCartonNum);
    }
    carton.pallet_carton[index].pallet_num = palletNum ? palletNum : '';
    this.checkEqual(carton);
  }

  private updateOrderAssigned() {
    for(var i = 0; i < this.listOrders.length; i++) {
      var order = this.listOrders[i],
          assigned = 0;
      for(var j = 0; j < order.cartons.length; j++) {
        assigned += order.cartons[j].assigned;
      }
      order.assigned = assigned;
    }
  }

  private checkPermission() {
    this.showLoadingOverlay = true;
    this._user.GetPermissionUser().subscribe(
      data => {
        this.hasAssignPalletPermission = true;
        // this.hasAssignPalletPermission = this._user.RequestPermission(data,'assignPallet');

        if(!this.hasAssignPalletPermission) {
            this._router.parent.navigateByUrl('/deny');
        }
        else {
          this.getOrderCarton();
        }
      },
      err => {
        this.showMessage('danger', this._Func.parseErrorMessageFromServer(err));
        this.showLoadingOverlay = false;
      }
    );
  }

  private getOrderCarton() {
    this.showLoadingOverlay = true;
    this._palletAssignmentServices.getOrderCarton(this.orderIDs)
      .subscribe(
        data => {
          this.listOrders = data.data;
          this.initPalletCarton();
          this.showLoadingOverlay = false;
        },
        err => {
          this.showMessage('danger', this._Func.parseErrorMessageFromServer(err));
          this.showLoadingOverlay = false;
        }
      );
  }

  private addMorePallet(carton, pallet_carton) {
    if(carton.total != carton.assigned) {
      pallet_carton.push({
        pallet_num: '',
        carton_num: '',
        checked: false
      });
      carton.checkAll = false;
    }
  }

  private deleteSelectedItem() {
    for(var i = 0; i < this.listOrders.length; i++) {
      var order = this.listOrders[i];
      for(var j = 0; j < order.cartons.length;j++) {
        var carton = order.cartons[j],
            recheckEqual = false;
        for(var k = carton.pallet_carton.length - 1; k >= 0; k--) {
          var pallet_carton = carton.pallet_carton[k];
          if(pallet_carton.checked) {
            carton.pallet_carton.splice(k, 1);
            recheckEqual = true;
          }
        }
        if(recheckEqual) {
          this.checkEqual(carton);
        }
      }
    }
    this.initPalletCarton();
  }

  private initPalletCarton() {
    for(var i = 0; i < this.listOrders.length; i++) {
      var order = this.listOrders[i];
      if(!order.cartons) order.cartons = [];
      for(var j = 0; j < order.cartons.length;j++) {
        var carton = order.cartons[j];
        carton.checkAll = false;
        if(!carton.pallet_carton) {
          carton.pallet_carton = [{
            pallet_num: '',
            carton_num: '',
            checked: false
          }]
        }
        else {
          if(!carton.pallet_carton.length) {
            carton.pallet_carton.push({
              pallet_num: '',
              carton_num: '',
              checked: false
            });
          }
        }
      }
    }
  }

  private checkAll(value, pallet_carton) {
    for(var i = 0; i < pallet_carton.length; i++) {
      pallet_carton[i].checked = value;
    }
  }

  private checkCheckAll(carton, pallet_carton) {
    var isCheckAll = true;
    for(var i = 0; i < pallet_carton.length; i++) {
      if(!pallet_carton[i].checked) {
        isCheckAll = false;
      }
    }
    carton.checkAll = isCheckAll;
  }

  private save(saveAndPrint = null) {
    // validate
    this.messages = null;
    for(let i = 0; i < this.listOrders.length; i++) {
      let order = this.listOrders[i];
      for(let j = 0; j < order.cartons.length;j++) {
        let carton = order.cartons[j];
        carton.hasError = false;
        carton.hasErrorEqual = false;
        for(let k = 0; k < carton.pallet_carton.length; k++) {
          this.formatValue('pallet', carton.pallet_carton[k].pallet_num, carton.pallet_carton[k], carton, true);
          this.formatValue('carton', carton.pallet_carton[k].carton_num, carton.pallet_carton[k], carton, true);
        }
        this.checkEqual(carton);
      }
    }

    let isValid = true,
        isNotEqual = false;
    for(let i = 0; i < this.listOrders.length; i++) {
      let order = this.listOrders[i];
      for(let j = 0; j < order.cartons.length;j++) {
        let carton = order.cartons[j];
        if(carton.hasError || carton.hasErrorEqual) {
          isValid = false;
          if(carton.hasErrorEqual) {
            isNotEqual = true;
          }
        }
      }
    }

    if(isValid) {
      var listOrderName = '';
      for(var i = 0; i < this.listOrders.length; i++) {
        var prefix = i == 0 ? 'Order ' : ', Order ';
        listOrderName+= prefix + this.listOrders[i].odr_num;
      }
      if(saveAndPrint) {
        this.saveAndPrint(JSON.stringify({'data': this.listOrders}), listOrderName);
      }
      else {
        this._palletAssignmentServices.assignPallet(JSON.stringify({'data': this.listOrders}))
          .subscribe(
            data => {
              this.showMessage('success', this.listErrMsg.AP2.replace('{{listOrderName}}', listOrderName));
              setTimeout(() => {
                this._router.parent.navigateByUrl('/outbound/pallet-assignment');
              }, 1000);
              this.showLoadingOverlay = false;
            },
            err => {
              this.showMessage('danger', this._Func.parseErrorMessageFromServer(err));
              this.showLoadingOverlay = false;
            }
          );
      }
    }
    else {
      if(isNotEqual) {
        this.showMessage('danger', this.listErrMsg.AP1);
      }
    }
  }

  private saveAndPrint(data, listOrderName){
    var that = this;
    let api = that._API.API_Pack  + '/packs-assign-pallets/1';
    that.showLoadingOverlay = true;
    try{
      let xhr = new XMLHttpRequest();

      xhr.open("PUT", api , true);
      xhr.setRequestHeader("Authorization", 'Bearer ' +that._Func.getToken());
      xhr.setRequestHeader("Content-Type", 'application/json');
      xhr.responseType = 'blob';

      xhr.onreadystatechange = function () {
        if(xhr.readyState == 2) {
          if(xhr.status == 200) {
            xhr.responseType = "blob";
          } else {
            xhr.responseType = "text";
          }
        }

        if(xhr.readyState === 4) {
          if(xhr.status === 200) {
            var today = new Date();
            var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
              "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
            ];
            var day = today.getDate();
            var month = monthNames[today.getMonth()];
            var year = today.getFullYear().toString().substr(2,2);
            var fileName = 'AssignPallet_' + month+ day + year;
            var blob = new Blob([this.response], {type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'});
            saveAs.saveAs(blob, fileName + '.pdf');
            that.showMessage('success', that.listErrMsg.AP2.replace('{{listOrderName}}', listOrderName));
            setTimeout(() => {
              that._router.parent.navigateByUrl('/outbound/pallet-assignment');
            }, 2000);
          }else{
            let errMsg = '';
            if(xhr.response) {
              try {
                var res = JSON.parse(xhr.response);
                errMsg = res.errors && res.errors.message ? res.errors.message : xhr.statusText;
              }
              catch(err){errMsg = xhr.statusText}
            }
            else {
              errMsg = xhr.statusText;
            }
            if(!errMsg) {
              errMsg = that._Func.msg('VR100');
            }
            that.showMessage('danger', errMsg);
          }
          that.showLoadingOverlay = false;
        }
      }
      xhr.send(data);
    }catch (err){
      that.showLoadingOverlay = false;
      that.showMessage('danger', that._Func.msg('VR100'));
    }
  }

  private cancel() {
    var that = this;
    this._boxPopupService.showWarningPopup()
          .then(function (ok) {
            if(ok)
              that._router.navigateByUrl('/outbound/pallet-assignment');
          });
  }

  private printCartonLabel(){
    swal({
      text: 'This will be developed in the future.',
      type: 'warning',
      showCancelButton: false,
      showConfirmButton: true,
      confirmButtonText: 'OK',
      confirmButtonClass: 'btn btn-primary',
      width: '280'
    })
  }
}




