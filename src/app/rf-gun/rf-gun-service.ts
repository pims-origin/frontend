import {Component, Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {API_Config, Functions} from '../common/core/load';

@Injectable()
export class RFGunServices {

  public AuthHeader = this._Func.AuthHeader();
  public AuthHeaderPostJson = this._Func.AuthHeaderPostJson();
  private pageTitle='title demo';

  constructor(private _Func: Functions, private _API: API_Config, private http: Http) {

  }

  /*
  * title service
  * */
  setPageTitle(title)
  {
    this.pageTitle = title;
  }

  /*
  * Get page title
  * */
  getPageTile()
  {
    return this.pageTitle;
  }

  searchOrderNumber(params) {
    return this.http.get(this._API.API_ORDERS+params,{ headers: this.AuthHeader })
      .map((res: Response) => res.json());
  }

  // Get list goods receipt
  public RF_GUN_API_GET (whs_id,$params) {

    return this.http.get(this._API.API_RF_GUN + '/' + whs_id + '/put-away' +  $params, {headers: this.AuthHeader})
      .map(res => res.json());

  }

  // Get get_warehouse
  public get_warehouse ($param) {

    return this.http.get(this._API.API_Warehouse+$param, {headers: this.AuthHeader})
      .map(res => res.json());

  }

  reLocate(whs_id,data)
  {
    return this.http.put(this._API.API_RF_GUN + "/" + whs_id + '/relocation', data , {headers: this.AuthHeaderPostJson})
      .map(res => res.json().data);
  }


  // Get list goods receipt
  public RF_GUN_API_UPDATE (whs_id,data) {

    return this.http.put(this._API.API_RF_GUN + "/" + whs_id + '/put-away', data , {headers: this.AuthHeaderPostJson})
      .map(res => res.json().data);

  }



}
