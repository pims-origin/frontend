import {Component} from '@angular/core';
import {Router, RouterLink, RouteConfig, ROUTER_DIRECTIVES} from '@angular/router-deprecated';
import {OrderListComponent} from "../order/order-list/order-list.component";
import {CancelOrderDetailComponent} from "./cancel-order-detail/cancel-order-detail.component";
import {CancelOrderListComponent} from "./cancel-order-list/cancel-order-list.component";

@Component ({
    selector: 'cancel-order-management',
    directives: [ROUTER_DIRECTIVES],
    template: `<router-outlet></router-outlet>`,
})

@RouteConfig([
    { path: '/', component: CancelOrderListComponent , name: 'Cancel Order List', data:{page:'cancel-order'}, useAsDefault: true },
    { path: '/:id', component: CancelOrderDetailComponent , name: 'Cancel Order Detail', data:{action:'view'}}
])
export class CancelOrderManagementComponent {

}
