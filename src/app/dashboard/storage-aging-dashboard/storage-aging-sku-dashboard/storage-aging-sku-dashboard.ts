import { Component, Input, Output, EventEmitter } from '@angular/core';
import { DecimalPipe, ControlGroup, FormBuilder, Control } from '@angular/common';

import { WMSPagination } from '../../../common/directives/directives';
import { DashboardServices } from '../../dashboard.service';
import { Functions } from '../../../common/core/load';
import { PaginatePipe, PaginationControlsCmp } from '../../../common/directives/directives';
import { WMSNumberFormatter } from '../../../common/pipes/wms-number-formatter';

declare var Chart: any;

@Component({
  selector: 'storage-aging-sku-dashboard',
  templateUrl: 'storage-aging-sku-dashboard.html',
  directives: [WMSPagination, PaginationControlsCmp],
  providers: [DashboardServices],
  pipes: [PaginatePipe, DecimalPipe, WMSNumberFormatter]
})
export class StorageAgingSKUDashboard {
  private listStorageAging = {};
  private whsId: string = '';
  totalValue: number = 0;
  listStorageAgingStatus = [];

  @Input('currentWHSId') set updateWHSId(value: any) {
    this.whsId = value;
  }

  @Output() showLoading = new EventEmitter();

  constructor(private dashboardServices: DashboardServices, private funcs: Functions) {
    this.getStoringAgingbySKU();
  }

  private defaultPieChartOpts = {
    responsive: true,
    tooltips: {
      callbacks: {
        label: (tooltipItem, data) => {
          let index = tooltipItem.index;
          return data.labels[index] + ': ' + Number(data.datasets[0].data[index]).toFixed(0);
        }
      }
    },
    legend: {
      labels: {
        boxWidth: 30
      }
    },
    layout: {
      padding: {
        top: 5
      }
    }
  };

  getStoringAgingbySKU(sku: string = '') {
    this.showLoading.emit(true);

    this.dashboardServices.getStorageAgingDataBySKU(sku).subscribe(
      (data) => {
        this.showLoading.emit(false);
        this.listStorageAging = data.data;

        this.totalValue =
          this.listStorageAging[0] && this.listStorageAging[1] && this.listStorageAging[2]
            ? Number.parseInt(this.listStorageAging[0]['count']) +
              Number.parseInt(this.listStorageAging[1]['count']) +
              Number.parseInt(this.listStorageAging[2]['count'])
            : 0;

        this.drawPiesChart();
        this.drawBarChart();
      },
      (err) => {
        this.showLoading.emit(false);
        console.log(err);
      }
    );
  }

  calculatePercentage(itemNumber: number, totalNumber: number) {
    return totalNumber ? (itemNumber * 100) / totalNumber : 0;
  }

  private chart: any;
  private drawBarChart() {
    let barChartData = {
      datasets: [
        {
          data: [],
          backgroundColor: [],
          label: ['']
        }
      ],
      labels: []
    };

    const data = this.listStorageAging;
    barChartData['labels'] = ['Period 0-30', 'Period 31-60', 'Period 61-90'];
    barChartData['datasets'][0]['data'] = [data[0]['count'], data[1]['count'], data[2]['count']];
    barChartData['datasets'][0]['backgroundColor'] = ['#008000', '#d91e18', '#ddd', '#36c6d3'];

    const ctx = document.getElementById('storage-sku-bar-chart-content');
    if (ctx == null) {
      return true;
    }

    this.chart = new Chart(ctx, {
      type: 'bar',
      data: barChartData,
      options: {
        title: {
          display: false
        },
        legend: false,
        tooltips: {
          mode: 'single',
          callbacks: {
            label: (tooltipItem, data) => {
              return ': ' + this.funcs.formatSWISNumber(tooltipItem.yLabel);
            }
          }
        },
        responsive: true,
        scales: {
          yAxes: [
            {
              stacked: true,
              ticks: {
                beginAtZero: true,
                callback: (value, index, values) => {
                  if (value.toString().indexOf('.') !== -1) {
                    return this.funcs.formatSWISNumber(value.toFixed(1));
                  }
                  return this.funcs.formatSWISNumber(value);
                }
              }
            }
          ]
        },
        layout: {
          padding: {
            top: 20
          }
        }
      }
    });
  }

  private pieChart;

  drawPiesChart() {
    let pieChartData = {
      datasets: [
        {
          data: [],
          backgroundColor: [],
          label: 'Dataset 1'
        }
      ],
      labels: []
    };

    const data = this.listStorageAging;
    pieChartData['labels'] = ['Period 0-30', 'Period 31-60', 'Period 61-90'];
    pieChartData['datasets'][0]['data'] = [data[0]['count'], data[1]['count'], data[2]['count']];
    pieChartData['datasets'][0]['backgroundColor'] = ['#008000', '#d91e18', '#ddd', '#36c6d3'];

    const ctx = document.getElementById('storage-sku-pie-chart-content');
    if (ctx == null) {
      return true;
    }

    ctx.style.height = '200px';

    if (this.pieChart) this.pieChart.destroy();
    this.pieChart = new Chart(ctx, {
      type: 'pie',
      data: pieChartData,
      options: this.defaultPieChartOpts
    });
  }

  listDataAutocomplete = {};

  autoCompleteSku($nameField, $e) {
    this.listDataAutocomplete[$nameField] = null;

    if ($e.target.value) {
      this.listDataAutocomplete['loading-' + $nameField] = true;

      // do Search
      var param = `?sku=` + this.funcs.trim($e.target.value) + `&limit=20`;

      this.dashboardServices
        .getSKUsSearch(this.whsId, param)
        .debounceTime(400)
        .distinctUntilChanged()
        .subscribe(
          (data) => {
            this.listDataAutocomplete[$nameField] = $e.target.value ? data.data : null;
            this.listDataAutocomplete['loading-' + $nameField] = false;
          },
          (err) => {
            this.listDataAutocomplete['loading-' + $nameField] = false;
            console.log(err);
          },
          () => {}
        );
    }
  }

  selectedSKU: string = '';
  selectedItem($item) {
    this.listDataAutocomplete['sku'] = null;
    this.selectedSKU = $item.sku;
    if (this.selectedSKU) {
      this.getStoringAgingbySKU(this.selectedSKU);
    }
  }
}
