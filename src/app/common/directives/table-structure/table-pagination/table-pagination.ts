
import {Component, Input, Injectable} from '@angular/core';
import {TableFunctions} from '../../../core/table-functions';

@Component ({
    selector: 'table-pagination',
    directives:[],
    providers: [
        TableFunctions
    ],
    templateUrl: 'table-pagination.html',
})

@Injectable()

export class TablePagination {

    @Input() rowData: any [];
    @Input() Pagination;
    @Input() perPage = 20;
    @Input() parentComponent;

    constructor(
        private _tableFunc: TableFunctions
    )
    {

    }

    //*************************************************************************

    public filterList(pageNumber: number)
    {
        this._tableFunc.callGetTableData({
            parentComponent: this.parentComponent,
            page: pageNumber
        });
    }

    //*************************************************************************

}
