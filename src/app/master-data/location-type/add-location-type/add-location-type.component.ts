import { Component} from '@angular/core';
import {FORM_DIRECTIVES,ControlGroup,FormBuilder, Control ,Validators} from '@angular/common';
import { Http } from '@angular/http';
import { Router, RouterLink, ROUTER_DIRECTIVES, RouteParams } from '@angular/router-deprecated';
import 'ag-grid-enterprise/main';
import 'rxjs/add/operator/map';
import {API_Config} from "../../../common/core/API_Config";
import {UserService} from "../../../common/users/users.service";
import {Functions} from "../../../common/core/functions";
import {ControlMessages} from "../control-messages";
import {ValidationService} from "../validation.service";
import {BoxPopup} from "../../../common/popup/box-popup";
import {WarningBoxPopup} from "../../../common/popup/warning-box-popup";
import {BoxPopupService} from "../../../common/popup/box-popup.service";
import {JwtHelper} from "angular2-jwt/angular2-jwt";
import {WMSBreadcrumb,WMSMessages} from '../../../common/directives/directives';
declare var jQuery: any;

@Component ({
    selector: 'add-location-type',
    templateUrl: 'add-location-type.component.html',
    directives: [ControlMessages, FORM_DIRECTIVES, WMSMessages,WMSBreadcrumb],
    providers: [BoxPopupService],
})

export class AddLocationTypeComponent {

    private headerGet: any;
    private headerPost: any;
    private urlAPILocationType: any;
    private messages;
    locaTypeForm: ControlGroup;
    private flagCode = false;
    private flagName = false;
    private listNull = {};
    private action = 'new';
    public showLoadingOverlay = false;
    constructor (
        private _http: Http,
        private _router:Router,
        private _user: UserService,
        private _boxPopupService: BoxPopupService,
        public jwtHelper: JwtHelper,
        private _Func: Functions,
        private _API_Config: API_Config,
        params: RouteParams, private _formBuilder: FormBuilder) {

        // Check permission for user using this page
        // this.ck_permisson();
        this.checkPermission();
        this.headerGet = this._Func.AuthHeader();
        this.headerPost = this._Func.AuthHeaderPost();
        this.urlAPILocationType = this._API_Config.API_Location_type;

    }

    private checkPermission() {
        this.showLoadingOverlay = true;
        this._user.GetPermissionUser().subscribe(
            data => {
                if((this.action == 'view' && !this._user.RequestPermission(data,'viewLocationType')) || (this.action == 'edit' && !this._user.RequestPermission(data,'editLocationType')) || (this.action == 'new' && !this._user.RequestPermission(data,'createLocationType'))) {
                    this._router.parent.navigateByUrl('/deny');
                }
                this.showLoadingOverlay = false;
            },
            err => {
                this.showMessage('danger', this._Func.parseErrorMessageFromServer(err));
                this.showLoadingOverlay = false;
            }
        );
    }

    private checkLength($event, length) {

        if ($event.target.value.length >= length  && $event.keyCode != 8) {

            $event.preventDefault();
        }
    }

    // Validate float
    private validateFloat(float ) {

        var re = /^([0-9,.]*)$/;
        return re.test(float);

    }

    // Validate number
    private validateNumber(number) {

        var re = /^([0-9]*)$/;
        return re.test(number);
    }

    // Filter form only show choose one
    private filterForm2 ($target) {

        var arr_object = [
            {'input': 'loc_type_code', 'typeValidate': 'required'},
            {'input': 'loc_type_code', 'typeValidate': 'invalidLength2Or3'},
            {'input': 'loc_type_name', 'typeValidate': 'required'},
            {'input': 'loc_type_name', 'typeValidate': 'invalidNameStr'}
        ];

        try {

            for (var i = 0; i < arr_object.length; i++) {

                if (arr_object[i].input == $target.name) {

                    switch (arr_object[i].typeValidate) {

                        case 'required_float':

                            if ($target.value.trim() == "" && arr_object[i].typeValidate == 'required_float') {

                                this.listNull[$target.name + "_" + arr_object[i].typeValidate] = true;

                            } else if (!this.validateFloat($target.value) && arr_object[i].typeValidate == 'required_float') {

                                this.listNull[$target.name + "_" + arr_object[i].typeValidate] = true;

                            } else {

                                this.listNull[$target.name + "_" + arr_object[i].typeValidate] = false;
                            }
                            break;

                        case 'required':

                            if ($target.value.trim() == "" && arr_object[i].typeValidate == 'required') {

                                this.listNull[$target.name + "_" + arr_object[i].typeValidate] = true;

                            } else {

                                this.listNull[$target.name + "_" + arr_object[i].typeValidate] = false;
                            }
                            break;
                        case 'invalidLength2Or3':
                            if ($target.value.trim() && arr_object[i].typeValidate == 'invalidLength2Or3') {
                                this.listNull[$target.name + "_" + arr_object[i].typeValidate] = !/^[a-zA-Z0-9]{2,3}$/i.test($target.value);
                            } else {
                                this.listNull[$target.name + "_" + arr_object[i].typeValidate] = false;
                            }
                            break;
                        case 'invalidNameStr':
                            if ($target.value.trim() && arr_object[i].typeValidate == 'invalidNameStr') {
                                this.listNull[$target.name + "_" + arr_object[i].typeValidate] = !/^[a-zA-Z0-9 _-]*$/i.test($target.value);
                            } else {
                                this.listNull[$target.name + "_" + arr_object[i].typeValidate] = false;
                            }
                            break;
                        case 'int':

                            if (!this.validateNumber($target.value) && arr_object[i].typeValidate == 'int') {

                                this.listNull[$target.name + "_" + arr_object[i].typeValidate] = true;
                            } else {

                                this.listNull[$target.name + "_" + arr_object[i].typeValidate] = false;
                            }
                            break;
                        case 'float':

                            if (!this.validateFloat($target.value) && arr_object[i].typeValidate == 'float') {

                                this.listNull[$target.name + "_" + arr_object[i].typeValidate] = true;
                            } else {

                                this.listNull[$target.name + "_" + arr_object[i].typeValidate] = false;
                            }
                            break;

                        default:
                            break;

                    }

                }

            }

            return false;

        } catch (err) {

            return false;
        }

    }

    private add() {

        var tar_get = this;
        jQuery('form').find('input, textarea, select').each(function(i, field) {

            tar_get.filterForm2(field);

        });

         if (Object.keys(this.listNull).length < 1) {

             return false;
         }
        var submit = true;
        jQuery.each( this.listNull, function( key, value ) {

            if (value) {

                submit = false;
                return false;
            }
        });

        if (!submit) {

            return false;
        }

        var params = jQuery("form").serialize();

        this._http.post(this.urlAPILocationType, params , {headers: this.headerPost})
            .map(res => res.json())
            .subscribe(
                data => {
                    this.messages = this._Func.Messages('success', this._Func.msg('VR107'));
                   },
                err => {
                    this.showMessage('danger', this._Func.parseErrorMessageFromServer(err));
                },
                () => {

                    let parent = this;
                    setTimeout(function(){

                        parent._router.parent.navigateByUrl('/master-data/location-type');
                    }, 1100);

                }
            );
    }

    private cancel() {
        let n = this;
        /*case 1: modify text*/
        // let warningPopup = new WarningBoxPopup();
        // warningPopup.text = "Are you sure to cancel";
        // this._boxPopupService.showWarningPopup(warningPopup)

        /*case 2: default*/
        this._boxPopupService.showWarningPopup()
            .then(function (dm) {
                if(dm)
                    n._router.navigateByUrl('/master-data/location-type');

            });
    }

    private parseError (err) {

        if (err.status == 400) {

            this.messages = this._Func.Messages('danger', err.json().error.message);
        }
        else
            this.messages = this._Func.Messages('danger', this._Func.msg('VR100'));


    }

    private showMessage(msgStatus, msg) {
        // if(this.timeoutHideMessage) clearTimeout(this.timeoutHideMessage);
        this.messages = {
            'status': msgStatus,
            'txt': msg
        }
        // this.timeoutHideMessage = setTimeout(() => {
        //     this.messages = null;
        // }, 4000);
        jQuery(window).scrollTop(0);
    }

}
