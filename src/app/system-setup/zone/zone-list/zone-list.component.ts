import {Component} from '@angular/core';
import {API_Config} from "../../../common/core/API_Config";
import {UserService} from "../../../common/users/users.service";
import {Functions} from "../../../common/core/functions";
import {Http} from "@angular/http";
import { Router, RouterLink, ROUTER_DIRECTIVES, RouteParams } from '@angular/router-deprecated';
import { CORE_DIRECTIVES, FORM_DIRECTIVES } from '@angular/common';
import {WMSPagination,AgGridExtent,WMSBreadcrumb} from '../../../common/directives/directives';
declare var jQuery: any;

@Component ({
    selector: 'zone-list',
    directives:[AgGridExtent,WMSPagination,WMSBreadcrumb],
    templateUrl: 'zone-list.component.html',
})

export class ZoneListComponent {

  private headerURL = this._API_Config.API_User_Metas+'/zon';
  private messages;
  private Loading:any =[];
  private zoneList:any = [];
  private authHeader = this._Func.AuthHeader();
  private warehouse_id:string = localStorage.getItem('whs_id');
  private warehouseList:any = [];
  private zoneTypeList:any = [];

  // Pagination
  private Pagination;
  private currentPage = 1;
  private perPage = 20;

  private action='search';
  public ListSelectedItem:Array<any>=[];
  public selectedAll:boolean=false;
  private showLoadingOverlay:boolean=false;

  private zone_name:string = '';

  private columnData = {
    zone_id:{
      attr:'checkbox',
      id:true,
      title:'#',
      width:25,
      pin:true,
      ver_table:2
    },
    zone_code:{
      title:'Zone Code',
      width:80,
      url:'#/system-setup/zone/',
      field_get_id:'zone_id',
      pin:true,
      sort:true
    },
    zone_name: {
      title:'Zone Name',
      width:160,
      pin:true,
      sort:true
    },
    zone_type_name: {
      title:'Zone Type',
      width:80,
      pin:true
    },
    zone_min_count:{
      title: 'Min Count',
      width:70,
      pin:true,
      sort:true
    },
    zone_max_count: {
      title: 'Max Count',
      width:70,
      pin:true,
      sort:true
    },
    zone_num_of_loc:{
      title: 'Number of Locations',
      width:80,
      sort:true
    },
    zone_num_of_inactive_loc:{
      title: 'Number of Inactive Locations',
      width:80,
      sort:true
    },
    cus_name:{
      title: 'Customer',
      parent :'customers',
      width:140
    },
    zone_description:{
      title: 'Description',
      width:120
    }
  };

  constructor(
    params: RouteParams,
    private _http: Http,
    private _router:Router,
    private _user: UserService,
    private _Func: Functions,
    private _API_Config: API_Config) {

    this.checkPermission();

  }


  private createZone;
  private viewZone;
  private editZone;
  private allowAccess:boolean=false;
  private checkPermission() {
    this.showLoadingOverlay = true;
    this._user.GetPermissionUser().subscribe(
      data => {
        this.createZone = this._user.RequestPermission(data, 'createZone');
        this.viewZone = this._user.RequestPermission(data,'viewZone');
        this.editZone=this._user.RequestPermission(data,'editZone');
        /* Check orther permission if View allow */
        if(!this.viewZone) {
          this._router.parent.navigateByUrl('/deny');
        }
        else {
          // pass
          this.allowAccess=true;
          this.showLoadingOverlay = false;
          this.getZoneList(this.currentPage);
          this.getZoneType();
          // end pass
        }
      },
      err => {
        this.messages=this._Func.Messages('danger', this._Func.parseErrorMessageFromServer(err));
        this.showLoadingOverlay = false;
      }
    );
  }

  private getZoneType(){
    let params = "?sort[zone_type_name]=asc&limit=999";
    this._http.get(this._API_Config.API_Zone_Type + params, {
        headers: this.authHeader
      })
      .map(res => res.json().data)
      .subscribe(
        data => {
          this.zoneTypeList = data;
        },
        err => {
        },
        () => {}
      );
  }

  private getZoneTypeTest(){
    let params = "?zone_name=" + this.zone_name;
    this._http.get(this._API_Config.API_Warehouse + '/' + this.warehouse_id + '/zone/search' + params, {
      headers: this.authHeader
    })
      .map(res => res.json().data)
      .subscribe(
        data => {
          let fdata = data.map(function(e,i){
            return e.zone_name;
          });
          jQuery("#zone_name").autocomplete({
            source: fdata
          });
        },
        err => {
        },
        () => {}
      );
  }

  private getWarehouse() {
    this.messages = null;

    this._http.get(this._API_Config.API_Warehouse + '?limit=20', {
        headers: this.authHeader
      })
      .map(res => res.json().data)
      .subscribe(
        data => {
          this.warehouseList = data;
        },
        err => {
        },
        () => {}
      );
  }
  private sortData={
    fieldname:'zone_code',
    sort:'asc'
  }
  private getZoneList(page=null) {
  //let params = "?page="+page+"&sort[zone_code]=asc&limit="+this.perPage;
    let param="?sort["+this.sortData['fieldname']+"]="+this.sortData['sort']+"&page="+page+"&limit="+this.perPage;

    this.messages = null;
    this.action='getlist';
    this.showLoadingOverlay=true;

  this._http.get(this._API_Config.API_Warehouse + '/' + this.warehouse_id + '/zone/search' + param, {
        headers: this.authHeader
      })
      .map(res => res.json())
      .subscribe(
          data => {
            this.selectedAll = false;
            this.ListSelectedItem=[];
            this.zoneList = this._Func.formatData(data.data);
            this.showLoadingOverlay=false;
            // console.log(this.zoneList);
            //pagin function
            this.Pagination=data['meta']['pagination'];
            this.Pagination['tmpPageCount']=this._Func.Pagination(this.Pagination);


          },
          err => {
            if(err.status==500)
            {
              this.messages = this._Func.Messages('danger', this._Func.msg('VR100'));
            }
            else{
              this.messages = this._Func.Messages('danger', this._Func.msg('VR100'));
            }
            this.showLoadingOverlay=false;
          },
          () => {}
      );
  }
  //Search
  private search(page=null) {
    this.messages = null;
    this.showLoadingOverlay=true;
    this.action='search';
    let params="?sort["+this.sortData['fieldname']+"]="+this.sortData['sort']+"&page="+page+"&limit="+this.perPage+'&';
    this.Loading['searching']=true;
    params = params + jQuery("form#form-filter").serialize();
    this.messages = null;

    this._http.get(this._API_Config.API_Warehouse + '/' + this.warehouse_id + '/zone/search' + params, {
          headers: this.authHeader
        })
        .map(res => res.json())
        .subscribe(
            data => {
              this.showLoadingOverlay=false;
              this.selectedAll = false;
              this.ListSelectedItem=[];
              this.zoneList = this._Func.formatData(data.data);
              // console.log(this.zoneList);
              //pagin function
              this.Pagination=data['meta']['pagination'];
              this.Pagination['tmpPageCount']=this._Func.Pagination(this.Pagination);
            },
            err => {
              this.showLoadingOverlay=false;
              if(err.status==500)
              {
                this.messages = this._Func.Messages('danger', this._Func.msg('VR100'));
              }
              else{
                this.messages = this._Func.Messages('danger', this._Func.msg('VR100'));
              }
            },
            () => {}
        );
  }

  private resetSearch()
  {
    jQuery("#form-filter input, #form-filter select").each(function( index ) {

      jQuery(this).val("");
    });
    this.getZoneList();
  }


  private onPageSizeChanged($event)
  {
    this.perPage = $event.target.value;
    if((this.Pagination['current_page'] - 1) * this.perPage >= this.Pagination['total']) {
      this.Pagination['current_page'] = 1;
    }
    if(this.action=='search'){
      this.search(this.Pagination['current_page']);
    }else{
      this.getZoneList(this.Pagination['current_page']);
    }
  }

  private filterList(num)
  {
    if(this.action=='search'){
      this.search(num);
    }else{
      this.getZoneList(num);
    }
  }
  private getPage(num)
  {
    let arr=new Array(num);
    return arr;
  }

  /*
   * Check All/Off
   * */
  private CheckAll(event) {
    if (event) {
      this.selectedAll = true;
    }
    else {
      this.selectedAll = false;
    }
    // set empty array  ListSelectedItem
    this.ListSelectedItem=[];
    // Loop
    this.zoneList.forEach((item) => {
      item['selected'] = this.selectedAll;
      if(this.selectedAll)
      {
        // if check all = true , pushing to ListSelectedItem
        this.ListSelectedItem.push(item['zone_id']);

      }
    });
  }

  /*
   * Selected
   * */
  private Selected($event) {


    let item = $event.target.value;

    this.selectedAll=false;


    if ($event.target.checked) {
      this.ListSelectedItem.push(item);
      this.SetItemListSelected(item,true);
      if(this.ListSelectedItem.length==this.zoneList.length)
      {
        this.selectedAll=true;
      }
    }
    else {
      this.selectedAll=false;
      let el = this.ListSelectedItem.indexOf(item);
      // remove item out of array
      this.ListSelectedItem.splice(el, 1);
      this.SetItemListSelected(item,false);

    }
  }

  /*
   * Set select item
   * */
  SetItemListSelected(id,value)
  {
    this.zoneList.forEach((item) => {
      if(item['zone_id']==id)
      {
        item['selected']=value;
      }
    });
  }

  /**
   * Edit zone item
   */
  Edit() {
    //console.log('# of selected: ',this.ListSelectedItem.length);
    if (this.ListSelectedItem.length > 1) {
      this.messages = this._Func.Messages('danger', 'Please choose only one item to edit');
      return false;
      //alert('Please select only one item to edit');
    } else if (this.ListSelectedItem.length == 0) {
      this.messages = this._Func.Messages('danger', 'Please choose one item to edit');
      return false;
      //alert('Please select one item to edit');
    } else if (this.ListSelectedItem.length == 1) {
      let isDynamicZone = this.isDynamicZone(this.ListSelectedItem[0]);
      if (isDynamicZone === 1) {
        this.messages = this._Func.Messages('danger', 'Cannot edit Dynamic Zone');
      } else {
        this._router.parent.navigateByUrl('/system-setup/zone/' + this.ListSelectedItem[0]['zone_id'] + '/edit');
      }
    }
  }

  /**
   * Validate zone is dynamic or not
   *
   * @param selectedZoneItemFromGrid Selected zone item from grid.
   * @returns {number}
   */
  isDynamicZone(selectedZoneItemFromGrid: any): number {
    let selectedZone = this.zoneList.find(zone => zone['zone_id'] === selectedZoneItemFromGrid['zone_id']);
    let isDynamic = selectedZone ? selectedZone['is_dynamic'] : 0;

    return isDynamic;
  }
}
