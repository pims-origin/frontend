import {Component, DynamicComponentLoader, ElementRef, Injector} from '@angular/core';
import {FORM_DIRECTIVES, ControlGroup, FormBuilder, Control, Validators} from '@angular/common';
import {Http} from '@angular/http';
import {Router, RouterLink, ROUTER_DIRECTIVES, RouteParams} from '@angular/router-deprecated';
import 'ag-grid-enterprise/main';
import 'rxjs/add/operator/map';
import {API_Config} from "../../common/core/API_Config";
import {Functions} from "../../common/core/functions";
import {JwtHelper} from "angular2-jwt/angular2-jwt";
import {
    WMSBreadcrumb,
    WMSMessages,
    WMSPagination,
    PaginationControlsCmp,
    PaginationService,
    InvoiceTracking
} from '../../common/directives/directives';
import {GoBackComponent} from "../../common/component/goBack.component";
import {UserService} from "../../common/users/users.service";
import {PbServices} from "./pb.service";
declare var jQuery:any;

@Component({
    selector: 'pb-receipt-detail',
    templateUrl: 'pb-receipt-detail.component.html',
    providers: [PaginationService, PbServices],
    directives: [GoBackComponent, WMSMessages, WMSPagination, PaginationControlsCmp, WMSBreadcrumb, InvoiceTracking],
})

export class PbReceiptDetailComponent {

    private id:any;
    private messages:any;
    private dataReturnOrderDetail = [];
    private hasCreateBtn:any;
    private token = '';

    constructor(private _router:Router,
                private _Func:Functions,
                private _pbServices:PbServices,
                params:RouteParams,
                private _user:UserService) {

        // Check permission
        this.checkPermission();
        this.id = params.get('id');
        this.getReturOrderById();
        this.token = this._Func.getToken();
    }

    // Check Permission
    private checkPermission() {

        this._user.GetPermissionUser().subscribe(
            data => {

                this.hasCreateBtn = this._user.RequestPermission(data, 'createGoodsReceipt');
                var flag = this._user.RequestPermission(data, 'viewPutbackReceipt');

                if (!flag) {
                    this._router.parent.navigateByUrl('/deny');
                }
            },
            err => {

                this.showMessage('danger', this._Func.parseErrorMessageFromServer(err));

            }
        );
    }

    private showMessage(msgStatus, msg) {

        this.messages = {
            'status': msgStatus,
            'txt': msg
        };
        jQuery(window).scrollTop(0);
    }

    private requestPermission(permission, permission_req) {

        if (permission && permission.length) {

            for (var per of permission) {

                if (per.name == permission_req) {

                    return true;
                }
            }

        }

        return false;

    }

    // Get return order by ID
    private getReturOrderById() {

        var that = this;
        this._pbServices.getReturOrderById(this.id)
            .subscribe(
                data => {

                    this.dataReturnOrderDetail = data.data;

                },
                err => {

                    this.parseError(err);
                },
                () => {

                    if (that.dataReturnOrderDetail['return_sts'] == 'NW') {

                        that._router.parent.navigateByUrl('/putback-process/pb-receipt');

                    }
                }
            );

    }

    // Go back
    private goBack() {

        history.back();
    }

    // Show error when server die or else
    private parseError(err) {

        this.messages = {'status': 'danger', 'txt': this._Func.parseErrorMessageFromServer(err)};
    }


}
