import {Component, Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {API_Config, Functions} from '../../common/core/load';

@Injectable()
export class WarehouseServices {

  public AuthHeader = this._Func.AuthHeader();
  public AuthHeaderPost = this._Func.AuthHeaderPost();
  public Warehouse_Config_URL = this._API.API_URL_ROOT + '/core/warehouse/v1/warehouses';

  constructor(private _Func: Functions, private _API: API_Config, private http: Http)
  {

  }



  /*
  * Add new warehouse
  * */
  Add_New_WareHouse(data)
  {
    //console.log(data);
    return this.http.post(this._API.API_Warehouse, data, { headers: this._Func.AuthHeaderPostJson() })
      .map((res: Response) => res.json().data);
  }

  /*
   * Update new warehouse
   * */
  Update_WareHouse(id,data)
  {
    //console.log('__update warehouse id > ' +id,data);
    return this.http.put(this._API.API_Warehouse+'/'+id, data, { headers: this._Func.AuthHeaderPostJson() })
      .map((res: Response) => res.json().data);
  }

  /*
  * Get list warehosue
  * */
  GetWareHouseList(param)
  {
    return this.http.get(this._API.API_Warehouse+param,{ headers: this.AuthHeader })
      .map((res: Response) => res.json());
  }

  /*
  * Get a detail warehouse
  * */
  GetDetailWareHouse(id)
  {
    return this.http.get(this._API.API_Warehouse+'/'+id,{ headers: this.AuthHeader })
      .map((res: Response) => res.json());
  }
  /*
  * Delete function
  * */
  DeleteWareHouse(id)
  {
    return this.http.delete(this._API.API_Warehouse + "/" + id, { headers: this.AuthHeader });
  }

  /*
  *
  * Search function
  * */
  Search(param)
  {
    return this.http.get(this._API.API_Warehouse+'/search'+param,{ headers: this.AuthHeader })
      .map((res: Response) => res.json());
  }


  /*
   *
   * Get Zone function
   * */
  GetZone(id,param)
  {
    return this.http.get(this._API.API_Warehouse+'/'+id+'/wh-zone/search?'+param,{ headers: this.AuthHeader })
      .map((res: Response) => res.json());
  }

  /*
   *
   * Get Localtions function
   * */
  GetLocaltions(id,param)
  {
    return this.http.get(this._API.API_Warehouse+'/'+id+'/wh-locations?'+param,{ headers: this.AuthHeader })
      .map((res: Response) => res.json());
  }


  /*
  * Get custommer
  * */
  getCustomer(id,param)
  {
    return this.http.get(this._API.API_CUSTOMER_MASTER+'/customers-wh?whs_id='+id+'&'+param,{ headers: this.AuthHeader })
      .map((res: Response) => res.json());
  }

  /*1/
   * Country List
   * */
  Country(params)
  {
    return this.http.get(this._API.API_Country +params,{ headers: this.AuthHeader })
      .map((res: Response) => res.json());
  }

  /*
   * State
   * */
  State(idCountry,params)
  {
    return this.http.get(this._API.API_Country+'/'+idCountry+'/'+params,{ headers: this.AuthHeader })
      .map((res: Response) => res.json());
  }

  /*
   * Warehouse configuration
   * */
  getConfiguration(whsId, params) {
    return this.http.get(this.Warehouse_Config_URL + '/' + whsId + '/configuration' + params, { headers: this.AuthHeader })
      .map((res: Response) => res.json());
  }

  updateConfiguration(whsId, data) {
    return this.http.post(this.Warehouse_Config_URL + '/' + whsId + '/configuration', data, { headers: this._Func.AuthHeaderPostJson() })
      .map((res: Response) => res.json().data);
  }

  getTimezones(params) {
    return this.http.get(this.Warehouse_Config_URL + '/search-timezone' + params, { headers: this.AuthHeader })
      .map((res: Response) => res.json());
  }

}
