export class DateFunctions
{
    constructor()
    {
    }

    /*
    ****************************************************************************
    */

    public getDateString(date, format='mm/dd/yyyy')
    {
        let returnDate = '',
            day = date.getDate(),
            month = date.getMonth() + 1,
            yyyy = date.getFullYear();

        let dd = day < 10 ? '0' + day : day,
            mm = month < 10 ? '0' + month : month;

        switch (format) {
            case 'yyyy-mm-dd':
                returnDate = yyyy + '-' + mm + '-' + dd;
                break;
            case 'mm/dd/yyyy':
                returnDate = mm + '/' + dd + '/' + yyyy;
                break;
            default:
                break;
        }

        return returnDate;
    }

    /*
    ****************************************************************************
    */

}
