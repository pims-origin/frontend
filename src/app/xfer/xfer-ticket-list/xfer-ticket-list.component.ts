import {Component,Input} from '@angular/core';
import {Functions} from "../../common/core/functions";
import { Router } from '@angular/router-deprecated';
import {AgGridExtent} from "../../common/directives/ag-grid-extent/ag-grid-extent";
import {WMSPagination} from "../../common/directives/wms-pagination/wms-pagination";
import {API_Config} from "../../common/core/API_Config";
import {WorkOrderServices} from "../../outbound-process/work-order/work-order-service";
import { FORM_DIRECTIVES,ControlGroup,FormBuilder, Control,Validators} from '@angular/common';
import {WMSMessages} from "../../common/directives/messages/messages";
import {XFERTicketListService} from "./xfer-ticket-list.service";
import {WMSBreadcrumb} from "../../common/directives/wms-breadcrumb/wms-breadcrumb";
import {UserService} from "../../common/users/users.service";
declare var jQuery: any;
declare var saveAs: any;
@Component ({
    selector: 'xfer-ticket-list',
    templateUrl: 'xfer-ticket-list.component.html',
    providers: [ WorkOrderServices, XFERTicketListService],
    directives: [AgGridExtent, WMSPagination, WMSMessages, WMSBreadcrumb]
})

export class XFERTicketListComponent {

    private headerURL = this.apiService.API_User_Metas+'/xtk';
    private xferList = [];
    private messages;
    private columnData = {
        xfer_ticket_id:{
            attr:'checkbox',
            id:true,
            title:'#',
            width:25,
            pin:true,
            ver_table:'t156g9'
        },
        xfer_ticket_sts_name: {
            title:'Status',
            width:100,
            pin:true,
            sort:true
        },
        xfer_ticket_num: {
            title:'XFER Ticket Number',
            width:150,
            url:'#/xfer/xfer-ticket/',
            field_get_id:'xfer_ticket_id',
            pin:true,
            sort:true
        },
        xfer_hdr_num: {
            title:'XFER Number',
            width:150,
            url:'#/xfer/',
            field_get_id:'xfer_hdr_id',
            pin:true,
            sort:true
        },
        cus_name:{
            title:'Customer',
            width:120,
            pin:true
        },
        item_id: {
            title:'Item ID',
            width:80,
            pin:true,
            sort:true
        },
        cus_upc: {
            title:'UPC',
            width:80,
            pin:true,
            sort: true
        },
        sku: {
            title:'SKU',
            width:130,
            pin:true,
            sort:true
        },
        lot: {
            title:'LOT',
            width:70,
            pin:true
        },
        size: {
            title:'Size',
            width:70,
            pin:true,
        },
        color: {
            title:'Color',
            width:70,
            pin:true,
        },
        pieces: {
            title:'Requested Pieces',
            width:90,
            pin:true,
        },
        nw_loc:{
            title:'To Location',
            width:120,
            pin:true
        },

    };
    private whs_id;
    private ListSelectedItem:Array<any>=[];
    private Customers:Array<any>=[];
    searchForm:ControlGroup;
    private sortData={fieldname:'created_at',sort:'desc'};
    private perPage = 20;
    private isSearch = false;
    private showLoadingOverlay:boolean;
    private Pagination;
    constructor(
        private wodService:WorkOrderServices,
        private xferListService: XFERTicketListService,
        private _Func: Functions,
        private  apiService:API_Config,
        private fb: FormBuilder,
        private _router: Router,
        private _user: UserService
    ){
        this.whs_id = localStorage.getItem('whs_id');
        this.showLoadingOverlay = true;
        this.checkPermission();
        this.buildSearchForm();
    }

    // Show error when server die or else
    private parseError (err) {
        err = err.json();
        this.messages = this._Func.Messages('danger', err.errors.message);
    }

    private viewXFerTicket = false;
    private createXFer = false;
    // private editXFer = false;
    private updateXferInv = false;
    private checkPermission() {
        this._user.GetPermissionUser().subscribe(
            permissions => {
                this.viewXFerTicket = this._user.RequestPermission(permissions, 'updateXferInv');
                this.updateXferInv = this._user.RequestPermission(permissions, 'updateXferInv');

                if(this.viewXFerTicket) {
                    this.getXferTicketList(1);
                    this.getCustomersByWH();
                } else {
                    this.redirectDeny();
                }
            },
            err => {
                this.showMessage('danger', this._Func.parseErrorMessageFromServer(err));
                // this.messages=this._Func.Messages('danger', this._Func.parseErrorMessageFromServer(err));
                this.showLoadingOverlay = false;
            }


        );
    }

    private showMessage(msgStatus, msg) {
        this.messages = {
            'status': msgStatus,
            'txt': msg
        }
        jQuery(window).scrollTop(0);
    }

    redirectDeny(){
        this._router.parent.navigateByUrl('/deny');
    }

    // get list custommer

    private getCustomersByWH () {

        let params='?whs_id=' + this.whs_id + "&limit=10000";
        this.wodService.getCustomersByWH(params)
            .subscribe(
                data => {
                    this.Customers = data.data;
                },
                err => {
                    // this.parseError(err);
                    this.showMessage('danger', this._Func.parseErrorMessageFromServer(err));
                },
                () => {
                }
            );

    }

    buildSearchForm()
    {
        this.searchForm = this.fb.group({
            cus_id: [''],
            xfer_ticket_sts: [''],
            sku: [''],
            item_id: [''],
            cus_upc: [''],
            xfer_ticket_num: [''],
        });
    }

    /*
     * Reset Form Builder
     * */
    ResetSearch()
    {
        this.showLoadingOverlay = true;
        this.isSearch=false; // remove action search
        this._Func.ResetForm(this.searchForm);
        // this.getWorkOrderList(this.Pagination['current_page']);
        this.getXferTicketList(1);
    }

    private searchParam;
    Search()
    {
        this.showLoadingOverlay = true;
        this.isSearch = true;
        let data = this.searchForm.value;
        // set status search is true
        // assign to SearchQuery
        // whs_id=&cus_code=&cus_city_name=&cus_name=&cus_billing_account=&cus_country_id=&cus_state_id=&cus_postal_code=
        this.searchParam="&cus_id="+data['cus_id']+"&xfer_ticket_sts="+encodeURIComponent(data['xfer_ticket_sts'])+"&sku="
            +encodeURIComponent(data['sku'])+"&item_id="+data['item_id']+"&cus_upc="+data['cus_upc']+"&xfer_ticket_num="+data['xfer_ticket_num'];

        this.getXferTicketList(1);
    }

    getXferTicketList(page) {
        this.showLoadingOverlay = true;
        // let param = "?page=" + page + "&limit=" + this.perPage;
        let param = '';
        if(this.sortData['fieldname'] == 'wv_tf_sts_name') {
            param="?sort[xfer_ticket_sts]="+this.sortData['sort']+"&page="+page+"&limit="+this.perPage;
        } else {
            param="?sort["+this.sortData['fieldname']+"]="+this.sortData['sort']+"&page="+page+"&limit="+this.perPage;
        }
        if(this.isSearch){
            param = param + this.searchParam;
        }
        this.xferListService.getXferTicketList(param).subscribe(
            data => {
                this.xferList = this._Func.formatData(data.data);
                this.Pagination = data['meta']['pagination'];
                this.Pagination['numLinks']=3;
                this.Pagination['tmpPageCount']=this._Func.Pagination(this.Pagination);
                this.showLoadingOverlay = false;
            },
            err => {
                // this.parseError(err);
                this.showMessage('danger', this._Func.parseErrorMessageFromServer(err));
                this.showLoadingOverlay = false;
            }
        )
    }

    createXferTicket() {
        // let id = this.ListSelectedItem[0]['wv_tf_id'];
        if(!this.checkOnSelecting()) {
            let sts = 'PK';
            if (this.ListSelectedItem[0]['xfer_ticket_sts_name'] == 'Complete') {
                sts = 'CO';
            }
            let id = this.ListSelectedItem[0]['xfer_ticket_id'];
            let fileName = jQuery(this.ListSelectedItem[0]['xfer_ticket_num']).text();
            this.xferListService.printXFerTicket(id, sts, fileName, this);
            // this.getXferList(this.Pagination['current_page']);
        }
    }

    scolltoTop(){
        jQuery('html, body').animate({
            scrollTop: 0
        }, 700);
    }

    checkOnSelecting():boolean {
        let Errflag = false;
        if (this.ListSelectedItem.length > 1) {
            this.messages = this._Func.Messages('danger', this._Func.msg('XF004'));
            this.scolltoTop();
            Errflag = true;
        } else if(this.ListSelectedItem.length == 0) {
            this.messages = this._Func.Messages('danger', this._Func.msg('XF005'));
            this.scolltoTop();
            Errflag = true;
        } else if (this.ListSelectedItem[0]['xfer_ticket_sts_name'] == 'Pending') {
            this.messages = this._Func.Messages('danger', this._Func.msg('XF005'));
            this.scolltoTop();
            Errflag = true;
        }
        return Errflag ;
    }

    /*
     * Router edit
     * */
    Edit() {
        if (this.ListSelectedItem.length > 1) {
            this.messages = this._Func.Messages('danger',this._Func.msg('XF003'));
            this.scolltoTop();
        }
        else if(this.ListSelectedItem.length == 0)
        {
            this.messages = this._Func.Messages('danger',this._Func.msg('XF003'));
            this.scolltoTop();
            // this._router.navigateByUrl('/xfer/' + this.ListSelectedItem[0]['wv_tf_id'] + '/update-inv');
        }
        else if (this.ListSelectedItem[0]['xfer_ticket_sts_name'] != 'Picking') {
            this.messages = this._Func.Messages('danger', this._Func.msg('XF006'));
            this.scolltoTop();
        }
        else {
            this._router.navigateByUrl('/xfer/xfer-ticket/' + this.ListSelectedItem[0]['xfer_ticket_id'] + '/update-inv');
        }

    }

    private onPageSizeChanged($event) {
        let currentPage = this.Pagination['current_page'];
        this.perPage = $event.target.value;
        if((this.Pagination['current_page'] - 1) * this.perPage >= this.Pagination['total']) {
            currentPage = 1;
        }
        // else {
        //     this.currentPage = this.Pagination['current_page'];
        // }
        this.getXferTicketList(currentPage);
    }
}
