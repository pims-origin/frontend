import {Component, Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {API_Config, Functions} from '../../common/core/load';

@Injectable()
export class EDIServices {

    public AuthHeader = this._Func.AuthHeader();
    public AuthHeaderPost = this._Func.AuthHeaderPost();

    constructor(private _Func: Functions, private _API: API_Config, private http: Http) {

    }

    public get_EDI_List(param){
        return  this.http.get(this._API.API_EDI+'/edi-cus-list'+param, {headers: this.AuthHeader})
            .map(res => res.json());
    }
    

    public updateEDIT(data){
        return this.http.post(this._API.API_EDI+"/cus-configs", data, { headers: this._Func.AuthHeaderPostJson() })
            .map((res: Response) => res.json().data);
    }


    public EDIT_detail(whs,cusid){

        return  this.http.get(this._API.API_EDI+'/'+whs+'/'+cusid, {headers: this.AuthHeader})
            .map(res => res.json())

    }



}
