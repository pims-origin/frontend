import {Component} from '@angular/core';
import { FORM_DIRECTIVES,ControlGroup,FormBuilder, Control ,Validators} from '@angular/common';
import {API_Config, Functions, UserService} from '../../../common/core/load';
import {RouteParams,Router,RouteData } from '@angular/router-deprecated';
import {  ValidationService } from '../../../common/core/validator';
import {BoxPopupService} from "../../../common/popup/box-popup.service";
import { OrderBy } from "../../../common/pipes/order-by.pipe";
import {WMSBreadcrumb,WMSPagination,WMSMessages} from '../../../common/directives/directives';
import {GoBackComponent} from "../../../common/component/goBack.component";
declare var jQuery: any;
@Component ({
  selector: 'shipping-update',
  directives: [FORM_DIRECTIVES,GoBackComponent,WMSBreadcrumb,WMSMessages,WMSPagination],
  pipes: [OrderBy],
  providers: [ValidationService,FormBuilder, BoxPopupService],
  templateUrl: 'shipping-update.component.html',
})

export class ShippingUpdateComponent {

  constructor(
      private _func: Functions,
      private params: RouteParams,
      private _router:Router,
      private _Valid: ValidationService,
      private _user: UserService,
      private _boxPopupService: BoxPopupService,
      _RTAction: RouteData,
      private fb: FormBuilder) {

  }
}
