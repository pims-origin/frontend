import {Component, Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {API_Config, Functions} from '../../../common/core/load';

@Injectable()
export class Services {

    public AuthHeader = this._Func.AuthHeader();
    public AuthHeaderPost = this._Func.AuthHeaderPost();


    constructor(private _Func: Functions, private _API: API_Config, private http: Http)
    {

    }
    Add_Loc_To_Zone(zone_id,warehouse_id,data)
    {
        return this.http.post(this._API.API_Warehouse + '/' + warehouse_id + '/zone/' + zone_id + '/locations', data, { headers: this._Func.AuthHeaderPostJson() })
            .map((res: Response) => res.json());
    }

    DeleteItemMaster(id)
    {
        return this.http.delete(this._API.API_ItemMaster + "/" + id, { headers: this.AuthHeader });
    }
}
