import {Component} from "@angular/core";
import {Router, ROUTER_DIRECTIVES} from '@angular/router-deprecated';
import {ControlGroup, FormBuilder, Control} from '@angular/common';
import {API_Config, UserService, Functions} from '../../common/core/load';
import {BoxPopupService} from "../../common/popup/box-popup.service";
import {ShippingReportService} from './shipping-report-service';
import {WMSPagination, AgGridExtent, WMSMessages, ReportFileExporter} from '../../common/directives/directives';
import 'ag-grid-enterprise/main';
declare var jQuery: any;
declare const $:any;

@Component({
	selector: 'shipping-report',
	providers: [ShippingReportService, BoxPopupService],
	directives: [ROUTER_DIRECTIVES, AgGridExtent, WMSMessages, WMSPagination, ReportFileExporter],
	templateUrl: 'shipping-report.component.html',

})
export class ShippingReportComponent {
	private pagination:any;
	private perPage:number = 20;
	private whs_id:string;
	private messages:any;
	private searchForm:ControlGroup;
	private sortData = {fieldname: 'created_at', sort: 'desc'};
	private showLoadingOverlay:boolean = false;
	private shippingReports:Array<any> = [];
	private headerURL:string = this.apiService.API_User_Metas + '/shr';
	private columnData:any = {
		cus_code: {
			title: 'Customer Code',
			width: 120,
			pin: true,
			ver_table: '150713'
		},
		cus_name: {
			title: 'Customer Name',
			width: 120,
			pin: true
		},
		odr_num: {
			title: 'Order',
			width: 170,
			pin: true,
			id: true
		},
		odr_type: {
			title: 'Order Type',
			width: 170,
			pin: true,
			id: true
		},
		item_id: {
			title: 'Item ID',
			width: 50,
			pin: true
		},
		sku: {
			title: 'SKU',
			width: 170,
			pin: true
		},
		size: {
			title: 'Size',
			width: 50
		},
		color: {
			title: 'Color',
			width: 170
		},
		lot: {
			title: 'Lot',
			width: 50
		},
		upc: {
			title: 'UPC/EAN',
			width: 50
		},
		pack: {
			title: 'Packsize',
			width: 50
		},
		shipping_pack: {
			title: 'Shipping CTNS',
			width: 50
		},
		cube: {
			title: 'Cube',
			width: 50
		},
		piece_qty: {
			title: 'Shipped QTY',
			width: 70
		},
		ship_dt: {
			title: 'Shipped Date',
			width: 70
		},
		ship_by: {
			title: 'Shipped By',
			width: 70
		},
		cus_odr_num: {
			title: 'Customer Order #',
			width: 70
		},
		cus_po: {
			title: 'PO',
			width: 70
		},
		ship_to_name: {
			title: 'Shipping To Name',
			width: 70
		},
		ship_to_add_1: {
			title: 'Address',
			width: 70
		},
		ship_to_city: {
			title: 'City',
			width: 70
		},
		sys_state_name: {
			title: 'State',
			width: 70
		},
		ship_to_zip: {
			title: 'Zip Code',
			width: 70
		},
		carrier: {
			title: 'Carrier',
			width: 70
		},
	};
	private customers:Array<any> = [];
	private orders:Array<any> = [];
	private isLoadingOrders:boolean = false;
	private SKUs:Array<any> = [];
	private isLoadingSKUs:boolean = false;
	private currentSKU:any = {};

	// Check permission for user using this function page
	private canAccess:boolean = false;

	// Export data to csv file.
	private exportAPIUrl:string = '';
	private fileName = 'Shipping-Report.csv';

	constructor(private formBuilder:FormBuilder,
	            private shippingReportService:ShippingReportService,
	            private _Func:Functions,
	            private _user:UserService,
	            private  apiService:API_Config,
	            private _router:Router) {
		this.buildSearchForm();
		this.whs_id = localStorage.getItem('whs_id');
		this.checkPermission();
		this.initializeCurSKU();
	}

	buildSearchForm() {
		this.searchForm = this.formBuilder.group({
			cus_id: [''],
			odr_num: [''],
			sku: [''],
			odr_type: [''],
		});
	}

	initializeCurSKU() {
		this.currentSKU = {size: '', color: '', lot: ''};
	}

	private checkPermission() {
		this.showLoadingOverlay = true;
		this._user.GetPermissionUser().subscribe(
			data => {
				this.showLoadingOverlay = false;
				this.canAccess = this._user.RequestPermission(data, 'viewReport');
				if (this.canAccess) {
					this.getCustomers();
					this.getShippingReports();
					this.getOrderTypeList();
				} else {
					// redirect to deny page.
					this._router.parent.navigateByUrl('/deny');
				}
			},
			err => {
				this.messages = this._Func.Messages('danger', this._Func.parseErrorMessageFromServer(err));
				this.showLoadingOverlay = false;
			}
		);
	}

	private changePageSize($event:any) {
		this.perPage = $event.target.value;
		if (this.pagination) {
			if ((this.pagination['current_page'] - 1) * this.perPage >= this.pagination['total']) {
				this.pagination['current_page'] = 1;
			}
			this.getShippingReports(this.pagination['current_page']);
		}

	}

	private getShippingReports(pageNum:number = 1) {
		this.searchForm.value['page'] = pageNum;
		this.searchForm.value['limit'] = this.perPage;
		this.searchForm.value['ship_dt_from'] = jQuery("#ship_dt_from").val();
		this.searchForm.value['ship_dt_to'] = jQuery("#ship_dt_to").val();
		this.exportAPIUrl = this.shippingReportService.getShippingReportAPIUrl(this.whs_id, `?` + $.param(this.searchForm.value));
		this.showLoadingOverlay = true;

		this.shippingReportService.getShippingReports(this.whs_id, `?` + $.param(this.searchForm.value))
			.subscribe(
				data => {
					this.showLoadingOverlay = false;
					this.shippingReports = data.data;
					for (var i = 0; i < this.shippingReports.length; i++) {
						this.shippingReports[i]['shipping_pack'] = this.shippingReports[i]['pack'] != 0 ? Math.round(this.shippingReports[i]['piece_qty']/this.shippingReports[i]['pack']) : 0;
					}

					// paging
					this.pagination = data['meta']['pagination'];
					this.pagination['numLinks'] = 3;
					this.pagination['tmpPageCount'] = this._Func.Pagination(this.pagination);
				},
				err => {
					this.messages = this._Func.Messages('danger', err.json().errors.message);
					this.showLoadingOverlay = false;
				},
				() => {}
			);
	}

	private getCustomers() {
		const params:string = `${this.whs_id}?limit=100`;
		this.shippingReportService.getCustomersByWH(params).subscribe(
			data => {
				this.customers = data['data'];
			},
			err => {
				this.messages = this._Func.Messages('danger', err.json().errors.message);
			},
			() => {
			}
		);
	}

	loadOrders($event:any) {
		const cusId = this.searchForm.value['cus_id'];
		const orderKeySearch = this.searchForm.value['odr_num'].trim();
		const orderTypeSearch = this.searchForm.value['odr_type'].trim();
		const params = `${this.whs_id}?cus_id=${cusId}&odr_num=${orderKeySearch}&odr_type=${orderTypeSearch}&limit=20`;
		const enCodeSearchQuery = this._Func.FixEncodeURI(params);

		if (!orderKeySearch) {
			return;
		}
		this.isLoadingOrders = true;
		this.shippingReportService.getOrders(enCodeSearchQuery).debounceTime(400).distinctUntilChanged().subscribe(
			data => {
				this.isLoadingOrders = false;
				this.orders = data['data'];
			},
			err => {
				this.isLoadingOrders = false;
				this.messages = this._Func.Messages('danger', err.json().errors.message);
			},
			() => {
			}
		);
	}

	loadSKUs($event:any) {
		const cusId = this.searchForm.value['cus_id'];
		const orderNum = this.searchForm.value['odr_num'];
		const orderType = this.searchForm.value['odr_type'];
		const skuKeySearch = this.searchForm.value['sku'].trim();
		const params = `${this.whs_id}?cus_id=${cusId}&odr_num=${orderNum}&odr_type=${orderType}&sku=${skuKeySearch}&limit=20`;
		const enCodeSearchQuery = this._Func.FixEncodeURI(params);

		this.initializeCurSKU();
		if (!skuKeySearch) {
			return;
		}
		this.isLoadingSKUs = true;
		this.shippingReportService.getSKUs(enCodeSearchQuery).debounceTime(400).distinctUntilChanged().subscribe(
			data => {
				this.isLoadingSKUs = false;
				this.SKUs = data['data'];
			},
			err => {
				this.isLoadingSKUs = false;
				this.messages = this._Func.Messages('danger', err.json().errors.message);
			},
			() => {
			}
		);
	}

	reset() {
		this.resetSearchForm();
		this.initializeCurSKU();
		this.getShippingReports();
	}

	search() {
		this.getShippingReports();
	}

	selectOrderNum($event, order) {
		(<Control>this.searchForm.controls['odr_num']).updateValue(order.odr_num);
	}

	selectSKU($event, sku) {
		this.currentSKU = sku;
		(<Control>this.searchForm.controls['sku']).updateValue(sku.sku);
	}

	resetSearchForm() {
		(<Control>this.searchForm.controls['cus_id']).updateValue('');
		(<Control>this.searchForm.controls['odr_num']).updateValue('');
		(<Control>this.searchForm.controls['odr_type']).updateValue('');
		(<Control>this.searchForm.controls['sku']).updateValue('');
		jQuery("#ship_dt_from").val("");
		jQuery("#ship_dt_to").val("");
	}

	private updateMessages(messages) {
		this.messages = messages;
	}

	private orderTypes = [];
	private getOrderTypeList () {
		this.shippingReportService.getOrderTypeList()
			.subscribe(
				data => {
					this.orderTypes = data.data;
				}
			);
	}
}
