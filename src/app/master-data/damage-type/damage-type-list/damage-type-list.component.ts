import {Component} from '@angular/core';
import { Router } from '@angular/router-deprecated';
import {API_Config, UserService, Functions} from '../../../common/core/load';
import {DamageTypeService} from '../damage-type.service';
import { FORM_DIRECTIVES,ControlGroup,FormBuilder, Control} from '@angular/common';
import { Http } from '@angular/http';
import {WMSBreadcrumb} from '../../../common/directives/directives';
declare var jQuery: any;

@Component({
    selector: 'damage-type-list',
    directives: [FORM_DIRECTIVES,WMSBreadcrumb],
    providers: [DamageTypeService],
    templateUrl: 'damage-type-list.component.html',

})
export class DamageTypeListComponent{
    private damageTypeList: Array<any> = [];
    private selectedAll = false;
    public listSelectedItem = [];
    private Pagination;
    private totalCount = 0;
    private tmpPageCount;
    private pageCount=0;
    private ItemOnPage=0;
    private currentPage = 1;
    private perPage = 20;
    private numLinks = 3;
    private messages;
    searchForm: ControlGroup;
    public loading = [];
    public showLoadingOverlay = false;
    private hasEditPermission = false;
    private hasCreatePermission = false;
    private viewUser = false;
    private searchCriteria = {};
    constructor(
        private _http: Http,
        private _func: Functions,
        private _user: UserService,
        private fb: FormBuilder,
        private _damageTypeService: DamageTypeService,
        private _router: Router) {

        this.builderSearchForm();
        this.searchCriteria = this._func.genSearchCriteriaFromForm(this.searchForm);
        this.checkPermission();
    }

    // Check permission for user using this function page
    private checkPermission() {
        this.showLoadingOverlay = true;
        this._user.GetPermissionUser().subscribe(
            data => {
                this.hasEditPermission = this._user.RequestPermission(data, 'editDamageType');
                this.hasCreatePermission = this._user.RequestPermission(data, 'createDamageType');
                this.viewUser = this._user.RequestPermission(data,'viewDamageType');
                /* Check orther permission if View allow */
                if(!this.viewUser) {
                    this._router.parent.navigateByUrl('/deny');
                }
                else {
                    this.getDamageTypeList();
                }
            },
            err => {
                this.showMessage('danger', this._func.parseErrorMessageFromServer(err));
                this.showLoadingOverlay = false;
            }
        );
    }

    private getDamageTypeList(page = null) {
        let param="?page="+page+"&limit="+this.perPage;
        if(Object.keys(this.searchCriteria).length !== 0) // check if searchCriteria Object not empty  ( {} )
        {
            param += '&dmg_code=' + this.searchCriteria['dmg_code'] +
                '&dmg_name=' + this.searchCriteria['dmg_name'];
        }
        if(this.sortQuery != '') {
            param += '&'+this.sortQuery;
        }
        this.loading['getDamageType'] = true;
        this.showLoadingOverlay = true;
        this._damageTypeService.getDamageType(param).subscribe(
            data => {
                this.selectedAll = false;
                this.listSelectedItem=[];
                this.damageTypeList = data.data;
                //pagin function
                this.Pagination=data['meta']['pagination'];
                this.Pagination['numLinks']=3;
                this.Pagination['tmpPageCount']=this._func.Pagination(this.Pagination);

                this.loading['getDamageType'] = false;
                this.showLoadingOverlay = false;
            },
            err => {},
            () => {}
        );
    }

    private onPageSizeChanged($event) {
        this.perPage = $event.target.value;
        if((this.Pagination['current_page'] - 1) * this.perPage >= this.Pagination['total']) {
            this.currentPage = 1;
        }
        else {
            this.currentPage = this.Pagination['current_page'];
        }
        this.getDamageTypeList(this.currentPage);
    }

    private editSelectedItem() {
        if (this.listSelectedItem.length > 1) {
            this.messages = this._func.Messages('danger',this._func.msg('CF004'));
        }
        else {
            if(this.listSelectedItem.length) {
                this._router.navigateByUrl('/master-data/damage-type/' + this.listSelectedItem[0] + '/edit');
            }
            else {
                this.messages = this._func.Messages('danger',this._func.msg('CF005'));
            }
        }
    }

    private checkAll(event) {
        this.selectedAll = !this.selectedAll;
        // set empty array  ListSelectedItem
        this.listSelectedItem=[];
        // Loop
        this.damageTypeList.forEach((item) => {
            item['selected'] = this.selectedAll;
            if(this.selectedAll) {
                // if check all = true , pushing to ListSelectedItem
                this.listSelectedItem.push(item['dmg_id']);
            }
        })
    }

    private checkSelectItem(item) {
        let id = item['dmg_id'];
        item.selected = !item.selected;
        this.selectedAll=false;
        if (item.selected) {
            this.listSelectedItem.push(id);
            this.setItemListSelected(id, true);
            if(this.listSelectedItem.length==this.damageTypeList.length)
            {
                this.selectedAll=true;
            }
        }
        else {
            let el = this.listSelectedItem.indexOf(id);
            // remove item out of array
            this.listSelectedItem.splice(el, 1);
            this.setItemListSelected(id, false);
        }
    }

    private setItemListSelected(id,value) {
        this.damageTypeList.forEach((item) => {
            if(item['chg_type_id'] == id) {
                item['selected'] = value;
            }
        });
    }

    private filterList(num) {
        this.currentPage = num;
        this.getDamageTypeList(num);
    }

    search(data) {
        this.messages = null;
        this.listSelectedItem = [];
        this.loading['getDamageType'] = true;
        this.showLoadingOverlay = true;
        let param="?page="+1+"&limit="+this.perPage;
        this.searchCriteria = this._func.genSearchCriteriaFromForm(this.searchForm);

        let encodeSearchQuery=param+"&dmg_code="+ encodeURIComponent(this.searchCriteria['dmg_code'])+
                                "&dmg_name="+encodeURIComponent(this.searchCriteria['dmg_name']);
        // let encodeSearchQuery = this._func.FixEncodeURI(new_query);
        if(this.sortQuery != '') {
            encodeSearchQuery += '&'+this.sortQuery;
        }
        this.loading['searching']=true;

        this._damageTypeService.search(encodeSearchQuery).subscribe(
            data => {
                // disable loading icon
                this.loading['searching']=false;
                this.selectedAll = false;

                this.damageTypeList = data.data;
                //pagin function
                this.Pagination=data['meta']['pagination'];
                this.Pagination['numLinks']=3;
                this.Pagination['tmpPageCount']=this._func.Pagination(this.Pagination);

                this.loading['getDamageType'] = false;
                this.showLoadingOverlay = false;
            },
            err =>{
                this.messages = this._func.Messages('danger','Not found');
            },
            () => {}
        );
    }

    resetSearch(data) {
        // if(data.dmg_code || data.dmg_name) {
            (<Control>this.searchForm.controls["dmg_code"]).updateValue("");
            (<Control>this.searchForm.controls["dmg_name"]).updateValue("");
            this.searchCriteria = this._func.genSearchCriteriaFromForm(this.searchForm);
            this.getDamageTypeList(this.Pagination['current_page']);
        // }
    }

    private builderSearchForm() {
        this.searchForm =this.fb.group({
            'dmg_code': [''],
            'dmg_name': [''],
        });
    }

    private showMessage(msgStatus, msg) {
        this.messages = {
            'status': msgStatus,
            'txt': msg
        }
        jQuery(window).scrollTop(0);
    }

    private FieldsToSortFlag = {
        dmg_code: false,
        dmg_name: false,
    };
    private countClick = {
        dmg_code: 3,
        dmg_name: 3,
    }
    private sortQuery = 'sort[dmg_code]=asc';
    sort(field) {
        if(this.countClick[field] == 3) {
            this.countClick[field] = 0;
        }

        this.countClick[field] ++;
        if(this.countClick[field] == 3) {
            this.sortQuery = 'sort[dmg_code]=asc';
        }
        else {
            this.FieldsToSortFlag[field] = !this.FieldsToSortFlag[field];

            this.sortQuery = 'sort['+field+']';
            if(this.FieldsToSortFlag[field]) {
                this.sortQuery += '=asc';
            } else {
                this.sortQuery += '=desc';
            }
        }
        if(field) {
            for (let sortField in this.FieldsToSortFlag ) {
                if(sortField != field) {
                    this.FieldsToSortFlag[sortField] = false;
                    this.countClick[sortField] = 3;
                }
            }
        }
        this.getDamageTypeList(this.currentPage);
    }
}
