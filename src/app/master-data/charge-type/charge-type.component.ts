import {Component} from '@angular/core';
import {Router, RouterLink, RouteConfig, ROUTER_DIRECTIVES} from '@angular/router-deprecated';
import {ChargeTypeListComponent} from "./charge-type-list/charge-type-list.component";
import {CruChargeTypeComponent} from "./cru-charge-type/cru-charge-type.component";

@Component ({
    selector: 'charge-type',
    directives: [ROUTER_DIRECTIVES],
    template: `<router-outlet></router-outlet>`,
})

@RouteConfig([
    { path: '/', component:ChargeTypeListComponent , name: 'Charge Type List', useAsDefault: true},
    { path: '/new', component:CruChargeTypeComponent  , name: 'Add Charge Type ', data: {action: 'new'}},
    { path: ':id', component:CruChargeTypeComponent  , name: 'View Charge Type',data: {action: 'view'}},
    { path: ':id/edit', component:CruChargeTypeComponent  , name: 'Edit Charge Type', data: {action: 'edit'}},
])
export class ChargeTypeComponent {


}
