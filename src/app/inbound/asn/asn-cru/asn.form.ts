import { SkipSelf,Host,Optional,Component,Output, Input, forwardRef } from '@angular/core';
import { FORM_DIRECTIVES,ControlContainer,ControlGroup,FormBuilder, ControlArray ,Control,Validators} from '@angular/common';
import {Functions} from '../../../common/core/load';
import { ValidationService } from '../../../common/core/validator';
import {FormBuilderFunctions} from '../../../common/core/formbuilder.functions';
declare var jQuery: any;
@Component ({
    providers: []
})
export class ASNFormProvider
{
    constructor(
                @Optional() @Host() @SkipSelf() private parentForm: ControlContainer,
                private fb: FormBuilder,
                private _Valid: ValidationService,
                private _globalFunc:Functions,
                private fbdFunc:FormBuilderFunctions) {}

    addNewItem(controlArray:ControlArray,data={}) {
        // data['dtl_po'] = 'po' + Date.now();
        data['dtl_height'] = 1;
        data['dtl_length'] = 1;
        data['dtl_weight'] = 1;
        data['dtl_width'] = 1;

        controlArray.push(
            new ControlGroup({
                dtl_itm_id: new Control(data['dtl_item_id']),
                asn_dtl_sts : new Control(data['asn_dtl_sts']),
                asn_dtl_sts_code : new Control(data['asn_dtl_sts_code']),
                gr_hdr_num : new Control(data['gr_hdr_num']),
                asn_dtl_id: new Control(data['asn_dtl_id']),
                asn_dtl_cus_upc: new Control(data['asn_dtl_cus_upc'],Validators.compose([this._Valid.checkUPCdigitCode,this._Valid.checkUPClength])),
                dtl_sku: new Control(data['dtl_sku'],Validators.compose([Validators.required,this._Valid.validateSpace])),
                dtl_size: new Control(data['dtl_size']),
                dtl_color: new Control(data['dtl_color']),
                dtl_lot: new Control(data['asn_dtl_lot'], Validators.compose([this._Valid.validateEco])),
                dtl_des : new Control(data['dtl_des']),
                dtl_uom_id: new Control(data['dtl_uom_id'] ? data['dtl_uom_id'] : '',Validators.compose([Validators.required])),
                dtl_uom_name: new Control(data['dtl_uom_name']),
                dtl_pack :new Control(data['asn_dtl_pack']),
                dtl_ctn_ttl: new Control(data['dtl_ctn_ttl'],Validators.compose([Validators.required,this._Valid.inPutOnlyNumber])),
                dtl_length: new Control(data['dtl_length'],Validators.compose([Validators.required,this._Valid.isZero,this._Valid.isDecimalNumber])),
                dtl_width: new Control(data['dtl_width'],Validators.compose([Validators.required,this._Valid.isZero,this._Valid.isDecimalNumber])),
                dtl_height: new Control(data['dtl_height'],Validators.compose([Validators.required,this._Valid.isZero,this._Valid.isDecimalNumber])),
                dtl_weight: new Control(data['dtl_weight'],Validators.compose([Validators.required,this._Valid.isZero,this._Valid.isMaxDecimalNumber])),
                dtl_crs_doc: new Control(data['dtl_crs_doc'],this._Valid.validateInt),
                dtl_po: new Control(data['dtl_po'], Validators.required),
                dtl_po_date: new Control(data['dtl_po_date']),
                created_from: new Control(data['created_from']),
                tmp_asn_dtl_sts_code : new Control(data['asn_dtl_sts_code']),
                flag_dis_we : new Control(parseFloat(data['dtl_weight']) > 0 ? true: false),
                flag_dis_wi : new Control(parseFloat(data['dtl_width']) > 0 ? true: false),
                flag_dis_h : new Control(parseFloat(data['dtl_height']) > 0 ? true: false),
                flag_dis_l : new Control(parseFloat(data['dtl_length']) > 0 ? true: false),
                expired_dt:new Control(data['expired_dt']),
                prod_line : new Control(data['prod_line']),
                cmp : new Control(data['cmp'])
            })
        );

    }

    asnFormBuilder(data={},itemsTable:ControlArray,whs_id:any){

       let asn_details = {};
       if(data['asn_details']){
           if(data['asn_details'].length){
               asn_details = data['asn_details'][0];
           }
       }

       return this.fb.group({
            cus_id:[data['cus_id'] ? data['cus_id'] : '', Validators.compose([Validators.required])],
            asn_hdr_num:[data['asn_hdr_num']],
            asn_ref:[data['asn_hdr_ref'] ? data['asn_hdr_ref'] : ''],
            ctnr_num : [asn_details['ctnr_num'] ? asn_details['ctnr_num'] : '', Validators.compose([Validators.required])],
            ctnr_id : [asn_details['ctnr_id'] ? asn_details['ctnr_id'] : 0],
            ctnr_id_preview : [asn_details['ctnr_id'] ? asn_details['ctnr_id'] : 0],
            asn_hdr_itm_ttl:[data['asn_hdr_itm_ttl']],
            exp_date:[data['asn_hdr_ept_dt'] ? data['asn_hdr_ept_dt'] : ''],
            measurement_code: [data['sys_measurement_code'] ? data['sys_measurement_code'] : 'IN'],
            details:itemsTable,
            whs_id:[whs_id],
            asn_sts_name: [data['asn_sts_name'] ? data['asn_sts_name'] : 'New'],
            asn_sts: [data['asn_sts'] ? data['asn_sts'] : 'NW'],
            ctnr_sts : [data['ctnr_sts'] ? data['ctnr_sts'] : 0],
            asn_dtl_ept_dt :[asn_details['asn_dtl_ept_dt'] ? asn_details['asn_dtl_ept_dt'] :'', Validators.compose([Validators.required])],
       }, { validator: this.checkExpectedDate('asn_dtl_ept_dt') });

    }

    /*=========================
     * selectedContainer
     * =======================*/
    public selectedContainer(controlGroup:ControlGroup,ctn){
        this.fbdFunc.setValueControl(controlGroup,'ctnr_num',ctn['ctnr_num']);
        this.fbdFunc.setValueControl(controlGroup,'ctnr_id',ctn['ctnr_id']);
    }

    /*=================================
     * Check Exist Container
     * ===============================*/
    public checkExistContainer(controlGroup:ControlGroup,key,dataArr) {

        this.fbdFunc.setNewValue(controlGroup,'ctnr_num','newKey',true);
        for (let i in dataArr){
            let item = dataArr[i];
            if (key == this._globalFunc.trim(item['ctnr_num'])) {
                this.fbdFunc.setValueControl(controlGroup,'ctnr_num',item['ctnr_num']);
                this.fbdFunc.setValueControl(controlGroup,'ctnr_id',item['ctnr_id']);
                this.fbdFunc.setNewValue(controlGroup,'ctnr_num','newKey',false);
                return;
            }
        }
        this.fbdFunc.setValueControl(controlGroup,'ctnr_id',0);

    }

    public selectedItem(controlGroup:ControlGroup,dataItem={})
    {
        // set default dimension
        dataItem['height'] = dataItem['height'] && Number(dataItem['height']) !== 0? dataItem['height'] : 1;
        dataItem['length'] = dataItem['length'] && Number(dataItem['length']) !== 0? dataItem['length'] : 1;
        dataItem['weight'] = dataItem['weight'] && Number(dataItem['weight']) !== 0? dataItem['weight'] : 1;
        dataItem['width'] = dataItem['width'] && Number(dataItem['width']) !== 0? dataItem['width'] : 1;

        controlGroup['disabled']=true;
        this.fbdFunc.setValueControl(controlGroup,'dtl_itm_id',dataItem['itm_id']);
        this.fbdFunc.setValueControl(controlGroup,'asn_dtl_cus_upc',dataItem['cus_upc']);
        this.fbdFunc.setValueControl(controlGroup,'dtl_des',dataItem['description']);
        this.fbdFunc.setValueControl(controlGroup,'dtl_sku',dataItem['sku']);
        this.fbdFunc.setValueControl(controlGroup,'dtl_lot',dataItem['lot']);
        this.fbdFunc.setValueControl(controlGroup,'dtl_size',dataItem['size']);
        this.fbdFunc.setValueControl(controlGroup,'dtl_color',dataItem['color']);
        this.fbdFunc.setValueControl(controlGroup,'dtl_itm_id',dataItem['item_id']);
        this.fbdFunc.setValueControl(controlGroup,'dtl_weight',dataItem['weight']);
        this.fbdFunc.setValueControl(controlGroup,'dtl_height',dataItem['height']);
        this.fbdFunc.setValueControl(controlGroup,'dtl_width',dataItem['width']);
        this.fbdFunc.setValueControl(controlGroup,'dtl_length',dataItem['length']);
        this.fbdFunc.setValueControl(controlGroup,'dtl_pack',dataItem['pack']);
        this.fbdFunc.setValueControl(controlGroup,'prod_line',dataItem['product_line']);
        this.fbdFunc.setValueControl(controlGroup,'campaign',dataItem['cmp']);

    }

    public checkKeyExist(controlGroup:ControlGroup, key:string, searchdata_array:Array<any>, fieldname) {
        let string = key;
        for (let i in searchdata_array) {
            let item = searchdata_array[i];
            if (string == this._globalFunc.trim(item[fieldname])) {
                this.selectedItem(controlGroup, item);
                return;
            }
        }
        (<Control>controlGroup['controls']['dtl_sku']).setErrors({invaildValue: true});
    }

    public checkExpqty(controlGroup:ControlGroup){
        
        let item = controlGroup.value;
        let control = (<Control>controlGroup['controls']['dtl_crs_doc']);
        this.removeErrorControl(control,'invalid_expqty');
        if(parseInt(item['dtl_ctn_ttl']) <  parseInt(item['dtl_crs_doc'])){
            control.setErrors({invalid_expqty: true});
        }
        
    }

    public removeErrorControl(control:Control,fieldname_to_delete){

        if(control.hasError(fieldname_to_delete)){
            delete  control.errors[fieldname_to_delete];
            if(!Object.keys(control.errors).length) {
                (<Control>control).setErrors(null);
            }
        }

    }

    public setTouched(control:Control){
        control['control']['xtouched'] = true;
    }

    checkExpectedDate(asn_dtl_ept_dt:string) {
        return (group:ControlGroup):{[key:string]:any} => {
          let currentDay = new Date(this.toDay());
          let dataDate = group.controls[asn_dtl_ept_dt].value ? new Date(group.controls[asn_dtl_ept_dt].value) : null;
    
          if (dataDate && dataDate < currentDay) {
            group.controls[asn_dtl_ept_dt].setErrors({invalidDate: true});
          }
          return null;
        }
      }

    toDay() {
    let today = new Date();
    let dd = '' + today.getDate();
    let mm = '' + (today.getMonth() + 1); //January is 0!
    let yyyy = today.getFullYear();
    if (parseInt(dd) < 10) {
        dd = '0' + dd
    }
    if (parseInt(mm) < 10) {
        mm = '0' + mm
    }
    return mm + '/' + dd + '/' + yyyy;
    }
}
