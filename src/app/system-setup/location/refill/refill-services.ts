import {Component, Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {API_Config, Functions} from '../../../common/core/load';

@Injectable()
export class RefillServices {

    public AuthHeader = this._Func.AuthHeader();
    public AuthHeaderPost = this._Func.AuthHeaderPost();

    constructor(private _Func: Functions, private _API: API_Config, private http: Http) {

    }

    getCustomersByWH($params) {
        return  this.http.get(this._API.API_CUSTOMER_MASTER + '/customer-warehouses' + $params + '&sort[cus_name]=asc' , {headers: this.AuthHeader})
            .map(res => res.json());
    }

    addNewRefill(data){
        return this.http.post(this._API.API_Refill,data, { headers: this._Func.AuthHeaderPostJson() })
            .map((res: Response) => res.json().data);
    }

    updateRefill(id,data){
        return this.http.post(this._API.API_Refill, data, { headers: this._Func.AuthHeaderPostJson() })
            .map((res: Response) => res.json().data);
    }

    getRefillList(locId){
        return  this.http.get(this._API.API_Refill+'/location'+'/'+locId, {headers: this.AuthHeader})
            .map(res => res.json());
    }

    searchItems(params){
        return this.http.get(this._API.API_ItemMaster+params,{ headers: this.AuthHeader })
            .map((res: Response) => res.json());
    }


}
