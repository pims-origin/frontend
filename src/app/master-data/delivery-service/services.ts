import {Component, Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {API_Config, Functions} from '../../common/core/load';

@Injectable()

export class Services {
    public AuthHeader = this._Func.AuthHeader();
    public AuthHeaderPost = this._Func.AuthHeaderPostJson();

    constructor(private _Func:Functions, private _API:API_Config, private http:Http) {

    }

    public getDeliveryService(param='') {
        return this.http.get(this._API.API_ORDERS_CORE + '/delivery-service'+param, {headers: this.AuthHeader})
            .map((res:Response) => res.json());
    }

    public createDeliveryService(data:any){
        return this.http.post(this._API.API_ORDERS_CORE + '/delivery-service' ,data, {headers: this.AuthHeaderPost})
            .map((res:Response) => res.json());
    }

}
