import {Component, DynamicComponentLoader, ElementRef, Injector} from '@angular/core';
import { FORM_DIRECTIVES,ControlGroup,FormBuilder, Control ,Validators} from '@angular/common';
import { Http } from '@angular/http';
import { Router, RouterLink, ROUTER_DIRECTIVES, RouteParams } from '@angular/router-deprecated';
import 'ag-grid-enterprise/main';
import 'rxjs/add/operator/map';
import {API_Config} from "../../common/core/API_Config";
import {Functions} from "../../common/core/functions";
import {GoodsReceiptServices} from "../goods.receipt.service";
import {JwtHelper} from "angular2-jwt/angular2-jwt";
import {WMSMessages, WMSBreadcrumb} from '../../common/directives/directives';
import {UserService} from "../../common/users/users.service";
declare var jQuery: any;

@Component ({
    selector: 'edit-goods-receipt',
    directives: [WMSMessages, WMSBreadcrumb],
    templateUrl: 'edit-goods-receipt.component.html',
    providers: [GoodsReceiptServices],
})

export class EditGoodsReceiptComponent {

    private id: any;
    private listNull = {};
    private listNull2 = {};
    private dataListContainer = {};
    private messages:any;
    private tmpItem = [null];
    private dataListMeasurements = [];
    private dataListContainersByASN = [];
    private dataListCustomer = [];
    private dataAsn = [];
    private loc_whs_id: any;
    private messagesItem = {};
    private dataGoodReceipt = [];
    private flagSubmitValid = false;
    private timeoutHideMessage;
    private status = {
        'RE' : '',
        'RG' : ''
    };

    private flagDisableFields = false;

    // Construct
    constructor (
        private _router:Router,
        private _goodsReceiptServices: GoodsReceiptServices,
        params: RouteParams,
        private _Func: Functions,
        public jwtHelper: JwtHelper,
        private _user: UserService
    ) {

        // Check permission
        this.checkPermission();
        this.id = params.get("id");
        this.getGoodsReceiptById();

    }

    // Check Permission
    private checkPermission() {

        this._user.GetPermissionUser().subscribe(

            data => {

                var flag = this._user.RequestPermission(data,'editGoodsReceipt');

                if(!flag) {
                    this._router.parent.navigateByUrl('/deny');
                }
            },
            err => {

                this.showMessage('danger', this._Func.parseErrorMessageFromServer(err));

            }
        );
    }

    private showMessage(msgStatus, msg) {

        this.messages = {
            'status': msgStatus,
            'txt': msg
        }
        jQuery(window).scrollTop(0);
    }

    private requestPermission(permission, permission_req) {

        if (permission && permission.length) {

            for (var per of permission) {

                if (per.name == permission_req) {

                    return true;
                }
            }

        }

        return false;

    }

    // Get goods receipt by id
    private getGoodsReceiptById() {

        var tar_get = this;
        this._goodsReceiptServices.getGoodsReceiptById(this.id)
            .subscribe(
                data => {

                    this.dataGoodReceipt = data.data;

                },
                err => {

                    this.parseError(err);
                },
                () => {

                    if (tar_get.dataGoodReceipt['gr_sts_code'].toLowerCase() == 're') {

                        tar_get.flagDisableFields = true;

                    }
                }
            );
    }

    // cancel gôt list
    private cancel () {

        this._router.parent.navigateByUrl('/goods-receipt');
    }

    /*
     * Disable input text
     * */
    checkInputNumber(evt,value)
    {
        evt = evt || window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (!evt.ctrlKey && charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 46){
            evt.preventDefault();
        }
    }

    // save
    private save ($status: string) {

        if (this.flagDisableFields) {

            return false;
        }

        this.flagSubmitValid = false;
        this.messages = false;
        var tar_get = this;

        // Check logic for GR
        // Case only submit button
        this.listNull2 = {};
        var submit = true;

        if ($status == '0') {

            this.flagSubmitValid = true;
            jQuery('form').find('input[id^=gr_dtl_act_ctn_ttl_]').each(function(i, field) {

                tar_get.filterForm2(field, 'required_int_greater0');

            });

        } else {

            jQuery('form').find('input[id^=gr_dtl_act_ctn_ttl_]').each(function(i, field) {

                tar_get.filterForm2(field, 'int_greater0');

            });

        }

        this.calTotalEP();
        jQuery.each( this.listNull2, function( key, value ) {

            if (value) {

                submit = false;
                return false;
            }
        });
        if (!submit) {

            return false;
        }

        var params = jQuery("form").serializeArray();

        var paramsJson = this.buildParams(params);
        paramsJson['gr_received_pending'] = $status;

        this._goodsReceiptServices.updateGoodsReceipt(this.id, JSON.stringify(paramsJson))
            .subscribe(
                data => {

                    this.messages = this._Func.Messages('success', 'The ' + this.dataGoodReceipt['gr_hdr_num'] + ' was updated successfully!');
                },
                err => {

                    this.parseError(err);
                },
                () => {

                    let parent = this;

                    var statusGR = tar_get.dataGoodReceipt['gr_sts_code'].toLowerCase();

                    if ($status == '0' && (statusGR == 'rg' || statusGR == 'rd')) {

                        var tmp_checked = jQuery('input[id^=gr_dtl_is_dmg]:checkbox:checked').length;

                        if (tmp_checked > 0) {  // goto damage

                            setTimeout(function(){

                                parent._router.parent.navigateByUrl('/goods-receipt/' + parent.id + '/damaged-carton');
                            }, 1100);

                        } else {  // goto assign pallet

                            setTimeout(function(){

                                parent._router.parent.navigateByUrl('/goods-receipt/' + parent.id + '/pallet-assignment');
                            }, 1100);

                        }

                    } else {

                        setTimeout(function(){

                            parent._router.parent.navigateByUrl('/goods-receipt');
                        }, 1100);
                    }

                }
            );
    }

    // Filter form only show choose one
    private filterForm2 ($target, flag = 'required') {

        try {

            switch (flag) {

                case 'required_int_greater0':

                    this.listNull2[$target.id + "_required"] = false;
                    this.listNull2[$target.id + "_int"] = false;
                    this.listNull2[$target.id + "_greater0"] = false;

                    if ($target.value == "") {

                        this.listNull2[$target.id + "_required"] = true;

                    } else if(!this.validateInt($target.value)){

                        this.listNull2[$target.id + "_int"] = true;

                    } else if ($target.value < 1) {

                        this.listNull2[$target.id + "_greater0"] = true;
                    }

                    break;

                case 'int_greater0':

                    this.listNull2[$target.id + "_int"] = false;
                    this.listNull2[$target.id + "_greater0"] = false;

                    if(!this.validateInt($target.value) && $target.value != ''){

                        this.listNull2[$target.id + "_int"] = true;

                    } else if ($target.value < 1  && $target.value != '') {

                        this.listNull2[$target.id + "_greater0"] = true;
                    }

                    break;

                case 'int':
                    if (!this.validateInt($target.value) && flag == 'int') {

                        this.listNull2[$target.id + "_int"] = true;

                    } else {

                        this.listNull2[$target.id + "_int"] = false;
                    }
                    break;

                default:
                    break;

            }

            return false;

        } catch (err) {

            return false;
        }


    }

    // Validate Int
    private validateInt(number = '') {

        var re = /^([0-9]*)$/;
        var re2 = /^0([0-9]*)$/;
        if (re.test(number)) {

            if (number.length > 1 && re2.test(number)) {

                return false;

            }

            return true;
        }

        return false;
    }

    private removeRequired($target) {

        if ($target.value != "") {

            this.listNull2[$target.id + "_required"] = false;

        }

    }

    // Validate float
    private validateFloat(float ) {

        var re = /^([0-9,.]*)$/;
        return re.test(float);

    }

    // Validate number
    private validateNumber(number) {

        var re = /^([0-9]*)$/;
        return re.test(number);
    }

    // value = .. return true
    private validateMultidot(float) {

        var rgx = /\.\./;
        return (rgx.test(float));
    }

    // build params
    private buildParams (params) {

        var object = {};
        object['gr_in_note'] = jQuery("#gr_in_note")[0].value;
        object['gr_ex_note'] = jQuery("#gr_ex_note")[0].value;


        var detail = {};
        var tmp = [];

        var target = this;
        jQuery('form').find("input[id^=gr_dtl_act_ctn_ttl_]").each(function(j, obj) {

            var index = obj.id.replace('gr_dtl_act_ctn_ttl_', '');
            detail['asn_dtl_id']  = target.dataGoodReceipt['gr_details'][index]['dtl_asn_dtl_id'];
            detail['gr_dtl_act_ctn_ttl']  = jQuery("#gr_dtl_act_ctn_ttl_" + index)[0].value;

            if(jQuery("#gr_dtl_is_dmg_" + index).is(':checked'))
                detail['gr_dtl_is_dmg'] = '1';
            else
                detail['gr_dtl_is_dmg'] = '0';

            tmp.push(detail);
            detail = {};

        });

        object['details'] = tmp;
        return object;
    }


    // Get asn by id
    private getAsnByID() {

        this._goodsReceiptServices.getAsnByID(this.id)
            .subscribe(
                data => {

                    this.dataAsn = data.data;
                },
                err => {

                    this.parseError(err);
                },
                () => {}
            );
    }

    // Cal disc
    private calDisc ($event) {


        if (this.validateNumber($event.target.value)) {

            var index = $event.target.id.replace("gr_dtl_act_ctn_ttl_", "");

            var act_ctn = parseInt($event.target.value);

            var ctn = parseInt(jQuery("#dtl_gr_dtl_ept_ctn_ttl_" + index).html());

            var tmpCal = act_ctn - ctn;

            if(isNaN(tmpCal)) {

                tmpCal = 0;
            }

            var html  = '<span style="color: #D91E18">' + tmpCal + '</span>';

            if (tmpCal == 0) {

                var html  = '<span>' + tmpCal + '</span>';
            }

            jQuery("#dtl_gr_dtl_disc_" + index).html(html);

        }

    }

    // Show error when server die or else
    private parseError (err) {

        this.messages = {'status' : 'danger', 'txt' : this._Func.parseErrorMessageFromServer(err)};
    }

    private changeColor($event, objectText = '') {

        if (objectText == 'input') {

            jQuery("#gr_dtl_is_dmg_" + index).prop("checked", true);
            var index = $event.target.id.replace("gr_dtl_is_dmg_", "");
            jQuery($event.target).hide();
            jQuery("#checked_" + index).show();
        }else if (objectText == 'check') {

            var index = $event.target.id.replace("checked_", "");
            jQuery($event.target).hide();
            jQuery("#gr_dtl_is_dmg_" + index).show();
            jQuery("#gr_dtl_is_dmg_" + index).prop("checked", false);
        }

    }

    private calTotalEP() {

        var target = this;
        var tmpTotal = 0;
        jQuery('form').find("input[id^=gr_dtl_act_ctn_ttl_]").each(function(j, obj) {

            if (obj.value != '') {

                tmpTotal += parseInt(obj.value);
                target.listNull2['gr_dtl_act_ctn_ttl_' + j +'_less9999'] = false;
            }
        });
        if (tmpTotal > 99999) {

            jQuery('form').find("input[id^=gr_dtl_act_ctn_ttl_]").each(function(j, obj) {

                target.listNull2['gr_dtl_act_ctn_ttl_' + j +'_less9999'] = true;
            });
        }
    }

}
