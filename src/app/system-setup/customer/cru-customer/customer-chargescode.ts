import {Component } from '@angular/core';
import {ControlGroup, Control ,ControlArray,Validators} from '@angular/common';
import {CustomerFunctions} from './customer-functions';
import { ValidationService } from '../../../common/core/validator';
import { Functions} from '../../../common/core/load';
@Component({
    providers: [Functions,CustomerFunctions,ValidationService]
})
export class CustomerChargesCode {

    constructor(private  cusFun:CustomerFunctions,
                private _Valid: ValidationService,
                private func:Functions){}

    /*===================================
     * selectedItemChargCode
     * =================================*/
    public selectedItemChargCode(controlArray:ControlArray,row,chargcode,click=false)
    {
        controlArray['controls'][row]['disabled']=true;
        this.cusFun.setValueControlArray(controlArray,row,'chg_code_id',chargcode['chg_code_id']);
        if(click){
            this.cusFun.setValueControlArray(controlArray,row,'chg_code',chargcode['chg_code']);
        }
        this.cusFun.setValueControlArray(controlArray,row,'chg_type_id',chargcode['chg_type_id']);
        this.cusFun.setValueControlArray(controlArray,row,'chg_uom_id',chargcode['chg_uom_id']);
        this.cusFun.setValueControlArray(controlArray,row,'chg_code_des',chargcode['chg_code_des']);
    }

    /*===================================
     * checkChargCodeExist
     * =================================*/
    public checkChargCodeExist(controlArray:ControlArray,row,str:string,arr:Array<any>)
    {

        let string=str;
        for(let i in arr){
            if(string==this.func.trim(arr[i]['chg_code'])) {
                this.selectedItemChargCode(controlArray,row,arr[i],false);
                return;
            }
        }
        // set error if not found
        if(str){
            this.cusFun.setErrorControlArray(controlArray,row,'chg_code',{'chg_code_not_exited':true})
        }

    }


    /*===================================
     * checkDuplicatedChargeCode
     * =================================*/
    checkDuplicatedChargeCode(controlArray:ControlArray)
    {
        let fieldcheckDup=['whs_id','chg_code'];
        this.cusFun.checkDuplicatedRowByField(controlArray,fieldcheckDup);
    }

    /*
     * Update value Item row billing
     * */
    public resetChargesCodeRow(controlArray:ControlArray,row)
    {
        controlArray.controls[row]['disabled']=false;
        let fieldReset=['chg_code_id','chg_type_id','chg_uom_id','chg_code_des'];
        for(let key in fieldReset){
                this.cusFun.setValueControlArray(controlArray,row,fieldReset[key],'');
        }
    }

    public addNewChargeCodeItem(controlArray:ControlArray,item={}){

        var tmp = {
            'whs_id':new Control(item['whs_id'] ? item['whs_id'] : '', Validators.required),
            'chg_code_id' :new Control(item['chg_code_id']),
            'chg_code':new Control(item['chg_code'], Validators.compose([Validators.required, this._Valid.validateSpace])),
            'chg_type_id':new Control(item['chg_type_id'] ? item['chg_type_id'] : '', Validators.required),
            'chg_cd_price':new Control(item['chg_cd_price'], Validators.compose([Validators.required, this._Valid.isDecimalNumberMore])),
            'chg_uom_id':new Control(item['chg_uom_id'] ? item['chg_uom_id'] : '', Validators.required),
            'chg_code_des':new Control(item['chg_code_des']),
            'chg_cd_cur' : new Control(item['chg_cd_cur'] ? item['chg_cd_cur'] : '', Validators.required)
        };

        controlArray.push(
            new ControlGroup(tmp)
        );

        controlArray.controls[controlArray.length-1]['disabled']=true;

    }
}