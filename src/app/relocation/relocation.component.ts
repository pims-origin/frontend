import {Component} from "@angular/core";
import { Router, RouterLink, ROUTER_DIRECTIVES } from '@angular/router-deprecated';
import { FORM_DIRECTIVES,ControlGroup,FormBuilder, Control,Validators} from '@angular/common';
import {API_Config, UserService, Functions} from '../common/core/load';
import { Http } from '@angular/http';
import {WarningBoxPopup} from "../common/popup/warning-box-popup";
import {BoxPopupService} from "../common/popup/box-popup.service";
import {RelocationServices} from './relocation-service';
import { ValidationService } from '../common/core/validator';
import {WMSPagination,AgGridExtent,WMSMessages,WMSBreadcrumb} from '../common/directives/directives';
import 'ag-grid-enterprise/main';
@Component({
  selector: 'relocation',
  providers:[RelocationServices,ValidationService,BoxPopupService],
  directives: [ROUTER_DIRECTIVES,AgGridExtent,WMSMessages,WMSPagination,WMSBreadcrumb],
  templateUrl: 'relocation.component.html',
})
export class RelocationComponent{
  private dataForRenderTable:Array<any>=[];
  private headerURL = this.apiService.API_User_Metas+'/rel';
  private columnData = {
    item_id: {
      title:'Item ID',
      width:70,
      pin:true,
      ver_table:'10284:5422AM'
    },
    sku:{
      title:'SKU',
      width:120,
      pin:true
    },
    asn_dtl_lot: {
      title:'LOT',
      width:80,
      pin:true
    },
    size : {
      title:'Size',
      width:90,
      pin:true
    },
    color: {
      title:'Color',
      width:90,
      pin:true
    },
    asn_dtl_pack: {
      title:'Pack Size',
      width:90
    },
    uom_name: {
      title:'UOM',
      width:80
    },
    asn_dtl_width: {
      title:'W-IN',
      width:70
    },
    asn_dtl_height: {
      title:'H-IN',
      width:70
    } ,
    asn_dtl_weight: {
      title:'WE-LB',
      width:70
    } ,
    ctn_ttl:{
      title:'# CTN',
      width:75
    }
  };
  private Pagination;
  private perPage=20;
  private whs_id;
  private messages;
  private Loading={};
  private searching={};
  private LocationAutoCompleteData={};
  ReLocationForm:ControlGroup;
  private locationModel={};
  private sortData={fieldname:'item_id',sort:'asc'};
  private showLoadingOverlay:boolean=false;
  constructor(
    private reloService:RelocationServices,
    private _Func: Functions,
    private _boxPopupService: BoxPopupService,
    private  apiService:API_Config,
    private fb: FormBuilder,
    private _user:UserService,
    private _http: Http,
    private _Valid: ValidationService,
    private _router: Router) {
    this.checkPermission();
    this.buildForm();
    /*
     * Get Warehouse id current
     * */
    if(localStorage.getItem('whs_id'))
    {
      this.whs_id=localStorage.getItem('whs_id');
    }
  }
  // Check permission for user using this function page
  private relocation;
  private allowAccess:boolean=false;
  private checkPermission() {
    this.showLoadingOverlay = true;
    this._user.GetPermissionUser().subscribe(
      data => {
        this.showLoadingOverlay = false;
        this.relocation = this._user.RequestPermission(data, 'relocation');
        if(this.relocation){
          this.allowAccess=true;
          // set default to get data
          this.ReLocationForm.value['from_loc_id']=0;
          this.getListItemOfLocation(0);
        }
        else{
          this.redirectDeny();
        }
      },
      err => {
        this.messages=this._Func.Messages('danger', this._Func.parseErrorMessageFromServer(err));
        this.showLoadingOverlay = false;
      }
    );
  }
  redirectDeny(){
    this._router.parent.navigateByUrl('/deny');
  }
  private buildForm(){
    this.ReLocationForm =this.fb.group({
      from_loc_code: ['', Validators.compose([Validators.required, this._Valid.validateSpace])],
      from_loc_id: ['', Validators.required],
      to_loc_id: ['', Validators.required],
      to_loc_code: ['', Validators.compose([Validators.required, this._Valid.validateSpace])],
    });
  }
  private firstSearch:number=0;
  autoSearchLocation(loadingName,ListDataName,controlCode,controlId,key)
  {
    this.exitsLocation[controlCode]=null;
    let keystrim=this._Func.trim(key);
    if(keystrim)
    {
      let n=this;
      // do Search
      let param='?loc_code='+encodeURIComponent(keystrim)+'&without=ECO&limit=20';
      n.searching[loadingName]=true;
      n.reloService.Search(n.whs_id,param).subscribe(
        data => {
          n.firstSearch++;
          // disable loading icon
          n.searching[loadingName]=false;
          n.autoSelectMatchItem(data.data,controlCode,controlId,key,'loc_code');
          n.LocationAutoCompleteData[ListDataName] = data.data;
          /* ### */
          if(n.LocationAutoCompleteData[ListDataName].length==1)
          {
            let loccode=n.LocationAutoCompleteData[ListDataName][0];
            if(loccode['loc_code']==keystrim)
            {
              n.selectedItem(controlCode,controlId,loccode);
            }
          }
        },
        err =>{
          n.searching[loadingName]=false;
        },
        () => {}
      );
    }
  }
  private exitsLocation={};
  selectedItem(controlCode,controlId,data)
  {
    this.exitsLocation[controlCode]=true;
    (<Control>this.ReLocationForm.controls[controlCode]).updateValue(data['loc_code']);
    (<Control>this.ReLocationForm.controls[controlId]).updateValue(data['loc_id']);
    if(controlCode=='from_loc_code'){
      this.getListItemOfLocation();
    }
    // after selected item , call check function
    this.checkLoc_Status(controlCode,data);

  }
  checkExitsLocation(controlCode)
  {
    let value=this.ReLocationForm.value[controlCode];
    if(this.exitsLocation[controlCode]!==true&&value)
    {
      this.exitsLocation[controlCode]=false;
    }
  }
  private onPageSizeChanged($event)
  {
    this.perPage = $event.target.value;
    if(this.Pagination){
      if((this.Pagination['current_page'] - 1) * this.perPage >= this.Pagination['total']) {
        this.Pagination['current_page'] = 1;
      }
      this.getListItemOfLocation(this.Pagination['current_page']);
    }
  }
  /*
   * reLocation
   * */
  private submitForm:boolean=false;
  private listitemLocation:Array<any>=[];
  getListItemOfLocation(page=null)
  {
    this.showLoadingOverlay=true;
    let idLocation=this.ReLocationForm.value['from_loc_id'];
    let params="?sort["+this.sortData['fieldname']+"]="+this.sortData['sort']+"&page="+page+"&limit="+this.perPage;
    this.Loading['getList'] = true;
    this.reloService.getItemLocation(idLocation,params)
      .subscribe(
        data => {
          this.showLoadingOverlay= false;
          this.listitemLocation = this._Func.formatData(data.data);
          //pagin function
          if(typeof  data['meta']!=='undefined')
          {
            this.Pagination=data['meta']['pagination'];
            this.Pagination['tmpPageCount']=this._Func.Pagination(this.Pagination);
          }
        },
        err => {
          this.showLoadingOverlay= false;
          this.parseError(err);
        },
        () => {
        }
      );
  }
  private showForm=true;
  reLocation()
  {
    let data=this.ReLocationForm.value;


    if(this.ReLocationForm.valid)
    {

      if(this._Func.checkSameValue(this.ReLocationForm.value['from_loc_code'],this.ReLocationForm.value['to_loc_code']))
      {
        this.messages = this._Func.Messages('danger',`New location is the same as current location!`);
        return false;
      }

      let d=data;
      let params=JSON.stringify(data);
      this.showLoadingOverlay=true;
      this.Loading['relo']= true;
      this.reloService.reLocation(params)
        .subscribe(
          data => {
            this.submitForm=false;
            this.showLoadingOverlay= false;
            // then reset the form
            this.showForm = false;
            setTimeout(() => {
              this.buildForm();
              this.showForm = true;
            });
            this.listitemLocation=[];
            this.Pagination={};
            this.messages = this._Func.Messages('success', data.message);
          },
          err => {
            this.showLoadingOverlay= false;
            this.submitForm=false;
            if(err.json().errors.message){
              let errcode=err.json().errors.code;
              let errsttcode=err.json().errors.status_code;
              let loc_id=this.ReLocationForm.value['to_loc_id'];
              // case : Not active || fix to only not active
              if(errcode==1){
                /*case 1: modify text*/
                let n = this;
                let warningPopup = new WarningBoxPopup();
                warningPopup.text= this._Func.msg('RLC001');
                this._boxPopupService.showWarningPopup(warningPopup)
                  .then(function (yes) {
                    if(yes){
                      n.activeLocation(loc_id);
                    }
                  });
              }
              else{
                this.messages = this._Func.Messages('danger',err.json().errors.message);
              }
            }
          },
          () => {
          }
        );
    }
  }
  /*
   *Active loc
   */
  activeLocation(loc_id){
    this.Loading['activeloc'] = true;
    this.reloService.activeLoc(loc_id).subscribe(
      data => {
        this.Loading['activeloc'] = false;
        this.reLocation();
      },
      err => {
        let errMsg = err.json().message || err.json().errors.message;
        if(errMsg){
          this.messages = this._Func.Messages('danger',errMsg);
        }
        else{
          this.messages = this._Func.Messages('danger',this._Func.msg('VR111'));
        }
      },
      () => {}
    );
  }
  overWriteNewLocation(locationId,params,d)
  {
    this.Loading['overWriteNewLocation'] = true;
    this.reloService.overWriteNewLocation(locationId,params).subscribe(
      data => {
        this.Loading['overWriteNewLocation'] = false;
        this.submitForm=false;
        // then reset the form
        this.showForm = false;
        setTimeout(() => {
          this.buildForm();
          this.showForm = true;
        });
        this.listitemLocation=[];
        this.Pagination={};
        this.messages = this._Func.Messages('success','Pallet ID '+data['plt_num']+' has been relocated from '+d['from_loc_code']+' to  '+d['to_loc_code']+'');
      },
      err => {
        let errMsg = err.json().message || err.json().errors.message;
        if(errMsg){
          this.messages = this._Func.Messages('danger',errMsg);
        }
        else{
          this.messages = this._Func.Messages('danger',this._Func.msg('VR111'));
        }
      },
      () => {}
    );
  }
  autoSelectMatchItem(ListDataName:Array<any>,controlCode,controlId,key,keycompare){
    for(let item in ListDataName){
      //console.log('compare ', item , ListDataName[item], ' --- ', key ,keycompare , ListDataName[item][keycompare]==key);
      if(this._Func.trim(ListDataName[item][keycompare])==this._Func.trim(key))
      {
        this.selectedItem(controlCode,controlId,ListDataName[item]);
        return;
      }
    }
  }

  checkLoc_Status(control,loc_item)
  {
    if(loc_item['loc_sts_code']=='LK')
    {
      this.setErrorsControl(control,{'loc_status_is_locked':true});
    }
    if(loc_item['loc_sts_code']=='IA')
    {
      // show popup change stt
    }
    if(loc_item['loc_sts_code']=='CL')
    {
      this.setErrorsControl(control,{'loc_status_is_locked_cycle':true});
    }
  }

  afterRelocation()
  {
    this.submitForm=true;

    if(this.ReLocationForm.value['from_loc_code']&&this.ReLocationForm.value['to_loc_code'])
    {
      this.autoSearchLocation('newloLoading','NewLocations','to_loc_code','to_loc_id',this.ReLocationForm.value['to_loc_code']);
      this.autoSearchLocation('reloLoading','CurrentLocation','from_loc_code','from_loc_id',this.ReLocationForm.value['from_loc_code']);
      this.messages = this._Func.Messages('warning','Check Locations status...'+`<img src="/assets/images/loading.gif">`);
      // spend 2s to recheck
      setTimeout(() => {
        this.messages=null;
        this.reLocation();
      },2500);
    }


  }

  /*=====================================================
   setErrorsControl
  *=====================================================*/
  setErrorsControl(control,err){
    (<Control>this.ReLocationForm.controls[control]).setErrors(err);
  }
  // Show error when server die or else
  private parseError (err) {
    err = err.json();
    this.messages = this._Func.Messages('danger', err.errors.message);
  }
}
