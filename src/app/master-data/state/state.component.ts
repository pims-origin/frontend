import {Component} from '@angular/core';
import {Router, RouterLink, RouteConfig, ROUTER_DIRECTIVES} from '@angular/router-deprecated';
import {StateListComponent} from "./state-list/state-list.component";
import {CURStateComponent} from "./cru-state/cru-state.component";


@Component ({
    selector: 'state',
    directives: [ROUTER_DIRECTIVES],
    template: `<router-outlet></router-outlet>`,
})

@RouteConfig([
    { path: '/', component:StateListComponent  , name: 'State List', useAsDefault: true},
    { path: '/new', component:CURStateComponent  , name: 'Add State',data:{action:'new'} },
    { path: '/:id/:edit', component:CURStateComponent  , name: 'Edit State',data:{action:'edit'} },
    { path: '/:id', component:CURStateComponent  , name: 'View State',data:{action:'view'} }
])
export class StateComponent {

}
