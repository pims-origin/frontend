
import {Component} from '@angular/core';
import {ModalForm} from '../../../core/classes_us/modal-form';

@Component ({
    selector: 'alert-popup',
    templateUrl: 'alert.html',
})

export class Alert extends ModalForm
{
    public content;

    //**************************************************************************

    public submit()
    {
        this.isVisible = false;
    }

    //**************************************************************************

    public show(content: string)
    {
        this.content = content;

        this.isVisible = true;
    }

    //**************************************************************************

}
