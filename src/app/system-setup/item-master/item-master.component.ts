import {Component} from '@angular/core';
import {Router, RouterLink, RouteConfig, ROUTER_DIRECTIVES} from '@angular/router-deprecated';
import {ItemMasterListComponent} from "./item-master-list/item-master-list.component";
import {CruItemMasterComponent} from "./cru-item-master/cru-item-master";


@Component ({
    selector: 'item-master',
    directives: [ROUTER_DIRECTIVES],
    template: `<router-outlet></router-outlet>`,
})

@RouteConfig([
    { path: '/', component:ItemMasterListComponent  , name: 'Item Master List', useAsDefault: true},
    { path: '/new', component:CruItemMasterComponent  , name: 'CRU Item Master', data: {Action: 'New'}},
    { path: ':id', component:CruItemMasterComponent  , name: 'View Item Master',data: {Action: 'View'}},
    { path: ':id/edit', component:CruItemMasterComponent  , name: 'Edit Item Master', data: {Action: 'Edit'}},
])
export class ItemMasterComponent {

}
