
declare var jQuery: any;

export class PopupServices {

    constructor()
    {
    }

    //**************************************************************************

    public checkValues(parentComponent: any)
    {
        let data = parentComponent.get('data');

        let emptyInputs = [],
            params = {},
            errors = '',
            $parent = jQuery('#' + data.id);

        for (let count=0; count<data.items.length; count++) {

            let item = data.items[count];
            let name = item.name;

            let value = item.hasOwnProperty('dropdown') ?
                    jQuery('select[name="' + name + '"] option:selected', $parent).val() :
                    jQuery('input[name="' + name + '"]', $parent).val();

            params[name] = value;

            if (item.mandatory && ! value) {
                emptyInputs.push(item.caption);
            }

            if (item.hasOwnProperty('min') && value < item.min) {

                errors += errors ? '<br><br>' : '';

                errors += item.caption + ' cannot be less than ' + item.min;
            }

            if (item.hasOwnProperty('max') && value > item.max) {

                errors += errors ? '<br><br>' : '';

                errors += item.caption + ' cannot be greater than ' + item.max;
            }
        }

        errors += errors && emptyInputs.length ? '<br><br>' : '';

        errors += emptyInputs.length ?
                'Missing mandatory values:<br>' + emptyInputs.join(', ') : '';

        if (errors) {

            let _tableFunc = parentComponent.get('_tableFunc');

            _tableFunc.showMessage(parentComponent, 'danger', errors);
        }

        return errors ? false : params;
    }

    //**************************************************************************

}