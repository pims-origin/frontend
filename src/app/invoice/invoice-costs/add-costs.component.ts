
import {Component, ViewChild} from '@angular/core';
import {API_Config} from '../../common/core/API_Config';
import {WMSMessages} from '../../common/directives/directives';
import {BaseService} from '../../common/core/classes_us/base-service';
import {ModalForm} from '../../common/core/classes_us/modal-form';
import {TableFunctions} from '../../common/core/table-functions';
import {CustomersService} from '../../master-data/customers/customers-service';
import {ChargeCodeService} from '../../master-data/charge-code/charge-code-service';
import {PopupServices} from '../../common/directives/popup-modal_us/add/add-service';
import {OrderBy} from '../../common/pipes/order-by.pipe';
import {Alert} from '../../common/directives/popup-modal_us/alert/alert';

@Component({
    selector: 'add',
    directives: [
        WMSMessages,
        Alert
    ],
    providers: [
        BaseService,
        TableFunctions,
        PopupServices,
        CustomersService,
        ChargeCodeService
    ],
    pipes: [OrderBy],
    templateUrl: '../../common/directives/popup-modal_us/add/add.html'
})

export class AddCosts extends ModalForm
{
    @ViewChild(Alert) alert: Alert;

    public data = {
        id: 'add-costs',
        title: 'Add Invoice Costs',
        class_name: 'add-cost-modal-form',
        caption_class: 'add-cost-caption',
        value_class: 'add-cost-value',
        items: [
            {
                caption: 'Customer',
                name: 'cus_id',
                class_name: 'add-charge-costs-dropdown form-control margin-bottom5',
                mandatory: true,
                dropdown: {
                    source: [],
                    idField: 'cus_id',
                    nameField: 'cus_name',
                    service: '_customersService'
                }
            },
            {
                caption: 'Charge Code Name',
                name: 'chg_code_id',
                class_name: 'add-charge-costs-dropdown form-control margin-bottom5',
                mandatory: true,
                dropdown: {
                    source: [],
                    idField: 'chg_code_id',
                    nameField: 'chg_code_name',
                    service: '_chargeCodeService'
                }
            },
            {
                caption: 'Currency',
                name: 'chg_cd_cur',
                value: 'USD',
                max_length: 3,
                class_name: 'cur-code form-control margin-bottom5',
                mandatory: true
            },
            {
                caption: 'Price',
                name: 'chg_cd_price',
                class_name: 'charge-code-rate form-control margin-bottom5',
                step: 0.01,
                min: 0.01,
                mandatory: true
            }
        ]
    };

    //**************************************************************************

    constructor(
        public _api: API_Config,
        public _tableFunc: TableFunctions,
        public _tableService: BaseService,
        public _popupServices: PopupServices,
        public _customersService: CustomersService,
        public _chargeCodeService: ChargeCodeService
    )
    {
        super();

        this._tableFunc.getDropDowns({
            tableComponent: this,
            sourceInfo: this.data.items
        });
    }

    //**************************************************************************

    public showInfo(tableComponent: any)
    {
        this.parentComponent = tableComponent;
        this.messages = '';
        this.isVisible = true;
    }

    //**************************************************************************

    public submitInfo()
    {
        let params = this._popupServices.checkValues(this);

        if (params) {

            params['whs_id'] = this.parentComponent.get('whs_id');

            this.showLoadingOverlay = true;

            let url = this._api.API_Invoice_Costs + '/check-charge-code?' +
                    'cus_id=' + params['cus_id'] +
                    '&whs_id=' + params['whs_id'] +
                    '&chg_cd_cur=' + params['chg_cd_cur'] +
                    '&chg_code_id=' + params['chg_code_id'];

            this._tableService.get(url)
                .subscribe(
                    data => {
                        if (data) {
                            this.addCosts(params);
                        } else {
                            this.alert.show('This charge code is already used '
                                          + 'with a different system of measurement');

                            this.showLoadingOverlay = false;
                        }
                    },
                    err => {

                        this.messages = this._tableFunc.parseError(err);

                        this.showLoadingOverlay = false;
                    },
                    () => {}
            );
        }
    }

    //**************************************************************************

    public addCosts(params)
    {
        let data = JSON.stringify(params),
            url = this._api.API_Invoice_Costs + '/update';

        this._tableService.post(url, data)
            .subscribe(
                data => {

                    this._tableFunc.getTableData({
                        tableComponent: this.parentComponent
                    });

                    this.isVisible = false;
                },
                err => {
                    this.messages = this._tableFunc.parseError(err);
                },
                () => {
                    this.showLoadingOverlay = false;
                }
        );
    }

    //**************************************************************************

}
