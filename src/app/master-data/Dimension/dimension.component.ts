import {Component} from '@angular/core';
import {Router, RouterLink, RouteConfig, ROUTER_DIRECTIVES} from '@angular/router-deprecated';
import {DimensionListComponent} from "./dimension-list/dimension-list.component";
import {CruDimensionComponent} from "./cru-dimension/cru-dimension.component";


@Component ({
    selector: 'dimension',
    directives: [ROUTER_DIRECTIVES],
    template: `<router-outlet></router-outlet>`,
})

@RouteConfig([
    { path: '/', component:DimensionListComponent  , name: 'Dimension List', useAsDefault: true},
    { path: '/new', component:CruDimensionComponent  , name: 'Add Dimension',data:{action:'new'} },
    { path: '/:id/:edit', component:CruDimensionComponent  , name: 'Edit Dimension',data:{action:'edit'} },
    { path: '/:id', component:CruDimensionComponent  , name: 'View Dimension',data:{action:'view'} }
])
export class DimensionComponent {

}
