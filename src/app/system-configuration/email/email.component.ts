import {Component} from "@angular/core";
import { CORE_DIRECTIVES, FORM_DIRECTIVES, ControlGroup,FormBuilder, Control ,ControlArray,Validators } from '@angular/common';
import {TimeService} from "../../common/services/time.service";
import {SystemConfigurationServices} from "../system-configuration.service";
import {StylesheetService} from "../../common/services/stylesheet.service";
import { ValidationService } from '../../common/core/validator';
import {WMSMessages} from "../../common/directives/messages/messages";
import {Functions} from "../../common/core/functions";
import {UserService} from "../../common/users/users.service";
import { Router, RouterLink, ROUTER_DIRECTIVES, RouteParams } from '@angular/router-deprecated';


@Component({
    selector: 'email',
    directives: [CORE_DIRECTIVES, FORM_DIRECTIVES, WMSMessages],
    providers: [TimeService,SystemConfigurationServices,StylesheetService, ValidationService,FormBuilder,],
    templateUrl: 'email.component.html',
})
export class EmailComponent{

  public smtpName:string = '';
  public smtpHost:string = '';
  public smtpPort:string = '';
  public usrName:string = '';
  public pwd:string = '';
  public encryption:string = 'noEncryption';
  public authentication:string = 'noAuthentication';
  public adminEmail:string = '';
  public issueReport:string = '';
  public invoiceAlert:string = '';
  emailForm: ControlGroup;

  public SuccessMsg:string;
  private messages:any;

  constructor(
      private _timeService:TimeService,
      private _sysService:SystemConfigurationServices,
      private _styleService:StylesheetService,
      private fb: FormBuilder,
      private _Valid: ValidationService,
      private _Func: Functions,
      private _user: UserService,
      private _router:Router

  ){
    this.checkPermission();

    this._styleService.addLoading();
    this._sysService.getDatabyTabName('email').subscribe(
      data => {
        if(data.length > 0){

          this.FormBuilder(data);

        }
        this._styleService.removeLoadding();
      },
      err => {

        this.parseError(err);
        this._styleService.removeLoadding();
      },
      () => {

      }
    )
  }

  // Check Permission
  private checkPermission() {

    this._user.GetPermissionUser().subscribe(

        data => {

          var flagCreate = this._user.RequestPermission(data,'configSystem');

          if(!flagCreate) {
            this._router.parent.navigateByUrl('/deny');
          }
        },
        err => {

          this.parseError(err);

        }
    );
  }

  private submitForm:boolean=false;
  saveEmail(){

    this.submitForm = true;
    if(!this.emailForm.valid) {
      return false;
    }

    this.messages = false;


    let creds = "EC[host]=" + encodeURIComponent(this.emailForm.value['smtpHost']) + "&EC[port]=" + encodeURIComponent(this.emailForm.value['smtpPort']) +
      "&EC[encryption]=" + encodeURIComponent(this.encryption) + "&EC[username]=" + encodeURIComponent(this.emailForm.value['usrName'])
        + "&EC[password]=" + encodeURIComponent(this.emailForm.value['pwd']) +
      "&EC[name]=" + encodeURIComponent(this.emailForm.value['smtpName']) + "&EC[authenticate]=" + encodeURIComponent(this.authentication) + "&EA="
        + encodeURIComponent(this.emailForm.value['adminEmail'])
        + "&ER=" + encodeURIComponent(this.emailForm.value['issueReport']) + "&EI=" +  encodeURIComponent(this.emailForm.value['invoiceAlert']);

    this._styleService.addLoading();
    this._sysService.saveDatabyTabName('email',creds).subscribe(
      data => {

        this.messages = this._Func.Messages('success', this._Func.msg('VR109'));

        this._styleService.removeLoadding();
      },
      err => {

        this.parseError(err);
        this._styleService.removeLoadding();
      },
      () => {

      }
    )
  }

  FormBuilder(data)
  {

    var tmp = {};
    tmp['adminEmail']  = ['', Validators.compose([Validators.required, this._Valid.validateSpace,  this._Valid.emailValidator])];
    tmp['invoiceAlert']  = ['', Validators.compose([Validators.required, this._Valid.validateSpace,  this._Valid.emailValidator])];
    tmp['issueReport']  = ['', Validators.compose([Validators.required, this._Valid.validateSpace,  this._Valid.emailValidator])];
    tmp['smtpName'] = ['', Validators.compose([Validators.required, this._Valid.validateSpace])];
    tmp['smtpHost']= ['', Validators.compose([Validators.required, this._Valid.validateSpace])];
    tmp['smtpPort'] = ['', Validators.compose([Validators.required, this._Valid.validateSpace])];
    tmp['usrName']  =   ['', Validators.compose([Validators.required, this._Valid.validateSpace])];
    tmp['pwd']  = ['', Validators.compose([Validators.required, this._Valid.validateSpace])];

    for(let i = 0; i < data.length ;i++ ){
      if(data[i].setting_key == "EC" && data[i].setting_value){
        this.encryption = data[i].setting_value.encryption;
        this.authentication = data[i].setting_value.authenticate;
        tmp['smtpName'] = [data[i].setting_value.name, Validators.compose([Validators.required, this._Valid.validateSpace])];
        tmp['smtpHost']= [data[i].setting_value.host, Validators.compose([Validators.required, this._Valid.validateSpace])];
        tmp['smtpPort'] = [data[i].setting_value.port, Validators.compose([Validators.required, this._Valid.validateSpace])];
        tmp['usrName']  =   [data[i].setting_value.username, Validators.compose([Validators.required, this._Valid.validateSpace])];
        tmp['pwd']  = [ data[i].setting_value.password, Validators.compose([Validators.required, this._Valid.validateSpace])];
      }

      if(data[i].setting_key == "EA" && data[i].setting_value != null){

        tmp['adminEmail']  = [data[i].setting_value, Validators.compose([Validators.required, this._Valid.validateSpace,  this._Valid.emailValidator])];

      }

      if(data[i].setting_key == "EI" && data[i].setting_value != null){

        tmp['invoiceAlert']  = [data[i].setting_value, Validators.compose([Validators.required, this._Valid.validateSpace,  this._Valid.emailValidator])];

      }
      if(data[i].setting_key == "ER" && data[i].setting_value != null){

        tmp['issueReport']  = [data[i].setting_value, Validators.compose([Validators.required, this._Valid.validateSpace,  this._Valid.emailValidator])];

      }
    }

    this.emailForm = this.fb.group(tmp);

  }

  // Show error when server die or else
  private parseError (err) {

    this.messages = {'status' : 'danger', 'txt' : this._Func.parseErrorMessageFromServer(err)};
  }
}
