import { Component } from '@angular/core';
import { FORM_DIRECTIVES, ControlGroup, FormBuilder, Control, ControlArray, Validators } from '@angular/common';
import { RouteParams, Router, RouteData } from '@angular/router-deprecated';
import { UserService, Functions } from '../../../common/core/load';
import { CustomerServices } from '../customer-service';
import { ValidationService } from '../../../common/core/validator';
import {
  WMSBreadcrumb, PaginatePipe, PaginationControlsCmp, ConfigOrderFlowDirective,
  ColorPickerDirective, ColorPickerService, PaginationService, ThirdPartyDirective,
  DocumentUpload, DmsIn
} from '../../../common/directives/directives';
import { Http } from '@angular/http';
import { search } from './filter-pipe';
import { GoBackComponent } from "../../../common/component/goBack.component";
import { WarningBoxPopup } from "../../../common/popup/warning-box-popup";
import { BoxPopupService } from "../../../common/popup/box-popup.service";
import { OrderBy } from "../../../common/pipes/order-by.pipe";
import { CustomerFunctions } from "./customer-functions";
import { User, CsrPkr } from "./csr-pkr";
import { isNullOrUndefined } from 'util';
declare var jQuery: any;
@Component({
  selector: 'cru-customer',
  providers: [CustomerServices,CustomerFunctions,PaginationService,ColorPickerService,ValidationService,FormBuilder, BoxPopupService,UserService],
  directives: [
    FORM_DIRECTIVES, PaginationControlsCmp, ConfigOrderFlowDirective, ColorPickerDirective,
    GoBackComponent, WMSBreadcrumb, ThirdPartyDirective, DocumentUpload, DmsIn
  ],
  pipes: [search,PaginatePipe,OrderBy],
  templateUrl: 'cru-customer.component.html',
  styleUrls: ['cru-customer.component.scss']
})
export class CruCustomerComponent {
  private isZonecode: boolean = true;
  private showIconDanger: boolean = false;
  private billingModel={};
  public Loading = [];
  private messages;
  private Action;
  private TitlePage='View Customer';
  private IsView:boolean=false;
  private ZoneTypeDetail=[];
  private ListNull:Array<any>=[];
  private Customers:Array<any>=[];
  private Detail;
  private sortData={fieldname:'whs_code',sort:'asc'};
  private currentPage=1;
  private selectedAll={
    'ListZonesByCustomer':false,
    'ListDataPopup':false,
    'csrControlsGroups':false,
    'pkrControlsGroups':false
  };
  private ChargeCodeSearchData:Array<any>=[];
  private WareHouseList:Array<any>=[];
  private StateProvinces = {};
  private enableCreateChargCode=[];
  private ListSelectedItem:Array<any>=[];
  private perPage=20;
  CustomerForm: ControlGroup;
  configurationForm: ControlGroup;
  private Country=[];
  private cusId;
  private StatePaginAddZone=[];
  private showLoadingOverlay:boolean=false;
  /* form control settup */
  ctConTact:ControlGroup[]=[];
  shippingInfo:ControlGroup[]=[];
  shippingArray: ControlArray= new ControlArray(this.shippingInfo);
  contactArray: ControlArray = new ControlArray(this.ctConTact);
  configs = [];
  putaway = {config_name: 'putaway', config_value: 'request only', ac: 'Y'}; //radio
  allocs = {
    auto: {config_name: 'allocation', config_value: 'auto', ac: 'N'} , // radio
    full: {config_name: 'allocation', config_value: 'full', ac: 'Y'} ,
    partial: {config_name: 'allocation', config_value: 'partial', ac: 'Y'} ,
  };
  pickings = {
    value: 'fifo',
  };

  // Options for Inbound Config and default value
  stepOption = {
    value: 1
  }

  palletOption = {
    value: 1
  }

  lotOption = {
    value: 0
  };

  expireDateOption = {
    value: 0
  };

  pickingAlgo = {
    fifo: { config_name: 'picking_algorithm', config_value: 'fifo', ac: "N"}, // radio
    lifo:{ config_name: 'picking_algorithm', config_value: 'lifo', ac: "N"}, // radio
    fefo: { config_name: 'picking_algorithm', config_value: 'fefo', ac: "N"} // radio
  };
  backOdr = {
    short: {config_name: 'back_order', config_value: 'short', ac: 'N'},
    full: {config_name: 'back_order', config_value: 'full', ac: 'Y'},
  } ;
  mixedSku = {config_name: 'mixed_sku', config_value: 'outbound', ac: 'N'};
  private  auto_mode = {config_name: 'auto_mode', config_value: 'auto', ac: 'N'}; //radio

  private cus_color: string = "";

  private doneAPI=0;
  private listCurrency = [];

  // Add warehouses to customer
  private StatePaginAddWarehouse=[];
  private StatePaginZone;
  listWhsOfCustomer = [];
  warehouseNotInCusList: Array<any> = [];
  dataWhsBeforeSave: any;
  savedWarehouses = [];

  constructor(
    private _http: Http,
    private _Func: Functions,
    private params: RouteParams,
    private _CTService: CustomerServices,
    private fb: FormBuilder,
    private _Valid: ValidationService,
    private _boxPopupService: BoxPopupService,
    _RTAction: RouteData,
    private cusFunc:CustomerFunctions,
    private _user:UserService,
    private _router: Router) {
    this.checkPermission();
    this.Action=_RTAction.get('Action');
    
    this.getUOMs();
    this.getChargeTypes();
    this.GetCountry();
    this.GetWareHouseList();
  }
  pickingChange() {
    this.pickingAlgo[this.pickings.value]['ac'] = "Y";
  }

  genConfigs() {
    this.pickingChange();
    this.configs.push(
      this.putaway,
      this.allocs.auto,
      this.allocs.partial,
      this.allocs.full,
      this.pickingAlgo.fifo,
      this.pickingAlgo.lifo,
      this.pickingAlgo.fefo,
      this.backOdr.full,
      this.backOdr.short,
      this.mixedSku,
      this.auto_mode
    );
  }

  // Check permission for user using this function page
  private createCustomer;
  private viewCustomer;
  private editCustomer;
  private allowAccess:boolean=false;
  private checkPermission() {
    this.showLoadingOverlay = true;
    this._user.GetPermissionUser().subscribe(
      data => {
        this.showLoadingOverlay = false;
        this.createCustomer = this._user.RequestPermission(data, 'createCustomer');
        this.viewCustomer = this._user.RequestPermission(data, 'viewCustomer');
        this.editCustomer = this._user.RequestPermission(data, 'editCustomer');
        /* Check orther permission if View allow */
        if (!this.viewCustomer) {
          this._router.parent.navigateByUrl('/deny');
        }
        else {

          this.getCurrency();

          if (this.Action == 'View') {
            this.IsView = true;
            this.TitlePage = 'View Customer';
            if (!this.viewCustomer) {
              this.redirectDeny()
            } else {
              this.allowAccess = true;
              this.csrControlArr.push(this.genControls(new User()));
            }
          }

          // Action new
          if (this.Action == 'New') {
            if (!this.createCustomer) {
              this.redirectDeny()
            } else {
              this.allowAccess = true;
              this.TitlePage = 'Add Customer';
              /*get warehouse list*/
              this.GetWareHouseList();
              this.FormBuilder();
            }
          }

          if (this.Action == 'Edit') {
            this.TitlePage = 'Edit Customer';
            if (!this.editCustomer) {
              this.redirectDeny()
            } else {
              this.allowAccess = true;
            }
          }

          if (this.Action == 'Edit' || this.Action == 'View') {
            // edit
            this.cusId = this.params.get('id');

            //this.params.get('id');
            this._CTService.Get_CustomerDetail(this.cusId).subscribe(
              data => {
                this.Detail = data;
                this.FormBuilderViewEdit();
                this.GetWareHouseList();
                this.GetZoneByCustomer();
                this.GetLocationByCustomer();
                // render config data
                this.renderConfigData(this.Detail['configs']);
                this.getCarrierData();

                let isEdit = this.Action == 'Edit';
                // render Inbound config
                this._CTService.getInboundConfig(this.cusId).subscribe((data) => {
                  this.stepOption.value = isNullOrUndefined(data['step']) || data['step'] == '' && isEdit ? this.stepOption.value : data['step'];
                  this.palletOption.value = isNullOrUndefined(data['pallet']) || data['pallet'] == '' && isEdit ? this.palletOption.value : data['pallet'];
                  this.lotOption.value = isNullOrUndefined(data['lot']) || data['lot'] == '' && isEdit ? this.lotOption.value : data['lot'];
                  this.expireDateOption.value = isNullOrUndefined(data['expire_dt']) || data['expire_dt'] == '' && isEdit ? this.expireDateOption.value : data['expire_dt'];
                });

                this._CTService.getConfigurationEmail(this.cusId).subscribe(res=>{
                  let configEmail = res.data.cus_email;
                  (<Control>this.configurationForm.controls['config_email']).updateValue(configEmail);
                });

                //watch form
                this.CustomerForm.valueChanges.subscribe(data => {
                  this.cusFunc.changeAddressType(this.shippingArray);
                });

                this.pkrCsrWarehouses = this.Detail['warehouses'];

                if (data['warehouses']) {
                  /* For CSR tab */
                  this.buildCsrForm();
                  this.getListCsrUser(this.cusId);
                }

				this.savedWarehouses = Object.assign([], this.Detail['warehouses']);
                this.getWarehouseNotInCus(this.cusId);
              },
              err => {
                this._router.parent.navigateByUrl('/system-setup/customer');
              },
              () => { }
            );
          }
        }
      },
      err => {
        this.messages = this._Func.Messages('danger', this._Func.parseErrorMessageFromServer(err));
        this.showLoadingOverlay = false;
      }
    );
  }

  /*========================================
  * Using only one function to do tab config
  * ========================================*/
  setYesNoConfig(acObject,$event){
    if($event.target.checked){
      acObject['ac']='Y';
    }else{
      acObject['ac']='N';
    }
  }

  setYesNoConfigValue(acObject, $event) {
    if ($event.target.checked) {
      acObject['value'] = 1;
    } else {
      acObject['value'] = 0;
    }
  }

  /*======================
  * Render Config Data
  *==================== */
  renderConfigData(configs){
    if(configs.length != 0) {
      let n = this;
      this.Detail['configs'].forEach((item)=>{
        if(item.config_name == 'putaway') {
          this.putaway = item;
        }
        if(item.config_name == 'picking_algorithm') {
          if(item.ac == 'Y') {
            this.pickings.value = item.config_value;
          }
        }
        if(item.config_name == 'back_order') {

          if(item.config_value == 'full')
            this.backOdr.full = item;
          if(item.config_value == 'short') {
            this.backOdr.short = item;
          }
        }
        if(item.config_name == 'allocation') {
          if(item.config_value == 'auto')
            this.allocs.auto = item;
          if(item.config_value == 'full') {
            this.allocs.full = item;
          }
          if(item.config_value == 'partial') {
            this.allocs.partial = item;
          }
        }
        if(item.config_name == 'mixed_sku') {
          this.mixedSku = item;
        }
        if(item.config_name == 'auto_mode') {
          this.auto_mode = item;
        }
      })
    }
  }
  redirectDeny(){
    this._router.parent.navigateByUrl('/deny');
  }
  FormBuilder()
  {
    // add control;
    this.addContact();
    this.addAddress();
    this.addAddress();
    this.buildConfigurationForm();
    
    this.CustomerForm =this.fb.group({
      cus_status:['AC'],
      cl_code:[''],
      cus_code: [null, Validators.compose([Validators.required, this._Valid.validateSpace, this._Valid.invalidLength3])],
      cus_name: [null, Validators.compose([Validators.required, this._Valid.validateSpace, this._Valid.invalidNameStr])],
      cus_billing_account: [null, Validators.compose([Validators.required, this._Valid.validateSpace])],
      cus_country_id: ['', Validators.compose([Validators.required])],
      cus_des:[''],
      contacts:this.contactArray,
      addresses: this.shippingArray,
      sales_rep: [''],
      billed_through: [''],
    });

    //watch form
    this.CustomerForm.valueChanges.subscribe(data =>{
      this.cusFunc.changeAddressType(this.shippingArray);
    });

  }
  FormBuilderViewEdit()
  {
    this.buildConfigurationForm();

    if (this.Detail['contacts']) {
      this.Detail['contacts'].forEach((item) => {
        this.addContact(item);
      });
    } else {
      this.addContact();
    }

    // binding this.addresses
    if(this.Detail['addresses']){
      let param="?sort[sys_state_name]=asc&limit=500";
      this.showLoadingOverlay = true;
      this.Detail['addresses'].forEach((item, _index)=>{
        this._CTService.State(item.cus_add_country_id,param).subscribe(
            data=>{
              this.StateProvinces[_index] = data;
              this.addAddress(item, _index);
              if(this.Detail['addresses'].length === 1) {
                this.addAddress();
                this.showLoadingOverlay = false;
              }
              if(_index === 1) {
                this.showLoadingOverlay = false;
              }
            },
            err=>{
            },
            () => {}
        );
      });
    } else {
      this.addAddress();
      this.addAddress();
    }

    this.CustomerForm =this.fb.group({
      cus_status:[this.Detail['cus_status']],
      cl_code:[this.Detail['cl_code']],
      cus_code: [this.Detail['cus_code'], Validators.compose([Validators.required, this._Valid.validateSpace, this._Valid.invalidLength3])],
      cus_name: [this.Detail['cus_name'], Validators.compose([Validators.required, this._Valid.validateSpace, this._Valid.invalidNameStr])],
      cus_billing_account: [this.Detail['cus_billing_account'], Validators.compose([Validators.required, this._Valid.validateSpace])],
      cus_country_id: [this.Detail['cus_country_id'], Validators.required],
      cus_des:[this.Detail['cus_des']],
      contacts:this.contactArray,
      addresses: this.shippingArray,
      warehouses:[this.Detail['warehouses']],
      sales_rep: [this.Detail['sales_rep']],
      billed_through: [this.Detail['billed_through']],
    });

    /* assign cl_code*/
    this.cus_color=this.Detail['cl_code'];

  }

  private messagesLimitAddContact;
  addContact(item={}):void{
    if(this.contactArray.length<5)
    {
      this.contactArray.push(
          new ControlGroup({
            'cus_ctt_id':new Control(item['cus_ctt_id']),
            'cus_ctt_fname':new Control(item['cus_ctt_fname'], Validators.compose([Validators.required, this._Valid.validateSpace])),
            'cus_ctt_lname':new Control(item['cus_ctt_lname'], Validators.compose([Validators.required, this._Valid.validateSpace])),
            'cus_ctt_email':new Control(item['cus_ctt_email'], Validators.compose([Validators.required, this._Valid.emailValidator,this._Valid.validateSpace])),
            'cus_ctt_phone':new Control(item['cus_ctt_phone'], Validators.compose([Validators.required, this._Valid.validateSpace])),
            'cus_ctt_ext':new Control(item['cus_ctt_ext']),
            'cus_ctt_position':new Control(item['cus_ctt_position']),
            'cus_ctt_mobile':new Control(item['cus_ctt_mobile'])
          })
      );
    }
    else{
      // Message if user add more than 5 contact
      this.messagesLimitAddContact = this._Func.Messages('danger',this._Func.msg('CU001'));
    }
  }

  buildConfigurationForm() {
    this.configurationForm = this.fb.group({
      config_email: new Control(null, Validators.compose([this._Valid.emailValidator, this._Valid.validateSpace]))
    });
  }

  private messagesLimitAddAddress;
  addAddress(data={}, index = 0):void{    
    if(this.shippingArray.length<2)
    {
      this.shippingArray.push(
          new ControlGroup({
            'index': new Control(index),
            'cus_add_id':new Control(data['cus_add_id']),
            'cus_add_line_1':new Control(data['cus_add_line_1'], Validators.compose([Validators.required, this._Valid.validateSpace])),
            'cus_add_country_id'  :new Control(data['cus_add_country_id'] ? data['cus_add_country_id'] : '',Validators.compose([Validators.required])),
            'cus_add_type' :new Control(data['cus_add_type'] ? data['cus_add_type'] : '',Validators.compose([Validators.required])),
            'cus_add_city_name' :new Control(data['cus_add_city_name']),
            'cus_add_line_2':new Control(data['cus_add_line_2']),
            'cus_add_state_id':new Control(data['cus_add_state_id'] ? data['cus_add_state_id'] : '', Validators.compose([Validators.required])),
            'cus_add_postal_code':new Control(data['cus_add_postal_code'], Validators.compose([Validators.required, this._Valid.validateSpace]))
          })
      );
    }
    else{
      // Message if user add more than 5 contact
      this.messagesLimitAddAddress = this._Func.Messages('danger',this._Func.msg('CU002'));
    }
    this.shippingInfo.sort((a, b) => {
      if (a['value']['index'] < b['value']['index']) return -1;
      else if (a['value']['index'] > b['value']['index']) return 1;
      else return 0;
    });
    
  }

  checkInputNumber(evt)
  {
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 46){
      evt.preventDefault();
    }
  }
  /*
   * deleteBlockContact
   * */
  deleteBlockContact(index)
  {
    this.contactArray.removeAt(index);
  }
  
  private removeBlockAddress(_index){
    this.shippingArray.removeAt(_index);
  }
  /*
   * List list warehouse to get  whs code
   * */
  private GetWareHouseList(page=null)
  {
    let param="?limit=500&sort[whs_code]=asc";
    this.Loading['getListWareHouseList']=true;
    this._CTService.GetWareHouseList(param).subscribe(
      data => {
        this.WareHouseList = data.data;
        this.Loading['getListWareHouseList'] = false;
      },
      err => {},
      () => {}
    );
  }

  
  /**
   * Get warehouse list not in customer
   */
  getWarehouseNotInCus(cusId: string) {
    this._CTService.getWarehouseNotInCus(cusId).subscribe(res => {
      this.warehouseNotInCusList = res.data;
    });
  }

  /*
   * Get Country
   * */
  GetCountry()
  {
    let param='?sort[sys_country_name]=asc&limit=500';
    this._CTService.getCountry(param).subscribe(
      data=>{
        this.Country=data;
      },
      err=>{
      },
      () => {}
    );
  }

  GetState(idCountry, _index) {
    let param = "?sort[sys_state_name]=asc&limit=500";
    this._CTService.State(idCountry, param).subscribe(
      data => {
        this.StateProvinces[_index] = data;
        (<Control>this.CustomerForm.controls['addresses']['controls'][_index]['controls']['cus_add_state_id']).updateValue('');

      },
      err => { },
      () => { }
    );
  }

  /*
   * Load state of country when changle
   * */
  ChangeCountry(item, _index)
  {
    if(item)
    {
      this.GetState(item,_index);
    }
    else{
      this.StateProvinces={};
      (<Control>this.CustomerForm.controls['addresses']['controls'][_index]['controls']['cus_add_state_id']).updateValue('');
    }
  }
  /*
   * Save
   * */
  private submitForm:boolean=false;
  Save(): void {
    if ( this.isZonecode ) {
      this.showIconDanger = false;
      let data = this.CustomerForm.value;
      data['zones'] = this.ListZonesByCustomer;
      this.genConfigs();
      data['configs'] = this.configs;
      this.submitForm = true;

    // For get warehouse IDs that no saved yet 
      if (data['warehouses']) {
        let arrWhsId = data['warehouses'].map(whs => whs.whs_id);
        let savedWhsIds:any = this.savedWarehouses.map(item => item.whs_id);

        let arrNoSavedWhsId = [];
        arrWhsId.forEach(item => {
          if(!savedWhsIds.includes(item)){
            arrNoSavedWhsId.push(item);
          }
        });

        let dataWhs = {
          whs_ids: arrNoSavedWhsId
        };
        this.dataWhsBeforeSave = dataWhs;
      }

      if (this.CustomerForm.valid) {
        let data_json = JSON.stringify(data);

        if (this.Action == 'New') {
          // add new
          this.AddNew(data_json);
        }
        else if (this.Action == 'Edit') {
          // edit zone type
          this.doneAPI = 0;
          this.Update(this.cusId, data_json);
        }
      }
    } else {
      this.showIconDanger = true;
    }
    
  }

  AddNew(data) {
    let n = this;
    this.showLoadingOverlay = true;
    this._CTService.Add_New_Customer(data).subscribe(
      data => {
        this.showLoadingOverlay = false;
        this.saveConfigurationEmail(data.cus_id);

        //Reset Form
        this.messages = this._Func.Messages('success', this._Func.msg('VR107'));
        setTimeout(function () {
          n._router.parent.navigateByUrl('/system-setup/customer');
        }, 600);
      },
      err => {
        this.showLoadingOverlay = false;
        if (err.json().errors.message) {
          this.messages = this._Func.Messages('danger', err.json().errors.message);
        }
      },
      () => { }
    );
  }

  Update(id, data) {
    let n = this;
    this.showLoadingOverlay = true;
    this._CTService.Update_Customer(id, data).subscribe(
      data => {
        this._CTService.updateWarehouseToCustomer(this.cusId, JSON.stringify(this.dataWhsBeforeSave)).subscribe(res => {
          console.log(`updateWarehouseToCustomer`, res);
        });

        // reset done
        this.saveOverFlow();
        this.saveInboundConfig();
        this.saveConfigurationEmail(data.cus_id);

        if (this.ListZonesByCustomer.length && this.Detail['warehouses']) {
          this.submitCSR();
          this.saveCarrier();
        } else {
          this.doneAPI = 1;
        }

        var inter = setInterval(() => {
          if (this.doneAPI == 5) {
            clearInterval(inter);
            this.showLoadingOverlay = false;
            this.messages = this._Func.Messages('success', this._Func.msg('VR109'));
            setTimeout(() => {
              n._router.parent.navigateByUrl('/system-setup/customer');
            }, 600);
          }
        }, 1000);

      },
      err => {
        this.showLoadingOverlay = false;
        if (err.json().errors.status_code == 422) {
          this.messages = this._Func.Messages('danger', err.json().errors.message);
        }
        else {
          this.messages = this._Func.Messages('danger', err.json().errors.message);
        }
      },
      () => { }
    );
  }

  GoToList()
  {
    this._router.navigateByUrl('system-setup/item-master');
  }
 
  /*
   * GetUOMs
   * */
  private UOMsList:Array<any>=[];
  getUOMs()
  {
    let param="?limit=500";
    this.Loading['getUOMsList']=true;
    this._CTService.getUOMs(param).subscribe(
      data => {
        this.Loading['getUOMsList']=false;
        this.UOMsList = data.data;
      },
      err => {
        this.Loading['getUOMsList']=false;
      },
      () => {}
    );
  }
  /*
   * getChargeTypes
   * */
  private chargeTypesList:Array<any>=[];
  getChargeTypes()
  {
    let param="?&limit=500";
    this.Loading['getChargeTypes']=true;
    this._CTService.getChargeTypes(param).subscribe(
      data => {
        this.Loading['getChargeTypes']=false;
        this.chargeTypesList = data.data;
      },
      err => {
        this.Loading['getChargeTypes']=false;
      },
      () => {}
    );
  }
 
  private onPageSizeChanged($event,funcGetList,currentPage)
  {
    this.perPage =  parseInt($event.target.value);
  }
  private filterList(num,ListName)
  {
    eval(`this.`+ListName+`(num)`);
  }
  private getPage(num)
  {
    let arr=new Array(num);
    return arr;
  }
  /*
   * Check All/Off
   * */
  private CheckAll(event,ListName:string,StatePagin,objName) {
    let Array=eval(`this.`+ListName);
    if(!Array.length)
    {
      this.selectedAll[ListName] = false;
      return;
    }
    if (event) {
      this.selectedAll[ListName] = true;
    }
    else {
      this.selectedAll[ListName]= false;
    }
    let end=StatePagin[objName]['end'];
    if(StatePagin[objName]['size']<StatePagin[objName]['end'])
    {
      end=StatePagin[objName]['size'];
    }
    for(let i=StatePagin[objName]['start'];i<end;i++)
    {
      Array[i]['selected']=this.selectedAll[ListName];
    }
  }
  /*
   * Selected
   * */
  private Selected($event,item,ListName:string,state,paginId) {
    let Array=eval(`this.`+ListName);
    this.selectedAll[ListName]=false;
    if ($event.target.checked) {
      item['selected']=true;
      this.checkCheckAll(ListName,state,paginId);
    }
    else {
      item['selected']=false;
    }
  }

  SelectedWhs($event, item, ListName: string, state, paginId) {
    let Array = eval(`this.` + ListName);
    this.selectedAll[ListName] = false;
    if ($event) {
      item['selected'] = true;
      this.checkCheckAll(ListName, state, paginId);
    }
    else {
      item['selected'] = false;
    }
  }

  Delete(ListName:string,IsControlGroupForm=false)
  {
    let n = this;
    // create array temp
    let Arr=eval(`n.`+ListName);
    let ArrTemp=Arr.slice();
    let hasChoose=this.checkDelete(ArrTemp);
    if(!hasChoose)
    {
      this.messages=this._Func.Messages('danger',this._Func.msg('VR113'));
      this._Func.scrollToTop();
      // if not choose any item to delete
      return false;
    }
    /*case 1: modify text*/
    let warningPopup = new WarningBoxPopup();
    warningPopup.text = this._Func.msg('PBAP002');
    this._boxPopupService.showWarningPopup(warningPopup)
        .then(function (dm) {
          if(!dm){
            return;
          }else{
            if(n.selectedAll[ListName])
            {
              // check delete form control group
              n.selectedAll[ListName]=false;
              let i=0;
              ArrTemp.forEach((item)=>{
                // find item selected and remove it
                if(item['selected']==true)
                {
                  let el = Arr.indexOf(item);
                  Arr.splice(el, 1);
                }
                i++;
                // console.log(i);
              });
              n.selectedAll[ListName]=false;
              n.isZonecode = false;

            }
            else{
              let i=0;
              ArrTemp.forEach((item)=>{
                // find item selected and remove it
                if(item['selected']==true)
                {
                  let el = Arr.indexOf(item);
                  Arr.splice(el, 1);
                }
                i++;
              });
            }
          }
        });
  }

  DeleteCSRPicker(ListName:string,controlArray:ControlArray)
  {
    let n = this;
    // create array temp
    let Arr=eval(`n.`+ListName);
    let ArrTemp=Arr.slice();
    let hasChoose=this.checkDelete(ArrTemp);
    if(!hasChoose)
    {
      this.messages=this._Func.Messages('danger',this._Func.msg('VR113'));
      this._Func.scrollToTop();
      // if not choose any item to delete
      return false;
    }

    let checkRemoveDefaultOrNot = this.checkoutDefault(ArrTemp);
    if(checkRemoveDefaultOrNot){
      let warningPopup = new WarningBoxPopup();
      warningPopup.text = 'You cannot delete user who is marked as default CSR.';
      warningPopup.showCancelButton = false;
      this._boxPopupService.showWarningPopup(warningPopup);
      return false;
    }
    /*case 1: modify text*/
      if(n.selectedAll[ListName])
      {
          // check delete form control group
          if(controlArray)
          {
              let warningPopup = new WarningBoxPopup();
              warningPopup.text = 'You cannot delete all.';
              warningPopup.showCancelButton = false;
              this._boxPopupService.showWarningPopup(warningPopup)
                  .then(function (dm) {
                      ArrTemp.forEach((item)=>{
                          item['selected'] = false;
                      });
                      // auto add one row
                      n.selectedAll[ListName]=false;
                  });
          }
          n.selectedAll[ListName]=false;
      } else{
          let warningPopup = new WarningBoxPopup();
          warningPopup.text = this._Func.msg('PBAP002');
          this._boxPopupService.showWarningPopup(warningPopup)
              .then(function (dm) {
                  if(!dm){
                      return;
                  }else{
                      let i=0;
                      ArrTemp.forEach((item)=>{
                          // find item selected and remove it
                          if(item['selected']==true)
                          {
                              if(controlArray){
                                  controlArray.removeAt(i);
                                  // this.csr.users.splice(i, 1);
                                  if(ListName == 'csrControlsGroups') {
                                      n.csr.users.splice(i, 1);
                                      n.csr.checkDuplicated();
                                  }
                                  i--;
                              }
                              else{
                                  let el = Arr.indexOf(item);
                                  Arr.splice(el, 1);
                              }
                          }
                          i++;
                      });
                  }
              });
      }
  }
  checkDelete(list:Array<any>){
    for(let i in list){
      if(list[i]['selected']){
        return true;
      }
    }
    return false;
  }

  checkoutDefault(list:Array<any>){
    for(let i in list){
      if(list[i]['selected'] && list[i]['_value']['set_default'] == '1'){
        return true;
      }
    }
    return false;
  }
  checkCheckAll(ListName:string,StatePagin,objName)
  {
    let Array=eval(`this.`+ListName);
    let el=StatePagin[objName];
    let end=el['end'];
    let chk=0;
    if(el['size'])
    {
      if(el['size']<el['end'])
      {
        end=el['size'];
      }
      for(let i=el['start'];i<end;i++)
      {
        if(Array[i]['selected'])
        {
          chk++;
        }
      }
      if(chk==el['slice'].length)
      {
        this.selectedAll[ListName]=true;
      }
      else{
        this.selectedAll[ListName]=false;
      }
    } // end if size
  }
  
  private RedirectToList()
  {
    let n = this;
    this._boxPopupService.showWarningPopup()
      .then(function (dm) {
        if(dm)
          n._router.navigateByUrl('/system-setup/customer/');
      })
  }
  private ListZonesByCustomer=[];
  private filterwhs='';
  private GetZoneByCustomer(page=null)
  {
    let param="?limit=9000";
    this.Loading['getListZoneByCustomer']=true;
    this._CTService.getZoneByCustomer(this.cusId,param).subscribe(
      data => {
        this.Loading['getListZoneByCustomer'] = false;
        this.ListZonesByCustomer = data.data;
        if (this.ListZonesByCustomer.length == 0) {
          this.isZonecode = false;
        } else {
          this.isZonecode = true;
        }
      },
      err => {
        this.Loading['getListZoneByCustomer'] = false;
      },
      () => {}
    );
  }
 
  private ListLocationByCustomer=[];
  private Pagin_Location={};
  private GetLocationByCustomer(page=null)
  {
    let param="?limit=9999";
    this.Loading['GetLocationByCustomer']=true;
    this._CTService.getLocationByCustomer(this.cusId,param).subscribe(
      data => {
        this.selectedAll['ListZonesByCustomer'] = false;
        this.ListLocationByCustomer = data.data;
        this.Loading['GetLocationByCustomer'] = false;
      },
      err => {
        this.Loading['GetLocationByCustomer'] = false;
      },
      () => {}
    );
  }
  /*
   *
   * */
  private ListDataPopup:Array<any>=[];
  private ListDataPopupTemp:Array<any>=[];
  private ListPopupActive:string='getListWareHouse';
  private PaginationPopup;
  private qzone_id;
  GetListDataPopup(ListName :string,ArrName:string)
  {
    let param="?limit=9999";
    this.Loading['LoadListPopup']=true;
    var currentWH_ID = localStorage.getItem('whs_id');
    for(var i = 0; i < this.WareHouseList.length; i++) {
      if(this.WareHouseList[i].whs_id == currentWH_ID) {
        this.filterwhs = this.WareHouseList[i].whs_code;
      }
    }

    this.qzone_id='';
    this.selectedAll['ListDataPopup'] = false;
    this.ListPopupActive = ArrName;
    //reset Pagination , ListData
    this.PaginationPopup = [];
    this.ListDataPopup = [];
    eval(`this._CTService.` + ListName + `(param).subscribe(
          data => {
            this.Loading['LoadListPopup']=false;
            //filter show
            this.ListDataPopup=this.filterShowDataListPopup(data.data);
            this.ListDataPopupTemp=this.ListDataPopup.slice();
            this.filterZoneByWareHouse(this.filterwhs);
          },
          err => {
            console.log(err);
            this.Loading['LoadListPopup']=false;
            this.messages = this._Func.Messages('danger','Get '+ListName+' has error !');
          },
          () => console.log('Get data LoadListPopup complete')
        );`);
  }

  addZoneToCustomerTemp()
  {
    this.selectedAll['ListDataPopup']=false;
    this.ListDataPopupTemp.forEach((item)=>{
      if(item['selected'])
      {
        item['selected']=false;
        this.ListZonesByCustomer.push(item);
        this.isZonecode = true;
      }
    });
  }

  addWhsToCustomerTemp() {
    if (!this.CustomerForm.value['warehouses']) {
      this.CustomerForm.value['warehouses'] = [];
    }

    this.selectedAll['warehouseNotInCusList'] = false;
    this.warehouseNotInCusList.forEach((item) => {
      if (item['selected']) {
        item['selected'] = false;
        this.CustomerForm.value['warehouses'].push(item);
      }
    });
  }
  
  private perPagePopup=20;
  private listdataFilterbyWHS:Array<any>=[];
  filterZoneByWareHouse(whsCode)
  {
    let arr:Array<any>=[];
    if(whsCode)
    {
      this.ListDataPopupTemp.forEach((item)=>{
        if(item['whs_code']==whsCode) {
          arr.push(item);
        }
        this.listdataFilterbyWHS=arr;
      });
    }else{
      arr=this.ListDataPopupTemp;
    }
    // filter
    if(this.qzone_id){
      this.searchZoneCode(this.qzone_id);
    }else {
      this.ListDataPopup = arr;
    }
  }
  searchZoneCode(key)
  {
    let arr=this.ListDataPopupTemp;
    // filter
    if(this.filterwhs){
      arr=this.listdataFilterbyWHS;
    }
    this.ListDataPopup=this._Func.filterbyfieldName(arr,'zone_code',key);
  }
  private storePopTmp=[];
  filterShowDataListPopup(array_data)
  {
    let ListZone=this.ListZonesByCustomer;
    this.storePopTmp=[];
    if(ListZone.length)
    {
      for(var i in ListZone)
      {
        for(let j=0; j < array_data.length; j++)
        {
          if((ListZone[i]['zone_code']==array_data[j]['zone_code'])&&(ListZone[i]['whs_code']==array_data[j]['whs_code']))
          {
            // remove item over to array popup
            let el = array_data.indexOf(array_data[j]);
            array_data.splice(el, 1);
          }
        }
      }
    }
    return array_data;
  }
  setCheckedAllList(arr:Array<any>,value:boolean)
  {
    arr.forEach((item)=>{
      item['selected']=value;
    })
  }

  private checkSpace(evt, onlyNumber) {
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode === 0 || charCode === 32) {
      evt.preventDefault();
    }
  }

  changeCustomerColor(color){
    this.cusFunc.setValueControlGroup(this.CustomerForm,'cl_code',color);
  }

  private listOrderFlow = [];

  updateCheckedOrderFlow(index, event) {

    this.listOrderFlow[index]['usage'] = 0;
    if(event.target.checked) {

        this.listOrderFlow[index]['usage'] = 1;

    }
  }

  private saveOverFlow() {

    this.messages = false;
    var target = this;
    this.showLoadingOverlay = true;
    var object = {};
    object= this.listOrderFlow;
    this._CTService.saveOrderFlow(this.cusId, JSON.stringify(object)).subscribe(
        data => {
          this.doneAPI++;
        },
        err => {

          target.showLoadingOverlay = false;
        },
        () => {
          target.showLoadingOverlay = false;
        }
    );
  }

  saveConfigurationEmail(cus_id: string) {
    let configEmail = this.configurationForm.controls['config_email'].value;
    if (configEmail) {
      let dataEmail = {
        cus_email: configEmail
      };

      this._CTService.saveConfigurationEmail(cus_id, JSON.stringify(dataEmail)).subscribe(res => {
        console.log(res.data.message);
        this.doneAPI++;
      }, err => {
        console.log(err);
      });
    }
  }

  /**
   * Save Inbound Config
   */
  saveInboundConfig() {
    let inboundConfigData = {
      "qualifier": "IBC",
      "value": [
        {
          "step": this.stepOption.value,
          "pallet": this.palletOption.value,
          "lot": this.lotOption.value,
          "expire_dt": this.expireDateOption.value
        }
      ]
    }

    this._CTService.saveInboundConfig(this.cusId, JSON.stringify(inboundConfigData)).subscribe(res => {
      this.doneAPI++;
    }, err => {
      console.log(err);
    });
  }


  /* *
   * CSR Tab
   * */
  showLoadingCSR = [];
  showCsrErr = false;
  private csrForm:ControlGroup;
  csrUserID: Control;
  default: Control;

  csrControlsGroups:ControlGroup[] = [];
  csrControlArr: ControlArray = new ControlArray(this.csrControlsGroups);
  csr:CsrPkr = new CsrPkr();
  buildCsrForm () {
    this.csrForm = this.fb.group(
        {
          qualifier: this.csr.qualifier,
          value:this.csrControlArr
        }
    )
  }

  genControls(user:User) {
    return new ControlGroup({
      whs_id: new Control(user.whsID, Validators.required),
      user_id: new Control(user.userID, Validators.required),
      set_default_check: new Control(user.defaultCheck),
      set_default: new Control(user.defaultSet),
      duplicated: new Control(user.isDuplicated, this.areDuplicated),
      not_set_default: new Control(user.hasErrorNotSetDefault, this.areNotSetDefault)
    })
  }

  private dataCSRUser = {};
  private pkrCsrWarehouses = [];
  getListCRSByCustomer(cus_id,whs_id ='', i) {
    this.showLoadingCSR[i] = true;
    this._CTService.getListCRSByCustomer(cus_id,whs_id)
        .subscribe(
        data => {
          this.showLoadingCSR[i] = false;
          if(data.data && data.data.length) {
            this.dataCSRUser[i] = data.data;
          }
          this.showLoadingOverlay = false;
        },
        err => {

          this.showMessage('danger', this._Func.parseErrorMessageFromServer(err));
          this.showLoadingOverlay = false;
        }
    );
  }


  private addMoreCSR() {
    this.csr.checkDuplicated();
    this.showCsrErr = true;
    if(this.csrForm.valid) {
      this.showCsrErr = false;
      let user = this.csr.addUser(new User('', false));
      this.csrControlArr.push(this.genControls(user))
    }

  }

  onChangeUserId(user:User, csrPkr:CsrPkr) {
    csrPkr.checkDuplicated();
  }

  onChangeDefaultSet(user:User, ind, defaultSetControl:Control, prevDefaultSetControl:Control, csrPkr:CsrPkr,type) {
    if(user.defaultCheck == true) {
      user.defaultSet = 1;
      let checkbyId=true
      csrPkr.setDefault(ind, checkbyId);
      this.handleCheckbox(ind,csrPkr,type);
    }
    else {
      user.defaultSet = 0;
    }
    let datagroup = this._Func.groupBy(csrPkr.users, 'whsID');
    csrPkr.checkAreNotSetDefaultForEachWarehouse(datagroup);
  }


  onWarehouseChange(csrPkr:CsrPkr, i, tabName='') {
    csrPkr.resetUserDuplicate();
    csrPkr.users[i].userID = ''; // set Name = Select user when change warehouse
    if(csrPkr.users[i].whsID != ''){
      if(tabName == 'csr') {
        this.getListCRSByCustomer(this.cusId,csrPkr.users[i].whsID, i);
        let datagroup = this._Func.groupBy(this.csr.users, 'whsID');
        this.csr.checkAreNotSetDefaultForEachWarehouse(datagroup);
      }
    }
  }

  submitCSR() {
    this.showCsrErr = true;
    if(this.csrForm.valid) {
      this.save(JSON.stringify(this.csrForm.value));
    }
  }



  save(data) {
    var target = this;
    this.showLoadingOverlay = true;
    this._CTService.addUserOfCustomer(this.cusId, data)
      .subscribe(
        data => {
          this.doneAPI++;
        },
        err => {

          target.showLoadingOverlay = false;
        },
        () => {
          target.showLoadingOverlay = false;
        })
  }

  listCsrUser = [];
  getListCsrUser(cusId) {
    this.showLoadingOverlay = true;
    this._CTService.getListCsrUser(cusId)
        .subscribe(
            data => {

              this.listCsrUser = data.data;
              if(this.listCsrUser.length > 0){
                for(let i=0; i < this.listCsrUser.length; i++) {
                  let defaultCheck = this.listCsrUser[i].set_default == 1 ? true: false;
                  let user = this.csr.addUser(new User(this.listCsrUser[i].user_id, defaultCheck, this.listCsrUser[i].set_default, this.listCsrUser[i].whs_id));
                  this.csrControlArr.push(this.genControls(user));
                  this.getListCRSByCustomer(this.cusId,this.listCsrUser[i].whs_id,i);
                }
              }else{
                let user = new User('', true);
                this.csr.addUser(user);
                this.csrControlArr.push(this.genControls(user))
              }
              this.csr.checkDuplicated();
              this.showLoadingOverlay = false;
            },
            err => {

              this.showMessage('danger', this._Func.parseErrorMessageFromServer(err));
              this.showLoadingOverlay = false;
            }
        );
  }



  /*-------------------*/

  areDuplicated(control:Control):{[key:string]:any} {
    let value = control.value;
    if(value == true)
        return {
            areDuplicated: true
        }
    return null;
  }

  areNotSetDefault(control:Control):{[key:string]:any} {
    let value = control.value;
    if(value == true)
      return {
        areNotSetDefault: true
      }
    return null;
  }

  private showMessage(msgStatus, msg) {
    this.messages = {
      'status': msgStatus,
      'txt': msg
    }
    jQuery(window).scrollTop(0);
  }

    private handleCheckbox(_index,csrPkr:CsrPkr, type){
        let whs_id = csrPkr.users[_index].whsID;
        if(type == 'csr'){
            let data = this.csrForm.controls['value']['controls'];
            for(let i = 0; i < data.length;i++){
                if(data[i]['_value'].whs_id == whs_id){
                    this.csr.users[i].defaultCheck = false;
                    (<Control>this.csrForm.controls['value']['controls'][i]['controls']['set_default']).updateValue('0');
                }
            }
            this.csr.users[_index].defaultCheck = true;
            (<Control>this.csrForm.controls['value']['controls'][_index]['controls']['set_default']).updateValue('1');
        }
        
    }

  private getCurrency() {

    this._CTService.getCurrency()
        .subscribe(
            data => {
              this.listCurrency = data;
            },
            err => {
              this.showMessage('danger', this._Func.parseErrorMessageFromServer(err));

            }
        );
  }

  private setClickedRow(item, ListName, state, paginId) {
    item.selected = !item.selected;
    this.checkCheckAll(ListName,state,paginId);
  }

  getCarrierData() {
    this.showLoadingOverlay = true;
    this._CTService.getCarrierData(this.cusId)
      .subscribe(
        data => {
          this.carrierInfo = data.data;
          this.buildCarrier();
          this.showLoadingOverlay = false;
        },
        err => {
          this.showMessage('danger', this._Func.parseErrorMessageFromServer(err));
          this.showLoadingOverlay = false;
        }
      );
  }

  carrierForm: ControlGroup;
  carrierInfo = [];
  buildCarrier() {
    const fdx = this.carrierInfo ? this.getQualifier(this.carrierInfo, 'FDX') : '';
    const ups = this.carrierInfo ? this.getQualifier(this.carrierInfo, 'UPS') : '';
    this.carrierForm = this.fb.group({
      fdx: this.fb.group({
        FEDEX_KEY: [fdx ? fdx['FEDEX_KEY'] : ''],
        FEDEX_PASSWORD: [fdx ? fdx['FEDEX_PASSWORD'] : ''],
        FEDEX_METER_NUMBER: [fdx ? fdx['FEDEX_METER_NUMBER'] : ''],
        FEDEX_ACCOUNT_NUMBER: [fdx ? fdx['FEDEX_ACCOUNT_NUMBER'] : ''],
        FEDEX_APP_ENV: [fdx ? fdx['FEDEX_APP_ENV'] : ''],
        visible: [fdx ? fdx['visible'] : false]
      }),

      ups: this.fb.group({
        UPS_LICENSE_NUM: [ups ? ups['UPS_LICENSE_NUM'] : ''],
        UPS_USERNAME: [ups ? ups['UPS_USERNAME'] : ''],
        UPS_PASSWORD: [ups ? ups['UPS_PASSWORD'] : ''],
        UPS_APP_ENV: [ups ? ups['UPS_APP_ENV'] : ''],
        UPS_ACCOUNT_NUMBER: [ups ? ups['UPS_ACCOUNT_NUMBER'] : ''],
        visible: [ups ? ups['visible'] : false]
      })
    })
  }

  getQualifier(arr: any[], key: string): any {
    let val = '';

    arr.forEach(item => {
      if (item['qualifier'].toLowerCase() === key.toLowerCase()) {
        val = key.toLowerCase() === 'ups' || key.toLowerCase() === 'fdx' ? JSON.parse(item['value']) : item['value'];
      }
    });

    return val;
  }

  prepareQualifier(obj: any): any[] {
    let val = [];

    for (const key in obj) {
      if (obj.hasOwnProperty(key)) {
        val.push({ 'qualifier': key.toUpperCase(), 'value': obj[key] });
      }
    }

    return val;
  }

  saveCarrier() {
    if (this.carrierForm.valid) {
      var target = this;
      this.showLoadingOverlay = true;
      const configInfoJson = JSON.stringify(this.prepareQualifier(this.carrierForm.value));
      this._CTService.submitCarrier(this.cusId, configInfoJson)
        .subscribe(
          data => {
            this.doneAPI++;
          },
          err => {
            target.showLoadingOverlay = false;
          },
          () => {
            target.showLoadingOverlay = false;
          })
    }
  }
}