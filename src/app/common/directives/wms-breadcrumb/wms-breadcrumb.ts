import {Component,Input,Output,EventEmitter,ElementRef, Injectable} from '@angular/core';
import {Http} from '@angular/http';
import { Router,RouteData} from '@angular/router-deprecated';
import {DEFAULT_TEMPLATES} from './templates';
import 'rxjs/add/operator/filter';
@Component({
  selector: 'wms-breadcrumb',
  providers:[],
  directives: [],
  template: DEFAULT_TEMPLATES,
})
@Injectable()
export class WMSBreadcrumb {


  @Output() dataSelected  = new EventEmitter();

  @Output() sortData  = new EventEmitter();

  private currentInstruction;
  private urlCurrent;
  private wmsbreadcrumb=[];
  private currentBrc;
  private currentComponent:string;
  private menuData:Array<any>=[];
  private columnnameCPM='description';

  constructor(
    private _router:Router,
    private elementRef: ElementRef,
    private routerData: RouteData,
    private _http: Http){

    // get menu data;
    this.getMenuLocationStoge();


    this.moduleRouter(this._router.root);

    this._router.root.subscribe((router)=>{
      //console.log('router', router);
      if(typeof router!=='undefined'){
        // when every change router , reset
        this.moduleRouter(router);
      }
    });

  }

  moduleRouter(router){
    this.wmsbreadcrumb=[];
    this.urlCurrent=router;
    this.currentInstruction= this._router.root.currentInstruction;
   // console.log('_currentInstruction ',this.currentInstruction );
    this.getcurrentComponent(this.currentInstruction);
    this.buildBreadCrumNoRecusive(this.menuData);
    this.detechLastRouter(this.currentComponent);
    this.detechBrcCurrent();
  }

  getcurrentComponent(insData){
      if(insData['child']!==null){
        // recusive still is null
        this.getcurrentComponent(insData['child']);
      }else{
        this.currentComponent=insData['component']['componentType']['name'];
      }
  }

  detechLastRouter(componentTypeName:string){
    let lasrBrc;
    var action = this.routerData.get('Action') || this.routerData.get('action');
    if(action){
      lasrBrc=action;
    }else{
      if(componentTypeName.search(/list/i)>=0){
        lasrBrc='List'
      }
      if(componentTypeName.search(/new/i)>=0||componentTypeName.search(/create/i)>=0){
        lasrBrc='New'
      }
      if(componentTypeName.search(/detail/i)>=0||componentTypeName.search(/view/i)>=0){
        lasrBrc='View'
      }
      if(componentTypeName.search(/edit/i)>=0){
        lasrBrc='Edit'
      }
    }
    this.currentBrc=lasrBrc;
  }

  getMenuLocationStoge(){

    let menuData=JSON.parse(localStorage.getItem('leftmenu'));
    if(typeof menuData!=='undefined'){
      this.menuData=menuData;
    }
    else{
      this.menuData=[];
    }

  }



  private path=[];
  private childnodes=false;
  private parentNode;

  buildBreadcrumb(menuData){

      let brcTemplate:Array<any>=[];
      for(let i in menuData)
      {
        if(this.parentNode){
          //
          console.log('add parent node for child ',menuData[i]['name']);
          menuData[i]['parent']=this.parentNode;
          console.log('item after add parent node',menuData[i]);
        }
        if(this.isCurrentComponent(this.currentComponent,menuData[i]['description'])){
          // exit break;
          console.log('breadcrumb data', menuData[i]);
          return;
        }
        else{
          // if not valid -> check child
          if(menuData[i]['nodes'].length>0){
            // browser childs
            console.log('add parent node ',menuData[i]['name'] );
             this.parentNode= menuData[i];
             menuData[i]['nodes']
              this.buildBreadcrumb(menuData[i]['nodes']);
          }else{

          }
        }

      }


  }


  buildBreadCrumNoRecusive(menuData){

    let brcTemplate:Array<any>=[];
    for(let i in menuData)
    {
      if(this.isCurrentComponent(this.currentComponent,menuData[i]['description'])){
        this.wmsbreadcrumb.push(this.getPatch(menuData[i]));
        //console.log('this.wmsbreadcrumb', this.wmsbreadcrumb);
        return;
      }
      else{
        // if not valid -> check child
        if(menuData[i]['nodes'].length>0){
          // browser childs

          for(let j in menuData[i]['nodes'])
          {

            if(this.isCurrentComponent(this.currentComponent,menuData[i]['nodes'][j]['description'])){
              this.wmsbreadcrumb.push(this.getPatch(menuData[i]));
              this.wmsbreadcrumb.push(this.getPatch(menuData[i]['nodes'][j]));
              //console.log('this.wmsbreadcrumb', this.wmsbreadcrumb);
              return;
            }

          }

        }
      }

    }

  }

  getPatch(item){
    return {txt:item['name'],patch:item['url']};
  }

  detechBrcCurrent(){
    if(this.currentBrc==null|| typeof  this.currentBrc=='undefined'){
      // fix bug current undefined
        if( this.wmsbreadcrumb.length>0){
          this.wmsbreadcrumb[this.wmsbreadcrumb.length-1]['current']=true;
        }
    }
  }

  /**
   * Check menu item component current
   * */
  isCurrentComponent(currentCpn, menuCp){
    let el=menuCp.indexOf(currentCpn);
    return el>=0 ? true : false;
  }

  componentItemMenu(itemComponent){

      return itemComponent.split(",");

  }

}
