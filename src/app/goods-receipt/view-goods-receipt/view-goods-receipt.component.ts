import {Component, DynamicComponentLoader, ElementRef, Injector} from '@angular/core';
import { FORM_DIRECTIVES,ControlGroup,FormBuilder, Control ,Validators} from '@angular/common';
import { Http } from '@angular/http';
import { Router, RouterLink, ROUTER_DIRECTIVES, RouteParams } from '@angular/router-deprecated';
import 'ag-grid-enterprise/main';
import 'rxjs/add/operator/map';
import {API_Config} from "../../common/core/API_Config";
import {Functions} from "../../common/core/functions";
import {GoodsReceiptServices} from "../goods.receipt.service";
import {UnsetDamageDirective} from "../unset-damage/unset-damage";
import {JwtHelper} from "angular2-jwt/angular2-jwt";
import {PalletDirective} from "../pallet/pallet";
import {
    WMSBreadcrumb, PaginatePipe,WMSMessages,WMSPagination, LaborCostDirective, PaginationControlsCmp,
    DocumentUpload, PaginationService, InvoiceTracking, ReportFileExporter, WMSDatepicker, DmsIn
} from '../../common/directives/directives';
import { ValidationService } from '../../common/core/validator';

declare var jQuery: any;
declare var saveAs: any;
import {GoBackComponent} from "../../common/component/goBack.component";
import {UserService} from "../../common/users/users.service";

@Component ({
    selector: 'view-goods-receipt',
    templateUrl: 'view-goods-receipt.component.html',
    directives:[
        GoBackComponent,UnsetDamageDirective, WMSMessages,DocumentUpload,LaborCostDirective,
        WMSPagination,PaginationControlsCmp, GoBackComponent,WMSBreadcrumb, PalletDirective,
        InvoiceTracking, ReportFileExporter, WMSDatepicker, DmsIn
    ],
    providers: [GoodsReceiptServices, PaginationService],
    pipes:[PaginatePipe],
})

export class ViewGoodsReceiptComponent {

    private id: any;
    private listNull = {};
    private listNull2 = {};
    private dataListContainer = {};
    private messages:any;
    private tmpItem = [null];
    private dataListMeasurements = [];
    private dataListContainersByASN = [];
    private dataListCustomer = [];
    private dataAsn = [];
    private loc_whs_id: any;
    private messagesItem = {};
    private dataGoodReceipt = [];
    private urlAPIGR: any;
    private urlPrintLPN: any;
    private urlPrintDMC: any;
    private flag_print = false;
    private flag_dmc = false;
    private hasViewPut = false;
    private hasSavePut = false;
    private dataEventTracking = [];
    private status = {
        'RE' : '',
        'RG' : ''
    };
    private timeoutHideMessage;
    private perPage=20;
    private Pagination;
    private currentPage = 1;
    private numLinks = 3;
    private showForm = false;
    private showLoadingOverlay = false;
    private loggerUser = {};
    private completeGRList = [];
    private messagesCompleteGR:any;
    private flagCLGR = false;
    private viewGRForm: ControlGroup;
    private submitForm = false;
    private listGRTypes = [];
    private listContainerSizes = [
        {'value': null, 'label': 'None'},
        {'value': 20, 'label': '20'},
        {'value': 40, 'label': '40'},
        {'value': 45, 'label': '45'},
        {'value': 53, 'label': '53'},
    ];
    private force_gr = false;
    // Export csv file
	public exportAPIUrl:string = '';
    public fileName:string = 'Goods-Receipt.csv';
    public whsId = '';
    // confirm complete list
    public listError = [];
    // Construct
    constructor (
        private _router:Router,
        private _goodsReceiptServices: GoodsReceiptServices,
        params: RouteParams,
        private _Func: Functions,
        public jwtHelper: JwtHelper,
        private _API_Config: API_Config,
        private _user: UserService,
        private fb: FormBuilder,
        private _Valid: ValidationService,
    ) {

        // Check permission
        this.checkPermission();
        this.urlAPIGR = this._API_Config.API_Goods_Receipt;
        this.urlPrintLPN = this._API_Config.API_Goods_Receipt;
        this.urlPrintDMC = this._API_Config.API_Damage_Carton;
        this.id = params.get("id");
        this.whsId = localStorage.getItem('whs_id');
        this.getGoodsReceiptById();
        this.getLoggedUser();
        this.getListGRType();
    }

    FormBuilder(data={})
    {
        this.viewGRForm = this.fb.group({
            in_note: [data['in_note'] ? data['in_note'] : ''],
            ex_note: [data['ex_note'] ? data['ex_note'] : ''],
            gr_type: [data['gr_type'] ? data['gr_type'] : 'CTNR'],
            ctnr_size: [data['ctnr_size'] ? data['ctnr_size'] : null],
            pallet_ttl: [data['pallet_ttl'] ? data['pallet_ttl'] : null, Validators.compose([this._Valid.isZero,this._Valid.validateInt])],
            volume_unit: [data['volume_unit'] ? data['volume_unit'] : 'volume'],
            volume: [data['volume'] ? data['volume'] : null, Validators.compose([this._Valid.isZero,this._Valid.isDecimalNumberMore])],
            cube: [data['cube'] ? data['cube'] : null],
            gr_hdr_act_dt: [data['gr_hdr_act_dt'] ? data['gr_hdr_act_dt'] : data['gr_hdr_ept_dt']],
        });
    }

    getListGRType() {
        this._goodsReceiptServices.getGoodsReceiptType().subscribe(
            data => {
                this.listGRTypes = data.data;
            },
            err => {
                this.showMessage('danger', this._Func.parseErrorMessageFromServer(err));
            },
            () => {}
        )
    }

    private hasPermissionUpdatePallet;
    // Check Permission
    private checkPermission() {

        this._user.GetPermissionUser().subscribe(

            data => {

                this.hasViewPut = this._user.RequestPermission(data, 'viewPutaway');
                this.hasSavePut = this._user.RequestPermission(data, 'saveAndPrintPutaway');
                this.hasPermissionUpdatePallet = this._user.RequestPermission(data, 'editGoodsReceipt');
                var flag = this._user.RequestPermission(data,'viewGoodsReceipt');

                if(!flag) {
                    this._router.parent.navigateByUrl('/deny');
                }

            },
            err => {

                this.showMessage('danger', this._Func.parseErrorMessageFromServer(err));

            }
        );
    }

    private showMessage(msgStatus, msg) {

        this.messages = {
            'status': msgStatus,
            'txt': msg
        }
        jQuery(window).scrollTop(0);
    }

    private requestPermission(permission, permission_req) {

        if (permission && permission.length) {

            for (var per of permission) {

                if (per.name == permission_req) {

                    return true;
                }
            }

        }

        return false;

    }

    isExistedReceivingGR: boolean;
    checkExistedReceivingGR(lstItem: Array<any>) {
        let existed = lstItem.some((item) => item.dtl_gr_dtl_sts.toUpperCase() == 'RECEIVING');
        return existed;
    }

    // Get goods receipt by id
    private getGoodsReceiptById() {

        this._goodsReceiptServices.getGoodsReceiptById(this.id)
            .subscribe(
                data => {

                    this.dataGoodReceipt = data.data;
                    this.FormBuilder(this.dataGoodReceipt);
                    if (this.dataGoodReceipt['created_from'] != 'WMS') {
                        //this.flagCLGR = true;
                        this.ckCancelledAllItems();
                    }

                    this.isExistedReceivingGR = this.checkExistedReceivingGR(this.dataGoodReceipt['gr_details']);
                },
                err => {

                    this.parseError(err);
                },
                () => {

                    this.getEventTrackingByAsn();

                    if (this.dataGoodReceipt['gr_sts_code'].toLowerCase() == 're' && this.dataGoodReceipt['created_from'].toLowerCase() != 'wap' && this.dataGoodReceipt['created_from'].toLowerCase() != 'gun' ) {
                        this.flag_print = true;
                        this.ckFlagDMC();
                    }

                    if (this.dataGoodReceipt['gr_sts_code'].toLowerCase() === 're') {
                        this.fileName = 'Goods-Receipt-' + this.dataGoodReceipt['gr_hdr_num'] +'.csv';
                        this.exportAPIUrl = `${this._API_Config.API_GOODS_RECEIPT_REPORT}/reports/${this.whsId}?gr_hdr_num=${this.dataGoodReceipt['gr_hdr_num']}&export=1`;
                    }
                }
            );
    }

    // Get Asn by ID
    private getEventTrackingByAsn (page = 1) {

        var param = '?owner=' + this.dataGoodReceipt['gr_hdr_num'] + '&type=ib';

        param += "&page=" + page + "&limit=" + this.perPage;
        this.showLoadingOverlay=true;

        try {

            this._goodsReceiptServices.getEventTrackingByAsn(param)
                .subscribe(
                    data => {

                        this.dataEventTracking = data.data;

                        this.Pagination = data['meta']['pagination'];
                        this.Pagination['numLinks'] = 3;
                        this.Pagination['tmpPageCount'] = this._Func.Pagination(this.Pagination);
                        this.showLoadingOverlay=false;
                    },
                    err => {
                        this.showLoadingOverlay=false;
                        this.parseError(err);
                    },
                    () => {}
                );

        } catch (err) {

            this.messages = this._Func.Messages('danger', this._Func.msg('VR100'));
        }

    }

    // ckFlagDMC
    private ckFlagDMC () {

        for (var i = 0; i < this.dataGoodReceipt['gr_details'].length; i++ ) {

            if (this.dataGoodReceipt['gr_details'][i]['dtl_gr_dtl_is_dmg'] == '1') {

                this.flag_dmc = true;
                return false;
            }

        }

        return false;
    }
    private printPutAway() {

        try {

            var that = this;
            that.messages = false;
            this.messagesCompleteGR = false;

            var xhr = new XMLHttpRequest();
            xhr.open("GET", this.urlAPIGR + "/" + this.id + "/print", true);
            xhr.setRequestHeader("Authorization", 'Bearer ' +this._Func.getToken());
            xhr.responseType = 'blob';

            xhr.onreadystatechange = function () {
                if(xhr.readyState == 2) {
                    if(xhr.status == 200) {
                        xhr.responseType = "blob";
                    } else {
                        xhr.responseType = "text";
                    }
                }
                // If we get an HTTP status OK (200), save the file using fileSaver
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {

                        var blob = new Blob([this.response], {type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'});
                        saveAs.saveAs(blob, that.dataGoodReceipt['gr_hdr_num'] + ' Putaway List.pdf');

                    } else {
                        let errMsg = '';
                        if(xhr.response) {
                            try {
                                var res = JSON.parse(xhr.response);
                                errMsg = res.errors && res.errors.message ? res.errors.message : xhr.statusText;
                            }
                            catch(err){errMsg = xhr.statusText}
                            }
                        else {
                            errMsg = xhr.statusText;
                        }
                        if(!errMsg) {
                            errMsg = that._Func.msg('VR100');
                        }
                        that.messages = that._Func.Messages('danger', errMsg);
                    }
                }
            };

            xhr.send();
        } catch (err) {

            this.messages = this._Func.Messages('danger', this._Func.msg('VR100'));
        }
    }

    //
    private printLPN() {

        try {

            var tar_get = this;
            tar_get.messages = false;
            this.messagesCompleteGR = false;
            var xhrl = new XMLHttpRequest();

            xhrl.open("GET", this.urlPrintLPN + "/" + this.id + "/print-lpn", true);
            xhrl.setRequestHeader("Authorization", 'Bearer ' +this._Func.getToken());
            xhrl.responseType = 'blob';

            xhrl.onreadystatechange = function () {

                // If we get an HTTP status OK (200), save the file using fileSaver
                if (xhrl.readyState === 4) {

                    if (xhrl.status === 200) {

                        var blob = new Blob([this.response], {type: 'application/pdf'});
                        saveAs.saveAs(blob, tar_get.dataGoodReceipt['gr_hdr_num'] + '_License_Plate.pdf');

                    } else {

                        tar_get.messages = tar_get._Func.Messages('danger', xhrl.statusText);
                    }
                }
            };

            xhrl.send();
        } catch (err) {

            this.messages = this._Func.Messages('danger', this._Func.msg('VR100'));
        }

    }

    private printDMC() {

        try {

            var tar_get = this;
            tar_get.messages = false;
            this.messagesCompleteGR = false;
            var xhrl = new XMLHttpRequest();
            xhrl.open("POST", this.urlPrintDMC + "/print/" + this.id , true);
            xhrl.setRequestHeader("Authorization", 'Bearer ' +this._Func.getToken());
            xhrl.responseType = 'blob';

            xhrl.onreadystatechange = function () {

                // If we get an HTTP status OK (200), save the file using fileSaver
                if (xhrl.readyState === 4) {

                    if (xhrl.status === 200) {

                        var blob = new Blob([this.response], {type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'});
                        saveAs.saveAs(blob, tar_get.dataGoodReceipt['gr_hdr_num'] + '_Damaged CTN.pdf');

                    } else {

                        tar_get.messages = tar_get._Func.Messages('danger', xhrl.statusText);
                    }
                }
            };

            xhrl.send();
        } catch (err) {

            this.messages = this._Func.Messages('danger', this._Func.msg('VR100'));
        }

    }

    // Show error when server die or else
    private parseError (err) {

        this.messages = {'status' : 'danger', 'txt' : this._Func.parseErrorMessageFromServer(err)};
    }

    // Event when user filter form
    private onPageSizeChanged ($event,el) {

        this.perPage = $event.target.value;

        if((this.Pagination['current_page'] - 1) * this.perPage >= this.Pagination['total']) {
            this.currentPage = 1;
        }
        else {
            this.currentPage = this.Pagination['current_page'];
        }

        this.getEventTrackingByAsn(this.currentPage);
    }

    private getDateTime(){
        var now   = new Date();
        var year  = now.getFullYear();
        var month = now.getMonth();
        var date  = now.getDate().toString();
        var day   = now.getDay();
        var dayNames = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
        var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        if(date.toString().length == 1) {
            var date = '0'+date;
        }
        var dateTime = dayNames[day] + ', ' + monthNames[month] +' '+date+', '+year;
        return dateTime;
    }

    //get Name
    private getLoggedUser(){
        this.loggerUser = localStorage.getItem('logedUser');
        this.loggerUser = JSON.parse('' + this.loggerUser);
    }

    private checkActualDate() {
        let actDateCtrl = this.viewGRForm.controls['gr_hdr_act_dt'];
        let actDateVal = jQuery("input[name='gr_hdr_act_dt']").val().trim();

        if(actDateVal == null || actDateVal === '') {
            actDateCtrl.setErrors({'required': true});
            this.submitForm = true;
            return false;
        }
        if (!this.checkValidateComplete()) {
            return false;
        }
        return true;
    }

    private er1 = false;
    private completeGR(msgErr = 'messages') {
        if (this.viewGRForm.valid) {
            const paramsPost = this.viewGRForm.value;

            if (!this.checkActualDate()) {
                return false;
            }

            try {
                const target = this;
                target.er1 = false;
                this.showLoadingOverlay = true;
                this[msgErr] = false;
                const loc_whs_id = localStorage.getItem('whs_id');
                this._goodsReceiptServices.completeGR(loc_whs_id, this.dataGoodReceipt['gr_hdr_id'], '', JSON.stringify(paramsPost))
                    .subscribe(
                        data => {

                            this.completeGRList = data.data;
                            this.showLoadingOverlay=false;

                            if (data.error == '1') {
                                target.er1 = true;
                                jQuery("#completeGRConfirm").modal("hide");
                                jQuery("#completeGR").modal("show");

                            } else if(data.error == '2') {
                                jQuery("#completeGRConfirm").modal("hide");
                                jQuery("#completeGR").modal("show");

                            } else {
                                target.getGoodsReceiptById();
                                target.flagCLGR = false;
                                this.messages = this._Func.Messages('success', this._Func.msg('CLGR001'));
                                jQuery("#completeGRConfirm").modal("hide");
                            }


                        },
                        err => {
                            this.showLoadingOverlay=false;
                            this[msgErr] =this._Func.Messages('danger',this._Func.parseErrorMessageFromServer(err));
                        },
                        () => {}
                    );

            } catch (err) {
                this[msgErr] = this._Func.Messages('danger', this._Func.msg('VR100'));
            }
        } else {
            this.submitForm = true;
        }
    }

    private agreeCompleteGR () {

        try {
            var target = this;
            this.showLoadingOverlay = true;
            this.messagesCompleteGR = false;
            this.messages = false;
            var loc_whs_id = localStorage.getItem('whs_id');
            const param = this.force_gr ? '&force_gr=true' : '';
            this._goodsReceiptServices.completeGR(loc_whs_id, this.dataGoodReceipt['gr_hdr_id'], '?agree=1' + param)
                .subscribe(
                    data => {

                        target.getGoodsReceiptById();
                        this.showLoadingOverlay=false;
                        this.messages = this._Func.Messages('success', this._Func.msg('CLGR003'));
                        target.flagCLGR = false;
                        target.force_gr = false;
                        jQuery("#completeGR").modal("hide");
                    },
                    err => {

                        this.showLoadingOverlay=false;
                        this.messagesCompleteGR = {'status' : 'danger', 'txt' : this._Func.parseErrorMessageFromServer(err)};
                    },
                    () => {}
                );

        } catch (err) {

            this.messagesCompleteGR = this._Func.Messages('danger', this._Func.msg('VR100'));
        }

    }

    private ckCancelledAllItems () {
        let cancelledAllItems = true;
        for (var i = 0; i < this.dataGoodReceipt['gr_details'].length; i++ ) {
            const status = this.dataGoodReceipt['gr_details'][i]['dtl_gr_dtl_sts'];
            if (status && status.toLowerCase() !== 'cancelled') {
                cancelledAllItems = false;
                break;
            }

        }
        this.flagCLGR = !cancelledAllItems;
    }

    checkInputNumber($event, int){
        this._Valid.isNumber($event, int);
    }

    checkInputFloatNumber(evt, val) {
        this._Valid.validateFloatKeyPress(val, evt);
    }

    updateMessages(messages) {
		this.messages = messages;
    }

    private checkValidateComplete() {
        let actDateCtrl = this.viewGRForm.controls['gr_hdr_act_dt'];
        let actDateVal = jQuery("input[name='gr_hdr_act_dt']").val().trim();
        if (actDateVal) {
            const currMonth = new Date().getMonth() + 1;
            const currYear = new Date().getFullYear();
            const currDate = new Date().getDate();
            const monthVal = new Date(actDateVal).getMonth() + 1;
            const yearVal = new Date(actDateVal).getFullYear();
            if ((yearVal == currYear && (monthVal == currMonth-1 && currDate > 7)||monthVal < currMonth - 1)||(yearVal < currYear-1)||(yearVal == currYear-1 && currMonth != 1 && currDate > 7)) {
                actDateCtrl.setErrors({'invalid': true, 'required': false});
                this.submitForm = true;
                return false;
            }
            actDateCtrl.setErrors(null);
        }

        return true;
    }

    public completeGRConfirm() {
        this.showForm = false;
        this.listError = [];
        this.messagesCompleteGR = false;
        setTimeout(() => {
            this.listError = this.dataGoodReceipt['gr_details'].filter(x => x.dtl_item_status_code =='RG');
            if (this.listError.length > 0) {
                this.showForm = true;
                jQuery("#completeGRConfirm").modal("show");
            } else {
                this.completeGR();
            }
        });
    }

    approveGR() {
        const dataObj = {
            gr_ids: [this.id]
        };

        let that = this;
        this._goodsReceiptServices.approveGoodsReceipt(JSON.stringify(dataObj)).subscribe(res => {
            if (res.data.message) {
                this.messages = this._Func.Messages('success', res.data.message);
            }

            setTimeout(function () {
                that.goToList();
            }, 600);
        });
    }

    goToList() {
        this._router.navigateByUrl('goods-receipt');
    }

    /**
     * Get highlight color based on status code
     *
     * @param dtl_item_status_code
     */
    getHighlightColor(dtl_item_status_code: string) {
        return dtl_item_status_code == 'RG' ? 'red' : 'black';
    }
}
