import {Component, Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {API_Config, Functions} from '../../common/core/load';

@Injectable()
export class CountryService {

  public AuthHeader = this._Func.AuthHeader();
  public AuthHeaderPost = this._Func.AuthHeaderPost();

  constructor(private _Func: Functions, private _API: API_Config, private http: Http)
  {

  }

  /*1/
   * Country List
   * */
  Country()
  {
    return this.http.get(this._API.API_Country + '?limit=1000&sort[sys_country_name]=asc',{ headers: this.AuthHeader })
      .map((res: Response) => res.json());
  }

    /*
    ****************************************************************************
    */

    get()
    {
        return this.http.get(this._API.API_Country + '?sort[sys_country_name]=asc',{
            headers: this.AuthHeader
        })
        .map((res: Response) => res.json());
    }

    /*
    ****************************************************************************
    */

    getDropdown()
    {
        let params = '?sort[sys_country_name]=asc';

        return this.http.get(this._API.API_Country + '/dropdown' + params, {
            headers: this.AuthHeader
        })
        .map((res: Response) => res.json());
    }

  /*
   * State
   * */
  State(idCountry)
  {
    return this.http.get(this._API.API_Country+'/'+idCountry+'/states?sort[sys_state_code]=asc',{ headers: this.AuthHeader })
      .map((res: Response) => res.json());
  }

  getCountry(params) {
    return this.http.get(this._API.API_Country + params,{ headers: this.AuthHeader })
      .map((res: Response) => res.json());
  }

  getCountryDetail(countryID) {
    return this.http.get(this._API.API_Country+'/'+countryID, { headers: this.AuthHeader })
      .map((res: Response) => res.json().data);
  }

  updateCountry(countryID, stringData) {
    return this.http.put(this._API.API_Country+'/'+countryID, stringData, { headers: this._Func.AuthHeaderPostJson() })
      .map((res: Response) => res.json().data);
  }

  addNewCountry(stringData) {
    return this.http.post(this._API.API_Country, stringData, { headers: this._Func.AuthHeaderPostJson() })
      .map((res: Response) => res.json().data);
  }

  search(param) {
    return this.http.get(this._API.API_Country+param,{ headers: this.AuthHeader })
      .map((res: Response) => res.json());
  }

}
