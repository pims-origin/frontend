import {Component, Input, Output, EventEmitter} from '@angular/core';
import {Functions} from '../../../common/core/load';
import {LabelingServices} from "./labeling.service";
declare var jQuery:any;

@Component({
	selector: 'labeling',
	providers: [LabelingServices],
	templateUrl: 'labeling.template.html'
})

export class Labeling {
	private ordHdrId:string = '';
	private canUpdate:boolean = true;
	private dataLabeling = [];
	private whs_id:any = '';

	@Input('orderHeaderId') set setOrderId(value:string) {
		this.ordHdrId = value;
		this.getOrderFlow();
	}

	@Output() messages=  new EventEmitter();

	constructor(
	            private funcs:Functions,
	            private labelingService:LabelingServices) {

		this.whs_id=this.funcs.lstGetItem('whs_id');

	}

	private getOrderFlow(){

		this.labelingService.getLabeling(this.whs_id, this.ordHdrId).subscribe(
			data => {

				this.dataLabeling = data['data'];
			},
			err => {

				this.parseError(err);
			},
			() => {
			}
		);

	}

	private parseError(err){
		this.messages.emit({'status' : 'danger', 'txt' : this.funcs.parseErrorMessageFromServer(err)});
	}

}
