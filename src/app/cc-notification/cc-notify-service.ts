import {Component, Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {Functions, API_Config} from '../common/core/load';

@Injectable()

export class CCNotifyServices {

    private headerGet = this._func.AuthHeader();
    private headerPostJson = this._func.AuthHeaderPostJson();

    private urlAPICustomerMaster = this._api.API_CUSTOMER_MASTER;
    private urlAPIPutAway = this._api.API_Put_Away;
    constructor(
        private _func: Functions,
        private _api: API_Config,
        private _http: Http) {

    }

    // Get customers by WH
    public getStatusCCN ($wh_id) {

        return  this._http.get(this._api.API_Cycle_Count_Root + '/ccn/' + $wh_id + '/status' , {headers: this.headerGet})
            .map(res => res.json());

    }

    // Get list asn
    public getListCCN ($wh_id ,$params = '') {

        return this._http.get(this._api.API_Cycle_Count_Root + '/ccn/' + $wh_id + '/list' +  $params, {headers: this.headerGet})
            .map(res => res.json());

    }

    public getLocationCCNL(params){
        return this._http.get(this._api.API_Cycle_Count_Root + '/ccn' + params,{ headers: this.headerGet })
            .map((res: Response) => res.json());
    }

    public autoSearchComplete($wh_id, $params){
        return this._http.get(this._api.API_Cycle_Count_Root + '/ccn/' + $wh_id +  $params, { headers: this.headerGet })
            .map((res: Response) => res.json());
    }

    public updateCCNotify($wh_id, $cc_id,  params) {

        return this._http.put(this._api.API_Cycle_Count_Root + '/ccn/' + $wh_id + '/save/' + $cc_id, params , {headers: this.headerPostJson})
            .map(res => res.json())

    }

    getCycleCountUsers() {

        return this._http.get(this._api.API_Cycle_Count_User, {headers: this.headerGet})
            .map(res => res.json());

    }

    public addNewCCNL(whs_id,data){
        return this._http.post(this._api.API_Cycle_Count_Root  + `/ccn/${whs_id}/save`, data, { headers: this.headerPostJson })
            .map((res: Response) => res.json());
    }

    public addCC(whs_id,data){
        return this._http.post(this._api.API_Cycle_Count_Root  + `/ccn/${whs_id}/add-list-cycle-count`, data, { headers: this.headerPostJson })
            .map((res: Response) => res.json());
    }

    public getSKUs(whs_id,params) {
        return this._http.get(`${this._api.API_Cycle_Count_Root}/ccn/${whs_id}/auto-sku` + params, {headers: this.headerGet})
            .map(res => res.json());

    }

    public getListReason() {
        return this._http.get(`${this._api.API_Cycle_Count_Root}/ccn/reason-drop-down?limit=20`, {headers: this.headerGet})
            .map(res => res.json());

    }
}

