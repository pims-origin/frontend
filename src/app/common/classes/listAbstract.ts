/**
 * Created by admin on 1/6/2017.
 */
import {Injectable} from "@angular/core";
import {Headers, Http} from "@angular/http";
import {API_Config} from "../core/API_Config";
declare var jQuery:any;
declare var bootbox:any;
@Injectable()
export abstract class ListAbstract {

    public headerURL;
    public urlList;
    private headerDef = [];
    private rowData:any[];
    public getSelectedRow;
    private objSort;
    private searchParams;
    private headerExport;
    private whs_id:any;
    private Pagination;
    private totalCount = 0;
    private tmpPageCount;
    private pageCount = 0;
    private ItemOnPage = 0;
    private currentPage = 1;
    private perPage = 20;
    private numLinks = 3;
    public messages:any;
    private dataListTable = [];
    private expandTable = false;
    private showLoadingOverlay = false;

    constructor(public objAPI, public tableID:string, public idForm:string, public headerD, public _http:Http,public _api:API_Config) {
        this.headerURL = this.objAPI.headerURL;
        this.urlList = this.objAPI.urlList;
        this.headerDef = this.headerD;
        this.whs_id = localStorage.getItem('whs_id');
        this.checkPermission();
        this.loadDataListTable(1);
        this.setUpData();
        setTimeout(() => {
            jQuery(this.tableID).on('mouseover', '.hover-value', function () {
                var ul = jQuery(this).find('ul'),
                    aTag = ul.prev();
                ul.css({
                    display: 'block',
                    position: 'fixed',
                    top: aTag.offset().top - jQuery(window).scrollTop() + 16,
                    left: aTag.offset().left,
                    zIndex: 9999
                })
            }).on('mouseout', '.hover-value', function () {
                var ul = jQuery(this).find('ul');
                ul.css({
                    display: 'none'
                })
            });
        }, 400);
    }

    checkPermission() {
        throw new Error("This is abstract method");
    }

    setUpData() {
        throw new Error("This is an abtract method");
    }

    afterGetSelectedRow($event) {
        throw new Error("This is an abtract method");
    }
    
    addNew(){
        throw new Error("This is an abtract method");
    }
    
    edit(){
        throw new Error("This is an abtract method");
    }
    loadDataListTable(page = null) {
        this.showLoadingOverlay = true;
        if (!page) page = 1;
        let params = "?page=" + page + "&limit=" + this.perPage;
        params += "&whs_id=" + this.whs_id;
        if (this.objSort && this.objSort['sort_type'] != 'none') {
            params += '&sort[' + this.objSort['sort_field'] + ']=' + this.objSort['sort_type'];
        }
        else {
            params += '&sort[created_at]=desc';
        }
        if (this.searchParams) {
            params += this.searchParams;
        }

        this._http.get(this.urlList + params, {headers: this.AuthHeader()})
            .map(res => res.json()).subscribe(
            data => {

                this.dataListTable = data.data;
                this.initPagination(data);
                this.createRowData(data);
                this.showLoadingOverlay = false;
            },
            err => {
                this.showLoadingOverlay = false;
                this.parseError(err);
            }
        )
    }

    private createRowData(data) {
        if (typeof data.data != 'undefined') {
            data = this.formatData(data.data);
            this.rowData = this.renderRowData(data);
        }
    }

    private renderRowData(data):any[] {
        let rowData:any[] = [];
        for (let i = 0; i < data.length; i++) {
            let temp = {};
            for (let j = 2; j < this.headerDef.length; j++) {
                temp[this.headerDef[j]['id']] = data[i][this.headerDef[j]['id']];
            }
            if(this.addMoreInfoToRow().length > 0){
                for(let k = 0 ; k < this.addMoreInfoToRow().length; k ++){
                    temp[this.addMoreInfoToRow()[k]] = data[i][this.addMoreInfoToRow()[k]];
                }
            }
            rowData.push(temp);
        }
        return rowData;
    }

    addMoreInfoToRow(): any[]{
        return []
    }
    search() {
        this.searchParams = '';
        let params_arr = jQuery(this.idForm).serializeArray();
        for (let i in params_arr) {
            if (params_arr[i].value == "") continue;
            this.searchParams += "&" + params_arr[i].name.trim() + "=" + encodeURIComponent(params_arr[i].value.trim());
        }
        this.loadDataListTable(1);
    }

    reset() {
        jQuery(this.idForm + ' select, input').filter(function () {
            jQuery(this).val("");
        });
        this.searchParams = '';
        this.loadDataListTable(1);
    }

    private doSort(objSort) {
        this.objSort = objSort;
        this.loadDataListTable(this.Pagination.current_page);
    }

    private onPageSizeChanged($event) {
        this.perPage = $event.target.value;
        if ((this.Pagination['current_page'] - 1) * this.perPage >= this.Pagination['total']) {
            this.currentPage = 1;
        }
        else {
            this.currentPage = this.Pagination['current_page'];
        }
        this.loadDataListTable(this.currentPage);
    }

    private filterList(pageNumber) {
        this.loadDataListTable(pageNumber);
    }

    private initPagination(data) {
        var meta = data.meta;
        this.Pagination = meta['pagination'];
        this.Pagination['numLinks'] = 3;
        this.Pagination['tmpPageCount'] = this.doPagination(this.Pagination);

    }

    private doPagination(data) {
        let totalCount = data['total'];
        let ItemOnPage = data['count'];
        let pageCount = data['total_pages'];
        let currentPage = data['current_page'];
        let perPage = data['per_page'];
        let numLinks = 3;
        let start = 1;
        let end = 1;
        let tmpPageCount = [];
        if (currentPage > numLinks) {
            start = currentPage - numLinks;
        } else {
            start = 1;
        }
        if (currentPage + numLinks < pageCount) {
            end = currentPage + numLinks;
        } else {
            end = pageCount;
        }
        //Create tmp for loop perPage in view
        for (var i = start; i <= end; i++) {
            tmpPageCount[i] = i;
        }
        tmpPageCount = tmpPageCount.filter(function (item) {
            return item !== undefined;
        });
        return tmpPageCount;
    }

    public parseError(err) {
        this.messages = {'status': 'danger', 'txt': this.parseErrorMessageFromServer(err)};
    }

    private formatData(data) {
        for (var i = 0, l = data.length; i < l; i++) {
            var item = data[i];
            for (let key in item) {
                if (typeof item[key] == 'string') {
                    item[key] = item[key].replace(/</g, '&lt;');
                }
                if (typeof item[key] == 'object') {
                    if (jQuery.isArray(item[key])) {
                        for (var j = 0, k = item[key].length; j < k; j++) {
                            var subItem = item[key][j];
                            if (typeof subItem == 'string') {
                                item[key][j] = item[key][j].replace(/</g, '&lt;');
                            }
                            if (typeof subItem == 'object') {
                                for (let key2 in subItem) {
                                    if (typeof subItem[key2] == 'string') {
                                        subItem[key2] = subItem[key2].replace(/</g, '&lt;');
                                    }
                                }
                            }
                        }
                    }
                    else {
                        for (let key2 in item[key]) {
                            if (typeof item[key][key2] == 'string') {
                                item[key][key2] = item[key][key2].replace(/</g, '&lt;');
                            }
                        }
                    }
                }
            }
        }
        return data;
    }

    private viewListFullScreen() {
        this.expandTable = true;
        setTimeout(() => {
            this.expandTable = false;
        }, 500);
    }

    showMessage(msgStatus, msg) {
        this.messages = {
            'status': msgStatus,
            'txt': msg
        }
        jQuery(window).scrollTop(0);
    }

    parseErrorMessageFromServer(err) {
        var errMessage = '';
        try {
            if (typeof err == 'object' && err._body) {
                err = JSON.parse(err._body);
            }
            else {
                err = err.json();
            }
            if (err.error) {
                if (typeof err.error == 'object') {
                    for (let key in err.error) {
                        let msgErr = typeof err.error[key] == 'string' ? err.error[key] : err.error[key][0];
                        errMessage += '<p>' + msgErr + '</p>'
                    }
                }
                if (typeof err.error == 'string') {
                    errMessage = err.error;
                }
            }
            else if (err.message || err.detail) {
                errMessage = err.message || err.detail;
            }
            else if (err.errors) {
                err = err.errors;
                errMessage = err.message || err.detail;
            }
        }
        catch (err) {
        }
        if (!errMessage) {
            errMessage = "Sorry, there's a problem with the connection.";
        }
        return errMessage;
    }

    AuthHeaderPostJson() {
        var authHeader = new Headers();
        authHeader.append('Content-Type', 'application/json');
        authHeader.append('Authorization', 'Bearer ' + localStorage.getItem('jwt'));
        return authHeader;
    }

    AuthHeader() {
        var authHeader = new Headers();
        authHeader.append('Authorization', 'Bearer ' + localStorage.getItem('jwt'));
        return authHeader;
    }

    RequestPermission(permission, permission_req) {
        if (permission && permission.length) {
            for (var per of permission) {
                //console.log(per.name + "<==>" + permission_req)
                if (per.name == permission_req) {
                    //console.log('found permission');
                    return true;
                }
            }
            return false;

        }
        else {
            return false;
        }
    }
}