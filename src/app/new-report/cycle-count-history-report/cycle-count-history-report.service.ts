import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {API_Config, Functions} from '../../common/core/load';

@Injectable()
export class CycleCountHistoryReportService {
	private authHeader:any;
	private authHeaderPost:any;
	private whsId:string;

	constructor(private func:Functions, private api:API_Config, private http:Http) {
		this.authHeader = func.AuthHeader();
		this.authHeaderPost = func.AuthHeaderPost();
		this.whsId = localStorage.getItem('whs_id');
	}

	getReports(params:string = '') {
		return this.http.get(`${this.api.API_CYCLE_COUNT_HIS_REPORT}/reports/${this.whsId}` + params, {headers: this.authHeader})
			.map(res => res.json());
	}

	getReportAPIUrl(params:string = '') {
		return `${this.api.API_CYCLE_COUNT_HIS_REPORT}/reports/${this.whsId}${params}&export=1`;
	}


	getCustomers(params:string = '') {
		return this.http.get(`${this.api.API_CYCLE_COUNT_HIS_REPORT}/customers/${this.whsId}` + params, {headers: this.authHeader})
			.map(res => res.json());
	}

	getStatues(params:string = '') {
		return this.http.get(`${this.api.API_CYCLE_COUNT_HIS_REPORT}/status` + params, {headers: this.authHeader})
			.map(res => res.json());
	}

	getCycleTypes(params:string = '') {
		return this.http.get(`${this.api.API_CYCLE_COUNT_HIS_REPORT}/cycle-type` + params, {headers: this.authHeader})
			.map(res => res.json());
	}

	getCountBys(params:string = '') {
		return this.http.get(`${this.api.API_CYCLE_COUNT_HIS_REPORT}/count-by` + params, {headers: this.authHeader})
			.map(res => res.json());
	}

	getCycleNums(params:string = '') {
		return this.http.get(`${this.api.API_CYCLE_COUNT_HIS_REPORT}/auto-cycle-num/${this.whsId}` + params, {headers: this.authHeader})
			.map(res => res.json());
	}

	getLocations(params:string = '') {
		return this.http.get(`${this.api.API_CYCLE_COUNT_HIS_REPORT}/auto-location/${this.whsId}` + params, {headers: this.authHeader})
			.map(res => res.json());
	}

	getSKUs(params:string = '') {
		return this.http.get(`${this.api.API_CYCLE_COUNT_HIS_REPORT}/auto-sku/${this.whsId}` + params, {headers: this.authHeader})
			.map(res => res.json());
	}
}
