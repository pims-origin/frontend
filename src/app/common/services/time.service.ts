/**
 * Created by admin on 6/9/2016.
 */
import {Injectable} from '@angular/core';
declare var jQuery: any;

@Injectable()
export class TimeService{
  constructor(){

  }

  getListTimeZones(){
    let timeZones: Array<string> = [];
    timeZones.push('(UTC-11:00) Midway Island , Pacific/Midway');
    timeZones.push('(UTC-11:00) Samoa , Pacific/Samoa');
    timeZones.push('(UTC-10:00) Hawaii , Pacific/Honolulu');
    timeZones.push('(UTC-09:00) Alaska , US/Alaska');
    timeZones.push('(UTC-08:00) Pacific Time (US & Canada) , America/Los_Angeles');
    timeZones.push('(UTC-08:00) Tijuana , America/Tijuana');
    timeZones.push('(UTC-07:00) Arizona , US/Arizona');
    timeZones.push('(UTC-07:00) Chihuahua , America/Chihuahua');
    timeZones.push('(UTC-07:00) La Paz , America/Chihuahua');
    timeZones.push('(UTC-07:00) Mazatlan , America/Mazatlan');
    timeZones.push('(UTC-07:00) Mountain Time (US & Canada) , US/Mountain');
    timeZones.push('(UTC-06:00) Central America , America/Managua');
    timeZones.push('(UTC-06:00) Central Time (US & Canada) , US/Central');
    timeZones.push('(UTC-06:00) Guadalajara , America/Mexico_City');
    timeZones.push('(UTC-06:00) Mexico City , America/Mexico_City');
    timeZones.push('(UTC-06:00) Monterrey , America/Monterrey');
    timeZones.push('(UTC-06:00) Saskatchewan , Canada/Saskatchewan');
    timeZones.push('(UTC-05:00) Bogota , America/Bogota');
    timeZones.push('(UTC-05:00) Eastern Time (US & Canada) , US/Eastern');
    timeZones.push('(UTC-05:00) Indiana (East) , US/East-Indiana');
    timeZones.push('(UTC-05:00) Lima , America/Lima');
    timeZones.push('(UTC-05:00) Quito , America/Bogota');
    timeZones.push('(UTC-04:00) Atlantic Time (Canada) , Canada/Atlantic');
    timeZones.push('(UTC-04:30) Caracas , America/Caracas');
    timeZones.push('(UTC-04:00) La Paz , America/La_Paz');
    timeZones.push('(UTC-04:00) Santiago , America/Santiago');
    timeZones.push('(UTC-03:30) Newfoundland , Canada/Newfoundland');
    timeZones.push('(UTC-03:00) Brasilia , America/Sao_Paulo');
    timeZones.push('(UTC-03:00) Buenos Aires , America/Argentina/Buenos_Aires');
    timeZones.push('(UTC-03:00) Georgetown , America/Argentina/Buenos_Aires');
    timeZones.push('(UTC-03:00) Greenland , America/Godthab');
    timeZones.push('(UTC-02:00) Mid-Atlantic , America/Noronha');
    timeZones.push('(UTC-01:00) Azores , Atlantic/Azores');
    timeZones.push('(UTC-01:00) Cape Verde Is. , Atlantic/Cape_Verde');
    timeZones.push('(UTC+00:00) Casablanca , Africa/Casablanca');
    timeZones.push('(UTC+00:00) Edinburgh , Europe/London');
    timeZones.push('(UTC+00:00) Greenwich Mean Time : Dublin , Etc/Greenwich');
    timeZones.push('(UTC+00:00) Lisbon , Europe/Lisbon');
    timeZones.push('(UTC+00:00) London , Europe/London');
    timeZones.push('(UTC+00:00) Monrovia , Africa/Monrovia');
    timeZones.push('(UTC+00:00) UTC , UTC');
    timeZones.push('(UTC+01:00) Amsterdam , Europe/Amsterdam');
    timeZones.push('(UTC+01:00) Belgrade , Europe/Belgrade');
    timeZones.push('(UTC+01:00) Berlin , Europe/Berlin');
    timeZones.push('(UTC+01:00) Bern , Europe/Berlin');
    timeZones.push('(UTC+01:00) Bratislava , Europe/Bratislava');
    timeZones.push('(UTC+01:00) Brussels , Europe/Brussels');
    timeZones.push('(UTC+01:00) Budapest , Europe/Budapest');
    timeZones.push('(UTC+01:00) Copenhagen , Europe/Copenhagen');
    timeZones.push('(UTC+01:00) Ljubljana , Europe/Ljubljana');
    timeZones.push('(UTC+01:00) Madrid , Europe/Madrid');
    timeZones.push('(UTC+01:00) Paris , Europe/Paris');
    timeZones.push('(UTC+01:00) Prague , Europe/Prague');
    timeZones.push('(UTC+01:00) Rome , Europe/Rome');
    timeZones.push('(UTC+01:00) Sarajevo , Europe/Sarajevo');
    timeZones.push('(UTC+01:00) Skopje , Europe/Skopje');
    timeZones.push('(UTC+01:00) Stockholm , Europe/Stockholm');
    timeZones.push('(UTC+01:00) Vienna , Europe/Vienna');
    timeZones.push('(UTC+01:00) Warsaw , Europe/Warsaw');
    timeZones.push('(UTC+01:00) West Central Africa , Africa/Lagos');
    timeZones.push('(UTC+01:00) Zagreb , Europe/Zagreb');
    timeZones.push('(UTC+02:00) Athens , Europe/Athens');
    timeZones.push('(UTC+02:00) Bucharest , Europe/Bucharest');
    timeZones.push('(UTC+02:00) Cairo , Africa/Cairo');
    timeZones.push('(UTC+02:00) Harare , Africa/Harare');
    timeZones.push('(UTC+02:00) Helsinki , Europe/Helsinki');
    timeZones.push('(UTC+02:00) Istanbul , Europe/Istanbul');
    timeZones.push('(UTC+02:00) Jerusalem , Asia/Jerusalem');
    timeZones.push('(UTC+02:00) Kyiv , Europe/Helsinki');
    timeZones.push('(UTC+02:00) Pretoria , Africa/Johannesburg');
    timeZones.push('(UTC+02:00) Riga , Europe/Riga');
    timeZones.push('(UTC+02:00) Sofia , Europe/Sofia');
    timeZones.push('(UTC+02:00) Tallinn , Europe/Tallinn');
    timeZones.push('(UTC+02:00) Vilnius , Europe/Vilnius');
    timeZones.push('(UTC+03:00) Baghdad , Asia/Baghdad');
    timeZones.push('(UTC+03:00) Kuwait , Asia/Kuwait');
    timeZones.push('(UTC+03:00) Minsk , Europe/Minsk');
    timeZones.push('(UTC+03:00) Nairobi , Africa/Nairobi');
    timeZones.push('(UTC+03:00) Riyadh , Asia/Riyadh');
    timeZones.push('(UTC+03:00) Volgograd , Europe/Volgograd');
    timeZones.push('(UTC+03:30) Tehran , Asia/Tehran');
    timeZones.push('(UTC+04:00) Abu Dhabi , Asia/Muscat');
    timeZones.push('(UTC+04:00) Baku , Asia/Baku');
    timeZones.push('(UTC+04:00) Moscow , Europe/Moscow');
    timeZones.push('(UTC+04:00) Muscat , Asia/Muscat');
    timeZones.push('(UTC+04:00) St. Petersburg , Europe/Moscow');
    timeZones.push('(UTC+04:00) Tbilisi , Asia/Tbilisi');
    timeZones.push('(UTC+04:00) Yerevan , Asia/Yerevan');
    timeZones.push('(UTC+04:30) Kabul , Asia/Kabul');
    timeZones.push('(UTC+05:00) Islamabad , Asia/Karachi');
    timeZones.push('(UTC+05:00) Karachi , Asia/Karachi');
    timeZones.push('(UTC+05:00) Tashkent , Asia/Tashkent');
    timeZones.push('(UTC+05:30) Chennai , Asia/Calcutta');
    timeZones.push('(UTC+05:30) Kolkata , Asia/Kolkata');
    timeZones.push('(UTC+05:30) Mumbai , Asia/Calcutta');
    timeZones.push('(UTC+05:30) New Delhi , Asia/Calcutta');
    timeZones.push('(UTC+05:30) Sri Jayawardenepura , Asia/Calcutta');
    timeZones.push('(UTC+05:45) Kathmandu , Asia/Katmandu');
    timeZones.push('(UTC+06:00) Almaty , Asia/Almaty');
    timeZones.push('(UTC+06:00) Astana , Asia/Dhaka');
    timeZones.push('(UTC+06:00) Dhaka , Asia/Dhaka');
    timeZones.push('(UTC+06:00) Ekaterinburg , Asia/Yekaterinburg');
    timeZones.push('(UTC+06:30) Rangoon , Asia/Rangoon');
    timeZones.push('(UTC+07:00) Bangkok , Asia/Bangkok');
    timeZones.push('(UTC+07:00) Hanoi , Asia/Bangkok');
    timeZones.push('(UTC+07:00) Jakarta , Asia/Jakarta');
    timeZones.push('(UTC+07:00) Novosibirsk , Asia/Novosibirsk');
    timeZones.push('(UTC+08:00) Beijing , Asia/Hong_Kong');
    timeZones.push('(UTC+08:00) Chongqing , Asia/Chongqing');
    timeZones.push('(UTC+08:00) Hong Kong , Asia/Hong_Kong');
    timeZones.push('(UTC+08:00) Krasnoyarsk , Asia/Krasnoyarsk');
    timeZones.push('(UTC+08:00) Kuala Lumpur , Asia/Kuala_Lumpur');
    timeZones.push('(UTC+08:00) Perth , Australia/Perth');
    timeZones.push('(UTC+08:00) Singapore , Asia/Singapore');
    timeZones.push('(UTC+08:00) Taipei , Asia/Taipei');
    timeZones.push('(UTC+08:00) Ulaan Bataar , Asia/Ulan_Bator');
    timeZones.push('(UTC+08:00) Urumqi , Asia/Urumqi');
    timeZones.push('(UTC+09:00) Irkutsk , Asia/Irkutsk');
    timeZones.push('(UTC+09:00) Osaka , Asia/Tokyo');
    timeZones.push('(UTC+09:00) Sapporo , Asia/Tokyo');
    timeZones.push('(UTC+09:00) Seoul , Asia/Seoul');
    timeZones.push('(UTC+09:00) Tokyo , Asia/Tokyo');
    timeZones.push('(UTC+09:30) Adelaide , Australia/Adelaide');
    timeZones.push('(UTC+09:30) Darwin , Australia/Darwin');
    timeZones.push('(UTC+10:00) Brisbane , Australia/Brisbane');
    timeZones.push('(UTC+10:00) Canberra , Australia/Canberra');
    timeZones.push('(UTC+10:00) Guam , Pacific/Guam');
    timeZones.push('(UTC+10:00) Hobart , Australia/Hobart');
    timeZones.push('(UTC+10:00) Melbourne , Australia/Melbourne');
    timeZones.push('(UTC+10:00) Port Moresby , Pacific/Port_Moresby');
    timeZones.push('(UTC+10:00) Sydney , Australia/Sydney');
    timeZones.push('(UTC+10:00) Yakutsk , Asia/Yakutsk');
    timeZones.push('(UTC+11:00) Vladivostok , Asia/Vladivostok');
    timeZones.push('(UTC+12:00) Auckland , Pacific/Auckland');
    timeZones.push('(UTC+12:00) Fiji , Pacific/Fiji');
    timeZones.push('(UTC+12:00) International Date Line West , Pacific/Kwajalein');
    timeZones.push('(UTC+12:00) Kamchatka , Asia/Kamchatka');
    timeZones.push('(UTC+12:00) Magadan , Asia/Magadan');
    timeZones.push('(UTC+12:00) Marshall Is. , Pacific/Fiji');
    timeZones.push('(UTC+12:00) New Caledonia , Asia/Magadan');
    timeZones.push('(UTC+12:00) Solomon Is. , Asia/Magadan');
    timeZones.push('(UTC+12:00) Wellington , Pacific/Auckland');
    timeZones.push('(UTC+13:00) Nuku\'alofa , Pacific/Tongatapu');
    return timeZones;
  }

  getDateFormat(){
    let dataFormats:Array<string> = [];
    dataFormats.push('M/d/yyyy');
    dataFormats.push('M/d/yy');
    dataFormats.push('MM/dd/yy');
    dataFormats.push('MM/dd/yyyy');
    dataFormats.push('yy/MM/dd');
    dataFormats.push('yyyy-MM-dd');
    dataFormats.push('dd-MMM-yy');
    // read references https://www.vsni.co.uk/products/genstat/htmlhelp/spread/DateFormats.htm
    return dataFormats;
  }

  getDateFormat2(){
    let dataFormats:Array<string> = [];
    dataFormats.push('yyyy-mm-dd');
    dataFormats.push('mm/dd/yyyy');
    return dataFormats;
  }

  getDayOfWeek(){
    let dayOfWeek:Array<string> = [];
    dayOfWeek.push('Monday');
    dayOfWeek.push('Tuesday');
    dayOfWeek.push('Wednesday');
    dayOfWeek.push('Thursday');
    dayOfWeek.push('Friday');
    dayOfWeek.push('Saturday');
    dayOfWeek.push('Sunday');
    return dayOfWeek;
  }

  getNumberDayOfWeek(date){
    let DateOfWeekMap = new Map<string,string>();
    DateOfWeekMap.set('Monday', '1');
    DateOfWeekMap.set('Tuesday', '2');
    DateOfWeekMap.set('Wednesday', '3');
    DateOfWeekMap.set('Thursday', '4');
    DateOfWeekMap.set('Friday', '5');
    DateOfWeekMap.set('Saturday', '6');
    DateOfWeekMap.set('Sunday', '7');

    return DateOfWeekMap.get(date);
  }

  getDayOfWeekFromNumber(numb){
    let DateOfWeekMap = new Map<string,string>();
    DateOfWeekMap.set('1', 'Monday');
    DateOfWeekMap.set('2', 'Tuesday');
    DateOfWeekMap.set('3', 'Wednesday');
    DateOfWeekMap.set('4', 'Thursday');
    DateOfWeekMap.set('5', 'Friday');
    DateOfWeekMap.set('6', 'Saturday');
    DateOfWeekMap.set('7', 'Sunday');

    return DateOfWeekMap.get(numb);
  }
}
