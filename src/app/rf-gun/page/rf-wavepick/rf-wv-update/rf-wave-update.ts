import {Component,OnInit} from "@angular/core";
import { Router,RouteParams, RouterLink, ROUTER_DIRECTIVES } from '@angular/router-deprecated';
import { FORM_DIRECTIVES,FormBuilder} from '@angular/common';
import {UserService, Functions} from '../../../../common/core/load';
import {WMSBreadcrumb,WMSPagination,PaginationControlsCmp,ColorPickerDirective,WMSMessages,ColorPickerService,PaginationService} from '../../../../common/directives/directives';
import {CustomerServices} from "../../../../system-setup/customer/customer-service";
import {RF_WavePickServices} from "../wavepick.service";
@Component({
  selector: 'rf-putaway-update',
  providers: [CustomerServices,RF_WavePickServices,PaginationService,ColorPickerService,UserService],
  directives: [FORM_DIRECTIVES,WMSPagination,PaginationControlsCmp,ColorPickerDirective,WMSMessages,WMSBreadcrumb],
  templateUrl: 'rf-wave-update.html',
})
export class RF_WavePickUpdateComponent {

  private allowAccess;
  private showLoadingOverlay;
  private messages;
  private rfGunPermission;
  private whs_id;
  private LocPalletModel={};
  private skuDetail={};
  private suggestLocModel;
  private suggestPalletLoc;
  private wvDetail={};
  private type='CT';

  constructor(private rfGunService:RF_WavePickServices,
              private _router: Router,
              private params: RouteParams,
              private _user:UserService,
              private _Func: Functions)
  {
    this.checkPermission();
  }


  checkPermission(){

    this.showLoadingOverlay = true;
    this._user.GetPermissionUser().subscribe(
      data => {
        this.showLoadingOverlay = false;
        this.rfGunPermission = this._user.RequestPermission(data, 'viewRFGUN');
        if(this.rfGunPermission) {
          this.allowAccess=true;
          this.whs_id = localStorage.getItem('whs_id');
          this.getDetailWavePick();
        }
        else{
          this.redirectDeny();
        }
      },
      err => {
        this.parseError(err);
        this.showLoadingOverlay = false;
      }
    );
  }

  redirectDeny(){
    this._router.parent.navigateByUrl('/deny');
  }

  removeEmptyKey(object)
  {
    let data_tmp=JSON.parse(JSON.stringify(object));
    Object.keys(object['codes']).map(
      key =>{
        if(object['codes'][key]==''){
          delete data_tmp['codes'][key];
        }
      }
    );
    return data_tmp
  }


  getDetailWavePick()
  {
    this.showLoadingOverlay=true;
    let params="/sku/"+this.params.get('id');
    this.rfGunService.RF_GUN_API_GET(this.whs_id,params)
      .subscribe(
        data => {
          this.showLoadingOverlay=false;
          this.wvDetail=data.data;
        },
        err => {
          this.showLoadingOverlay=false;
          if(err.json)
          {
            this.parseError(err);
          }
        },
        () => {
        }
      );

  }


  /*
   * updateLocation
   * */

  updateLocation(data):void
  {
    let data_r={};
    data_r['type']=this.type;
    data_r['codes']=(<any>Object).assign({},data.split("\n"));
    data_r=this.removeEmptyKey(data_r);
    this.showLoadingOverlay=true;
    let param='/sku/'+this.wvDetail['wv_dtl_id'];
    this.rfGunService.RF_GUN_API_UPDATE(this.whs_id,JSON.stringify(data_r),param)
      .subscribe(
        data => {
          this.messages=this._Func.Messages('success',data['message']);
          this.showLoadingOverlay=false;
          this.suggestPalletLoc=null;
          // reget
          this.getDetailWavePick();
        },
        err => {
          this.showLoadingOverlay=false;
          this.parseError(err);
        },
        () => {
        }
      );

  }

  getSuggetWavePick(clickType = 'click')
  {
    let params="/sku/"+this.params.get('id')+'/suggest?type='+this.type;
    this.rfGunService.RF_GUN_API_GET(this.whs_id,params)
      .subscribe(
        data => {
          if(!data.data.length)
          {
            this.messages=this._Func.Messages('danger','No Results Found.');
          }
          else{
            if(clickType == 'click'){
              let dataSuggest = [data.data[0]];
              this.suggestPalletLoc=dataSuggest.join("\n");
            }else{
              this.suggestPalletLoc=data.data.join("\n");
            }

          }
        },
        err => {
          if(err.json)
          {
            this.parseError(err);
          }
        },
        () => {
        }
      );

  }

  _type(types)
  {
    this.type=types;
  }

  // Show error when server die or else
  private parseError (err) {
    err = err.json();
    this.messages = this._Func.Messages('danger', err.errors.message);
  }

}
