import { Component, Input, Output, EventEmitter, Injectable, OnInit } from '@angular/core';
import {
    WMSPagination, PaginationControlsCmp, PaginationService, PaginatePipe
} from '../directives';
import { Functions } from '../../core/load';
import { Services } from './services';
declare const jQuery: any;
@Injectable()
@Component({
    templateUrl: 'template.html',
    styleUrls: ['./style.css'],
    selector: 'popup-assign-multi-pickers',
    directives: [WMSPagination, PaginationControlsCmp],
    pipes: [PaginatePipe],
    providers: [Services]
})

export class PopupAssignMultiPickers implements OnInit {
    public infoSearchUser = {};
    public oldDataCSRUser = [];
    public listUser = [];
    public _idDetail: any = '';
    public selectedAll = false;
    public selectedItems = [];
    public usersExisted = [];
    public valueOfType = {};
    @Input('type') set type(value) {
        if (value) {
            if (value === 'PICKER') {
                this.valueOfType = {
                    title: 'Pickers',
                    api: 'saveListPickers',
                    param: 'pickers',
                    type: 'PICKER'
                }
            } else if (value === 'CSR') {
                this.valueOfType = {
                    title: 'CSR',
                    api: 'saveListCSR',
                    param: 'data',
                    type: 'CSR'
                }
            } else if (value === 'MULTIPLE') {
                this.valueOfType = {
                    title: 'MULTIPLE',
                    api: 'saveListPickersforMuiltiWP',
                    param: 'pickers',
                    type: 'MULTIPLE'
                }                
            }
        }
    }
    @Input('idDetail') set idDetail(value) {
        if (value) {
            this._idDetail = value;
        }
    }
    @Input('listSelectedItem') set listSelectedItem(value) {
        if (value) {
            jQuery('#assign-multi').modal('show');
            this.infoSearchUser = {};
            this.listUser = value;
            this.oldDataCSRUser = value;
        }
    }
    @Input('listAssigned') set listAssigned(value) {
        if (value) {
            this.usersExisted = value;
        }
    }
    @Output() messages = new EventEmitter();
    @Output() isAssigned = new EventEmitter();
    public showLoadingOverlay = false;
    constructor(private _func: Functions,
        private service: Services) { }
    public checkSelectItem(item) {
        item['selected'] = !item['selected'];
        this.selectedAll = false;
        for (let i = 0, l = this.listUser.length; i < l; i++) {
            item['odr_id'] = parseInt(this._idDetail, 10);
            item['csr'] = parseInt(item['user_id'], 10);
            if (this.listUser[i]['selected']) {
                const found = this.selectedItems.findIndex(
                    (item) => item.user_id === this.listUser[i]['user_id']
                )
                if (found === -1)
                    this.selectedItems.push(this.listUser[i]);
            } else {
                this.selectedItems = this.selectedItems.filter(
                    (item) => item.user_id !== this.listUser[i].user_id
                )
            }
        }
    }

    ngOnInit() {
        if (this.usersExisted) {
            for(const i in this.usersExisted) {
                const foundIndex = this.oldDataCSRUser.findIndex(
                    (user) => user.username === this.usersExisted[i]['user_name']
                );
                this.usersExisted[i]['selected'] = true;
                this.usersExisted[i]['user_id'] = this.oldDataCSRUser[foundIndex]['user_id'];
            }
            this.selectedItems = this.selectedItems.concat(this.usersExisted);
        }
    }

    private getIdUser(array) {
        let IdList = []
        array.forEach(element => {
            IdList.push(JSON.parse(element.user_id));
        })
        return IdList;
    }

    private assignUser: any;
    public assign() {
        const data = this.valueOfType['type'] === 'CSR' ? this.usersExisted.concat(this.selectedItems) : this.getIdUser(this.selectedItems);

        this._saveCSR(data);
    }

    public _saveCSR(list) {
        const data = {
            [this.valueOfType['param']]: list
        }
        this.showLoadingOverlay = true;
        this.service[this.valueOfType['api']](this._idDetail, JSON.stringify(data)).subscribe(
            data => {
                jQuery('#assign-multi').modal('hide');
                this.showLoadingOverlay = false;
                this.isAssigned.emit(true);
                if (typeof data['data'] === 'string') {
                    this.showMessage('success', data['data']);
                }
            },
            err => {
                jQuery('#assign-multi').modal('hide');
                this.showMessage('danger', this._func.parseErrorMessageFromServer(err));
                this.showLoadingOverlay = false;
            }
        );
    }

    filterUser() {
        let data = this.oldDataCSRUser;
        if (this.infoSearchUser['first_name']) {
            data = this.oldDataCSRUser.filter(x => x.first_name.toLowerCase().includes(this.infoSearchUser['first_name'].toLowerCase()));
        }
        if (this.infoSearchUser['last_name']) {
            data = data.filter(x => x.last_name.toLowerCase().includes(this.infoSearchUser['last_name'].toLowerCase()));
        }
        if (this.infoSearchUser['email']) {
            data = data.filter(x => x.email.toLowerCase().includes(this.infoSearchUser['email'].toLowerCase()));
        }
        this.listUser = data;
    }

    resetFilterUser() {
        this.infoSearchUser['first_name'] = '';
        this.infoSearchUser['last_name'] = '';
        this.infoSearchUser['email'] = '';
        this.listUser = this.oldDataCSRUser;
    }

    private showMessage(msgStatus, msg) {
        this.messages.emit({
            'status': msgStatus,
            'txt': msg
        })
        jQuery(window).scrollTop(0);
    }
    private _resetSelectedItems() {
        this.selectedAll = false;
        this.selectedItems = [];
    }

    // public checkAll(event) {
    //     this.selectedAll = !this.selectedAll;
    //     this.listUser.forEach((item) => {
    //         item['selected'] = this.selectedAll;
    //         item['odr_id'] = parseInt(this._idDetail, 10);
    //         item['csr'] = parseInt(item['user_id'], 10);
    //         if (this.selectedAll) {
    //             this.selectedItems.push(item);
    //         }
    //     })
    // }

}