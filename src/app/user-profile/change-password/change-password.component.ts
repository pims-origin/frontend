import {Component} from "@angular/core";
import { Router, RouterLink, ROUTER_DIRECTIVES } from '@angular/router-deprecated';
import { CORE_DIRECTIVES, FORM_DIRECTIVES,ControlGroup,FormBuilder, Control ,Validators} from '@angular/common';
import {UserProfileServices} from "../user-profile.service";
import {StylesheetService} from "../../common/services/stylesheet.service";
import {ValidationService } from '../../common/core/validator';
import {Functions} from "../../common/core/functions";
import {matchingPasswords } from '../../common/core/validator';
import {WMSMessages} from "../../common/directives/messages/messages";
declare var jQuery: any;

@Component({
    selector    : 'change-password',
    directives  : [CORE_DIRECTIVES, FORM_DIRECTIVES, WMSMessages],
    providers   : [UserProfileServices,StylesheetService, ValidationService],
    templateUrl : 'change-password.html',

})
export class ChangePasswordComponent{
  private showError = false;
  private messages;

  private old_password = '';
  private password = '';
  private password_confirmation = '';
  private resetPasswordMsgs = {}
  constructor(
    private _router: Router,
    private _func: Functions,
    private _Valid: ValidationService,
    private fb: FormBuilder,
    private _profileService: UserProfileServices,
    private _styleService: StylesheetService
  ){
    this.resetPasswordMsgs['err'] = [];
  }

  showMessage(msgStatus, msg) {
    this.messages = {
        'status': msgStatus,
        'txt': msg
    }
    jQuery(window).scrollTop(0);
  }

  submitForm():void {
    this.showError = true;
    if(this.checkPasswordPolicy()) {
      let creds = {
        'old_password': this.old_password,
        'password': this.password,
        'password_confirmation': this.password_confirmation
      }
        let stringData = JSON.stringify(creds);
        this._styleService.addLoading();
        let ObjectData = new Map();
        ObjectData.set("changePassword","changePassword");
        this._profileService.updateProfileByUserId(stringData,ObjectData).subscribe(
          response => {
            if(response.data.success){
              this.showMessage('success', 'Your password has changed successfully.');
              setTimeout(() =>{
                this._router.parent.navigateByUrl('/profile');
              }, 600);
              this._styleService.removeLoadding();
            }
          },
          err => {
            this.showMessage('danger', this._func.parseErrorMessageFromServer(err));
            this._styleService.removeLoadding();
          },
          () => {}
        );
    }
  }

  cancel(){
    this._router.parent.navigateByUrl('/profile');
  }

  private checkPasswordPolicy(){
    this.resetPasswordMsgs['err'] = this._Valid.validatePasswordByPolicy(this.password,this.password_confirmation);
    if(this.old_password != '' && this.old_password.length < 8){
      this.resetPasswordMsgs['err'].push('Your old password must have at least 8 characters');
    }
    if(this.old_password == ''){
      this.resetPasswordMsgs['err'].push('Your old password cannot be empty');
    }
    if(this.resetPasswordMsgs['err'].length > 0){
      return false;
    }
    return true;
  }

  private removeMsgs(msg){
      let index = this.resetPasswordMsgs['err'].indexOf(msg);
      this.resetPasswordMsgs['err'].splice(index,1);
  }
}
