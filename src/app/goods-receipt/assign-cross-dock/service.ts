import {Component, Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {Functions, API_Config} from '../../common/core/load';

@Injectable()

export class AssignCrossDockServices {

    private headerGet = this._func.AuthHeader();
    private headerPostJson = this._func.AuthHeaderPostJson();

    constructor(
        private _func: Functions,
        private _api: API_Config,
        private _http: Http) {
    }

    // Print damage ctn
    public getCrossXdock (id:any) {

        return this._http.get(this._api.API_GOODS_RECEIPT_MASTER + "/goods-receipts/assign-xdoc/"+id , {headers: this.headerGet})
            .map(res => res.json())
    }

    // Print damage ctn
    public assginCrossXdock (id:any,data) {

        return this._http.put(this._api.API_GOODS_RECEIPT_MASTER + "/goods-receipts/assign-xdoc/"+id ,data, {headers: this.headerPostJson})
            .map(res => res.json())
    }

    // Print damage ctn
    public updateCrossXdockQty (data) {

        return this._http.put(this._api.API_GOODS_RECEIPT_MASTER + "/goods-receipts/update-xdoc" ,data, {headers: this.headerPostJson})
            .map(res => res.json())
    }

    // Print damage ctn
    public getXdocLocation (userId:any) {
        return this._http.get(this._api.API_GOODS_RECEIPT_MASTER + "/goods-receipts/xdoc-location/"+userId , {headers: this.headerGet})
            .map(res => res.json())
    }

    // Save damage ctn
    public saveDamageCarton ($params = '') {

        return this._http.post(this._api.API_GOODS_RECEIPT_MASTER, $params , {headers: this.headerPostJson});
        //.map(res => res.json())
    }



}
