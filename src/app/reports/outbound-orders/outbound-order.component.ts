
import {Component} from '@angular/core';
import {API_Config} from '../../common/core/API_Config';
import {BaseTable} from '../../common/core/classes_us/base-table';
import {TableFunctions} from '../../common/core/table-functions';
import {DateFunctions} from '../../common/core/functions/date-functions';
import {BaseService} from '../../common/core/classes_us/base-service';
import {CustomersService} from '../../master-data/customers/customers-service';
import {TableStructure} from '../../common/directives/directives';

@Component ({
    selector: 'receiving-container',
    directives: [
        TableStructure
    ],
    templateUrl: '../../common/directives/table-structure/table.component.html',
    providers: [
        BaseService,
        TableFunctions,
        DateFunctions,
        CustomersService
    ]
})

export class OutboundOrdComponent extends BaseTable
{
    public tableID = 'outbound-orders';
    public url = this._api.API_OUTBOUND_ORDER;
    public headerUrlSuffix = 'rep';
    public title = 'Outbound Orders';
    public permissionPage = 'viewReport';

    public headerDef = [
        {
            id: 'ver_table',
            value: 1 + Math.random() * 10000
        },
        this.columnList.enterby,
        this.columnList.checkoutby,
        this.columnList.customer,        
        this.columnList.warehouse,
        this.columnList.createdBy,
        this.columnList.customerPO,
        this.columnList.customerOrderNumber,
        this.columnList.orderNumber,
        this.columnList.eComm,
        this.columnList.orderStatus,
        this.columnList.conditions,
        this.columnList.totalCartons,
        this.columnList.totalPieces,
        this.columnList.shipByDate,
        this.columnList.cancelByDate,
        this.columnList.daysLeft,
        this.columnList.dtOdrReq,
        this.columnList.tmOdrReq,
        this.columnList.docoDt,
        this.columnList.dpcoDt,
        this.columnList.dcPerson,
        this.columnList.opcoDt,
        this.columnList.dsciDt,
        this.columnList.shippedDt,
        this.columnList.dtDiff,
        this.columnList.cusNotes,
        this.columnList.billingDt,
    ];

    public unsortableColumns = [
        'enterby',
        'checkoutby',
        'customer',
        'warehouse',
        'createdBy',
        'customerPO',
        'customerOrderNumber',
        'orderNumber',
        'eComm',
        'orderStatus',
        'conditions',
        'totalCartons',   
        'totalPieces',
        'shipByDate',
        'cancelByDate',
        'daysLeft',
        'dtOdrReq',
        'tmOdrReq',
        'docoDt',
        'dpcoDt',
        'dcPerson',
        'opcoDt',
        'dsciDt',
        'shippedDt',
        'dtDiff',
        'cusNotes',
        'billingDt',          
    ];

    public searches = [
        this.searchList.customer,
    ];

    /*
    ****************************************************************************
    */

    constructor (
        public _api: API_Config,
        public _tableFunc: TableFunctions,
        public _tableService: BaseService,
        public _customersService: CustomersService
    )
    {
        super();

        this._tableFunc.permissionCall(this);
    }

    //**************************************************************************

}