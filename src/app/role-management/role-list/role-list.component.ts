import {Component, OnInit} from "@angular/core";
import {Http, Headers} from "@angular/http";
import { Router, RouterLink, ROUTER_DIRECTIVES } from '@angular/router-deprecated';
import { CORE_DIRECTIVES, FORM_DIRECTIVES } from '@angular/common';
import {API_Config} from "../../common/core/API_Config";
import {AuthorizationService} from "../../common/services/authorization.service";
import {UserService} from "../../common/users/users.service";
import {Functions} from "../../common/core/functions";
import {RoleListMessages } from '../role-list/role-list-messages';
import {WMSBreadcrumb,AgGridExtent,WMSPagination,WMSMessages} from '../../common/directives/directives';
declare var jQuery: any;
declare var swal: any;

@Component({
    selector: 'role-list',
    directives: [CORE_DIRECTIVES, AgGridExtent,FORM_DIRECTIVES,WMSPagination, WMSMessages, WMSBreadcrumb],
    providers : [RoleListMessages],
    templateUrl: 'role-list.html',

})
export class RoleListComponent implements OnInit{

    private headerURL = this.apiService.API_User_Metas+'/rol';
    private allRoles: any = [];
    private isLoading = true;

    private Pagination;
    private totalCount = 0;
    private tmpPageCount;
    private pageCount=0;
    private ItemOnPage=0;
    private currentPage = 1;
    private perPage = 20;
    private numLinks = 3;

    private rowCount = "";

    private viewRole:boolean=false;
    private hasAddPermission = false;
    private hasEditPermission = false;

    private messages;
    private listMessage;
    private showLoadingOverlay = false;

    public ListSelectedItem:Array<any>=[];

  private columnData = {
    type:{
      attr:'checkbox',
      title:'#',
      width:25,
      pin:true,
      ver_table:'0022421'
    },
    code: {
      title:'Role Code',
      url:'#/roles/',
      field_get_id:'name',
      width:180,
      id:true,
      pin:true,
      sort:true
    },
    name: {
      title:'Role Name',
      width:160,
      pin:true,
      sort:true
    },
    description: {
      title:'Description',
      width:190
    },
  };

    constructor (private  apiService:API_Config,private _http: Http, private _router:Router, private _user: UserService, private _func: Functions, private _roleListMessage: RoleListMessages, private _API_Config: API_Config) {
    }

    ngOnInit() {
      this.listMessage = this._roleListMessage.messages;
      this.ck_permisson();
    }

    private ck_permisson() {
        this.showLoadingOverlay = true;
        this._user.GetPermissionUser().subscribe(
            data => {
                this.viewRole = this._user.RequestPermission(data,'viewRole');
                this.hasAddPermission = this._user.RequestPermission(data,'createRole');
                this.hasEditPermission = this._user.RequestPermission(data,'editRole');
                if (!this.viewRole) {
                    this._router.parent.navigateByUrl('/deny');
                }
                else {
                    this.getRoleList();
                }
            },
            err => {
              this.showMessage('danger', this._func.parseErrorMessageFromServer(err));
              this.showLoadingOverlay = false;
            }
        );
    }

    private checkCheckAll($event) {
      var checkbox = jQuery($event.target),
          val = checkbox.val(),
          table = checkbox.closest('table'),
          checkboxAll = table.find('thead :checkbox');
      table.find('tbody :checkbox:not(:checked)').length ? checkboxAll.prop('checked', false) : checkboxAll.prop('checked', true);
    }

    private removeCheckAll() {
      jQuery('#role-list').find('thead :checkbox').prop('checked', false);
    }

    private toggleCheckAll($event) {
      var that = this,
          checkboxAll = jQuery($event.target),
          checkAllVal = checkboxAll.prop('checked'),
          table = checkboxAll.closest('table');

      if(checkboxAll.prop('checked')) {
        table.find('tbody :checkbox').prop('checked', true);
      }
      else {
        table.find('tbody :checkbox').prop('checked', false);
      }
    }

    private showMessage(msgStatus, msg) {
        this.messages = {
            'status': msgStatus,
            'txt': msg
        }
        jQuery(window).scrollTop(0);
    }

    // Get list Role set default limit = 10
    private getRoleList (page = null) {
        if(!page) page = 1;
        let params="?page="+page+"&limit="+this.perPage;
        let params_arr = jQuery("#form-filter").serializeArray() ;
        for (let i in params_arr) {
            if (params_arr[i].value == "") continue;
            params +="&" +  params_arr[i].name.trim() + "=" + encodeURIComponent(params_arr[i].value.trim());
        }
        if(this.sortQuery != '') {
            params += '&'+this.sortQuery;
        }
        var headers = new Headers();
        headers.append("Authorization", 'Bearer '+localStorage.getItem('jwt'));
        // this.allRoles = [];
        this.removeCheckAll();
        this.showLoadingOverlay = true;
        this.isLoading = true;

        this._http.get(this._API_Config.API_Roles + params, {headers: headers})
            .map(res => res.json())
            .subscribe(
                data => {
                  if(data.data) {
                    this.allRoles = data.data;
                    this.initPagination(data);
                    this.removeCheckAll();
                  }
                  else {
                    this.allRoles = [];
                    this.Pagination = null;
                  }
                  this.showLoadingOverlay = false;
                  this.isLoading = false;
                },
                err => {
                  this.showMessage('danger', this._func.parseErrorMessageFromServer(err));
                  this.showLoadingOverlay = false;
                  this.isLoading = false;
                }
            );
    }

    // Get fieds user input or change in form
    private filterRole(currentPage, perPage) {

        var params = "?page="+currentPage+"&limit="+perPage;
        this.getRoleList(params);
    }

    // Event when user chage page size
    private onPageSizeChanged ($event) {
      this.perPage = $event.target.value;
      if((this.Pagination['current_page'] - 1) * this.perPage >= this.Pagination['total']) {
          this.currentPage = 1;
      }
      else {
          this.currentPage = this.Pagination['current_page'];
      }
      this.getRoleList(this.currentPage);
    }

    // edit Role
    private editRole() {

      if (this.ListSelectedItem.length > 1) {
        this.messages = this._func.Messages('danger','Please choose only one item to edit');
      }
      else {
        if(this.ListSelectedItem.length)
        {
          this._router.navigateByUrl('/roles/' + this.ListSelectedItem[0]['name'] + '/edit');
        }
        else{
          this.messages = this._func.Messages('danger','Please choose one item to edit');
        }
      }

    }

    // View detail role
    private viewDetail (role_name) {
        this._router.parent.navigateByUrl('/roles/'+role_name);
    }

    // View create role
    private createDetail () {
        this._router.parent.navigateByUrl('/roles/new');
    }

    public delete() {
      let that = this;
      let listAllItem = jQuery('#role-list tbody :checkbox');
      let listItemChecked = listAllItem.filter(':checked');

      if (listItemChecked.length < 1) {
        this.showMessage('danger', this.listMessage['remove_none']);
        return false;
      }

      swal({
          title: this.listMessage['remove_confirm'],
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: this.listMessage['agree'],
          cancelButtonText: this.listMessage['cancel'],
          confirmButtonClass: 'btn btn-success',
          cancelButtonClass: 'btn btn-danger',
          buttonsStyling: false
      }).then((agree) => {
          if (agree) {
            var deleteRoles = [];
            listAllItem.each(function(i) {
              if (jQuery(this).prop('checked')) {
                deleteRoles.push(that.allRoles[i].name);
              }
            });
            that.showLoadingOverlay = true;

            that._http.post(this._API_Config.API_Roles + '/delete', JSON.stringify({
              roles: deleteRoles
            }), { headers: that._func.AuthHeaderPostJson() })
            .map((res) => res.json().data).subscribe(
                data => {
                  that.getRoleList(that.currentPage);
                },
                err => {
                  this.showMessage('danger', this._func.parseErrorMessageFromServer(err));
                  that.showLoadingOverlay = false;
                }
            );
          }
      });
    }

    // Set params for pagination
    private initPagination(data){
      let meta = data['meta'];
      if(meta['pagination']) {
        this.Pagination=meta['pagination'];
      }
      else {
        let perPage = meta['perPage'],
            curPage = meta['currentPage'],
            total = meta['totalCount'];
        let count = perPage * curPage > total ? perPage * curPage - total : perPage;
        this.Pagination = {
          "total": total,
          "count": count,
          "per_page": perPage,
          "current_page": curPage,
          "total_pages": meta['pageCount'],
          "links": {
            "next": data['link'] && data['link']['next'] ? data['link']['next'] : ''
          }
        }
      }
      this.Pagination['numLinks']=3;
      this.Pagination['tmpPageCount']=this._func.Pagination(this.Pagination);
    }

    private filterList(pageNumber) {
      this.getRoleList(pageNumber);
    }

    private getPage(pageNumber) {
      let arr=new Array(pageNumber);
      return arr;
    }

    private search() {
        this.getRoleList(1);
    }

    private reset() {
        jQuery("#form-filter input, #form-filter select").each(function() {
            jQuery(this).val("");
        });
        this.getRoleList(1);
    }

    private FieldsToSortFlag = {
        code: false,
        name: false,
    };
    private countClick = {
        code: 3,
        name: 3,
    };
    private sortQuery = 'sort[code]=asc';
    sort(field) {
        if(this.countClick[field] == 3) {
            this.countClick[field] = 0;
        }

        this.countClick[field] ++;
        if(this.countClick[field] == 3) {
            this.sortQuery = '';
        }
        else {
            this.FieldsToSortFlag[field] = !this.FieldsToSortFlag[field];

            this.sortQuery = 'sort['+field+']';
            if(this.FieldsToSortFlag[field]) {
                this.sortQuery += '=asc';
            } else {
                this.sortQuery += '=desc';
            }
        }

        if(field) {
            for (let sortField in this.FieldsToSortFlag ) {
                if(sortField != field) {
                    this.FieldsToSortFlag[sortField] = false;
                    this.countClick[sortField] = 3;
                }
            }
        }
        this.getRoleList(this.currentPage);
    }
}
