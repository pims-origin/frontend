import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {Functions, API_Config} from '../common/core/load';

@Injectable()

export class DashboardServices {
    private headerGet = this.funcs.AuthHeader();

    constructor(private funcs:Functions, private apiConfig:API_Config, private http:Http) {
    }

    public getWavepicks() {
        return this.http.get(`${this.apiConfig.API_DashBoard}/wave-pick`, {headers: this.headerGet})
            .map(res => res.json());
    }

    public getOccupancyPCT(params:string = '') {
        return this.http.get(`${this.apiConfig.API_DashBoard}/occupancy-pct${params}`, {headers: this.headerGet})
            .map(res => res.json());
    }

    getOccupancyPCTbyCus(cusId: string) {
        return this.http.get(`${this.apiConfig.API_DashBoard}/occupancy-pct-by-cus?cus_id=${cusId}`, { headers: this.headerGet })
            .map(res => res.json());
    }

    public getOccupancyPCTStatus(params:string = '') {
        return this.http.get(`${this.apiConfig.API_DashBoard}/occupancy-pct-status${params}`, {headers: this.headerGet})
            .map(res => res.json());
    }

    getStorageAgingDataByCustomer(cusId: string = '') {
        return this.http.get(`${this.apiConfig.API_REPORTS_CORE}/storage-aging?cus_id=${cusId}`, { headers: this.headerGet })
            .map(res => res.json());
    }

    getStorageAgingDataBySKU(sku: string = '') {
        return this.http.get(`${this.apiConfig.API_REPORTS_CORE}/storage-aging?sku=${sku}`, { headers: this.headerGet })
            .map(res => res.json());
    }

    public getLocationTypes() {
        return this.http.get(this.apiConfig.API_Location_type, {headers: this.headerGet})
            .map(res => res.json());
    }

    public getCustomers(whsId: string = '') {
        return this.http.get(`${this.apiConfig.API_CUSTOMER_MASTER}/customer-warehouses?whs_id=${whsId}&limit=10000&sort[cus_name]=asc`, { headers: this.headerGet })
            .map(res => res.json());
    }

    public getZoneCapacities(cusId:string = '') {
        return this.http.get(`${this.apiConfig.API_DashBoard}/zoneCapacity-admin?cus_id=${cusId}`, {headers: this.headerGet})
            .map(res => res.json());
    }

    public getRoomAdmin() {
        return this.http.get(`${this.apiConfig.API_DashBoard}/room-admin`, {headers: this.headerGet})
            .map(res => res.json());
    }

    public getHisCapacities(whsId:string, params:string) {
        return this.http.get(`${this.apiConfig.API_REPORTS_CORE}/${whsId}/location-history${params}`, {headers: this.headerGet})
            .map(res => res.json())
    }

    public getAlertBarInfo() {
		return this.http.get(`${this.apiConfig.API_DashBoard}/alert-bar`, {headers: this.headerGet})
			.map(res => res.json());
    }

    public getCycleCount(type:string = '') {
        return this.http.get(`${this.apiConfig.API_WAREHOUSE_CORE}/dashboard/cycle-count?type=${type}`, {headers: this.headerGet})
            .map(res => res.json());
    }

    public getCycleCountType() {
        return this.http.get(this.apiConfig.API_Cycle_Type, {headers: this.headerGet})
            .map(res => res.json());
    }
    
    public getSKUsSearch(whs_id, params) {
        return this.http.get(`${this.apiConfig.API_Cycle_Count_Root}/ccn/${whs_id}/auto-sku` + params, { headers: this.headerGet })
            .map(res => res.json());
    }
}
