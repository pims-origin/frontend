import {Component } from '@angular/core';
import { FORM_DIRECTIVES,ControlGroup,FormBuilder, Control ,ControlArray,Validators} from '@angular/common';
import {RouteParams,Router ,RouteData} from '@angular/router-deprecated';
import {UserService, Functions} from '../../common/core/load';
import {RevertServices} from './revert-order.service';
import { ValidationService } from '../../common/core/validator';
import {WMSBreadcrumb,PaginatePipe,WMSMessages, PaginationControlsCmp, PaginationService} from '../../common/directives/directives';
import { Http } from '@angular/http';
import {GoBackComponent} from "../../common/component/goBack.component";
import {BoxPopupService} from "../../common/popup/box-popup.service";
import {WarningBoxPopup} from "../../common/popup/warning-box-popup";

@Component({
    selector: 'revert-order',
    providers: [RevertServices,PaginationService,ValidationService,FormBuilder, BoxPopupService,UserService],
    directives: [FORM_DIRECTIVES,PaginationControlsCmp,WMSMessages,GoBackComponent,WMSBreadcrumb],
    pipes:[PaginatePipe],
    templateUrl: 'revert-order.component.html',
})

export class RevertOrderComponent {

    public Loading = [];
    private messages;
    private Action;
    private TitlePage='Revert Order';
    private IsView:boolean=false;
    private perPage=20;
    ODForm: ControlGroup;
    private showLoadingOverlay:boolean=false;
    private Country=[];
    private whs_id;
    private orderType=[];
    private orderData;
    private orderId;
    private orderCannotAllocate = false;
    private allItem;

    constructor(
        private _http: Http,
        private _Func: Functions,
        private params: RouteParams,
        private fb: FormBuilder,
        private _Valid: ValidationService,
        private _boxPopupService: BoxPopupService,
        private ALCService:RevertServices,
        _RTAction: RouteData,
        private _user:UserService,
        private _router: Router) {


        this.Action=_RTAction.get('action');

        this.checkPermission();

    }

    // Check permission for user using this function page
    private allocateOrder;
    private allowAccess:boolean=false;
    checkPermission(){

        this.showLoadingOverlay = true;
        this._user.GetPermissionUser().subscribe(
            data => {
                this.showLoadingOverlay = false;
                this.allocateOrder = this._user.RequestPermission(data, 'allocateOrder');

                if(this.allocateOrder) {
                    this.allowAccess=true;
                    this.orderDetail();
                }
                else{
                    this.redirectDeny();
                }

            },
            err => {
                this.parseError(err);
                this.showLoadingOverlay = false;
            }
        );
    }

    redirectDeny(){
        this._router.parent.navigateByUrl('/deny');
    }

    Revert(){

        this.showLoadingOverlay = true;

        this.ALCService.revertOrder(this.orderId).subscribe(
            data => {

                this.showLoadingOverlay = false;
                //Reset Form
                this.messages = this._Func.Messages('success', this._Func.msg('ODR001'));
                setTimeout(()=>{
                    this._router.parent.navigateByUrl('/outbound/order');
                }, 600);

            },
            err => {
                this.showLoadingOverlay = false;
                if(err.json().errors.message)
                {
                    this.messages = this._Func.Messages('danger',err.json().errors.message);
                }
            },
            () => {}
        );

    }

    orderDetail(){

        this.orderId=this.params.get('id');
        this.ALCService.getOrderDetail(this.orderId).subscribe(
            data => {
                this.orderData=data;
                this.orderData['totalSku']=this.orderData['items'] ? this.orderData['items'].length : 0;
                this.allItem = this.orderData.items ? this.orderData.items : [];
            },
            err => {
                this.parseError(err);
            },
            () => {}
        );

    }

    // Show error when server die or else
    private parseError (err) {
        this.messages = {'status' : 'danger', 'txt' : this._Func.parseErrorMessageFromServer(err)};
    }

    redirectToList(){
        let n = this;
        this._boxPopupService.showWarningPopup()
            .then(function (dm) {
                if(dm)
                    n._router.parent.navigateByUrl('/outbound/order');
            })
    }

}
