$(window).click(function(event) {
    const targetEle = event.target;
    const inputtingData = localStorage.getItem('inputting');
    const data = targetEle.dataset; // {toggle: 'tab'}
    const isInternal = targetEle.className.search('internal') !== -1;
    // console.log(targetEle.nodeName, data, isInternal, inputtingData);

    if (targetEle.nodeName.toLowerCase() === 'a' && inputtingData === '1' && !data['toggle'] && !isInternal) {
        const confirmMessage = confirm("Are you sure to leave this page?");
        if (!confirmMessage) {
            event.stopPropagation();
            event.preventDefault();
        }
    }
});
// remove inputting action when close tab or browser
window.addEventListener('beforeunload', function(event) {
    localStorage.removeItem('inputting');
    localStorage.removeItem('local_cus_id');
});

// disbled Ctrl + P
$(document).bind('keyup keydown', function(e) {
    if(e.ctrlKey && e.keyCode == 80) {
        e.preventDefault();
    }
});