
import {Component} from '@angular/core';
import {API_Config} from '../../../common/core/API_Config';
import {ModalForm} from '../../../common/core/classes_us/modal-form';
import {WMSMessages} from '../../../common/directives/directives';
import {TableFunctions} from '../../../common/core/table-functions';
import {PopupServices} from '../../../common/directives/popup-modal_us/add/add-service';
import {BaseService} from '../../../common/core/classes_us/base-service';
import {OrderBy} from '../../../common/pipes/order-by.pipe';

@Component({
    selector: 'add-contact',
    directives: [
        WMSMessages
    ],
    providers: [
        TableFunctions,
        PopupServices
    ],
    pipes: [OrderBy],
    templateUrl: '../../../common/directives/popup-modal_us/add/add.html'
})

export class AddContact extends ModalForm
{
    public cus_id: number;

    public data = {
        id: 'add-contact',
        title: 'Add Contact',
        class_name: 'add-contact-modal-form',
        caption_class: 'add-contact-caption',
        value_class: 'add-contact-value',
        items: [
            {
                caption: 'Department',
                name: 'cus_ctt_department',
                class_name: 'add-contact-item form-control margin-bottom5',
                value: '',
                mandatory: false
            },
            {
                caption: 'First Name',
                name: 'cus_ctt_fname',
                class_name: 'add-contact-item form-control margin-bottom5',
                value: '',
                mandatory: true
            },
            {
                caption: 'Last Name',
                name: 'cus_ctt_lname',
                class_name: 'add-contact-item form-control margin-bottom5',
                value: '',
                mandatory: true
            },
            {
                caption: 'Position',
                name: 'cus_ctt_position',
                class_name: 'add-contact-item form-control margin-bottom5',
                value: '',
                mandatory: false
            },
            {
                caption: 'Email',
                name: 'cus_ctt_email',
                class_name: 'add-contact-item form-control margin-bottom5',
                value: '',
                mandatory: true
            },
            {
                caption: 'Phone',
                name: 'cus_ctt_phone',
                class_name: 'add-contact-item form-control margin-bottom5',
                value: '',
                mandatory: false
            },
            {
                caption: 'Extension',
                name: 'cus_ctt_ext',
                class_name: 'add-contact-item form-control margin-bottom5',
                value: '',
                mandatory: false
            },
            {
                caption: 'Mobile',
                name: 'cus_ctt_mobile',
                class_name: 'add-contact-item form-control margin-bottom5',
                value: '',
                mandatory: false
            }
        ]
    };

    //**************************************************************************

    constructor(
        protected _api: API_Config,
        private _tableFunc: TableFunctions,
        private _popupServices: PopupServices,
        private _baseService: BaseService
    )
    {
        super();
    }

    //**************************************************************************

    public showInfo(tableComponent: any)
    {
        this.parentComponent = tableComponent;
        this.cus_id = tableComponent.customSearches.cus_id;
        this.messages = '';
        this.isVisible = true;
    }

    //**************************************************************************

    public submitInfo()
    {
        let params = this._popupServices.checkValues(this);

        if (params) {

            let url = this._api.API_CUSTOMER_CONTACTS + '/addContact/' + this.cus_id,
                data = JSON.stringify(params);

            this.showLoadingOverlay = true;

            this._baseService.post(url, data)
                .subscribe(
                    data => {

                        this._tableFunc.permissionCall(this.parentComponent);

                        this.isVisible = false;
                    },
                    err => {
                        this.messages = this._tableFunc.parseError(err);
                    },
                    () => {
                        this.showLoadingOverlay = false;
                    }
            );
        }
    }

    //**************************************************************************

}
