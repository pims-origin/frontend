
import {Component, ViewChild} from '@angular/core';
import {API_Config} from '../../common/core/API_Config';
import {BaseTable} from '../../common/core/classes_us/base-table';
import {TableFunctions} from '../../common/core/table-functions';
import {TableStructure} from '../../common/directives/directives';
import {BaseService} from '../../common/core/classes_us/base-service';
import {InvoiceListCustomersServices} from './invoice-list-customers-service';
import {ReceivePayment} from './receive-payment/receive-payment.component';

declare var jQuery: any;

@Component({
    selector: 'invoice-list-customers',
    directives: [
        TableStructure,
        ReceivePayment
    ],
    providers: [
        TableFunctions,
        BaseService,
        InvoiceListCustomersServices
    ],
    templateUrl: 'invoice-list-customers.component.html'
})

export class InvoiceListCustomersComponent extends BaseTable
{
    @ViewChild(ReceivePayment) receivePayment: ReceivePayment;

    public tableID = 'invoice-list-customer';
    public headerURL = this._api.API_User_Metas + '/rep';

    public customSearches;
    public paymentDate;

    public headerDef = [
        {
            id: 'ver_table',
            value: 1 + Math.random() * 10000
        },
        {
            id: 'inv_radio',
            name: '',
            width: 20,
            unsortable: true
        },
        {
            id: 'inv_sts',
            name: 'INV STS',
            width: 80
        },
        {
            id: 'cus_name',
            name: 'CUSTOMER',
            width: 180
        },
        {
            id: 'inv_num',
            name: 'INV NBR',
            width: 120,
            unsortable: true
        },
        {
            id: 'cncl',
            name: 'CNCL',
            width: 80,
            unsortable: true
        },
        {
            id: 'inv_dt',
            name: 'INV DT',
            width: 90,
            unsortable: true
        },
        {
            id: 'cncl_inv_num',
            name: 'CNCL INV NBR',
            width: 120,
            unsortable: true
        },
        {
            id: 'cur_code',
            name: 'CUR',
            width: 50,
            unsortable: true
        },
        {
            id: 'amt',
            name: 'AMT',
            width: 100
        },
        {
            id: 'pmnt_rcv_dt',
            name: 'PMNT RCV DT',
            width: 148
        },
        {
            id: 'check_num',
            name: 'PMNT REF NBR',
            width: 120
        },
        {
            id: 'invoice_number',
            name: '',
            width: 0,
            hide: true
        },
        {
            id: 'cus_id',
            name: '',
            width: 0,
            hide: true
        }
    ];

    public skipPageSize = true;
    public skipExport = true;

    public permissionPage = 'viewInvoice';

    constructor (
        public _api: API_Config,
        public _tableFunc: TableFunctions,
        public _baseService: BaseService,
        public _tableService: InvoiceListCustomersServices
    )
    {
        super();

        this.tableComponent = this;

        setTimeout(() => {
            jQuery('#invoice-list-customer').on(
                'click', '.clear-payment-button', this, this.cancelPayment
            );
        }, 0);
    }

    //**************************************************************************

    private cancelPayment(param)
    {
        let component = param.data,
            inv_num = jQuery(this).attr('data-inv-num'),
            rowNo = parseInt(jQuery(this).attr('data-row-no')) + 1;

        let url = component._api.API_Invioce_Receive_Payment + '/' + inv_num;

        component.showLoadingOverlay = true;

        component._baseService.post(url)
            .subscribe(
                data => {

                    let $table = jQuery('#invoice-list-customer'),
                        cncl_html = component.getCellHtml({
                            cell: 'cncl'
                        }),
                        pmnt_rcv_dt_html = component.getCellHtml({
                            cell: 'pmnt_rcv_dt'
                        });

                    component.rowData[rowNo - 1].inv_sts = 'Invoiced';

                    jQuery('[colid="inv_sts"]', $table).eq(rowNo)
                            .html('Invoiced');
                    jQuery('[colid="cncl"]', $table).eq(rowNo).html(cncl_html);
                    jQuery('[colid="pmnt_rcv_dt"]', $table).eq(rowNo)
                            .html(pmnt_rcv_dt_html);
                    jQuery('[colid="check_num"]', $table).eq(rowNo).html('');
                },
                err => {},
                () => {
                    component.showLoadingOverlay = false;
                }
        );
    }

    //**************************************************************************

    public gridUpdate(customSearches)
    {
        this.customSearches = customSearches;

        this.searchParams = this._tableFunc.getSearchParams(customSearches);

        this._tableFunc.getTableData({
            tableComponent: this
        });
    }

    //**************************************************************************

    public createRowData(data)
    {
        for (let key in data) {

            data[key].inv_radio = data[key].inv_sts == 'Open' ? `
                    <input type="radio" name="openCust" class="selectInvoice"
                           value="` + data[key].cus_id + `">
                ` : '';

            if (data[key].inv_sts != 'Open') {
                data[key].inv_num = this.getCellHtml({
                    cell: 'inv_num',
                    inv_num: data[key].inv_num,
                    inv_typ: data[key].inv_sts
                });
            }

            switch (data[key].inv_sts) {
                case 'Invoiced':

                    data[key].cncl = this.getCellHtml({
                        cell: 'cncl'
                    });

                    data[key].pmnt_rcv_dt = this.getCellHtml({
                        cell: 'pmnt_rcv_dt'
                    });

                    break;

                case 'Paid':

                    data[key].pmnt_rcv_dt = this.getCellHtml({
                        cell: 'cncl_pmnt',
                        inv_num: data[key].invoice_number,
                        pmnt_rcv_dt: data[key].pmnt_rcv_dt,
                        key: key
                    });

                    break;

                case 'Cancelled':
                    break;
                default:
                    break;
            }
        }

        return data;
    }

    //**************************************************************************

    public onCellClicked($event)
    {
        let cellClicked = $event.colDef.field,
            index = $event.rowIndex;

        if (! ~jQuery.inArray(cellClicked, ['inv_num', 'cncl', 'pmnt_rcv_dt'])) {
            return;
        }

        let inv_num = this.rowData[index].invoice_number;

        if (this.rowData[index].inv_sts == 'Invoiced') {

            switch (cellClicked) {
                case 'pmnt_rcv_dt':
                    this.receivePayment.showInfo(this, inv_num, index);
                    break;
                case 'cncl':

                    let url = this._api.API_Invoice_Cancel + '/' + inv_num;

                    this.showLoadingOverlay = true;

                    this._baseService.post(url)
                        .subscribe(
                            data => {
                                this.gridUpdate(this.customSearches);
                            },
                            err => {
                                this.messages = this._tableFunc.parseError(err);
                            },
                            () => {
                                this.showLoadingOverlay = false;
                            }
                    );

                    break;

                default:
                    break;
            }
        }
    }

    //**************************************************************************

    public onPaymentNotify($event)
    {
        let $table = jQuery('#invoice-list-customer'),
            rowNo = $event.index + 1;

        this.rowData[$event.index].inv_sts = 'Paid'

        let pmnt_rcv_dt_html = this.getCellHtml({
                cell: 'cncl_pmnt',
                inv_num: this.rowData[$event.index].invoice_number,
                pmnt_rcv_dt: $event.pmnt_rcv_dt,
                key: $event.index
            });

        jQuery('[colid="inv_sts"]', $table).eq(rowNo).html('Paid');
        jQuery('[colid="cncl"]', $table).eq(rowNo).html('');
        jQuery('[colid="pmnt_rcv_dt"]', $table).eq(rowNo)
                .html(pmnt_rcv_dt_html);
        jQuery('[colid="check_num"]', $table).eq(rowNo)
                .html($event.check_num);
    }

    //**************************************************************************

    private getCellHtml(data)
    {
        let inv_num = data.inv_num;

        switch (data.cell) {
            case 'inv_num':

                let inv_typ = data.inv_typ == 'Cancelled' ? 'C' : 'O';

                return `
                    <a href="/#/invoice/invoice-issue/display/` + inv_num + `"
                       target="_blank">` + inv_num + ` - ` + inv_typ + `</a>
                `;
            case 'cncl':
                return `
                    <button class="btn btn-outline red cancel-button">
                        <i class="icon-trash"></i>Cancel
                    </button>
                `;
            case 'pmnt_rcv_dt':
                return `
                    <button class="btn btn-outline blue payment-button"">
                        <i class="icon-note"></i>Receive Payment
                    </button>
                `;
            case 'cncl_pmnt':
                return `
                    <div class="cell-text pmnt_rcv-dt-cell-text">
                        ` + data.pmnt_rcv_dt + `
                    </div>
                    <button class="btn btn-outline red clear-payment-button"
                            data-row-no="` + data.key + `"
                            data-inv-num="` + inv_num + `">
                        <i class="icon-trash"></i>Clear
                    </button>
                `;
            default:
                return null;
        }
    }

    //**************************************************************************

}
