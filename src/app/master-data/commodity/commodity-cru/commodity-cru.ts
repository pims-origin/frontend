import {Component} from '@angular/core';
import { FORM_DIRECTIVES,ControlGroup,FormBuilder, Control,Validators} from '@angular/common';
import {WMSBreadcrumb,DocumentUpload,WMSMessages} from '../../../common/directives/directives';
import {Router, RouteData, RouteParams } from '@angular/router-deprecated';
import {BoxPopupService} from "../../../common/popup/box-popup.service";
import {CommodityServices} from "../commodity-service";
import {GoBackComponent} from "../../../common/component/goBack.component";
import {WarningBoxPopup} from "../../../common/popup/warning-box-popup";
import { ValidationService } from '../../../common/core/validator';
import {UserService, Functions} from '../../../common/core/load';
declare var jQuery: any;
@Component ({
    selector: 'commodity-cru',
    directives:[WMSBreadcrumb,GoBackComponent,DocumentUpload,FORM_DIRECTIVES,WMSMessages],
    providers: [BoxPopupService,CommodityServices,ValidationService,UserService],
    templateUrl: 'commodity-cru.html'
})

export class CommodityCRU {

    CommodityForm: ControlGroup;
    private action = 'new';
    private messages;
    private showLoadingOverlay=false;
    private TitlePage="Add Commodity";
    private IsView=false;
    private Action;
    private whs_id;
    private cmd_id;
    private submitForm=false;
    private cus_id = '';
    private loading={};
    private CommodityDeatail;

    constructor(private _router: Router,
                private _boxPopupService:BoxPopupService,
                _RTAction: RouteData,
                private _user:UserService,
                private fb: FormBuilder,
                private _Valid: ValidationService,
                private _Func: Functions,
                private _cmdSerivce:CommodityServices,
                private params: RouteParams) {

        this.Action=_RTAction.get('action');
        this.whs_id=this._Func.lstGetItem('whs_id');
        this.checkPermission();

    }


    // Check permission for user using this function page
    private createCommodity;
    private viewCommodity;
    private editCommodity;
    private allowAccess:boolean=false;
    private checkPermission() {
        this.showLoadingOverlay = true;
        this._user.GetPermissionUser().subscribe(
            data => {


                this.showLoadingOverlay = false;
                this.createCommodity = this._user.RequestPermission(data, 'createCommodity');
                this.viewCommodity = this._user.RequestPermission(data,'viewCommodity');
                this.editCommodity=this._user.RequestPermission(data,'editCommodity');

                if(this.Action=='view')
                {
                    if(this.viewCommodity) {
                        this.IsView = true;
                        this.TitlePage = 'View Commodity';
                        this.allowAccess=true;
                    }
                    else{
                        this.redirectDeny();
                    }
                }

                // Action new
                if(this.Action=='new')
                {
                    if(this.createCommodity) {
                        this.allowAccess=true;
                        this.TitlePage = 'Add Commodity';
                        this.formBuilder();
                    }
                    else{
                        this.redirectDeny();
                    }
                }

                // load detail / init data form
                if(this.Action=='edit'||this.Action=='view')
                {
                    this.getCommodityDetail();
                }

                /*
                 * Action router Edit
                 * */
                if(this.Action=='edit')
                {
                    if(this.editCommodity) {
                        this.allowAccess=true;
                        this.TitlePage = 'Edit Commodity';
                    }
                    else{
                        this.redirectDeny();
                    }
                }

            },
            err => {
                this.parseError(err);
                this.showLoadingOverlay = false;
            }
        );
    }

    private redirectDeny(){
        this._router.parent.navigateByUrl('/deny');
    }



    private formBuilder(data={}){

        this.CommodityForm =this.fb.group({
            cmd_name: [data['cmd_name'], Validators.compose([Validators.required, this._Valid.validateSpace, this._Valid.invalidNameStr])],
            cmd_cls: [data['cmd_cls'],Validators.compose([Validators.required, this._Valid.validateSpace, this._Valid.invalidCommodityClass])],
            commodity_nmfc: [data['commodity_nmfc'],Validators.compose([Validators.required, this._Valid.validateSpace, this._Valid.invalidCommodityNMFC])],
            cmd_des: [data['cmd_des']]
        });

    }





    getCommodityDetail(){

        this.cmd_id=this.params.get('id');
        this._cmdSerivce.getCommodityDetail(this.cmd_id).subscribe(
            data => {
                this.formBuilder(data);
                this.CommodityDeatail=data;
            },
            err => {
                this.parseError(err);
            },
            () => {}
        );
    }




    Save(){

        this.submitForm=true;

        // empty messages
        this.messages=false;

        // scroll to top
        jQuery('html, body').animate({
            scrollTop: 0
        }, 700);

        if(this.CommodityForm.valid){
            let data=this.CommodityForm.value;
            // check by order , if no by order -> remove order number

            data['cmd_name'] = data['cmd_name'].trim();
            let data_json = JSON.stringify(data);

            if (this.Action == 'new') {
                // add new
                this.CreateCommodity(data_json);
            }
            else {
                // Update
                this.Update(data_json);
            }

        }
        else{
        }

    }

    CreateCommodity(data){
        this.showLoadingOverlay=true;
        this._cmdSerivce.createCommodity(data).subscribe(
            data => {
                this.showLoadingOverlay = false;
                //Reset Form
                this.messages = this._Func.Messages('success', this._Func.msg('VR107'));
                setTimeout(()=>{
                    this.redirectToList();
                }, 600);
            },
            err => {
                this.showLoadingOverlay= false;
                this.parseError(err);
            },
            () => {}
        );
    }

    Update(data){

        this.showLoadingOverlay=true;
        this._cmdSerivce.updateCommodity(data,this.cmd_id).subscribe(
            data => {
                this.showLoadingOverlay=false;
                this.messages = this._Func.Messages('success', this._Func.msg('VR109'));
                setTimeout(()=>{
                    this.redirectToList()
                }, 600);
            },
            err => {
                this.showLoadingOverlay=false;
                this.parseError(err);
            },
            () => {}
        );

    }

    private redirectToList(){
        this._router.navigateByUrl('/master-data/commodity');
    }

    private cancel()
    {
        let n = this;
        this._boxPopupService.showWarningPopup()
            .then(function (dm) {
                if(dm)
                  n.redirectToList();
            });
    }


    // Show error when server die or else
    private parseError (err) {
        this.messages = {'status' : 'danger', 'txt' : this._Func.parseErrorMessageFromServer(err)};
    }

    private isNumber(evt, int:boolean = false) {
        this._Valid.isNumber(evt, int);
    }
}
