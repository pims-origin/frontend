import { Component, Input, Output, EventEmitter } from '@angular/core';
declare const jQuery: any;

@Component({
    selector: 'wms-multi-select',
    templateUrl: 'wms-multi-select.html'
})

export class WMSMultiSelectComponent {

    public id: string;
    public _name: string;
    public _options: any[] | null;
    @Input() set name(value: string) {
        if (value) {
            this.id = value;
            this._name = value;
        }
    };
    @Input() set options(value: any[] | null) {
        if (value) {
            this._options = value;
            this._iniMultiselect();
        }
    };
    @Input() numberDisplayed = 2;
    @Input() maxHeight = 500;
    @Input() nonSelectedText = 'ALL';
    @Input() includeSelectAllOption = true;
    @Input() selectAllText = 'Select All';
    @Input() set refresh(value: any[] | null) {
        if (value) {
            this._refreshMultiselect();
        }
    }

    @Output() onRefreshed = new EventEmitter();

    constructor(
        
    ) {}

    private _iniMultiselect() {
        setTimeout(() => {
            jQuery(`[name='${this._name}']`).multiselect({ 
                numberDisplayed: this.numberDisplayed,
                maxHeight: this.maxHeight,
                nonSelectedText: this.nonSelectedText,
                includeSelectAllOption: this.includeSelectAllOption,
                selectAllText: this.selectAllText
            });
        }, 500);
    }

    private _refreshMultiselect() {
        jQuery(`[name='${this._name}']`).multiselect('refresh');
        this.onRefreshed.emit(false);
    }

}