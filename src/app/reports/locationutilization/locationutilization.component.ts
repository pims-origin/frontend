
import {Component} from '@angular/core';
import {API_Config} from '../../common/core/API_Config';
import {BaseTable} from '../../common/core/classes_us/base-table';
import {Functions} from '../../common/core/functions';
import {TableFunctions} from '../../common/core/table-functions';
import {DateFunctions} from '../../common/core/functions/date-functions';
import {BaseService} from '../../common/core/classes_us/base-service';
import {CustomersService} from '../../master-data/customers/customers-service';
import {TableStructure} from '../../common/directives/directives';

@Component ({
    selector: 'locationutilization',
    directives: [
        TableStructure
    ],
    templateUrl: '../../common/directives/table-structure/table.component.html',
    providers: [
        BaseService,
        CustomersService,
        TableFunctions,
        DateFunctions
    ]
})

export class LocationUtilizationComponent extends BaseTable
{
    public tableID = 'locationutilization';
    public url = this._api.API_LOCATIONUTILIZATION_REPORTS;
    public headerUrlSuffix = 'rep';
    public title = 'Location Utilization Report';
    public permissionPage = 'viewReport';
    public pinCols = 2;

    public headerDef = [
        {
            id: 'ver_table',
            value: 1 + Math.random() * 10000
        },
        this.columnList.warehouse,
        this.columnList.location,
        this.columnList.locationType,
        this.columnList.customer,
        this.columnList.plateNumber,
        this.columnList.totalCustomer,
        this.columnList.totalCartons,
        this.columnList.totalPieces

    ];

     public unsortableColumns = [
        'warehouse',
        'locationType',
        'customer',
        'plateNumber',
        'totalCustomer',
        'totalCartons',
        'totalPieces'

     ];

      public searches = [
        this.searchList.customer,
        this.searchList.location,
        this.searchList.locationType,
        this.searchList.plateNumber,
        this.searchList.totalCartons,
        this.searchList.totalCustomer,
        this.searchList.totalPieces
     ];

    /*
    ****************************************************************************
    */

    constructor (
        public _api: API_Config,
        public _func: Functions,
        public _tableFunc: TableFunctions,
        public _tableService: BaseService,
        public _customersService: CustomersService
    )
    {
        super();

        this._tableFunc.permissionCall(this);
    }

    //**************************************************************************

}
