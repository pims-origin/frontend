import {Component} from '@angular/core';
import { FORM_DIRECTIVES,ControlGroup,FormBuilder, ControlArray ,Control,Validators} from '@angular/common';
import {WMSBreadcrumb,DocumentUpload,WMSMessages, WMSDatepicker} from '../../../common/directives/directives';
import {Router, RouteData, RouteParams } from '@angular/router-deprecated';
import {BoxPopupService} from "../../../common/popup/box-popup.service";
import {GoBackComponent} from "../../../common/component/goBack.component";
import { ValidationService } from '../../../common/core/validator';
import {UserService, Functions,FormBuilderFunctions} from '../../../common/core/load';
import {ASN_Services} from "../asn-services";
import {ASNFunctions} from "../functions";
import {WarningBoxPopup} from "../../../common/popup/warning-box-popup";
import {ASNFormProvider} from "./asn.form";
declare var jQuery: any;
@Component ({
    selector: 'asn-cru',
    styleUrls:['../style.scss'],
    directives:[WMSDatepicker,WMSBreadcrumb,GoBackComponent,DocumentUpload,FORM_DIRECTIVES,WMSMessages],
    providers: [BoxPopupService,ASNFormProvider,ASNFunctions,ValidationService,FormBuilderFunctions,ASN_Services,UserService,Functions],
    templateUrl: 'asn-cru.html'
})
export class ANS_CRU_Component{

    ASNForm: ControlGroup;
    private messages;
    private showLoadingOverlay=false;
    private TitlePage="ADD ASN";
    private IsView=false;
    private Action;
    private whs_id;
    private submitForm=false;
    private cus_id;
    items:ControlGroup[]=[];
    itemsTable: ControlArray= new ControlArray(this.items);
    private Customers:Array<any>=[];
    private dataListMeasurements:Array<any>=[];
    private asnID;
    private ctn_id;
    private dataListContainersByASN:Array<any>=[];
    private idPiece:any;
    private dataListUoms:Array<any>=[];
    private mea_unit_default={txt:'Inch', unit:'IN',wt:'Pound',wt_code:'LB'};
    private mea_unit=this.mea_unit_default;

    constructor(private _router: Router,
                private _boxPopupService:BoxPopupService,
                _RTAction: RouteData,
                private _user:UserService,
                private _ans_service:ASN_Services,
                private fb: FormBuilder,
                private asnFun:ASNFunctions,
                private asnForm:ASNFormProvider,
                private fbdFun:FormBuilderFunctions,
                private _Valid: ValidationService,
                private _Func: Functions,
                private params: RouteParams) {

        this.Action=_RTAction.get('action');
        this.whs_id=this._Func.lstGetItem('whs_id');
        this.checkPermission();

    }


    private createASN;
    private editASN;
    private allowAccess:boolean=false;
    checkPermission(){

        this.showLoadingOverlay = true;
        this._user.GetPermissionUser().subscribe(
            data => {

                this.showLoadingOverlay = false;
                this.createASN = this._user.RequestPermission(data, 'createASN');
                this.editASN = this._user.RequestPermission(data,'editASN');
                this.getCustomersByWH();
                this.getUoms();

                if(this.Action=='new')
                {
                    if(this.createASN){
                        this.allowAccess=true;
                        this.TitlePage = 'Add ASN';
                        this.formBuilder();
                    } else{
                        this.redirectDeny();
                    }

                }

                if(this.Action=='edit')
                {
                    if(this.editASN){
                        this.allowAccess=true;
                        this.asnID = this.params.get('id');
                        this.ctn_id = this.params.get('ctn_id');
                        this.getAsnByIdCtnId(this.ctn_id);
                        this.getASNStatus();
                        this.TitlePage = 'Edit ASN';
                    }

                    else{
                        this.redirectDeny();
                    }

                }


            },
            err => {
                this.parseError(err);
                this.showLoadingOverlay = false;
            }
        );
    }

    private redirectDeny(){
        this._router.parent.navigateByUrl('/deny');
    }

    private addNewItem(data={}) {

        if(!data['dtl_uom_id']){
            //data['dtl_uom_id'] = this.idPiece;
        }
        
        this.asnForm.addNewItem(this.itemsTable,data);

    }

    private formBuilder(data={}){

        this.ASNForm = this.asnForm.asnFormBuilder(data,this.itemsTable,this.whs_id);

        if(data['asn_details']){
            data['asn_details'].forEach((item)=>{
                this.addNewItem(item);
            });
        }else{
            this.addNewItem();
        }

        this.itemsTable.valueChanges.subscribe(data =>{
            this.asnFun.checkTotalExpCarton(this.itemsTable);
            this.asnFun.checkDuplicatedKey(this.itemsTable,this._ans_service.duplicatedField());
        });

        this.ckStatusForDiableBtn();

    }

    private selectedContainer(controlGroup:ControlGroup,ctn={}){
        this.asnForm.selectedContainer(controlGroup,ctn);
    }

    private showForm=true;
    private changeCtnr($event){

        this.messages = false;
        let curentValue=this.ASNForm.value['ctnr_id_preview'];
        if(curentValue&&!this.IsView){
            let that = this;
            let warningPopup = new WarningBoxPopup();
            warningPopup.text = this._Func.msg('ASNM043');
            this._boxPopupService.showWarningPopup(warningPopup)
                .then(function (dm) {
                    if(dm) {
                        that.fbdFun.emptyControlArray(that.itemsTable);
                        if(parseInt($event.target.value)>0){
                            that.getAsnByIdCtnId($event.target.value);
                        }else{
                            that.addNewItem();
                            that.fbdFun.setValueControl(that.ASNForm,'ctnr_num','');
                        }
                    }
                    else{
                        that.fbdFun.setValueControl(that.ASNForm,'ctnr_id_preview',curentValue);
                    }
                });
        }else{
            if($event.target.value){
                this.fbdFun.emptyControlArray(this.itemsTable);
                this.getAsnByIdCtnId($event.target.value);
            }
        }

    }

    private changeCustomer(){
        this.fbdFun.emptyControlArray(this.itemsTable);
        this.addNewItem(this.itemsTable);
    }

    private getContainersByASN () {

        this._ans_service.getContainersByASN(this.asnID)
            .subscribe(
                data => {
                    this.dataListContainersByASN = data.data;
                },
                err => {
                    this.parseError(err);
                },
                () => {}
            );

    }

    private cancel()
    {
        let n = this;
        this._boxPopupService.showWarningPopup()
            .then(function (dm) {
                if(dm)
                    n._router.navigateByUrl('/inbound/asn');

            });
    }

    private getAsnByIdCtnId (ctn_id) {

        let data;
        this.showLoadingOverlay = true;
        this._ans_service.getAsnByIdCtnId(this.asnID, ctn_id)
            .subscribe(
                data => {
                    this.initForm(data.data);
                    this.flagCkHeader(data.data['asn_details']);
                },
                err => {
                    this.showLoadingOverlay = false;
                    this.parseError(err);
                }
            );

    }

    private initForm(data={}){
        // FIX BUG
        this.showForm = false;
        setTimeout(()=>{
            this.showForm = true;
            this.formBuilder(data);
            setTimeout(()=>{
                this.showLoadingOverlay = false;
            },100);
        })

    }

    private autoSearchData=[];
    private autoComplete(controlGroup:ControlGroup,key,fieldname){

        this.autoSearchData=[];
        if(this.ASNForm.value['cus_id'] && key){

            controlGroup['showLoading']=true;
            let searchkey=this._Func.trim(key);
            let param="?"+fieldname+"="+searchkey+"&cus_id="+this.ASNForm.value['cus_id']+"&limit=20&sort["+fieldname+"]=asc";
            let enCodeSearchQuery=this._Func.FixEncodeURI(param);

            // if(controlGroup['subscribe']){
            //     controlGroup['subscribe'].unsubscribe();
            // }
            controlGroup['subscribe']=this._ans_service.searchItems(enCodeSearchQuery).debounceTime(400)
                .distinctUntilChanged().subscribe(
                data => {
                    controlGroup['showLoading']=false;
                    this.autoSearchData = data.data;
                    console.log(this.autoSearchData);
                    if(key){
                        this.asnForm.checkKeyExist(controlGroup,key,data.data,'sku');
                    }
                },
                err =>{
                    controlGroup['showLoading']=false;
                    this.parseError(err);
                }
            );
        }

    }


    private containerAutoData=[];
    private autoCompleteContainer(key){

        let searchkey=this._Func.trim(key);
        let params = "?ctnr_num=" + key + "&limit=20";
        let item=this.ASNForm.controls['ctnr_num'];
        item['showLoading']=true;

        this._ans_service.getContainerByName(this._Func.FixEncodeURI(params)).debounceTime(400).distinctUntilChanged().subscribe(
            data => {
                item['showLoading']=false;
                this.containerAutoData = data;
                this.asnForm.checkExistContainer(this.ASNForm,key,data);
            },
            err =>{
                item['showLoading']=false;
                this.parseError(err);
            }
        );


    }

    private selectedItem(controlGroup: ControlGroup, item = {}) {
        this.fbdFun.setValueControl(controlGroup, 'dtl_uom_id', item['uom_id']);
        this.asnForm.selectedItem(controlGroup, item);
    }

    private saveMoreContainer:boolean = false;
    private Save(form,saveMoreContainer=false){

        this.submitForm=true;
        this.asnFun.checkTotalExpCarton(this.itemsTable);
        if(form.valid){
            this.showLoadingOverlay = true;
            let data = JSON.stringify(form.value);
            this._ans_service.saveASN(this.asnID, data, form.value['ctnr_id'])
                .subscribe(
                    data => {

                        this.submitForm=false;
                        if(saveMoreContainer){
                            this._saveMoreContainer(data);
                        }else{
                            this.saveMoreContainer = false;
                        }

                        if(this.Action=='new'&&!saveMoreContainer) {
                            this.messages = this._Func.Messages('success', 'The ' + data.data['asn_hdr_num'] + ' was added successfully!');
                        }
                        if(this.Action=='edit'&&!saveMoreContainer){
                            this.messages = this._Func.Messages('success', 'The ' + data.data['asn_hdr_num'] + ' was updated successfully!');
                        }

                        this.showLoadingOverlay = false;

                    },
                    err => {
                        this.showLoadingOverlay = false;
                        this.parseError(err);
                    },
                    () => {
                        if(!saveMoreContainer){
                            setTimeout(()=> {
                                this._router.parent.navigateByUrl('/inbound/asn');
                            }, 1100);
                        }
                    }
                );
        }
    }
    private flagShowBtnAddMore = true;
    private ckStatusForDiableBtn () {

        if (this.Action != "edit")
        {
            return true;
        }
        this.flagShowBtnAddMore = true;

        for (var i = 0; i < this.itemsTable['value'].length; i++) {

            if (this.itemsTable['value'][i]['created_from'].toLowerCase() == 'wms') {

                this.flagShowBtnAddMore = false;

            }

            this.flagShowBtnAddMore = false;
            if ( this.itemsTable['value'][i]['created_from'].toLowerCase() != 'wms' && (this.itemsTable['value'][i]['asn_dtl_sts_code'] == 'NW' || this.itemsTable['value'][i]['asn_dtl_sts_code'] == 'RG')) {
                this.flagShowBtnAddMore = true;
                break;

            }
        }
    }

    private getIDPiece() {

        var target = this;
        var param = '?sys_uom_code=ea';
        this._ans_service.getUoms(param).subscribe(
            data =>{
                this.idPiece =  data.data[0].sys_uom_id;
                if(this.itemsTable.controls.length && this.Action=='new'){
                    this.fbdFun.setValueControlItem(this.itemsTable,0,'dtl_uom_id',this.idPiece);
                }
            },
            err => {
                this.getUoms();
            },
            () => {
                this.getUoms();
            }
        );
    }

    // Get uoms
    private getUoms () {

        var params = "?limit=10000&sort[sys_uom_name]=asc&sys_uom_type=item";
        this._ans_service.getUoms(params)
            .subscribe(
                data => {
                    this.dataListUoms = data.data;
                    for (var i = 0; i < this.dataListUoms.length; i++) {

                        if ( this.dataListUoms[i]['sys_uom_code'].toLowerCase() == 'ct') {
                            this.idPiece = this.dataListUoms[i]['sys_uom_id'];
                            //this.fbdFun.setValueControlItem(this.itemsTable,0,'dtl_uom_id',this.idPiece);
                        }
                    }
                },
                err => {
                    this.parseError(err);
                },
                () => {}
            );

    }

    private _saveMoreContainer(data:any){

        this.messages = this._Func.Messages('success', 'The container ' + this.ASNForm.value['ctnr_num'] + ' is added to ASN ' + data.data['asn_hdr_num'] + ' successfully!');
        this.asnID=data.data['asn_hdr_id'];
        this.getContainersByASN();
        this.fbdFun.emptyControlArray(this.itemsTable);
        this.fbdFun.setValueControl(this.ASNForm,'ctnr_id',0);
        this.fbdFun.setValueControl(this.ASNForm,'ctnr_num','');
        this.fbdFun.setValueControl(this.ASNForm,'ctnr_id_preview',0);
        this.fbdFun.setValueControl(this.ASNForm,'ctnr_sts',0);
        this.ASNForm.controls['ctnr_num']['xtouched'] = false;
        this.addNewItem();
        this.saveMoreContainer = true;
    }


    deleteItem(controlArray:ControlArray){

        let n = this;
        if (this.fbdFun.getItemchecked(this.itemsTable) > 0) {

            let warningPopup = new WarningBoxPopup();
            warningPopup.text = this._Func.msg('PBAP002');
            this._boxPopupService.showWarningPopup(warningPopup)
                .then(function (dm) {
                    if (!dm) {
                        return;
                    } else {
                        n.fbdFun.deleteItem(n.itemsTable).subscribe(deletedAll=>{
                            if(deletedAll) {
                                n.addNewItem();
                            }
                        });
                    }
                });
        }
        else {
            this.messages = this._Func.Messages('danger', this._Func.msg('VR113'));
        }
    }

    private getCustomersByWH () {

        let params='?whs_id=' + this.whs_id + "&limit=10000";
        this._ans_service.getCustomersByWH(params)
            .subscribe(
                data => {
                    this.Customers = data.data;
                },
                err => {
                    this.parseError(err);
                }
            );

    }

    checkInputNumber($event,int){
        this._Valid.isNumber($event,int);
    }

    private parseError (err) {
        this.messages = {'status' : 'danger', 'txt' : this._Func.parseErrorMessageFromServer(err)};
    }
    private parseErrorStandard (err) {
        this.messages = {'status' : 'danger', 'txt' : this._Func.parseErrorMessageFromServerStandard(err)};
    }



    private checkExpqty(controlGroup:ControlGroup){
        this.asnForm.checkExpqty(controlGroup);
    }

    // Get ASN All status
    private asnStatus = [];
    private getASNStatus () {

        this._ans_service.getAsnStatus()
            .subscribe(
                data => {

                    this.asnStatus = data.data;
                    this.getContainersByASN();

                },
                err => {

                    this.parseError(err);
                },
                () => {}
            );

    }

    private disableHeader = false;
    private flagCkHeader($data = []){

        for (var i = 0; i < $data.length; i++) {

            if ($data[i]['asn_dtl_sts_code'] !='NW'){
                this.disableHeader = true;
                break;
            }
        }
    }

}