import {Component,OnInit} from "@angular/core";
import { Router, RouterLink, ROUTER_DIRECTIVES } from '@angular/router-deprecated';
import { FORM_DIRECTIVES,ControlGroup,FormBuilder, Control,Validators} from '@angular/common';
import {API_Config, UserService, Functions} from '../../../common/core/load';
import { Http } from '@angular/http';
import {BoxPopupService} from "../../../common/popup/box-popup.service";
import {WorkOrderServices} from "../work-order-service";
import { ValidationService } from '../../../common/core/validator';
import {WMSPagination,AgGridExtent,WMSMessages} from '../../../common/directives/directives';
import 'ag-grid-enterprise/main';
declare var jQuery: any;

@Component({
  selector: 'event-tracking',
  providers:[WorkOrderServices,ValidationService,BoxPopupService],
  directives: [ROUTER_DIRECTIVES,AgGridExtent,WMSMessages,WMSPagination],
  templateUrl: 'work-order-list.component.html',

})
export class WorkOrderList{

  private headerURL = this.apiService.API_User_Metas+'/wo';
  private columnData = {
    wo_hdr_id:{
      attr:'checkbox',
      id:true,
      title:'#',
      width:25,
      pin:true,
      ver_table:'23s9996'
    },
    wo_sts: {
      title:'Status',
      width:80,
      pin:true,
      sort:true
    },
    wo_hdr_num: {
      title:'Work Order Number',
      width:170,
      url:'#/outbound/work-order/',
      field_get_id:'wo_hdr_id',
      pin:true,
      sort:true
    },
    cus_name:{
      title:'Customer',
      width:120,
      pin:true
    },
    odr_hdr_num: {
      title:'Order Number',
      width:170,
      pin:true,
      sort:true
    },
    type:{
      title:'Pack By',
      width:120,
      pin:true
    },
    sku: {
      title:'# of SKUs',
      width:70,
      pin:true
    },
    chg_code: {
      title:'# of Charge Code',
      width:120,
      pin:true,
      sort:true
    },
    created_by: {
      title:'User',
      width:150,
      sort:true
    },
    created_at: {
      title:'Created Date',
      width:130,
      sort:true
    }
  };

  private Pagination;
  private perPage=20;
  private whs_id;
  private messages;
  private Loading={};
  private searching={};
  searchForm:ControlGroup;
  private isSearch:boolean=false;
  private workOrderList:Array<any>=[];
  private sortData={fieldname:'created_at',sort:'desc'};
  private showLoadingOverlay:boolean=false;
  private ListSelectedItem:Array<any>=[];
  private Customers:Array<any>=[];

  constructor(
    private wodService:WorkOrderServices,
    private _Func: Functions,
    private _boxPopupService: BoxPopupService,
    private  apiService:API_Config,
    private _user:UserService,
    private fb: FormBuilder,
    private _http: Http,
    private _Valid: ValidationService,
    private _router: Router) {

    this.checkPermission();

    this.buildSearchForm();
    this.whs_id = localStorage.getItem('whs_id');


  }

  // Check permission for user using this function page
  private createWorkOrder;
  private viewWorkOrder;
  private editWorkOrder;
  private allowAccess:boolean=false;
  private checkPermission() {
    this.showLoadingOverlay = true;
    this._user.GetPermissionUser().subscribe(
      data => {
        this.createWorkOrder = this._user.RequestPermission(data, 'createWorkOrder');
        this.viewWorkOrder = this._user.RequestPermission(data,'viewWorkOrder');
        this.editWorkOrder=this._user.RequestPermission(data,'editWorkOrder');
        /* Check orther permission if View allow */
        if(!this.viewWorkOrder) {
          this.redirectDeny();
        }
        else {
          this.allowAccess=true;

          // load default
          this.getCustomersByWH();

          this.getWorkOrderList();
        }
      },
      err => {
        this.messages=this._Func.Messages('danger', this._Func.parseErrorMessageFromServer(err));
        this.showLoadingOverlay = false;
      }
    );
  }

  buildSearchForm()
  {
    this.searchForm =this.fb.group({
      cus_id: [''],
      wo_hdr_num: [''],
      odr_hdr_num: [''],
      wo_sts: [''],
      type: ['']
    });
  }

  private searchParam;
  Search()
  {
    this.isSearch=true;
    let data=this.searchForm.value;
    // set status search is true
    // assign to SearchQuery
    // whs_id=&cus_code=&cus_city_name=&cus_name=&cus_billing_account=&cus_country_id=&cus_state_id=&cus_postal_code=
    this.searchParam="&cus_id="+data['cus_id']+"&wo_hdr_num="+encodeURIComponent(data['wo_hdr_num'])+"&odr_hdr_num="
      +encodeURIComponent(data['odr_hdr_num'])+"&wo_sts="+data['wo_sts']+"&type="+data['type'];

    this.getWorkOrderList(1);
  }


  redirectDeny(){
    this._router.parent.navigateByUrl('/deny');
  }

  // get list custommer

  private getCustomersByWH () {

    let params='?whs_id=' + this.whs_id + "&limit=10000";
    this.wodService.getCustomersByWH(params)
      .subscribe(
        data => {
          this.Customers = data.data;
        },
        err => {
          this.parseError(err);
        },
        () => {
        }
      );

  }

  private onPageSizeChanged($event)
  {
    this.perPage = $event.target.value;
    if(this.Pagination){
      if((this.Pagination['current_page'] - 1) * this.perPage >= this.Pagination['total']) {
        this.Pagination['current_page'] = 1;
      }
      this.getWorkOrderList(this.Pagination['current_page']);
    }

  }


  /*
   * Router edit
   * */
  Edit() {
    let selectedItem:any;

    if (this.ListSelectedItem.length > 1) {
      this.messages = this._Func.Messages('danger',this._Func.msg('WMS002'));
      this.scolltoTop();
    }
    else {
      selectedItem = this.ListSelectedItem.length ? this.ListSelectedItem[0] : null;

      if (!selectedItem) {
        this.messages = this._Func.Messages('danger',this._Func.msg('WMS001'));
        this.scolltoTop();
      } else {
        this._router.navigateByUrl('/outbound/work-order/' + this.ListSelectedItem[0]['wo_hdr_id'] + '/edit');
      }
      /*else {
        this.messages = this._Func.Messages('danger',this._Func.msg('WO002'));
        this.scolltoTop();
      }*/
    }
  }


  private getWorkOrderList(page=null){

    let param="?sort["+this.sortData['fieldname']+"]="+this.sortData['sort']+"&page="+page+"&limit="+this.perPage;
    if(this.isSearch){
      param=param+this.searchParam;
    }
    this.showLoadingOverlay=true;
    this.wodService.getWorkOrderList(param).subscribe(
      data => {
        this.showLoadingOverlay=false;
        this.workOrderList = this._Func.formatData(data.data);
        //pagin function
        this.Pagination=data['meta']['pagination'];
        this.Pagination['numLinks']=3;
        this.Pagination['tmpPageCount']=this._Func.Pagination(this.Pagination);
      },
      err => {
        this.parseError(err);
        this.showLoadingOverlay=false;
      },
      () => {}
    );

  }
  scolltoTop(){
    jQuery('html, body').animate({
      scrollTop: 0
    }, 700);
  }
  /*
   * Reset Form Builder
   * */
  ResetSearch()
  {
    this.isSearch=false; // remove action search
    this._Func.ResetForm(this.searchForm);
    this.getWorkOrderList(this.Pagination['current_page']);
  }

  // Show error when server die or else
  private parseError (err) {
    err = err.json();
    this.messages = this._Func.Messages('danger', err.errors.message);
  }

}
