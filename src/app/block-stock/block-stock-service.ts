import {Component, Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {API_Config, Functions} from '../common/core/load';

@Injectable()

export class BlockStockServices {
  public AuthHeader = this._Func.AuthHeader();
  public AuthHeaderPost = this._Func.AuthHeaderPost();
  public AuthHeaderPostJSON = this._Func.AuthHeaderPostJson();
  private urlAPIItem = this._API.API_ItemMaster;

  constructor(private _Func:Functions, private _API:API_Config, private http:Http) {

  }

  /*
   ********************************************************************************************************************
   */

  addNewCycleCount(data) {

    return this.http.post(this._API.API_Cycle_Count, data , {headers: this.AuthHeaderPostJSON})
      .map(res => res.json());
  }

  /*
   ********************************************************************************************************************
   */

  addReasonBlockStock(data) {

    return this.http.post(this._API.API_Block_Stock_Root + '/add-reason', data , {headers: this.AuthHeaderPostJSON})
        .map(res => res.json());
  }


  /*
   ********************************************************************************************************************
   */

  addNewBlockStock(data) {

    return this.http.post(this._API.API_Block_Stock_Root, data , {headers: this.AuthHeaderPostJSON})
        .map(res => res.json());
  }

  /*
   ********************************************************************************************************************
   */

  addUnblockBlockStock(BlockID, data) {

    return this.http.post(this._API.API_Block_Stock_Root + '/unblock/' + BlockID, data , {headers: this.AuthHeaderPostJSON})
        .map(res => res.json());
  }


  /*
   ********************************************************************************************************************
   */

  checkSameCusAssign(data) {

    return this.http.post(this._API.API_Cycle_Count + '/check-cus-in-user', data , {headers: this.AuthHeaderPostJSON})
        .map(res => res.json());
  }

  // Autocomplete for search
  public getItemBySKU (sku, cus_id = '') {

    return this.http.get(this.urlAPIItem +  '?sku=' + sku + '&cus_id=' + cus_id, {headers: this.AuthHeader})
        .map(res => res.json().data);


  }

  // Autocomplete for search
  public getItemBySKUCC (sku, cus_id = '') {

    return this.http.get(this._API.API_Cycle_Count_Item +  '?sku=' + sku + '&cus_id=' + cus_id, {headers: this.AuthHeader})
        .map(res => res.json().data);


  }

  /*
   ********************************************************************************************************************
   */

  updateCycleCount(id, data) {

    return this.http.put(this._API.API_Cycle_Count + '/' + id, data, {headers: this._Func.AuthHeaderPostJson()})
      .map(res => res.json());
  }

  /*
   ********************************************************************************************************************
   */

  updateItemOfCycleCount(id, data) {

    return this.http.put(this._API.API_Count_Item_List + id, data, {headers: this._Func.AuthHeaderPostJson()})
        .map(res => res.json());
  }

  /*
   ********************************************************************************************************************
   */

  getCycleCountDetail(id) {

    return this.http.get(this._API.API_Cycle_Count + '/' + id, {headers: this.AuthHeader})
      .map((res:Response) => res.json());
  }

  /*
 ********************************************************************************************************************
 */

  getCycleCount(param) {

    return this.http.get(this._API.API_Cycle_Count + param, {headers: this.AuthHeader})
        .map((res:Response) => res.json());
  }

  /*
   ********************************************************************************************************************
   */

  getListReason(params = '') {

    return this.http.get(this._API.API_Block_Stock_Root + '/reason-list', {headers: this.AuthHeader})
        .map((res:Response) => res.json());
  }

  /*
   ********************************************************************************************************************
   */

  getBlockStock(param) {

    return this.http.get(this._API.API_Block_Stock_Root + param, {headers: this.AuthHeader})
        .map((res:Response) => res.json());
  }

  /*
   ********************************************************************************************************************
   * Print CC
   */

  printPdf($cc_id) {

    return this.http.get(this._API.API_Cycle_Count + "/print-pdf/" + $cc_id  ,{headers: this.AuthHeader})
        .map((res:Response) => res.json());
  }


  /*
   ********************************************************************************************************************
   * Recount CC
   */

  recount($params) {

    return this.http.post(this._API.API_Cycle_Count + "/recount", $params ,{headers: this.AuthHeaderPostJSON})
        .map((res:Response) => res.json());
  }


  /*
   ********************************************************************************************************************
   * accept CC
   */

  accept($params) {

    return this.http.post(this._API.API_Cycle_Count + "/approve", $params ,{headers: this.AuthHeaderPostJSON})
        .map((res:Response) => res.json());
  }


  //Save SKU
  saveSku(cc_id, $params) {

    return this.http.post(this._API.API_Cycle_Count + "/" + cc_id + '/add-sku', $params ,{headers: this.AuthHeaderPostJSON})
        .map((res:Response) => res.json());
  }

  /*
   ********************************************************************************************************************
   */

  search(param) {
    return this.http.get(this._API.API_Cycle_Count + param, {headers: this.AuthHeader})
      .map((res:Response) => res.json());
  }

  /*
   ********************************************************************************************************************
   */

  getCycleCountStatus() {
    return this.http.get(this._API.API_Cycle_Status, {headers: this.AuthHeader})
      .map(res => res.json());
  }

  /*
   ********************************************************************************************************************
   */

  public getCustomersByWH($params) {

    return this.http.get(this._API.API_CUSTOMER_MASTER + '/customer-warehouses' + $params + '&sort[cus_name]=asc',
      {headers: this.AuthHeader}).map(res => res.json());

  }

  /*
 ********************************************************************************************************************
 */

  getLocationByWH(whsId, param) {
    return this.http.get(this._API.API_Warehouse + "/" + whsId + "/locations" + param, {headers: this.AuthHeader})
        .map((res:Response) => res.json());
  }

  /*
   ********************************************************************************************************************
   */

  getItemByWH(param) {
    return this.http.get(this._API.API_Block_Stock_Root + '/items' + param, {headers: this.AuthHeader})
        .map((res:Response) => res.json());
  }

  /*
   ********************************************************************************************************************
   */

  getReason() {

    return this.http.get(this._API.API_Block_Stock_Root + '/reason-list', {headers: this.AuthHeader})
        .map((res:Response) => res.json());
  }

  /*
   ********************************************************************************************************************
   */

  getItemLocation(idLocations, params) {
    return this.http.get(this._API.API_GOODS_RECEIPT_MASTER + '/relocations' + "/" + idLocations + params, {headers: this.AuthHeader})
      .map((res:Response) => res.json());
  }




  /*
   ********************************************************************************************************************
   */

  public getCountItemByBlockID(blockID, param = '') {

    return this.http.get(this._API.API_Block_Stock_Root + '/' + blockID + '/list' + param, {headers: this.AuthHeader})
      .map(res => res.json());

  }

  /*
   ********************************************************************************************************************
   */

  public getCountItemByCycleID(cycleID, param) {

    return this.http.get(this._API.API_Count_Item_List + cycleID + param, {headers: this.AuthHeader})
        .map(res => res.json());

  }

  /*
   ********************************************************************************************************************
   */

  getCycleCountUsers() {

    return this.http.get(this._API.API_Cycle_Count_User, {headers: this.AuthHeader})
      .map(res => res.json());

  }

  /*
 ********************************************************************************************************************
 */

  getDetailBlockStock(blockID) {

    return this.http.get(this._API.API_Block_Stock_Root + '/' + blockID, {headers: this.AuthHeader})
        .map(res => res.json());

  }

  /*
   ********************************************************************************************************************
   */

  getCycleCountType() {
    return this.http.get(this._API.API_Cycle_Type, {headers: this.AuthHeader})
      .map(res => res.json());
  }

  /*
   ********************************************************************************************************************
   */

  autoSKU(params){
    return this.http.get(`${this._API.API_Cycle_Count}/auto-sku${params}`, {headers: this.AuthHeader})
        .map(res => res.json());
  }

}
