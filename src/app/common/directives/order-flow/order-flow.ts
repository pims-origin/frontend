import {Component,Input, Output,EventEmitter} from '@angular/core';
import {orderFlowServices} from "./order-flow-service";
import { API_Config,Functions} from '../../../common/core/load';

@Component ({
  selector: 'order-flow-directive',
  directives:[],
  providers: [orderFlowServices],
  templateUrl: 'order-flow.html'
})

export class OrderFlowDirective {

  private _cusId = '';
  private orderFlowDetail={};
  private _orderId;
  private _disable;
  private _environment;
  private hiddenFlowCodes = ['APK', 'PAK', 'BOL', 'ARS'];
  private _orderType = '';
  private _flagOrderFolow = '';
  private _orderStatus;
  private _action;

  @Input('action') set action(value: string) {
    this._action=value;
  }

  @Input('orderStatus') set orderStatus(value: string) {
    this._orderStatus = value;
  }

  @Input('orderId') set orderId(value: string) {
    this._orderId=value;
  }

  @Input('disabled') set disabled(value: string) {
    if (this._action == 'view') {
      this._disable = value;
    } else if (this._action == 'edit' && this._orderStatus != 'New'){
      this._disable = value;
    }
  }

  @Input('environment') set environment(value: string) {
    this._environment=value;
  }

  @Input('flagOrderFolow') set flagOrderFolow(value: string) {
    this._flagOrderFolow=value;
  }

  @Input('orderType') set orderType(value: string) {
    if (value) {
      this._orderType=value;
      if (this.orderFlowDetail['value']) {
        this._checkRelationship();
        this.emitData();
      }
    }
  }


  @Input('cusId') set cusId(value: string) {
    this._cusId=value;
    if(value){
      this.getOrderFlow();
    }
  }

  @Output() orderFlowReturnData  = new EventEmitter();
  @Output() messages=  new EventEmitter();
  @Output() showLoadingOverlay=  new EventEmitter();

  constructor(
              private _Func: Functions,
              private _API: API_Config,
              private _ODFSerivce:orderFlowServices) {}

  private getOrderFlow(){
    let param = this._cusId;
    // this.showLoadingOverlay.emit(true);

    if(this._environment=='customer'){
      param=this._API.API_Customer+'/order-flow/'+this._cusId;
    }else if(this._environment=='order'){
      if(!this._orderId){
        param=this._API.API_Customer+'/order-flow/'+this._cusId;
      }else{
        param=this._API.API_ORDERS+'/order-flows/'+this._cusId + "?orderid=" + this._orderId;
      }
    }

    this._ODFSerivce.getOrderFlow(param).subscribe(
      data => {
        if (this._orderId == '') {
          var newData = data[0].value.map((item, index) => {
            var newObject = {
              odr_flow_id: item.odr_flow_id,
              odr_sts: item.odr_sts,
              type: item.type,
              description: item.description,
              flow_code: item.flow_code,
              dependency: item.dependency,
              name: item.name,
              step: item.step,
            };
            data.forEach((itemData, indexData) => {
              switch (data[indexData].qualifier) {
                case 'OFF':
                  newObject['bulk'] = {
                    qualifier:data[indexData].qualifier,
                    value: data[indexData].value[index].usage
                  }
                break;
                case 'OSF':
                  newObject['usage'] = {
                    qualifier:data[indexData].qualifier,
                    value: data[indexData].value[index].usage
                  }
                break;
                case 'OEF':
                  newObject['ecom'] = {
                    qualifier:data[indexData].qualifier,
                    value: data[indexData].value[index].usage
                  }
                break;

                default:
                  break;
              }
            });
            return newObject;
          });
          this.orderFlowDetail['value'] = newData;
        } else {
          this.orderFlowDetail['value'] = data.data;
        }

        this._checkRelationship();
        this.emitData();
      },
      err => {
        // this.showLoadingOverlay.emit(false);
        this.parseError(err);
      },
      () => {
        // this.showLoadingOverlay.emit(false);
      }
    );

  }



  /*=========================
  * Return data every changed value
  *======================== */
  private changeCheckedOrderFlow($event, item, typeItem) {
    if (!this._orderId) {
      item[typeItem].value = $event.target.checked ? 1 : 0;
    } else {
      item[typeItem] = $event.target.checked ? 1 : 0;
    }

    if (item.flow_code === 'PAM') {
      const type = this._orderType === 'BU' ? 'bulk' : 'usage';
      const isCheck = !this._orderId ? item[type].value === 1 : item.usage === 1;
      if (isCheck) {
        this.orderFlowDetail['value'].forEach(step => {
          if (this._isValidFlowCode(step.flow_code)) {
            if (!this._orderId) {
              step[type].value = 1;
            } else {
              step.usage = 1;
            }
          }
        });
      }

      this._disabledShippingArea(isCheck);
    }

    if (item.flow_code === 'ASS') {
      const type = this._orderType === 'BU' ? 'bulk' : 'usage';
      const isCheck = !this._orderId ? item[type].value === 1 : item.usage === 1;
      const checkValue = isCheck ? 1 : 0;

      const itemARS = this.orderFlowDetail['value'].find(step => step['flow_code'] === 'ARS');
      if (!this._orderId) {
        itemARS[type].value = checkValue
      } else {
        itemARS.usage = checkValue;
      }
    }

    this.emitData();
  }

  private emitData() {
    let outputOrderFlow = JSON.parse(JSON.stringify(this.orderFlowDetail['value']));
    if (this._orderId == '') {
      outputOrderFlow.forEach(el => {
        if (this._orderType === 'RTL') {
          el.usage = el.usage.value;
        } else {
          el.usage = el.bulk.value;
        }
        delete el.ecom;
        delete el.bulk;
      });
    }
    this.orderFlowReturnData.emit(outputOrderFlow);
  }

  private parseError(err){
    this.messages.emit({'status' : 'danger', 'txt' : this._Func.parseErrorMessageFromServer(err)});
  }

  private _disabledShippingArea(disabled: boolean = false) {
    this.orderFlowDetail['value'].forEach(step => {
      if (this._isValidFlowCode(step.flow_code)) {
        step.disabled = disabled;
      } else {
        step.disabled = false;
      }
      const type = this._orderType === 'BU' ? 'bulk' : 'usage';
      if (type == 'usage') {
        const arrayRTLDisable = ['CSR'];
        const defaultCheckRTL = ['CSR','BOL'];
        if (arrayRTLDisable.indexOf(step['flow_code']) > -1) {
          step.disabled = true;
        }
        if (defaultCheckRTL.indexOf(step['flow_code']) > -1) {
          if (!this._orderId) {
            step[type].value = 1;
          } else {
            step.usage = 1;
          }
        }
      }
      if (type == 'bulk') {
        const arrayRTLDisable = [];
        const defaultCheckRTL = ['BOL'];
        if (arrayRTLDisable.indexOf(step['flow_code']) > -1) {
          step.disabled = true;
        }
        if (defaultCheckRTL.indexOf(step['flow_code']) > -1) {
          if (!this._orderId) {
            step[type].value = 1;
          } else {
            step.usage = 1;
          }
        }
      }
    });
  }

  _isValidFlowCode(flowCode): boolean {
    const validFlowCode = this._orderType === 'RTL' ? ['BOL'] : ['ARS'];
    return validFlowCode.indexOf(flowCode) !== -1;
  }

  _checkRelationship() {
    this.orderFlowDetail['value'].forEach(odrFlow => {
      const type = this._orderType === 'BU' ? 'bulk' : 'usage';
      const usageValue = this._orderId ? odrFlow['usage'] : odrFlow[type].value;
      if (odrFlow['flow_code'] === 'PAM') {
        this._disabledShippingArea(usageValue === 1);
      }
    });
  }

}
