import {Component} from '@angular/core';
import {Router, RouterLink, RouteConfig, ROUTER_DIRECTIVES} from '@angular/router-deprecated';
import {ChargeCodeListComponent} from "./charge-code-list/charge-code-list.component";
import {CruChargeCodeComponent} from "./cru-charge-code/cru-charge-code.component";

@Component ({
    selector: 'charge-code',
    directives: [ROUTER_DIRECTIVES],
    template: `<router-outlet></router-outlet>`,
})

@RouteConfig([
    { path: '/', component:ChargeCodeListComponent , name: 'Charge Code List', useAsDefault: true},
    { path: '/new', component:CruChargeCodeComponent  , name: 'Add charge Code ', data: {action: 'new'}},
    { path: ':id', component:CruChargeCodeComponent  , name: 'View Charge Code',data: {action: 'view'}},
    { path: ':id/edit', component:CruChargeCodeComponent  , name: 'Edit Charge Code', data: {action: 'edit'}},
])
export class ChargeCodeComponent {


}
