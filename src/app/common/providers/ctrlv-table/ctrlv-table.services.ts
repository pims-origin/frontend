// ans tool service
import {Injectable, ElementRef, EventEmitter} from '@angular/core';
import { ControlGroup, ControlArray, Control} from '@angular/common';
import {CTRVTableFunctions} from "./ctrlv-table.functions";
import {FormBuilderFunctions} from "../../core/formbuilder.functions";
interface Window {
    clipboardData: any;
}
@Injectable()
export class CTRLVServices {

    constructor(private func:CTRVTableFunctions,
    private fbdFun:FormBuilderFunctions
    ){}

    public executeOnTempElement(
        callbackNow: (element: HTMLTextAreaElement)=>void,
        callbackAfter?: (element: HTMLTextAreaElement)=>void): void {

        var eTempInput = <HTMLTextAreaElement> document.createElement('textarea');
        eTempInput.style.width = '1px';
        eTempInput.style.height = '1px';
        eTempInput.style.top = '0px';
        eTempInput.style.left = '0px';
        eTempInput.style.position = 'absolute';
        eTempInput.style.opacity = '0.0';

        document.body.appendChild(eTempInput);

        try {
            var result = callbackNow(eTempInput);
        } catch (err) {
        }

        if (callbackAfter) {
            setTimeout( ()=> {
                callbackAfter(eTempInput);
                document.body.removeChild(eTempInput);
            }, 0);
        } else {
            document.body.removeChild(eTempInput);
        }
    }

    // From http://stackoverflow.com/questions/1293147/javascript-code-to-parse-csv-data
    // This will parse a delimited string into an array of
    // arrays. The default delimiter is the comma, but this
    // can be overriden in the second argument.
    public dataToArray(strData: string): string[][] {
        var strDelimiter = '\t';

        // Create a regular expression to parse the CSV values.
        var objPattern = new RegExp(
            (
                // Delimiters.+-----------------------------------------------------------------+
                "(\\" + strDelimiter + "|\\r?\\n|\\r|^)" +
                // Quoted fields.
                "(?:\"([^\"]*(?:\"\"[^\"]*)*)\"|" +
                // Standard fields.
                "([^\"\\" + strDelimiter + "\\r\\n]*))"
            ),
            "gi"
        );

        // Create an array to hold our data. Give the array
        // a default empty first row.
        var arrData: string[][] = [[]];

        // Create an array to hold our individual pattern
        // matching groups.
        var arrMatches: string[] = null;

        // Keep looping over the regular expression matches
        // until we can no longer find a match.
        while (arrMatches = objPattern.exec( strData )){

            // Get the delimiter that was found.
            var strMatchedDelimiter = arrMatches[ 1 ];

            // Check to see if the given delimiter has a length
            // (is not the start of string) and if it matches
            // field delimiter. If id does not, then we know
            // that this delimiter is a row delimiter.
            if (
                strMatchedDelimiter.length &&
                strMatchedDelimiter !== strDelimiter
            ) {

                // Since we have reached a new row of data,
                // add an empty row to our data array.
                arrData.push( [] );

            }

            var strMatchedValue: string;

            // Now that we have our delimiter out of the way,
            // let's check to see which kind of value we
            // captured (quoted or unquoted).
            if (arrMatches[ 2 ]){

                // We found a quoted value. When we capture
                // this value, unescape any double quotes.
                strMatchedValue = arrMatches[ 2 ].replace(
                    new RegExp( "\"\"", "g" ),
                    "\""
                );

            } else {

                // We found a non-quoted value.
                strMatchedValue = arrMatches[ 3 ];

            }


            // Now that we have our value string, let's add
            // it to the data array.
            arrData[ arrData.length - 1 ].push( strMatchedValue );
        }

        // Return the parsed data.
        return arrData;
    }

    public pasteData(controlArray:ControlArray,_row,control,data,__callback){

        let cellCurrent=this.func.getPositionControl(controlArray,control,_row);
        let wightTable=Object.keys(controlArray.controls).length;
        data.pop();
        for(let row in data)
        {
            let _cellTemp=cellCurrent;
            if(typeof controlArray.controls[_row] == 'undefined') {
                __callback;
            }
            for(let cel in data[row]) {
                let control=this.func.getControlNameByPositon(controlArray.controls[_row]['controls'],_cellTemp);
                if(control) {
                    this.fbdFun.setValueControlItem(controlArray,_row,control,data[row][cel]);
                }
                _cellTemp++;
            }
            _row++;
        }

    }

}
