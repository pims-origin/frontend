import {Component} from '@angular/core';
import {Router, RouterLink, RouteConfig, ROUTER_DIRECTIVES} from '@angular/router-deprecated'
import {ANS_CRU_Component} from "./asn-cru/asn-cru";
import {ASNViewComponent} from "./asn-view/view-asn.component";
import {ASNListComponent} from "./asn-list/asn-list.component";
@Component ({
    selector: 'inbound-asn',
    directives: [ROUTER_DIRECTIVES],
    template: `<router-outlet></router-outlet>`,
})

@RouteConfig([
    { path: '/', component: ASNListComponent , name: 'ASN List', useAsDefault: true },
    { path: '/new', component: ANS_CRU_Component , name: 'Create ASN', data:{action:'new'}},
    { path: '/:id/:ctn_id', component: ASNViewComponent , name: 'View ASN', data:{action:'view'}},
    { path: '/:id/:ctn_id/edit', component: ANS_CRU_Component , name: 'Edit ASN', data:{action:'edit'}}
])
export class InboundASNComponent {

}
