/*==================
 Load.ts
 ===================*/
'use strict';

import {CTRVTableFunctions} from './ctrlv-table.functions';
import {CTRLVServices} from './ctrlv-table.services';
export * from './ctrlv-table.functions';
export * from './ctrlv-table.services';

export const CTRLVTABLE_PROVIDERS = [CTRVTableFunctions, CTRLVServices]; /* load service*/
//export const SF_DIRECTIVES: any[] = [];