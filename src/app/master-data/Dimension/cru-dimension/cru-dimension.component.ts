import {Component} from '@angular/core';
import {API_Config, UserService, Functions} from '../../../common/core/load';
import {FormBuilder,ControlGroup,Validators} from '@angular/common';
import { ValidationService } from '../../../common/core/validator';
import {Router, RouteData, RouteParams } from '@angular/router-deprecated';
import {DimensionServices} from "../dimension-services";
import {BoxPopupService} from "../../../common/popup/box-popup.service";
import {Http} from '@angular/http';
import {GoBackComponent} from "../../../common/component/goBack.component";
import {WMSBreadcrumb,WMSMessages} from '../../../common/directives/directives';

declare var jQuery: any;


@Component ({
    selector: 'cru-dimension',
    providers: [ValidationService,UserService,DimensionServices],
    directives:[WMSBreadcrumb,GoBackComponent,WMSMessages],
    templateUrl: 'cru-dimension.component.html',

})

export class CruDimensionComponent {

    DimensionForm: ControlGroup;
    private showLoadingOverlay=false;
    private Action;
    private IsView=false;
    private TitlePage="Add Dimension";
    private messages;
    private pack_ref_id;
    private DimensionDetail;
    private submitForm=false;
    private packTypeList:any = [];


    constructor(
        private _user:UserService,
        private _router: Router,
        private fb: FormBuilder,
        private _Valid: ValidationService,
        private _Func: Functions,
        private params: RouteParams,
        private _dmsService: DimensionServices,
        _RTAction: RouteData,
        private _http: Http,
        private API:API_Config,
        private _boxPopupService:BoxPopupService
    ){
        this.Action=_RTAction.get('action');
        this.checkPermission();
        this.getPackType();
    }

    // Check permission for user using this function page
    private createDimension;
    private viewDimension;
    private editDimension;
    private allowAccess:boolean=false;
    private authHeader = this._Func.AuthHeader();
    private checkPermission() {
        this.showLoadingOverlay = true;
        this._user.GetPermissionUser().subscribe(
            data => {


                this.showLoadingOverlay = false;
                this.createDimension = this._user.RequestPermission(data, 'createCartonDimension');
                this.viewDimension = this._user.RequestPermission(data,'viewCartonDimension');
                this.editDimension=this._user.RequestPermission(data,'editCartonDimension');

                if(this.Action=='view')
                {
                    if(this.viewDimension) {
                        this.IsView = true;
                        this.TitlePage = 'View Dimension';
                        this.allowAccess=true;
                    }
                    else{
                        this.redirectDeny();
                    }
                }

                // Action new
                if(this.Action=='new')
                {
                    if(this.createDimension) {
                        this.allowAccess=true;
                        this.TitlePage = 'Add Dimension';
                        this.formBuilder();
                    }
                    else{
                        this.redirectDeny();
                    }
                }

                // load detail / init data form
                if(this.Action=='edit'||this.Action=='view')
                {
                    this.getDimensionDetail();
                }

                /*
                 * Action router Edit
                 * */
                if(this.Action=='edit')
                {
                    if(this.editDimension) {
                        this.allowAccess=true;
                        this.TitlePage = 'Edit Dimension';
                    }
                    else{
                        this.redirectDeny();
                    }
                }

            },
            err => {
                this.parseError(err);
                this.showLoadingOverlay = false;
            }
        );
    }
    private redirectDeny(){
        this._router.parent.navigateByUrl('/deny');
    }
    private formBuilder(data={}){

        this.DimensionForm =this.fb.group({
            length: [data['length'],Validators.compose([Validators.required, this._Valid.validateSpace, this._Valid.isDecimalNumber, this._Valid.isZero])],
            width: [data['width'],Validators.compose([Validators.required, this._Valid.validateSpace, this._Valid.isDecimalNumber, this._Valid.isZero])],
            height: [data['height'],Validators.compose([Validators.required, this._Valid.validateSpace, this._Valid.isDecimalNumber, this._Valid.isZero])],
            dimension: [data['dimension']],
            pack_type: [data['pack_type'] ? data['pack_type'] : '' ,Validators.compose([Validators.required, this._Valid.validateSpace])]
        });

    }
    private parseError (err) {
        this.messages = {'status' : 'danger', 'txt' : this._Func.parseErrorMessageFromServer(err)};
    }
    private redirectToList(){
        this._router.navigateByUrl('/master-data/dimension');
    }
    getDimensionDetail(){

        this.pack_ref_id=this.params.get('id');
        this._dmsService.getDimesnionDetail(this.pack_ref_id).subscribe(
            data => {
                this.formBuilder(data);
                this.DimensionDetail=data;
            },
            err => {
                this.parseError(err);
            },
            () => {}
        );
    }
    CreateDimension(data){
        this.showLoadingOverlay = true;
        this._dmsService.createDimesion(data).subscribe(
            data => {
                this.showLoadingOverlay = false;
                //Reset Form
                this.messages = this._Func.Messages('success', this._Func.msg('VR107'));
                setTimeout(()=>{
                    this.redirectToList();
                }, 600);
            },
            err => {
                this.showLoadingOverlay= false;
                this.parseError(err);
            },
            () => {}
        );
    }
    updateDimension(data){
        this.showLoadingOverlay=true;
        this._dmsService.updateDimension(data,this.pack_ref_id).subscribe(
            data=>{
                this.showLoadingOverlay=false;
                this.messages = this._Func.Messages('success', this._Func.msg('VR109'));
                setTimeout(()=>{
                    this.redirectToList()
                }, 600);
            },
            err=>{
                this.showLoadingOverlay=false;
                this.parseError(err);
            },
            () => {}
        );
    }
    private getPackType(){
        let params = "";
        this._http.get(this.API.API_Packtype+ params, {headers: this.authHeader
        })
            .map(res => res.json())
            .subscribe(
                data => {
                    this.packTypeList = data['pack_types'];
                },
                err => {
                },
                () => {}
            );
    }
    Save(){

        this.submitForm=true;

        // empty messages
        this.messages=false;

        // scroll to top
        jQuery('html, body').animate({
            scrollTop: 0
        }, 700);

        if(this.DimensionForm.valid){
            let data=this.DimensionForm.value;
            // check by order , if no by order -> remove order number

            let data_json = JSON.stringify(data);

            if (this.Action == 'new') {
                // add new
                this.CreateDimension(data_json);
            }
            else {
            //    update
                this.updateDimension(data_json);
            }

        }
        else{
        }

    }
    private cancel()
    {
        let n = this;
        this._boxPopupService.showWarningPopup()
            .then(function (dm) {
                if(dm)
                    n.redirectToList();
            });
    }

    checkInputNumber(evt)
    {
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 46){
            evt.preventDefault();
        }
    }

}