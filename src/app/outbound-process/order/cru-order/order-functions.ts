/*
 * @tien.nguyen
 * */
import {Component,Injectable} from '@angular/core';
import { ControlGroup, ControlArray, Control} from '@angular/common';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import {Functions,FormBuilderFunctions} from '../../../common/core/load';
import set = Reflect.set;
declare var jQuery: any;
@Injectable()
export class OrderFunctions
{
    uomList:Array<any>=[];
    constructor(private _globalFun:Functions,
                private fbdFunc:FormBuilderFunctions ) {}


    public changeRequestedQuantity(controlArray:ControlArray,row) {

        let pack=this.fbdFunc.getValueControlItem(controlArray,row,'pack');
        let piece_qty=this.fbdFunc.getValueControlItem(controlArray,row,'piece_qty');
        if (pack && piece_qty) {
            if (( piece_qty && pack ) > 0) {
                let cartonsNumber =parseInt(piece_qty) / parseInt(pack);
                if (cartonsNumber % 1 != 0) {
                    setTimeout(() => {
                        this.fbdFunc.setValueControlItem(controlArray,row,'qty',cartonsNumber.toFixed(2));
                        this.fbdFunc.setErrorsControlItem(controlArray,row,'qty',{errorNumber: true});
                    }, 100)
                }
                else {
                    this.fbdFunc.setValueControlItem(controlArray,row,'qty',cartonsNumber);
                }
            }
        }
    }

    /*==================================
     * Check Key exist
     *================================= */
    public checkKeyExist(controlArray:ControlArray, row, str:string, searchdata_array:Array<any>, fieldname, orderType) {
        let string = str;
        for (let i in searchdata_array) {
            let item = searchdata_array[i];
            if (string == this._globalFun.trim(item[fieldname])) {
                this.selectedItem(controlArray,row, item, orderType);
                return;
            }
        }
        this.fbdFunc.setErrorsControlItem(controlArray, row, fieldname, {invaildValue: true});
    }


    /*========================
    *
    *============================ */
    findKeyInList(Array:Array<any>=[],filename:any,key:any){


        for(let item of Array){

             if(this._globalFun.trim(item[filename]) == key)   {
                 return item;
             }

        }

        return null;

    }

    /*=================================
     * Auto select item if match
     * =================================*/
    selectedItem(controlArray:ControlArray,row, dataItem, orderType) {
        let uomCode: string = dataItem['uom_code'];
        let uomId: number = dataItem['uom_id'];

        if (orderType === 'XDK') {
            const uomCodeDefaultXDK = 'CT';
            for (let i in this.uomList) {
                if (this.uomList[i]['sys_uom_code'] === uomCodeDefaultXDK) {
                    uomId = this.uomList[i]['sys_uom_id'];
                    uomCode = this.uomList[i]['sys_uom_code'];
                    break;
                }
            }
        }

        controlArray.controls[row]['disabled'] = true;
        this.fbdFunc.setValueControlItem(controlArray,row,'itm_id',dataItem['item_id']);
        this.fbdFunc.setValueControlItem(controlArray,row,'cus_upc',dataItem['cus_upc']);
        this.fbdFunc.setValueControlItem(controlArray,row,'sku',dataItem['sku']);
        this.fbdFunc.setValueControlItem(controlArray,row,'color',dataItem['color']);
        this.fbdFunc.setValueControlItem(controlArray,row,'size',dataItem['size']);
        this.fbdFunc.setValueControlItem(controlArray,row,'avail',dataItem['avail']);
        this.fbdFunc.setValueControlItem(controlArray,row,'avail_pickable',dataItem['avail']);
        this.fbdFunc.setValueControlItem(controlArray,row,'pack',dataItem['pack']);
        this.fbdFunc.setValueControlItem(controlArray,row,'uom_id', uomId);
        this.fbdFunc.setValueControlItem(controlArray,row,'uom_level',dataItem['level_id']);
        this.fbdFunc.setValueControlItem(controlArray,row,'uom_code', uomCode);
        this.changeRequestedQuantity(controlArray,row);
        const control = (<ControlGroup>controlArray.controls[row]);
        this.computePickableQty(control);
    }

    /*===============================
    * backOrder
    *=============================== */
    public backOrder(controlArray:ControlArray,row, $event) {

        if ($event.target.checked) {
            controlArray.controls[row]['back_odr'] = 1;
            controlArray.controls[row]['checked'] = true;
        }
        else {
            controlArray.controls[row]['back_odr'] = 0;
            controlArray.controls[row]['checked'] = false;
        }

    }

    //By Nhan Truong
    public getDateTime() {
        var now = new Date();
        var year = now.getFullYear();
        var month = now.getMonth();
        var date = now.getDate().toString();
        var day = now.getDay();
        var dayNames = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
        var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        if (date.toString().length == 1) {
            var date = '0' + date;
        }
        var dateTime = dayNames[day] + ', ' + monthNames[month] + ' ' + date + ', ' + year;
        return dateTime;
    }

    /*==============================
     set_required_ship_track_id
    * =============================*/
    public set_required_ship_track_id(controlArray:ControlArray,enabled) {
        if (enabled) {
            for (let i in controlArray.controls) {
                this.fbdFunc.setErrorsControlItem(controlArray,i,'ship_track_id',{required: true});
            }
        }
        else {
            for (let i in controlArray.controls) {
                this.fbdFunc.setErrorsControlItem(controlArray,i,'ship_track_id',null);
            }
        }
    }

    public caclulatorPieceCtn(item:ControlGroup,uom_level:Array<any>=[], items: ControlGroup[] = []){

        this.changeUOM(item,uom_level);
        let uom_qty = item.value['uom_qty'] ? parseInt(item.value['uom_qty'], 10) : item.value['uom_qty'];
        this.fbdFunc.setValueControl(item,'uom_qty', uom_qty);
        this.computePickableQty(item);
        // fix show NaN Requested QTY
        if(!item.value['uom_qty']){
            return;
        }

        setTimeout(()=>{

            if(item.value['uom_level']==1){

                //piece_ttl = qty; ctn_ttl = 0
                this.fbdFunc.setValueControl(item,'piece_qty',item.value['uom_qty']);
                this.fbdFunc.setValueControl(item,'qty',0);

            }

            if(item.value['uom_level']==2){

                /*if(item.value['uom_qty']<item.value['pack']){
                 this.fbdFunc.setErrorControl(item,'uom_qty',{'smaller_than_pack':true});
                 return;
                 }*/

                //piece_ttl = qty; ctn_ttl = qty / pack_size
                let qty = parseFloat(item.value['uom_qty'])/parseFloat(item.value['pack']);
                this.fbdFunc.setValueControl(item,'qty',Math.ceil(qty));
                this.fbdFunc.setValueControl(item,'piece_qty',item.value['uom_qty']);

            }

            if(item.value['uom_level']==3){
                //ctn_ttl = qty, piece_ttl = qty * pack_size
                this.fbdFunc.setValueControl(item,'qty',item.value['uom_qty']);
                this.fbdFunc.setValueControl(item,'piece_qty',parseFloat(item.value['uom_qty'])*parseFloat(item.value['pack']));
            }

            this.calTotalPiece(items);

        });
    }

    public changeCTNS(controlArray:ControlArray,row,$event:any){

        let pack=this.fbdFunc.getValueControlItem(controlArray,row,'pack');
        let piece_qty:number;
        if($event.target.value&&pack){
            piece_qty=parseInt($event.target.value)*parseInt(pack);
            this.fbdFunc.setValueControlItem(controlArray,row,'piece_qty',piece_qty);
        }

    }

    public changeUOM(item:ControlGroup,uom_level:Array<any>=[]){
        for( let uom of uom_level){
            if(uom['sys_uom_id']==item.value['uom_id']){
                this.fbdFunc.setValueControl(item,'uom_level',uom['level_id']);
            }
        }
    }



    public  exceed9999(controlArray:ControlArray){

        // reset error
        this.fbdFunc.removeErrorsByFieldName(controlArray, ['qty'], 'exceed9999');

            let totalCarton = 0;

            // calculator the total cartons
            for (let i in controlArray.value) {
                totalCarton = totalCarton + parseInt(controlArray.value[i]['qty']);
            }

            if (totalCarton > 99999) {
                for (let i in controlArray.value) {
                    if (this.fbdFunc.getValidControlItem(controlArray,i,'qty')) {
                        this.fbdFunc.setErrorsControlItem(controlArray, i, 'qty', {exceed9999: true})
                    }
                }

            }


    }

    public computePickableQty(item:ControlGroup){

        item['disabled_uom']=false;
        
        if(item.value['uom_level']==4){
            item['disabled_uom']=true;
        }

        if(item.value['uom_level']==3 || item.value['uom_level']==4){
            let new_pickableqty = parseFloat(item.value['avail_pickable'])/parseFloat(item.value['pack']);
            if(new_pickableqty){
                this.fbdFunc.setValueControl(item,'avail',Math.floor(new_pickableqty));
            }
        }else{
            this.fbdFunc.setValueControl(item,'avail',item.value['avail_pickable']);
        }

    }

    public calTotalPiece(items:ControlGroup[] = []) {

        var tmp = 0;
        for (var i = 0; i < items.length; i++) {

            if (items[i]['value']['piece_qty'] != '' && items[i]['value']['piece_qty'] != null) {

                tmp += parseInt(items[i]['value']['piece_qty']);
            }
        }

        this.fbdFunc.setValueControl(items[0],'total_piece_qty',tmp);
        items[0]['value']['total_piece_qty'] = tmp;
    }

}
