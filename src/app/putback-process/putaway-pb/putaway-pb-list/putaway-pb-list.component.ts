import {Component} from "@angular/core";
import {CruPaPbListComponent} from "../cru-pa-pb-list.component";

@Component ({
    selector: 'putaway-pb-list',
    directives: [CruPaPbListComponent],
    templateUrl: 'putaway-pb-list.component.html',
})

export class PutawayPbListComponent{
    constructor(){

    }
}