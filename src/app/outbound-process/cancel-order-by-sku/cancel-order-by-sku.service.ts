import {Component, Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {API_Config, Functions} from '../../common/core/load';

@Injectable()
export class CancalOrderBySKUServices {

    public AuthHeader = this._Func.AuthHeader();
    public AuthHeaderPost = this._Func.AuthHeaderPost();

    constructor(private _Func: Functions, private API:API_Config,private _API: API_Config, private http: Http) {

    }


    allCateOrder(id, stringData:any,type:any){

        if(type=='XDK'){
            return this.http.put(this.API.API_ORDERS+'/xdoc-allocates/'+id, stringData, { headers: this._Func.AuthHeaderPostJson() })
                .map((res: Response) => res.json().data);
        }else{
            return this.http.put(this._API.API_ALLOCATE+'/'+id, stringData, { headers: this._Func.AuthHeaderPostJson() })
                .map((res: Response) => res.json().data);
        }
    }


    getOrderDetail(orderId){
        return this.http.get(this._API.API_ORDERS+'/'+orderId,{ headers: this.AuthHeader })
            .map((res: Response) => res.json().data);
    }

    cancelOrder(listOrder) {
        return this.http.get(this._API.API_ORDERS+'/cancel?odr_id=' +listOrder, { headers: this.AuthHeader })
            .map((res: Response) => res.json());
    }

    cancelOrderBySKU(cancelInfoObj) {
        return this.http.post(`${this._API.API_ORDERS}/cancel-order-details`, cancelInfoObj,
            { headers: this._Func.AuthHeaderPostJson() }).map((res: Response) => res.json());
    }

}
