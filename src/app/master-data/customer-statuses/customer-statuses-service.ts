import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {API_Config, Functions} from '../../common/core/load';

@Injectable()

export class CustomerStatusesService {

    public AuthHeader = this._Func.AuthHeader();
    public AuthHeaderPost = this._Func.AuthHeaderPost();
    public url = this._API.API_Customer_Statuses;

    constructor(
        private _Func: Functions,
        private _API: API_Config,
        private http: Http
    )
    {

    }

    //**************************************************************************

    getDropdown()
    {
        let params = '?sort[chg_type_name]=asc';

        return this.http.get(this.url + '/dropdown' + params, {
            headers: this.AuthHeader
        })
        .map((res: Response) => res.json());
    }

    //**************************************************************************

}