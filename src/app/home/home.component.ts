import {Component, OnDestroy} from '@angular/core';
import { CORE_DIRECTIVES } from '@angular/common';
import {RouteConfig, RouterLink, Router, ROUTER_DIRECTIVES} from '@angular/router-deprecated';
// import { AuthHttp } from 'angular2-jwt/angular2-jwt';
// import { Http, Headers } from '@angular/http';
import {HeaderComponent} from "../layout/header/header.component";
import {MainMenuComponent} from "../layout/main-menu/main-menu.component";
import {MainBodyComponent} from "../layout/main-body/main-body.component";
import {FooterComponent} from "../layout/footer/footer.component";
import {MenuConfigurationComponent} from "../menu-configuration/menu-configuration.component";
import {DashboardComponent} from "../dashboard/dashboard.component";
import {UserManagementComponent} from "../user-management/user-management.component";
import {UserProfileComponent} from "../user-profile/user-profile.component";
import {DenyComponent} from "../common/deny/deny.component";
import {PageNotFoundComponent} from "../common/page404/page404.component";
import {RoleManagementComponent} from "../role-management/role-management.component";
import {SystemConfigurationComponent} from "../system-configuration/system-configuration.component";
import {SystemSetupComponent} from "../system-setup/system-setup.component";
import {MasterDataComponent} from "../master-data/master-data.component";
import {GoodsReceiptComponent} from "../goods-receipt/goods-receipt.component";
import {PutAwayComponent} from "../putaway/putaway";
import {RelocationComponent} from "../relocation/relocation.component";
import {ConsolidationComponent} from "../consolidation/consolidation.component";
import {Functions, API_Config} from "../common/core/load";
import {UserService} from "../common/users/users.service";
import {BoxPopupService} from "../common/popup/box-popup.service";
import {EventTrackingComponent} from "../inbound-events-tracking/event-tracking.component";
import {OutboundProcessComponent} from "../outbound-process/outbound-process.component";
import {InboundComponent} from "../inbound/inbound.component";
import { Http, Headers, HTTP_BINDINGS } from '@angular/http';
import {WMSMessages} from "../common/directives/messages/messages";
import { FORM_DIRECTIVES,ControlGroup,FormBuilder ,Validators} from '@angular/common';
import { ValidationService } from '../common/core/validator';
import {ReportsComponent} from "../reports/reports.routes";
import {InvoicesManagementComponent} from "../invoice/invoice.component";
import {CycleCountManagementComponent} from "../cycle-count/cycle-count.component";
import {XferComponent} from "../xfer/xfer";
import {EditCartonComponent} from "../utilities/edit-carton/edit-carton.component";
import {UtilitiesComponent} from "../utilities/utilities";
import {BlockStockManagementComponent} from "../block-stock/block-stock.component";
import {PerformanceReportComponent} from "../performance-report/performance-report.component";
import {PutAwayAssignmentComponent} from "../putaway-assignment/putaway-assignment";
import {ShippingReportComponent} from "../new-report/shipping-report/shipping-report.component";
import {OrderReportComponent} from "../new-report/order-report/order-report.component";
import {InventoryReportComponent} from "../new-report/inventory-report/inventory-report.component";
import {ASN_REPORT_Component} from "../new-report/asn-report/asn-report.component";
import {GRReportComponent} from "../new-report/gr-report/gr-report.component";
import {CartonReportComponent} from "../new-report/carton-report/carton-report.component";
import {ContainerPlug_REPORT_Component} from "../new-report/container-plug-report/container-plug-report.component";
import {DamagedCartonReportComponent} from "../new-report/damaged-carton-report/damaged-carton-report.component";
import {ContainerPlugComponent} from "../container-plug/container-plug.component";
import {ANSTool} from "../asn-tool/asn-tool";
import {PutbackProcessComponent} from "../putback-process/pb-process.component";
import {EDIComponent} from "../edi/edi.component";
import {WapManagementComponent} from "../wap/wap.component";
import {PutBackReportComponent} from "../new-report/put-back-report/put-back-report.component";
import {CycleCountNotifyManagementComponent} from "../cc-notification/cc-notify.component";
import {ItemMigrationComponent} from "../migration/item/item-migration.component";
import {InventoryMigrationComponent} from "../migration/inventory/inventory-migration.component";
import {CustomerMigrationComponent} from "../migration/customer/customer-migration.component";
import {PLReportComponent} from "../new-report/pallet-report/pallet-report.component";
import {ASNMigrationComponent} from "../migration/asn/asn-migration.component";
import {CycleCountHistoryReportComponent} from "../new-report/cycle-count-history-report/cycle-count-history-report.component";
import {WarningBoxPopup} from "../common/popup/warning-box-popup";
import {OrderMigrationComponent} from "../migration/order/order-migration.component";
import {TransferPalletComponent} from "../transfer-pallet/transfer-pallet.component";
import {DailyInventoryReportComponent} from "../new-report/daily-inventory-report/daily-inventory-report.component";
import { DataShareService } from "../common/services/data-share.service";
import {RoleComponent} from "../role/role.component";
import {SkuReportComponent} from "../new-report/sku-report/sku-report.component";
import {ForecastMigrationComponent} from "../migration/forecast/forecast-migration.component";
import {ForecastReportComponent} from "../new-report/forecast-report/forecast-report.component";
import {SKUTrackingReportComponent} from "../new-report/sku-tracking-report/sku-tracking-report.component";
import {GateManagementComponent} from "../gate-management/gate-management.component";
import {GateManagement_REPORT_Component} from "../new-report/gate-management-report/gate-management-report.component";
import { LPNReportComponent } from '../new-report/lpn-report/lpn-report.component';
import { StorageReportComponent } from '../new-report/storage-report/storage-report.component';
import { LaborTrackingReportComponent } from '../new-report/labor-tracking-report/labor-tracking-report.component';

declare var jQuery: any;
declare var swal: any;

@Component({
  selector: 'home',
  directives: [CORE_DIRECTIVES ,ROUTER_DIRECTIVES, HeaderComponent, MainMenuComponent, MainBodyComponent, FooterComponent,WMSMessages, UtilitiesComponent],
  templateUrl: 'home.component.html',
  providers : [BoxPopupService, ValidationService, FormBuilder, DataShareService]
})

@RouteConfig([
  { path: '', redirectTo: ['Dashboard'] },
  { path: '/dashboard', component: DashboardComponent, name: 'Dashboard', useAsDefault: true },
  { path: '/menu', component: MenuConfigurationComponent, name: 'Menu Configuration' },
  { path: '/users/...', component: UserManagementComponent, name: 'User Management' },
  { path: '/roles/...', component: RoleManagementComponent , name: 'Role Management' },
  { path: '/profile/...', component: UserProfileComponent, name: 'User Profile' },
  { path: '/deny', component: DenyComponent, name: 'Deny' },
  { path: '/system/...', component: SystemConfigurationComponent, name: 'System' },
  { path: '/system-setup/...', component: SystemSetupComponent, name: 'System Set up' },
  { path: '/master-data/...', component: MasterDataComponent, name: 'Master Data' },
  { path: '/goods-receipt/...', component: GoodsReceiptComponent, name: 'Goods Receipt' },
  { path: '/asn-tool/new', component: ANSTool, name: 'ASN Tool' , data:{action:'new'}},
  { path: '/asn-tool/:id/:containerID/edit', component: ANSTool, name: 'ASN Tool' , data:{action:'edit'}},
  { path: '/asn-tool/:id/:containerID', component: ANSTool, name: 'ASN Tool' , data:{action:'view',titlePage:'View Asn Detail'}},
  { path: '/putaway/...', component: PutAwayComponent, name: 'PutAway' },
  { path: '/putaway-assignment/...', component: PutAwayAssignmentComponent, name: 'PutAway Assignment' },
  { path: '/relocation', component: RelocationComponent, name: 'Relocation'},
  { path: '/consolidation', component: ConsolidationComponent, name: 'Consolidation' },
  { path: '/transfer-pallet', component: TransferPalletComponent, name: 'Tranfer Pallet' },
  { path: '/event-tracking', component: EventTrackingComponent, name: 'Event Tracking' },
  { path: '/inbound/...', component: InboundComponent, name: 'Inbound' },
  { path: '/outbound/...', component: OutboundProcessComponent, name: 'Outbound Process' },
  { path: '/reports/...', component: ReportsComponent, name: 'Reports' },
  { path: '/invoice/...', component: InvoicesManagementComponent, name: 'Invoices' },
  { path: '/cycle-count/...', component: CycleCountManagementComponent, name: 'CycleCount' },
  { path: '/block-stock/...', component: BlockStockManagementComponent, name: 'BlockStock' },
  { path: '/xfer/...', component: XferComponent, name: 'Xfer' },
  { path: '/utilities/...', component: UtilitiesComponent, name: 'Utilities'},
  { path: '/performance-report', component: PerformanceReportComponent, name: 'Performance Report'},
  { path: '/shipping-report', component: ShippingReportComponent, name: 'Shipping Report'},
  { path: '/order-report', component: OrderReportComponent, name: 'Order Report'},
  { path: '/asn-report', component: ASN_REPORT_Component, name: 'ASN Report'},
  { path: '/gr-report', component: GRReportComponent, name: 'GR Report'},
  { path: '/carton-report', component: CartonReportComponent, name: 'Carton Report'},
  { path: '/pallet-report', component: PLReportComponent, name: 'Pallet Report'},
  { path: '/lpn-report', component: LPNReportComponent, name: 'LPN Report'},
  { path: '/monthly-inventory-report', component: DailyInventoryReportComponent, name: 'Monthly Inventory Report'},
  { path: '/inventory-report', component: InventoryReportComponent, name: 'Inventory Report'},
  { path: '/container-plug-report', component: ContainerPlug_REPORT_Component, name: 'Container Plug Report'},
  { path: '/damaged-carton-report', component: DamagedCartonReportComponent, name: 'Damaged Carton Report'},
  { path: '/container-plug', component: ContainerPlugComponent, name: 'Container Plug'},
  { path: '/putback-process/...', component: PutbackProcessComponent, name: 'Putback Process'},
  { path: '/edi/...', component: EDIComponent, name: 'EDI'},
  { path: '/put-back-report', component: PutBackReportComponent, name: 'Put Back Report'},
  { path: '/cycle-count-history-report', component: CycleCountHistoryReportComponent, name: 'Cycle Count History Report'},
  { path: '/wap/...', component: WapManagementComponent, name : 'WAP'},
  { path: '/cycle-count-notify/...', component: CycleCountNotifyManagementComponent, name: 'CycleCountNotify' },
  { path: '/migration/item', component: ItemMigrationComponent, name : 'Import Item'},
  { path: '/migration/inventory', component: InventoryMigrationComponent, name : 'Import Inventory'},
  { path: '/migration/customer', component: CustomerMigrationComponent, name : 'Import Customer'},
  { path: '/migration/asn', component: ASNMigrationComponent, name : 'Import ASN'},
  { path: '/migration/order', component: OrderMigrationComponent, name : 'Import Order'},
  { path: '/role', component: RoleComponent, name : 'Role'},
  { path: '/sku-report', component: SkuReportComponent, name: 'Sku Report'},
  { path: '/migration/forecast', component: ForecastMigrationComponent, name : 'Import Forecast'},
  { path: '/forecast-report', component: ForecastReportComponent, name: 'Forecast Report'},
  { path: '/sku-tracking-report', component: SKUTrackingReportComponent, name: 'SKU Tracking Report'},
  { path: '/gate-management', component: GateManagementComponent, name: 'Gate Management'},
  { path: '/gate-management-report', component: GateManagement_REPORT_Component, name: 'Gate Management Report'},
  { path: '/storage-report', component: StorageReportComponent, name: 'Storage Report'},
  { path: '/labor-tracking-report', component: LaborTrackingReportComponent, name: 'Labor Tracking Report'},
  { path: '**',    component: PageNotFoundComponent },
])

export class Home implements OnDestroy {
  private isValidToken;
  private subscrible;
  private notCheckPath = ['/login', '/forgot-password', '/forgot-password-notice', '/reset-password'];
  private modalLogin;
  private messages;
  public loading:boolean=false;
  private authHeaderNoTK = this._func.AuthHeaderNoTK();
  private showPopupLogin = false;
  private showPopupWarning = false;
  private timeoutCheckToken = 86400000;
  private intervalCheckVersion = 120000;
  private intervalCheck;
  loginForm:ControlGroup;
  private submitForm:boolean=false;
  private bodyElm;
  private overlayLogin;
  private hasModalOpen = false;

  private toutIdleWarningTime:any;
  private toutIdleTime:any;
  private idleWarningTime = 1740000;
  private idleTime = 1800000;
  private isLoggedOut:boolean = false;
  isFirst: boolean = true;
  AppVersion = process.env.CONFIG['VERSION'];

  constructor(private router:Router, private _userService: UserService,private _boxPopupService: BoxPopupService, private _func: Functions,public http: Http,private _API: API_Config, private _Valid: ValidationService,
        fb: FormBuilder) {

    this.loginForm =fb.group({
      username: [null, Validators.compose([Validators.required])],
      password: [null,Validators.required]
    });
  }

  ngOnDestroy() {
    clearInterval(this.intervalCheck);
  }

  ngAfterViewInit() {
    this.checkVersionChange();
    this.turnOffAutocomplete();
    this.router.root.subscribe((route) => {
      if(typeof route !== 'undefined'){
        this.turnOffAutocomplete();
      }
    });
    this.intervalCheck = setInterval(() => {
      this.checkVersionChange();
    }, this.intervalCheckVersion);

    this.toutIdleWarningTime = setTimeout(() => { this.showIdleTimeWarning(); }, this.idleWarningTime);
    this.toutIdleTime = setTimeout(() => { this.showLoginPopup(); }, this.idleTime);
    this.checkIdleTimeout();

    this.modalLogin = jQuery('#modal-login').css('z-index', 1070);
    this.bodyElm = jQuery('body');
    if(jQuery('#overlay-login').length) {
      this.overlayLogin = jQuery('#overlay-login');
    }
    else {
      this.overlayLogin = jQuery('<div id="overlay-login" class="modal-backdrop fade" style="z-index: 1060;display:none;"></div>').appendTo(this.bodyElm);
    }
    var currentRoot = window.location.hash.split('?')[0].replace('#', '');
    if(this.notCheckPath.indexOf(currentRoot) == -1) {
      if(this.subscrible) {
        this.subscrible.unsubscribe();
      }
      this.subscrible = this._userService.checkAuthentication().subscribe(
        data => {
          // console.log('token ok');
          this.isValidToken = true;
          setInterval(() => {
            this.checkToken();
          }, this.timeoutCheckToken);
        },
        err => {
          // console.log('token err', err);
          this.router.parent.navigateByUrl('/login');
          setInterval(() => {
            this.checkToken();
          }, this.timeoutCheckToken);
        }
      )
    }
  }

  checkVersionChange() {
    const versionURL = `${window.location.origin}/assets/version/version.txt`;
    const that = this;
    let xhr = new XMLHttpRequest();
    xhr.onreadystatechange = () => {
      if (xhr.readyState == 4 && xhr.status == 200) {
        const curAppVersion = localStorage.getItem(that.AppVersion);
        const newAppVersion = xhr.responseText.toString().trim();

        if (that.isFirst) {
          that.isFirst = false;
          localStorage.setItem(that.AppVersion, newAppVersion);
          return;
        }

        if (!curAppVersion || newAppVersion !== curAppVersion) {
          let warningPopup = new WarningBoxPopup();
          warningPopup.text = that._func.msg('VERSION_CHANGE');
          that._boxPopupService.showWarningPopup(warningPopup)
              .then((isOK) => {
                if (!isOK) {
                  return;
                } else {
                  localStorage.setItem(that.AppVersion, newAppVersion);
                  window.location.reload(true);
                }
              });
        }
      }
    };
    xhr.open('GET', versionURL);
    xhr.send();
  }

  checkToken() {
    if(this.showPopupLogin) {return;}
      // console.log('check token');
      var currentRoot = window.location.hash.split('?')[0].replace('#', '');
      if(this.notCheckPath.indexOf(currentRoot) == -1) {
        if(this.subscrible) {
          this.subscrible.unsubscribe();
        }

        this.subscrible = this._userService.checkAuthentication().subscribe(
          data => {
            // console.log('token ok');
          },
          err => {
            currentRoot = window.location.hash.split('?')[0].replace('#', '');
            if(this.notCheckPath.indexOf(currentRoot) == -1) {
              if(!this.showPopupLogin && err.status == 401) {
                this.messages = this._func.Messages('danger', this._func.parseErrorMessageFromServer(err));
                this.showPopupLogin = true;
                // this.modalLogin.modal('show');
                this.hasModalOpen = this.bodyElm.hasClass('modal-open') ? true : false;
                this.bodyElm.addClass('modal-open').css('padding-right', 15);
                this.modalLogin.addClass('in').show();
                this.overlayLogin.addClass('in').show();
              }
            }
          }
        )
      }
  }

  login(data:any) {
      this.submitForm=true;
      if(this.loginForm.valid){
        this.messages = null;
        this.loading=true;
        var creds = "username="+data['username']+"&password="+encodeURIComponent(data['password']);
        this.http.post(this._API.API_URL_LOGIN, creds, {
            headers: this.authHeaderNoTK
          })
          .subscribe(
            data => {
              localStorage.setItem('jwt', data.json().data.token);
              localStorage.setItem('id_token', data.json().data.token);
              // this.modalLogin.modal('hide');
              if(!this.hasModalOpen) {
                this.bodyElm.removeClass('modal-open').css('padding-right', '');
              }
              this.modalLogin.removeClass('in').hide();
              this.overlayLogin.removeClass('in').hide();
              this.showPopupLogin = false;
              this.loading = false;

              // Re-initialize timer
              this.toutIdleWarningTime = setTimeout(() => { this.showIdleTimeWarning(); }, this.idleWarningTime);
              this.toutIdleTime = setTimeout(() => { this.showLoginPopup(); }, this.idleTime);
            },
            err => {
              this.loading = false;
              this.messages= this._func.Messages('danger', this._func.parseErrorMessageFromServer(err));
            },
            () => {}
          )
      }

  }

  hideModalLogin() {
    this.modalLogin.removeClass('in').hide();
    this.overlayLogin.removeClass('in').hide();
    this.showPopupLogin = false;
  }

  private checkIdleTimeout() {
    document.onload = this.resetAllTimer.bind(this);
    document.onmousemove = this.resetAllTimer.bind(this);
    document.onmousedown =  this.resetAllTimer.bind(this);
    document.ontouchstart = this.resetAllTimer.bind(this); // touchscreen presses
    document.onclick = this.resetAllTimer.bind(this);      // touchpad clicks
    document.onscroll = this.resetAllTimer.bind(this);     // scrolling with arrow keys
    document.onkeypress = this.resetAllTimer.bind(this);
  }

  private resetAllTimer() {
    if (this.isLoggedOut) {
      return;
    }

    this.resetIdleWarningTimer();
    this.resetIdleTimer();
  }

  private showIdleTimeWarning() {
    this.showPopupWarning = true;
    let popupInfo = new WarningBoxPopup();
    popupInfo.text = this._func.msg('IDLE_TIME_WARNING');
    const idleTimeWarningPopup = this._boxPopupService.showWarningPopup(popupInfo)
        .then((isOK) => {
          this.showPopupWarning = false;
          if (!isOK) {
            this.isLoggedOut = true;
            clearTimeout(this.toutIdleWarningTime);
            clearTimeout(this.toutIdleTime);
            this.clearLocalStorage();
            this.router.navigateByUrl('/login');
          } else {
            idleTimeWarningPopup.showConfirmButton = false;
            this.resetIdleWarningTimer();
            this.resetIdleTimer();
          }
        });
  }

  private showLoginPopup() {
    const currentRoot = window.location.hash.split('?')[0].replace('#', '');

    clearTimeout(this.toutIdleWarningTime);
    clearTimeout(this.toutIdleTime);
    swal.close();
    this.clearLocalStorage();

    if (this.notCheckPath.indexOf(currentRoot) == -1) {
      this.messages = this._func.Messages('danger', this._func.msg('IDLE_TIME_EXPIRED'));
      this.showPopupLogin = true;
      this.hasModalOpen = this.bodyElm.hasClass('modal-open') ? true : false;
      this.bodyElm.addClass('modal-open').css('padding-right', 15);
      this.modalLogin.addClass('in').show();
      this.overlayLogin.addClass('in').show();
    } else {
      this.router.navigateByUrl('/login');
    }
  }

  private resetIdleWarningTimer() {
    if (this.showPopupWarning || this.showPopupLogin) {
      return;
    }
    clearTimeout(this.toutIdleWarningTime);
    this.toutIdleWarningTime = setTimeout(() => { this.showIdleTimeWarning(); }, this.idleWarningTime);
  }

  private resetIdleTimer() {
    if (this.showPopupWarning || this.showPopupLogin) {
      return;
    }
    clearTimeout(this.toutIdleTime);
    this.toutIdleTime = setTimeout(() => { this.showLoginPopup(); }, this.idleTime);
  }

  private clearLocalStorage() {
    localStorage.removeItem('jwt');
    /*localStorage.removeItem('access');
    localStorage.removeItem('logedUser');*/
  }
  
  private turnOffAutocomplete() {
    setTimeout(() => {
      jQuery('input[type=text]').each(function() {
        jQuery(this).attr('autocomplete', 'off');
      });
    }, 2000);
  }
}
