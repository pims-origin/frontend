import {Component, Injectable} from '@angular/core';
import {RouteConfig, Router, RouterLink, RouteParams, RouteData, ROUTER_DIRECTIVES} from '@angular/router-deprecated';
import { Http, Headers,Response } from '@angular/http';
import {API_Config, Functions} from '../../common/core/load';
import { Observable         } from 'rxjs/Rx';
@Injectable()

export class UserServices{

  private AuthHeader = this._Func.AuthHeader();
  api_user_permissions = this._API.API_USER_Permission;
  private UserPermission :any = [];
  active: boolean;
  message: string = '';
  constructor(
    public router: Router,
    public http: Http,
    private _Func: Functions,
    private _API: API_Config
  ) {



  }

  getUserPermission() {
      return this.http.get(this.api_user_permissions, {
        headers: this.AuthHeader
      })
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);

  }

  private extractData(res: Response) {
    let body = res.json();
    return body.data || { };
  }

  private handleError (error: any) {
    // In a real world app, we might use a remote logging infrastructure
    // We'd also dig deeper into the error to get a better message
    let errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg); // log to console instead
    return Promise.reject(errMsg);
  }

}
