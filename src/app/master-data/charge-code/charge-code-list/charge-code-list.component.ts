import {Component} from '@angular/core';
import { Router } from '@angular/router-deprecated';
import {API_Config, UserService, Functions} from '../../../common/core/load';
import {ChargeCodeService} from '../charge-code-service';
import { FORM_DIRECTIVES,ControlGroup,FormBuilder, Control} from '@angular/common';
import { Http } from '@angular/http';
import {WMSBreadcrumb} from '../../../common/directives/directives';
declare var jQuery: any;
@Component({
    selector: 'charge-code-list',
    providers: [ChargeCodeService, FormBuilder],
    directives: [FORM_DIRECTIVES,WMSBreadcrumb],
    templateUrl: 'charge-code-list.component.html',
})

export class ChargeCodeListComponent {

    private chargeCodeList : Array<any> = [];
    private selectedAll = false;
    public listSelectedItem = [];
    private Pagination;
    private totalCount;
    private tmpPageCount;
    private pageCount=0;
    private ItemOnPage=0;
    private currentPage = 1;
    private perPage = 20;
    private numLinks = 3;
    private messages;
    searchForm: ControlGroup;
    public loading = [];
    public showLoadingOverlay = false;
    private hasEditPermission = false;
    private hasCreatePermission = false;
    private viewUser = false;
    private searchCriteria = {};
    constructor(
        private _http: Http,
        private _func: Functions,
        private _user: UserService,
        private fb: FormBuilder,
        private _chargeCodeService:ChargeCodeService,
        private router:Router) {

        this.builderSearchForm();
        this.searchCriteria = this._func.genSearchCriteriaFromForm(this.searchForm);
        this.checkPermission();
    }

    // Check permission for user using this function page
    private checkPermission() {
        this.showLoadingOverlay = true;
        this._user.GetPermissionUser().subscribe(
            data => {
                this.hasEditPermission = this._user.RequestPermission(data, 'editChargeCode');
                this.hasCreatePermission = this._user.RequestPermission(data, 'createChargeCode');
                this.viewUser = this._user.RequestPermission(data,'viewChargeCode');
                /* Check orther permission if View allow */
                if(!this.viewUser) {
                    this.router.parent.navigateByUrl('/deny');
                }
                else {
                    this.getChargeCodeList();
                }
            },
            err => {
                this.showMessage('danger', this._func.parseErrorMessageFromServer(err));
                this.showLoadingOverlay = false;
            }
        );
    }

    public getChargeCodeList(page = null) {
        this.showLoadingOverlay = true;
        let param = '?page='+page+'&limit='+this.perPage;
        if(Object.keys(this.searchCriteria).length !== 0) // check if searchCriteria Object not empty  ( {} )
        {
            param += '&chg_code=' + this.searchCriteria['chg_code'] +
            '&chg_type_name=' + this.searchCriteria['chg_type_name']+'&chg_uom_name=' +
            this.searchCriteria['chg_uom_name'];
        }
        if(this.sortQuery != '') {
            param += '&'+this.sortQuery;
        }
        this.loading['getChargeCode'] = true;
        this._chargeCodeService.getChargeCode(param).subscribe(
            data => {
                this.selectedAll = false;
                this.listSelectedItem = [];
                this.chargeCodeList = data.data;
                // pagin function
                this.Pagination=data['meta']['pagination'];
                this.Pagination['numLinks']=3;
                this.Pagination['tmpPageCount']=this._func.Pagination(this.Pagination);

                this.loading['getChargeCode'] = false;
                this.showLoadingOverlay = false;
            },
            err => {},
            () => {}
        );
    }

    public onPageSizeChanged($event) {
        this.perPage = $event.target.value;
        if((this.Pagination['current_page'] - 1) * this.perPage >= this.Pagination['total'] ) {
            this.Pagination['current_page'] = 1;
        }
        this.getChargeCodeList();
    }

    private filterList(num) {
        this.currentPage = num;
        this.getChargeCodeList(num);
    }

    search(data) {
        this.messages = null;
        this.listSelectedItem = [];

        let param="?page="+1+"&limit="+this.perPage;
        this.searchCriteria = this._func.genSearchCriteriaFromForm(this.searchForm);
        if(this.sortQuery != '') {
            param += '&'+this.sortQuery;
        }
        let encodeSearchQuery = param+"&chg_code="+encodeURIComponent(this.searchCriteria ['chg_code'])+"&chg_type_name=" +
            encodeURIComponent(this.searchCriteria ['chg_type_name']) + '&chg_uom_name=' +
            encodeURIComponent(this.searchCriteria ['chg_uom_name']);

        this.loading['searching'] = true;
        this.showLoadingOverlay = true;

        this._chargeCodeService.search(encodeSearchQuery).subscribe(
            data => {

                // disable loading icon
                this.loading['searching'] = false;
                this.selectedAll = false;

                this.chargeCodeList = data.data;
                //pagin function
                this.Pagination = data['meta']['pagination'];
                this.Pagination['numLinks']=3;
                this.Pagination['tmpPageCount'] = this._func.Pagination(this.Pagination);
                this.showLoadingOverlay = false;
            },
            err =>{
                this.messages = this._func.Messages('danger','Not found');
            },
            () => {}
        );
    }

    resetSearch(data) {
        // if(data.chg_code || data.chg_type_name || data.chg_uom_name) {
            (<Control>this.searchForm.controls["chg_code"]).updateValue("");
            (<Control>this.searchForm.controls["chg_type_name"]).updateValue("");
            (<Control>this.searchForm.controls["chg_uom_name"]).updateValue("");
            this.searchCriteria = this._func.genSearchCriteriaFromForm(this.searchForm);
            this.getChargeCodeList(this.Pagination['current_page']);
        // }
    }

    private builderSearchForm() {
        this.searchForm =this.fb.group({
            'chg_code': [''],
            'chg_type_name':[''],
            'chg_uom_name': ['']
        });
        // console.log('search form', this.searchForm);
    }

    private editSelectedItem() {
        if (this.listSelectedItem.length > 1) {
            this.messages = this._func.Messages('danger',this._func.msg('CF004'));
        }
        else {
            if(this.listSelectedItem.length) {
                this.router.navigateByUrl('/master-data/charge-code/' + this.listSelectedItem[0] + '/edit');
            }
            else {
                this.messages = this._func.Messages('danger',this._func.msg('CF005'));
            }
        }
    }

    private checkAll(event) {
        this.selectedAll = !this.selectedAll;
        // set empty array  ListSelectedItem
        this.listSelectedItem=[];
        // Loop
        this.chargeCodeList.forEach((item) => {
            item['selected'] = this.selectedAll;
            if(this.selectedAll) {
                // if check all = true , pushing to ListSelectedItem
                this.listSelectedItem.push(item['chg_type_id']);
            }
        })
    }

    private checkSelectItem(item) {
        let id = item['chg_code_id'];
        item.selected = !item.selected;
        this.selectedAll=false;
        if (item.selected) {
            this.listSelectedItem.push(id);
            this.setItemListSelected(id, true);
            if(this.listSelectedItem.length==this.chargeCodeList.length)
            {
                this.selectedAll=true;
            }
        }
        else {
            let el = this.listSelectedItem.indexOf(id);
            // remove item out of array
            this.listSelectedItem.splice(el, 1);
            this.setItemListSelected(id, false);
        }
    }

    private setItemListSelected(id,value) {
        this.chargeCodeList.forEach((item) => {
            if(item['chg_code_id'] == id) {
                item['selected'] = value;
            }
        });
    }

    private showMessage(msgStatus, msg) {
        this.messages = {
            'status': msgStatus,
            'txt': msg
        }
        jQuery(window).scrollTop(0);
    }

    private FieldsToSortFlag = {
        chg_code: false,
        chg_type_name: false,
        chg_uom_name: false,
    };
    private countClick = {
        chg_code: 3,
        chg_type_name: 3,
        chg_uom_name: 3,
    };
    private sortQuery = 'sort[chg_code]=asc';
    sort(field) {
        if(this.countClick[field] == 3) {
            this.countClick[field] = 0;
        }

        this.countClick[field] ++;
        if(this.countClick[field] == 3) {
            this.sortQuery = 'sort[chg_code]=asc';
        }
        else {
            this.FieldsToSortFlag[field] = !this.FieldsToSortFlag[field];

            this.sortQuery = 'sort['+field+']';
            if(this.FieldsToSortFlag[field]) {
                this.sortQuery += '=asc';
            } else {
                this.sortQuery += '=desc';
            }
        }

        if(field) {
            for (let sortField in this.FieldsToSortFlag ) {
                if(sortField != field) {
                    this.FieldsToSortFlag[sortField] = false;
                    this.countClick[sortField] = 3;
                }
            }
        }
        this.getChargeCodeList(this.currentPage);
    }


}
