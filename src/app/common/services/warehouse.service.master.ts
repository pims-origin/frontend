/*
* Only return data
* */
import {Component, Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {API_Config, Functions} from '../../common/core/load';

@Injectable()
export class WarehouseServicesMaster {

  public AuthHeader = this._Func.AuthHeader();
  public AuthHeaderPost = this._Func.AuthHeaderPost();

  constructor(private _Func: Functions, private _API: API_Config, private http: Http)
  {

  }
  /*
   * Get list warehosue
   * */
  GetWareHouseList(param)
  {
    return this.http.get(this._API.API_Warehouse+param,{ headers: this.AuthHeader })
      .map((res: Response) => res.json());
  }

  /*
   * Get a detail warehouse
   * */
  GetDetailWareHouse(id)
  {
    return this.http.get(this._API.API_Warehouse+'/'+id,{ headers: this.AuthHeader })
      .map((res: Response) => res.json());
  }




  /*
   *
   * Get Zone function
   * */
  GetZone(id)
  {
    return this.http.get(this._API.API_Warehouse+'/'+id+'/zone',{ headers: this.AuthHeader })
      .map((res: Response) => res.json().data);
  }

  /*
   *
   * Get Localtions function
   * */
  GetLocaltions(id)
  {
    return this.http.get(this._API.API_Warehouse+'/'+id+'/locations',{ headers: this.AuthHeader })
      .map((res: Response) => res.json().data);
  }


  /*
   *
   * Status
   * */
  Status()
  {
    return this.http.get(this._API.API_Warehouse + '/statuses',{ headers: this.AuthHeader })
      .map((res: Response) => res.json());
  }
}
