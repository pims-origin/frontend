import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {API_Config, Functions} from '../../../common/core/load';

@Injectable()
export class LabelingServices {
	private authHeader:any = this.funcs.AuthHeader();

	constructor(private funcs:Functions,
	            private _api:API_Config,
	            private http:Http) {}
	getLabeling($whsId, $order_id) {
		return this.http.get(`${this._api.API_ORDERS_V2}/whs/${$whsId}/order/${$order_id}/label-link`, {headers: this.authHeader})
			.map(res => res.json());
	}
}
