import {Component,Input,ElementRef,AfterViewInit} from '@angular/core';
import { FORM_DIRECTIVES,ControlGroup,FormBuilder, Control ,Validators} from '@angular/common';
import {RouteParams,Router ,RouteData} from '@angular/router-deprecated';
import {API_Config, UserService, Functions} from '../../common/core/load';
import { ValidationService } from '../../common/core/validator';
import { Http } from '@angular/http';
import { ControlMessages } from '../../common/core/control-messages';
import {GoBackComponent} from "../../common/component/goBack.component";
import {CycleCountServices} from "../cycle-count-service";
import {ErrorMessageForControlService} from "../../common/services/error-message-for-control.service";
import {BoxPopup} from "../../common/popup/box-popup";
import {WarningBoxPopup} from "../../common/popup/warning-box-popup";
import {BoxPopupService} from "../../common/popup/box-popup.service";
import {JwtHelper} from "angular2-jwt/angular2-jwt";
import {WMSBreadcrumb,WMSMessages, PaginatePipe,PopupLocations, ProGressBarDirective,PaginationControlsCmp, PaginationService} from '../../common/directives/directives';
import { OrderBy } from "../../common/pipes/order-by.pipe";
import {search} from "./locationCode.pipe";

declare var jQuery:any;

@Component({
  selector: 'create-cycle-count',
  directives: [
    FORM_DIRECTIVES,
    ControlMessages,
    GoBackComponent,
    WMSBreadcrumb,
    WMSMessages,
    PaginationControlsCmp,
    ProGressBarDirective,
    PopupLocations
  ],
  pipes: [search,PaginatePipe, OrderBy],
  providers: [
    CycleCountServices,
    ValidationService,
    FormBuilder,
    ErrorMessageForControlService,
    BoxPopupService,
    PaginationService
  ],
  templateUrl: 'create-cycle-count.component.html',
})

export class CreateCycleCountComponent {
  private listNull = {};
  private dataListCustomer = [];
  public loading = [];
  private messages;
  private action;
  private whs_id;
  private s_loading = {'location_from': false, 'location_to': false};
  public cycleCountUserList: any = [];
  private showLoadingOverlay = false;
  private isAccess = false;
  private warningMessages;
  private arrFields = [
    'cycle_name',
    'cycle_des',
    'cycle_assign_to',
    'cycle_due_date',
    'customer',
    'sku'
  ];

  private disabledloc:boolean=true;
  private locations:Array<any>=[];
  private dataLocation = [];
  private dataListSku = [];
  private dataSlectedSku = {};

  private exitsLocation = {};
  private flagShowSCL = false;

  constructor(private _http:Http,
              private _Func:Functions,
              private _user:UserService,
              private params:RouteParams,
              private _Valid:ValidationService,
              private _cycleCountService:CycleCountServices,
              private _ErrMsgControlService:ErrorMessageForControlService,
              private _boxPopupService:BoxPopupService,
              private fb:FormBuilder,
              _RTAction:RouteData,
              private _router:Router) {

      // Check permission
      this.checkPermission();

      if (localStorage.getItem('whs_id')) {

          this.whs_id = localStorage.getItem('whs_id');
      }
      this.getCycleCountUsers();
      this.getCustomersByWH();
  }

  private checkPermission() {

    this.showLoadingOverlay = true;

    this._user.GetPermissionUser().subscribe(
      data => {

        if (!this._user.RequestPermission(data, 'createCycleCount')) {

          this._router.parent.navigateByUrl('/deny');
        }
        this.showLoadingOverlay = false;
      },
      err => {

        this.messages('danger', this._Func.parseErrorMessageFromServer(err));

        this.showLoadingOverlay = false;
      }
    );
  }

  /*
   ********************************************************************************************************************
   */

  private cancel() {
    let n = this;
    this._boxPopupService.showWarningPopup()
      .then(function (dm) {
        if (dm)
          n._router.navigateByUrl('/cycle-count');

      })
  }

  /*
   ********************************************************************************************************************
   */

  private getCustomersByWH() {
    const params = `/${this.whs_id}?limit=100`;
    this._cycleCountService.getCustomersByWH(params)
      .subscribe(
        data => {
          this.dataListCustomer = data.data;
        },
        err => {
          this.parseError(err);
        },
        () => {
        }
      );
  }

  /*
   ********************************************************************************************************************
   */

  private parseError(err) {

    this.messages = {'status': 'danger', 'txt': this._Func.parseErrorMessageFromServer(err)};
  }

  /*
   ********************************************************************************************************************
   */


  // Filter form only show choose one
  private filterForm($target) {

    try {

      if ($target.value == "") {

        this.listNull[$target.id + "_required"] = true;

      } else {

        this.listNull[$target.id + "_required"] = false;
      }

    } catch (err) {

      return false;
    }
  }

  /*
   ********************************************************************************************************************
   */

  private selectedItem($event, name) {

    var id = $event.target.id.replace(name + "_", "");
    var code = jQuery($event.target).text().trim();
    this.listNull[name + '_required'] = false;
    jQuery("#ul_" + name).hide();
    jQuery("#" + name).val(code);
    jQuery("body").off("click.a" + name)

  }

  /*
   ********************************************************************************************************************
   */

  // validate date
  private assignDate($target) {

    if ($target.value != "") {

      this.listNull[$target.id + "_required"] = false;
    }

    var target = this;
    jQuery("body").off("click.myExpect").on("click.myExpect", function (e) {

      if (e.target.id !== "cycle_due_date") {
        target.filterFormDate();
      }
    });

  }

  /*
   ********************************************************************************************************************
   */

  // Filter form only show choose one
  private filterFormDate() {

    var $target = jQuery("#cycle_due_date");

    this.listNull["cycle_due_date_required"] = false;
    if ($target.val() == "") {

      this.listNull["cycle_due_date_required"] = true;

    }

  }

  /*
   ********************************************************************************************************************
   */

  // Change select radio for cycle by
  private changeCycleBy($event) {

      var id = $event.target.id;
      this.listNull['sku_required'] = false;
      this.listNull['sku_invalid'] = false;
      this.listNull['sku_select'] = false;
      this.listNull['customer_required'] = false;
      this.dataSlectedSku = {};
      this.disabledloc=true;
      this.flagShowSCL = false;
      this.locations = [];
      jQuery("#customer").prop('disabled', true);
      jQuery("#sku").prop('disabled', true);
      jQuery("#sku").val('');
      this.listNull['localtions']=false;

      if (id == 'rcustomer') {
        jQuery("#customer").prop('disabled', false);
      } else if (id == 'rsku') {

        jQuery("#customer").prop('disabled', false);
        if(jQuery("#customer").val()){
          jQuery("#sku").prop('disabled', false);
        }

     } else {

        jQuery("#customer").val('');
        this.disabledloc=false;
    }

  }

  /*
   ********************************************************************************************************************
   */

  // Save data
  private save() {

    try {

      this.messages = false;
      var tar_get = this;
      var submit = true;

      // Validate fields Top
      jQuery('#cycle_name, #cycle_assign_to, #cycle_due_date').each(function (i, object) {

        tar_get.filterForm(object);
      });

      if (jQuery("#rcustomer").is(':checked')) {

        tar_get.filterForm(jQuery("#customer")[0]);

      } else if (jQuery("#rsku").is(':checked')) {

        tar_get.filterForm(jQuery("#sku")[0]);
        tar_get.filterForm(jQuery("#customer")[0]);


      }else{

        if(!this.locations.length){
          this.listNull['locations'] = true;
        }

      }

      jQuery.each(this.listNull, function (key, value) {
        if (value) {

          submit = false;
          return false;
        }
      });



      if (! submit) {
        return false;
      }

      var paramsJson = this.buildParams();
      var params = JSON.stringify(paramsJson);



      this.showLoadingOverlay = true;

      this._cycleCountService.addNewCycleCount(params)
        .subscribe(
          data => {
            this.messages = this._Func.Messages('success', this._Func.msg('CC001'));
          },
          err => {
            this.parseError(err);
            this.showLoadingOverlay = false;
          },
          () => {
            tar_get.showLoadingOverlay = false;
            setTimeout(function(){

              tar_get._router.parent.navigateByUrl('/cycle-count');
            }, 1100);
          }
        );

    } catch (err) {

    }

  }

  /*
   ********************************************************************************************************************
   */

  // build params
  private buildParams() {

    var params = {};



    params['whs_id'] = this.whs_id;
    var tar_get = this;

    // Validate fields Top
    jQuery('#cycle_name, #cycle_assign_to, #cycle_due_date, #cycle_des').each(function (i, object) {

      params[this.id] = this.value;
    });

    if(jQuery("#rcustomer").is(':checked')) {

      params['cycle_type'] = 'CS';
      params['cycle_detail'] = jQuery("#customer").val();


    } else if (jQuery("#rsku").is(':checked')) {

      params['cycle_type'] = 'SK';
      params['cycle_detail'] = jQuery("#sku").val();
      params['items'] = [];
      var item = {item_id: this.dataSlectedSku['item_id'], lot:  jQuery("#lot").val()};
      params['items'].push(item);

    } else if (jQuery("#rlocation").is(':checked')) {

      params['cycle_type'] = 'LC';
      params['cycle_detail'] = [];
      this.locations.forEach((item)=>{
        params['cycle_detail'].push(item['loc_id']);
      });
      params['cycle_detail'] = params['cycle_detail'].join();

    }
    
    params['cycle_has_color_size'] = jQuery("#sku-with-color-size").is(":checked");
    params['cycle_count_by'] = jQuery("input[name=cycle_count_by_OUM]:checked").val();
    params['cycle_method'] = jQuery("input[name=unknown_type]:checked").val();


    return params;

  }

  /*
   ********************************************************************************************************************
   */


  private showMessage(msgStatus, msg) {

    this.messages = {
      'status': msgStatus,
      'txt': msg
    };
    jQuery(window).scrollTop(0);
  }

  /*
   ********************************************************************************************************************
   */


  private getCycleCountUsers() {

    this._cycleCountService.getCycleCountUsers()
      .subscribe(
        data => {

          this.cycleCountUserList = data.data;
        },
        err => {
          this.parseError(err);
        },
        () => {
        }
      );
  }

  private checkSameCusAssign($event) {

    var customer_id = jQuery("#customer").val();
    var cycle_assign_to_id = jQuery("#cycle_assign_to").val();

    if (customer_id == '' || cycle_assign_to_id == '') {
      this.listNull["customer_not_access"] = false;
      this.listNull["cycle_assign_to_not_access"] = false;
      return false;
    }

    var cusHtml = jQuery('#customer option[value=' + customer_id + ']').html().trim(),
        assHtml = jQuery("#cycle_assign_to option[value=" + cycle_assign_to_id + "]").html().trim();

    var params = {'cus_id': customer_id, 'user_id': cycle_assign_to_id};

    this._cycleCountService.checkSameCusAssign(JSON.stringify(params))
        .subscribe(
            data => {
              if (data.status) {
                this.listNull["customer_not_access"] = false;
                this.listNull["cycle_assign_to_not_access"] = false;
              } else {
                this.listNull[$event.target.id + "_not_access"] = true;
                this.warningMessages = assHtml + ' can not access to ' + cusHtml;
              }
            },
            err => {
              this.parseError(err);
            },
            () => {
            }
        );

  }

  private enableSku($event) {

    this.flagShowSCL = false;
    this.dataSlectedSku = {};
    this.listNull['sku_invalid'] = false;
    this.listNull['sku_required'] = false;
    jQuery("#sku").val('');
    
    jQuery("#sku").prop('disabled', true);
    if (jQuery("#rsku").is(':checked') && $event.target.value != '') {

      jQuery("#sku").prop('disabled', false);

    }
  }

  private autoCompleteSku($event) {

    if ($event.type == 'keyup') {

      this.flagShowSCL = false;
      this.dataSlectedSku = {};
      this.listNull['sku_select'] = false;
    }
    try {
      var sku = $event.target.value;

      if (sku == '') {

        return false;
      }

      var cus_id = jQuery("#customer").val();
      if (cus_id == '') {

        return false;
      }

      var params = `/?cus_id=${cus_id}&sku=${encodeURIComponent(sku)}&limit=20`;

      jQuery("#list-sku").show();
      let target = this;
      // do Search
      this._cycleCountService.getSKUAdd(params).debounceTime(400).distinctUntilChanged().subscribe(
          data => {

            this.dataListSku = data.data;
          },
          err => {

            jQuery("#list-sku").hide();

          },
          () => {

            jQuery("body").off("click.sku").on("click.sku", function (e) {

              if (e.target.id != 'sku' && e.target.id != 'list-sku') {

                jQuery("#list-sku").hide();

              }

            });
          }
      );

    } catch (err) {

      return false;
    }

  }

  // select one item sku list
  private selectedItemSku($event) {

    this.listNull['sku_required'] = false;
    this.flagShowSCL = true;
    var index = parseInt($event.target.id.replace("list-sku-", ""));
    var dataDetailSku = this.dataListSku[index];
    this.dataSlectedSku = this.dataListSku[index];

    if (this.dataSlectedSku['lot'].length > 1) {

      this.dataSlectedSku['lot'] = ['All', ...this.dataSlectedSku['lot']];
    }

    jQuery("#sku").val(dataDetailSku['sku']);
    jQuery("#lot").prop("disabled", false);

  }
  private checkInvalid ($target) {

    var that = this;

    setTimeout(function(){

      var flag = false;
      for (var i = 0; i < that.dataListSku.length; i++) {

        if ($target.value == that.dataListSku[i]['sku']) {

          flag = true;
          break;
        }
      }
      
      that.listNull[$target.id + '_invalid'] = false;
      if (!flag && $target.value != '') {

        that.listNull[$target.id + '_invalid'] = true;

      }

      that.listNull[$target.id + '_select'] = false;

      if ($target.value != '' && !that.dataSlectedSku.hasOwnProperty('item_id')) {

        that.listNull[$target.id + '_select'] = true;
      }

    }, 200);

  }

  private addLocation($event:Array<any>=[]){

    if(this.locations.length==0){
      this.locations = $event;
    }else{
      this.locations.push(...this.filteredDuplicatedItem(this.locations,$event));
    }

    if(this.locations.length){
      this.listNull['locations'] = false;
    }

  }

  private filteredDuplicatedItem(ArrayDes:Array<any>=[],array_push:Array<any>=[]){

    let arr_temp:Array<any> = [];
    for (let new_item of array_push){
      let is_existed = 0;
      for (let old_item of ArrayDes){
        if(new_item['loc_id'] == old_item['loc_id'] ){
          is_existed++;
        }
      }
      if(is_existed==0){
        arr_temp.push(new_item);
      }
    }
    return arr_temp;

  }


}
