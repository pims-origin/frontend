import {Component } from '@angular/core';
import {WMSBreadcrumb,WMSMessages} from '../../common/directives/directives';
import {GoBackComponent} from "../../common/component/goBack.component";
import {CycleCountServices} from "../cycle-count-service";
import {Functions} from "../../common/core/functions";
import {RouteParams, Router} from '@angular/router-deprecated';
import {UserService} from "../../common/users/users.service";
import {API_Config} from "../../common/core/API_Config";
declare var jQuery: any;
declare var saveAs: any;

@Component({
    selector: 'cycle-count-adjust',
    directives: [WMSBreadcrumb,GoBackComponent, WMSMessages],
    providers: [
        CycleCountServices,
    ],
    templateUrl: 'cycle-count-adjust.component.html',
})

export class CycleCountReviewComponent {

    private cycleID: any;
    private cycleDetail = {};
    private countItemList = [];
    private messages:any;
    private showLoadingOverlay = false;
    private Pagination;
    private totalCount = 0;
    private tmpPageCount;
    private pageCount=0;
    private ItemOnPage=0;
    private currentPage = 1;
    private perPage = 20;
    private numLinks = 3;
    private urlAPICC = '';
    private page_current = 1;
    private hasPrintPermission = false;
    private hasApprovePermission = false;
    private isCountedByCarton:boolean = false;
    private cycleCountListOI = {'orders': []};


    constructor(
        private _Func: Functions,
        private _router:Router,
        params: RouteParams,
        private _API_Config: API_Config,
        private _user: UserService,
        private _cycleCountServices: CycleCountServices
    ) {

        this.checkPermission();
        this.cycleID = params.get('id');
        this.getCycleCountDetail(this.cycleID);
        this.getCountItemByCycleID(1);
        this.urlAPICC = this._API_Config.API_Cycle_Count;

        var vis = (function(){
            var stateKey, eventKey, keys = {
                hidden: "visibilitychange",
                webkitHidden: "webkitvisibilitychange",
                mozHidden: "mozvisibilitychange",
                msHidden: "msvisibilitychange"
            };
            for (stateKey in keys) {
                if (stateKey in document) {
                    eventKey = keys[stateKey];
                    break;
                }
            }
            return function(c) {

                if (c) document.addEventListener(eventKey, c);
                return !document[stateKey];
            }
        })();

        var target = this;
        vis(function() {
            if (vis('')) {
                target.checkStatusReduce();
            }

        });

    }


    private checkPermission() {

        this._user.GetPermissionUser().subscribe(

            data => {

                var hasViewCC = this._user.RequestPermission(data,'viewCycleCount');
                this.hasPrintPermission = this._user.RequestPermission(data,'printCycleCount');
                this.hasApprovePermission = this._user.RequestPermission(data,'approveCycleCount');

                if(!hasViewCC) {

                    this._router.parent.navigateByUrl('/deny');
                }
            },
            err => {

                this.showMessage('danger', this._Func.parseErrorMessageFromServer(err));

            }
        );
    }

    // Get cycle detail
    private getCycleCountDetail(cycleID) {

        this._cycleCountServices.getCycleCountDetail(cycleID).subscribe(
            data => {

                this.cycleDetail = data;
                this.isCountedByCarton = this.cycleDetail['count_by'] === 'CARTON' ? true : false;

            },
            err => {

                this.showMessage('danger', this._Func.parseErrorMessageFromServer(err));
            },
            () => {}
        );
    }

    private recount() {

        try {

            if (jQuery("#role-list").find('tbody :checkbox:checked').length < 1) {

                this.messages = this._Func.Messages('danger',this._Func.msg('CC0011'));
                return false;
            }

            var tmpArr = {};
            var target = this;
            var cycle_dtl_ids = [];
            jQuery("#role-list").find('tbody :checkbox:checked').each(function( index, element ){

                cycle_dtl_ids[index] = target.countItemList[jQuery(element).val()]['cycle_dtl_id'];

            });

            var params = JSON.stringify({'cycle_hdr_id' : this.cycleDetail['cycle_hdr_id'], 'cycle_dtl_ids': cycle_dtl_ids});

            this._cycleCountServices.recount(params)
                .subscribe(
                    data => {

                        this.messages = this._Func.Messages('success', this._Func.msg('VR109'));
                    },
                    err => {

                        this.parseError(err);
                    },
                    () => {

                        setTimeout(function(){

                            target.getCycleCountDetail(target.cycleID);
                            jQuery("#role-list input[type=checkbox]").prop('checked', false);
                            target.getCountItemByCycleID(this.page_current);

                        }, 1100);
                    }
                );

        } catch (err) {


        }

    }

    private printPdf() {

        try {

            var tar_get = this;
            var xhrl = new XMLHttpRequest();

            xhrl.open("GET", this.urlAPICC + "/print-pdf/" + this.cycleDetail['cycle_hdr_id'], true);
            xhrl.setRequestHeader("Authorization", 'Bearer ' +this._Func.getToken());
            xhrl.responseType = 'blob'; 

            xhrl.onreadystatechange = function () {

                // If we get an HTTP status OK (200), save the file using fileSaver
                if (xhrl.readyState === 4) {
                    if (xhrl.status === 200) {
                        var blob = new Blob([this.response], {type: 'application/pdf'});
                        saveAs.saveAs(blob, 'cycle_count.pdf');
                    } else {
                        var fr = new FileReader();
                        fr.onload = function(e){
                            const responseText = JSON.parse(e.target['result']);
                            tar_get.messages = tar_get._Func.Messages('danger', responseText.errors.message);
                        }
                        fr.readAsText(xhrl.response);
                    }
                }
            };

            xhrl.send();
        } catch (err) {

            this.messages = this._Func.Messages('danger', this._Func.msg('VR100'));
        }

    }

    private accept() {

        try {
            jQuery("#show-reduce").modal("hide");
            if (jQuery("#role-list").find('tbody :checkbox:checked').length < 1) {

                this.messages = this._Func.Messages('danger',this._Func.msg('CC0010'));
                return false;
            }

            var tmpArr = {};
            var target = this;
            var cycle_dtl_ids = [];
            jQuery("#role-list").find('tbody :checkbox:checked').each(function( index, element ){

                cycle_dtl_ids[index] = target.countItemList[jQuery(element).val()]['cycle_dtl_id'];

            });

            var params = JSON.stringify({'cycle_hdr_id' : this.cycleDetail['cycle_hdr_id'], 'cycle_dtl_ids': cycle_dtl_ids});

            this.showLoadingOverlay = true;
            this._cycleCountServices.accept(params)
                .subscribe(
                    data => {

                        this.showLoadingOverlay = false;
                        this.messages = this._Func.Messages('success', this._Func.msg('VR109'));

                    },
                    err => {

                        this.showLoadingOverlay = false;
                        if (err.status == 405) {

                            jQuery("#show-reduce").modal("show");
                            this.cycleCountListOI = JSON.parse(err.text())['data'];
                            for (var i = 0; i < this.cycleCountListOI['orders'].length; i++) {

                                this.cycleCountListOI['orders'][i]['flagClick'] = false;
                            }
                            this.showText();

                        } else {

                            this.parseError(err);
                        }

                    },
                    () => {

                        jQuery("#role-list input[type=checkbox]").prop('checked', false);


                        setTimeout(function(){

                            target.getCycleCountDetail(target.cycleID);
                            var params = '&status=' + encodeURIComponent(jQuery("input[name=displayFilter]:checked").val());
                            target.getCountItemByCycleID(this.page_current, params);


                        }, 1100);

                    }
                );

        } catch (err) {


        }
    }

    private parseError(err) {

        this.messages = {'status': 'danger', 'txt': this._Func.parseErrorMessageFromServer(err)};
    }

    // Get item detail by cycle count
    private getCountItemByCycleID(page = 1, $params = '') {

        this.messages = false;
        this.page_current = page;
        let param = "?page=" + page + "&limit=" + this.perPage + $params;

        jQuery("#role-list input[type=checkbox]").prop('checked', false);
        this.showLoadingOverlay = true;

        this._cycleCountServices.getCountItemByCycleID(this.cycleID, param).subscribe(
            data =>{

                this.countItemList = data.data;
                this.Pagination = data['meta']['pagination'];
                this.Pagination['numLinks'] = 3;
                this.Pagination['tmpPageCount'] = this._Func.Pagination(this.Pagination);

                this.showLoadingOverlay = false;
            },
            err => {

                this.showMessage('danger', this._Func.parseErrorMessageFromServer(err));
            },
            () => {}
        );
    }

    // Change size item show list
    private onPageSizeChanged($event) {

        this.perPage = $event.target.value;

        if((this.Pagination['current_page'] - 1) * this.perPage >= this.Pagination['total']) {
            this.currentPage = 1;
        }
        else {
            this.currentPage = this.Pagination['current_page'];
        }

        this.getCountItemByCycleID(this.currentPage);
    }

    private filterStatus($status) {
        
        var params = '&status=' + encodeURIComponent($status);
        this.getCountItemByCycleID(1, params);

    }

    // Event for pagination
    private filterList(num) {

        this.getCountItemByCycleID(num);
    }

    private checkCheckAll($event) {

        var checkboxAll = jQuery($event.target).parents('table').find('thead :checkbox');

        if(jQuery($event.target).parents('table').find('tbody :checkbox:not(:checked)').length > 0) {

            checkboxAll.prop('checked', false);
        } else {

            checkboxAll.prop('checked', true);
        }
    }

    // check all
    private checkAll($event) {

        var checkboxItem = jQuery($event.target).parents('table').find('tbody :checkbox:enabled');

        if(jQuery($event.target).is(':checked')) {

            checkboxItem.prop('checked', true);
        } else {

            checkboxItem.prop('checked', false);
        }

    }

    // Show message
    private showMessage(msgStatus, msg) {

        this.messages = {
            'status': msgStatus,
            'txt': msg
        };
        jQuery(window).scrollTop(0);
    }

    private revert(index, item) {

        window.open(window.location.origin + '/#/outbound/order-revert/' + item['odr_id'], '_blank');
    }

    private cancelReduce(index, item) {

        window.open(window.location.origin + '/#/outbound/cancel-order/' + item['odr_id'], '_blank');
    }

    private checkStatusReduce() {

        var param = {'odr_ids': []};

        if (this.cycleCountListOI['orders'].length < 1) {
            return false;
        }
        for (var i = 0; i < this.cycleCountListOI['orders'].length; i ++) {

            param['odr_ids'].push(this.cycleCountListOI['orders'][i]['odr_id']);
        }


        this._cycleCountServices.checkStatusReduce(JSON.stringify(param)).subscribe(
            data =>{
                var tmp = 0;
                for (var i = 0; i < data.data.length; i++) {

                    for (var j = 0; j < this.cycleCountListOI['orders'].length; j++) {

                        if (data.data[i]['odr_id'] == this.cycleCountListOI['orders'][j]['odr_id']) {

                            this.cycleCountListOI['orders'][j]['odr_sts'] = data.data[i]['odr_sts'];
                            this.cycleCountListOI['orders'][j]['odr_sts_name'] = data.data[i]['odr_sts_name'];
                        }

                        if ((data.data[i]['odr_sts'] == 'NW' || data.data[i]['odr_sts'] == 'CC') && (data.data[i]['odr_id'] == this.cycleCountListOI['orders'][j]['odr_id']) && !this.cycleCountListOI['orders'][j]['flagClick']) {

                            tmp += parseInt(this.cycleCountListOI['orders'][j]['alloc_qty']);
                            this.cycleCountListOI['orders'][j]['flagClick'] = true;
                        }

                    }
                }

                this.cycleCountListOI['item_info']['discrepancy_qty'] = tmp + this.cycleCountListOI['item_info']['discrepancy_qty'];

                this.showText();
            },
            err => {

                this.showMessage('danger', this._Func.parseErrorMessageFromServer(err));
            },
            () => {}
        );
    }

    private textRevert = '';
    private textCC = '';
    private showText () {

        this.textCC = '';
        this.textRevert = '';
        for (var i = 0; i < this.cycleCountListOI['orders'].length; i++) {

            if (!(this.cycleCountListOI['orders'][i]['odr_sts'] == 'CC' || this.cycleCountListOI['orders'][i]['odr_sts'] == 'NW' || this.cycleCountListOI['orders'][i]['picked_qty'] != 0)) {

                this.textRevert = 'revert';
            }

            if (this.cycleCountListOI['orders'][i]['odr_sts'] != 'CC' && this.cycleCountListOI['orders'][i]['odr_sts'] != 'NW') {

                this.textCC = 'cancel';
            }
        }
    }

    private setClickedRow(item, $event) {
        if (item.cycle_dtl_sts !== 'AC') {
            item.selected = !item.selected;
            let that = this;
            setTimeout(function(){
                that.checkCheckAll($event);
            }, 100);
        }
    }
}




