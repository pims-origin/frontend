import {Component, OnInit, OnDestroy} from '@angular/core';
import {FORM_DIRECTIVES, ControlGroup, FormBuilder, Control, ControlArray, Validators, AbstractControl} from '@angular/common';
import {RouteParams, Router, RouteData} from '@angular/router-deprecated';
import {UserService, Functions,FormBuilderFunctions} from '../../../common/core/load';
import {CRUOrderServices} from './cru-order-service';
import {OrderFunctions} from './order-functions';
import {ValidationService} from '../../../common/core/validator';
import {
    WMSBreadcrumb, PaginatePipe,
    WMSMessages, WMSPagination,
    OrderFlowDirective,
    ThirdPartyFormDirective,
    AutocompleteDirective,
    InfiniteScrollAutocomplete,
    PaginationControlsCmp, DocumentUpload, WMSDateTimePickerDirective, DmsIn,
    LaborCostDirective, PaginationService, OrderOutboundPallet, Labeling,PalletListDirective, InvoiceTracking } from '../../../common/directives/directives';
import {Http} from '@angular/http';
import {GoBackComponent} from "../../../common/component/goBack.component";
import {BoxPopupService} from "../../../common/popup/box-popup.service";
import {WarningBoxPopup} from "../../../common/popup/warning-box-popup";
import {API_Config} from "../../../common/core/API_Config";
import { CSRListComponent } from '../csr-list/csr-list.component';
import { GoogleAPI } from '../../../common/services/google-api'
declare var jQuery:any;
declare var saveAs:any;

@Component({
    selector: 'cru-order',
    providers: [
        CRUOrderServices,
        OrderFunctions,
        PaginationService,
        ValidationService,
        FormBuilder,
        BoxPopupService,
        GoogleAPI,
        UserService],
    directives: [
        InfiniteScrollAutocomplete, DmsIn,
        FORM_DIRECTIVES, WMSPagination, OrderFlowDirective,ThirdPartyFormDirective,
        PaginationControlsCmp, DocumentUpload, WMSMessages,AutocompleteDirective,
        GoBackComponent, WMSBreadcrumb, LaborCostDirective, OrderOutboundPallet, Labeling,
        PalletListDirective, CSRListComponent, InvoiceTracking, WMSDateTimePickerDirective
    ],
    pipes: [PaginatePipe],
    templateUrl: 'cru-order.component.html',
})


export class CRUOrderComponent implements OnInit, OnDestroy {

    public Loading = [];
    private messages;
    private Action;
    private TitlePage = 'View Customer';
    private IsView:boolean = false;
    private ListSelectedItem:Array<any> = [];
    private perPage = 20;
    ODForm:ControlGroup;
    private cusId;
    private uomListSliced:any = {};
    private showLoadingOverlay:boolean = false;
    private Country = [];
    private StateProvinces = [];
    private Customers:Array<any> = [];
    items:ControlGroup[] = [];
    itemsArray:ControlArray = new ControlArray(this.items);
    private selectedAll = {};
    private whs_id;
    private orderType = [];
    showForm:boolean = true;
    private totalSKU;
    private uomList:Array<any> = [];
    private evenTracking:Array<any> = [];
    private enableShippingTracking;
    private allCarton = [];
    private ckLoadDocument = {'flag': false, 'message': ''};
    private uploadAPIDocumentString;
    private Pagination;
    private currentPage = 1;
    private numLinks = 3;
    private _orderDetail = {};
    private sys_uom_code_default = 'EA';
    private sys_uom_id_default;
    private listOrderFlow = [];
    private ship_to_name_current={};
    private flagVCS = false;
    public IsEditBO = false;
    private loggerUser = {};
    private allowPickingSlip:boolean = false;
    public autocomplete;
    carriers: any[] = [];
    carriersTypeOther: any[] = [];
    deliveryServices: any[] = [];
    isLoadingDeliServices: boolean = false;
    canUndoPackCartons:boolean = false;
    isTransferWarehouse: boolean = false;
    private flagOrderFolow = false;

    public rushList = [{
        key: 0,
        value: 0
    },{
        key: 1,
        value: 1
    },{
        key: 2,
        value: 2
    },{
        key: 3,
        value: 3
    },{
        key: 4,
        value: 4
    },{
        key: 5,
        value: 5
    }];

    constructor(private _http:Http,
                private _Func:Functions,
                private fbdFunc:FormBuilderFunctions,
                private params:RouteParams,
                private fb:FormBuilder,
                private odrFunc:OrderFunctions,
                private _Valid:ValidationService,
                private _boxPopupService:BoxPopupService,
                private ODService:CRUOrderServices,
                _RTAction:RouteData,
                private _user:UserService,
                private _API:API_Config,
                private googleAPI: GoogleAPI,
                private _router:Router) {


        this.Action = _RTAction.get('action');
        this.whs_id = this._Func.lstGetItem('whs_id');

        this.checkPermission();
        this.getLoggedUser();
        this.getWarehouseList();
    }

    ngOnInit() {
        if (this.Action === 'new') {
            localStorage.setItem('inputting', '1');
        }
    }

    ngOnDestroy() {
        localStorage.setItem('inputting', '0');
    }

    // Check permission for user using this function page
    private createOrder;
    private viewOrder;
    private editOrder;
    private allowAccess:boolean = false;
    private outPalletTabShow:boolean = false;

    canCSRComment = false;
    canWHComment = false;
    checkPermission() {

        this.showLoadingOverlay = true;
        this._user.GetPermissionUser().subscribe(
            data => {
                this.showLoadingOverlay = false;
                this.createOrder = this._user.RequestPermission(data, 'createOrder');
                this.viewOrder = this._user.RequestPermission(data, 'viewOrder');
                this.editOrder = this._user.RequestPermission(data, 'editOrder');
                this.canCSRComment = this._user.RequestPermission(data, 'csrComment');
                this.canWHComment = this._user.RequestPermission(data, 'whseComment');
                this.canUndoPackCartons = this._user.RequestPermission(data, 'undoPackCartons');

                /*
                 * Action router View detail
                 * */
                if (this.Action == 'view') {
                    if (this.viewOrder) {
                        this.IsView = true;
                        this.TitlePage = 'View Order';
                        this.allowAccess = true;
                        this.orderDetail();
                        this.getCarton();
                    }
                    else {
                        this.redirectDeny();
                    }
                }

                // Action new
                if (this.Action == 'new') {
                    if (this.createOrder) {
                        this.allowAccess = true;
                        this.TitlePage = 'Add Order';
                        this.formBuilder();
                        this.handleOrderTypeValueChanges();
                    }
                    else {
                        this.redirectDeny();
                    }
                }

                /*
                 * Action router Edit
                 * */
                if (this.Action == 'edit') {
                    if (this.editOrder) {
                        this.allowAccess = true;
                        this.TitlePage = 'Edit Order';
                        this.orderDetail();
                    }
                    else {
                        this.redirectDeny();
                    }
                }

                /*
                 * Action router Edit
                 * */
                if (this.Action == 'comment') {
                    if (this.viewOrder) {
                        this.IsView = true;
                        this.allowAccess = true;
                        this.TitlePage = 'Edit Comments';
                        this.orderDetail();
                    }
                    else {
                        this.redirectDeny();
                    }
                }

                if (this.allowAccess) {
                    // load default
                    this.GetCountry();
                    this.getOrderType();
                    this.getCustomersByUser();
                    this.getUOMList();
                    if (this.Action !== 'view' && !this.autocomplete) {
                        setTimeout(() => {
                            this.addAutoComplete();
                        }, 300);
                    }
                }


            },
            err => {
                this.parseError(err);
                this.showLoadingOverlay = false;
            }
        );
    }

    redirectDeny() {
        this._router.parent.navigateByUrl('/deny');
    }

    handleOrderTypeValueChanges() {
        if (this.ODForm) {
            this.ODForm.controls['odr_type'].valueChanges.subscribe(val => {
                if (val !== 'XDK') {
                    this.fbdFunc.emptyControlArray(this.itemsArray);
                    this.fbdFunc.setValueControl(this.ODForm, 'qualifier', []);
                    this.addNewItem();
                }

                if (val === 'RTL') {
                    this.getCarriers();
                    const fields = ['carrier_code', 'dlvy_srvc_code'];
                    this._validateRetailerInfo(fields);
                    this.ODForm.addControl('ship_to_phone', new Control('', Validators.compose([Validators.required, Validators.pattern('^[0-9()\-]+$'), this.validateShipToPhone])));
                } else {
                    // reset validation carrier_code, delivery service, ship_to_phone
                    (<Control>this.ODForm.controls['carrier_code']).setErrors(null);
                    (<Control>this.ODForm.controls['carrier']).setErrors(null);
                    (<Control>this.ODForm.controls['dlvy_srvc_code']).setErrors(null);
                    this.ODForm.removeControl('ship_to_phone');
                }

                this.ODForm.updateValueAndValidity();
            });

            if (this._orderDetail) {
                if (this._orderDetail['odr_type_key'] === 'RTL') {
                    this.ODForm.addControl('ship_to_phone', new Control(this._orderDetail['ship_to_phone'] ? this._orderDetail['ship_to_phone'] : '', Validators.compose([Validators.required, Validators.pattern('^[0-9()\-]+$'), this.validateShipToPhone])));
                } else {
                    this.ODForm.removeControl('ship_to_phone');
                }
            }
        }
    }

    /***
     *
     * Order forrm
     * **/
    formBuilder() {

        this.ODForm = this.fb.group({
            cus_id: ['', Validators.required],
            cus_odr_num: [null, Validators.compose([Validators.required, this._Valid.validateSpace])],
            ref_cod: [null],
            cus_po: [null, Validators.compose([Validators.required, this._Valid.validateSpace])],
            odr_type: [this.orderType[0]],
            rush_odr: [0],
            sku_ttl: [null],
            odr_sts: ['New'],
            odr_num: [null],
            carrier: [null, Validators.compose([Validators.required, this._Valid.validateSpace])],
            carrier_code: [''],
            dlvy_srvc: [''],
            dlvy_srvc_code: [''],
            ship_to_name: [null, Validators.compose([Validators.required, this._Valid.validateSpace])],
            ship_to_add_1: [null, Validators.compose([Validators.required, this._Valid.validateSpace])],
            ship_to_add_2: [],
            ship_to_city: [null, Validators.compose([Validators.required, this._Valid.validateSpace])],
            ship_to_country: ['US', Validators.required],
            ship_to_state: ['', Validators.required],
            ship_to_zip: [null, Validators.compose([Validators.required, this._Valid.validateSpace])],
            ship_by_dt: [null, Validators.compose([this._Valid.validateSpace])],
            cancel_by_dt: [null, Validators.compose([this._Valid.validateSpace])],
            req_cmpl_dt: [null, this._Valid.validateSpace],
            shipped_dt: [null],
            schd_ship_dt: [null],
            routed_dt: [null],
            act_cancel_dt: [null],
            items: this.itemsArray,
            in_notes: [null],
            cus_notes: [null],
            csr_notes: [null],
            pack_odr: [null],
            whs_id_to: [''],
            qualifier: [],
            spec_instruct: [null]
        }, { validator: this._Valid.cancelByDate('ship_by_dt', 'cancel_by_dt', 'req_cmpl_dt', 'schd_ship_dt') });

        // init the first row for this.itemsArray
        this.addNewItem();

        this.GetState('US');

        //watch form
        this.ODForm.valueChanges.subscribe(data => {
            this.checkDuplicatedKey();
            this.odrFunc.exceed9999(this.itemsArray);
            this.getUPCtotals();
        });

        setTimeout(() => {
            this.fbdFunc.handelDropdownDatePicker(this.ODForm);
        }, 200);

        this.ODForm.controls['odr_type'].valueChanges.subscribe(
            value => {
                this.isTransferWarehouse = value.toUpperCase() === 'WHS' ? true : false;
            }
        );
    }

    validateBeforeSubmit() {
        // validate Warehouse control
        if (this.isTransferWarehouse && !this.ODForm.value['whs_id_to']) {
            this.ODForm.controls['whs_id_to'].setErrors({ 'required': true });
        } else {
            this.ODForm.controls['whs_id_to'].setErrors(null);
        }
    }

    /**
     * Get ship to phone error message
     */
    getShipToPhoneErrorMessage() {
        const requiredMessage = 'Ship To Phone is required.';
        const patternMessage = `Ship To Phone is not valid (only allow number and '(', '-', ')' ).`;
        const customMinMessage = 'Ship To Phone should contain 10 numbers at least.';
        const customMaxMessage = 'Ship To Phone should contain 15 numbers at maximum.';

        return this.ODForm.controls['ship_to_phone'].hasError('required') ? requiredMessage :
            this.ODForm.controls['ship_to_phone'].hasError('pattern') ? patternMessage :
                this.ODForm.controls['ship_to_phone'].hasError('customMinLength') ? customMinMessage :
                    this.ODForm.controls['ship_to_phone'].hasError('customMaxLength') ? customMaxMessage : '';
    }

    validateShipToPhone(control: AbstractControl): { [key: string]: boolean } | null {
		const value = control.value.replace(/[()\-]+/gi, '');

        if (control.value && value.length < 10) {
            return { customMinLength: true };
		}

        if (control.value && value.length > 15) {
            return { customMaxLength: true };
        }

        return null;
    }

    /*
     * form edit view
     *
     * ShipbydateModel , CancelByDateModel , RequestcompleteDateModel
     *
     * */
    private ShipbydateModel;
    private CancelByDateModel;
    private RequestcompleteDateModel;

    formBuilderEditView(data) {
        let packOdr = data['pack_odr'] == 1 ? true : false;
        this.ODForm = this.fb.group({
            cus_id: [data['cus_id'], Validators.required],
            cus_odr_num: [data['cus_odr_num'], Validators.compose([Validators.required, this._Valid.validateSpace])],
            ref_cod: [data['ref_cod']],
            cus_po: [data['cus_po'], Validators.compose([Validators.required, this._Valid.validateSpace])],
            odr_type: [data['odr_type_key']],
            rush_odr: [data['rush_odr']],
            sku_ttl: [data['sku_ttl']],
            odr_sts: [data['odr_sts']],
            odr_num: [data['odr_num']],
            carrier: [data['carrier'], Validators.compose([Validators.required, this._Valid.validateSpace])],
            carrier_code: [data['carrier_code'] ? data['carrier_code'] : ''],
            dlvy_srvc: [data['dlvy_srvc'] ? data['dlvy_srvc'] : ''],
            dlvy_srvc_code: [data['dlvy_srvc_code'] ? data['dlvy_srvc_code'] : ''],
            ship_to_name: [data['ship_to_name'], Validators.compose([Validators.required, this._Valid.validateSpace])],
            ship_to_add_1: [data['ship_to_add_1'], Validators.compose([Validators.required, this._Valid.validateSpace])],
            ship_to_add_2: [data['ship_to_add_2']],
            ship_to_city: [data['ship_to_city'], Validators.compose([Validators.required, this._Valid.validateSpace])],
            ship_to_country: [data['ship_to_country'], Validators.required],
            ship_to_state: [data['ship_to_state'], Validators.required],
            ship_to_zip: [data['ship_to_zip'], Validators.compose([Validators.required, this._Valid.validateSpace])],
            ship_by_dt: [data['ship_by_dt'], Validators.compose([this._Valid.validateSpace])],
            cancel_by_dt: [data['cancel_by_dt'], Validators.compose([this._Valid.validateSpace])],
            req_cmpl_dt: [data['req_cmpl_dt'], this._Valid.validateSpace],
            shipped_dt: [data['shipped_dt']],
            schd_ship_dt: [data['schd_ship_dt']],
            routed_dt: [data['routed_dt']],
            act_cancel_dt: [data['act_cancel_dt']],
            items: this.itemsArray,
            in_notes: [data['in_notes']],
            cus_notes: [data['cus_notes']],
            csr_notes: [data['csr_notes']],
            whs_id_to: [data['whs_id_to']],
            pack_odr: [packOdr],
            qualifier: [data['qualifier']],
            spec_instruct: [data['spec_instruct']]
        }, { validator: this._Valid.cancelByDate('ship_by_dt', 'cancel_by_dt', 'req_cmpl_dt', 'schd_ship_dt') });

        // set value to model to update value on datapicker field
        this.ShipbydateModel = data['ship_by_dt'];
        this.CancelByDateModel = data['cancel_by_dt'];
        this.RequestcompleteDateModel = data['req_cmpl_dt'];

        if (this.Action === 'view' || data['odr_sts_key'] === 'SH' || data['odr_sts_key'] === 'CC') {
            this.flagVCS = true;
        }

        if (this.Action === 'edit' && data['odr_sts_key'] === 'NW' && data['odr_type_key'] === 'BAC') {
            this.IsEditBO = true;
        }

        if (data['origin_odr_type'] == 'EC' || data['odr_type_key'] == 'EC') {
            this.IsView = true;
            this.enableShippingTracking = true;
        }

        if (data['odr_sts_key'] !== 'NW') {
            // this.Action = 'view';
            this.IsView = true;
        }

        if (data['odr_type_key'] !== 'BAC') {
            // if odr_type_key isn't BAC type -> remove BAC
            this.filterOrderType(this.orderType);
        }


        this.GetState(data['ship_to_country']);

        // push to items - hack for null valid
        if (data['items'] !== null && this.items.length === 0) {
            data['items'].forEach((item)=> {
                this.itemDetail(item);
            });
        }

        //watch form

        this.ODForm.valueChanges.subscribe(data => {
            if (this.Action !== 'view') {
                this.checkDuplicatedKey();
            }
            this.odrFunc.exceed9999(this.itemsArray);
            this.getUPCtotals();
        });

        setTimeout(() => {
            this.fbdFunc.handelDropdownDatePicker(this.ODForm);
        }, 200);
    }

    arrWarehouse = [];

    /**
     * Get warehouse list from localStorage
     */
    getWarehouseList() {
        const loggedUser = JSON.parse(localStorage.getItem('logedUser'));
        this.arrWarehouse = loggedUser['user_warehouses'].filter(warehouse => warehouse['whs_id'] !== this.whs_id);
    }

    addNewItem() {
        // init the first row for this.itemsArray
        this.itemsArray.push(
            new ControlGroup({
                'itm_id': new Control(''),
                'odr_dtl_id': new Control(''),
                'cus_upc': new Control(''),
                'sku': new Control('', Validators.compose([Validators.required, this._Valid.validateSpace])),
                'color': new Control(''),
                'size': new Control(''),
                'lot': new Control('Any'),
                'uom_id': new Control(this.sys_uom_id_default ? this.sys_uom_id_default : '', Validators.required),
                'uom_name': new Control(''),
                'uom_code': new Control(this.sys_uom_code_default),
                'piece_qty': new Control('', Validators.compose([Validators.required, this._Valid.isZero, this._Valid.inPutOnlyNumber])),
                'uom_qty': new Control('', Validators.compose([Validators.required,this._Valid.validateSpace])),
                'pack': new Control('', Validators.compose([this._Valid.isZero, this._Valid.inPutOnlyNumber])),
                'qty': new Control('',Validators.compose([this._Valid.inPutOnlyNumber])),
                'avail': new Control(''),
                'avail_pickable': new Control(''),
                'back_odr': new Control(''),
                'total_piece_qty': new Control(0),
                'ship_track_id': new Control(null, Validators.compose([this._Valid.validateSpace])),
                'uom_level' : new Control(),
            })
        );

        if (this.ODForm.value['odr_type'] == 'EC') {
            // default is not require the field  shipi track id
            this.odrFunc.set_required_ship_track_id(this.itemsArray,true);
        }

    }

    itemDetail(item) {

        this.itemsArray.push(
            new ControlGroup({
                'itm_id': new Control(item['itm_id']),
                'cus_upc': new Control(item['cus_upc']),
                'odr_dtl_id': new Control(item['odr_dtl_id']),
                'sku': new Control(item['sku'], Validators.compose([Validators.required, this._Valid.validateSpace])),
                'color': new Control(item['color']),
                'size': new Control(item['size']),
                'lot': new Control(item['lot']),
                'uom_id': new Control(item['uom_id'], Validators.required),
                'uom_name': new Control(item['uom_name']),
                'uom_code': new Control(item['uom_code']),
                'allocated_qty': new Control(item['allocated_qty']),
                'picked_qty': new Control(item['picked_qty']),
                'uom_qty': new Control(item['uom_qty'], Validators.compose([,Validators.required,this._Valid.validateSpace])),
                'piece_qty': new Control(item['piece_qty'], Validators.compose([Validators.required, this._Valid.isZero, this._Valid.inPutOnlyNumber])),
                'pack': new Control(item['pack'], Validators.compose([this._Valid.isZero, this._Valid.inPutOnlyNumber])),
                'qty': new Control(item['qty'],Validators.compose([this._Valid.inPutOnlyNumber])),
                'avail': new Control(item['avail']),
                'total_piece_qty': new Control(0),
                'avail_pickable': new Control(item['avail']),
                'back_odr': new Control(item['back_odr']),
                'ship_track_id': new Control(item['ship_track_id'], Validators.compose([this._Valid.validateSpace])),
                'uom_level' : new Control(item['uom_level']),
                'prod_line' : new Control(item['prod_line']),
                'cmp' : new Control(item['cmp']),
            })
        );

        if (this.ODForm.value['origin_odr_type'] == 'EC') {
            // default is not require the field  shipi track id
            this.odrFunc.set_required_ship_track_id(this.itemsArray,true);
        }

        let row = this.items.length - 1;

        if (item['back_odr'] == 1) {
            this.items[row]['checked'] = true;
        }

        // set true
        this.items[row]['item_code_valid'] = true;
        this.items[row]['sku_valid'] = true;

        const control = (<ControlGroup>this.items[row]);
        this.odrFunc.computePickableQty(control);
        this.odrFunc.calTotalPiece(this.items);

    }


    deleteItem() {

        let n = this;
        if (this.fbdFunc.getItemchecked(this.itemsArray) > 0) {

            /*case 1: modify text*/
            let warningPopup = new WarningBoxPopup();
            warningPopup.text = this._Func.msg('PBAP002');
            this._boxPopupService.showWarningPopup(warningPopup)
                .then(function (dm) {
                    if (!dm) {
                        return;
                    } else {
                        n.fbdFunc.deleteItem(n.itemsArray).subscribe(deletedAll=>{
                            if(deletedAll) {
                                n.addNewItem();
                            }
                            n.odrFunc.calTotalPiece(n.items);
                        });
                    }
                });
        }
        else {

            this.messages = this._Func.Messages('danger', this._Func.msg('VR113'));

        }
    }

    private orderId;

    orderDetail() {
        this.orderId = this.params.get('id');
        this.ODService.getOrderDetail(this.orderId).subscribe(
            data => {
                this.formBuilderEditView(data);
                this._orderDetail = data;
                if (['NW', 'AL', 'PK', 'CC', 'PPK', 'PAL', 'PCC'].indexOf(this._orderDetail['odr_sts_key']) === -1) {
                    this.allowPickingSlip = true;
                }
                
                this.handleOrderTypeValueChanges();

                this.getEventracking();
                this.getCarriers();
                this._getDeliveryServices(data['carrier_code']);
            },
            err => {
                this.parseError(err);
            },
            () => {}
        );

    }

    // Get Carton
    getCarton(page = null) {

        var params = "?page=" + page + "&limit=" + this.perPage;
        this.orderId = this.params.get('id');
        this.ODService.getCarton(this.orderId, params).subscribe(
            data => {
                this.initPagination(data);
                this.allCarton = data.data
            },
            err => {
                this.parseError(err);
            },
            () => {
            }
        );

    }

    private filterList(pageNumber) {

        this.getCarton(pageNumber);
    }

    // Set params for pagination
    private initPagination(data) {

        var meta = data.meta;
        this.Pagination = meta['pagination'];
        this.Pagination['numLinks'] = 3;
        this.Pagination['tmpPageCount'] = this._Func.Pagination(this.Pagination);

    }

    // Event when user filter form
    private onPageSizeChanged($event, el) {

        this.perPage = $event.target.value;

        if ((this.Pagination['current_page'] - 1) * this.perPage >= this.Pagination['total']) {
            this.currentPage = 1;
        }
        else {
            this.currentPage = this.Pagination['current_page'];
        }
        if (el == 'eventtrack') {
            this.getEventracking(this.currentPage);
        }
        else {
            this.getCarton(this.currentPage);
        }
    }

    getUPCtotals() {

        this.totalSKU = 0;
        let items = this.ODForm.controls['items']['controls'];
        for (let i in items) {
            let item = items[i];
            if (item.valid == true) {
                this.totalSKU++;
            }
        }
        ;


    }

    /*
     * Load state of country when changle
     * */
    ChangeCountry(item) {
        if (item) {
            this.GetState(item);
            this.StateProvinces = [];
            (<Control>this.ODForm.controls['ship_to_state']).updateValue('');
        }
        else {
            this.StateProvinces = [];
            (<Control>this.ODForm.controls['ship_to_state']).updateValue('');
        }
    }

    /*
     * Get Country
     * */

    GetCountry() {
        let param = '?sort[ordinal]=asc&sort[sys_country_name]=asc&limit=500';
        this.ODService.getCountry(param).subscribe(
            data=> {
                this.Country = data;
            },
            err=> {
            },
            () => {}
        );
    }

    GetState(idCountry, idState: string = '') {
        this.Loading['state'] = true;
        let param = "?sort[sys_state_name]=asc&limit=500";
        this.ODService.State(idCountry, param).subscribe(
            data=> {
                this.Loading['state'] = false;
                this.StateProvinces = data;
            },
            err=> {
                this.Loading['state'] = false;
            },
            () => {
                if (idState) {
                    this.setState(idState);
                }
            }
        );
    }

    // get list custommer

    private getCustomersByUser() {
        // let params = '?whs_id=' + this.whs_id + "&limit=10000";
        let params = "?limit=10000";
        this.ODService.getCustomersByUser(params)
            .subscribe(
                data => {
                    this.Customers = data.data;
                },
                err => {
                    this.parseError(err);
                },
                () => {
                }
            );

    }


    private autoSearchData = [];

    autoComplete(row, key, fieldname,page=1,reset=true) {

        // search only if cus_id have seleced
        if (this.ODForm.value['cus_id']) {

            this.items[row]['showLoading'] = true;
            this.items[row]['current_page'] = page;

            // reset row
            this.resetRow(row);

            let searchkey = this._Func.trim(key);

            if (searchkey == '') {
                return true;
            }

            const xdoc_param = this.ODForm.value['odr_type']=='XDK' ? '-xdock' : '';
            let param = xdoc_param + "?" + fieldname + "=" + searchkey + "&cus_id=" + this.ODForm.value['cus_id'] + "&limit=20&sort[sku]=asc&page="+page;
            let enCodeSearchQuery = this._Func.FixEncodeURI(param);

            if (this.items[row]['subscribe']) {
                this.items[row]['subscribe'].unsubscribe();
            }
            // do Search
            this.items[row]['subscribe'] = this.ODService.searchItems(enCodeSearchQuery).debounceTime(400).distinctUntilChanged().subscribe(
                data => {
                    // disable loading icon
                    this.items[row]['showLoading'] = false;
                    this.items[row]['paginData'] = data.meta;
                    if(!this.items[row]['autoSearchData'] || reset){
                        this.items[row]['autoSearchData'] = [];
                    }
                    this.items[row]['autoSearchData'].push(...data.data);
                    this.odrFunc.checkKeyExist(this.itemsArray,row,searchkey,data.data,'sku', this.ODForm.value['odr_type']);
                },
                err => {
                    this.items[row]['showLoading'] = false;
                    this.parseError(err);
                },
                () => {
                }
            );

        }


    }


    /*
    * autoCompleteShippingTo
    * */
    private shipping_to_list:Array<any>=[];
    private  autoCompleteShippingTo(key){

        this.Loading['shipping_to']=false;
        this.messages = false;
        let param = "/cus/"+this.ODForm.value['cus_id']+"/third-party?name="+key+"&limit=20";
        let enCodeSearchQuery = this._Func.FixEncodeURI(param);
        this.ship_to_name_current={};

        if (this.ODForm['subscribe']) {
            this.ODForm['subscribe'].unsubscribe();
        }

        if(key){
            // do Search
            this.Loading['shipping_to']=true;
            this.ODForm['subscribe'] = this.ODService.searchShippingTo(enCodeSearchQuery).debounceTime(400).distinctUntilChanged().subscribe(
                data => {
                    // disable loading icon
                    this.Loading['shipping_to']=false;
                    this.shipping_to_list = data.data;
                    let item = this.odrFunc.findKeyInList(this.shipping_to_list,'name', this._Func.trim(key));
                    if(item){
                        this.selectedShippingTo(item);
                    }else{
                        // this.fbdFunc.setErrorControl(this.ODForm,'ship_to_name',{'not_existed':true});
                    }

                },
                err => {
                    this.Loading['shipping_to']=false;
                    this.parseError(err);
                },
                () => {
                }
            );
        }

    }

    private selectedShippingTo(item={}){

        this.changeShipName(item);

    }

    private changeShipName(item={}){

        this.setValueShippingInfo(item);
        this.GetState(item['country']);

    }


    changeCustomer($event) {

        let new_cus = $event.target.value;
        let current_cus = this.ODForm.value['cus_id'];
        if(current_cus&&this.ODForm.value['ship_to_name']){

            let n = this;
            /*case 1: modify text*/
            let warningPopup = new WarningBoxPopup();
            warningPopup.text = this._Func.msg('ODIT001');
            this._boxPopupService.showWarningPopup(warningPopup)
                .then(function (dm) {
                    if (dm){
                        n.fbdFunc.emptyControlArray(n.itemsArray);
                        n.setValueShippingInfo();
                        n.addNewItem();
                    }else{
                        n.fbdFunc.setValueControl(n.ODForm,'cus_id',current_cus);
                    }
                })
        }else{

            this.fbdFunc.emptyControlArray(this.itemsArray);
            this.addNewItem();

        }


    }


    setValueShippingInfo(item={}){

        // reset all group shipping info

        this.fbdFunc.setValueControl(this.ODForm,'ship_to_name',item['name']);
        this.fbdFunc.setValueControl(this.ODForm,'ship_to_state',item['state']);
        this.fbdFunc.setValueControl(this.ODForm,'ship_to_zip',item['zip']);
        this.fbdFunc.setValueControl(this.ODForm,'ship_to_city',item['city']);
        this.fbdFunc.setValueControl(this.ODForm,'ship_to_country',item['country']);

        item['add_1'] = item['add_1'] ? item['add_1'] : '';
        item['add_2'] = item['add_2'] ? item['add_2'] : '';

        this.fbdFunc.setValueControl(this.ODForm,'ship_to_add_1',item['add_1']);
        this.fbdFunc.setValueControl(this.ODForm,'ship_to_add_2',item['add_2']);
        this.ship_to_name_current = item;

        this.getZipcode(null);
    }

    changeOdrType($event){
        if(this.ODForm.value['odr_type']=='XDK'){
            if($event.target.value!=='XDK'){
                this.fbdFunc.emptyControlArray(this.itemsArray);
                this.addNewItem();
            }
        }

        if(this.ODForm.value['odr_type']!=='XDK'){
            if($event.target.value=='XDK'){
                this.fbdFunc.emptyControlArray(this.itemsArray);
                this.addNewItem();
            }
        }

    }


    checkDuplicatedKey() {
        this.fbdFunc.checkDuplicatedKey(this.itemsArray,['sku','size','color','lot']);
    }


    resetRow(row) {
        this.items[row]['disabled'] = false;
        this.items[row]['item_code_valid'] = false;
        this.items[row]['sku_valid'] = false;
        (<Control>this.ODForm.controls['items']['controls'][row]['controls']['itm_id']).updateValue('');
        (<Control>this.ODForm.controls['items']['controls'][row]['controls']['cus_upc']).updateValue('');
        (<Control>this.ODForm.controls['items']['controls'][row]['controls']['color']).updateValue('');
        (<Control>this.ODForm.controls['items']['controls'][row]['controls']['size']).updateValue('');
        //(<Control>this.ODForm.controls['items']['controls'][row]['controls']['lot']).updateValue('');
        (<Control>this.ODForm.controls['items']['controls'][row]['controls']['ship_track_id']).updateValue('');
        (<Control>this.ODForm.controls['items']['controls'][row]['controls']['pack']).updateValue('');
        (<Control>this.ODForm.controls['items']['controls'][row]['controls']['uom_id']).updateValue(this.sys_uom_id_default);
        // reset qty to fixed WMS2-5090
        (<Control>this.ODForm.controls['items']['controls'][row]['controls']['uom_qty']).updateValue('');
        (<Control>this.ODForm.controls['items']['controls'][row]['controls']['piece_qty']).updateValue('');
    }

    set_uom_id_default(data:Array<any>) {
        for (let i in data) {
            if (data[i]['sys_uom_code'] == this.sys_uom_code_default) {
                // found sys_uom_code_default -> update
                this.sys_uom_id_default = data[i]['sys_uom_id'];
                // update frist row item
                if (this.Action == 'new') {
                    (<Control>this.ODForm.controls['items']['controls'][0]['controls']['uom_id']).updateValue(this.sys_uom_id_default);
                }
                return;
            }
        }
    }

    /*
     * Hack for datepicker
     * */
    mousOver(control) {
        if (this.ODForm.controls[control].hasError('required')) {
            (<Control>this.ODForm.controls[control]).setErrors(null);
        }
        setTimeout(() => {
            if (!this.ODForm.controls[control].value) {
                (<Control>this.ODForm.controls[control]).updateValue('');
            }
        }, 250);
    }


    /*
     * Save
     * */
    private submitForm:boolean = false;

    Save() {
        let data = this.ODForm.value;

        this.submitForm = true;

        this.validateBeforeSubmit();

        if (this.ODForm.valid) {
            //Trim Leading and Trailing
            data.carrier = data.carrier.trim();
            data.ship_to_name = data.ship_to_name.trim();
            data.ship_to_add_1 = data.ship_to_add_1.trim();
            data.ship_to_city = data.ship_to_city.trim();
            data.ship_to_zip = data.ship_to_zip.trim();
            data.pack_odr = data.pack_odr ? 1 : 0;
            data.is_ecom = 0;
            data['tp_id'] = this.ship_to_name_current['tp_id'];

            let data_json = JSON.stringify(data);

            if (this.Action == 'new') {
                // add new
                this.AddNew(data_json);
            }
            else {
                // edit zone type
                this.orderId = this.params.get('id');
                this.Update(this.orderId, data_json);
            }

        }
    }

    /*
     *
     * */
    AddNew(data) {
        this.showLoadingOverlay = true;
        if (this.isTransferWarehouse) {
            this.ODService.addNewOrdersTransferWarehouse(data).subscribe(
                data => {
                    this.flagOrderFolow = true;
                    this.showLoadingOverlay = false;
                    //Reset Form
                    this._afterCreateOrder(data);
                },
                err => {
                    this.flagOrderFolow = false;
                    this.showLoadingOverlay = false;
                    if (err.json().errors.message) {
                        this.messages = this._Func.Messages('danger', err.json().errors.message);
                    }
                },
                () => { }
            );
        } else {
            this.ODService.addNewOrders(data).subscribe(
                data => {
                    this.flagOrderFolow = true;
                    this.showLoadingOverlay = false;
                    //Reset Form
                    this._afterCreateOrder(data);
                },
                err => {
                    this.flagOrderFolow = false;
                    this.showLoadingOverlay = false;
                    if (err.json().errors.message) {
                        this.messages = this._Func.Messages('danger', err.json().errors.message);
                    }
                },
                () => { }
            );
        }
    }
    
    _afterCreateOrder(data: any) {
        let that = this;
        this.orderId = data.odr_id;
        //
        const info = {
            'schd_ship_dt': this.ODForm.value.schd_ship_dt,
            'routed_dt': this.ODForm.value.routed_dt,
        }
        this.updateScheduleRouteDate(this.orderId, info, () => {
            if (that.itemFiles.length) {
                // upload file
                that.uploadFile(data.odr_num);
            } else {
                that.messages = that._Func.Messages('success', that._Func.msg('VR107'));
                that._redirectToList(that);
            }
        });
    }

    _redirectToList(that, time = 600) {
        setTimeout(function () {
            that._router.parent.navigateByUrl('/outbound/order');
        }, time);
    }

    Update(id, data) {

        let n = this;
        if(this.ODForm.value['odr_type']!=='XDK'){
            this.saveOverFlow();
        }
        this.showLoadingOverlay = true;
        this.ODService.updateOrder(id, data).subscribe(
            data => {
                const info = {
                    'schd_ship_dt': this.ODForm.value.schd_ship_dt,
                    'routed_dt': this.ODForm.value.routed_dt,
                }
                this.updateScheduleRouteDate(this.orderId, info, () => {
                    console.log('saved ScheduleRouteDate.');
                })
                this.showLoadingOverlay = false;
                this.messages = this._Func.Messages('success', this._Func.msg('VR109'));
                n._redirectToList(n);
            },
            err => {
                this.showLoadingOverlay = false;
                this.parseError(err);
            },
            () => {}
        );

    }

    checkInputNumber(evt) {
        // evt = evt || window.event;
        this._Valid.isNumber(evt, true);
    }

    getOrderType() {

        this.ODService.getOrderType().subscribe(
            data => {
                this.orderType = data.data;
                // if is add new
                if (this.Action == 'new') {
                    // remove BAC
                    this.filterOrderType(this.orderType);
                    for (let i = 0; i < this.orderType.length; i++) {
                        if (this.orderType[i].key == 'EC') {
                            this.orderType.splice(i, 1);
                        }
                    }
                    (<Control>this.ODForm.controls['odr_type']).updateValue(this.orderType[0]['key']);
                }

                /*
                if (this.Action == 'edit') {
                    for (let i = 0; i < this.orderType.length; i++) {
                        if (this.orderType[i].key == 'EC' || this.orderType[i].key == 'BAC') {
                            this.orderType.splice(i, 1);
                        }
                    }
                }
                */
            },
            err => {
                this.parseError(err);
            },
            () => {}
        );


    }

    filterOrderType(orderTypeArr:Array<any>) {
        let arayTmp = orderTypeArr.slice();
        arayTmp.forEach((item)=> {
            if (item['key'] == 'BAC') {
                let index = orderTypeArr.indexOf(item);
                orderTypeArr.splice(index, 1);
            }
        });
    }


    // Show error when server die or else
    private parseError(err) {
        this.messages = {'status': 'danger', 'txt': this._Func.parseErrorMessageFromServer(err)};
    }

    redirectToList() {
        let n = this;
        /*case 1: modify text*/
        // let warningPopup = new WarningBoxPopup();
        // warningPopup.text = "Are you sure to cancel";
        // this._boxPopupService.showWarningPopup(warningPopup)

        /*case 2: default*/
        this._boxPopupService.showWarningPopup()
            .then(function (dm) {
                if (dm)
                    n._redirectToList(n, 0);
            })
    }

    getEventracking(page = null) {
        let odr_num = this._orderDetail['odr_num'];
        let param = "?owner=" + odr_num + "&type=ob&page=" + page + "&limit=" + this.perPage;
        this.ODService.getEventracking(param).subscribe(
            data => {
                this.evenTracking = data.data;
                //pagin function
                this.Pagination = data['meta']['pagination'];
                this.Pagination['numLinks'] = 3;
                this.Pagination['tmpPageCount'] = this._Func.Pagination(this.Pagination);
            },
            err => {
                this.parseError(err);
            },
            () => {}
        );
    }

    changeOrderType($event:any) {
        if(this.ODForm.value['odr_type']=='XDK'){
            if($event.target.value!=='XDK'){
                this.fbdFunc.emptyControlArray(this.itemsArray);
                this.fbdFunc.setValueControl(this.ODForm,'qualifier',[]);
                this.addNewItem();
            }
        }

        if ($event.target.value === 'RTL') {
            this.getCarriers();
            const fields = ['carrier_code', 'dlvy_srvc_code', 'ship_to_phone'];
            this._validateRetailerInfo(fields);
        } else {
            // reset validation carrier_code, delivery service, ship_to_phone
            (<Control>this.ODForm.controls['carrier_code']).setErrors(null);
            (<Control>this.ODForm.controls['carrier']).setErrors(null);
            (<Control>this.ODForm.controls['dlvy_srvc_code']).setErrors(null);
            (<Control>this.ODForm.controls['ship_to_phone']).setErrors(null);
        }
    }

    private saveOverFlow() {

        this.messages = false;
        var target = this;
        var object = {};
        object = {
            qualifier: 'OFF',
            value: this.listOrderFlow
        };
        var orderId = this.params.get('id');
        
        this.ODService.saveOrderFlow(orderId, JSON.stringify(object)).subscribe(
            data => {
                target.showLoadingOverlay = false;
                // this.messages = this._Func.Messages('success', this._Func.msg('VR109'));
                // target._redirectToList(target);
            },
            err => {
                target.parseError(err);
                target.showLoadingOverlay = false;
            },
            () => {
                target.showLoadingOverlay = false;
            }
        );
    }


    //get Name
    private getLoggedUser() {
        this.loggerUser = localStorage.getItem('logedUser');
        this.loggerUser = JSON.parse('' + this.loggerUser);
    }

    //end by Nhan Truong


    private checkAllCarton($event) {

        var item = jQuery("#tabcarton tbody").find("input:checkbox");

        item.prop("checked", false);

        if ($event.target.checked) {

            item.prop("checked", true);
        }

    }

    private selectedCarton = [];

    private checkItemCarton($event, item, index) {

        var checkAll = jQuery("#tabcarton thead").find("input:checkbox");

        if (jQuery("#tabcarton tbody").find("input:checkbox:not(:checked)").length > 0) {

            checkAll.prop("checked", false);
        } else {

            checkAll.prop("checked", true);
        }

        if ($event.target.checked) {

            this.selectedCarton.push(item);
        } else {

            for (var i = 0; i < this.selectedCarton.length; i++) {

                if (item['pack_hdr_id'] == this.selectedCarton[i]['pack_hdr_id']) {

                    this.selectedCarton.splice(i, 1);
                }
            }

        }
    }

    private printCarton() {

        var tmp = [];
        if (jQuery("#tabcarton thead").find("input:checkbox:checked").length > 0) {

            tmp = this.allCarton;
        } else {
            tmp = this.selectedCarton;
        }

        let that = this;

        if (tmp.length > 0) {
            let packHdrId = [];
            for (let i in tmp) {

                if (tmp[i]['has_rfid']) {
                    that.showMessage('danger', that._Func.msg('PRCT01'));
                    packHdrId =  [];
                    break;
                }

                packHdrId.push(tmp[i].pack_hdr_id);

            }

            if (packHdrId.length < 1) {
                return;
            }

            let api = that._API.API_Pack + '/packs/' + that.orderId + '/print?pack_hdr_id=' + packHdrId.join();
            try {
                let xhr = new XMLHttpRequest();
                xhr.open("GET", api, true);
                xhr.setRequestHeader("Authorization", 'Bearer ' + that._Func.getToken());
                xhr.responseType = 'blob';

                xhr.onreadystatechange = function () {
                    if (xhr.readyState == 2) {
                        if (xhr.status == 200) {
                            xhr.responseType = "blob";
                        } else {
                            xhr.responseType = "text";
                        }
                    }

                    if (xhr.readyState === 4) {
                        if (xhr.status === 200) {
                            //update printed label value
                            for (let i = 0; i < that.allCarton.length; i++) {
                                for (let j = 0; j < tmp.length; j++)
                                    if (that.allCarton[i]['pack_hdr_id'] == tmp[j]['pack_hdr_id']) {
                                        that.allCarton[i]['is_print'] = 1;
                                    }
                            }
                            var today = new Date();
                            var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
                                "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
                            ];
                            var day = today.getDate();
                            var month = monthNames[today.getMonth()];
                            var year = today.getFullYear().toString().substr(2, 2);
                            var fileName = 'PackCarton_' + month + day + year;
                            var blob = new Blob([this.response], {type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'});
                            saveAs.saveAs(blob, fileName + '.pdf');
                        } else {
                            let errMsg = '';
                            if (xhr.response) {
                                try {
                                    var res = JSON.parse(xhr.response);
                                    errMsg = res.errors && res.errors.message ? res.errors.message : xhr.statusText;
                                }
                                catch (err) {
                                    errMsg = xhr.statusText
                                }
                            }
                            else {
                                errMsg = xhr.statusText;
                            }
                            if (!errMsg) {
                                errMsg = that._Func.msg('VR100');
                            }
                            that.showMessage('danger', errMsg);
                        }
                    }
                }
                xhr.send();
            } catch (err) {
                that.showMessage('danger', that._Func.msg('VR100'));
            }
        } else {
            that.showMessage('danger', that._Func.msg('PRCT01'));
        }

    }


    public showMessage(msgStatus, msg) {
        this.messages = {
            'status': msgStatus,
            'txt': msg
        }
        jQuery(window).scrollTop(0);
    }

    private openFormThirdParty:boolean=false;
    private _openFormThirdParty(){

        this.openFormThirdParty=true;
        setTimeout(()=>{
            this.openFormThirdParty=false;
        },300);
    }

    private separateListUOM() {
        this.uomListSliced['KG'] = this.uomList.filter(uom => uom['sys_uom_code'] === 'PL' || uom['sys_uom_code'] === 'KG');
        this.uomListSliced['NoKG'] = this.uomList.filter(uom => uom['sys_uom_code'] !== 'PL' && uom['sys_uom_code'] !== 'KG');
    }

    private removeLevel5Uom(){

        let uomList_temp = this.uomList.slice();
        uomList_temp.forEach((item)=>{
            if(item['level_id']==5){
                let index = this.uomList.indexOf(item);
                if(index>=0){
                    this.uomList.splice(index,1);
                }
            }
        });

    }

    private changeUOM(item:ControlGroup,id:any){

        let select_uom = this.get_uom_item(id);
        let current_uom = item.value['uom_code'];

        if( (select_uom['sys_uom_code'] == 'KG' ) && (current_uom!=='KG') || (select_uom['sys_uom_code'] !== 'KG' )
            && (current_uom=='KG')){
            this.fbdFunc.setValueControl(item,'uom_qty','');
            this.odrFunc.caclulatorPieceCtn(item,this.uomList);
        }

        this.fbdFunc.setValueControl(item,'uom_code',select_uom['sys_uom_code']);
        this.fbdFunc.setValueControl(item,'uom_name',select_uom['sys_uom_name']);

    }


    private get_uom_item(id:any){

        for(let itm of this.uomList){
            if(itm['sys_uom_id']==id){
                return itm;
            }
        }

    }


    private getUOMList(page = null) {
        this.ODService.getUOM()
            .subscribe(
                data => {
                    this.uomList = data.data;
                    this.odrFunc.uomList = data.data;
                    this.set_uom_id_default(this.uomList);
                    this.removeLevel5Uom();
                    // Separate UOM list
                    this.separateListUOM();
                },
                err => {},
                () => {}
            );
    }
    
    private onScroll($event:any){

    }

    private setClickedRow(arItems, row, item) {
        item.selected = !item.selected;
        if (item.selected) {
            this.selectedCarton.push(item);
        } else {

            for (var i = 0; i < this.selectedCarton.length; i++) {

                if (item['pack_hdr_id'] == this.selectedCarton[i]['pack_hdr_id']) {

                    this.selectedCarton.splice(i, 1);
                }
            }
        }
    }

    private checkedAll(allItems, isCheck) {
        allItems.forEach(item => item.selected = isCheck);
        this.selectedCarton = isCheck ? JSON.parse(JSON.stringify(allItems)) : [];
    }

    public showLoading(loading: boolean) {
        this.showLoadingOverlay = loading;
    }

    public reloadOrder(reload: boolean) {
        if (reload) {
            this.orderDetail();
        }
    }

    public updateScheduleRouteDate(id, data, cb) {
        this.showLoadingOverlay = true;
        this.ODService.updateScheduleRouteDate(id, JSON.stringify(data)).subscribe(
            data => {
                this.showLoadingOverlay = false;
                cb();
            },
            err => {
                this.showLoadingOverlay = false;
                this.parseError(err);
                cb();
            },
            () => {}
        );

    }


    public printPickingSlip() {
        let that = this;

        if (this.allowPickingSlip) {
            let api = that._API.API_ORDERS + '/' + that.orderId + '/order-picking-slip/print';
            try {
                let xhr = new XMLHttpRequest();
                xhr.open("GET", api, true);
                xhr.setRequestHeader("Authorization", 'Bearer ' + that._Func.getToken());
                xhr.responseType = 'blob';

                xhr.onreadystatechange = function () {
                    if (xhr.readyState == 2) {
                        if (xhr.status == 200) {
                            xhr.responseType = "blob";
                        } else {
                            xhr.responseType = "text";
                        }
                    }

                    if (xhr.readyState === 4) {
                        if (xhr.status === 200) {
                            var today = new Date();
                            var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
                                "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
                            ];
                            var day = today.getDate();
                            var month = monthNames[today.getMonth()];
                            var year = today.getFullYear().toString().substr(2, 2);
                            var fileName = 'PickingSlip_' + that._orderDetail['odr_num'] + '_' + month + day + year;
                            var blob = new Blob([this.response], {type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'});
                            saveAs.saveAs(blob, fileName + '.pdf');
                        } else {
                            let errMsg = '';
                            if (xhr.response) {
                                try {
                                    var res = JSON.parse(xhr.response);
                                    errMsg = res.errors && res.errors.message ? res.errors.message : xhr.statusText;
                                }
                                catch (err) {
                                    errMsg = xhr.statusText
                                }
                            }
                            else {
                                errMsg = xhr.statusText;
                            }
                            if (!errMsg) {
                                errMsg = that._Func.msg('VR100');
                            }
                            that.showMessage('danger', errMsg);
                        }
                    }
                }
                xhr.send();
            } catch (err) {
                that.showMessage('danger', that._Func.msg('VR100'));
            }
        } else {
            that.showMessage('danger', that._Func.msg('ODR004'));
        }
    }
    
    private calCompleteDate(control) {
        setTimeout(() => {
            if (this.ODForm.controls[control].value) {

                (<Control>this.ODForm.controls['req_cmpl_dt']).setErrors(null);
                var shipByDate = new Date(this.ODForm.controls[control].value);
                var tmp = new Date();
                var curentDate = new Date((tmp.getMonth() + 1) + '/' + tmp.getDate() + '/' + tmp.getFullYear());

                if (shipByDate < curentDate) {

                    (<Control>this.ODForm.controls['req_cmpl_dt']).updateValue(this.ODForm.controls[control].value);

                } else if ((shipByDate.getTime() - curentDate.getTime())/1000 >= 3*24*60*60) {

                    var dateOffset = (24*60*60*1000) * 3; //5 days
                    var myDate = shipByDate;
                    myDate.setTime(myDate.getTime() - dateOffset);

                    var month = (myDate.getMonth() + 1) > 9 ? (myDate.getMonth() + 1) : '0' + (myDate.getMonth() + 1);
                    (<Control>this.ODForm.controls['req_cmpl_dt']).updateValue( month + '/' + myDate.getDate() + '/' + myDate.getFullYear());

                } else {

                    var month = (curentDate.getMonth() + 1) > 9 ? (curentDate.getMonth() + 1) : '0' + (curentDate.getMonth() + 1);
                    (<Control>this.ODForm.controls['req_cmpl_dt']).updateValue( month + '/' + curentDate.getDate() + '/' + curentDate.getFullYear());
                }
            }
        }, 250);

    }

    private saveComment() {

        if (this.Action == 'new') {
            return ;
        }
        let n = this;
        this.showLoadingOverlay = true;
        var data = {};
        data['cus_notes'] = this.ODForm.value['cus_notes'];
        data['in_notes'] = this.ODForm.value['in_notes'];
        data['csr_notes'] = this.ODForm.value['csr_notes'];
        data['rush_odr'] = this.ODForm.value['rush_odr'];
        data = JSON.stringify(data);
        this.ODService.updateComment(this.orderId, data).subscribe(
            data => {

                this.messages = this._Func.Messages('success', this._Func.msg('CSRC01'));
                this.showLoadingOverlay = false;
            },
            err => {
                this.showLoadingOverlay = false;
                this.parseError(err);
            },
            () => {}
        );
    }
    
    public itemFiles: any[] = [];
    public changeFile(event, i: number) {
		this.itemFiles[i] = <Array<File>> event.target.files[0];
    }
    
    maxFile = 5;
    numbers = [0];
    curFile = 1;
    addNewFile() {
        if (this.curFile < this.maxFile) {
            this.curFile++;
            this.numbers = Array.from(Array(this.curFile), (x,i) => i);
        }
    }
    
    public uploadFile(orderNum) {
		const that = this;
		that.messages = null;

		if (this.itemFiles.length) {
			let formData:any = new FormData();
            let xhr = new XMLHttpRequest();
            let totalFile = 0;
            this.itemFiles.forEach(item => {
                if (item) {
                    formData.append("file[]", item, item.name);
                    totalFile++;
                }
            });

            if (!totalFile) {
                return;
            }

			that.showLoadingOverlay = true;
			xhr.open("POST", that._API.API_ORDERS_CORE + '/integrate-document-to-dms/' + orderNum, true);
			xhr.setRequestHeader("Authorization", 'Bearer ' + that._Func.getToken());
			xhr.onreadystatechange = function () {
				if (xhr.readyState == 2) {
					if (xhr.status == 200) {
						xhr.responseType = "blob";
					} else {
						xhr.responseType = "text";
					}
				}

				if (xhr.readyState == 4) {
                    let msg = '';

                    if (xhr.response) {
                        try {
                            var res = JSON.parse(xhr.response);
                            msg = res.errors && res.errors.message ? res.errors.message : res.data.message;
                        }
                        catch (err) {
                            msg = xhr.statusText
                        }
                    }
                    else {
                        msg = xhr.statusText;
                    }

                    if (!msg) {
                        msg = that._Func.msg('VR100');
                    }

                    if (xhr.status === 200 || xhr.status === 201) {
                        that.messages = that._Func.Messages('success', msg);
                        that._redirectToList(that);
                    } else {
                        that.messages = that._Func.Messages('danger', 'Order was created successfully. But ' + msg);
                        that._redirectToList(that, 2000);
                    }
                    that.showLoadingOverlay = false;
				}
			};
			xhr.send(formData);
		}
    }
    
    public printPackingSlip() {
        let that = this;

        if (this.allowPickingSlip) {
            let api = that._API.API_ORDERS + '/' + that.orderId + '/order-packing-slip/print';
            try {
                let xhr = new XMLHttpRequest();
                xhr.open("GET", api, true);
                xhr.setRequestHeader("Authorization", 'Bearer ' + that._Func.getToken());
                xhr.responseType = 'blob';

                xhr.onreadystatechange = function () {
                    if (xhr.readyState == 2) {
                        if (xhr.status == 200) {
                            xhr.responseType = "blob";
                        } else {
                            xhr.responseType = "text";
                        }
                    }

                    if (xhr.readyState === 4) {
                        if (xhr.status === 200) {
                            var today = new Date();
                            var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
                                "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
                            ];
                            var day = today.getDate();
                            var month = monthNames[today.getMonth()];
                            var year = today.getFullYear().toString().substr(2, 2);
                            var fileName = 'PackingSlip_' + that._orderDetail['odr_num'] + '_' + month + day + year;
                            var blob = new Blob([this.response], {type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'});
                            saveAs.saveAs(blob, fileName + '.pdf');
                        } else {
                            let errMsg = '';
                            if (xhr.response) {
                                try {
                                    var res = JSON.parse(xhr.response);
                                    errMsg = res.errors && res.errors.message ? res.errors.message : xhr.statusText;
                                }
                                catch (err) {
                                    errMsg = xhr.statusText
                                }
                            }
                            else {
                                errMsg = xhr.statusText;
                            }
                            if (!errMsg) {
                                errMsg = that._Func.msg('VR100');
                            }
                            that.showMessage('danger', errMsg);
                        }
                    }
                }
                xhr.send();
            } catch (err) {
                that.showMessage('danger', that._Func.msg('VR100'));
            }
        } else {
            that.showMessage('danger', that._Func.msg('ODR004'));
        }
    }
    
    public addAutoComplete() {
        const that = this;
        this.googleAPI.initAutoComplete('order-autocomplete').then(() => {
            that.autocomplete = that.googleAPI.autocomplete;
            that.autocomplete.addListener('place_changed', function () {
                const place = that.autocomplete.getPlace();
                let addr = '';
                let state = '';
                let zipCode = '';
                let city = '';
                let country = '';
                // var length = place.address_components.length;
                if (place.address_components) {
                    for (let i = 0; i < place.address_components.length; i++) {
                        const addressType = place.address_components[i].types[0];
                        if (addressType === 'street_number') { // number address
                            addr += place.address_components[i].short_name;
                        }
                        if (addressType === 'route') { // street
                            addr += ' ' + place.address_components[i].short_name;
                        }
                        if (addressType === 'locality') { // city
                            city = place.address_components[i].short_name;
                        }
                        if (addressType === 'administrative_area_level_1') { // state
                            state = place.address_components[i].short_name;
                        }
                        if (addressType === 'postal_code') { // zip code
                            zipCode = place.address_components[i].short_name;
                        }
                        if (addressType === 'country') { // country
                            country = place.address_components[i].short_name;
                        }
                    }

                    // let location = place.geometry.location;
                    const paramObj = {
                        ship_to_add_1: addr,
                        ship_to_city: city,
                        ship_to_zip: zipCode,
                        ship_to_country: country,
                        ship_to_state: state
                    };
                    that._autoCompleteUpdateForm(that.ODForm, paramObj);
                }
            });
            that._turnOffAutofill();
        });
    }

    private _turnOffAutofill() {
        setTimeout(() => {
            jQuery('.address').attr('autocomplete', 'new-password');
        }, 1500);
    }

    private _autoCompleteUpdateForm(form, paramObj) {
        for (const key in paramObj) {
            if (paramObj.hasOwnProperty(key)) {
                this.setField(key, paramObj[key]);
            }
        }
        this.GetState(paramObj.ship_to_country, paramObj.ship_to_state);
    }

    getZipcode(evt) {
        this.trimFields(['ship_to_zip', 'ship_to_city']);
        const data = this.ODForm.value;
        if (data['ship_to_country'] === 'US') {
            this.googleAPI.getAddress(data['ship_to_zip']).then(address => {
                if (address['city'].toLowerCase() !== data['ship_to_city'].toLowerCase() || address['state'].toLowerCase() !== data['ship_to_state'].toLowerCase()) {
                    // (<Control>this.ODForm.controls['ship_to_zip']).setErrors({invaildValue: true});
                }
            });
        }
    }

    public setState(val: string) {
        this.setField('ship_to_state', val);
    }

    public setField(fieldname: string, value: any) {
        (<Control>this.ODForm.controls[fieldname]).updateValue(value);
    }

    resetZipcode() {
        this.setField('ship_to_zip', '');
    }

    trimFields(fields: string[]) {
        fields.forEach(field => {
            const val = this.ODForm.value[field];
            this.setField(field, val? val.trim() : val);
        })
    }

    private getCarriers() {
        const params = '?md_type=CR&md_sts=AC&limit=999';
        this.showLoadingOverlay = true;
        this.ODService.getCarrierList(params).subscribe(
            data => {
              this.showLoadingOverlay = false;
              this.carriers = data['data'];
            },
            err => {
              this.showLoadingOverlay = false;
              this.showMessage('danger', this._Func.parseErrorMessageFromServer(err));
            }
        );
    }

    public changeCarrier($event: any) {
        const carrier_code = $event.target.value;
        const carrierIndex = $event.target.selectedIndex;
        const carrier = carrier_code.length ? this.carriers[carrierIndex-1]['md_name'] : '';
        (<Control>this.ODForm.controls['carrier_code']).updateValue(carrier_code);
        (<Control>this.ODForm.controls['dlvy_srvc']).updateValue('');
        (<Control>this.ODForm.controls['dlvy_srvc_code']).updateValue('');
        (<Control>this.ODForm.controls['carrier']).updateValue(carrier);
        this._getDeliveryServices(carrier_code);
        // 
        (<Control>this.ODForm.controls['carrier_code']).markAsTouched();
        const fields = ['carrier_code', 'dlvy_srvc_code'];
        this._validateRetailerInfo(fields);
    }

    _validateRetailerInfo(fields: string[]) {
        setTimeout(() => {
            fields.forEach(field => this._validateField(field));
        });
    }

    _validateField(field: string) {
        if (<Control>this.ODForm.controls[field].value) {
            this.fbdFunc.setErrorControl(this.ODForm, field, null);
        } else {
            this.fbdFunc.setErrorControl(this.ODForm, field, {'required': true});
        }
    }

    public changeDeliveryService($event: any) {
        const val = $event.target.value;
        const i = $event.target.selectedIndex;
        const dService = val.length ? this.deliveryServices[i-1]['md_name'] : '';
        (<Control>this.ODForm.controls['dlvy_srvc_code']).updateValue(val);
        (<Control>this.ODForm.controls['dlvy_srvc']).updateValue(dService);
        (<Control>this.ODForm.controls['dlvy_srvc_code']).markAsTouched();
        const fields = ['dlvy_srvc_code'];
        this._validateRetailerInfo(fields);
    }

    public inputShipToPhone($event: any) {
        const val = $event.target.value;
        (<Control>this.ODForm.controls['ship_to_phone']).updateValue(val);
        (<Control>this.ODForm.controls['ship_to_phone']).markAsTouched();
    }

    private _getDeliveryServices(carrier_code: string) {
        if (!carrier_code) {
            return;
        }
        const params = `?md_type=${carrier_code}&md_code=&md_sts=AC&limit=999`;
        this.isLoadingDeliServices = true;
        this.ODService.getDeliveryService(params).subscribe(
            data => {
                this.isLoadingDeliServices = false;
                this.deliveryServices = data['data'];
            },
            err => {
                this.isLoadingDeliServices = false;
            }
        );
    }

    printShippingLabel() {
        var tmp = [];
        if (jQuery("#tabcarton thead").find("input:checkbox:checked").length > 0) {

            tmp = this.allCarton;
        } else {
            tmp = this.selectedCarton;
        }

        let that = this;

        if (tmp.length === 1) {
            let packHdrId = tmp[0]['pack_hdr_id'];
            
            let api = that._API.API_Pack_v2 + '/packs/download-shipping-label/' + packHdrId;
            try {
                let xhr = new XMLHttpRequest();
                xhr.open("GET", api, true);
                xhr.setRequestHeader("Authorization", 'Bearer ' + that._Func.getToken());
                xhr.responseType = 'blob';

                xhr.onreadystatechange = function () {
                    if (xhr.readyState == 2) {
                        if (xhr.status == 200) {
                            xhr.responseType = "blob";
                        } else {
                            xhr.responseType = "text";
                        }
                    }

                    if (xhr.readyState === 4) {
                        if (xhr.status === 200) {
                            //update printed label value
                            for (let i = 0; i < that.allCarton.length; i++) {
                                for (let j = 0; j < tmp.length; j++)
                                    if (that.allCarton[i]['pack_hdr_id'] == tmp[j]['pack_hdr_id']) {
                                        that.allCarton[i]['is_print'] = 1;
                                    }
                            }
                            var today = new Date();
                            var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
                                "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
                            ];
                            var day = today.getDate();
                            var month = monthNames[today.getMonth()];
                            var year = today.getFullYear().toString().substr(2, 2);
                            var fileName = 'PackCarton_ShippingLabel_' + month + day + year;
                            var blob = new Blob([this.response], {type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'});
                            saveAs.saveAs(blob, fileName + '.pdf');
                        } else {
                            let errMsg = '';
                            if (xhr.response) {
                                try {
                                    var res = JSON.parse(xhr.response);
                                    errMsg = res.errors && res.errors.message ? res.errors.message : xhr.statusText;
                                }
                                catch (err) {
                                    errMsg = xhr.statusText
                                }
                            }
                            else {
                                errMsg = xhr.statusText;
                            }
                            if (!errMsg) {
                                errMsg = that._Func.msg('VR100');
                            }
                            that.showMessage('danger', errMsg);
                        }
                    }
                }
                xhr.send();
            } catch (err) {
                that.showMessage('danger', that._Func.msg('VR100'));
            }
        } else {
            that.showMessage('danger', that._Func.msg('PRCT02'));
        }
    }

    undoPackCartons() {
        const that = this;
        const warningPopup = new WarningBoxPopup();
        warningPopup.text = this._Func.msg('ODR009');
        this._boxPopupService.showWarningPopup(warningPopup).then(function (ok) {
            if (!ok) {
                return;
            } else {
                that.showLoadingOverlay = true;
                that.ODService.undoPackCartons(that.orderId).subscribe(
                    data => {
                        that.showLoadingOverlay = false;
                        that.showMessage('success', that._Func.msg('ODR008'));
                        that.reloadOrder(true);
                        that.getCarton(that.currentPage);
                    },
                    err => {
                        that.showLoadingOverlay = false;
                        that.showMessage('danger', that._Func.parseErrorMessageFromServer(err));
                    }
                );
            }
        });
    }
    
    changeRush(evt) {
        if (this.canWHComment&&this.Action == 'comment') {
            (<Control>this.ODForm.controls['rush_odr']).updateValue(evt.target.value)
            this.saveComment();
        }
    }

    private  autoCompleteCarrier(key){

        this.Loading['carrier']=false;
        this.messages = false;
        let param = `?name=${this.ODForm.value['carrier']}&limit=10`;
        let enCodeSearchQuery = this._Func.FixEncodeURI(param);

        if (this.ODForm['subscribe']) {
            this.ODForm['subscribe'].unsubscribe();
        }

        if(key){
            this.Loading['carrier']=true;
            this.fbdFunc.setErrorControl(this.ODForm,'carrier',null);
            this.ODForm['subscribe'] = this.ODService.getCarrierTypeOther(enCodeSearchQuery).debounceTime(400).distinctUntilChanged().subscribe(
                data => {
                    this.Loading['carrier']=false;
                    this.carriersTypeOther = data.data;
                    let item = this.odrFunc.findKeyInList(this.carriersTypeOther,'carrier_name', this._Func.trim(key));
                    if(item){
                        this.selectedCarrier(item);
                    }

                },
                err => {
                    this.Loading['carrier']=false;
                    this.parseError(err);
                },
                () => {
                }
            );
        }

    }

    private selectedCarrier(item){
        (<Control>this.ODForm.controls['carrier']).updateValue(item.carrier_name);
    }

    public printRequestSlip() {
        let that = this;

        // if (this.allowPickingSlip) {
            let api = that._API.API_ORDERS + '/' + that.orderId + '/order-request-slip/print';
            try {
                let xhr = new XMLHttpRequest();
                xhr.open("GET", api, true);
                xhr.setRequestHeader("Authorization", 'Bearer ' + that._Func.getToken());
                xhr.responseType = 'blob';

                xhr.onreadystatechange = function () {
                    if (xhr.readyState == 2) {
                        if (xhr.status == 200) {
                            xhr.responseType = "blob";
                        } else {
                            xhr.responseType = "text";
                        }
                    }

                    if (xhr.readyState === 4) {
                        if (xhr.status === 200) {
                            var today = new Date();
                            var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
                                "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
                            ];
                            var day = today.getDate();
                            var month = monthNames[today.getMonth()];
                            var year = today.getFullYear().toString().substr(2, 2);
                            var fileName = 'RequestSlip_' + that._orderDetail['odr_num'] + '_' + month + day + year;
                            var blob = new Blob([this.response], {type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'});
                            saveAs.saveAs(blob, fileName + '.pdf');
                        } else {
                            let errMsg = '';
                            if (xhr.response) {
                                try {
                                    var res = JSON.parse(xhr.response);
                                    errMsg = res.errors && res.errors.message ? res.errors.message : xhr.statusText;
                                }
                                catch (err) {
                                    errMsg = xhr.statusText
                                }
                            }
                            else {
                                errMsg = xhr.statusText;
                            }
                            if (!errMsg) {
                                errMsg = that._Func.msg('VR100');
                            }
                            that.showMessage('danger', errMsg);
                        }
                    }
                }
                xhr.send();
            } catch (err) {
                that.showMessage('danger', that._Func.msg('VR100'));
            }
        // } else {
        //     that.showMessage('danger', that._Func.msg('ODR004'));
        // }
    }
    
    private updateDetailOrderFlow($event) {
        this.fbdFunc.setValueControl(this.ODForm,'qualifier',$event);
        this.listOrderFlow = $event;
    }

    /**
     * Update traking number
     * 
     * @param $event 
     * @param packHdrId 
     */
    private changeTrackingNumber($event, packHdrId) {
        let dataInputTracking = $event.target.value;
        let dataJson = {
                            "pack_hdr_id": packHdrId,
                            "tracking_number": dataInputTracking,
                        }
        this.showLoadingOverlay = true;
        this.ODService.updateTrakingNumber(this.orderId, JSON.stringify(dataJson))
        .subscribe(
            (data) => {
                this.showLoadingOverlay = false;
                this.showMessage('success', data.msg);
                this.getCarton(this.currentPage);
            },
            (err) => {
                this.parseError(err);
                this.showLoadingOverlay = false;
            },
            () => { }
        )
    }

    private changeOrderTrackingNumber($event) {
        let dataJson = {
            "track_num": $event.target.value
        }
        this.showLoadingOverlay = true;
        this.ODService.updateOrderTrackingNumber(this.orderId, JSON.stringify(dataJson))
        .subscribe(
            (data) => {
                this.showLoadingOverlay = false;
                this.showMessage('success', this._Func.msg('ODR012'));
                this.getCarton(this.currentPage);
            },
            (err) => {
                this.parseError(err);
                this.showLoadingOverlay = false;
            },
            () => { }
        )
    }

}