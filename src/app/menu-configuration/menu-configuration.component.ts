import {Component, Input, ElementRef} from '@angular/core';
import {Response} from '@angular/http';
import { Router, RouterLink, ROUTER_DIRECTIVES } from '@angular/router-deprecated';
import {TreeView} from './tree-view';
import {MenuConfigurationServices} from './menu-configuration.service';
import { NgForm, FORM_DIRECTIVES}    from '@angular/common';
import {Functions,UserService} from '../common/core/load';
declare var jQuery: any;

@Component({
  selector: 'tree-view',
  templateUrl: 'menu-configuration.component.html',
  providers: [MenuConfigurationServices],
  directives: [TreeView, FORM_DIRECTIVES],
  pipes: []
})
export class MenuConfigurationComponent {

 public GroupMenu=[];
 public SortMenuData=[];
 public ListMenu:Array<any>=[];
 public selectedMenuGroup;
 public MenuGroupName;
 public MenuDes;
 public Addnew=false;
 public selectedListMenu=false;
 public activeBtnUpdate:boolean=false;
 public Loading:Array<any>=[
   { getGroupMenu: false},
   { getListMenu: false },
   { updateMenu: false },
   { createGroupMenu: false },
   { updateGroupMenu: false }
 ];
 public ListPermission=[];
 public messages;
 elementRef: ElementRef;
 public active:boolean;

 /*Permission require*/
 private viewMenuConfiguration:boolean=false;
 private createMenu:boolean=false;

 constructor(
   public router: Router,
  elementRef: ElementRef,
  private _Func: Functions,
  private _user:UserService,
  private _menucofService: MenuConfigurationServices
  )
  {

    /* Action for jQuery*/
     this.elementRef = elementRef;

     /*get list group*/
     this.getGroupMenu();

     /*check permission*/
     this._user.GetPermissionUser().subscribe(
      data => {
            this.ListPermission=data;
            this.viewMenuConfiguration = this._user.RequestPermission(data,'configMenu');
             /* Check orther permission if View allow */
            if(this.viewMenuConfiguration)
            {
              this.createMenu = this._user.RequestPermission(data, 'configMenu');
            }
            else{
              this.router.parent.navigateByUrl('/deny');
            }
        },
        err => {},
        () => {}
      );



  }

   ngOnInit() {
  }

  UpdateMenu(e)
  {
      var list   = e.length ? e : jQuery(e.target),
      output = list.data('output');
      output.val(JSON.stringify(list.nestable('serialize')));//, null, 2));
      this.SortMenuData = list.nestable('serialize');
  }

  getGroupMenu() {

    this.Loading['getGroupMenu'] = true; // active show loading

    this._menucofService.getGroupMenu().subscribe(
        data => {
          this.GroupMenu = data;
          this.selectedMenuGroup=this.GroupMenu[0];
          /* get list menu default*/
          let menu_group_id=this.selectedMenuGroup.menu_group_id;
          this.getListMenu(this.selectedMenuGroup);
          this.Loading['getGroupMenu'] = false;
        },
        err => {
            this.Loading['getGroupMenu'] = false;
            if (err == "Failed to load resource: the server responded with a status of 504 (Gateway Time-out)" || err == "504 (Gateway Time-out)")
            {
              alert("Gateway Time-out");
            }
        },
        () => {}
      );

  }

  getListMenu(item) {

      this.Loading['getListMenu'] = true; // active show loading
      let menu_group_id= item.menu_group_id;

      this._menucofService.GetListMenu(menu_group_id).subscribe(
      data => {

          this.ListMenu = data;
          this.Loading['getListMenu'] = false;
          setTimeout(() => {
            this.editMenuList();
          }, 1000);

        },
        err => {},
        () => {}
      );
  }

  editMenuList()
  {
    let n = this;
    jQuery(this.elementRef.nativeElement).find('.dd').nestable({
      expandBtnHTML   : '<button class="btn-collapse" data-action="expand" type="button">Expand</button>',
      collapseBtnHTML : '<button class="btn-collapse" data-action="collapse" type="button">Collapse</button>',
      maxDepth: 2
    }).on('change', function() {
      n.UpdateMenu(jQuery(n.elementRef.nativeElement).find("#nestable").data('output', jQuery('#nestable-output')));
    });
  }


  /*
  Add new menu item
  */
  AddNewItem(item)
  {
    /* First : add to array */
    item.menu_id=null;
    item.nodes =[];
    this.ListMenu.push(item);
  }

  /*=========

  SaveMenuConfig

  ===========*/

  UpdateMenuConfig()
  {

    /*
      Permission = UpdateMenuConfig
    */
    if (this.createMenu)
    {
      // show loading
      this.Loading['updateMenu'] = true;

      let data;

      if(this.SortMenuData.length)
      {
        /*update json*/
        this.UpdateMenu(jQuery(this.elementRef.nativeElement).find("#nestable"));
        data = this.SortMenuData;
      }
      else{
        // if do not sortable
        // check array ListMenu
        data = this.ListMenu;
      }

      data = 'param='+JSON.stringify(data);

        this._menucofService.UpdateMenu(this.selectedMenuGroup.menu_group_id,data).subscribe(
        data => {
          this.Loading['updateMenu'] = false;
          this.messages = this._Func.Messages('success', 'Updates completed successfull');
        },
        err => {
          this.messages = this._Func.Messages('danger', 'Error unknown ! please try again later or contact administrator');
        },
        () => {}
      );

    }


  }

  /*
    Add New Menu Group
  */
  AddNewGroup(data : any): void {

    this.Loading['createGroupMenu']=true;

    let param = 'name=' + data.MenuGroupNameNew + '&description=' + data.MenuDesNew;

       this._menucofService.CreateNewMenuGroup(param).subscribe(
           data => {

               this.Loading['createGroupMenu'] = false;

               /*push data to array GroupMenu current*/
               this.GroupMenu.push(data);

               /*set empty Listmenu*/
               this.ListMenu = [];

               /*select that self*/
               this.selectedListMenu=true;
               /*
                 assign value for selectedMenuGroup
               */
               this.selectedMenuGroup = data;

           },
           err => {},
           () => {}
       );


  }

  /* UpDateMenuGroup */
  UpDateMenuGroup(data : any):void
  {
    this.Loading['createGroupMenu'] = true;

    let param = 'name=' + this.selectedMenuGroup.name + '&description=' + this.selectedMenuGroup.description;

       this._menucofService.UpdateMenuGroup(this.selectedMenuGroup.name,param).subscribe(
           data => {

               this.Loading['createGroupMenu'] = false;

               /*push data to array GroupMenu current*/
               this.GroupMenu.push(data);

               /*set empty Listmenu*/
               this.ListMenu = [];

               /*select that self*/
               this.selectedListMenu=true;
               /*
                 assign value for selectedMenuGroup
               */
               this.selectedMenuGroup = data;

           },
           err => {},
           () => {}
       );


  }

  /*
  removeGroup
  */
  removeGroup()
  {
     /*remove list menu*/
     this._menucofService.DeleteGroup(this.selectedMenuGroup.menu_group_id).subscribe(
       (res: Response) => {

         let mes;

         if(res.status==204)
         {
           mes = {
             status: "success",
             txt: "<strong>Delete menu group success!</strong>"
           };
         }
         else{
           mes = {
             status: "danger",
             txt: "<strong>Delete menu group error!</strong>"
           };
         }

         this.messages.push(mes);

       },
       err => {},
       () => {}
     );
  }



}
