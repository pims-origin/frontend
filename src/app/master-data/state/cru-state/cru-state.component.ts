import {Component} from '@angular/core';
import { FORM_DIRECTIVES,ControlGroup,FormBuilder, Control ,Validators} from '@angular/common';
import {RouteParams,Router } from '@angular/router-deprecated';
import {API_Config, UserService, Functions} from '../../../common/core/load';
import { ValidationService } from '../../../common/core/validator';
import {StateService} from '../state-service';
import {CountryService} from '../../country/country-service';
import { Http } from '@angular/http';
import {GoBackComponent} from "../../../common/component/goBack.component";
import {ErrorMessageForControlService} from "../../../common/services/error-message-for-control.service";
import {BoxPopup} from "../../../common/popup/box-popup";
import {WarningBoxPopup} from "../../../common/popup/warning-box-popup";
import {BoxPopupService} from "../../../common/popup/box-popup.service";
import {JwtHelper} from "angular2-jwt/angular2-jwt";
import {WMSBreadcrumb,WMSMessages} from '../../../common/directives/directives';
declare var jQuery: any;
@Component({
    selector: 'cru-state',
    providers: [StateService,CountryService,FormBuilder,ValidationService,ErrorMessageForControlService, BoxPopupService ],
    directives: [FORM_DIRECTIVES,GoBackComponent,WMSMessages,WMSBreadcrumb],
    templateUrl: 'cru-state.component.html',
})

export class CURStateComponent {

    public loading = [];
    public showLoadingOverlay = false;
    constructor(
        private _http: Http,
        private _func: Functions,
        private _user: UserService,
        private params: RouteParams,
        private _Valid: ValidationService,
        private _ErrMsgControlService: ErrorMessageForControlService,
        private _stateService: StateService,
        private _countryService: CountryService,
        private _boxPopupService: BoxPopupService,
        private fb: FormBuilder,
        private _router: Router) {
        
        this.checkPermission();

        this._ErrMsgControlService.errorMsg.invalidSpace = 'is required.';  // set error message for invalid space
        this.stateID=this.params.get('id');

        this.buildForm();
        this.getCountryList();
        if(this.stateID) {
            this.action = this.params.get('edit') == 'edit' ? 'edit' : 'view';
            this._stateService.getStateDetail(this.stateID).subscribe(
                data => {
                    this.hasState = true;
                    this.viewCountryName = data['sys_country_name'];
                    this.viewCountryId = data['sys_state_country_id'];
                    // console.log('get state detail success', data, this.viewCountryName);
                    this.stateForm =this.fb.group({
                        sys_state_code: [data['sys_state_code'], Validators.compose([Validators.required, this._Valid.validateSpace, this._Valid.invalidLength2Or3])],
                        sys_state_name: [data['sys_state_name'], Validators.compose([Validators.required, this._Valid.validateSpace, this._Valid.invalidNameStr])],
                        sys_state_country_id: [data['sys_state_country_id'], Validators.required],
                        sys_state_desc: [data['sys_state_desc']],
                    });

                },
                err => {
                    this._router.parent.navigateByUrl('/master-data/state');
                },
                () => {}
            );
            if(this.action == 'view') {
                this.isView = true;
                this.TitlePage='View State';
            }
            else {
                this.actionText = "UPDATE";
                this.TitlePage='Edit State';
            }

        }
        else {
            this.action = 'new';
            this.actionText = "SAVE";
            this.TitlePage='Add State'
        }
    }

    private checkPermission() {
        this.showLoadingOverlay = true;
        this._user.GetPermissionUser().subscribe(// viewState
            data => {
                if((this.action == 'view' && !this._user.RequestPermission(data,'viewState')) || (this.action == 'edit' && !this._user.RequestPermission(data,'editState')) || (this.action == 'new' && !this._user.RequestPermission(data,'createState'))) {
                    this._router.parent.navigateByUrl('/deny');
                }
                this.showLoadingOverlay = false;
            },
            err => {
                this.showMessage('danger', this._func.parseErrorMessageFromServer(err));
                this.showLoadingOverlay = false;
            }
        );
    }

    private showMessage(msgStatus, msg) {
        this.messages = {
            'status': msgStatus,
            'txt': msg
        }
        jQuery(window).scrollTop(0);
    }
    
    private countryList: Array<any> = [];
    private viewCountryName: string = '';
    private viewCountryId = '';
    private messages;
    private action;
    private actionText;
    private TitlePage='';
    private hasState:boolean = false;
    private stateID;
    private isView:boolean = false;

    stateForm: ControlGroup;

    private getCountryList(page = null) {
        this._countryService.Country().subscribe(
            data => {
                this.countryList = data.data;
            },
            err => {},
            () => {}
        );
    }

    buildForm() {
        this.stateForm =this.fb.group({
            sys_state_code: [null, Validators.compose([Validators.required, this._Valid.validateSpace, this._Valid.invalidLength2Or3])],
            sys_state_name: [null, Validators.compose([Validators.required, this._Valid.validateSpace, this._Valid.invalidNameStr])],
            sys_state_country_id: ['', Validators.required],
            sys_state_desc: [null]
        });
    }

    private flagsubmitForm:boolean=false;
    submitForm(data :Object):void {

        this.flagsubmitForm = true;
        // console.log('data form >>', data);
        this._ErrMsgControlService._genErrMsgForMultiControls(this.stateForm);
        if(this.stateForm.valid) {

            let stringData = JSON.stringify(data);
            if (this.action == 'edit') {
                this.update(this.stateID, stringData);
            }
            else {
                this.addNew(stringData);
            }

        }
        // else{
        //   this.messages = this._func.Messages('danger', this._func.msg('VR108'));
        // }
    }

    update(stateID, stringData) {
        let that = this;
        this.loading['AddUpdate']=true;
        this._stateService.updateState(stateID,stringData).subscribe(
          data => {
            this.loading['AddUpdate']=false;
            this.messages = this._func.Messages('success', this._func.msg('VR109'));

            setTimeout(function(){
                that._router.parent.navigateByUrl('/master-data/state');
            }, 600);
          },
          err => {
            this.showMessage('danger', this._func.parseErrorMessageFromServer(err));
            this.loading['AddUpdate']=false;
          },
          () => {}
        );
    }

    addNew(stringData) {
        let that = this;
        this.loading['AddUpdate']=true;
        this._stateService.addNewState(stringData).subscribe(
            data => {
                this.loading['AddUpdate']=false;
                this.messages = this._func.Messages('success', this._func.msg('VR107'));
                setTimeout(function(){
                    that._router.parent.navigateByUrl('/master-data/state');
                }, 600);
            },
            err => {
                this.showMessage('danger', this._func.parseErrorMessageFromServer(err));
                this.loading['AddUpdate']=false;
            },
            () => {}
        );
    }

    cancel() {
        let n = this;
        /*case 1: modify text*/
        // let warningPopup = new WarningBoxPopup();
        // warningPopup.text = "Are you sure to cancel";
        // this._boxPopupService.showWarningPopup(warningPopup)

        /*case 2: default*/
        this._boxPopupService.showWarningPopup()
            .then(function (dm) {
                if(dm)
                    n._router.navigateByUrl('/master-data/state');

            });
    }

    detectControlErr(control) {
        // this._genErrMsgForControl(control, this.errorMsg);
        this._ErrMsgControlService._genErrMsgForControl(control);
    }

}
