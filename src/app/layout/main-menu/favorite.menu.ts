import {Component, Input} from '@angular/core';
import {MainMenuComponent} from "./main-menu.component";
import {Http, Response, Headers, HTTP_BINDINGS } from '@angular/http';
import {MainMenuService} from "./main-menu.service";

@Component({
  selector: 'favorite-menu',
  templateUrl: 'favorite.menu.html',
  directives: [FavoriteMenu],
  providers: [MainMenuService],
})
export class FavoriteMenu {
  @Input() FavoriteMenu: Array<MainMenuComponent>;

  public active:boolean = false;


  constructor(public _MenuService: MainMenuService) {}
  /*
        Remove by click button
    */
  removeItemFavorite(item) {

    let index = this.FavoriteMenu.indexOf(item);
    /*
     Remove item in Json | dislay
   */
    this.FavoriteMenu.splice(index, 1);

    /*delete call from service*/
    this._MenuService.DeleteItemMenuFavorite(item.menu_id)
      .subscribe(
      (res: Response) => {
        return res.status === 204 ? console.log('delete done') : alert('Error delete!');
      },
      err => this.handleError(err)
      );

  }
  toggleMenu()
  {
    this.FavoriteMenu.forEach(
    item => { item['active'] = !item.active; console.log(item); }
    );
  }
  private handleError(error: any) {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }
}
