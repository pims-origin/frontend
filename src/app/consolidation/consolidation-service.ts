import { Component, Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Http, Response } from '@angular/http';
import { API_Config, Functions } from '../common/core/load';

@Injectable()
export class ConsolidationServices {

  public AuthHeader = this._Func.AuthHeader();
  public AuthHeaderPost = this._Func.AuthHeaderPost();
  private apiService = this._API.API_GOODS_RECEIPT_MASTER;

  constructor(private _Func: Functions, private _API: API_Config, private http: Http) {
  }

  autocompleteLPNLoc(params) {
    return this.http.get(this.apiService + '/loc-lpn' + params, { headers: this.AuthHeader })
      .map((res: Response) => res.json());
  }

  callConsolidation(params) {
    return this.http.put(this.apiService + '/consolidation', params, { headers: this._Func.AuthHeaderPostJson() })
      .map((res: Response) => res.json());
  }

}
