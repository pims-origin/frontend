import { Component} from '@angular/core';
import { Http } from '@angular/http';
import { Router, RouterLink, ROUTER_DIRECTIVES, RouteParams } from '@angular/router-deprecated';
import 'ag-grid-enterprise/main';
import 'rxjs/add/operator/map';
import {API_Config} from "../../../common/core/API_Config";
import {UserService} from "../../../common/users/users.service";
import {Functions} from "../../../common/core/functions";
import {BoxPopup} from "../../../common/popup/box-popup";
import {WarningBoxPopup} from "../../../common/popup/warning-box-popup";
import {BoxPopupService} from "../../../common/popup/box-popup.service";
import {JwtHelper} from "angular2-jwt/angular2-jwt";
import {WMSBreadcrumb,WMSMessages} from '../../../common/directives/directives';
declare var jQuery: any;

@Component ({
    selector: 'edit-location-type',
    templateUrl: 'edit-location-type.component.html',
    providers: [BoxPopupService],
    directives: [WMSMessages,WMSBreadcrumb]
})

export class EditLocationTypeComponent {

    private id: any;
    private flagEditLocation = false;
    private headerGet: any;
    private headerPost: any;
    private urlAPILocation: any;
    private urlAPILocationType: any;
    private messages;
    private messagesForm = {};
    private dataLocaType: any = {};
    private EMLocationTypeCode = null;
    private EMLocationTypeName = null;
    private flagSubmit = true;
    private dataResetForm = {};
    private listNull = {};
    public showLoadingOverlay = false;
    public action = 'edit';

    constructor (
        private _http: Http,
        private _router:Router,
        private _user: UserService,
        private _boxPopupService: BoxPopupService,
        private _Func: Functions,
        private _API_Config: API_Config,
        params: RouteParams) {

        this.id = params.get('id');

        if(this.id == '') {

            this._router.parent.navigateByUrl('/deny');
        }
        // Check permission for user using this page
        // this.ck_permisson();
        this.checkPermission();

        this.headerGet = this._Func.AuthHeader();
        this.headerPost = this._Func.AuthHeaderPost();
        this.urlAPILocation = this._API_Config.API_Location;
        this.urlAPILocationType = this._API_Config.API_Location_type;
        this.getLocaTypeDetail();

    }


    private checkLength($event, length) {

        if ($event.target.value.length >= length  && $event.keyCode != 8) {

            $event.preventDefault();
        }
    }

    private checkPermission() {
        this.showLoadingOverlay = true;
        this._user.GetPermissionUser().subscribe(
            data => {
                if((this.action == 'view' && !this._user.RequestPermission(data,'viewLocationType')) || (this.action == 'edit' && !this._user.RequestPermission(data,'editLocationType')) || (this.action == 'new' && !this._user.RequestPermission(data,'createLocationType'))) {
                    this._router.parent.navigateByUrl('/deny');
                }
                this.showLoadingOverlay = false;
            },
            err => {
                this.showMessage('danger', this._Func.parseErrorMessageFromServer(err));
                this.showLoadingOverlay = false;
            }
        );
    }

    // Update data
    private update() {

        var tar_get = this;
        jQuery('form').find('input, textarea, select').each(function(i, field) {

            tar_get.filterForm2(field);

        });

        if (Object.keys(this.listNull).length < 1) {

            return false;
        }
        var submit = true;
        jQuery.each( this.listNull, function( key, value ) {

            if (value) {

                submit = false;
                return false;
            }
        });

        if (!submit) {

            return false;
        }

        var params = jQuery("form").serialize();

        this._http.put(this.urlAPILocationType + "/" +  this.id, params , {headers: this.headerPost})
            .map(res => res.json())
            .subscribe(
                data => {
                    this.messages = this._Func.Messages('success', this._Func.msg('VR109'));
                },
                err => {
                    this.showMessage('danger', this._Func.parseErrorMessageFromServer(err));
                },
                () => {

                    let parent = this;
                    setTimeout(function(){

                        parent._router.parent.navigateByUrl('/master-data/location-type');
                    }, 1100);
                }
            );
    }

    private getLocaTypeDetail () {

        this._http.get (this.urlAPILocationType + "/" + this.id, {headers: this.headerGet})
            .map(res => res.json())
            .subscribe(
                data => {

                    this.dataLocaType = data.data;
                    this.dataResetForm = this.dataLocaType;

                },
                error => {

                    this.parseError(error);
                },
                () => {}
            );
    }

    private reset() {

        jQuery("textarea[name=loc_type_desc]").val("");
        this.dataLocaType = {};
        this.getLocaTypeDetail();
    }

    private cancel() {
        let n = this;
        /*case 1: modify text*/
        // let warningPopup = new WarningBoxPopup();
        // warningPopup.text = "Are you sure to cancel";
        // this._boxPopupService.showWarningPopup(warningPopup)

        /*case 2: default*/
        this._boxPopupService.showWarningPopup()
            .then(function (dm) {
                if(dm)
                    n._router.navigateByUrl('/master-data/location-type');

            });
    }

    // Filter form only show choose one
    private filterForm2 ($target) {

        var arr_object = [
            {'input': 'loc_type_code', 'typeValidate': 'required'},
            {'input': 'loc_type_code', 'typeValidate': 'invalidLength2Or3'},
            {'input': 'loc_type_name', 'typeValidate': 'required'},
            {'input': 'loc_type_name', 'typeValidate': 'invalidNameStr'}
        ];

        try {

            for (var i = 0; i < arr_object.length; i++) {

                if (arr_object[i].input == $target.name) {

                    switch (arr_object[i].typeValidate) {

                        case 'required_float':

                            if ($target.value.trim() == "" && arr_object[i].typeValidate == 'required_float') {

                                this.listNull[$target.name + "_" + arr_object[i].typeValidate] = true;

                            } else if (!this.validateFloat($target.value) && arr_object[i].typeValidate == 'required_float') {

                                this.listNull[$target.name + "_" + arr_object[i].typeValidate] = true;

                            } else {

                                this.listNull[$target.name + "_" + arr_object[i].typeValidate] = false;
                            }
                            break;

                        case 'required':

                            if ($target.value.trim() == "" && arr_object[i].typeValidate == 'required') {

                                this.listNull[$target.name + "_" + arr_object[i].typeValidate] = true;

                            } else {

                                this.listNull[$target.name + "_" + arr_object[i].typeValidate] = false;
                            }
                            break;
                        case 'invalidLength2Or3':
                            if ($target.value.trim() && arr_object[i].typeValidate == 'invalidLength2Or3') {
                                this.listNull[$target.name + "_" + arr_object[i].typeValidate] = !/^[a-zA-Z0-9]{2,3}$/i.test($target.value);
                            } else {
                                this.listNull[$target.name + "_" + arr_object[i].typeValidate] = false;
                            }
                            break;
                        case 'invalidNameStr':
                            if ($target.value.trim() && arr_object[i].typeValidate == 'invalidNameStr') {
                                this.listNull[$target.name + "_" + arr_object[i].typeValidate] = !/^[a-zA-Z0-9 _-]*$/i.test($target.value);
                            } else {
                                this.listNull[$target.name + "_" + arr_object[i].typeValidate] = false;
                            }
                            break;
                        case 'int':

                            if (!this.validateNumber($target.value) && arr_object[i].typeValidate == 'int') {

                                this.listNull[$target.name + "_" + arr_object[i].typeValidate] = true;
                            } else {

                                this.listNull[$target.name + "_" + arr_object[i].typeValidate] = false;
                            }
                            break;
                        case 'float':

                            if (!this.validateFloat($target.value) && arr_object[i].typeValidate == 'float') {

                                this.listNull[$target.name + "_" + arr_object[i].typeValidate] = true;
                            } else {

                                this.listNull[$target.name + "_" + arr_object[i].typeValidate] = false;
                            }
                            break;

                        default:
                            break;

                    }

                }

            }

            return false;

        } catch (err) {

            return false;
        }

    }

    // Validate float
    private validateFloat(float ) {

        var re = /^([0-9,.]*)$/;
        return re.test(float);

    }

    // Validate number
    private validateNumber(number) {

        var re = /^([0-9]*)$/;
        return re.test(number);
    }

    // Validate number
    private validateSpace(str) {

        var re = /\ /;
        return re.test(str);
    }

    private parseError (err) {

        if (err.status == 400) {

            this.messages = this._Func.Messages('danger', err.json().error.message);
        }
        else
            this.messages = this._Func.Messages('danger', this._Func.msg('VR100'));


    }

    private showMessage(msgStatus, msg) {
        // if(this.timeoutHideMessage) clearTimeout(this.timeoutHideMessage);
        this.messages = {
            'status': msgStatus,
            'txt': msg
        }
        // this.timeoutHideMessage = setTimeout(() => {
        //     this.messages = null;
        // }, 4000);
        jQuery(window).scrollTop(0);
    }
}
