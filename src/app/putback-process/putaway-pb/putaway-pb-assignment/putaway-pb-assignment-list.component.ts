import {Component} from "@angular/core";
import {CruPaPbListComponent} from "../cru-pa-pb-list.component";

@Component ({
    selector: 'putaway-pb-assignment-list',
    directives: [CruPaPbListComponent],
    templateUrl: 'putaway-pb-assignment-list.component.html',
})

export class PutAwayPbAssignmentListComponent{
    constructor(){

    }
}