import {Component,Input, Injectable} from '@angular/core';
import { Router,RouteData } from '@angular/router-deprecated';
import { FORM_DIRECTIVES,ControlGroup,FormBuilder, Control, Validators } from '@angular/common';
import {API_Config, UserService, Functions, FormBuilderFunctions} from '../../../common/core/load';
import { Http } from '@angular/http';
import {
  WMSBreadcrumb, AdvanceTable, ReportFileExporter, WMSDateTimePickerDirective,
  WMSMultiSelectComponent, WMSPagination, PopupAssignPicker, PopupAssignDriver
} from '../../../common/directives/directives';
import {WMSMessages} from "../../../common/directives/messages/messages";
import {OrderListService} from '../order-list/order-list-service';
import { OrderBy } from "../../../common/pipes/order-by.pipe";
import {WarningBoxPopup} from "../../../common/popup/warning-box-popup";
import {BoxPopupService} from "../../../common/popup/box-popup.service";
import { ValidationService } from '../../../common/core/validator';
import { DataShareService } from '../../../common/services/data-share.service';
import { error } from 'util';
declare var jQuery: any;
declare var saveAs: any;
declare var swal: any;
import { JwtHelper } from 'angular2-jwt/angular2-jwt';

@Component ({
  selector: 'order-list',
  providers: [OrderListService, FormBuilder, BoxPopupService,ValidationService],
  directives: [
    FORM_DIRECTIVES, WMSBreadcrumb,WMSMessages, AdvanceTable, ReportFileExporter, WMSDateTimePickerDirective,
    WMSMultiSelectComponent, WMSPagination, PopupAssignPicker, PopupAssignDriver
  ],
  pipes: [OrderBy],
  templateUrl: 'order-list.component.html',
})

@Injectable()
export class OrderListComponent {
  /** list permission need to check **/
  private hasViewPermission;
  private hasScheduleToShipDatePermission;

  private hasCreatePermission;

  private hasAtLeastOneActionPermission;

  private hasEditPermission;
  private hasAssignCSRPermission;
  private hasAllocatePermission;
  private hasCreateWavePickPermission;
  private hasPrintWavePickPermission;
  private hasWorkOrderPermission;
  private hasUpdateOrderPackingPermission;
  private hasAssignPalletPermission;
  private showPrintLPN;
  private hasCreateBOLPermission;
  private hasShippingPermission;
  private hasCommercialInvoicePermission = false;
  private hasCancelOrderPermission;
  private hasImportOnlineOrderPermission;
  private hasCompletePickingOrderPermission;
  private hasCompleteXDockOrderPermission;
  private hasShippedDate = false;
  hasCSRComment = false;
  hasWHComment = false;
  /* end list permission */

  private messages;
  private showLoadingOverlay = false;
  private local_cus_id = null;

  private tableID = 'order-list';
  headerURL = this._API.API_User_Metas + '/ord';
  headerDef = [{id: 'ver_table', value: 11},
                    {id: 'ck', name: '', width: 25},
                    {id: 'odr_sts', name: 'Status', width: 70},
                    {id: 'odr_num', name: 'Order Number', width: 130},
                    {id: 'cus_code', name: 'Customer', width: 100, 'unsortable': true},
                    {id: 'odr_type_name', name: 'Order Type', width: 45, 'unsortable': true},
                    {id: 'back_odr', name: 'Back Order', width: 45},
                    //  {id: 'bol', name: 'BOL', width: 45},
                    {id: 'cus_odr_num', name: 'Customer Order #', width: 60},
                    {id: 'cus_po', name: 'PO', width: 50},
                    {id: 'ship_to_name', name: 'Shipping To', width: 130},
                    {id: 'carrier', name: 'Carrier', width: 130},
                    {id: 'ship_by_dt', name: 'Ship By Date', width: 110},
                    {id: 'cancel_by_dt', name: 'Cancel By Date', width: 110},
                    {id: 'shipped_dt', name: 'Shipped Date', width: 110},
                    {id: 'schd_ship_dt', name: 'Scheduled To Ship Date', width: 110},
                    {id: 'csr_name', name: 'CSR', width: 130, 'unsortable': true},
                    {id: 'user', name: 'User', width: 130, 'unsortable': true},
                    {id: 'driver_name', name: 'Driver', width: 130, 'unsortable': true},
                    {id: 'created_at', name: 'Created Date', width: 90},
                    {id: 'rush_odr', name: 'Priority', width: 90}];

  private pinCols = 5;
  private rowData: any[] = [];
  private getSelectedRow;
  private objSort;
  private searchParams;
  private refreshRowData = false;
  private delayTime: number = 3000;

  private Pagination;
  private totalCount = 0;
  private tmpPageCount;
  private pageCount=0;
  private ItemOnPage=0;
  private currentPage = 1;
  private perPage = 20;
  private numLinks = 3;

  private orderTypes = [];
  private orderStatus = [];
  private dataListCustomer = [];
  private assignUser;
  private listSelectedOrder = [];
  private listSelectForPopup = [];
  private showPopupAssignCSR = false;
  private showPopupAssignDriver = false;

  private action;
  private defaultSearch = '';
  private isOnlineList = false;
  private pageTitle = 'Order List';

  onlineImportForm: ControlGroup;
  private showError = false;
  private filesToUpload = [];
  private cancelList = [];

  printWapLPNForm: ControlGroup;
  private showErrorWapLPN = false;
  private printWapOdrNum;
  private printWapOdrId;

  updateShippedDateForm: ControlGroup;
  private showErrorShippedDate = false;

  updateScheduleShipDateForm: ControlGroup;
  updateRoutedDateForm: ControlGroup;
  private showErrorScheduleShipDate = false;
  private showErrorRoutedDate = false;

  multiCommentForm: ControlGroup;
  private showErrorMultiComment = false;
  

  mergeOrderFrom: ControlGroup;
  private showErrorMergeOrder = false;
  orderNumberList = [];
  customerList = [];
  POList = [];
  listSelectedItem = [];
  showForm = false;
  statusAllowMerge = ['NW','AL','PK','PD','PN','PA','PTG','PTD'];

  private listErrMsg = {
    // for edit
    'ED1': 'BackOrder can not be edited!',
    'ED2': 'Online Order can not be edited!',

    // for assign CSR
    'CSR1': 'Canceled Order can not be assigned CSR!',
    'CSR2': 'Shipped Order can not be assigned CSR!',
    'CSR3': 'Please select and assign CSR to orders that belong to one customer only!',
    // for assign pallet
    'AP1': 'Please choose at least one Order to assign pallet!',
    'AP2': 'Assign pallet can be created for Packed or Partial Packed Orders only!',

    // for print LPN
    'PR1': 'Please choose at least one Staging or Partial Staging Order to print LPN',
    'PR2': 'Only Staging or Partial Staging Orders can print LPN!',

    // for print WAP LPN
    'PRW1': 'Please choose only one Order to print WAP LPNs',
    'PRW2': 'Cancelled Orders can not be printed WAP LPNs!',

    // for print Wave Pick
    'PWP1': 'Please choose one Order to print Wave Pick!',
    'PWP2': 'Please choose only one Order to print Wave Pick!',
    'PWP3': 'Wave Pick can be printed for Picking/Picked or Partial Picking/Picked Orders only!',

    // for create work order
    'WO1': 'Please choose at least one Order to create Work Order!',
    'WO2': 'Please choose only one Order to create Work Order!',
    'WO3': 'Work Order can\'t be created for Order that has status of New, Canceled, Hold, Shipped or Partial Shipped!',
    'WO4': 'Work Order {{work-order-number}} is created for the selected Order already!',

    // for create BOL
    'BOL1': 'Please choose at least one Order to create Bill of Ladings!',
    'BOL2': 'Please choose only one Order to create Bill of Ladings!',
    'BOL3': 'Only Staging or Partial Staging or Ready To Ship or Scheduled To Ship Order can have Bill of Ladings!',
    'BOL4': 'Bill of Ladings has been created for this Order already!',

    // for cancel order
    'CC1': 'Please choose at least one Order to cancel!',
    'CC2': 'On Hold, Canceled or Shipped Orders are not processed!',
    'CC3': 'Please choose only one Order to cancel!',
    'CC4': 'Canceled or Shipped Orders are not processed!',

    // for shipping
    'SP1': 'Please choose at least one Order to ship!',
    'SP2': 'Only Staging or Partial Staging Orders can be shipped!',
    'SP3': 'BOL has not been finalized. Order cannot be shipped.',
    'SP4': 'Please choose only one Order to ship!',
    'SP5': 'Only Scheduled to Ship Order can be Shipped!',

    // for update shipped date
    'USD1': 'Update Shipped Date Successfully!',

    // for update schedule to ship date
    'USC1': 'Update Schedule To Ship Date Successfully!',
    'URD1': 'Update Routed Date Successfully!',
    // for merge orders
    
    'MODR1': 'Please select and merge orders that belong to one Customer only!',
    'MODR2': 'Please select and merge orders that belong to one Status only!',
    'MODR3': 'Please select and merge orders that belong to one PO only!',
    'MODR4': 'Please select and merge orders that belong to one Customer Order Number only!',
    'MODR5': 'Please choose one of the following status: "New, Allocated, Picking, Picked, Packing, Packed, Palletizing, Palletized"!',
    // 'MODR6': 'Can not merge orders with order type is Back Order.',
  };

  // Export data to csv file.
  private dataExport:any[];
  private headerExport = ['Status', 'Order Number', 'Customer', 'Order Type', 'Back Order', 'Shipping To', 'Ship By Date', 'Cancel By Date', 'Shipped Date', 'CSR', 'User', 'Created Date'];
  private fileName = 'Orders.csv';
  private exportAPIUrl: string = '';
  private queryParams;
  private userId: Number;
  public refreshMultiSelect = false;

  public priority = [{key:'No Priority', value: '0'},
                    {key:'Priority', value: 'priority'}]
  public isMerged = false;

  public URLAssign = this._API.API_ORDERS_CORE+'/assign-csr';
  public URLget = this._API.API_Authen + '/csr-warehouse/';
  constructor(
  	private _http: Http,
    private _func: Functions,
    private _API: API_Config,
    private _orderListService: OrderListService,
    private _Valid: ValidationService,
    private fb: FormBuilder,
    private _user: UserService,
    private _boxPopupService: BoxPopupService,
    private _routeData: RouteData,
    private _router:Router,
    private dataShareService: DataShareService,
    private fbdFunc: FormBuilderFunctions,
    private jwtHelper: JwtHelper
  ){
    const token = localStorage.getItem('jwt');
    const userInfo = this.jwtHelper.decodeToken(token);
    this.userId = userInfo ? userInfo.jti : 0;

    this.action = _routeData.get('page');
    this._getQueryParams();
    switch (this.action) {
      case 'onlineOrder':
        this.defaultSearch = 'type=online-order'; // Show all online orders
        break;
      case 'assignCSR':
        this.defaultSearch = 'type=csr'; // Show new status
        break;
      case "allocate":
        this.defaultSearch = 'type=allocate'; // Show new and allocated/parital status
        break;
      case 'wavepick':
        this.defaultSearch = 'type=wave-pick'; // Show picking/partial, picked/partial, allocated/parital  status
        break;
      case 'packing':
        this.addFieldForPacking();
        this.defaultSearch = 'type=packing'; // Show picked/partial, packing/partial, packed/partial status
        break;
      case 'pallet-assignment':
        this.defaultSearch = 'type=pallet-assignment'; // Show packed/partial status
        break;
      case 'shipping':
        this.defaultSearch = 'type=shipping'; // Show staging/partial, shipped status
        break;
      case 'cancel-order':
        this.defaultSearch = 'type=cancel-order'; // Show canceled status
        break;
      default:
        // order list
        this.addFieldForOrder();
        this.defaultSearch = 'type=order'; //Show all orders that are not online orders
        break;
    }
    
    if(this.action == 'onlineOrder') {
      this.isOnlineList = true;
      this.pageTitle = 'Online Order List';
      this.buildForm();
    }

    if(this.action == 'shipping') {
      this.headerURL = this._API.API_User_Metas + '/osh';
      this.headerDef = [{id: 'ver_table', value: 10},
                        {id: 'ck', name: '', width: 25},
                        {id: 'odr_sts', name: 'Status', width: 70},
                        {id: 'odr_num', name: 'Order Number', width: 130},
                        {id: 'cus_code', name: 'Customer', width: 130, 'unsortable': true},
                        {id: 'odr_type_name', name: 'Order Type', width: 45, 'unsortable': true},
                        {id: 'back_odr', name: 'Back Order', width: 45},
                        // {id: 'bol', name: 'BOL', width: 80, 'unsortable': true},
                        {id: 'ship_to_name', name: 'Shipping To', width: 130},
                        {id: 'ship_by_dt', name: 'Ship By Date', width: 110},
                        {id: 'cancel_by_dt', name: 'Cancel Date', width: 90},
                        {id: 'shipped_dt', name: 'Shipped Date', width: 110},
                        {id: 'csr_name', name: 'CSR', width: 130, 'unsortable': true},
                        {id: 'user', name: 'User', width: 130, 'unsortable': true},
                        {id: 'created_at', name: 'Created Date', width: 90}];
    }
    this.buildFormPrintWapLPN();
    this.buildFormUpdateShippedDate();
    this.buildFormUpdateScheduleShipDate();
    this.buildFormUpdateRoutedDate();
    this.buildFormMultipleComment();
    this.buildFormMergeOrder();
    this.checkPermission();
    this.getCustomersByUser();
    this.getOrderTypeList();
  }

  ngOnDestroy() {
    this._resetQueryParams();
  }

  private _resetQueryParams() {
      this.queryParams = null;
      this.dataShareService.data = '';
  }

  _getQueryParams() {
      // get params from Dashboard
      const queryString = this.dataShareService.data;

      this.queryParams = this._func.parseQueryString(queryString);
      if(queryString.length) {
          if(this.searchParams) {
              this.searchParams += "&" + queryString;
          } else {
              this.searchParams = "&" + queryString;
          }
      }
  }

  ngOnInit() {
    var _this = this;
    setTimeout(() => {
      jQuery('#shipped_date').on('change.update', function() {
        (<Control>_this.updateShippedDateForm.controls['shipped_date']).updateValue(jQuery(this).val());
      });
    }, 400);
  }

  private showMessage(msgStatus, msg) {
      this.messages = {
          'status': msgStatus,
          'txt': msg
      }
      jQuery(window).scrollTop(0);
  }

  private checkPermission() {
      this.showLoadingOverlay = true;
      this._user.GetPermissionUser().subscribe(
          data => {
            switch (this.action) {
              case "allocate":
                this.hasAllocatePermission = this._user.RequestPermission(data,'allocateOrder');
                this.hasCancelOrderPermission = this._user.RequestPermission(data,'cancelOrder');
                break;
              case 'assignCSR':
                this.hasAssignCSRPermission = this._user.RequestPermission(data,'assignCSR');
                this.hasCancelOrderPermission = this._user.RequestPermission(data,'cancelOrder');
                break;
              case 'wavepick':
                this.hasCreateWavePickPermission = this._user.RequestPermission(data,'createWavePick');
                this.hasPrintWavePickPermission = this._user.RequestPermission(data,'printWavePick');
                this.hasCancelOrderPermission = this._user.RequestPermission(data,'cancelOrder');
                break;
              case 'packing':
                this.hasUpdateOrderPackingPermission = this._user.RequestPermission(data,'orderPacking');
                this.hasCancelOrderPermission = this._user.RequestPermission(data,'cancelOrder');
                break;
              case 'pallet-assignment':
                this.hasAssignPalletPermission = this._user.RequestPermission(data,'palletAssignment');
                this.hasCancelOrderPermission = this._user.RequestPermission(data,'cancelOrder');
                break;
              case 'shipping':
                this.hasShippingPermission = this._user.RequestPermission(data,'orderShipping');
                var hasAssignPalletPermission = this._user.RequestPermission(data,'palletAssignment');
                if(hasAssignPalletPermission) {
                  this.showPrintLPN = true;
                }
                this.hasCreateBOLPermission = this._user.RequestPermission(data,'createBOL');
                this.hasCancelOrderPermission = this._user.RequestPermission(data,'cancelOrder');
                this.hasShippedDate = this._user.RequestPermission(data,'editOrderShipDate');
                break;
              case 'onlineOrder':
                this.hasImportOnlineOrderPermission = this._user.RequestPermission(data,'importOnlineOrder');
                this.hasCreatePermission = this._user.RequestPermission(data,'createOrder');
                this.hasEditPermission = this._user.RequestPermission(data,'editOrder');
                this.hasAssignCSRPermission = this._user.RequestPermission(data,'assignCSR');
                this.hasAllocatePermission = this._user.RequestPermission(data,'allocateOrder');
                this.hasCreateWavePickPermission = this._user.RequestPermission(data,'createWavePick');
                this.hasWorkOrderPermission = this._user.RequestPermission(data,'viewWorkOrder');
                this.hasUpdateOrderPackingPermission = this._user.RequestPermission(data,'orderPacking');
                this.hasAssignPalletPermission = this._user.RequestPermission(data,'palletAssignment');
                if(this.hasAssignPalletPermission) {
                  this.showPrintLPN = true;
                }
                this.hasShippingPermission = this._user.RequestPermission(data,'orderShipping');
                this.hasCancelOrderPermission = this._user.RequestPermission(data,'cancelOrder');
                break;
              default:
                this.hasCreatePermission = this._user.RequestPermission(data,'createOrder');
                
                this.hasAssignCSRPermission = this._user.RequestPermission(data,'assignCSR');
                this.hasAllocatePermission = this._user.RequestPermission(data,'allocateOrder');
                this.hasCreateWavePickPermission = this._user.RequestPermission(data,'createWavePick');
                this.hasWorkOrderPermission = this._user.RequestPermission(data,'viewWorkOrder');
                this.hasUpdateOrderPackingPermission = this._user.RequestPermission(data,'orderPacking');
                this.hasAssignPalletPermission = this._user.RequestPermission(data,'palletAssignment');
                if(this.hasAssignPalletPermission) {
                  this.showPrintLPN = true;
                }
                this.hasShippingPermission = this._user.RequestPermission(data,'orderShipping');
                this.hasCancelOrderPermission = this._user.RequestPermission(data,'cancelOrder');
                this.hasShippedDate = this._user.RequestPermission(data,'editOrderShipDate');
                this.hasCommercialInvoicePermission = this._user.RequestPermission(data, 'viewInvoice');
                break;
            }

            this.hasCSRComment = this._user.RequestPermission(data, 'csrComment');
            this.hasWHComment = this._user.RequestPermission(data, 'whseComment');
            this.hasEditPermission = this.action === 'cancel-order' ? false : this._user.RequestPermission(data,'editOrder');
            this.hasViewPermission = this._user.RequestPermission(data,'viewOrder');
            this.hasScheduleToShipDatePermission = this._user.RequestPermission(data,'updateScheduleToShipDate');
            this.hasCompletePickingOrderPermission = this._user.RequestPermission(data, 'completePickingOrder');
            this.hasCompleteXDockOrderPermission = this._user.RequestPermission(data, 'completeXDock');

            if(this.hasEditPermission || this.hasAssignCSRPermission || this.hasAllocatePermission || this.hasCreateWavePickPermission || this.hasWorkOrderPermission || this.hasUpdateOrderPackingPermission || this.hasAssignPalletPermission || this.hasCreateBOLPermission || this.hasShippingPermission || this.hasCancelOrderPermission) {
              this.hasAtLeastOneActionPermission = true;
            }

            if (this.action != 'shipping') {
              if (!this.hasViewPermission) {
                this._router.parent.navigateByUrl('/deny');
              } else {
                this.getOrderStatusList();
              }
            } else {// Access deny error displays when user go to BOL page
              if(this._user.RequestPermission(data, 'viewBOL') || this._user.RequestPermission(data, 'orderShipping')){
                this.getOrderStatusList();
              }else{
                this._router.parent.navigateByUrl('/deny');
              }
            }//end

          },
          err => {
              // console.log('check permission err', err);
              this.showMessage('danger', this._func.parseErrorMessageFromServer(err));
              this.showLoadingOverlay = false;
          }
      );
  }

  private getCustomersByUser () {
    var params ="?limit=10000";
    this._orderListService.getListCustomersByUser(params)
      .subscribe(
        data => {
          this.dataListCustomer = data.data;
        },
        err => {},
        () => {}
      );
  }

  private getOrderStatusList () {
    this._orderListService.getOrderStatusList(this.defaultSearch)
      .subscribe(
        data => {
          this.orderStatus = data.data;
          let preSelectStatus = false;
          this.orderStatus.forEach(status => {
            if (this.queryParams['inprocess_status'] && this.queryParams['inprocess_status'] === status.value.toLowerCase()){
              status.checked = true;
              if (!preSelectStatus)
                preSelectStatus = true;
            }
            if (status.checked) {
              if (!preSelectStatus)
                preSelectStatus = true;
            }
          });
          
          if (preSelectStatus) {
            setTimeout(() => {
                this.search();
            }, 500);
          } else {
            this.getOrderList(1);
          }
        }
      );
  }

  private getOrderTypeList () {
    this._orderListService.getOrderTypeList()
      .subscribe(
        data => {
          this.orderTypes = data.data;
        }
      );
  }

  private getOrderList(page = null){
    this.showLoadingOverlay = true;
    this.local_cus_id = localStorage.getItem('local_cus_id');
    if(!page) page = 1;
    let params="?page="+page+"&limit="+this.perPage;
    if(this.objSort && this.objSort['sort_type'] != 'none') {
      params += '&sort['+ this.objSort['sort_field']+ ']='+ this.objSort['sort_type'];
    }
    else {
      params += '&sort[created_at]=desc&sort[odr_num]=desc'
    }

    if(this.searchParams) {
      params += this.searchParams;
    }
    if(this.defaultSearch) {
      params += this.local_cus_id ? '&' + this.defaultSearch + '&cus_id=' +this.local_cus_id : '&' + this.defaultSearch;
    }

    // update exportAPIUrl
    const exportPath = this.action === 'shipping' ? '/order-shippings' : '/orders';
    this.exportAPIUrl = this._API.API_ORDERS_CORE + exportPath + params + '&export=1';

    if(this.action == 'shipping') {
      this._orderListService.getOrderShippingList(params)
        .subscribe(
          data => {
            this.createRowData(data);
            this.showLoadingOverlay = false;
          },
          err => {
            this.showMessage('danger', this._func.parseErrorMessageFromServer(err));
            this.showLoadingOverlay = false;
          }
        );
    }
    else {
      this._orderListService.getOrderList(params)
        .subscribe(
          data => {
            this.createRowData(data);
            this.showLoadingOverlay = false;
          },
          err => {
            this.showMessage('danger', this._func.parseErrorMessageFromServer(err));
            this.showLoadingOverlay = false;
          }
        );
    }
  }

  private createRowData(data) {
      var rowData: any[] = [];
      if (typeof data.data != 'undefined') {
        this.initPagination(data);
        this.setDataExport(data.data);

        data = this._func.formatData(data.data);
        for (var i = 0; i < data.length; i++) {
          let itemData = {
            odr_sts: data[i].odr_sts_name,
            odr_num: '<a href="#/outbound/order/' + data[i].odr_id + '">'+ data[i].odr_num +'</a>',
            cus_code: data[i].cus_code,
            odr_type_name: data[i].odr_type_name,
            back_odr: data[i].back_odr == 1 ? 'Yes' : 'No',
            ship_to_name: data[i].ship_to_name,
            ship_by_dt: data[i].ship_by_dt,
            cancel_by_dt: data[i].cancel_by_dt,
            shipped_dt:data[i].shipped_dt,
            csr_name: data[i].csr_name,
            user: data[i].created_by,
            created_at: data[i].created_at,
            odr_id: data[i].odr_id,
            cus_id: data[i].cus_id,
            odr_sts_code: data[i].odr_sts,
            odr_type: data[i].odr_type,
            origin_odr_type: data[i].origin_odr_type,
            work_order: data[i].work_order,
            work_order_number: data[i].wo_hdr_num,
            ord_number: data[i].odr_num,
            bol_num: data[i].bol_num,
            wv_num: data[i].wv_num,
            schd_ship_dt: data[i].schd_ship_dt,
            csr_id: data[i].csr_id,
            routed_dt: data[i].routed_dt,
            cus_odr_num: data[i].cus_odr_num,
            cus_po: data[i].cus_po,
            carrier: data[i].carrier,
            rush_odr: data[i].rush_odr,
            alloc_qty: data[i].alloc_qty,
            picked_qty: data[i].picked_qty,
            alloc_ctns: data[i].alloc_ctns,
            picked_ctns: data[i].picked_ctns,
            requested_qty: data[i].requested_qty,
            requested_ctns: data[i].requested_ctns,
            driver_name: data[i].driver_name,
            ship_id: data[i].ship_id,
            odr_number: data[i].odr_num
          };
          if(this.action == 'shipping' || this.action == 'order') {
            itemData['bol'] = data[i].bol;
          }
          if(this.action == 'cancel-order') {
            itemData['odr_num'] = '<a href="#/outbound/cancel-order/' + data[i].odr_id + '">'+ data[i].odr_num +'</a>';
          }
          rowData.push(itemData);
        }
      }
      else {
        this.rowData = [];
        this.Pagination = null;
      }

      this.rowData = rowData;
  }

  private initPagination(data){
    let meta = data['meta'];
    if(meta['pagination']) {
      this.Pagination=meta['pagination'];
    }
    else {
      let perPage = meta['perPage'],
          curPage = meta['currentPage'],
          total = meta['totalCount'];
      let count = perPage * curPage > total ? perPage * curPage - total : perPage;
      this.Pagination = {
        "total": total,
        "count": count, // TODO
        "per_page": perPage,
        "current_page": curPage,
        "total_pages": meta['pageCount'],
        "links": {
          "next": data['link']['next']
        }
      }
    }
    this.Pagination['numLinks']=3;
    this.Pagination['tmpPageCount']=this._func.Pagination(this.Pagination);
  }

  private filterList(pageNumber) {
      this.getOrderList(pageNumber);
  }

  private getPage(pageNumber) {
      let arr=new Array(pageNumber);
      return arr;
  }

  private onPageSizeChanged($event) {
      this.perPage = $event.target.value;
      if((this.Pagination['current_page'] - 1) * this.perPage >= this.Pagination['total']) {
          this.currentPage = 1;
      }
      else {
          this.currentPage = this.Pagination['current_page'];
      }
      this.getOrderList(this.currentPage);
  }

  private search() {
    this.searchParams = '';
    this._getQueryParams();
    this._prepareSearchForm();
    this.getOrderList(1);
  }

  private _prepareSearchForm() {
    let params_arr = jQuery("#form-filter").serializeArray() ;
    for (let i in params_arr) {
        if (params_arr[i].value == "") continue;
        this.searchParams +="&" +  encodeURIComponent(params_arr[i].name.trim()) + "=" + encodeURIComponent(params_arr[i].value.trim());
        localStorage.setItem('local_cus_id', params_arr[1].value);
    }
  }

  private doSort(objSort) {
    this.objSort = objSort;
    this.getOrderList(this.Pagination.current_page);
  }
  
  private reset() {
    localStorage.removeItem('local_cus_id');
    this._resetQueryParams();
    jQuery("#form-filter input[type=text], #form-filter select").each(function( index ) {
        jQuery(this).val("");
    });
    this.refreshMultiSelect = true;
    this.searchParams = '';
    this.messages = null;
    this.getOrderList(1);
  }

  private edit() {
    this.getSelectedRow = 'edit';
  }

  private comment() {
    this.getSelectedRow = 'comment';
  }

  private getListCSR() {
    this.getSelectedRow = 'assignCSR';
  }

  private modalListDriver() {
    this.getSelectedRow = 'assignDriver';
  }

  private allocate() {
    this.getSelectedRow = 'allocate';
  }

  private createWavePick() {
    this.getSelectedRow = 'create-wave-pick';
  }

  private printWavePick() {
    this.getSelectedRow = 'print-wave-pick';
  }

  private pack() {
    this.getSelectedRow = 'pack';
  }

  private packingSlip() {
    this.getSelectedRow = 'packing-slip';
  }

  private assignPallet() {
    this.getSelectedRow = 'assign-pallet';
  }

  private checkPrintLPN() {
    this.getSelectedRow = 'print-lpn';
  }

  private checkPrintWAPLPN() {
    this.getSelectedRow = 'print-wap-lpn';
  }

  private createBOL() {
    this.getSelectedRow = 'create-bol';
  }

  private createWorkOrder() {
    this.getSelectedRow = 'create-work-order';
  }

  private confirmCancelOrder() {
    this.getSelectedRow = 'cancel-order';
  }

  private cancelOrderbySKU() {
    this.getSelectedRow = 'cancel-order-by-sku';
  }

  private shipping() {
    this.getSelectedRow = 'shipping';
  }

  private shippedDate() {
    this.getSelectedRow = 'shippedDate';
  }

  private completePiking() {
    this.getSelectedRow = 'completePiking';
  }

  private scheduleShipDate() {
    this.getSelectedRow = 'scheduleShipDate';
  }

  private setRoutedDate() {
    this.getSelectedRow = 'routedDate';
  }

  printCommercialInvoiceMenu(){
    this.getSelectedRow = 'print-commercial-invoice';
  }

  afterGetSelectedRow($event) {
    var listSelectedItem = $event.data;
    this.messages = null;
    this.listSelectedOrder = [];
    for(let i = 0, l = listSelectedItem.length; i < l; i++) {
      this.listSelectedOrder.push(listSelectedItem[i].odr_id);
    }
    switch ($event.action) {
      case 'edit':
        if (listSelectedItem.length > 1) {
          this.showMessage('danger', this._func.msg('CF004'));
        }
        else {
          if (listSelectedItem.length < 1) {
            this.showMessage('danger', this._func.msg('CF005'));
          }
          else {

            switch (listSelectedItem[0].odr_type) {
              case 'EC':
                this.showMessage('danger', this.listErrMsg.ED2);
                break;

              default:
                this._router.parent.navigateByUrl('/outbound/order/' + listSelectedItem[0].odr_id + "/edit");
                break;
            }
          }
        }
        break;
      case 'comment':
        if (listSelectedItem.length > 1) {
          // this.showMessage('danger', this._func.msg('CF004'));
          this.resetFormMultipleComment(listSelectedItem);
          jQuery('#multiple-comment').modal('show');
        }
        else {
          if (listSelectedItem.length < 1) {
            this.showMessage('danger', this._func.msg('CF005'));
          }
          else {

            switch (listSelectedItem[0].odr_type) {
              case 'EC':
                this.showMessage('danger', this.listErrMsg.ED2);
                break;

              default:
                this._router.parent.navigateByUrl('/outbound/order/' + listSelectedItem[0].odr_id + '/comment');
                break;
            }
          }
        }
        break;
      case 'assignCSR':
        this.assignUser = null;
        this.listSelectForPopup = [];
        this.showPopupAssignCSR = false;
        if (listSelectedItem.length < 1) {
          this.showMessage('danger', this._func.msg('VR121'));
        }
        else {
          var listCustomer = [];
          let hasCanceledOrder = false;
          let hasShipped = false;
          for(let i = 0; i < listSelectedItem.length; i++) {
            if(listSelectedItem[i].odr_sts_code == 'CC') {
              hasCanceledOrder = true;
            }
            if(listSelectedItem[i].odr_sts_code == 'SH') {
              hasShipped = true;
            }
          }
          if(hasCanceledOrder) {
            this.showMessage('danger', this.listErrMsg.CSR1);
          }else if(hasShipped){
            this.showMessage('danger', this.listErrMsg.CSR2);
          }else {
            for(let i = 0, l = listSelectedItem.length; i < l; i++) {
              let cusId = listSelectedItem[i].cus_id;
              if(listCustomer.indexOf(cusId) == -1) {
                listCustomer.push(cusId);
              }
            }
            if(listCustomer.length > 1) {
              this.showMessage('danger', this.listErrMsg.CSR3);
            }
            else {
              this.listSelectForPopup = this.listSelectedOrder;
              this.showPopupAssignCSR = true;
            }
          }
        }
        break;

      // case for assign driver
      case 'assignDriver':
        if (listSelectedItem.length < 1) {
          this.showMessage('danger', this._func.msg('OD001'));
        } else {
          listSelectedItem.forEach(element => {
             // status odr_sts 'SH' shipped
             if (element.odr_sts.toUpperCase() != 'SHIPPED') {
              listSelectedItem = [];
              this.showMessage('danger', this._func.msg('OD002'));
              return;
             }
          });

          this.listSelectedItem = listSelectedItem;
          this.showPopupAssignDriver = true;
        }
      break;

      case 'allocate':
        if (listSelectedItem.length > 1) {
          this.showMessage('danger', this._func.msg('VR123'));
        }
        else {
          if (listSelectedItem.length < 1) {
            this.showMessage('danger', this._func.msg('VR122'));
          }
          else {
            if(!listSelectedItem[0].csr_name) {
              let warningPopup = new WarningBoxPopup();
              warningPopup.text = this._func.msg('VR125');

              this._boxPopupService.showWarningPopup(warningPopup)
                .then((ok) => {
                  if(ok) {
                    this.getListCSR();
                  }
                })
            }
            else
              if(listSelectedItem[0].odr_sts_code != 'NW') {
                this.showMessage('danger', this._func.msg('VR124'));
              }
              else {
                this._router.parent.navigateByUrl('/outbound/allocate/' + listSelectedItem[0].odr_id);
              }
          }
        }
        break;

      case 'create-wave-pick':
        // if (listSelectedItem.length > 1) {
        //   this.showMessage('danger', this._func.msg('VR142'));
        // }
        // else {
          if (listSelectedItem.length < 1) {
            this.showMessage('danger', this._func.msg('VR127'));
          }
          else {
            var listOrderCannotCreateWavePick = [];
            // Wave pick can be created for allocated or partial allocated orders only!
            var allowStatusForCreateWavePick = ['AL', 'PAL'];
            var listOrder = [];
            for(let i = 0, l = listSelectedItem.length; i < l; i++) {
              if(allowStatusForCreateWavePick.indexOf(listSelectedItem[i].odr_sts_code) == -1) {
                listOrderCannotCreateWavePick.push(listSelectedItem[i].ord_number);
              }
              listOrder.push(listSelectedItem[i].odr_id);
            }
            if(listOrderCannotCreateWavePick.length) {
              this.showMessage('danger', this._func.msg('VR128'));
            }
            else {
              this.showLoadingOverlay = true;
              try {
                var that = this;
                var xhr = new XMLHttpRequest();
  
                xhr.open("POST", this._API.API_Wavepick  + '/create-wave-pick/', true);
                xhr.setRequestHeader("Authorization", 'Bearer ' +this._func.getToken());
                xhr.setRequestHeader("Content-Type", 'application/json');
                xhr.responseType = 'blob';
  
                xhr.onreadystatechange = function () {
                  if(xhr.readyState == 2) {
                    if(xhr.status == 200) {
                        xhr.responseType = "blob";
                    } else {
                        xhr.responseType = "text";
                    }
                  }
  
                  if(xhr.readyState === 4) {
                    that.showLoadingOverlay = false;
                    if(xhr.status === 200) {
                      //that.updateOrdersStatusToPicking();
                      that.getOrderList(1);
                      var today = new Date();
                      var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
                        "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
                      ];
                      var day = today.getDate();
                      var month = monthNames[today.getMonth()];
                      var year = today.getFullYear().toString().substr(2,2);
                      var fileName = 'Wave Pick _ '+ month+ day + year;
                      var blob = new Blob([this.response], {type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'});
                      // saveAs.saveAs(blob, fileName + '.pdf');
                      that.showMessage('success', that._func.msg('WP003'));
                    }
                    else {
                      // console.log('create wave pick err', xhr);
                      var errMsg = '';
                      if(xhr.response) {
                        try {
                          var res = JSON.parse(xhr.response);
                          errMsg = res.errors && res.errors.message ? res.errors.message : xhr.statusText;
                        }
                        catch(err){errMsg = xhr.statusText}
                      }
                      else {
                        errMsg = xhr.statusText;
                      }
                      if(!errMsg) {
                        errMsg = that._func.msg('VR100');
                      }
                      that.showMessage('danger', errMsg);
                    }
                  }
                };
  
                xhr.send(JSON.stringify({'odr_ids': listOrder}));
              } catch (err) {
                this.showMessage('danger', this._func.msg('VR100'));
                this.showLoadingOverlay = false;
              }
            }
          }
        // }
        break;

      case 'print-wave-pick':
        if (listSelectedItem.length < 1) {
          this.showMessage('danger', this.listErrMsg.PWP1);
        }
        else {
          if(listSelectedItem.length > 1) {
            this.showMessage('danger', this.listErrMsg.PWP2);
          }
          else {
            var allowStatusForPrint = ['PK', 'PPK','PD', 'PIP']; // picking/partial
            if(allowStatusForPrint.indexOf(listSelectedItem[0].odr_sts_code) == -1) {
              this.showMessage('danger', this.listErrMsg.PWP3);
            }
            else {
              this.printWavepickTicket(listSelectedItem[0].odr_id, listSelectedItem[0].wv_num);
            }
          }
        }
        break;


        case 'revert-order' :

            if (listSelectedItem.length > 1) {
                this.showMessage('danger', this._func.msg('VRRVOD01'));
            }
            else {
                if (listSelectedItem.length < 1) {
                    this.showMessage('danger', this._func.msg('VRRVOD02'));
                }
                else {

                    if(listSelectedItem[0].odr_sts_code != 'AL' &&
                        listSelectedItem[0].odr_sts_code != 'PK') {
                        this.showMessage('danger', this._func.msg('VRRVOD03'));
                    }
                    else {
                        this._router.parent.navigateByUrl('/outbound/order-revert/' + listSelectedItem[0].odr_id);
                    }
                }
            }

            break;

        case 'pack':
            if (listSelectedItem.length > 1) {
              this.showMessage('danger', this._func.msg('VR133'));
            }
            else {
              if (listSelectedItem.length < 1) {
                this.showMessage('danger', this._func.msg('VR132'));
              }
              else {
                if(listSelectedItem[0].odr_sts_code != 'PD' && listSelectedItem[0].odr_sts_code != 'PN' && listSelectedItem[0].odr_sts_code != 'PIP' && listSelectedItem[0].odr_sts_code != 'PPA') {
                  this.showMessage('danger', this._func.msg('VR134'));
                }
                else {
                  this._router.parent.navigateByUrl('/outbound/packing/' + listSelectedItem[0].odr_id);
                }
              }
            }
            break;

      case 'packing-slip':
        if (listSelectedItem.length == 1 && listSelectedItem[0].origin_odr_type == 'EC') {
          this.printPackingSlip(listSelectedItem[0].odr_id, listSelectedItem[0].ord_number, listSelectedItem[0].cus_name);
        }
        else {
          this.showMessage('danger', this._func.msg('VR140'));
        }
        break;

      case 'assign-pallet':
        if (listSelectedItem.length < 1) {
          this.showMessage('danger', this.listErrMsg.AP1);
        }
        else {
          var listOrderCannotAssignPallet = [];
          var allowStatusForAssignPallet = ['PA', 'PAP'];
          var listOrder = [];
          for(let i = 0, l = listSelectedItem.length; i < l; i++) {
            if(allowStatusForAssignPallet.indexOf(listSelectedItem[i].odr_sts_code) == -1) {
              listOrderCannotAssignPallet.push(listSelectedItem[i].ord_number);
            }
            listOrder.push(listSelectedItem[i].odr_id);
          }
          if(listOrderCannotAssignPallet.length) {
            this.showMessage('danger', this.listErrMsg.AP2);
          }
          else {
            this._router.parent.navigateByUrl('/outbound/pallet-assignment/' + listOrder.join(','));
          }
        }
        break;

      case 'print-lpn':
        if (listSelectedItem.length < 1) {
          this.showMessage('danger', this.listErrMsg.PR1);
        }
        else {
          var listOrderCannotPrintLPN = [];
          var allowStatusForPrintLPN = ['ST', 'PSH']; //staging, partial staging
          var listOrder = [];
          for(let i = 0, l = listSelectedItem.length; i < l; i++) {
            if(allowStatusForPrintLPN.indexOf(listSelectedItem[i].odr_sts_code) == -1) {
              listOrderCannotPrintLPN.push(listSelectedItem[i].ord_number);
            }
            listOrder.push(listSelectedItem[i].odr_id);
          }
          if(listOrderCannotPrintLPN.length) {
            this.showMessage('danger', this.listErrMsg.PR2);
          }
          else {
            this.printLPN(listOrder.join(','));
          }
        }
        break;

      case 'print-wap-lpn':
        if (listSelectedItem.length != 1) {
          this.showMessage('danger', this.listErrMsg.PRW1);
        }
        else {
          if(listSelectedItem[0].odr_sts_code == 'CC') {
            this.showMessage('danger', this.listErrMsg.PRW2);
          }
          else {
            this.printWapOdrNum = listSelectedItem[0].ord_number;
            this.printWapOdrId = listSelectedItem[0].odr_id;
            (<Control>this.printWapLPNForm.controls['num_of_pallet']).updateValue('');
            this.showErrorWapLPN = false;
            jQuery('#print-wap-lpn').modal('show');
          }
        }
        break;

      case 'create-bol':
        if (listSelectedItem.length < 1) {
          this.showMessage('danger', this.listErrMsg.BOL1);
        }
        else {
          if(listSelectedItem.length > 1) {
            this.showMessage('danger', this.listErrMsg.BOL2);
          }
          else {
            var allowStatus = ['ST', 'PSH', 'RS', 'SS']; //staging, partial staging, Ready To Ship, Scheduled To Ship
            if(allowStatus.indexOf(listSelectedItem[0].odr_sts_code) == -1) {
              this.showMessage('danger', this.listErrMsg.BOL3);
            }
            else {
              if(listSelectedItem[0].bol == 'New' || listSelectedItem[0].bol == 'Final') {
                this.showMessage('danger', 'BOL ' + listSelectedItem[0].bol_num +' has been created for this Order already!');
              }
              else {
                this._router.parent.navigateByUrl('/outbound/bill-of-ladings/' + listSelectedItem[0].cus_id + '/' + listSelectedItem[0].odr_id + '/new');
              }
            }
          }
        }
        break;

      case 'create-work-order':
        if (listSelectedItem.length < 1) {
          window.location.href = `${window.location.origin}/invoice/#/work-order-list/new`
        }
        else {
          if (listSelectedItem.length > 1) {
            this.showMessage('danger', this.listErrMsg.WO2);
          }
          else {
            if (listSelectedItem[0].work_order) {
              this.showMessage('danger', this.listErrMsg.WO4.replace('{{work-order-number}}', listSelectedItem[0].work_order_number));
            }
            else {
              window.location.href = `${window.location.origin}/invoice/#/work-order/new/${listSelectedItem[0].cus_id}/${listSelectedItem[0].ord_number}`
            }
          }
        }
        break;

      case 'cancel-order':
        if (listSelectedItem.length < 1) {
          this.showMessage('danger', this.listErrMsg.CC1);
        }
        else {
          if(listSelectedItem.length > 1) {
            this.showMessage('danger', this.listErrMsg.CC3);
          }
          else {

              var sts_name = '';
              sts_name = listSelectedItem[0]['odr_sts'];

              if (sts_name.toLowerCase() == "staging" && listSelectedItem[0]['bol'].toLowerCase() == 'final') {
                  this.showMessage('danger', this._func.msg('WMS005'));
                  break;
              }

              if (listSelectedItem[0]['odr_type'].toLowerCase() == 'xdk') {
                  this.showMessage('danger', this._func.msg('WMS006'));
                  break;
              }

              var notAllowStatusForCancel = ['SH', 'HO', 'CC', 'PRC']; // shipped, hold, Canceled, partial shipped
            if(notAllowStatusForCancel.indexOf(listSelectedItem[0].odr_sts_code) != -1) {
              this.showMessage('danger', this.listErrMsg.CC2);
            }
            else {
              this._router.parent.navigateByUrl('/outbound/cancel-order/' + listSelectedItem[0].odr_id);
            }
          }
        }
        break;

        case 'cancel-order-by-sku':
          if (listSelectedItem.length < 1) {
            this.showMessage('danger', this.listErrMsg.CC1);
          }
          else {
            if(listSelectedItem.length > 1) {
              this.showMessage('danger', this.listErrMsg.CC3);
            } else {
              var notAllowStatusForCancel = ['SH', 'CC']; // shipped, Canceled
              if(notAllowStatusForCancel.indexOf(listSelectedItem[0].odr_sts_code) != -1) {
                this.showMessage('danger', this.listErrMsg.CC4);
              } else {
                this._router.parent.navigateByUrl('/outbound/cancel-order-by-sku/' + listSelectedItem[0].odr_id);
              }
            }
          }
          break;

      case 'shipping':
        if (listSelectedItem.length > 1) {
          this.showMessage('danger', this.listErrMsg.SP4);
        }
        else {
          if (listSelectedItem.length < 1) {
            this.showMessage('danger', this.listErrMsg.SP1);
          }
          else {
            var listOrderCannotShip = [];
            var allowStatusForShip = ['SS']; // Scheduled to Ship
            var listOrder = [];
            for(let i = 0, l = listSelectedItem.length; i < l; i++) {
              if(allowStatusForShip.indexOf(listSelectedItem[i].odr_sts_code) == -1) {
                listOrderCannotShip.push(listSelectedItem[i].ord_number);
              }
              listOrder.push(listSelectedItem[i].odr_id);
            }
            if(listOrderCannotShip.length) {
              this.showMessage('danger', this.listErrMsg.SP5);
            }
            else {
              this.resetShippedDateForm(listSelectedItem);
              jQuery('#shipped-date').modal('show');
            }
          }
        }
        break;

      case 'shippedDate':

        if (listSelectedItem.length < 1) {

          this.showMessage('danger', this._func.msg('WMS001'));

        } else if (listSelectedItem.length > 1 ) {

          this.showMessage('danger', this._func.msg('WMS002'));
        }else {

            if (listSelectedItem[0].odr_sts_code != 'SH') {

              this.showMessage('danger', this._func.msg('SD001'));
              break;
            }
          this.resetShippedDateForm(listSelectedItem);
          jQuery('#shipped-date').modal('show');

        }

        break;

        case 'completePiking':
            if (listSelectedItem.length < 1) {
          
              this.showMessage('danger', this._func.msg('WMS001'));
    
            } else if (listSelectedItem.length > 1 ) {
    
              this.showMessage('danger', this._func.msg('WMS002'));

            } else if (listSelectedItem[0].odr_sts_code !== 'PK') {
              this.showMessage('danger', this._func.msg('ODR002'));
            } else {
              this.completePikingOrder(listSelectedItem[0].odr_id);
            }
        break;

      case 'print-commercial-invoice':
        if (listSelectedItem.length < 1) {
          this.showMessage('danger', this._func.msg('WMSPCI001'));
        } else if (listSelectedItem.length > 1) {
          this.showMessage('danger', this._func.msg('WMSPCI002'));
        } else if (listSelectedItem[0].odr_sts_code !== 'SH') {
          this.showMessage('danger', this._func.msg('ODRPCI001'));
        } else if (!listSelectedItem[0]['ship_id']) {
          this.showMessage('danger', this._func.msg('ODRPCI002'));
        } else {
          const shipID = listSelectedItem[0]['ship_id'];
          const cusId = listSelectedItem[0]['cus_id'];
          const orderNum = listSelectedItem[0]['odr_number'];
          this.printCommercialInvoice(shipID, cusId, orderNum);
        }

        break;

        case 'scheduleShipDate':
          if (listSelectedItem.length < 1) {
            this.showMessage('danger', this._func.msg('WMS001'));  
          } else if (listSelectedItem.length > 1 ) {  
            this.showMessage('danger', this._func.msg('WMS002'));
          } else {
              if (listSelectedItem[0].csr_id !== this.userId && !this.hasScheduleToShipDatePermission) {
                this.showMessage('danger', this._func.msg('ODR005'));
              } else {
                const invalidStatuses = ['NW', 'AL', 'PK'];
                if (invalidStatuses.indexOf(listSelectedItem[0].odr_sts_code) !== -1) {
                  this.showMessage('danger', this._func.msg('ODR004'));
                } else {
                  this.resetScheduleShipDateForm(listSelectedItem);
                  jQuery('#schedule-ship-date').modal('show');
                }
              }
          }
        break;
        case 'routedDate':
          if (listSelectedItem.length < 1) {
            this.showMessage('danger', this._func.msg('WMS001'));  
          } else if (listSelectedItem.length > 1 ) {  
            this.showMessage('danger', this._func.msg('WMS002'));
          } else {
              if (listSelectedItem[0].csr_id !== this.userId) {
                this.showMessage('danger', this._func.msg('ODR006'));
              } else if (listSelectedItem[0].odr_sts_code != 'SS') {  
                this.showMessage('danger', this._func.msg('ODR007'));
              } else {
                this.resetRoutedDateForm(listSelectedItem);
                jQuery('#routed-date').modal('show');
              }
          }
        break;
        case 'merge-order':
        
          this.showForm = false;
          if (listSelectedItem.length < 2) {
            this.showMessage('danger', this._func.msg('ODR011'));
          } else {
            
            const customer = listSelectedItem[0].cus_code;
            const customerOrder = listSelectedItem[0].cus_odr_num.split('-')[0];
            const po = listSelectedItem[0].cus_po.split('-')[0];
            const status = listSelectedItem[0].odr_sts_code;
            if (listSelectedItem.find(x => x.cus_code !== customer)) {
                this.showMessage('danger', this.listErrMsg.MODR1);
            } else if (listSelectedItem.find(x => x.cus_po.split('-')[0] !== po)) {
              this.showMessage('danger', this.listErrMsg.MODR3);
            } else if (listSelectedItem.find(x => x.cus_odr_num.split('-')[0] !== customerOrder)) {
              this.showMessage('danger', this.listErrMsg.MODR4);
            } else if (listSelectedItem.find(x => x.odr_sts_code !== status)) {
              this.showMessage('danger', this.listErrMsg.MODR2);
            } else if (!this.statusAllowMerge.find(x => x === status)) {
              this.showMessage('danger', this.listErrMsg.MODR5);
            // }  else if (status === 'NW'&& listSelectedItem.find(x => x.odr_type === 'BAC')) {
            //   this.showMessage('danger', this.listErrMsg.MODR6);
            } else {
              setTimeout(() => {
              this.showForm = true;
              this.buildFormMergeOrder(listSelectedItem);
              jQuery('#merge_order').modal('show');
              });
            }
          }
          break;


        case 'complete-x-dock':
          if (listSelectedItem.length > 1) {
            this.showMessage('danger', this._func.msg('XD0001'));
          }
          else {
            if (listSelectedItem.length < 1) {
              this.showMessage('danger', this._func.msg('XD0002'));
            }
            else {
              if(listSelectedItem[0].odr_sts != 'XDP' && listSelectedItem[0].odr_type != 'XDK') {
                this.showMessage('danger', this._func.msg('XD0003'));
              }
              else {
                const whId = localStorage.getItem('whs_id');
                var data = {odr_id: listSelectedItem[0].odr_id};
                this.completeXDock(data, listSelectedItem[0].cus_id, whId);
            }
          }
        }
        break;
      }
    this.getSelectedRow = false;
  }

  private completePikingOrder(orderId) {
    let that = this;
    this.showLoadingOverlay = true;
    this._orderListService.completePikingOrder(orderId).subscribe(
      (data) => {
        this.showMessage('success', this._func.msg('ODR003'));
        this.showLoadingOverlay = false;
        setTimeout(function () {
          that.getOrderList(1);
        }, that.delayTime);
      },
      (err) => {
        this.showMessage('danger', this._func.parseErrorMessageFromServer(err));
        this.showLoadingOverlay = false;
      }
    )
  }

  private resetShippedDateForm(listSelectedItem) {
    this.showErrorShippedDate = false;
    jQuery('#d-order-num').text(listSelectedItem[0]['ord_number']);
    jQuery('#d-created-date').text(listSelectedItem[0]['created_at']);
    if(listSelectedItem[0]['shipped_dt']) {
      // jQuery('#shipped_date').val(listSelectedItem[0]['shipped_dt']).datepicker('setStartDate', new Date(listSelectedItem[0]['created_at']));
      (<Control>this.updateShippedDateForm.controls['shipped_date']).updateValue(listSelectedItem[0]['shipped_dt']);
    }
    else {
      // jQuery('#shipped_date').val('').datepicker('setStartDate', new Date(listSelectedItem[0]['created_at']));
      (<Control>this.updateShippedDateForm.controls['shipped_date']).updateValue('');
    }
  }

  private cancelOrder() {
    var listOrderConfirm = [];
    for(var i = 0; i < this.cancelList.length; i++) {
      if(this.cancelList[i].checked) {
        listOrderConfirm.push(this.cancelList[i].odr_id);
      }
    }
    if(!listOrderConfirm.length) {
      jQuery('#cancel-order').modal('hide');
    }
    else {
      this.showLoadingOverlay = true;
      this._orderListService.cancelOrder(listOrderConfirm.join(','))
        .subscribe(
          data => {
            this.showMessage('success', data.data);
            this.showLoadingOverlay = false;
            jQuery('#cancel-order').modal('hide');
            this.getOrderList(this.Pagination['current_page']);
          },
          err => {
            this.showMessage('danger', this._func.parseErrorMessageFromServer(err));
            this.showLoadingOverlay = false;
            jQuery('#cancel-order').modal('hide');
          }
        );
    }
  }

  refreshList() {
    this.refreshRowData = true;
    setTimeout(()=>{
      this.refreshRowData = false;
    }, 400);
  }

  buildForm() {
    this.onlineImportForm =this.fb.group({
      customer: ['', Validators.required]
    });
  }

  buildFormPrintWapLPN() {
    this.printWapLPNForm =this.fb.group({
      num_of_pallet: ['', Validators.compose([Validators.required, this._Valid.isZero,this._Valid.inPutOnlyNumber])]
    });
  }

  buildFormUpdateShippedDate() {
    this.updateShippedDateForm =this.fb.group({
      shipped_date: ['', Validators.required]
    });
  }

  fileChangeEvent(fileInput: any){
    this.filesToUpload = <Array<File>> fileInput.target.files;
  }

  validImportFile(formData) {
    return new Promise((resolve, reject) => {
        this.showLoadingOverlay = true;
        var xhr = new XMLHttpRequest();
        xhr.open("POST", this._API.API_ORDERS_CORE + '/orders/validate-import', true);
        xhr.setRequestHeader("Authorization", 'Bearer ' +this._func.getToken());
        xhr.onreadystatechange = function () {
            if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                    resolve(JSON.parse(xhr.response));
                } else {
                    reject(xhr.response);
                }
            }
        };

        xhr.send(formData);
    });
  }

  private dowloadFile() {
      var xhrl = new XMLHttpRequest();

      xhrl.open("GET", this._API.API_ORDERS_CORE + "/orders/get-file-error", true);
      xhrl.setRequestHeader("Authorization", 'Bearer ' +this._func.getToken());
      xhrl.responseType = 'blob';

      xhrl.onreadystatechange = function () {
        if (xhrl.readyState === 4 && xhrl.status === 200) {
          var blob = new Blob([this.response], {type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'});
          saveAs.saveAs(blob, 'Report.xlsx');
        }
      };

      xhrl.send();
  }

  importOnlineOrder(data) {
    var that = this;
    this.showError = true;
    this.messages = null;
    if(this.onlineImportForm.valid) {
      var formData: any = new FormData();
      var xhr = new XMLHttpRequest();
      var files = this.filesToUpload;

      for(var i = 0; i < files.length; i++) {

          var fileExtension = files[i].name.substr(files[i].name.length - 5);

          if (fileExtension != ".xlsx") {

              this.messages = this._func.Messages('danger', this._func.msg('CF003'));
              return false;
          }

          formData.append("file", files[i], files[i].name);
      }
      formData.append("file_type", 'xlsx');

      formData.append('cus_id', data.customer);
      this.validImportFile(formData)
        .then(
          (result) => {
            swal({
                text: "There will be " + result['data']['total_rows'] + " Orders imported to system. Are you sure you want to process?",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'OK',
                cancelButtonText: 'Cancel',
                confirmButtonClass: 'btn btn-primary',
                cancelButtonClass: 'btn btn-default',
            }).then(function(agree) {
                if (agree) {
                  that.showLoadingOverlay = true;
                  that._http.get(that._API.API_ORDERS_CORE + "/orders/process-import/"+data.customer, {headers: that._func.AuthHeader()})
                    .map(res => res)
                    .subscribe(
                        data => {
                          var message;
                          if(typeof data == 'object' && data['_body']) {
                            message = JSON.parse(data['_body']).data.message;
                            // this.showMessage('danger', errors.message || errors.detail);
                          }
                          that.showMessage('success', message);
                          that.showLoadingOverlay = false;
                          jQuery("#importOnlineOrder").val("");
                          that.getOrderList();
                        },
                        err => {
                          that.showLoadingOverlay = false;
                          that.showMessage('danger', that._func.parseErrorMessageFromServer(err));
                        }
                    );
                }
            });
            this.showLoadingOverlay = false;
          },
          (error) => {
            try {
                error = JSON.parse(error);
                var errMsg = error.errors?error.errors.message : '';
                if(error.data && error.data.code == '422') {
                    that.showMessage('danger', error.data.message);
                }
                else {
                    if (error.errors && error.errors.status_code == 400) {
                        swal({
                            text: "Import data has some errors, please download file to check!",
                            type: 'warning',
                            showCancelButton: true,
                            confirmButtonText: 'OK',
                            cancelButtonText: 'Cancel',
                            confirmButtonClass: 'btn btn-primary',
                            cancelButtonClass: 'btn btn-default',
                        }).then(function(agree) {
                            if (agree) {
                              jQuery("#importOnlineOrder").val("");
                              that.dowloadFile();
                            }
                        });
                    }
                    else {
                        if(errMsg) {
                            that.showMessage('danger', errMsg);
                        }
                    }
                }
            }
            catch(err) {
            }
            this.showLoadingOverlay = false;
          }
        );
    }
  }

  printPackingSlip(orderID, orderNum, customerName) {
    var that = this;
    let api = that._API.API_ORDERS_CORE  + '/orders/print-pdf/' + orderID;
    try{
      let xhr = new XMLHttpRequest();
      xhr.open("GET", api , true);
      xhr.setRequestHeader("Authorization", 'Bearer ' + that._func.getToken());
      xhr.responseType = 'blob';

      xhr.onreadystatechange = function () {
        if(xhr.readyState == 2) {
          if(xhr.status == 200) {
            xhr.responseType = "blob";
          } else {
            xhr.responseType = "text";
          }
        }

        if(xhr.readyState === 4) {
          if(xhr.status === 200) {
            var fileName = orderNum + '-' + customerName + '-packing slip';
            var blob = new Blob([this.response], {type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'});
            saveAs.saveAs(blob, fileName + '.pdf');
          }else{
            let errMsg = '';
            if(xhr.response) {
              try {
                var res = JSON.parse(xhr.response);
                errMsg = res.errors && res.errors.message ? res.errors.message : xhr.statusText;
              }
              catch(err){errMsg = xhr.statusText}
            }
            else {
              errMsg = xhr.statusText;
            }
            if(!errMsg) {
              errMsg = that._func.msg('VR100');
            }
            that.showMessage('danger', errMsg);
          }
        }
      }
      xhr.send();
    }catch (err){
      that.showMessage('danger', that._func.msg('VR100'));
    }
  }

  printLPN(listOrderID) {
    var that = this;
    let api = that._API.API_Pack  + '/packs-assign-pallets-printLpns?odr_hdr_ids=' + listOrderID;
    try{
      let xhr = new XMLHttpRequest();
      xhr.open("GET", api , true);
      xhr.setRequestHeader("Authorization", 'Bearer ' + that._func.getToken());
      xhr.responseType = 'blob';

      xhr.onreadystatechange = function () {
        if(xhr.readyState == 2) {
          if(xhr.status == 200) {
            xhr.responseType = "blob";
          } else {
            xhr.responseType = "text";
          }
        }

        if(xhr.readyState === 4) {
          if(xhr.status === 200) {
            var today = new Date();
            var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
              "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
            ];
            var day = today.getDate();
            var month = monthNames[today.getMonth()];
            var year = today.getFullYear().toString().substr(2,2);
            var fileName = 'AssignPallet_' + month+ day + year;
            var blob = new Blob([this.response], {type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'});
            saveAs.saveAs(blob, fileName + '.pdf');
          }else{
            let errMsg = '';
            if(xhr.response) {
              try {
                var res = JSON.parse(xhr.response);
                errMsg = res.errors && res.errors.message ? res.errors.message : xhr.statusText;
              }
              catch(err){errMsg = xhr.statusText}
            }
            else {
              errMsg = xhr.statusText;
            }
            if(!errMsg) {
              errMsg = that._func.msg('VR100');
            }
            that.showMessage('danger', errMsg);
          }
        }
      }
      xhr.send();
    }catch (err){
      that.showMessage('danger', that._func.msg('VR100'));
    }
  }

  printCommercialInvoice(shipID: string, cusId: string, orderNum: string) {
    var that = this;
    let api = `${that._API.API_ORDERS_CORE}/invoice/${shipID}/print?cus_id=${cusId}`;
    try {
      let xhr = new XMLHttpRequest();
      xhr.open("GET", api, true);
      xhr.setRequestHeader("Authorization", 'Bearer ' + that._func.getToken());
      xhr.responseType = 'blob';

      xhr.onreadystatechange = function () {
        if (xhr.readyState == 2) {
          if (xhr.status == 200) {
            xhr.responseType = "blob";
          } else {
            xhr.responseType = "text";
          }
        }

        if (xhr.readyState === 4) {
          if (xhr.status === 200) {
            var fileName = 'Commercial-Invoice-' + orderNum;
            var blob = new Blob([this.response], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
            saveAs.saveAs(blob, fileName + '.pdf');
          } else {
            let errMsg = '';
            if (xhr.response) {
              try {
                var res = JSON.parse(xhr.response);
                errMsg = res.errors && res.errors.message ? res.errors.message : xhr.statusText;
              }
              catch (err) { errMsg = xhr.statusText }
            }
            else {
              errMsg = xhr.statusText;
            }
            if (!errMsg) {
              errMsg = that._func.msg('VR100');
            }
            that.showMessage('danger', errMsg);
          }
        }
      }
      xhr.send();
    } catch (err) {
      that.showMessage('danger', that._func.msg('VR100'));
    }
  }

  checkInputNumber(evt){
    evt = evt || window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (!evt.ctrlKey && charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 46){
      evt.preventDefault();
    }
  }

  printWapLPN() {
    var that = this;
    this.showErrorWapLPN = true;
    if(this.printWapLPNForm.valid) {
      let api = that._API.API_ORDERS_CORE + '/orders/print-wap-lpn/' + that.printWapOdrId + '?new_lpn=' + (<Control>this.printWapLPNForm.controls['num_of_pallet']).value;
      try{
        that.showLoadingOverlay = true;
        let xhr = new XMLHttpRequest();
        xhr.open("GET", api , true);
        xhr.setRequestHeader("Authorization", 'Bearer ' + that._func.getToken());
        xhr.responseType = 'blob';

        xhr.onreadystatechange = function () {
          if(xhr.readyState == 2) {
            if(xhr.status == 200) {
              xhr.responseType = "blob";
            } else {
              xhr.responseType = "text";
            }
          }

          if(xhr.readyState === 4) {
            if(xhr.status === 200) {
              var fileName = `License_Plate_${that.printWapOdrNum}`;
              var blob = new Blob([this.response], {type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'});
              saveAs.saveAs(blob, fileName + '.pdf');
            }else{
              let errMsg = '';
              if(xhr.response) {
                try {
                  var res = JSON.parse(xhr.response);
                  errMsg = res.errors && res.errors.message ? res.errors.message : xhr.statusText;
                }
                catch(err){errMsg = xhr.statusText}
              }
              else {
                errMsg = xhr.statusText;
              }
              if(!errMsg) {
                errMsg = that._func.msg('VR100');
              }
              that.showMessage('danger', errMsg);
            }
            jQuery('#print-wap-lpn').modal('hide');
            that.showLoadingOverlay = false;
          }
        }
        xhr.send();
      }catch (err){
        that.showMessage('danger', that._func.msg('VR100'));
        that.showLoadingOverlay = false;
      }
    }
  }

  private printWavepickTicket(orderID, wvNum){
    let that = this;
    let api = that._API.API_Wavepick  + '/print?odr_id=' +  orderID;
    try{
      let xhr = new XMLHttpRequest();
      xhr.open("GET", api , true);
      xhr.setRequestHeader("Authorization", 'Bearer ' +that._func.getToken());
      xhr.responseType = 'blob';

      xhr.onreadystatechange = function () {
        if(xhr.readyState == 2) {
          if(xhr.status == 200) {
            xhr.responseType = "blob";
          } else {
            xhr.responseType = "text";
          }
        }

        if(xhr.readyState === 4) {
          if(xhr.status === 200) {
            //update printed label value
            var today = new Date();
            var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
              "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
            ];
            var day = today.getDate();
            var month = monthNames[today.getMonth()];
            var year = today.getFullYear().toString().substr(2,2);
            // var fileName = 'Wavepick_' + month+ day + year;
            let fileName:string = wvNum;
            var blob = new Blob([this.response], {type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'});
            saveAs.saveAs(blob, fileName + '.pdf');
          }else{
            let errMsg = '';
            if(xhr.response) {
              try {
                var res = JSON.parse(xhr.response);
                errMsg = res.errors && res.errors.message ? res.errors.message : xhr.statusText;
              }
              catch(err){errMsg = xhr.statusText}
            }
            else {
              errMsg = xhr.statusText;
            }
            if(!errMsg) {
              errMsg = that._func.msg('VR100');
            }
            that.showMessage('danger', errMsg);
          }
        }
      }
      xhr.send();
    }catch (err){
      that.showMessage('danger', that._func.msg('VR100'));
    }
  }

  private expandTable = false;
  private viewListFullScreen() {
    this.expandTable = true;
    setTimeout(() => {
      this.expandTable = false;
    }, 500);
  }

  private setDataExport(data:Array<any> = []) {
    this.dataExport = [];
    for (let i in data) {
      this.dataExport.push([
        data[i].odr_sts_name,
        data[i].odr_num,
        data[i].cus_code,
        data[i].odr_type_name,
        data[i].back_odr === 1 ? 'Yes' : 'No',
        data[i].ship_to_name,
        data[i].ship_by_dt,
        data[i].cancel_by_dt,
        data[i].shipped_dt,
        data[i].csr_name,
        data[i].created_by,
        data[i].created_at
      ]);
    }
  }

  private saveShippedDate() {
    this.showErrorShippedDate = true;
    var data = {ship_dt: jQuery("#shipped_date").val()};
    this.checkValidateComplete();
    if(this.updateShippedDateForm.valid) {
      jQuery('#shipped-date').modal('hide');

      // ship order
      this.showLoadingOverlay = true;
      this._orderListService.shipOrder(this.listSelectedOrder[0], jQuery("#shipped_date").val())
        .subscribe(
          data => {
            this.showLoadingOverlay = false;
            this.getOrderList(this.Pagination['current_page']);
          },
          err => {

            this.showMessage('danger', this._func.parseErrorMessageFromServer(err));
            this.showLoadingOverlay = false;
          }
        );
    }
  }
    private revertOrder(){

        this.getSelectedRow = 'revert-order';

    }

    private mergeOrder() {
      this.getSelectedRow = 'merge-order';
    }

    buildFormUpdateScheduleShipDate() {
      this.updateScheduleShipDateForm =this.fb.group({
        schd_ship_dt: ['', Validators.required]
      }, {validator: this._Valid.validDate(['schd_ship_dt'])});
      setTimeout(() => {
        this.fbdFunc.handelDropdownDatePicker(this.updateScheduleShipDateForm);
      }, 200);
    }

    buildFormUpdateRoutedDate() {
      this.updateRoutedDateForm =this.fb.group({
        routed_dt: ['', Validators.required]
      });
      setTimeout(() => {
        this.fbdFunc.handelDropdownDatePicker(this.updateRoutedDateForm, '.datepicker-routed');
      }, 300);
    }

    private resetScheduleShipDateForm(listSelectedItem) {
      this.showErrorShippedDate = false;
      jQuery('#schd-order-num').text(listSelectedItem[0]['ord_number']);
      jQuery('#schd-created-date').text(listSelectedItem[0]['created_at']);

      if(listSelectedItem[0]['schd_ship_dt']) {
        (<Control>this.updateScheduleShipDateForm.controls['schd_ship_dt']).updateValue(listSelectedItem[0]['schd_ship_dt']);
      }
      else {
        (<Control>this.updateScheduleShipDateForm.controls['schd_ship_dt']).updateValue('');
      }
    }

    private resetRoutedDateForm(listSelectedItem) {
      this.showErrorRoutedDate = false;
      jQuery('#routed-order-num').text(listSelectedItem[0]['ord_number']);
      jQuery('#routed-created-date').text(listSelectedItem[0]['created_at']);
      jQuery('#routed_dt').val(listSelectedItem[0]['routed_dt']);
      if(listSelectedItem[0]['routed_dt']) {
        (<Control>this.updateRoutedDateForm.controls['routed_dt']).updateValue(listSelectedItem[0]['routed_dt']);
      }
      else {
        (<Control>this.updateRoutedDateForm.controls['routed_dt']).updateValue('');
      }
    }

    public saveScheduleShipDate() {
      this.showErrorScheduleShipDate = true;
      if(this.updateScheduleShipDateForm.valid) {
        jQuery('#schedule-ship-date').modal('hide');
        const data = this.updateScheduleShipDateForm.value;
  
        // ship order
        this.showLoadingOverlay = true;
        this._orderListService.updateScheduleRouteDate(this.listSelectedOrder[0], JSON.stringify(data))
          .subscribe(
            data => {
              this.showMessage('success', this.listErrMsg.USC1);
              this.getOrderList(this.Pagination['current_page']);
            },
            err => {
  
              this.showMessage('danger', this._func.parseErrorMessageFromServer(err));
            },
            () => {
              this.showLoadingOverlay = false;
            }
          );
      }
    }

    public saveRoutedDate() {
      this.showErrorRoutedDate = true;
      if(this.updateRoutedDateForm.valid) {
        jQuery('#routed-date').modal('hide');
        const data = this.updateRoutedDateForm.value;
  
        // ship order
        this.showLoadingOverlay = true;
        this._orderListService.updateScheduleRouteDate(this.listSelectedOrder[0], JSON.stringify(data))
          .subscribe(
            data => {
              this.showMessage('success', this.listErrMsg.URD1);
              this.getOrderList(this.Pagination['current_page']);
            },
            err => {
              this.showMessage('danger', this._func.parseErrorMessageFromServer(err));
            },
            () => {
              this.showLoadingOverlay = false;
            }
          );
      }
    }

    showDateTimePicker($event) {
      setTimeout(() => {
        const input = jQuery($event.target);
        const dt = input.next('div.bootstrap-datetimepicker-widget');
        dt.css({
            display: 'block',
            position: 'fixed',
            top: input.offset().top - 5,
            left: 325
        });
      }, 0);
    }

    updateMessages(messages) {
      this.messages = messages;
    }

    buildFormMultipleComment() {
      this.multiCommentForm =this.fb.group({
        orderIds: [''],
        cus_notes: [''],
        in_notes: [''],
        csr_notes: [''],
      });
    }

    private resetFormMultipleComment(items: any[]) {
      (<Control>this.multiCommentForm.controls['cus_notes']).updateValue('');
      (<Control>this.multiCommentForm.controls['in_notes']).updateValue('');
      (<Control>this.multiCommentForm.controls['csr_notes']).updateValue('');
      
      let orderIds = [];
      for(let i = 0; i < items.length; i++) {
        if(items[i].odr_type !== 'CC') {
          orderIds.push(items[i].odr_id);
        }
      }
      
      (<Control>this.multiCommentForm.controls['orderIds']).updateValue(orderIds);
    }

    saveComment() {
      if (this.multiCommentForm.valid) {
        let data = {};
        data['orderIds'] = this.multiCommentForm.value['orderIds'];
        data['cus_notes'] = this.multiCommentForm.value['cus_notes'];
        
        if (this.hasWHComment) {
          data['in_notes'] = this.multiCommentForm.value['in_notes'];
        }
        
        if (this.hasCSRComment) {
          data['csr_notes'] = this.multiCommentForm.value['csr_notes'];
        }
        
        data = JSON.stringify(data);
        this.showLoadingOverlay = true;
        jQuery('#multiple-comment').modal('hide');
        this._orderListService.updateComment(data).subscribe(
            data => {
                this.messages = this._func.Messages('success', this._func.msg('CSRC01'));
                this.showLoadingOverlay = false;
            },
            err => {
                this.showLoadingOverlay = false;
                this.showMessage('danger', this._func.parseErrorMessageFromServer(err));
            },
            () => {}
        );
      }
    }

    private checkValidateComplete() {
      let actDateCtrl = this.updateShippedDateForm.controls['shipped_date'];
      let actDateVal = jQuery("input[name='shipped_date']").val().trim();
      if (actDateVal) {
          const currMonth = new Date().getMonth() + 1;
          const currYear = new Date().getFullYear();
          const currDate = new Date().getDate();
          const monthVal = new Date(actDateVal).getMonth() + 1;
          const yearVal = new Date(actDateVal).getFullYear();
          if ((yearVal == currYear && (monthVal == currMonth-1 && currDate > 7)||monthVal < currMonth - 1)||(yearVal < currYear-1)||(yearVal == currYear-1 && currMonth != 1 && currDate > 7)) {
              actDateCtrl.setErrors({'invalid': true, 'required': false});
              this.showErrorShippedDate = true;
              return false; 
          }
          actDateCtrl.setErrors(null);
      }
      
      return true;
  }

  buildFormMergeOrder(items: any[] = null) {
    this.mergeOrderFrom = this.fb.group({
      whs_id: [localStorage.getItem('whs_id')],
      cus_id: [items ? items[0].cus_id : '', Validators.required],
      targetOrderId: [items ? JSON.stringify(items[0].odr_id):'', Validators.required],
      orderIds: [[]],
      cus_odr_num: ['', Validators.required],
      cus_po: ['', Validators.required],
    });
    if(items) {
      const orderIds = [];
      this.orderNumberList = [];
      this.customerList = [];
      this.POList = [];
      for(let i = 0; i < items.length; i++) {
        if (!this.customerList.find(x => x.value == items[i].cus_odr_num)) {
          this.customerList.push({value : items[i].cus_odr_num, display: items[i].cus_odr_num});
        }
        if (!this.POList.find(x => x.value == items[i].cus_po)) {
          this.POList.push({value : items[i].cus_po, display: items[i].cus_po});
        }
        this.orderNumberList.push({value : JSON.stringify(items[i].odr_id), display: items[i].ord_number});
        orderIds.push(items[i].odr_id);
      }
      (<Control>this.mergeOrderFrom.controls['orderIds']).updateValue(orderIds);
      this.listSelectedItem = items;
    }
  }

  private checkOrder(evt) {
    (<Control>this.mergeOrderFrom.controls['targetOrderId']).updateValue(evt.target.value);
  }

  submitMergeOrder() {
    if (!this.mergeOrderFrom.valid) {
      this.showErrorMergeOrder = true;
      return;
    }
    if (this.mergeOrderFrom.valid) {
      this.showLoadingOverlay = true;
      jQuery('#merge_order').modal('hide');
      this._orderListService.mergeOrder(JSON.stringify(this.mergeOrderFrom.value)).subscribe(
          data => {
              this.messages = this._func.Messages('success', this._func.msg('ODR010'));
              this.getOrderList();
              this.showLoadingOverlay = false;
          },
          err => {
              this.showLoadingOverlay = false;
              this.showMessage('danger', this._func.parseErrorMessageFromServer(err));
          },
          () => {}
      );
    }
  }

  private completeXDockRow() {
    this.getSelectedRow = 'complete-x-dock';
  }

  private completeXDock(data, cusId, whsId) {
    let that = this;
    this.showLoadingOverlay = true;
    this._orderListService.completeXDock(JSON.stringify(data), cusId, whsId).subscribe(
      (data) => {
        this.showMessage('success', this._func.msg('XD0004'));
        this.showLoadingOverlay = false;
        setTimeout(function () {
          that.getOrderList(1);
        }, that.delayTime);
      },
      (err) => {
        this.showMessage('danger', this._func.parseErrorMessageFromServer(err));
        this.showLoadingOverlay = false;
      }
    )
  }
  selectionChanged(evt) {
    const listSelectedItem = evt;
    this.isMerged = false;
    if (listSelectedItem.length > 0 && listSelectedItem.find(i => i.odr_sts_code == 'MG')) {
      this.isMerged = true;
      return
    }
  }

  private addFieldForPacking() {
    //this.headerDef = this.headerDef.filter(x=> x.id!=='ver_table');
     this.headerDef.push.apply(this.headerDef, [
                            {id: 'alloc_qty', name: 'Allocated QTY', width: 130},
                            {id: 'alloc_ctns', name: 'Allocated CTNS', width: 130},
                            {id: 'picked_qty', name: 'Picked QTY', width: 130},
                            {id: 'picked_ctns', name: 'Picked CTNS', width: 130}]);
    
  }

  private addFieldForOrder() {
    //this.headerDef = this.headerDef.filter(x=> x.id!=='ver_table');
     this.headerDef.push.apply(this.headerDef, [
                            {id: 'requested_qty', name: 'Requested QTY', width: 130},
                            {id: 'requested_ctns', name: 'Requested CTNS', width: 130},
                            {id: 'alloc_qty', name: 'Allocated QTY', width: 130},
                            {id: 'alloc_ctns', name: 'Allocated CTNS', width: 130},
                            {id: 'picked_qty', name: 'Picked QTY', width: 130},
                            {id: 'picked_ctns', name: 'Picked CTNS', width: 130}]);
    
  }
}
