import {Component,OnInit} from "@angular/core";
import { Router,RouteParams, RouterLink, ROUTER_DIRECTIVES } from '@angular/router-deprecated';
import { FORM_DIRECTIVES,FormBuilder} from '@angular/common';
import {UserService, Functions} from '../../../../common/core/load';
import {WMSBreadcrumb,WMSPagination,PaginationControlsCmp,ColorPickerDirective,WMSMessages,ColorPickerService,PaginationService} from '../../../../common/directives/directives';
import {CustomerServices} from "../../../../system-setup/customer/customer-service";
import {RFGUNPUTAWAYServices} from "../rf-putaway.service";
@Component({
  selector: 'rf-putaway-update',
  providers: [CustomerServices,RFGUNPUTAWAYServices,PaginationService,ColorPickerService,UserService],
  directives: [FORM_DIRECTIVES,WMSPagination,PaginationControlsCmp,ColorPickerDirective,WMSMessages,WMSBreadcrumb],
  templateUrl: 'rf-putaway-update.html',
})
export class RF_PutAwayUpdateComponent {

  private allowAccess;
  private showLoadingOverlay;
  private messages;
  private rfGunPermission;
  private whs_id;
  private LocPalletModel={};
  private skuDetail={};
  private suggestLocModel;
  private suggestPalletLoc;

  constructor(private rfGunService:RFGUNPUTAWAYServices,
              private _router: Router,
              private params: RouteParams,
              private _user:UserService,
              private _Func: Functions)
  {
    this.checkPermission();
  }


  checkPermission(){

    this.showLoadingOverlay = true;
    this._user.GetPermissionUser().subscribe(
      data => {
        this.showLoadingOverlay = false;
        this.rfGunPermission = this._user.RequestPermission(data, 'viewRFGUN');
        if(this.rfGunPermission) {
          this.allowAccess=true;
          this.whs_id = localStorage.getItem('whs_id');
          this.getLocaPallet();
        }
        else{
          this.redirectDeny();
        }
      },
      err => {
        this.parseError(err);
        this.showLoadingOverlay = false;
      }
    );
  }

  redirectDeny(){
    this._router.parent.navigateByUrl('/deny');
  }

  removeEmptyKey(object)
  {
    let data_tmp=JSON.parse(JSON.stringify(object));
    Object.keys(object['codes']).map(
      key =>{
        if(object['codes'][key]==''){
          delete data_tmp['codes'][key];
        }
      }
    );
    return data_tmp
  }


  getLocaPallet()
  {
    this.showLoadingOverlay=true;
    let params="/suggest/"+this.params.get('id');
    this.rfGunService.RF_GUN_API_GET(this.whs_id,params)
      .subscribe(
        data => {
          this.showLoadingOverlay=false;
          this.LocPalletModel = data;
          this.set_skuDetail(data['hdr'][0]);
        },
        err => {
          this.showLoadingOverlay=false;
          this.parseError(err);
        },
        () => {
        }
      );

  }


  set_skuDetail(data)
  {

    this.skuDetail['gr_hdr_num']=data['gr_hdr_num'];
    this.skuDetail['sku']=data['sku'];
    this.skuDetail['plt']=data['total']+'/'+data['actual'];

  }


  /*
   * updateLocation
   * */

  updateLocation(data):void
  {
    let data_r={};
    data_r['codes']=(<any>Object).assign({},data.split("\n"));
    data_r=this.removeEmptyKey(data_r);
    this.showLoadingOverlay=true;
    this.rfGunService.RF_GUN_API_UPDATE(this.whs_id,JSON.stringify(data_r))
      .subscribe(
        data => {
          this.messages=this._Func.Messages('success',data['message']);
          this.showLoadingOverlay=false;
          this.suggestPalletLoc=null;
          // reget
          this.getLocaPallet();
        },
        err => {
          this.showLoadingOverlay=false;
          this.parseError(err);
        },
        () => {
        }
      );

  }

  showSuggetPalletLoc()
  {
    if(!this.LocPalletModel['dtl'])
    {
      this.messages=this._Func.Messages('danger','No Results Found.');
    }
    else{
      let dataSuggest = [this.LocPalletModel['dtl'][0],this.LocPalletModel['dtl'][1],this.LocPalletModel['dtl'][2]];
      this.suggestPalletLoc= dataSuggest.join('\n');
    }
  }

  showSuggetPalletLocDbClick(){
    if(!this.LocPalletModel['dtl'])
    {
      this.messages=this._Func.Messages('danger','No Results Found.');
    }
    else{
      this.suggestPalletLoc= this.LocPalletModel['dtl'].join('\n');
    }
  }

  // Show error when server die or else
  private parseError (err) {
    err = err.json();
    this.messages = this._Func.Messages('danger', err.errors.message);
  }

}
