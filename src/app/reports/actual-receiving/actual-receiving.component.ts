
import {Component} from '@angular/core';
import {API_Config} from '../../common/core/API_Config';
import {BaseTable} from '../../common/core/classes_us/base-table';
import {TableFunctions} from '../../common/core/table-functions';
import {DateFunctions} from '../../common/core/functions/date-functions';
import {BaseService} from '../../common/core/classes_us/base-service';
import {CustomersService} from '../../master-data/customers/customers-service';
import {TableStructure} from '../../common/directives/directives';

@Component ({
    selector: 'actual-receiving',
    directives: [
        TableStructure
    ],
    templateUrl: '../../common/directives/table-structure/table.component.html',
    providers: [
        BaseService,
        TableFunctions,
        DateFunctions,
        CustomersService
    ]
})

export class ActRcvComponent extends BaseTable
{
    public tableID = 'available-inventory';
    public url = this._api.API_ACTUAL_RECEIVING;
    public headerUrlSuffix = 'rep';
    public title = 'Actual Receiving';
    public permissionPage = 'viewReport';
    
    public headerDef = [
      {
          id: 'ver_table',
          value: 1 + Math.random() * 10000
      },
      this.columnList.warehouse,
      this.columnList.customer,
      this.columnList.grCreatedAt,
      this.columnList.container,
      this.columnList.po,
      this.columnList.sku,
      this.columnList.upc,
      this.columnList.color,
      this.columnList.size,
      this.columnList.expPackSize,
      this.columnList.expCtnTtl,
      this.columnList.actPackSize,
      this.columnList.actCtnTtl,
      this.columnList.packDisc,
      this.columnList.ctnDisc,
      this.columnList.length,
      this.columnList.width,
      this.columnList.height,
      this.columnList.volume
    ];
    
    public unsortableColumns = [
        'warehouse'  
    ];
    
    
    public searches = [
        this.searchList.customer,
        this.searchList.container,
        this.searchList.sku,
        this.searchList.createdGrStart,
        this.searchList.createdGrFinish,
    ];
    
    /*
    ****************************************************************************
    */
    
    constructor (
        public _api: API_Config,
        public _tableFunc: TableFunctions,
        public _tableService: BaseService,
        public _customersService: CustomersService
    )
    {
        super();

        this._tableFunc.permissionCall(this);
    }
}