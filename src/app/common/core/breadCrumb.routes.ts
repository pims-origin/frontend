/**
 * Created by admin on 6/16/2016.
 */
import {Injectable} from '@angular/core';
@Injectable()
export class breadCrumbRoutes {
  private BASE_URL = '#';
  constructor(){

  }
  /*========================HOME===========================*/
  private HOME = {
    text: 'Home',
    url : '#/dashboard'
  }
  /*========================USER MANAGEMENT=================*/
  private USER_MANAGEMENT_URL = this.BASE_URL + '/users';
  private USER_MANAGEMENT = {
    text: 'User Management',
    url : this.USER_MANAGEMENT_URL
  }

  private USER_LIST_URL = {
    text: 'User List',
    url : this.USER_MANAGEMENT_URL
  }

  private CREATE_USER_URL = {
    text: 'Create User',
    url : this.USER_MANAGEMENT_URL + '/new'
  }

  getUserListBreadCrumb(){
    let breadCrumb = [];
    breadCrumb.push(this.HOME);
    breadCrumb.push(this.USER_MANAGEMENT);
    breadCrumb.push(this.USER_LIST_URL);
    return breadCrumb;
  }
}
