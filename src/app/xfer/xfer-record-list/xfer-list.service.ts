import {Component, Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {API_Config, Functions} from '../../common/core/load';
declare var saveAs: any;

@Injectable()
export class XFERListService {

    public AuthHeader = this._Func.AuthHeader();
    public AuthHeaderPost = this._Func.AuthHeaderPost();

    constructor(private _Func: Functions, private _API: API_Config, private http: Http) {

    }

    getXferList(param = '') {
        return this.http.get(this._API.API_WH_XFER + '/list-all-xfer' + param, {
            headers: this.AuthHeader
        }).map(res => res.json());
    }

    printXFerTicket(ids, fileName, that) {
        // var that = this;
        let api = this._API.API_WH_XFER_CREATE_TICKET;
        let param = {
                // "xfers":[{"wv_tf_id":id,"wv_tf_sts":'PD'}]
                "xfers":[]
        };
        ids.forEach(function (item, index) {
          param.xfers.push({"wv_tf_id":item,"wv_tf_sts":'PD'})  
        })
        let jsonParams = JSON.stringify(param);
        // let api = this._API.API_WH_XFER_CREATE_TICKET + '?wv_tf_id=' + id;
        try{
            let xhr = new XMLHttpRequest();
            xhr.open("POST", api , true);
            xhr.setRequestHeader("Authorization", 'Bearer ' + that._Func.getToken());
            xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
            xhr.responseType = 'blob';

            xhr.onreadystatechange = function () {
                if(xhr.readyState == 2) {
                    if(xhr.status == 200) {
                        xhr.responseType = "blob";
                    } else {
                        xhr.responseType = "text";
                    }
                }

                if(xhr.readyState === 4) {
                    if(xhr.status === 200) {
                        // var fileName = orderNum + '-' + customerName + '-packing slip';
                        var blob = new Blob([this.response], {type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'});
                        saveAs.saveAs(blob, fileName + '.pdf');
                        that.getXferList(that.Pagination['current_page']);
                    }else{
                        let errMsg = '';
                        if(xhr.response) {
                            try {
                                var res = JSON.parse(xhr.response);
                                errMsg = res.errors && res.errors.message ? res.errors.message : xhr.statusText;
                            }
                            catch(err){errMsg = xhr.statusText}
                        }
                        else {
                            errMsg = xhr.statusText;
                        }
                        if(!errMsg) {
                            errMsg = that._func.msg('VR100');
                        }
                        that.showMessage('danger', errMsg);
                    }
                }
            }

            // xhr.send(`{"xfers":[{"wv_tf_id":${id},"wv_tf_sts":'PD'}]}`);
            xhr.send(jsonParams);
        }catch (err){
            that.showMessage('danger', that._Func.msg('VR100'));
        }
    }
}