export class CRURoleMessages{
  public messages = {
  	"remove_none": "Please select one item to remove",
  	"remove_confirm": "Do you want to remove these items?",
    "connection": "Sorry, there's a problem with the connection.",
    "add_new_success": "Add new successfully",
    "no_permission": 'You must choose permission',
    "update_success": "Update successfully"
  }
}
