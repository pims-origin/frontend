/**
 * Created by admin on 6/8/2016.
 */
import {Component, Injectable} from '@angular/core';
import {Http, Response, JSONP_PROVIDERS} from '@angular/http';
import {API_Config, Functions} from '../common/core/load';

@Component({

})

@Injectable()
export class UserProfileServices {
  private AuthHeader = this._Func.AuthHeader();
  private AuthHeaderPost = this._Func.AuthHeaderPost();
  private AuthHeaderPostJson = this._Func.AuthHeaderPostJson();
  private apiUserDetail = this._API.API_Users + '/';
  //private apiUserEdit = this._API.API_Users + '/change-password';

  constructor(private _Func:Functions, private _API:API_Config, private http:Http) {
  }

  getProfileDetailByUserId() {
    let API = this.apiUserDetail + 'profile';
    return this.http.get(API, {headers: this.AuthHeader})
      .map((res:Response) => {
        if(res.status < 200 || res.status >= 300){
          throw new Error('This request has failed ' + res.status);
        }else{
          return {
            status: res.status,
            json: res.json().data
          }
        }
      });
  }

  updateProfileByUserId(dataToUpdate, objectData = new Map()) {

    let API ='';
    if(objectData.get("changePassword") === "changePassword"){

      API = this.apiUserDetail + 'change-password';
    }

    if(objectData.get("editProfile") === "editProfile"){
      API = this.apiUserDetail + 'profile';
      return this.http.put(API, dataToUpdate, {headers: this.AuthHeaderPostJson})
        .map((res:Response) => res.json());
    }
    //add more action for update profile
    return this.http.post(API, dataToUpdate, {headers: this.AuthHeaderPostJson})
      .map((res:Response) => res.json());
  }
}
