import {Component, Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {API_Config, Functions} from '../../../common/core/load';

@Injectable()
export class AllocateDetailServices {

  public AuthHeader = this._Func.AuthHeader();
  public AuthHeaderPost = this._Func.AuthHeaderPost();

  constructor(private _Func: Functions, private _API: API_Config, private http: Http) {

  }


  allCateOrder(id,data={},type:any){
    let param = type == 'XDK' ? 'xdoc-allocates/'+id : 'allocates/'+id;
    return this.http.put(this._API.API_ORDERS_CORE+'/'+param, JSON.stringify(data), { headers: this._Func.AuthHeaderPostJson() })
      .map((res: Response) => res.json().data);
  }


  getOrderDetail(orderId){
    return this.http.get(this._API.API_ORDERS+'/'+orderId,{ headers: this.AuthHeader })
      .map((res: Response) => res.json().data);
  }




}
