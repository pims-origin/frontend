import {Component} from '@angular/core';
import {RouteConfig, ROUTER_DIRECTIVES} from '@angular/router-deprecated';
import {GoodsReceiptStatusReport} from "./goods-receipt-status.component";
import {ASNStatusReport} from "./asn-status.component";



@Component ({
  selector: 'reports',
  directives: [ROUTER_DIRECTIVES],
  template: `<router-outlet></router-outlet>`
})

@RouteConfig([
  { path: '/asn-status', component: ASNStatusReport , name: 'ASN Status', useAsDefault: true },
  { path: '/goods-receipt-status', component: GoodsReceiptStatusReport  , name: 'Goods Receipt Status'},

])
export class ReceivingReportComponent {}
