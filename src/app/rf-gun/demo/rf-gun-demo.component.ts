import {Component,OnInit} from "@angular/core";
import { Router, RouterLink, ROUTER_DIRECTIVES } from '@angular/router-deprecated';
import { FORM_DIRECTIVES,FormBuilder} from '@angular/common';

@Component({
    selector: 'rf-gun-demo',
    directives: [FORM_DIRECTIVES],
    templateUrl: 'rf-gun-demo.component.html',
})
export class RFGUNDEMOComponent {}
