import {Component} from '@angular/core';
import {Router, RouterLink, RouteConfig, ROUTER_DIRECTIVES} from '@angular/router-deprecated';
import {CountryListComponent} from "./country-list/country-list.component";
import {CURCountryComponent} from "./cru-country/cru-country.component";

@Component ({
    selector: 'country',
    directives: [ROUTER_DIRECTIVES],
    template: `<router-outlet></router-outlet>`,
})

@RouteConfig([
    { path: '/', component:CountryListComponent  , name: 'Country List', useAsDefault: true},
    { path: '/new', component:CURCountryComponent  , name: 'Add Country' ,data:{action:'new'}},
    { path: '/:id/:edit', component:CURCountryComponent  , name: 'Edit Country' ,data:{action:'edit'} },
    { path: '/:id', component:CURCountryComponent  , name: 'View Country'  ,data:{action:'view'}}
])
export class CountryComponent {

}
