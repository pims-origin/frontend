import {Component} from '@angular/core';
import {Router, RouterLink, RouteConfig, ROUTER_DIRECTIVES} from '@angular/router-deprecated';
import {CycleCountDetailComponent} from "./cycle-count-detail/cycle-count-detail.component";
import {CycleCountListComponent} from "./cycle-count-list/cycle-count-list.component";
import {CreateCycleCountComponent} from "./create-cycle-count/create-cycle-count.component";
import {WMSBreadcrumb} from "../common/directives/wms-breadcrumb/wms-breadcrumb";
import {CycleCountReviewComponent} from "./cycle-count-adjust/cycle-count-adjust.component";
import {CreateCycleCountReasonComponent} from "./create-reason/create-cycle-count-reason.component";

@Component ({
    selector: 'cycle-count-management',
    directives: [
      ROUTER_DIRECTIVES,
      WMSBreadcrumb
    ],
    template: `<router-outlet></router-outlet>`,
})

@RouteConfig([
  {
    path: '/',
    component: CycleCountListComponent,
    name: 'CycleCount List',
    data:{
      action:'cycle-count'
    },
    useAsDefault: true
  },
  {
    path: '/:id',
    component: CycleCountDetailComponent,
    name: 'CycleCount Detail',
    data:{
      action:'view'
    }
  },
  {
    path: '/create',
    component: CreateCycleCountComponent,
    name: 'Create Cycle Count',
    data:{
      action:'create'
    }
  },
  {
    path: '/create-reason',
    component: CreateCycleCountReasonComponent,
    name: 'Create Cycle Count Reason',
    data:{
      action:'create'
    }
  },
  {
    path: '/:id/review',
    component: CycleCountReviewComponent,
    name: 'Cycle Count Review',
    data:{
      action:'review'
    }
  },
  {
    path: '/:id/edit',
    component: CycleCountDetailComponent,
    name: 'Update CycleCount Detail',
    data:{
      action:'update'
    }
  }
])

export class CycleCountManagementComponent {

}
