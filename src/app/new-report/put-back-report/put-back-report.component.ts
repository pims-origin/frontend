import {Component} from "@angular/core";
import {Router, ROUTER_DIRECTIVES} from '@angular/router-deprecated';
import {ControlGroup, FormBuilder, Control} from '@angular/common';
import {API_Config, UserService, Functions} from '../../common/core/load';
import {BoxPopupService} from "../../common/popup/box-popup.service";
import {PutBackReportService} from './put-back-report.service';
import {WMSPagination, AgGridExtent, WMSMessages, WMSBreadcrumb, ReportFileExporter} from '../../common/directives/directives';
import 'ag-grid-enterprise/main';

declare const $:any;

@Component({
  selector: 'put-back-report',
  providers: [PutBackReportService, BoxPopupService],
  directives: [ROUTER_DIRECTIVES, AgGridExtent, WMSMessages, WMSPagination, WMSBreadcrumb, ReportFileExporter],
  templateUrl: 'put-back-report.component.html',

})
export class PutBackReportComponent {
  private pagination:any;
  private perPage:number = 20;
  private messages:any;
  private searchForm:ControlGroup;
  private sortData = {fieldname: 'updated_at', sort: 'desc'};
  private showLoadingOverlay:boolean = false;
  private putBackReports:Array<any> = [];
  private headerURL:string = this.apiService.API_User_Metas + '/pre';
  private columnData:any = {
    cus_code: {
      title: 'Customer Code',
      width: 120,
      pin: true,
      ver_table: '050517'
    },
    cus_name: {
      title: 'Customer Name',
      width: 120,
      pin: true,
    },
    return_num: {
      title: 'PB Receipt Num',
      width: 120,
      pin: true,
      id: true
    },
    item_id: {
      title: 'Item ID',
      width: 50
    },
    sku: {
      title: 'SKU',
      width: 50
    },
    size: {
      title: 'Size',
      width: 50
    },
    color: {
      title: 'Color',
      width: 100
    },
    lot: {
      title: 'Batch',
      width: 50
    },
    cus_upc: {
      title: 'UPC/EAN',
      width: 50
    },
    return_qty: {
      title: 'Put Back Qty',
      width: 70
    },
    created_at: {
      title: 'Created Date',
      width: 100
    },
    created_by: {
      title: 'Created By',
      width: 100
    }
  };
  private months:Array<any> = [
    {name: 'January', value: 1},
    {name: 'February', value: 2},
    {name: 'March', value: 3},
    {name: 'April', value: 4},
    {name: 'May', value: 5},
    {name: 'June', value: 6},
    {name: 'July', value: 7},
    {name: 'August', value: 8},
    {name: 'September', value: 9},
    {name: 'October', value: 10},
    {name: 'November', value: 11},
    {name: 'December', value: 12}
  ];
  private years:Array<number> = [2017, 2018, 2019, 2020, 2021, 2022, 2023, 2024, 2025, 2026, 2027, 2028, 2029, 2030,
    2031, 2032, 2033, 2034, 2035, 2036, 2037, 2038, 2039, 2040, 2041, 2042, 2043, 2044, 2045, 2046, 2047, 2048, 2049,
    2050, 2051, 2052, 2053, 2054, 2055, 2056, 2057, 2058, 2059, 2060, 2061, 2062, 2063, 2064, 2065, 2026];

  // Check permission for user using this function page
  private canAccess:boolean = false;

  // Export csv file
  private exportAPIUrl:string = '';
  private fileName:string = 'Put-Back-Report.csv';

  constructor(private formBuilder:FormBuilder,
              private putBackReportService:PutBackReportService,
              private func:Functions,
              private userService:UserService,
              private apiService:API_Config,
              private router:Router) {
    this.buildSearchForm();
    this.checkPermission();
  }

  buildSearchForm() {
    const curDate = new Date()

    this.searchForm = this.formBuilder.group({
      month: [curDate.getMonth() + 1],
      year: [curDate.getFullYear()]
    });
  }

  private checkPermission() {
    this.showLoadingOverlay = true;
    this.userService.GetPermissionUser().subscribe(
        data => {
          this.showLoadingOverlay = false;
          this.canAccess = this.userService.RequestPermission(data, 'viewReport');
          if (this.canAccess) {
            this.getPutBackReports();
          } else {
            // redirect to deny page.
            this.router.parent.navigateByUrl('/deny');
          }
        },
        err => {
          this.messages = this.func.Messages('danger', this.func.parseErrorMessageFromServer(err));
          this.showLoadingOverlay = false;
        }
    );
  }

  private changePageSize($event:any) {
    this.perPage = $event.target.value;
    if (this.pagination) {
      if ((this.pagination['current_page'] - 1) * this.perPage >= this.pagination['total']) {
        this.pagination['current_page'] = 1;
      }
      this.getPutBackReports(this.pagination['current_page']);
    }

  }

  private getPutBackReports(pageNum:number = 1) {
    this.searchForm.value['page'] = pageNum;
    this.searchForm.value['limit'] = this.perPage;
    this.searchForm.value['sort[updated_at]'] = 'desc';

    this.exportAPIUrl = this.putBackReportService.getReportAPIUrl(`?` + $.param(this.searchForm.value));
    this.showLoadingOverlay = true;
    this.putBackReportService.getReports(`?` + $.param(this.searchForm.value))
        .subscribe(
            data => {
              this.showLoadingOverlay = false;
              this.putBackReports = data.data;

              // paging
              this.pagination = data['meta']['pagination'];
              this.pagination['numLinks'] = 3;
              this.pagination['tmpPageCount'] = this.func.Pagination(this.pagination);
            },
            err => {
              this.messages = this.func.Messages('danger', err.json().errors.message);
              this.showLoadingOverlay = false;
            },
            () => {
            }
        );
  }

  search() {
    this.getPutBackReports();
  }

  reset() {
    this.resetSearchForm();
    this.getPutBackReports();
  }

  resetSearchForm() {
    const curDate = new Date();

    (<Control>this.searchForm.controls['month']).updateValue(curDate.getMonth() + 1);
    (<Control>this.searchForm.controls['year']).updateValue(curDate.getFullYear());
  }

  private updateMessages(messages) {
    this.messages = messages;
  }
}
