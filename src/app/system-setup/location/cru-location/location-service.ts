import {Component, Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {API_Config, Functions} from '../../../common/core/load';

@Injectable()
export class LocationServices {

    public AuthHeader = this._Func.AuthHeader();
    public AuthHeaderPost = this._Func.AuthHeaderPost();

    constructor(private _Func: Functions, private _API: API_Config, private http: Http) {

    }

    createLocation(whsId,data){
        return this.http.post(this._API.API_Warehouse+'/'+whsId+'/locations', data, { headers: this._Func.AuthHeaderPostJson() })
            .map((res: Response) => res.json().data);
    }

    updateLocation(whsId,loc_id,data){
        return this.http.put(this._API.API_Warehouse+'/'+whsId+'/locations/'+loc_id, data, { headers: this._Func.AuthHeaderPostJson() })
            .map((res: Response) => res.json().data);
    }

    getLocationDetail(whsId,loc_id){
        return  this.http.get(this._API.API_Warehouse+'/'+whsId+'/locations'+'/'+loc_id, {headers: this.AuthHeader})
            .map(res => res.json().data);
    }

    getInventory($param) {
        return  this.http.get(this._API.API_GOODS_RECEIPT_MASTER+$param, {headers: this.AuthHeader})
            .map(res => res.json());
    }

    getLocationType(){
        return  this.http.get(this._API.API_Location_type, {headers: this.AuthHeader})
            .map(res => res.json().data);
    }


}
