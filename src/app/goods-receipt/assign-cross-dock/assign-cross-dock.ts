import {Component, DynamicComponentLoader, ElementRef, Injector} from '@angular/core';
import { Router, RouterLink, ROUTER_DIRECTIVES, RouteParams } from '@angular/router-deprecated';
import { FORM_DIRECTIVES,ControlGroup,FormBuilder,ControlArray, Control ,Validators} from '@angular/common';
import {Functions} from "../../common/core/functions";
import {AssignCrossDockServices} from "./service";
import {API_Config} from "../../common/core/API_Config";
import {WMSMessages, WMSBreadcrumb} from '../../common/directives/directives';
import {UserService} from "../../common/users/users.service";
import {WarningBoxPopup} from "../../common/popup/warning-box-popup";
import { ValidationService } from '../../common/core/validator';
import {BoxPopupService} from "../../common/popup/box-popup.service";
import {FormBuilderFunctions} from "../../common/core/formbuilder.functions";
declare var jQuery: any;
declare var saveAs: any;
declare var swal: any;
@Component({
    selector: 'assign-cross-dock',
    directives: [ROUTER_DIRECTIVES,FORM_DIRECTIVES, WMSMessages, WMSBreadcrumb],
    templateUrl: 'assign-cross-dock.html',
    providers: [AssignCrossDockServices]
})
export class AssignCrossDockComponent{

    AssignCrossDockForm:ControlGroup;
    items:ControlGroup[]=[];
    itemsArray: ControlArray= new ControlArray(this.items);

    private gr_id: any;
    private messages:any;
    private crossXdockDetail:Array<any>=[];
    private showLoadingOverlay = false;
    private XdocLocation:Array<any>=[];
    private _submitForm;

    // Construct
    constructor (
        private _router:Router,
        private _Services: AssignCrossDockServices,
        params: RouteParams,
        private _Valid:ValidationService,
        private fb: FormBuilder,
        private _fbdFunc:FormBuilderFunctions,
        private _Func: Functions,
        private _boxPopupService:BoxPopupService,
        private _user: UserService
    ) {
        // Check permission
        this.checkPermission();
        this.gr_id = params.get("id");
    }

    // Check Permission
    private role_assignCrossDock=false;
    private checkPermission() {

        this._user.GetPermissionUser().subscribe(
            data => {
                this.role_assignCrossDock=this._user.RequestPermission(data,'createGoodsReceipt');
                if(this.role_assignCrossDock){
                    this.getCrossXdock();
                }else{

                }
            },
            err => {

                this.showMessage('danger', this._Func.parseErrorMessageFromServer(err));

            }
        );
    }

    private getCrossXdock(){

        this._Services.getCrossXdock(this.gr_id).subscribe(
            data => {
                this.crossXdockDetail=data.data;
                this.formBuilder();
                let row=0;
                if(this.crossXdockDetail['xdoc_dtl'].length){
                    this.crossXdockDetail['xdoc_dtl'].forEach((item)=>{
                        this.initRowArray(item);
                        row++;
                    });
                    this.getXdocLocation();
                }else{
                    this._router.parent.navigateByUrl('/goods-receipt/' + this.gr_id + '/pallet-assignment');
                }

            },
            err => {
                this.parseError(err);
            }
        );

    }

    /*===========================
     * Builder Form Popup
     * ==========================*/
    private formBuilder(){
        this.AssignCrossDockForm =this.fb.group({
            data:this.itemsArray
        });
    }

    /*
     * itemsArray
     * */
    private initRowArray(data={}){

        this.itemsArray.push(
            new ControlGroup({
                'loc_id': new Control(data['loc_id'] ? data['loc_id'] : '',Validators.required),
                'gr_dtl_id':  new Control(data['gr_dtl_id']),
                'gr_dtl_act_ctn_ttl' :  new Control(data['gr_dtl_act_ctn_ttl']),
                'gr_dtl_dmg_ttl' :  new Control(data['gr_dtl_dmg_ttl']),
                'crs_doc' :  new Control(data['crs_doc']),
                'new_crs_doc' :  new Control(data['crs_doc'], Validators.compose([Validators.required, this._Valid.validateInt]))
            })
        );

        let row=this.itemsArray.controls.length-1;
        setTimeout(()=>{
            this.checkXdocValid(this.items[row],row);
            this.show_column_new_xdock();
        })

    }

    checkInputNumber(event)
    {
        this._Valid.isNumber(event,true);
    }

    /*============
     * disbale required picked_qty
     * ===========*/
    checkXdocValid(control:ControlGroup,row,input=false){
        // check qty
        let item=control.value;
        let itemVal=( parseInt(item['gr_dtl_act_ctn_ttl']) -  parseInt(item['gr_dtl_dmg_ttl'])) >=  parseInt(item['new_crs_doc']);
        if(!itemVal){
            this._fbdFunc.setErrorsControlItem(this.itemsArray,row,'new_crs_doc',{'invalid':true});
        }else{
            if(control.value['new_crs_doc']&&input){
                    let that = this;
                    let warningPopup = new WarningBoxPopup();
                    warningPopup.text = 'Do you make sure to adjust cross dock quantity less then original quantity?';
                    this._boxPopupService.showWarningPopup(warningPopup)
                        .then(function (dm) {
                            if (!dm) {
                                return;
                            } else {
                                that.updateCrossDockQty(control,row);
                            }
                        });

            }else{
                control['controls']['new_crs_doc']['hidden']=true;
            }
        }
    }

    private getXdocLocation(){

        this._Services.getXdocLocation(this.crossXdockDetail['cus_id']).subscribe(
            data => {
                this.XdocLocation=data.data;
            },
            err => {
                this.parseError(err);
            }
        );

    }

    private show_column_new_xdock(){

            let valid=0;
            this.itemsArray.controls.forEach((control)=>{
                if(control['controls']['new_crs_doc'].valid){
                    valid++;
                }
            });

            if(valid==this.itemsArray.controls.length){
                this.itemsArray['xdocQtyRow']=true;
            }else{
                this.itemsArray['xdocQtyRow']=false;
            }

    }


    private updateCrossDockQty(control:ControlGroup,row){

        this.showLoadingOverlay=true;
        let data=control.value;
        data['crs_doc']=control.value['new_crs_doc'];

        this._Services.updateCrossXdockQty(JSON.stringify(data)).subscribe(
            data => {

                this.showLoadingOverlay=false;
                control['success']=true;
                this.crossXdockDetail['xdoc_dtl'][row]['crs_doc']=control.value['new_crs_doc'];

                setTimeout(()=>{
                    control['success']=false;
                    this.show_column_new_xdock();
                    control['controls']['new_crs_doc']['hidden']=true;
                },1000)

            },
            err => {
                this.parseError(err);
                this.showLoadingOverlay=false;
                (<Control>control['controls']['new_crs_doc']).setErrors({'required':true});
                this.show_column_new_xdock();
            }
        );

    }

    private Save(){

        this._submitForm=true;
        if(this.AssignCrossDockForm.valid) {
            this.showLoadingOverlay = true;
            this._Services.assginCrossXdock(this.gr_id, JSON.stringify(this.AssignCrossDockForm.value['data'])).subscribe(
                data => {
                    this.showLoadingOverlay = false;
                    this.showMessage('success', this._Func.msg('VR109'));
                    setTimeout(()=>{
                        if(data.data['full_xdock']){
                            this._router.parent.navigateByUrl('/goods-receipt');
                        }else{
                            this._router.parent.navigateByUrl('/goods-receipt/' + this.gr_id + '/pallet-assignment');
                        }

                    }, 1100);
                },
                err => {
                    this.parseError(err);
                    this.showLoadingOverlay = false;
                }
            );
        }

    }
    
    private cancel(){

        var that = this;
        this._boxPopupService.showWarningPopup()
            .then(function (ok) {
                if(ok)
                    that._router.navigateByUrl('/goods-receipt');
            });
        
    }

    private showMessage(msgStatus, msg) {

        this.messages = {
            'status': msgStatus,
            'txt': msg
        };
        jQuery(window).scrollTop(0);
    }

    // Show error when server die or else
    private parseError (err) {

        this.messages = {'status' : 'danger', 'txt' : this._Func.parseErrorMessageFromServer(err)};
    }

}

