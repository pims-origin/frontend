
import {Component} from '@angular/core';
import {Http} from '@angular/http';
import {Functions} from '../../common/core/load';
import {JwtHelper} from 'angular2-jwt/angular2-jwt';

declare var jQuery: any;

@Component({
  selector: 'landing-page',
  templateUrl: 'landing-page.component.html'
})

export class LandingPageComponent {

    private token = '?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiIiLCJpYXQiOjE0NzQ0NTkzMjAsImV4cCI6MTQ3NDU0NTcyMCwibmFtZSI6IkNoYWNrbyBWZXJnaGVzZSIsInR5cGUiOiJXIiwianRpIjo4OTJ9.r0B6lWCnhxDycV9mEZRXtqEC_5Vs5fBi_faPNmNAMGw';
    private email = 'nganha@logisoft.com';
    private pass = 'Logi123!@#';
    private api = 'https://pims.dev.khachvip.vn/api/core/master-service/v1/login';
    private AuthHeaderNoTK = this._Func.AuthHeaderNoTK();
    private user_id;

    public links = {
        report: [
            'Search Style Locations',
            'Search Warehouse Locations',
            'Search Inventory',
            'Search Available Inventory',
            'Search Inventory History',
            'Search Customers',
            'Search Open Orders',
            'Search Orders',
            'Search Online Orders',
            'Search Wave Picks'
        ]
    };

    constructor(public http: Http,private _Func: Functions,public jwtHelper: JwtHelper){
        // this.loginFromCP();
        var decodedJwt = jwtHelper.decodeToken(localStorage.getItem('jwt'));
        this.user_id = decodedJwt['jti'];
        var that = this;
        jQuery('body').on('click.redirect', '.report-block a', function(e) {

            e.preventDefault();

            var link = jQuery(this),
                href = link.attr('href');

            if (href) {

                var newW = window.open('', '_blank');

                if (that.user_id == '338') {

                    var creds = "email="+that.email+"&password="+that.pass;

                    that.http.post(that.api, creds, {headers: that.AuthHeaderNoTK})
                        .subscribe(
                            data => {

                                let token = data.json().data.token;

                                newW.location.href = href + '?token=' + token;
                            },
                            err => {
                                newW.location.href = href + that.token;
                            }
                        )
                }
                else {
                  newW.location.href = href + that.token;
                }
            }
        });
    }

    //**************************************************************************

    private linkClick(pageCaption)
    {
        switch (pageCaption) {
            case 'Search Style Locations':
                window.open('/#/reports/style-locations', '_self');
                break;
            case 'Search Warehouse Locations':
                window.open('/#/reports/warehouse-locations', '_self');
                break;
            case 'Search Inventory':
                window.open('/#/reports/inventory', '_self');
                break;
            case 'Search Available Inventory':
                window.open('/#/reports/available-inventory', '_self');
                break;
            case 'Search Inventory History':
                window.open('/#/reports/inventory-history', '_self');
                break;
            case 'Search Customers':
                window.open('/#/reports/customers', '_self');
                break;
            case 'Search Open Orders':
                window.open('/#/reports/open-orders', '_self');
                break;
            case 'Search Orders':
                window.open('/#/reports/orders', '_self');
                break;
            case 'Search Online Orders':
                window.open('/#/reports/online-orders', '_self');
                break;
            case 'Search Wave Picks':
                window.open('/#/reports/wave-picks', '_self');
                break;
            default:
                break;
        }
    }

    //**************************************************************************

}
