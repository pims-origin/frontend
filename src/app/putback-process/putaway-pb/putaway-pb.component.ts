/**
 * Created by admin on 1/6/2017.
 */
import {Component} from '@angular/core';
import {Router, RouterLink, RouteConfig, ROUTER_DIRECTIVES} from '@angular/router-deprecated';
import {PutawayPbListComponent} from "./putaway-pb-list/putaway-pb-list.component";
import {PutawayPbUpdateComponent} from "./putaway-pb-update/putaway-pb-update.component";
import {PutAwayPbAssignmentListComponent} from "./putaway-pb-assignment/putaway-pb-assignment-list.component";
import {AssignPalletComponent} from "./putaway-pb-assignment/assign-pallet.component";
import {CruPaPbListComponent} from "./cru-pa-pb-list.component";
import {PbUpdateComponent} from "./putaway-pb-update/pb-update.component";

@Component ({
    selector: 'putback',
    directives: [ROUTER_DIRECTIVES],
    template: `<router-outlet></router-outlet>`,
})

@RouteConfig([
    { path: '/', component:PutawayPbListComponent  , name: 'PutAway Pb List', useAsDefault: true, data:{action:'pal'}},
    { path: '/:id/edit', component:PutawayPbUpdateComponent  , name: 'Update PutAway Pb'},
    { path: '/putaway-assignment', component:PutAwayPbAssignmentListComponent  , name: 'PutAway Pb Assignment List',data:{action:'psl'}},
    { path: '/putaway-update', component:PutawayPbUpdateComponent  , name: 'PutAway Update List',data:{action:'pul'}},
    { path: '/putaway-update/:id', component:PbUpdateComponent  , name: 'PutAway Update',data:{action:'pul'}},
    { path: '/assign-pallet/:id', component:AssignPalletComponent  , name: 'Assign Pallet'}
])
export class PutAwayPbComponent {

}
