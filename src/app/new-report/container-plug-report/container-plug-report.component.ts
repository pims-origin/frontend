import {Component,OnInit} from "@angular/core";
import { Router, RouterLink, ROUTER_DIRECTIVES } from '@angular/router-deprecated';
import { FORM_DIRECTIVES,ControlGroup,FormBuilder, Control,Validators} from '@angular/common';
import {API_Config, UserService, Functions} from '../../common/core/load';
import { Http } from '@angular/http';
import {BoxPopupService} from "../../common/popup/box-popup.service";
import {ContainerPlug_REPORT_Services} from "./container-plug-report-service";
import {WMSPagination,AgGridExtent,WMSMessages} from '../../common/directives/directives';
import 'ag-grid-enterprise/main';
declare var jQuery: any;

@Component({
  selector: 'container-plug-report',
  providers:[ContainerPlug_REPORT_Services],
  directives: [ROUTER_DIRECTIVES,AgGridExtent,WMSMessages,WMSPagination],
  templateUrl: 'container-plug-report.component.html',

})
export class ContainerPlug_REPORT_Component{

  private headerURL = this.apiService.API_User_Metas+'/ctp';
  private columnData = {
    cc_sts_name: {
      title:'Status',
      width:100,
      pin:true,
      ver_table:'66',
      sort:true
    },
    bay_name: {
      title:'Bay Name',
      width:70,
      pin:true,
      sort:true
    },
    ctnr_num: {
      title:'CTNR #',
      width:100,
      pin:true,
      sort:true
    },
    cus_name: {
      title:'Customer',
      width:170,
      pin:true,
      sort:true
    },
    start:{
      title:'Start',
      width:120,
      pin:true
    },
    end:{
      title:'End',
      width:120,
      pin:true
    },
    duration: {
      title:'Duration',
      width:170,
      sort:true
    },
    temp: {
      title:'Temp (C)',
      width:100,
      sort:true
    },
    created_at: {
      title:'Created at',
      width:120
    }
  };

  private Pagination;
  private perPage=20;
  private whs_id;
  private messages;
  private Loading={};
  private searching={};
  searchForm:ControlGroup;
  private isSearch:boolean=false;
  private containerPlugList:Array<any>=[];
  private sortData={fieldname:'bay',sort:'desc'};
  private showLoadingOverlay:boolean=false;
  private Customers:Array<any>=[];
  private viewTime=1;
  private cus_id='';
  private searchParams;
  private listContainers = [];
  constructor(
    private containerPlugReportService:ContainerPlug_REPORT_Services,
    private _Func: Functions,
    private _boxPopupService: BoxPopupService,
    private  apiService:API_Config,
    private _user:UserService,
    private fb: FormBuilder,
    private _http: Http,
    private _router: Router) {

    this.checkPermission();
    this.whs_id = localStorage.getItem('whs_id');
  }

  private viewReport;
  private allowAccess:boolean=false;
  private checkPermission() {
    this.showLoadingOverlay = true;
    this._user.GetPermissionUser().subscribe(
      data => {
        this.viewReport = this._user.RequestPermission(data, 'viewReport');
        if(!this.viewReport) {
          this.redirectDeny();
        }
        else {
          this.allowAccess=true;
          this.getContainerPlugReportList();
          this.getCustomersByWH();
          this.getListCTN();
        }
      },
      err => {
        this.messages=this._Func.Messages('danger', this._Func.parseErrorMessageFromServer(err));
        this.showLoadingOverlay = false;
      }
    );
  }


  private getCustomersByWH () {
    let params='?whs_id=' + this.whs_id + "&limit=10000";
    this.containerPlugReportService.getCustomersByWH(params)
      .subscribe(
        data => {
          this.Customers = data.data;
        },
        err => {
        }
      );

  }

  private getListCTN(){
    this.containerPlugReportService.getListContainer()
      .subscribe(
        data => {
          this.listContainers = data.data;
        },
        err => {
        }
      );
  }
  redirectDeny(){
    this._router.parent.navigateByUrl('/deny');
  }


  private onPageSizeChanged($event)
  {
    this.perPage = $event.target.value;
    if(this.Pagination){
      if((this.Pagination['current_page'] - 1) * this.perPage >= this.Pagination['total']) {
        this.Pagination['current_page'] = 1;
      }
      this.getContainerPlugReportList(this.Pagination['current_page']);
    }

  }

  private getContainerPlugReportList(page=null){

    if(!page) page = 1;
    let params="?page="+page+"&limit="+this.perPage;
    params += "&sort["+this.sortData['fieldname']+"]="+this.sortData['sort'];
    if(this.searchParams) {
      params += this.searchParams;
    }
    this.showLoadingOverlay=true;
    this.containerPlugReportService.getListContainerPlug(params).subscribe(
      data => {
        this.showLoadingOverlay=false;
        this.containerPlugList = this._Func.formatData(data.data);

        this.Pagination=data['meta']['pagination'];
        this.Pagination['numLinks']=3;
        this.Pagination['tmpPageCount']=this._Func.Pagination(this.Pagination);
      },
      err => {
        this.messages=this._Func.Messages('danger', this._Func.parseErrorMessageFromServer(err));
        this.showLoadingOverlay=false;
      }
    );

  }

  private reset() {
      jQuery("#form-filter input, #form-filter select").each(function( index ) {
          jQuery(this).val("");
      });
      this.searchParams = '';
      this.getContainerPlugReportList(1);
  }

  private search() {
    this.searchParams = '';
    let params_arr = jQuery("#form-filter").serializeArray() ;
    for (let i in params_arr) {
        if (params_arr[i].value == "") continue;
        this.searchParams +="&" +  params_arr[i].name.trim() + "=" + encodeURIComponent(params_arr[i].value.trim());
    }
      this.getContainerPlugReportList(1);
  }
}
