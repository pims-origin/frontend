import {Component} from '@angular/core';
import {Router, RouterLink, RouteConfig, ROUTER_DIRECTIVES} from '@angular/router-deprecated';

import {EditCartonComponent} from "./edit-carton/edit-carton.component";

@Component ({
    selector: 'utilities',
    directives: [ROUTER_DIRECTIVES],
    template: `<router-outlet></router-outlet>`,
})

@RouteConfig([
    {path: '/edit-carton', component: EditCartonComponent, name: 'Edit Carton'}
])
export class UtilitiesComponent {

}