import {Component, Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {Functions, API_Config} from '../common/core/load';

@Injectable()

export class GoodsReceiptServices {

    private headerGet = this._func.AuthHeader();
    private headerPostJson = this._func.AuthHeaderPostJson();
    private urlAPIContainer = this._api.API_Container;
    private urlAPIItem = this._api.API_ItemMaster;
    private urlAPIMeasurements = this._api.API_Measurements;
    private urlAPICustomer = this._api.API_Customer;
    private urlAPIAsn = this._api.API_Asns;
    private urlAPIGoodsReceipt = this._api.API_Goods_Receipt;
    private urlAPIUoms = this._api.API_UOM;
    private urlAPICustomerMaster = this._api.API_CUSTOMER_MASTER;
    private urlAPIGRStatus = this._api.API_GR_Status;
    private urlAPIDamageTye = this._api.API_DAMAGE_TYPE;
    private urlAPIDamageCarton  = this._api.API_Damage_Carton;
    private urlEventTracking = this._api.API_GOODS_RECEIPT_MASTER+'/eventracking';

    constructor(
        private _func: Functions,
        private _api: API_Config,
        private _http: Http) {
    }

    // Print damage ctn
    public printDamageCarton ($params = '') {

        return this._http.post(this.urlAPIDamageCarton + "/print", $params , {headers: this.headerPostJson})
            .map(res => res.json())
    }

    // Save damage ctn
    public saveDamageCarton ($params = '') {

        return this._http.post(this.urlAPIDamageCarton, $params , {headers: this.headerPostJson})
        //.map(res => res.json())
    }

    // Get ctn next
    public getCartonNext ($params = '') {

        return this._http.get(this.urlAPIDamageCarton + $params , {headers: this.headerGet})
            .map(res => res.json())
    }

    // Get damage type
    public getDamageType ($params = '') {

        return this._http.get(this.urlAPIDamageTye + $params , {headers: this.headerGet})
            .map(res => res.json())
    }

    // get ASN by ID
    public getAsnByID ($id) {

        return this._http.get(this.urlAPIAsn  + '/'  + $id , {headers: this.headerGet})
            .map(res => res.json())

    }

    // get ASN by ID
    public getStatus ($params = '') {

        return this._http.get(this.urlAPIGRStatus  + $params , {headers: this.headerGet})
            .map(res => res.json())

    }

    // print putaway
    public printPutAway ($params = '') {

        return this._http.get(this.urlAPIGoodsReceipt  + $params , {headers: this.headerGet})
            .map(res => res.json())

    }

    // get damage carton
    public getDamageCarton ($params) {

        return this._http.get(this.urlAPIDamageCarton  + $params , {headers: this.headerGet})
            .map(res => res.json())

    }

    // savePalletAssignment
    public savePalletAssignment ($gr_id , $params = '') {

        return this._http.post(this.urlAPIGoodsReceipt + '/' + $gr_id + "/pallets", $params , {headers: this.headerPostJson})
            .map(res => res.json())


    }

    // Get customers by WH
    public getCustomersByWH ($params) {

        return  this._http.get(this.urlAPICustomerMaster + '/customers-user' + $params + '&sort[cus_name]=asc' , {headers: this.headerGet})
            .map(res => res.json());

    }

    // Get customers by WH
    public getCustomersByUserWH ($params) {

        return  this._http.get(this.urlAPICustomerMaster + '/customers' + $params + '&sort[cus_name]=asc' , {headers: this.headerGet})
            .map(res => res.json());

    }

    public getEventTrackingByAsn($params) {

        return this._http.get(this.urlEventTracking + $params,{ headers: this.headerGet })
            .map(res => res.json());

    }

    // get Goods Receipt by ID
    public getGoodsReceiptById ($id) {

        return this._http.get(this.urlAPIGoodsReceipt  + '/'  + $id , {headers: this.headerGet})
            .map(res => res.json())

    }

    // get Goods Receipt by ID
    public getGoodsReceiptByIdCtnID ($id, $ctn_id) {

        return this._http.get(this.urlAPIAsn + "/" + $id + '/containers/' + $ctn_id, {headers: this.headerGet})
            .map(res => res.json())

    }

    // get Goods Receipt by ID
    public updateGoodsReceipt ($id, $params) {

        return this._http.put(this.urlAPIGoodsReceipt  + '/'  + $id , $params,{headers: this.headerPostJson})
            .map(res => res.json())

    }

    // get Goods Receipt by ID
    public addNewGoodsReceipt ($params) {

        return this._http.post(this.urlAPIGoodsReceipt, $params,{headers: this.headerPostJson})
            .map(res => res.json())

    }


    // Save ASN
    public saveAsn (params) {

        return this._http.post(this.urlAPIAsn, params , {headers: this.headerPostJson})
            .map(res => res.json())

    }

    // Save Goods Receipt
    public saveGoodsReceipt(params) {

        return this._http.post(this.urlAPIAsn, params , {headers: this.headerPostJson})
            .map(res => res.json())

    }

    // Get customers
    public getCustomers ($params = '') {

        return  this._http.get(this.urlAPICustomer + $params, {headers: this.headerGet})
            .map(res => res.json());

    }

    // Get uoms
    public getUoms () {

        return  this._http.get(this.urlAPIUoms + '&sys_uom_type=item', {headers: this.headerGet})
            .map(res => res.json());

    }

    // Get measurements
    public getMeasurements () {

        return  this._http.get(this.urlAPIMeasurements, {headers: this.headerGet})
            .map(res => res.json());

    }

    // Get containers
    public getContainersByASN () {

        return this._http.get(this.urlAPIMeasurements, {headers: this.headerGet})
            .map(res => res.json());

    }

    // Autocomplete for search
    public getItemBySKU (sku) {

        return this._http.get(this.urlAPIItem +  '?sku=' + sku, {headers: this.headerGet})
            .map(res => res.json().data);


    }

    // Autocomplete for search by name
    public getContainerByName (ctnr_num) {

        return this._http.get(this.urlAPIContainer +  '?ctnr_num=' + ctnr_num, {headers: this.headerGet})
            .map(res => res.json().data);


    }

    // Get container
    public getContainer ($params = '') {

        return this._http.get(this.urlAPIContainer +  $params, {headers: this.headerGet})
            .map(res => res.json());

    }

    // Get list asn
    public getListAsn ($params = '') {

        return this._http.get(this.urlAPIAsn +  $params, {headers: this.headerGet})
            .map(res => res.json());

    }

    // Get list goods receipt
    public getListGoodsReceipt ($params = '') {

        return this._http.get(this.urlAPIGoodsReceipt +  $params, {headers: this.headerGet})
            .map(res => res.json());

    }

    public getGoodsReceiptStatus() {
        return this._http.get(this._api.API_GOODS_RECEIPT + '/gr-statuses?limit=100&sort[asn_sts_name]=asc', {headers: this.headerGet})
            .map(res => res.json());
    }

    public completeGR($whsId, $grHdrId, $params = '', $paramsPost = '') {
        return this._http.post(this._api.API_GOODS_RECEIPT + '/whs/'+ $whsId +'/good-receipt-complete/'+ $grHdrId + $params, $paramsPost, {headers: this.headerPostJson})
            .map(res => res.json());
    }

    public getGoodsReceiptType() {
        return this._http.get(this._api.API_GOODS_RECEIPT + '/goods-receipt/getGrType', {headers: this.headerGet})
            .map(res => res.json());
    }

    /**
     * Approve goods receipt
     *
     * @param data
     */
    approveGoodsReceipt(data: any) {
        return this._http.post(`${this._api.API_GOODS_RECEIPT}/goods-receipts/approve`, data, { headers: this.headerPostJson })
            .map(res => res.json());
    }

    /**
     * Get list of ASN Type
     *
     * @param data
     */
    getASNTypeList() {
        return this._http.get(`${this._api.API_GOODS_RECEIPT}/asn-types`, { headers: this.headerGet })
            .map(res => res.json());
    }
}
