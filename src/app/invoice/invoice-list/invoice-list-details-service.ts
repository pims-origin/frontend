
import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {Functions, API_Config} from '../../common/core/load';

@Injectable()

export class InvoiceListDetailsServices {

    private headerGet = this._func.AuthHeader();
    private headerPostJson = this._func.AuthHeaderPostJson();

    constructor(
        private _func: Functions,
        private _api: API_Config,
        private _http: Http)
    {
    }

    /*
    ****************************************************************************
    */

    public getTableData(params = '')
    {
        return this._http.get(this._api.API_INVOICE_LIST_DETAILS + params, {
            headers: this.headerGet
        }).map(res => res.json());
    }

    /*
    ****************************************************************************
    */

}
