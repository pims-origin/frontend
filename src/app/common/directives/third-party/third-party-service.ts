import {Component, Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {API_Config,Functions} from '../../../common/core/load';

@Injectable()
export class thirdPartyServices {

    public AuthHeader = this._Func.AuthHeader();
    public AuthHeaderPost = this._Func.AuthHeaderPost();

    constructor(private _Func: Functions,
                private _API: API_Config,
                private http: Http) {

    }

    getThirdParty(cusId, param = ''){
        return this.http.get(this._API.API_SHIPPING+'/cus/'+cusId+'/third-party'+param ,{ headers: this.AuthHeader })
            .map((res: Response) => res.json());
    }

    createThirdParty(cusId, data) {
        return this.http.post(this._API.API_SHIPPING+'/cus/'+cusId+'/third-party', data, { headers: this._Func.AuthHeaderPostJson() })
            .map((res: Response) => res.json().data);
    }


    updateThirdParty(cusId, id,data) {
        return this.http.put(this._API.API_SHIPPING+'/cus/'+cusId+'/third-party/' + id, data, { headers: this._Func.AuthHeaderPostJson() })
            .map((res: Response) => res.json().data);
    }

    getItems(whsId, cusId, thirdPartyId, param = ''){
        return this.http.get(this._API.API_SHIPPING+'/whs/'+whsId+'/cus/'+cusId+'/third-party/'+thirdPartyId+'/item'+param ,{ headers: this.AuthHeader })
            .map((res: Response) => res.json());
    }

    updateItemPrice(whsId, cusId, thirdPartyId, itemId,data) {
        return this.http.put(this._API.API_SHIPPING+'/whs/'+whsId+'/cus/'+cusId+'/third-party/'+thirdPartyId+'/item/'+itemId+'/price', data, { headers: this._Func.AuthHeaderPostJson() })
            .map((res: Response) => res.json().data);
    }

    getCountry(param) {
        return this.http.get(this._API.API_Country + param,{ headers: this.AuthHeader })
            .map((res: Response) => res.json().data);
    }

    State(idCountry,param) {
        return this.http.get(this._API.API_Country+'/'+idCountry+'/states'+param,{ headers: this.AuthHeader })
            .map((res: Response) => res.json().data);
    }

}
