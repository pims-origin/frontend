import {Component} from '@angular/core';
import {Router, RouterLink, RouteConfig, ROUTER_DIRECTIVES} from '@angular/router-deprecated';
import {BillOfLadingsListComponent} from "./bill-of-ladings-list/bill-of-ladings-list.component";
import {CruBillOfLadingsComponent} from "./cru-bill-of-ladings/cru-bill-of-ladings.component";


@Component ({
    selector: 'bill-of-ladings',
    directives: [ROUTER_DIRECTIVES],
    template: `<router-outlet></router-outlet>`,
})

@RouteConfig([
    { path: '/', component: BillOfLadingsListComponent , name: 'Order List', useAsDefault: true },
    { path: '/new', component: CruBillOfLadingsComponent , name: 'Create Bill Of Ladings', data:{action:'new'}},
    { path: '/:cusId/:id/new', component: CruBillOfLadingsComponent , name: 'Create Bill Of Ladings', data:{action:'new'}},
    { path: '/:id', component: CruBillOfLadingsComponent , name: 'View Bill Of Ladings', data:{action:'view'}},
    { path: '/:id/edit', component: CruBillOfLadingsComponent , name: 'Edit Bill Of Ladings', data:{action:'edit'}}
])
export class BillOfLadingsManagementComponent {

}
