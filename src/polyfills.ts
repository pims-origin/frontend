// Polyfills
// These modules are what's in angular 2 bundle polyfills so don't include them
// import 'es6-shim';
// import 'es6-promise';
// import 'reflect-metadata';

// CoreJS has all the polyfills you need

import 'core-js';
require('zone.js/dist/zone');
if (process.env.ENV === 'production') {
    // Production
} else {
    // Development
    Error['stackTraceLimit'] = Infinity;
    require('zone.js/dist/long-stack-trace-zone');
}
