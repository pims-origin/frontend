import {Component, OnInit, Input} from '@angular/core';
import { Http, Headers } from '@angular/http';
import {AgGridNg2} from 'ag-grid-ng2/main';
import {GridOptions} from 'ag-grid/main';
import 'ag-grid-enterprise/main';


@Component({
    selector: 'ag-grid',
    directives: [AgGridNg2],
    templateUrl: 'ag-grid.component.html',
})

export class AgGridComponent{

    private gridOptions: GridOptions;
    private showGrid: boolean;
    private rowData: any [];
    @Input() data: any[];  // Array object
    @Input() rowName: any []; // Array
    @Input() headers: any[]; // Array object
    private dataTable: any[];
    private columnDefs: any[];
    private numRow: any;

    constructor (
        private _http: Http
    ) {

        this.showGrid = true;
        this.gridOptions = <GridOptions>{};
        this.createRowData();
        this.createColumnDefs();

    }

    // Define header for ag-grid
    private defineHeader() {

        var header = [];
        for (var i = 0; i < this.headers.length; i ++ ) {

            var tmp = {
                headerName: this.headers[i].hasOwnProperty('headerName') && this.headers[i]['headerName'] != '' ? this.headers[i]['headerName']: '',
                suppressMovable: this.headers[i].hasOwnProperty('suppressMovable') &&this.headers[i]['suppressMovable'] ? this.headers[i]['suppressMovable']: false,
                field: this.headers[i].hasOwnProperty('field') && this.headers[i]['field'] != '' ? this.headers[i]['field']: '',
                width: this.headers[i].hasOwnProperty('width') && this.headers[i]['width'] != '' ? this.headers[i]['width']: 50,
                headerClass: this.headers[i].hasOwnProperty('headerClass') && this.headers[i]['headerClass'] != '' ? this.headers[i]['headerClass']: '',
                headerCellTemplate: this.headers[i].hasOwnProperty('headerCellTemplate') && this.headers[i]['headerCellTemplate'] ? function(param){
                    var eCell = document.createElement('div');
                    eCell.className = "ag-header-cell ag-header-cell-sorted-none " + this.headers[i]['headerCellTemplate']['class'];
                    var html = this.headers[i]['headerCellTemplate']['html'];
                    eCell.innerHTML = html;
                    return eCell;

                }: "",
                checkboxSelection: this.headers[i].hasOwnProperty('checkboxSelection') && this.headers[i]['checkboxSelection'] ? this.headers[i]['checkboxSelection']: false,
                suppressSorting: this.headers[i].hasOwnProperty('suppressSorting') && this.headers[i]['suppressSorting'] ? this.headers[i]['suppressSorting']: false,
                suppressMenu: this.headers[i].hasOwnProperty('suppressMenu') && this.headers[i]['suppressMenu'] ? this.headers[i]['suppressMenu']: false,
                suppressSizeToFit: this.headers[i].hasOwnProperty('suppressSizeToFit') && this.headers[i]['suppressSizeToFit'] ? this.headers[i]['suppressSizeToFit']: false,
                editable: this.headers[i].hasOwnProperty('editable') && this.headers[i]['editable'] ? this.headers[i]['editable']: false,
                cellClass: this.headers[i].hasOwnProperty('cellClass') && this.headers[i]['cellClass'] != '' ? this.headers[i]['cellClass']: '',
            };

            header.push(tmp);
        }

        return header;

    }

    // Create data for row in table ag-grid
    // Row data will show on ag-grid
    private createRowData() {

        var rowData: any[] = [];
        var tmp = {};

        // Check data array
        if (this.data.length > 0) {

            for (var i = 0; i < this.data.length; i++) {

                for (var j = 0; j < this.rowName.length; j++) {

                    tmp[this.rowName[j]] = this.data[i].this.rowName[j];
                }

            }

        } else {

            for (var i = 0; i < this.numRow; i++) {

                for (var j = 0; j < this.rowName.length; j++) {

                    tmp[this.rowName[j]] = "";
                }

            }
        }
        rowData.push((tmp))
        this.rowData = rowData;

    }

    // Create column for ag-grid
    private createColumnDefs() {

        for (var i in this.headers) {

                this.columnDefs.push({
                    headerName: this.headers[i]['headerName'],
                    suppressMovable: this.headers[i]['suppressMovable'],
                    field: this.headers[i]['field'],
                    width: this.headers[i]['width'],
                    headerCellTemplate: this.headers[i]['headerCellTemplate'],
                    checkboxSelection: this.headers[i]['checkboxSelection'],
                    suppressSorting: this.headers[i]['suppressSorting'],
                    suppressMenu: this.headers[i]['suppressMenu'],
                    suppressSizeToFit: this.headers[i]['suppressSizeToFit'],
                    editable: this.headers[i]['editable'],
                    cellClass: this.headers[i]['cellClass']
                })

        }

    }

    private onReady() {
    }

    private onCellClicked($event) {
    }

    private onCellValueChanged($event) {
    }

    private onCellDoubleClicked($event) {
    }

    private onCellContextMenu($event) {
    }

    private onCellFocused($event) {
    }

    private onRowSelected($event) {
    }

    private onSelectionChanged() {
    }

    private onBeforeFilterChanged() {
    }

    private onAfterFilterChanged() {
    }

    private onFilterModified() {
    }

    private onBeforeSortChanged($event) {

    }

    private onAfterSortChanged() {
    }

    private onVirtualRowRemoved($event) {
        // because this event gets fired LOTS of times, we don't print it to the
    }

    private onRowClicked($event) {

    }

}
