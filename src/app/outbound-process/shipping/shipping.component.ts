import {Component} from '@angular/core';
import {Router, RouterLink, RouteConfig, ROUTER_DIRECTIVES} from '@angular/router-deprecated';
import {ShippingListComponent} from "./shipping-list/shipping-list.component";
import {ShippingUpdateComponent} from "./shipping-update/shipping-update.component";
import {ShippingAssignPalletComponent} from "./shipping-assign-pallet/shipping-assign-pallet.component";

@Component ({
    selector: 'shipping-management',
    directives: [ROUTER_DIRECTIVES],
    template: `<router-outlet></router-outlet>`,
})

@RouteConfig([
    { path: '/', component: ShippingListComponent , name: 'Shipping List', data:{page:'shipping'}, useAsDefault: true },
    { path: '/:id', component: ShippingUpdateComponent , name: 'Shipping Detail', data:{action:'view'}},
    { path: '/:id/assign-pallet', component: ShippingAssignPalletComponent , name: 'Shipping Assign Pallet', data:{action:'assign-pallet'}},
])
export class ShippingManagementComponent {

}
