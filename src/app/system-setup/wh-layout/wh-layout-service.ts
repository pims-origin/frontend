import {Component, Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {API_Config, Functions} from '../../common/core/load';

@Injectable()
export class WarehouseLayoutServices {

  public AuthHeader = this._Func.AuthHeader();
  public AuthHeaderPost = this._Func.AuthHeaderPost();

  constructor(private _Func: Functions, private _API: API_Config, private http: Http) {

  }

  getWarehouseLayout(params: string = '') {
    return this.http.get(this._API.API_WAREHOUSE_URL + '/whs-layout' + params , { headers: this.AuthHeader })
      .map((res: Response) => res.json());
  }

  getAisleDetail(aisle) {
    return this.http.get(this._API.API_WAREHOUSE_URL+'/whs-layout/' + aisle,{ headers: this.AuthHeader })
      .map((res: Response) => res.json());
  }

  getLocationDetail(locID) {
    return this.http.get(this._API.API_WAREHOUSE_URL+'/whs-layout/' + locID + '/show',{ headers: this.AuthHeader })
      .map((res: Response) => res.json());
  }

  filterWarehouse(cusCode, param = '') {
    return this.http.get(this._API.API_WAREHOUSE_URL+'/whs-layout?code='+cusCode + param,{ headers: this.AuthHeader })
        .map((res: Response) => res.json());
  }

  filterZone(zoneCode, param = '') {
    return this.http.get(this._API.API_WAREHOUSE_URL+'/whs-layout?type=zone&code='+zoneCode + param, { headers: this.AuthHeader })
        .map((res: Response) => res.json());
  }
  
  public getLocations(params: string = '') {
    const whsId = localStorage.getItem('whs_id');
    return this.http.get(`${this._API.API_Warehouse}/${whsId}/locations${params}`,{ headers: this.AuthHeader })
        .map((res: Response) => res.json());
  }

  public getSKUs(params) {
    return this.http.get(`${this._API.API_INVENTORY_NEW_REPORTS}/auto-sku${params}`, {headers: this.AuthHeader})
        .map(res => res.json());

  }

  public getCustomers() {
    return this.http.get(`${this._API.API_WAREHOUSE_CORE}/whs-layout/customer`, {headers: this.AuthHeader})
        .map(res => res.json());

  }

  public getZones() {
    return this.http.get(`${this._API.API_WAREHOUSE_CORE}/whs-layout/zone`, {headers: this.AuthHeader})
        .map(res => res.json());

  }

  public getLocationTypes() {
    return this.http.get(this._API.API_Location_type, {headers: this.AuthHeader})
        .map(res => res.json());
}
}
