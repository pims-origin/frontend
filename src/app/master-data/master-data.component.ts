import {Component} from '@angular/core';
import {Router, RouterLink, RouteConfig, ROUTER_DIRECTIVES} from '@angular/router-deprecated';
import {ZoneTypeComponent} from "./zone-type/zone-type.component";
import {LocationTypeComponent} from "./location-type/location-type.component";
import {CountryComponent} from "./country/country.component";
import {StateComponent} from "./state/state.component";
import {UOMComponent} from "./uom/uom.component";
import {ChargeCodeComponent} from "./charge-code/charge-code.component";
import {ChargeTypeComponent} from "./charge-type/charge-type.component";
import {DamageTypeComponent} from "./damage-type/damage-type.component";
import {UserDepartmentComponent} from "./user-department/user-department.component";
import {DimensionComponent} from "./Dimension/dimension.component";
import {DeliveryService} from "./delivery-service/delivery-service";
import {CommodityComponent} from "./commodity/commodity";
@Component ({
    selector: 'master-data',
    directives: [ROUTER_DIRECTIVES],
    template: `<router-outlet></router-outlet>`
})

@RouteConfig([
    { path: '/zone-type/...', component: ZoneTypeComponent, name: 'Zone Type', useAsDefault: true },
    { path: '/location-type/...', component: LocationTypeComponent, name: 'Location Type' },
    { path: '/country/...', component: CountryComponent, name: 'Country'},
    { path: '/state/...', component: StateComponent, name: 'State'},
    { path: '/uom/...', component: UOMComponent, name: 'UOM'},
    { path: '/charge-code/...', component: ChargeCodeComponent, name: 'Charge Code'},
    { path: '/charge-type/...', component: ChargeTypeComponent, name: 'Charge Type'},
    { path: '/charge-codes/...', component: ChargeCodeComponent, name: 'Charge Code List'},
    { path: '/damage-type/...', component: DamageTypeComponent, name: 'Damage Type List'},
    { path: '/user-department/...', component: UserDepartmentComponent, name: 'User Department'},
    { path: '/dimension/...', component: DimensionComponent, name: 'Dimension'},
    { path: '/commodity/...', component: CommodityComponent, name: 'Commodity'},
    { path: '/delivery-service', component: DeliveryService, name: 'Add Delivery Service'},
])
export class MasterDataComponent {


}
