import {Component} from '@angular/core';
import {RouteConfig, ROUTER_DIRECTIVES} from '@angular/router-deprecated';
import {WarehouseManagementComponent} from "./warehouse/warehouse.component";
import {LocationManagementComponent} from "./location/location.component";
import {ZoneManagementComponent} from "./zone/zone.component";
import {ItemMasterComponent} from "./item-master/item-master.component";
import {CustomerComponent} from "./customer/customer";
import {WarehouseLayoutComponent} from "./wh-layout/wh-layout.component";

@Component ({
    selector: 'system-setup',
    directives: [ROUTER_DIRECTIVES],
    template: `<router-outlet></router-outlet>`
})

@RouteConfig([
    { path: '/warehouse/...', component: WarehouseManagementComponent, name: 'Warehouse Management', useAsDefault: true },
    { path: '/location/...', component:LocationManagementComponent , name: 'Location Management' },
    { path: '/zone/...', component:ZoneManagementComponent , name: 'Zone Management' },
    { path: '/item-master/...', component:ItemMasterComponent , name: 'Item Master' },
    { path: '/customer/...', component:CustomerComponent , name: 'Customer' },
    { path: '/warehouse-layout/...', component:WarehouseLayoutComponent , name: 'Warehouse Layout' }
])
export class SystemSetupComponent {


}
