
import {Component} from '@angular/core';
import {RouteParams} from '@angular/router-deprecated';
import {API_Config} from '../../../../common/core/API_Config';
import {BaseTable} from '../../../../common/core/classes_us/base-table';
import {Functions} from '../../../../common/core/functions';
import {TableFunctions} from '../../../../common/core/table-functions';
import {DateFunctions} from '../../../../common/core/functions/date-functions';
import {BaseService} from '../../../../common/core/classes_us/base-service';
import {TableStructure} from '../../../../common/directives/directives';

@Component ({
    selector: 'containers',
    directives: [
        TableStructure
    ],
    templateUrl: '../../../../common/directives/table-structure/table.component.html',
    providers: [
        TableFunctions,
        DateFunctions,
        BaseService
    ]
})

export class OrderProcessingDetailsComponent extends BaseTable
{
    public tableID = 'receiving';
    public headerURL = this._api.API_User_Metas + '/rep';
    public title = 'View Order Processing Details';

    public skipPageSize = true;
    public skipPagination = true;

    public headerDef = [
        {
            id: 'ver_table',
            value: 1 + Math.random() * 10000
        },
        {
            id: 'client',
            name: 'Client',
            width: 150,
            unsortable: true
        },
        {
            id: 'cus_name',
            name: 'Customer Name',
            width: 150,
            unsortable: true
        },
        {
            id: 'odr_num',
            name: 'Order Number',
            width: 150,
            unsortable: true
        },
        {
            id: 'cust_odr_num',
            name: 'Customer Order Number',
            width: 170,
            unsortable: true
        },
        {
            id: 'client_odr_num',
            name: 'Client Order Number',
            width: 170,
            unsortable: true
        },
        {
            id: 'cq',
            name: 'Carton Quantity',
            width: 170,
            unsortable: true
        },
        {
            id: 'pq',
            name: 'Pieces Quantity',
            width: 170,
            unsortable: true
        },
        {
            id: 'plq',
            name: 'Pallet Quantity',
            width: 170,
            unsortable: true
        },
        {
            id: 'dt',
            name: 'Date',
            width: 120
        },
        {
            id: 'odr_typ',
            name: 'Order Type',
            width: 120
        }
    ];

    public permissionPage = 'viewInvoice';

    //**************************************************************************

    constructor (
        private _routeParams: RouteParams,
        private _api: API_Config,
        private _func: Functions,
        private _tableFunc: TableFunctions,
        private _tableService: BaseService,
        private _dateFunctions: DateFunctions
    )
    {
        super();

        this.tableComponent = this;

        this.rowData = [];

        let url =  this.printURL = this._api.API_Invoice_Order_Processing_Details
                + '?odr_id=' + this._routeParams.get('odr_id');

        this._tableService.get(url)
            .subscribe(
                data => {

                    this.rowData = data;
                    this.dataExport = data;

                    this.Pagination = {
                        current_page: 1,
                        total: this.rowData.length
                    };

                },
                err => {},
                () => {
                    this.showLoadingOverlay = false;
                }
            );

    }

    //**************************************************************************

}
