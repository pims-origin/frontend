import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {API_Config, Functions} from '../../common/core/load';

@Injectable()
export class ShippingReportService {
    public AuthHeader = this._Func.AuthHeader();
    public AuthHeaderPost = this._Func.AuthHeaderPost();

    constructor(private _Func:Functions,
                private _API:API_Config,
                private http:Http) {
    }

    getShippingReports(whid, params) {
        return this.http.get(`${this._API.API_WAREHOUSE_URL}/warehouses/${whid}/report/shipping` + params, {headers: this.AuthHeader})
            .map(res => res.json());
    }

    getShippingReportAPIUrl(whid:string, params:string='') {
        return `${this._API.API_WAREHOUSE_URL}/warehouses/${whid}/report/shipping${params}&export=1`;
    }

    getCustomersByWH(params) {
        return this.http.get(`${this._API.API_SHIPPING_NEW_REPORTS}/customers/${params}`, {headers: this.AuthHeader})
            .map(res => res.json());
    }

    getOrders(params) {
        return this.http.get(`${this._API.API_SHIPPING_NEW_REPORTS}/auto-order-num/${params}`, {headers: this.AuthHeader})
            .map((res:Response) => res.json());
    }

    getSKUs(params) {
        return this.http.get(`${this._API.API_SHIPPING_NEW_REPORTS}/auto-sku/${params}`, {headers: this.AuthHeader})
            .map((res:Response) => res.json());
    }

    getOrderTypeList($params = '?for=shippingReport') {
        return this.http.get(this._API.API_ORDERS_CORE + '/order-types' + $params,{ headers: this.AuthHeader })
            .map((res: Response) => res.json());
    }
}
