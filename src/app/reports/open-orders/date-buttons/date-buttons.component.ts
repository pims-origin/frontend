
import {Component, Injectable, Input} from '@angular/core';
import {DateFunctions} from '../../../common/core/functions/date-functions';

@Component ({
    selector: 'date-buttons',
    directives: [],
    templateUrl: 'date-buttons.component.html',
    providers: [
        DateFunctions
    ]
})

@Injectable()

export class DateButtonsComponent
{
    @Input() parentComponent = <any> {};

    private controlSearch = '';

    private dateButtons = ['Whole Week'];

    /*
    ****************************************************************************
    */

    constructor (
        private _dateFunctions: DateFunctions
    )
    {
        this.setReportDates();
    }

    //**************************************************************************

    public onFilterButtonClick(filterDate)
    {
        if (! this.controlSearch) {
            this.controlSearch = this.parentComponent.get('controlSearch');
        }

        let controlSearch = this.controlSearch;

        controlSearch += filterDate == 'Whole Week' ? '' :
                '&cancel_by_dt=' + this.getFilterDate(filterDate);

        this.parentComponent.set('controlSearch', controlSearch);

        this.parentComponent._searchesService.search(this.parentComponent);
    }

    //**************************************************************************

    private getFilterDate(date)
    {
        return this._dateFunctions.getDateString(new Date(date), 'yyyy-mm-dd');
    }

    //**************************************************************************

    private setReportDates()
    {
        let date = new Date();

        date.setDate(date.getDate() - 1);

        for (let count=0; count<7; count++) {

            date.setDate(date.getDate() + 1);

            let dateString = this._dateFunctions.getDateString(date);

            this.dateButtons.push(dateString);
        }
    }

    //**************************************************************************

}
