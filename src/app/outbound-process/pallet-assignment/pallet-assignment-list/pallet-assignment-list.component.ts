import {Component,Input} from '@angular/core';
import {OrderListComponent} from '../../order/order-list/order-list.component';
@Component ({
  selector: 'pallet-assignment-list',
  directives: [OrderListComponent],
  templateUrl: 'pallet-assignment-list.component.html',
})

export class PalletAssignmentListComponent {

  constructor(){}
}
