
import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {Functions} from '../load';

declare var saveAs: any;

@Injectable()

export class BaseService {

    protected getHeader = {
        headers: this._func.AuthHeader()
    };

    protected postHeader = {
        headers: this._func.AuthHeaderPostJson()
    };

    constructor(
        protected _func: Functions,
        protected _http: Http
    )
    {

    }

    //**************************************************************************

    public get(url)
    {
        return this._http.get(url, this.getHeader).map(res => res.json());
    }

    //**************************************************************************

    public post(url, data=null)
    {
        return this._http.post(url, data, this.postHeader).map(res => res.json());
    }

    //**************************************************************************

    public print(url, filename, method='POST', cb=null)
    {
        var that = this,
            errMsg = this._func.msg('VR100');

        try {

            let xhr = new XMLHttpRequest(),
                token = that._func.getToken();

            xhr.open(method, url, true);
            xhr.setRequestHeader('Authorization', 'Bearer ' + token);

            xhr.responseType = 'blob';

            xhr.onreadystatechange = function () {

                if (xhr.readyState == 2) {
                    xhr.responseType = xhr.status == 200 ? 'blob' : 'text';
                }

                if (xhr.readyState === 4) {
                    if(xhr.status === 200) {
                        const fileType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
                        let blob = new Blob([this.response], {
                                type: fileType
                            });

                        saveAs.saveAs(blob, filename);
                        cb(null);
                    } else {
                        if (xhr.response) {
                            try {

                                let res = JSON.parse(xhr.response);

                                errMsg = res.errors && res.errors.message ?
                                    res.errors.message : xhr.statusText;

                            } catch (err){
                                errMsg = xhr.statusText
                            }
                        } else {
                            errMsg = xhr.statusText;
                        }

                        // that._tableFunc.showMessage(this, 'danger', errMsg);
                        if (cb) {
                            cb(errMsg);
                        }
                    }
                }
            }

            xhr.send();

        } catch (err){
            // that._tableFunc.showMessage(this, 'danger', errMsg);
            cb(err);
        }
    }

    //**************************************************************************
}
