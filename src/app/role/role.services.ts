import {Component, Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {API_Config, Functions} from '../common/core/load';

@Injectable()
export class RoleService {

    public AuthHeader = this._Func.AuthHeader();
    public AuthHeaderPost = this._Func.AuthHeaderPost();

    constructor(private _Func: Functions, private _API: API_Config, private http: Http) {
    }

    getRole(params) {
        return this.http.get(this._API.API_Roles + params + '&sort[code]=asc', {headers: this.AuthHeader})
            .map((res: Response) => res.json());
    }

    getPermission(){
        return this.http.get(this._API.API_MASTER_URL+ '/permissions/groups', {headers: this.AuthHeader})
            .map((res: Response) => res.json());
    }

    getPermissionsByRoleName(roleName) {
        return this.http.get(this._API.API_MASTER_URL+'/roles/'+roleName, { headers: this.AuthHeader })
            .map((res: Response) => res.json().data);
    }

    addRole(stringData:string = '') {
        return this.http.post(`${this._API.API_MASTER_URL}/roles`, stringData, {headers: this._Func.AuthHeaderPostJson()})
            .map((res:Response) => res.json().data);
    }

    updateRole(roleName:string = '', stringData:string = '') {
        return this.http.put(`${this._API.API_MASTER_URL}/roles/${roleName}`, stringData, {headers: this._Func.AuthHeaderPostJson()})
            .map((res:Response) => res.json().data);
    }

}
