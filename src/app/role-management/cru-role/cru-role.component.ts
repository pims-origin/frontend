import {Component, OnInit} from '@angular/core';
import { FORM_DIRECTIVES,ControlGroup,FormBuilder, Control ,Validators} from '@angular/common';
import {RouteParams,Router } from '@angular/router-deprecated';
import {UserService} from '../../common/core/load';
import { Http } from '@angular/http';
import { JwtHelper } from 'angular2-jwt/angular2-jwt';
import {GoBackComponent} from "../../common/component/goBack.component";
import {ValidationService } from '../../common/core/validator';
import {UppercaseToSpaceLowerPipe} from "../string.pipe";
import {Functions} from "../../common/core/functions";
import {CRURoleService} from '../cru-role/cru-role-service';
import {CRURoleMessages } from '../cru-role/cru-role-messages';
import {BoxPopupService} from "../../common/popup/box-popup.service";
import {WMSMessages} from "../../common/directives/messages/messages";
import {WMSBreadcrumb} from '../../common/directives/directives';
declare var jQuery: any;

@Component({
    selector: 'cru-role',
    pipes: [UppercaseToSpaceLowerPipe],
    providers: [CRURoleService,CRURoleMessages,FormBuilder,ValidationService, BoxPopupService],
    directives: [FORM_DIRECTIVES, GoBackComponent, WMSMessages, WMSBreadcrumb],
    templateUrl: 'cru-role.html',

})
export class CRURoleComponent{

    private messages;
    private timeoutHideMessage;
    private listMessage = {};
    private action;
    private actionText;
    private TitlePage='';
    private cruRoleID;
    private isView:boolean = false;
    private showError = false;

    private showLoadingOverlay = false;

    private roleName = '';
    private addedGroupPermissions = [];
    private addedPermissions = [];
    private allGroupPermissions: any = [];

    roleForm: ControlGroup;

    constructor(
      private _http: Http,
      private _user: UserService,
      private _func: Functions,      
      private params: RouteParams,
      private _cruRoleService: CRURoleService,
      private _cruRoleMessages: CRURoleMessages,
      private _boxPopupService: BoxPopupService,
      public jwtHelper: JwtHelper,
      private _Valid: ValidationService,
      private fb: FormBuilder,
      private _router: Router) {
    }

    ngOnInit() {
      this.listMessage = this._cruRoleMessages.messages;
      this.buildForm();
      // this.initResetModal();

      this.cruRoleID=this.params.get('id');
      if(this.cruRoleID) {
          this.showLoadingOverlay = true;
          this.action = this.params.get('edit') == 'edit' ? 'edit' : 'view';
          if(this.action == 'view') {
              this.isView = true;
              this.TitlePage='View Role';
          }
          else {
              this.actionText = "Update";
              this.TitlePage='Edit Role';
          }
      }
      else {
          this.action = 'new';
          this.actionText = "Save";
          this.TitlePage='Add Role';
      }
      this._user.GetPermissionUser().subscribe(
          data => {
            if((this.action == 'view' && !this._user.RequestPermission(data,'viewRole')) || (this.action == 'edit' && !this._user.RequestPermission(data,'editRole')) || (this.action == 'new' && !this._user.RequestPermission(data,'createRole'))) {
              this._router.parent.navigateByUrl('/deny');
            }
            else {
              this.getAllPermissionByLogedUser();
              if(this.cruRoleID) {
                this.getRoleDetail();
              }
            }
          },
          err => {
            this.showMessage('danger', this._func.parseErrorMessageFromServer(err));
            this.showLoadingOverlay = false;
          }
      );
    }

    private buildForm() {
        this.roleForm =this.fb.group({
            code: [null, Validators.compose([this._Valid.customRequired, this._Valid.invalidCodeStr])],
            name: [null, Validators.compose([this._Valid.customRequired, this._Valid.invalidNameStr])],
            description: [null]
        });
    }

    private showMessage(msgStatus, msg) {
        this.messages = {
            'status': msgStatus,
            'txt': msg
        }
        jQuery(window).scrollTop(0);
    }

    private toggleCheckAll($event) {
      let checkbox = jQuery($event.target),
          status = checkbox.prop('checked'),
          listItem = checkbox.closest('table').find('tbody :checkbox');
      status ? listItem.prop('checked', true) : listItem.prop('checked', false);
    }

    private checkCheckAll($event) {
      var checkbox = jQuery($event.target),
          table = checkbox.closest('table'),
          checkboxAll = table.find('thead :checkbox');
      if(!checkbox.prop('checked')) {
        checkboxAll.prop('checked', false);
        if(checkbox.hasClass('cb-module')) {
          checkbox.closest('tr').find('.cb-permission').prop('checked', false);
        }
        if(checkbox.hasClass('cb-permission')) {
          checkbox.closest('tr').find('.cb-module').prop('checked', false);
        }
      }
      else {
        if(checkbox.hasClass('cb-module')) {
          checkbox.closest('tr').find('.cb-permission').prop('checked', true);
        }
        if(checkbox.hasClass('cb-permission') && !checkbox.closest('tr').find('.cb-permission:not(:checked)').length) {
          checkbox.closest('tr').find('.cb-module').prop('checked', true);
        }
        table.find('tbody :checkbox:not(:checked)').length ? checkboxAll.prop('checked', false) : checkboxAll.prop('checked', true);
      }
    }

    private initResetModal() {
      let that = this;
      jQuery('.modal').on('hidden.bs.modal', function(){
        let modal = jQuery(this);
        modal.find(':checkbox').prop('checked', false);
      });
    }

    private showCheckboxAddedPermission() {
      var that = this;
      if(this.allGroupPermissions && this.addedPermissions) {
        setTimeout(function() {
          var tableAllPermission = jQuery('#all-permission'),
              listAllPermission = tableAllPermission.find('.cb-permission'),
              allRows = tableAllPermission.find('tbody tr'),
              checkAll = true;
          listAllPermission.each(function() {
            var checkbox = jQuery(this);
            if(that.addedPermissions.indexOf(checkbox.val()) != -1) {
              checkbox.prop('checked', true);
            }
          })

          allRows.each(function() {
            var row = jQuery(this);
            if(!row.find('.cb-permission:not(:checked)').length) {
              row.find('.cb-module').prop('checked', true);
            }
            else {
              checkAll = false;
            }
          })

          if(checkAll) {
            tableAllPermission.find('thead :checkbox').prop('checked', true);
          }
        })
      }
    }

    // Get All permission
    private getAllPermissionByLogedUser() {
      this._cruRoleService.getAllPermissionByLogedUser().subscribe(
          data => {
            // console.log('get all permisson success', data);
            if(data.data.group_permissions) {
              this.allGroupPermissions = data.data.group_permissions;
            }
            this.showCheckboxAddedPermission();
          },
          err => {
            this.showMessage('danger', this._func.parseErrorMessageFromServer(err));
          }
      );
    }

    private addPermission() {
      let that = this;
      let listAllItem = jQuery('#all-permission tbody .cb-permission');
      var addedModuleAndPers = [],
          addedModule = [];
      that.addedPermissions = [];
      that.addedGroupPermissions = [];

      listAllItem.filter(':checked').each(function() {
        let permission = jQuery(this),
            moduleName = permission.closest('tr').find('.cb-module').val(),
            addedModuleIndex = addedModule.indexOf(moduleName);
        if(addedModuleIndex == -1) {
          addedModule.push(moduleName);
          addedModuleAndPers.push({
            'group': moduleName,
            'permissions': [permission.val()]
          })
        }      
        else {
          addedModuleAndPers[addedModuleIndex].permissions.push(permission.val())
        }
        that.addedPermissions.push(permission.val());
      });
      that.addedGroupPermissions = addedModuleAndPers;
      jQuery('#added-permission thead :checkbox').prop('checked', false);
    }

    private removeSpace($event) {
      var target = jQuery($event.target),
          newVal = target.val().replace(/ /g,'');
      target.val(newVal);
    }

    private submitForm(data :Object):void {
      this.showError = true;
      if(this.roleForm.valid) {
        if(!this.addedPermissions.length) {
          this.showMessage('danger', this.listMessage['no_permission']);
        }
        else {
          // trim data before submit
          for(let key in data) {
            let val = data[key];
            if(val && typeof val == 'string') {
              data[key] = jQuery.trim(val);
            }
          }
          data['permissions'] = this.addedPermissions;
          if(this.action == 'edit') {
            data['update_name'] = data['name'];
            data['name'] = this.cruRoleID;
          }
          let stringData = JSON.stringify(data);
          this.showLoadingOverlay = true;
          if (this.action == 'edit') {
            this.update(stringData);
          }
          else {
            this.addNew(stringData);
          }
        }
      }
    }

    private addNew(stringData) {
      let that = this;
      this._cruRoleService.addRole(stringData).subscribe(
          data => {
              this.showLoadingOverlay = false;
              this.showMessage('success', this.listMessage['add_new_success']);
              setTimeout(function(){
                  that._router.parent.navigateByUrl('/roles');
              }, 600);
          },
          err => {
              this.showLoadingOverlay = false;
              this.showMessage('danger', this._func.parseErrorMessageFromServer(err));
          }
      );
    }

    private getRoleDetail () {
      this._cruRoleService.getRoleDetail(this.cruRoleID).subscribe(
        data => {
            (<Control>this.roleForm.controls['code']).updateValue(data['code']);
            (<Control>this.roleForm.controls['name']).updateValue(data['name']);
            (<Control>this.roleForm.controls['description']).updateValue(data['description']);
            this.addedGroupPermissions = data.group_permissions;
            for(var i = 0, l = this.addedGroupPermissions.length; i < l; i++) {
              var module = this.addedGroupPermissions[i];
              if(!this.addedPermissions) {
                this.addedPermissions = module.permissions;
              }
              else {
                this.addedPermissions = this.addedPermissions.concat(module.permissions);
              }
            }
            this.showCheckboxAddedPermission();
            this.showLoadingOverlay = false;

        },
        err => {
            this.showLoadingOverlay = false;
            this.showMessage('danger', this._func.parseErrorMessageFromServer(err));
        }
      );
    }

    private update(stringData) {
      let that = this;
      this._cruRoleService.updateRole(this.cruRoleID, stringData).subscribe(
          data => {
              this.showLoadingOverlay = false;
              this.showMessage('success', this.listMessage['update_success']);
                setTimeout(function(){
                    that._router.parent.navigateByUrl('/roles');
                }, 600);
          },
          err => {
              this.showLoadingOverlay = false;
              this.showMessage('danger', this._func.parseErrorMessageFromServer(err));
          }
      );
    }

    private cancel() {
      var that = this;
      this._boxPopupService.showWarningPopup()
            .then(function (ok) {
              if(ok)
                that._router.navigateByUrl('/roles');

            });
    }
}
