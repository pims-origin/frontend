import {Component,OnInit} from "@angular/core";
import { Router, RouterLink, ROUTER_DIRECTIVES } from '@angular/router-deprecated';
import { FORM_DIRECTIVES,ControlGroup,FormBuilder, Control,Validators} from '@angular/common';
import {API_Config, UserService, Functions} from '../common/core/load';
import {BoxPopupService} from "../common/popup/box-popup.service";
import {PerformanceReportService} from './performance-report-service';
import {WMSPagination,AgGridExtent,WMSMessages} from '../common/directives/directives';
import 'ag-grid-enterprise/main';

@Component({
  selector: 'performance-report',
  providers:[PerformanceReportService,BoxPopupService],
  directives: [ROUTER_DIRECTIVES,AgGridExtent,WMSMessages,WMSPagination],
  templateUrl: 'performance-report.component.html',

})
export class PerformanceReportComponent{

  private Pagination;
  private perPage=20;
  private whs_id;
  private messages;
  private Loading={};
  private searching={};
  searchForm:ControlGroup;
  private isSearch:boolean=false;
  private listActivity:Array<any>=[];
  private sortData={fieldname:'created_at',sort:'desc'};
  private showLoadingOverlay:boolean=false;
  private viewTime=1;
  private userId;
  private userList:Array<any>=[];
  private loadDataStatus={};
  private listPerformance:Array<any>=[];

  constructor(
    private eventService:PerformanceReportService,
    private _Func: Functions,
    private _user:UserService,
    private _router: Router) {

    this.checkPermission();

  }

  // Check permission for user using this function page
  private viewReports;
  private allowAccess:boolean=false;
  private checkPermission() {
    this.showLoadingOverlay = true;
    this._user.GetPermissionUser().subscribe(
      data => {
        this.showLoadingOverlay = false;
        this.viewReports = this._user.RequestPermission(data, 'viewPerformanceActivity');
        if(this.viewReports){
          this.allowAccess=true;
          // add for show "No rows to show"
          //called after the constructor and called  after the first ngOnChanges()
          let userData=JSON.parse(localStorage.getItem('logedUser'));
         this.whs_id=localStorage.getItem('whs_id');
          this.userId = userData['user_id'];
          this.getUserList();
        }
        else{
          this.redirectDeny();
        }
      },
      err => {
        this.messages=this._Func.Messages('danger', this._Func.parseErrorMessageFromServer(err));
        this.showLoadingOverlay = false;
      }
    );
  }

  redirectDeny(){
    this._router.parent.navigateByUrl('/deny');
  }


  private onPageSizeChanged($event)
  {
    this.perPage = $event.target.value;
    if(this.Pagination){
      if((this.Pagination['current_page'] - 1) * this.perPage >= this.Pagination['total']) {
        this.Pagination['current_page'] = 1;
      }
      this.getListActivity(this.Pagination['current_page']);
    }

  }


  private getListActivity(page=null){

    let param=this.userId+"?day="+this.viewTime+"&page="+page+"&limit="+this.perPage;
    this.showLoadingOverlay=true;
    this.eventService.getListActivity(param).subscribe(
      data => {
        this.showLoadingOverlay=false;
        this.listActivity = data.data;
        this.loadDataStatus['list_activity']=true;
        //pagin function
        this.Pagination=data['meta']['pagination'];
        this.Pagination['numLinks']=3;
        this.Pagination['tmpPageCount']=this._Func.Pagination(this.Pagination);
      },
      err => {
        this.parseError(err);
        this.showLoadingOverlay=false;
      },
      () => {}
    );

  }

  private getListPerformance(page=null){

    let param=this.userId+"?day="+this.viewTime+"&warehouse="+this.whs_id;
    this.showLoadingOverlay=true;
    this.eventService.getListPerformance(param).subscribe(
      data => {
        this.showLoadingOverlay=false;
        this.listPerformance = data.data;
        this.loadDataStatus['listPerformance']=true;
      },
      err => {
        this.parseError(err);
        this.showLoadingOverlay=false;
      },
      () => {}
    );

  }

  getUserList()
  {
    let param='?limit=99999'
    this.eventService.getUserList(param).subscribe(
      data => {
        this.userList = data.data;
        this.getListActivity();
        this.getListPerformance();
      },
      err => {
        this.parseError(err);
      },
      () => {}
    );

  }

  /**
   * changeTimeView
   * **/
  changeTimeView(time)
  {
    this.viewTime=time;
    this.getListActivity();
    this.getListPerformance();
  }

  /*
  * changeCustomer
  * */
  changeCustomer(cusId)
  {
    this.userId=cusId;
    this.getListActivity();
    this.getListPerformance();
  }

  // Show error when server die or else
  private parseError (err) {
    err = err.json();
    this.messages = this._Func.Messages('danger', err.errors.message);
  }

}
