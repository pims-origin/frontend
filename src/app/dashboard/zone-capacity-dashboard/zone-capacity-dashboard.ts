import {Component, Input} from '@angular/core';
import {DecimalPipe} from '@angular/common';

import {WMSPagination} from '../../common/directives/directives';
import {DashboardServices} from '../dashboard.service';
import {Functions} from '../../common/core/load';
import {PaginatePipe, PaginationControlsCmp} from '../../common/directives/directives';
import {WMSNumberFormatter} from '../../common/pipes/wms-number-formatter';

declare let Chart:any;

@Component({
	selector: 'zone-capacity-dashboard',
	templateUrl: 'zone-capacity-dashboard.html',
	directives: [WMSPagination, PaginationControlsCmp],
	providers: [DashboardServices],
	pipes: [PaginatePipe, DecimalPipe, WMSNumberFormatter]
})

export class ZoneCapacityDashboard {
	private whsId:string = '';

	@Input('currentWHSId') set updateWHSId(value:any) {
		this.whsId = value;
		if (value) {
			this.getCustomers();
			this.getZoneCapacities();
		}
	}

	ngAfterViewInit()
	{
		// this.getCustomers();
		// this.getZoneCapacities();
	}

	constructor(private dashboardServices:DashboardServices, private funcs:Functions) {
	}

	private customers:Array<any> = [];

	private getCustomers() {
		this.dashboardServices.getCustomers(this.whsId).subscribe(
			data => {
				this.customers = data.data;
			},
			err => {
			}
		);
	}

	private changeCustomer(event) {
		let cusId = '';
		if (event.target.value) {
			cusId = event.target.value;
		}
		this.getZoneCapacities(cusId);
	}

	private zoneCapacities:Array<any> = [];
	private totalZoneLoc:any;

	private getZoneCapacities(cusId:string = '') {
		this.dashboardServices.getZoneCapacities(cusId).subscribe(
			data => {
				this.totalZoneLoc = data['meta'];
				this.zoneCapacities = data.data;
				this.drawZoneCapaciesChart();
			},
			err => {
			}
		);
	}

	private ZoneChart;

	drawZoneCapaciesChart() {
		const chartColors = ['#36c6d3', 'rgb(0, 128, 0)'];
		let dataLabels = [];
		let dataUsed = [];
		let dataUnused = [];
		for (let item of this.zoneCapacities) {
			dataLabels.push(item.zone_code);
			dataUsed.push(item.used);
			dataUnused.push(parseInt(item.total) - parseInt(item.used));
		}
		let barChartData = {
			labels: dataLabels,
			datasets: [{
				label: 'Used Location',
				backgroundColor: chartColors[0],
				data: dataUsed
			}, {
				label: 'Unused Location',
				backgroundColor: chartColors[1],
				data: dataUnused
			}]

		};

		let ctx = document.getElementById('zone-chart-wrapper');
		if (ctx == null) {
			return true;
		}
		if (this.ZoneChart) this.ZoneChart.destroy();
		this.ZoneChart = new Chart(ctx, {
			type: 'bar',
			data: barChartData,
			options: {
				title: {
					display: false,
					// text:""
				},
				tooltips: {
					callbacks: {
						title: (tooltipItem, data) => {
							let item = this.zoneCapacities[tooltipItem[0].index];
							return `${item.zone_name}: ${item.total}`;
						},
						label: (tooltipItem, data) => {
							return data.datasets[tooltipItem.datasetIndex].label + ": " + this.funcs.formatSWISNumber(tooltipItem.yLabel);
						}
					},
					mode: 'label'
				},
				responsive: true,
				scales: {
					xAxes: [{
						stacked: true,
					}],
					yAxes: [{
						stacked: true,
						ticks: {
							callback: (value, index, values) => {
								if (value.toString().indexOf(".") !== -1) {
									return this.funcs.formatSWISNumber(value.toFixed(1));
								}
								return this.funcs.formatSWISNumber(value);
							}
						}

					}]
				}
			}
		});
	}
}
