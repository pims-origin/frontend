import {Component} from '@angular/core';
import {Router, RouterLink, RouteConfig, ROUTER_DIRECTIVES} from '@angular/router-deprecated';
import {LogsListComponent} from "./logs-list/logs-list.component";

@Component ({
  selector: 'wap-management',
  directives: [ROUTER_DIRECTIVES],
  template: `<router-outlet></router-outlet>`
})

@RouteConfig([
  { path: '/', component: LogsListComponent, name: 'Logs List', useAsDefault: true },
  { path: '/logs', component: LogsListComponent, name: 'Logs List'},

])
export class WapManagementComponent {
}
