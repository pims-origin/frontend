import {Component, provide, Output, EventEmitter } from '@angular/core';
import {Http, Response, BrowserXhr} from '@angular/http';
import { Router, RouterLink, ROUTER_DIRECTIVES } from '@angular/router-deprecated';
import 'ag-grid-enterprise/main';
import 'rxjs/add/operator/map';
import {API_Config} from "../../../common/core/API_Config";
import {UserService} from "../../../common/users/users.service";
import {Functions} from "../../../common/core/functions";
declare var jQuery: any;
declare var saveAs: any;
declare var swal: any;
import {ChangeLocationStatusComponent} from "../location-status/change-location-status/change-location-status.component";
import {ViewLocationStatusComponent} from "../location-status/view-location-status/view-location-status.component";
import {WMSPagination, AdvanceTable,WMSBreadcrumb, WMSMessages} from '../../../common/directives/directives';

@Component ({
  selector: 'location-list',
  directives: [ChangeLocationStatusComponent, ViewLocationStatusComponent, AdvanceTable,WMSBreadcrumb,WMSMessages, WMSPagination],
  templateUrl: 'location-list.component.html',
})

export class LocationListComponent {
  private tableID = 'location-list';
  private headerURL = this._API_Config.API_User_Metas + '/loc';
  private headerDef = [{id: 'ver_table', value: 19},
    {id: 'ck', name: '', width: 25},
    {id: 'loc_sts_code_name', name: 'Status', width: 70},
    {id: 'cus_name', name: 'Customer', width: 100},
    {id: 'loc_code', name: 'Location Code', width: 100},
    // {id: 'loc_alternative_name', name: 'Alternative name', width: 120},
    {id: 'loc_type_name', name: 'Type', width: 70},
    {id: 'loc_width', name: 'Width', width: 60},
    {id: 'loc_length', name: 'Length', width: 60},
    {id: 'loc_height', name: 'Height', width: 60},
    {id: 'loc_available_capacity', name: 'Available Capacity', width: 60},
    {id: 'loc_weight_capacity', name: 'Weight Capacity', width: 40},
    {id: 'piece_ttl', name: 'Pieces Qty', width: 40, unsortable: true},
    {id: 'sku_ttl', name: '# of SKUs', width: 40, unsortable: true},
    {id: 'loc_zone_name', name: 'Zone', width: 40, unsortable: true},
    {id: 'rfid', name: 'RFID', width: 40, unsortable: true},
    {id: 'loc_sts_dtl_reason', name: 'Reason', width: 40, unsortable: true},
    {id: 'spc_hdl_name', name: 'Special Handling', width: 40, unsortable: true}];

  private pinCols = 5;
  private rowData: any[];
  private getSelectedRow;
  private refreshRowData = false;
  private objSort;
  private searchParams;

  private dataListLoca = [];
  private dataListLocaType = [];
  private dataListZoneType = [];
  private dataListZone = [];
  private viewSystem: boolean = false;
  private headerGet: any;
  private headerPost: any;
  private headerForm: any;
  private urlAPILocation: any;
  private urlAPILocationType: any;
  private urlAPIZoneType: any;
  private urlWarehouse: any;

  private Pagination;
  private totalCount = 0;
  private tmpPageCount;
  private pageCount=0;
  private ItemOnPage=0;
  private currentPage = 1;
  private perPage = 20;
  private numLinks = 3;

  private messages;
  private loc_whs_id: any;
  // private flagLoading = false;
  // private flagLoading2 = false;
  private importLoading = false;
  private urlAPILocationFirst: any;
  public hasBaseDropZoneOver:boolean = false;
  public hasAnotherDropZoneOver:boolean = false;
  private dataStausLocation: {};
  private dataItemLoca = {};
  private dataItemLocaView = {};
  private headerPostJson: any;
  filesToUpload: Array<File>;
  private messagesChangeStatus;
  private hasEditBtn:any;
  private hasCreateBtn:any;
  private hasChangeStatusBtn:any;
  private hasImport:any;
  private status = {
    'AC': 'Active',
    'IA': 'InActive',
    'LK': 'Locked',
    'RS': 'Reserved',

  };
  private selectedItemIndex;
  private showLoadingOverlay = false;
  private listMsg = {
    // 'ER1': 'Only file with extension .xlsx or .csv are supported!'
    'ER1': 'Only file with extension .xlsx is supported!'
  };

  arrSpecialHandling = [
    { key: 'RAC', name: 'Rack' },
    { key: 'SHE', name: 'Shelf' },
    { key: 'BIN', name: 'Bin' }
  ];

  constructor (
    private _http: Http,
    private _router:Router,
    private _user: UserService,
    private _func: Functions,
    private _API_Config: API_Config
  ) {
    // Check permission for user using this page
    this.checkPermission();
    this.loc_whs_id = localStorage.getItem('whs_id');
    this.headerGet = this._func.AuthHeader();
    this.headerPost = this._func.AuthHeaderPost();
    this.headerPostJson = this._func.AuthHeaderPostJson();
    this.urlWarehouse = this._API_Config.API_Warehouse;
    this.urlAPILocationFirst = this._API_Config.API_Location;
    this.urlAPILocation = this._API_Config.API_Warehouse + "/" + this.loc_whs_id + "/locations";
    this.urlAPILocationType = this._API_Config.API_Location_type;
    this.urlAPIZoneType = this._API_Config.API_Zone_Type;

    this.getLocaList(1);
    this.getLocaTypeList();
    this.getZoneList();
    // this.getZoneTypeList();
    this.filesToUpload = [];


  }

  // Check permission for user using this function page
  private checkPermission() {

    this._user.GetPermissionUser().subscribe(

      data => {

        this.hasImport = this._user.RequestPermission(data, 'importLocation');
        this.hasEditBtn = this._user.RequestPermission(data, 'editLocation');
        this.hasCreateBtn = this._user.RequestPermission(data, 'createLocation');
        this.hasChangeStatusBtn = this._user.RequestPermission(data, 'changeLocationStatus');

        var flagView = this._user.RequestPermission(data,'viewLocation');

        if(!flagView) {
          this._router.parent.navigateByUrl('/deny');
        }
      },
      err => {

        this.showMessage('danger', this._func.parseErrorMessageFromServer(err));

      }
    );
  }

  private showMessage(msgStatus, msg) {

    this.messages = {
      'status': msgStatus,
      'txt': msg
    }
    jQuery(window).scrollTop(0);
  }

  private requestPermission(permission, permission_req) {

    if (permission && permission.length) {

      for (var per of permission) {

        if (per.name == permission_req) {

          return true;
        }
      }

    }

    return false;

  }

  // Event for check all
  public delete() {

    if(jQuery(':checkbox:checked:not([id="ck-first"])').length < 1 ) {

      this.messages = this._func.Messages('danger', 'Please choose one item to edit');
      return false;
    }

    if(!confirm("Do you want delete action ?")) {

      return false;
    }

    var tmpDelete = [];
    jQuery(':checkbox:not([id="ck-first"])').each(function(i) {

      if (jQuery(this).context.checked) {

        tmpDelete.push(jQuery(this).val().trim());

      }
    });

    var params = 'ids=' + tmpDelete.join(",");
    this._http.delete(this.urlAPILocation, {body: params, headers: this.headerPost})
      .subscribe(
        data => {

          if(data.status == 204) {

            this.messages = this._func.Messages('success', 'Delete data success !!!');
            // var params = "?page=" + this.currentPage + "&limit=" + this.perPage;
            this.getLocaList(1);
          } else {

            this.parseError(data);
          }
        },
        err =>  {
          this.parseError(err);
        },
        () => {}
      );

  }

  // Reset form
  private reset() {

    jQuery("#form-filter input, #form-filter select").each(function( index ) {

      jQuery(this).val("");
    });
    this.searchParams = '';
    this.getLocaList(1);
  }

  // Search location
  private search() {
    this.searchParams = '';
    let params_arr = jQuery("#form-filter").serializeArray();
    for (var i in params_arr) {
      if (params_arr[i].value == "") continue;

      if (params_arr[i].name.trim() == 'loc_code') {
        var tmp_aray = params_arr[i].value.trim().split(",");

        for (var j in tmp_aray) {
          this.searchParams += "&" + params_arr[i].name.trim() + "[]=" + encodeURIComponent(tmp_aray[j]);
        }
      } else if (params_arr[i].name.trim() == 'rfid' || params_arr[i].name.trim() == 'spc_hdl_code') {
        this.searchParams += "&" + params_arr[i].name.trim() + "=" + encodeURIComponent(params_arr[i].value.trim());
      } else {
        this.searchParams += "&" + params_arr[i].name.trim() + "[]=" + encodeURIComponent(params_arr[i].value.trim());
      }
    }

    this.getLocaList(1);
  }

  // Get zone by warehouse
  private getZoneList () {

    this._http.get(this.urlWarehouse + "/" + this.loc_whs_id + "/zone?limit=10000&sort[zone_name]=asc" , {headers: this.headerGet})
      .map(res => res.json())
      .subscribe(
        data => {

          this.dataListZone = data.data;

        },
        err => {
          this.parseError(err);
        },
        () => {}
      );

  }

  // Get zone type
  private getZoneTypeList () {

    this._http.get(this.urlAPIZoneType , {headers: this.headerGet})
      .map(res => res.json())
      .subscribe(
        data => {

          this.dataListZoneType = data.data;

        },
        err => {
          this.parseError(err);
        },
        () => {}
      );

  }

  // Get location type
  private getLocaTypeList () {

    this._http.get(this.urlAPILocationType + "?limit=1000&sort[loc_type_name]=asc" , {headers: this.headerGet})
      .map(res => res.json())
      .subscribe(
        data => {

          this.dataListLocaType = data.data;

        },
        err => {
          this.parseError(err);
        },
        () => {}
      );

  }

  // Get location
  private getLocaList (page = null) {

    //params = params + "&sort[first_name]=asc";
    // console.log(params);
    // this.flagLoading2 = true;
    this.showLoadingOverlay = true;
    if(!page) page = 1;
    var params="?page="+page+"&limit="+this.perPage;
    if(this.objSort && this.objSort['sort_type'] != 'none') {
      if(this.objSort['sort_field'] == 'loc_sts_code_name') {
        this.objSort['sort_field'] = 'loc_sts_code';
      }
      params += '&sort['+ this.objSort['sort_field']+ ']='+ this.objSort['sort_type'];
    }
    if(this.searchParams) {
      params += this.searchParams;
    }
    this._http.get(this.urlWarehouse + "/" + this.loc_whs_id + "/locations" + params, {headers: this.headerGet})
      .map(res => res.json())
      .subscribe(
        data => {

          this.dataListLoca = data.data;
          this.createRowData(data);
          this.initPagination(data);
          this.showLoadingOverlay = false;
        },
        err => {
          this.parseError(err);
          this.showLoadingOverlay = false;
        },
        () => {
          // this.flagLoading = false;
          // this.flagLoading2 = false;
        }
      );

  }

  private filterList(pageNumber) {
    this.getLocaList(pageNumber);
  }

  private getPage(pageNumber) {
    let arr=new Array(pageNumber);
    return arr;
  }

  // Set params for pagination
  private initPagination(data){
    var meta = data.meta;
    this.Pagination=meta['pagination'];
    this.Pagination['numLinks']=3;
    this.Pagination['tmpPageCount']=this._func.Pagination(this.Pagination);

  }

  // Event when user filter form
  private onPageSizeChanged ($event) {
    this.perPage = $event.target.value;
    if((this.Pagination['current_page'] - 1) * this.perPage >= this.Pagination['total']) {
      this.currentPage = 1;
    }
    else {
      this.currentPage = this.Pagination['current_page'];
    }
    this.getLocaList(this.currentPage);
  }

  private createRowData(data) {
    var rowData: any[] = [];
    // Check data
    if (typeof data.data != 'undefined') {
      data = this._func.formatData(data.data);
      for (var i = 0; i < data.length; i++) {
        rowData.push(({
          loc_code: '<a href="#/system-setup/location/' + data[i].loc_id + '">'+ data[i].loc_code +'</a>',
          loc_code_raw: data[i].loc_code,
          // loc_alternative_name: data[i].loc_alternative_name,
          loc_type_name: data[i].loc_type_name,
          loc_width: data[i].loc_width,
          loc_length: data[i].loc_length,
          loc_height: data[i].loc_height,
          loc_available_capacity: data[i].loc_available_capacity,
          loc_weight_capacity: data[i].loc_weight_capacity,
          loc_min_count: data[i].loc_min_count,
          loc_zone_name: data[i].loc_zone_name,
          rfid: data[i].rfid,
          loc_sts_code_name: '<a>' + data[i].loc_sts_code_name + '</a>',
          loc_sts_dtl_reason: data[i].loc_sts_dtl_reason,
          loc_id: data[i].loc_id,
          piece_ttl: data[i].piece_ttl,
          sku_ttl: data[i].sku_ttl,
          loc_sts_code:data[i].loc_sts_code,
          loc_index: i,
          cus_name: data[i].cus_name,
          is_floor: data[i].is_floor,
          spc_hdl_name: this.getNameByKey(data[i]['spc_hdl_code'])
        }));
      }
    }
    else {
      this.dataListLoca = [];
      this.rowData = [];
    }

    this.rowData = rowData;
  }

  /**
   * Get name by key
   *
   * @param key
   */
  getNameByKey(key: string) {
    let item = this.arrSpecialHandling.find(item => item.key === key);
    return item ? item.name : '';
  }

  private onCellClicked($event) {
    let field = $event.colDef.field;
    // console.log('onCellClicked: ', $event, field);
    if(field == 'loc_sts_code_name') {
      var index = $event.data.loc_index;
      this.dataItemLocaView = {};
      this.dataItemLocaView = this.dataListLoca[index];
      jQuery('#viewLocationStatus').modal('show');
      setTimeout(function() {
        jQuery('body').trigger('viewLocationStatus');
      },200);
    }
  }

  // Navigation to Add page
  private add() {
    this._router.parent.navigateByUrl('/system-setup/location/new');
  }

  // Edit Role
  // Navigation to edit page
  private edit() {
    this.getSelectedRow = 'edit';
  }

  // Change status
  private changeStatus ($event) {
    this.getSelectedRow = 'changeStatus';
  }

  private printBarcode() {
    this.getSelectedRow = 'print-barcode';
  }

  // Save change status
  private saveChangeStatus(params) {
    var tmp = {};
    var that = this;
    jQuery.each( params, function( i, field ) {
      tmp[field.name] = field.value;
    });
    params = JSON.stringify(tmp);
    this._http.post(this.urlWarehouse + "/" +  this.loc_whs_id + "/locations/" + this.dataItemLoca['loc_id'] + "/change-status", params , {headers: this.headerPostJson})
      .map(res => res.json())
      .subscribe(
        data => {
          // console.log('update success', data, data.loc_sts_dtl_sts_code );
          for(var i = 0; i < this.dataListLoca.length; i++) {
            if(this.dataListLoca[i].loc_id == this.dataItemLoca['loc_id']) {
              this.dataListLoca[i].loc_sts_code = data.data.loc_sts_dtl_sts_code;
              this.dataListLoca[i].loc_sts_code_name = this.status[data.data.loc_sts_dtl_sts_code];

              this.dataListLoca[i].loc_sts_dtl_reason = data.data.loc_sts_dtl_reason;
              this.dataListLoca[i].loc_sts_dtl_remark = data.data.loc_sts_dtl_remark;

              this.rowData[this.selectedItemIndex].loc_sts_code_name = '<a>' + this.status[data.data.loc_sts_dtl_sts_code] + '</a>';
              this.rowData[this.selectedItemIndex].loc_sts_dtl_reason =  data.data.loc_sts_dtl_reason;
              this.refreshRowData = true;
              setTimeout(()=>{
                jQuery('#changeLocationStatus').modal('hide');
                this.refreshRowData = false;
              }, 400);
              break;
            }
          }

          this.messagesChangeStatus = this._func.Messages('success', this._func.msg('LCT001'));
          // jQuery('#changeLocationStatus').modal('hide');
          this.dataItemLocaView = {};
        },
        err => {
          this.messagesChangeStatus = this._func.Messages('danger', this._func.parseErrorMessageFromServer(err));
        },
        () => {
          this._router.parent.navigateByUrl('/system-setup/location');
        }
      );
  }

  afterGetSelectedRow($event) {
    var listSelectedItem = $event.data;
    switch ($event.action) {
      case 'edit':
        if (listSelectedItem.length > 1) {
          this.messages = this._func.Messages('danger', this._func.msg('CF004'));
        }
        else {
          if (listSelectedItem.length < 1) {
            this.messages = this._func.Messages('danger', this._func.msg('CF005'));
          }
          else if (listSelectedItem[0]['is_floor'] == 1) {
            this.messages = this._func.Messages('danger', this._func.msg('LC001'));
          } else {
            this._router.parent.navigateByUrl('/system-setup/location/' + listSelectedItem[0].loc_id + "/edit");
          }
        }
        break;
      case 'changeStatus':
        this.messages = null;
        this.messagesChangeStatus = null;
        if(listSelectedItem.length > 1 ) {
          this.messages = this._func.Messages('danger', this._func.msg('CF001'));
          jQuery('#changeLocationStatus').modal('hide');
        }
        else {
          if (listSelectedItem.length < 1) {
            this.messages = this._func.Messages('danger', this._func.msg('CF002'));
            jQuery('#changeLocationStatus').modal('hide');
          }
          else if (listSelectedItem[0]['is_floor'] == 1) {
            this.messages = this._func.Messages('danger', this._func.msg('LC002'));
          } else {
            // check stt loc
            if (listSelectedItem[0]['loc_sts_code']=='LK') {
              this.messages = this._func.Messages('danger', this._func.msg('LOC001'));
              jQuery('#changeLocationStatus').modal('hide');
              this.getSelectedRow = false;
              return false;
            }
            var index = listSelectedItem[0].loc_index;
            this.showLoadingOverlay = true;
            this.selectedItemIndex = index;
            this.messagesChangeStatus = null;
            this.dataItemLoca = {};
            this._http.get(this.urlWarehouse + "/" + this.loc_whs_id + "/locations/" + this.dataListLoca[index]['loc_id'] + "/change-status", {headers: this.headerGet})
              .map(res => res.json())
              .subscribe(
                data => {
                  var dataItemLoca = this.dataListLoca[index];
                  dataItemLoca['loc_sts_code'] = data.data['loc_sts_dtl_sts_code'];
                  dataItemLoca['loc_sts_dtl_to_date'] = data.data['loc_sts_dtl_to_date'];
                  dataItemLoca['loc_sts_dtl_reason'] = data.data['loc_sts_dtl_reason'];
                  dataItemLoca['loc_sts_dtl_remark'] = data.data['loc_sts_dtl_remark'];
                  dataItemLoca['loc_sts_reason_id'] = data.data['loc_sts_reason_id'];
                  this.dataItemLoca = dataItemLoca;
                  // console.log('success', this.dataItemLoca);
                  jQuery('#changeLocationStatus').modal('toggle');
                  this.showLoadingOverlay = false;
                },
                err => {
                  jQuery('#changeLocationStatus').modal('toggle');
                  this.dataItemLoca = this.dataListLoca[index];
                  this.dataItemLoca['loc_sts_dtl_to_date'] = null;
                  this.dataItemLoca['loc_sts_dtl_reason'] = null;
                  this.dataItemLoca['loc_sts_dtl_remark'] = null;
                  this.showLoadingOverlay = false;
                },
                () => {
                }
              );
          }
        }

        break;
      case 'delete':
        break;

      case 'print-barcode':
        this.messages = null;
        this.messagesChangeStatus = null;
          if (listSelectedItem.length < 1) {
            this.messages = this._func.Messages('danger', this._func.msg('CF010'));
          } else {
            this.callPrintBarcode(listSelectedItem);
          }

        break;
    }

    this.getSelectedRow = false;
  }

  private doSort(objSort) {
    this.objSort = objSort;
    this.getLocaList(this.Pagination.current_page);
  }

  // Upload file on server
  upload() {
    var that = this;
    this.messages = null;
    if(this.importLoading) return;

    this.makeFileRequest(this.urlWarehouse + "/" + this.loc_whs_id + "/locations/validate-import", [], this.filesToUpload).then((result) => {
      swal({
        text: "There will be " + result['data']['total_rows'] + " rows imported to system. Are you sure you want to process?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'OK',
        cancelButtonText: 'Cancel',
        confirmButtonClass: 'btn btn-primary',
        cancelButtonClass: 'btn btn-default',
        //buttonsStyling: false
      }).then(function(agree) {
        if (agree) {
          that.importLoading = true;
          that._http.get(that._API_Config.API_Location + "/process-import", {headers: that.headerGet})
            .map(res => res)
            .subscribe(
              data => {
                var message;
                if(typeof data == 'object' && data['_body']) {
                  message = JSON.parse(data['_body']).data.message;
                  // this.showMessage('danger', errors.message || errors.detail);
                }
                that.messages = that._func.Messages('success', message);
                that.importLoading = false;
                this.getLocaList(1);
              },
              err => {
                that.importLoading = false;
                that.messages = that._func.Messages('danger', that._func.parseErrorMessageFromServer(err));
              }
            );
        }
      });
      this.importLoading = false;
    }, (error) => {
      try {
        error = JSON.parse(error);
        var errMsg = error.errors?error.errors.message : '';
        if(error.data && error.data.code == '422') {
          this.messages = this._func.Messages('danger', error.data.message);
        }
        else {
          if (error.errors && error.errors.status_code == 400) {
            swal({
              text: "Import data has some errors, please download file to check!",
              type: 'warning',
              showCancelButton: true,
              confirmButtonText: 'OK',
              cancelButtonText: 'Cancel',
              confirmButtonClass: 'btn btn-primary',
              cancelButtonClass: 'btn btn-default',
            }).then(function(agree) {
              if (agree) {
                jQuery("#exampleInputFile").val("");

                that.dowloadFile();
              }
            });

          }
          else {
            if(errMsg) {
              this.messages = this._func.Messages('danger', errMsg);
            }
          }
        }
      }
      catch(err) {
      }
      this.importLoading = false;
    });

  }

  fileChangeEvent(fileInput: any){

    this.filesToUpload = <Array<File>> fileInput.target.files;
  }

  makeFileRequest(url: string, params: Array<string>, files: Array<File>) {

    return new Promise((resolve, reject) => {

      var formData: any = new FormData();

      var xhr = new XMLHttpRequest();

      for(var i = 0; i < files.length; i++) {

        // var fileExtension = files[i].name.substr(files[i].name.length - 5);
        const name = files[i].name.split('.');
        var fileExtension = name[name.length-1];
        // && fileExtension != "csv"
        if (fileExtension != "xlsx") {
          this.messages = this._func.Messages('danger', this.listMsg.ER1);
          return false;
        }

        formData.append("file", files[i], files[i].name);
      }

      this.importLoading = true;
      xhr.open("POST", url, true);
      xhr.setRequestHeader("Authorization", 'Bearer ' +this._func.getToken());
      xhr.onreadystatechange = function () {

        if (xhr.readyState == 4) {

          if (xhr.status == 200) {

            resolve(JSON.parse(xhr.response));

          } else {
            reject(xhr.response);
          }
        }
      };

      xhr.send(formData);
    });
  }

  private dowloadFile() {

    var xhrl = new XMLHttpRequest();

    xhrl.open("GET", this.urlAPILocationFirst + "/get-import-error", true);
    xhrl.setRequestHeader("Authorization", 'Bearer ' +this._func.getToken());
    xhrl.responseType = 'blob';

    xhrl.onreadystatechange = function () {

      // If we get an HTTP status OK (200), save the file using fileSaver
      if (xhrl.readyState === 4 && xhrl.status === 200) {

        var blob = new Blob([this.response], {type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'});
        saveAs.saveAs(blob, 'Report.xlsx');
      }
    };

    xhrl.send();

  }

  private doUpload() {

    var that = this;
    this.messages = null;
    if(this.importLoading) return;

    var url = this.urlWarehouse + "/" + this.loc_whs_id + "/locations/validate-import";
    var formData: any = new FormData();

    var xhr = new XMLHttpRequest();

    var files = this.filesToUpload;

    for(var i = 0; i < files.length; i++) {

      // var fileExtension = files[i].name.substr(files[i].name.length - 5);
      const name = files[i].name.split('.');
      var fileExtension = name[name.length-1];

      //&& fileExtension != "csv"
      if (fileExtension != "xlsx") {

        this.messages = this._func.Messages('danger', this.listMsg.ER1);
        return false;
      }

      formData.append("file", files[i], files[i].name);
    }

    this.showLoadingOverlay = true;
    xhr.open("POST", url, true);
    xhr.setRequestHeader("Authorization", 'Bearer ' +this._func.getToken());
    xhr.onreadystatechange = function () {

      if(xhr.readyState == 2) {
        if(xhr.status == 200) {
          xhr.responseType = "blob";
        } else {
          xhr.responseType = "text";
        }
      }
      // If we get an HTTP status OK (200), save the file using fileSaver
      if (xhr.readyState === 4) {

        if (xhr.status === 200) {

          that.messages = that._func.Messages('danger', 'Import file has some errors');

          var blob = new Blob([this.response], {type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'});
          saveAs.saveAs(blob, 'Report.xlsx');
          that.showLoadingOverlay = false;

        } else {

          //Load data
          let errMsg = '';

          if (xhr.status === 201) {

            that.getLocaList(1);
          }

          if(xhr.response) {

            try {
              var res = JSON.parse(xhr.response);
              errMsg = res.errors && res.errors.message ? res.errors.message : res.data.message;
            }
            catch(err){
              errMsg = xhr.statusText
            }
          }
          else {

            errMsg = xhr.statusText;
          }
          if(!errMsg) {
            errMsg = that._func.msg('VR100');
          }

          if (xhr.status === 201) {

            that.messages = that._func.Messages('success', errMsg);
          } else {

            that.messages = that._func.Messages('danger', errMsg);
          }
          that.showLoadingOverlay = false;
        }
      }

    };

    xhr.send(formData);

    setTimeout(function () {

      that.showLoadingOverlay = false;
    }, 30000);

  }


  // Get status of location
  private getStatusLoca(idLocation, $event) {

    this._http.get(this.urlWarehouse + "/" + this.loc_whs_id + "/locations/" + idLocation + "/change-status", {headers: this.headerGet})
      .map(res => res.json())
      .subscribe(
        data => {

          this.dataStausLocation = data.data;
          jQuery($event.target).attr ("data-target", "#viewLocationStatus");
        },
        err => {

          this.parseError(err);
        }
      );

  }

  private parseError (err) {

    this.messages = this._func.Messages('danger', this._func.parseErrorMessageFromServer(err));
  }

  private expandTable = false;
  private viewListFullScreen() {
    this.expandTable = true;
    setTimeout(() => {
      this.expandTable = false;
    }, 500);
  }

  private callPrintBarcode(items) {

    let locs = items.map(item => item['loc_code_raw']);
    locs = locs.length ? locs.join(',') : '';
    const that = this;
    const api = this._API_Config.API_WAREHOUSE_CORE +'/location/print-location-barcode?locs=' + locs;
    try{
      let xhr = new XMLHttpRequest();
      xhr.open("GET", api , true);
      xhr.setRequestHeader("Authorization", 'Bearer ' + that._func.getToken());
      xhr.responseType = 'blob';

      xhr.onreadystatechange = function () {
          if(xhr.readyState == 2) {
            if(xhr.status == 200) {
                xhr.responseType = "blob";
            } else {
                xhr.responseType = "text";
            }
          }

          if(xhr.readyState === 4) {
            if(xhr.status === 200) {
                var today = new Date();
                var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
                "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
                ];
                var day = today.getDate();
                var month = monthNames[today.getMonth()];
                var year = today.getFullYear().toString().substr(2,2);
                var fileName = 'LOCS_' + month +'_'+ day + '_' + year;
                var blob = new Blob([this.response], {type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'});
                saveAs.saveAs(blob, fileName + '.pdf');
            } else {
                let errMsg = '';
                if(xhr.response) {
                  try {
                      var res = JSON.parse(xhr.response);
                      errMsg = res.errors && res.errors.message ? res.errors.message : xhr.statusText;
                  } catch(err){errMsg = xhr.statusText}
                }
                else {
                    errMsg = xhr.statusText;
                }
                if(!errMsg) {
                    errMsg = that._func.msg('VR100');
                }
                that.showMessage('danger', errMsg);
              }
          }
      }
      xhr.send();
    } catch (err){
        that.showMessage('danger', that._func.msg('VR100'));
    }

  }
}
