/**
 * Created by admin on 1/4/2017.
 */

import {Component} from "@angular/core";
import {
    WMSBreadcrumb, WMSMessages, WMSPagination, AdvanceTable
} from '../../common/directives/directives';
import {API_Config} from "../../common/core/API_Config";
import {Functions} from "../../common/core/functions";
import {Http} from "@angular/http";
import {OrderBy} from "../../common/pipes/order-by.pipe";
import {ListAbstract} from "../../common/classes/listAbstract";
import {Router} from "@angular/router-deprecated";
import {PbServices} from "./pb.service";
declare var jQuery:any;

@Component ({
    selector: 'pb-order-list',
    directives: [AdvanceTable, WMSMessages, WMSBreadcrumb, WMSPagination],
    pipes: [OrderBy],
    providers: [PbServices],
    templateUrl: 'pb-order-list.component.html',
})

export class PbOrderListComponent extends ListAbstract{
    private createPutbackReceipt = false;
    private apiUserPermission = '';
    private dataListCustomer = [];
    private orderTypes = [];
    // Construct
    constructor(public _api:API_Config, private _pbServices:PbServices,public _http:Http,private _func: Functions,private _router:Router) {
        super(
            {
                headerURL: _api.API_User_Metas + '/pbr',
                urlList: _api.API_PUTBACK_ORDER
            },
            '#putback-order-rp-list',
            '#putback-order-search-list',
            [
                {id: 'ver_table', value: 7},
                {id: 'ck', name: '', width: 25},
                {id: 'odr_num', name: 'Order #', width: 80},
                {id: 'cus_name', name: 'Customer', width: 100},
                {id: 'ship_to', name: 'Shipping to', width: 100},
                {id: 'num_sku', name: '# of SKUs', width: 100},
                {id: 'return_qty', name: 'Put Back QTY', width: 100},
                {id: 'cancel_date', name: 'Cancelled Date', width: 100},
                {id: 'cancel_by', name: 'Cancelled By', width: 100},
            ], _http,_api);

        this.getCustomers();
        this.getOrderTypeList();
    }

    checkPermission(){
        let that = this;
        this._http.get(that._api.API_USER_Permission, { headers: that.AuthHeader() })
            .map(res => res.json().data).subscribe(
            data => {
                that.createPutbackReceipt = that.RequestPermission(data,'createPutbackReceipt');
                let view = this.RequestPermission(data,'viewPutbackReceipt');

                if(!view) {
                    that._router.parent.navigateByUrl('/deny');
                }
            },
            err => {
                that.showMessage('danger', that.parseErrorMessageFromServer(err));
            }
        )
    }

    setUpData(){

    }

    private getOrderTypeList () {
        this._pbServices.getOrderTypeList()
            .subscribe(
                data => {
                    this.orderTypes = data.data;
                }
            );
    }

    // Get customers
    private getCustomers() {

        var $params = '?limit=10000';
        this._pbServices.getCustomersByWH($params)
            .subscribe(
                data => {

                    this.dataListCustomer = data.data;
                },
                err => {

                    this.parseError(err);
                },
                () => {
                }
            );

    }

    afterGetSelectedRow($event){
        let listSelectedItem = $event.data;
        if($event.action == 'addPutbackReceipt') {
            if (listSelectedItem.length > 1) {
                this.messages = this._func.Messages('danger', this._func.msg('CF006'));
            }
            else {
                if (listSelectedItem.length < 1) {
                    this.messages = this._func.Messages('danger', this._func.msg('CF007'));
                }
                else {
                    let creds = {
                        odr_hdr_id: listSelectedItem[0].odr_id,
                        cus_id: listSelectedItem[0].cus_id
                    }
                    this.createPutBackReceipt(JSON.stringify(creds));
                }

            }
        }
    }

    addNew(){
        this.getSelectedRow = 'addPutbackReceipt';
    }

    edit(){
        
    }

    addMoreInfoToRow(){
        return ['odr_id','cus_id'];
    }

    createPutBackReceipt(creds){
        this._http.post(this._api.API_CREATE_PB_RECEIPT, creds,{headers: this.AuthHeaderPostJson()}).map( res => res.json().data).subscribe(
            data => {
                this.showMessage('success', this._func.msg('PBR001'));
                setTimeout(() => {
                    this._router.parent.navigateByUrl('/putback-process/putaway/assign-pallet/' + data.return_id);
                },600);
            },
            err => {
                this.showMessage('danger', this.parseErrorMessageFromServer(err));
            }
        );
    }
}