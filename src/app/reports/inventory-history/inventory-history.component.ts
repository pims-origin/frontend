
import {Component} from '@angular/core';
import {API_Config} from '../../common/core/API_Config';
import {BaseTable} from '../../common/core/classes_us/base-table';
import {Functions} from '../../common/core/functions';
import {TableFunctions} from '../../common/core/table-functions';
import {DateFunctions} from '../../common/core/functions/date-functions';
import {BaseService} from '../../common/core/classes_us/base-service';
import {CustomersService} from '../../master-data/customers/customers-service';
import {TableStructure} from '../../common/directives/directives';

@Component ({
    selector: 'orders',
    directives: [
        TableStructure
    ],
    templateUrl: '../../common/directives/table-structure/table.component.html',
    providers: [
        BaseService,
        TableFunctions,
        DateFunctions,
        CustomersService
    ]
})

export class InventoryHistoryComponent extends BaseTable
{
    public tableID = 'inventory-history';
    public url = this._api.API_INVENTORY_HISTORY_REPORTS;
    public headerUrlSuffix = 'rep';
    public title = 'Search Inventory History';
    public permissionPage = 'viewReport';

    public headerDef = [
        {
            id: 'ver_table',
            value: 1 + Math.random() * 10000
        },
        this.columnList.customer,
        this.columnList.container,
        this.columnList.orderNumber,
        this.columnList.logDate,
        this.columnList.totalCartons,
        this.columnList.totalPieces
    ];

    public unsortableColumns = [
        'totalCartons',
        'totalPieces'
    ];

    public objSort = {
        sort_field: 'log_date',
        sort_type: 'desc'
    };

    public searches = [
        this.searchList.customer,
        this.searchList.container,
        this.searchList.orderNumber,
        this.searchList.logDate
    ];

    /*
    ****************************************************************************
    */

    constructor (
        public _api: API_Config,
        public _func: Functions,
        public _tableFunc: TableFunctions,
        public _tableService: BaseService,
        public _customersService: CustomersService
    )
    {
        super();

        this._tableFunc.permissionCall(this);
    }

    //**************************************************************************

}