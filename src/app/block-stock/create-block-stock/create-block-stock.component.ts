import { Component,Input } from '@angular/core';
import { Router } from '@angular/router-deprecated';
import { API_Config, UserService, Functions } from '../../common/core/load';
import { FORM_DIRECTIVES,ControlGroup,FormBuilder, Control } from '@angular/common';
import { Http } from '@angular/http';
import { WMSBreadcrumb, PopupLocations } from '../../common/directives/directives';
import {BoxPopupService} from "../../common/popup/box-popup.service";
import {WarningBoxPopup} from "../../common/popup/warning-box-popup";
import {BlockStockServices} from "../block-stock-service";
import {AutoCompleteSKUProviders} from "./autocompleteSKU";
declare var jQuery: any;

@Component ({
  selector: 'cycle-count-list',
  providers: [BlockStockServices, FormBuilder, BoxPopupService, AutoCompleteSKUProviders],
  directives: [WMSBreadcrumb, PopupLocations],
  templateUrl: 'create-block-stock.component.html',
})

export class CreateBlockStockComponent {

  private dataListCustomer = [];
  private dataListReason = [];
  private whs_id;
  private blockStockList: Array<any> = [];
  private selectedAll = false;
  public listSelectedItem = [];
  public listStatusItem = [];
  private cycleCountStatus: any = [];
  private cycleCountTypes: any = [];
  private userList: any = [];
  private Pagination;
  private totalCount = 0;
  private tmpPageCount;
  private pageCount=0;
  private ItemOnPage=0;
  private currentPage = 1;
  private perPage = 20;
  private numLinks = 3;
  private messages;
  searchForm: ControlGroup;
  public loading = [];
  public showLoadingOverlay = false;
  private hasEditPermission = false;
  private hasCreatePermission = false;
  private hasViewCC = false;
  private hasDeleteCC = false;
  private eventCkEdit = true;
  private eventCkDelete = true;
  private eventCkReview = true;
  private currentUserID = {};
  private listNull = {};
  private s_loading = {
      'location_from': false,
      'location_to': false,
      'item': false,

  };
  private dataLocation = [];
  private dataItem = [];
  private sortQuery = '';
  private searchParams;
  public cycleCountUserList: any = [];
  private page_current = 1;
  private countClick = {
    block_hdr_id: 3,
    block_num: 3,
    block_type_name: 3,
    block_sts_name: 3,
    block_rsn_name: 3
  };
  private FieldsToSortFlag = {
    block_hdr_id: false,
    block_num: false,
    block_type_name: false,
    block_sts_name: false,
    block_rsn_name: false
  };
  private searchCriteria = {};
    private itemSelected = {};
    private locations: any[] = [];
    private searchBy: string = 'CS';

  constructor(private _http: Http,
              private _Func: Functions,
              private _user: UserService,
              private fb: FormBuilder,
              private _blockStockService: BlockStockServices,
              private _boxPopupService:BoxPopupService,
              private autoCompleteSKU:AutoCompleteSKUProviders,
              private _router: Router)
  {

    this.checkPermission();
    this.whs_id = localStorage.getItem('whs_id');

    this.getListReason();

    this._user.checkAuthentication().subscribe(data => {
      this.currentUserID = data.user_id;
    });

      this.getCustomersByWH('?limit=10000');
  }

  /*
   *********************************************************************************************************************
   */

  private checkPermission() {

    this.showLoadingOverlay = true;
    this._user.GetPermissionUser().subscribe(
        data => {

          this.hasCreatePermission = this._user.RequestPermission(data, 'createBlockStock');

          if (!this.hasCreatePermission) {

            this._router.parent.navigateByUrl('/deny');

          } else {

            this.getblockStock(1);
          }
        },
        err => {
            
          this.showMessage('danger', this._Func.parseErrorMessageFromServer(err));
          this.showLoadingOverlay = false;
        }
    );
  }


  /*
   *********************************************************************************************************************
   */

  private getblockStock(page=null) {

    this.page_current = page;
    let param = "?page=" + page + "&limit=" + this.perPage + '&whs_id=' + this.whs_id;


    if(this.sortQuery != '') {
      param += '&'+this.sortQuery;
    }

    this.loading['getCycleCount'] = true;
    this.showLoadingOverlay = true;

    this._blockStockService.getBlockStock(param).subscribe(
        data =>{

          this.selectedAll=false;
          this.blockStockList = data.data;

          this.Pagination=data['meta']['pagination'];
          this.Pagination['numLinks']=3;
          this.Pagination['tmpPageCount']=this._Func.Pagination(this.Pagination);

          this.loading['getCycleCount']=false;
          this.showLoadingOverlay = false;
        },
        err => {

            this.showLoadingOverlay = false;
          this.parseError(err);

        },
        () => {

            this.showLoadingOverlay = false;
        }
    );
  }

  /*
   *********************************************************************************************************************
   */

  private showMessage(msgStatus, msg) {
    this.messages = {
      'status': msgStatus,
      'txt': msg
    };
    jQuery(window).scrollTop(0);
  }


  /*
   *********************************************************************************************************************
   */

    private onPageSizeChanged($event) {
    this.perPage = $event.target.value;
    if((this.Pagination['current_page'] - 1) * this.perPage >= this.Pagination['total']) {
      this.currentPage = 1;
    }
    else {
      this.currentPage = this.Pagination['current_page'];
    }
    this.getblockStock(this.currentPage);
  }

  /*
   *********************************************************************************************************************
   */

  private filterList(num) {
    this.getblockStock(num);
  }

  /*
   *********************************************************************************************************************
   */

  private getPage(num) {
    let arr=new Array(num);
    return arr;
  }


  /*
   *********************************************************************************************************************
   */

  private getCycleCountType () {

    this._blockStockService.getCycleCountType()
        .subscribe(
            data => {
              this.cycleCountTypes = data;
            },
            err => {
            },
            () => {}
        );
  }

  /*
   *********************************************************************************************************************
   */

  private reset() {

    this.listNull = {};
    jQuery("#rcustomer").prop("checked", true);
    jQuery("#customer").prop("disabled", false);

    jQuery("#sku").prop("disabled", true);
    jQuery("#item").prop("disabled", true);
    jQuery("#location_from").prop("disabled", true);
    jQuery("#location_to").prop("disabled", true);

    jQuery("#form-filter input, #form-filter select").each(function( index ) {
      jQuery(this).val("");

    });

  }

  /*
   *********************************************************************************************************************
   */

  private sort(field) {
    if(this.countClick[field] == 3) {
      this.countClick[field] = 0;
    }

    this.countClick[field] ++;
    if(this.countClick[field] == 3) {
      this.sortQuery = 'sort[block_num]=asc';
    }
    else {
      this.FieldsToSortFlag[field] = !this.FieldsToSortFlag[field];

      this.sortQuery = 'sort['+field+']';
      if(this.FieldsToSortFlag[field]) {
        this.sortQuery += '=asc';
      } else {
        this.sortQuery += '=desc';
      }
    }

    if(field) {
      for (let sortField in this.FieldsToSortFlag ) {
        if(sortField != field) {
          this.FieldsToSortFlag[sortField] = false;
          this.countClick[sortField] = 3;
        }
      }
    }
    this.getblockStock(this.currentPage);
  }

  /* NEW */

  private getCustomersByWH($params) {

    this._blockStockService.getCustomersByWH($params)
        .subscribe(
            data => {
              this.dataListCustomer = data.data;
            },
            err => {

              this.parseError(err);
            },
            () => {
            }
        );
  }

  // Get reason
  private getListReason() {

    this._blockStockService.getListReason()
        .subscribe(
            data => {

              this.dataListReason = data.data;
            },
            err => {

              this.parseError(err);
            },
            () => {
            }
        );
  }


  /*
   ********************************************************************************************************************
   */

    private parseError(err) {

        this.messages = {'status': 'danger', 'txt': this._Func.parseErrorMessageFromServer(err)};
    }

    /*
     ********************************************************************************************************************
     */

    // Change select radio for cycle by
    private changeCycleBy($event) {
        let val = $event.target.value;
        this.searchBy = val;
        
        this.listNull['sku_required'] = false;
        this.listNull['customer_required'] = false;
        this.listNull['item_required'] = false;
        this.listNull['item_existed'] = false;
        this.listNull['locations_required'] = false

        if (['CS', 'SK'].indexOf(this.searchBy) === -1) {
            jQuery("#customer").val('');
        }
        jQuery("#sku").val('');
        jQuery("#item").val('');
        this.locations = [];

        jQuery("body").off("click.item");
    }

    // Filter form only show choose one
    private filterForm($target) {

        try {

            if ($target.value == "") {

                this.listNull[$target.id + "_required"] = true;

            } else {

                this.listNull[$target.id + "_required"] = false;
            }

        } catch (err) {

            return false;
        }
    }

    /*
     ********************************************************************************************************************
     */

    private validateLocation($target, name) {

        if ($target.value != "") {

            this.listNull[$target.id + "_required"] = false;
        }

        var target = this;
        jQuery("body").off("click." + name).on("click." + name, function (e) {


            var re = new RegExp("^" + name + "");
            target.listNull[name + "_required"] = false;

            if (!re.test(e.target.id) && jQuery("#" + name).val() == '' && jQuery("#rlocation").is(':checked')) {

                target.listNull[name + "_required"] = true;

            }

        });
    }

    /*
     ********************************************************************************************************************
     */

    private autoSearchLocation($event, name = '') {

        try {

            let n = this;
            // do Search
            let param = '?loc_code=' + encodeURIComponent($event.target.value) + '&limit=20';
            this.s_loading[name] = true;


            this._blockStockService.getLocationByWH(this.whs_id, param).subscribe(
                data => {

                    jQuery("#ul_" + name).show();

                    // disable loading icon
                    this.s_loading[name] = false;
                    this.dataLocation = data.data;

                },
                err => {

                    jQuery("#ul_" + name).hide();
                    this.s_loading[name] = false;
                },
                () => {

                    jQuery("#ul_" + name).show();

                    jQuery("body").off("click.a" + name).on("click.a" + name, function (e) {

                        var re = new RegExp("" + name + "");

                        if (!re.test(e.target.id) && jQuery("#rlocation").is(':checked')) {


                            jQuery("#ul_" + name).hide();

                        }

                    });
                }
            );

        } catch (err) {

            return false;
        }
    }

    /*
     ********************************************************************************************************************
     */

    private selectedItem($event, name, data:any) {

        this.listNull['item_existed'] = false;
        var id = $event.target.id.replace(name + "_", "");
        var code = jQuery($event.target).text().trim();
        this.listNull[name + '_required'] = false;
        jQuery("#ul_" + name).hide();
        jQuery("body").off("click.a" + name);

        if (name == 'item') {

           this.itemSelected = data;
            jQuery("#" + name).val(data['item_id']);

        } else {

            jQuery("#" + name).val(code);
        }

    }
    private ckOnlineExistItem() {

        var target = this;
        target.listNull['item_existed'] = false;
        if (target.dataItem != undefined && jQuery("#item").val() != '') {

            var tmp;
            for (var i = 0; i < target.dataItem.length; i++) {

                if (target.dataItem[i]['item_id'] == jQuery("#item").val() ) {

                    tmp = true;
                    break;
                }
            }

            if (!tmp) {

                target.listNull['item_existed'] = true;
            }

        } else {

            if (jQuery("#item").val() != '') {

                target.listNull['item_existed'] = true;

            }
        }
    }
    private ckItemExist($event) {

        this.listNull['item_existed'] = false;
        var target = this;
        jQuery("body").off("click.aitem-exist").on("click.aitem-exist", function (e) {

            if (e.target.id.indexOf("item") < 0 && jQuery("#rsitem").is(':checked') && jQuery("#item").val() == '') {

                target.listNull['item_required'] = true;
                return false;

            }

            var tmp = false;
            if (target.dataItem != undefined && jQuery("#item").val() != '') {

                for (var i = 0; i < target.dataItem.length; i++) {

                    if (target.dataItem[i]['item_id'] == jQuery("#item").val() ) {

                        tmp = true;
                        break;
                    }
                }

                if (!tmp) {

                    target.listNull['item_existed'] = true;
                }

            } else {

                if (jQuery("#item").val() != '') {

                    target.listNull['item_existed'] = true;

                }
            }

            jQuery("body").off("click.aitem-exist");

        });

    }

    /*
     ********************************************************************************************************************
     */

    private autoSearchItem($event, name = '') {

        try {

            var target = this;
            let n = this;
            // do Search
            let param = '?term=' + encodeURIComponent($event.target.value) + '&limit=20&whs_id=' + this.whs_id;
            this.s_loading[name] = true;


            this._blockStockService.getItemByWH(param).subscribe(
                data => {

                    jQuery("#ul_" + name).show();

                    // disable loading icon
                    this.s_loading[name] = false;
                    this.dataItem = data.data;

                },
                err => {

                    jQuery("#ul_" + name).hide();
                    this.s_loading[name] = false;
                },
                () => {

                    jQuery("#ul_" + name).show();

                    jQuery("body").off("click.a" + name).on("click.a" + name, function (e) {

                        var re = new RegExp("" + name + "");

                        if (!re.test(e.target.id) && jQuery("#rsitem").is(':checked')) {


                            jQuery("#ul_" + name).hide();

                        }

                    });
                    target.ckOnlineExistItem();
                }
            );

        } catch (err) {

            return false;
        }
    }

  // Save data
  private save() {

    try {

      this.messages = false;
      var tar_get = this;
      var submit = true;

      // Validate fields Top
      jQuery('#reason').each(function (i, object) {
        tar_get.filterForm(object);
      });

      switch(this.searchBy) {
        case 'CS':
          tar_get.filterForm(jQuery("#customer")[0]);
        break;
        case 'SK':
          tar_get.filterForm(jQuery("#sku")[0]);
        break;
        case 'IT':
          tar_get.filterForm(jQuery("#item")[0]);
        break;
        default:// LC
            this.listNull['locations_required'] = this.locations.length == 0;
      }

      jQuery.each(this.listNull, function (key, value) {

        if (value) {
          submit = false;
          return false;
        }
      });

      if (! submit) {
        return false;
      }

      var paramsJson = this.buildParams();
      var params = JSON.stringify(paramsJson);

      this._blockStockService.addNewBlockStock(params)
          .subscribe(

              data => {

                this.messages = this._Func.Messages('success', this._Func.msg('BS001'));
              },
              err => {

                this.parseError(err);
              },
              () => {

                this.getblockStock(1);
              }
          );

    } catch (err) {

    }

  }

  /*
   ********************************************************************************************************************
   */

  // build params
  private buildParams() {

    var params = {};

    params['whs_id'] = this.whs_id;
    params['block_rsn_id'] = jQuery("#reason").val();
    params['block_type'] = this.searchBy;

    switch(this.searchBy) {
      case 'CS':
        params['block_detail'] = jQuery("#customer").val();
      break;
      case 'SK':
        params['block_detail'] = jQuery("#sku").val();
        // params['block_detail'] = this.skuItem['itemIds'];
      break;
      case 'IT':
        params['block_detail'] = jQuery("#item").val();
        params['item_id'] = jQuery("#item").val();
      break;
      default:// LC
        let arLoc = this.locations.map(loc => loc.loc_id);
        params['block_detail'] = arLoc.join(',');
    }

    return params;
  }

  private addLocation($event: any[] = []){

    if (this.locations.length === 0) {
      this.locations = $event;
    } else {
      this.locations.push(...this.filteredDuplicatedItem(this.locations,$event));
    }

    if (this.locations.length) {
      this.listNull['locations'] = false;
    }

  }

  private filteredDuplicatedItem(ArrayDes:Array<any>=[],array_push:Array<any>=[]){

    let arr_temp:Array<any> = [];
    for (let new_item of array_push){
      let is_existed = 0;
      for (let old_item of ArrayDes){
        if(new_item['loc_id'] == old_item['loc_id'] ){
          is_existed++;
        }
      }
      if(is_existed==0){
        arr_temp.push(new_item);
      }
    }
    return arr_temp;

  }

  private autoSearchData = [];
  private skuItem={value:'', itemIds:''};
  autoCompleteSku(items, key, cus_id) {

      let searchkey = this._Func.trim(key);
      if (searchkey == '') {
          return true;
      }
      let param = `/?cus_id=${cus_id}&sku=${encodeURIComponent(this.autoCompleteSKU.extractLast(key))}&limit=20&groupBySku=1`;
      items['showLoading'] = true;
      let enCodeSearchQuery = this._Func.FixEncodeURI(param);
      if (items['subscribe']) {
          items['subscribe'].unsubscribe();
      }
      // do Search
      items['subscribe'] = this._blockStockService.autoSKU(enCodeSearchQuery).debounceTime(400).distinctUntilChanged().subscribe(
          data => {
              items['showLoading'] = false;
              this.autoSearchData = data.data;
          },
          err => {
              items['showLoading'] = false;
              this.parseError(err);
          },
          () => {
          }
      );
  }

  private selectedSKU(item){
      this.autoCompleteSKU.selectedSKU(this.skuItem,item);
  }


}
