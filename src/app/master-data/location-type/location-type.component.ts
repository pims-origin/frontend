import {Component} from '@angular/core';
import {Router, RouterLink, RouteConfig, ROUTER_DIRECTIVES} from '@angular/router-deprecated';
import {LocationTypeListComponent} from "./location-type-list/location-type-list.component";
import {AddLocationTypeComponent} from "./add-location-type/add-location-type.component";
import {EditLocationTypeComponent} from "./edit-location-type/edit-location-type.component";
import {ViewLocationTypeComponent} from "./view-location-type/view-location-type.component";


@Component ({
    selector: 'location-type',
    directives: [ROUTER_DIRECTIVES],
    template: `<router-outlet></router-outlet>`,
})

@RouteConfig([
    { path: '/', component:LocationTypeListComponent  , name: 'Location Type List', useAsDefault: true },
    { path: '/new', component:AddLocationTypeComponent  , name: 'New Location Type', data:{action:'new'} },
    { path: '/:id/edit', component: EditLocationTypeComponent  , name: 'Edit Location Type' , data:{action:'edit'} },
    { path: '/:id', component:ViewLocationTypeComponent  , name: 'View Location Type' , data:{action:'view'} }

])
export class LocationTypeComponent {


}
