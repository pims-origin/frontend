/*
 * @tien.nguyen
 * */
import {Component,Injectable} from '@angular/core';
import { ControlGroup, ControlArray, Control} from '@angular/common';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import {Functions,FormBuilderFunctions} from '../../../common/core/load';
declare var jQuery: any;
@Injectable()
export class PickingUpdateFunctions
{
    constructor(private _globalFun:Functions,
                private fbdFunc:FormBuilderFunctions ) {}


    public creatDataLocToCheck(controlArray:ControlArray,currentItem={}){


        let dataCheck={
            "item_id" : currentItem['itm_id'],
            "lot" : currentItem['lot'],
            "is_ecom" : 0,
            "lists":[]
        }
        
        for ( let i in controlArray.value){

            let item=controlArray.value[i];
            dataCheck['lists'].push({ 
                "loc_id": item['act_loc_id'] ? item['act_loc_id'] : item['loc_id'], 
                "picked_qty": item['picked_qty'], 
                "lpns" : item['lpns'] 
            });
            // set markAsTouched
            (<Control>controlArray.controls[i]['controls']['act_loc']).markAsTouched();

        }

        return dataCheck;

    }

    public copySuggestedLocationToActualLocation(controlArray:ControlArray){

        let data=controlArray.value;
       // this.startingProgress(controlArray);

        for ( let i in data){

            if(!data[i]['act_loc']){
                this.fbdFunc.setValueControlItem(controlArray,i,'act_loc',data[i]['loc_code']);
                this.fbdFunc.setValueControlItem(controlArray,i,'act_loc_id',data[i]['loc_id']);
            }
           // this.docaculatorProgress(controlArray,i,data.length);
        }



    }

    /*
    * This function will be handle data called form api
    * */
    public pareDataErrors(controlArray:ControlArray,data:Array<any>=[]){

        //this.startingProgress(controlArray);

        // reset error
        for ( let i in controlArray.controls){
            if(typeof controlArray.controls[i]['controls']['act_loc']['errorsMessages']!=="undefined"){
                controlArray.controls[i]['controls']['act_loc']['errorsMessages']="";
            }
        }

        // find and set errors
        for(let i in data){

            if(data[i]['error']!==""){
                let listIdxErr=this.getIndexLocId(controlArray.value,data[i]['loc_id']);
                for(let j in listIdxErr) {
                    controlArray.controls[listIdxErr[j]]['controls']['act_loc']['errorsMessages']=data[i]['error'];
                    this.fbdFunc.setErrorsControlItem(controlArray,listIdxErr[j],'act_loc',{'errors_from_api_check':true});
                }
            }

           // this.docaculatorProgress(controlArray,i,data.length);

        }

    }

    /*=============================
        Return index a key
    ==============================*/
    public  getIndexLocId(data,keyfindIndex) {

        var listIdxErr = [];
        for( let i in data){
            if(data[i]['act_loc_id']==keyfindIndex){
                // return i;
                listIdxErr.push(i);
            }
        }
        return listIdxErr;

    }

    public startingProgress(controlArray:ControlArray){
        controlArray['copyProgress']=1;
    }

    public docaculatorProgress(controlArray:ControlArray,doneIndex,lengthdata){

            controlArray['copyProgress'] = Math.ceil(((parseInt(doneIndex)+1)/lengthdata)*100);
            if(lengthdata==parseInt(doneIndex)+1){
                //controlArray['copyProgress']=0;
            }

    }

}
