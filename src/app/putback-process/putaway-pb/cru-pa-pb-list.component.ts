/**
 * Created by admin on 1/4/2017.
 */
import {Component,Injectable} from "@angular/core";
import {
    WMSBreadcrumb, WMSMessages, WMSPagination, AdvanceTable
} from '../../common/directives/directives';
import {Functions} from "../../common/core/functions";
import {API_Config} from "../../common/core/API_Config";
import {Http} from '@angular/http';
import {JwtHelper} from "angular2-jwt/angular2-jwt";
import {Router, RouterLink, RouteData, ROUTER_DIRECTIVES, RouteParams} from '@angular/router-deprecated';
import {UserService} from "../../common/users/users.service";
import {PaPbServices} from "./pa-pb.service";
declare var jQuery:any;
declare var jsPDF:any;
declare var saveAs:any;
@Component({
    selector: 'cru-pa-pb-list',
    directives: [AdvanceTable, WMSMessages, WMSBreadcrumb, WMSPagination],
    templateUrl: 'cru-pa-pb-list.component.html',
    providers: [PaPbServices],
})
@Injectable()
export class CruPaPbListComponent {
    private tableID = 'cru-pa-pb-list';
    private headerURL = this._API_Config.API_User_Metas + '/pbp';
    private headerDef = [{id: 'ver_table', value: 2},
        {id: 'ck', name: '', width: 25},
        {id: 'return_sts_name', name: 'Status', width: 90, unsortable: true},
        {id: 'return_num', name: 'Put Back Receipt #', width: 90},
        {id: 'odr_num', name: 'Order Number', width: 90},
        {id: 'cus_name', name: 'Customer', width: 90, unsortable: true},
        {id: 'num_of_sku', name: '# of SKUs', width: 90},
        {id: 'return_qty', name: 'Put Back QTY', width: 90, unsortable: true},
        {id: 'handler', name: 'Handler', width: 90, unsortable: true},
        {id: 'created_at', name: 'Created at', width: 110, unsortable: true},
        {id: 'created_by', name: 'Created by', width: 110, unsortable: true}];
    private pinCols = 5;
    private rowData:any[];
    private getSelectedRow;
    private objSort;
    private searchParams;
    private whs_id:any;
    private Pagination;
    private totalCount = 0;
    private tmpPageCount;
    private pageCount = 0;
    private ItemOnPage = 0;
    private currentPage = 1;
    private perPage = 20;
    private numLinks = 3;
    private messages:any;
    private dataList = [];
    private dataListCustomer = [];
    private asnStatus:any = [];
    private showLoadingOverlay = false;
    private hasEditBtn:any;
    private hasCreateBtn:any;
    private action;
    private dataCSRUser = [];
    private listSelectedGR = [];
    private assignUser;
    private refreshRowData = false;
    private flagPerPrintPutback = false;
    private hasUpdateBtn: any;

    // Construct
    constructor(
                private _paPbServices:PaPbServices,
                private _func:Functions,
                private _API_Config:API_Config,
                private _http:Http,
                _RTAction: RouteData,
                public jwtHelper:JwtHelper,
                private _router:Router,
                private _user:UserService) {

        this.action = _RTAction.get('action');

        this.whs_id = localStorage.getItem('whs_id');
        // Check permission
        this.checkPermission();
        this.getCustomers();
        var tmpparam = "&type=putaway";
        this.getListReturnOrder(1, tmpparam);

    }

    private checkPermission() {

        var target = this;
        this._user.GetPermissionUser().subscribe(
            data => {
                this.hasUpdateBtn = this._user.RequestPermission(data, 'updatePutback');
                this.flagPerPrintPutback = this._user.RequestPermission(data, 'printPutback');

                var flagView = this._user.RequestPermission(data, 'viewPutbackReceipt');
                
                if (!flagView) {
                    this._router.parent.navigateByUrl('/deny');
                }
            },
            err => {

                this.showMessage('danger', this._func.parseErrorMessageFromServer(err));

            }
        );
    }

    private showMessage(msgStatus, msg) {

        this.messages = {
            'status': msgStatus,
            'txt': msg
        };
        jQuery(window).scrollTop(0);
    }

    private requestPermission(permission, permission_req) {

        if (permission && permission.length) {

            for (var per of permission) {

                if (per.name == permission_req) {

                    return true;
                }
            }

        }

        return false;

    }

    // Format data for ag-grid
    private createRowData(data) {
        var rowData:any[] = [];
        // Check data
        if (typeof data.data != 'undefined') {

            data = this._func.formatData(data.data);
            for (var i = 0; i < data.length; i++) {

                let link = "";
                if(data[i].return_sts != 'NW'){
                    link = '<a href="#/putback-process/pb-receipt/' + data[i].return_id +'">'+ data[i].return_num+'</a>';
                } else {

                    link = data[i].return_num;
                }

                rowData.push({
                    return_sts_name: data[i].return_sts_name,
                    return_num: link,
                    odr_num: data[i].odr_num,
                    cus_name: data[i].cus_name,
                    num_of_sku: data[i].num_of_sku,
                    return_qty: data[i].return_qty,
                    handler: data[i].putter_name,
                    created_at: data[i].created_at,
                    created_by: data[i].created_by,
                    cus_id: data[i].cus_id,
                    return_id: data[i].return_id,
                    return_sts: data[i].return_sts,
                    return_num_name: data[i].return_num
                })
            }
        }

        this.rowData = rowData;
    }

    // Get ListReturnOrder
    private getListReturnOrder(page = null, tmpparam = '') {

        this.showLoadingOverlay = true;
        if (!page) page = 1;
        var params = "?page=" + page + "&limit=" + this.perPage + "&type=putaway";
        params += "&whs_id=" + this.whs_id;
        if (this.objSort && this.objSort['sort_type'] != 'none') {
            params += '&sort[' + this.objSort['sort_field'] + ']=' + this.objSort['sort_type'];
        }
        else {
            params += '&sort[created_at]=desc';
        }
        if (this.searchParams) {
            params += this.searchParams;
        }
        if (this.action == 'pul') {
            params+='&no_assign=1&putter=1';
        }
        this._paPbServices.getListReturnOrder(params)
            .subscribe(
                data => {
                    
                    this.dataList = data.data;
                    this.initPagination(data);
                    this.createRowData(data);
                    this.showLoadingOverlay = false;
                },
                err => {

                    this.parseError(err);
                    this.showLoadingOverlay = false;
                },
                () => {
                }
            );
    }

    // Get customers
    private getCustomers() {

        var $params = '?limit=10000';
        this._paPbServices.getCustomersByWH($params)
            .subscribe(
                data => {

                    this.dataListCustomer = data.data;
                },
                err => {

                    this.parseError(err);
                },
                () => {
                }
            );

    }

    private filterList(pageNumber) {
        this.getListReturnOrder(pageNumber);
    }

    private getPage(pageNumber) {
        let arr = new Array(pageNumber);
        return arr;
    }

    // Set params for pagination
    private initPagination(data) {
        var meta = data.meta;
        this.Pagination = meta['pagination'];
        this.Pagination['numLinks'] = 3;
        this.Pagination['tmpPageCount'] = this._func.Pagination(this.Pagination);

    }

    // Event when user filter form
    private onPageSizeChanged($event) {
        this.perPage = $event.target.value;
        if ((this.Pagination['current_page'] - 1) * this.perPage >= this.Pagination['total']) {
            this.currentPage = 1;
        }
        else {
            this.currentPage = this.Pagination['current_page'];
        }
        this.getListReturnOrder(this.currentPage);
    }

    // Edit
    // Navigation to edit page
    private edit() {
        this.getSelectedRow = 'edit';
    }

    saveAssignPutter() {

        var data = {
            'user_id': this.assignUser.user_id,
            'return_id': this.listSelectedGR
        };
        this.showLoadingOverlay = true;
        this._paPbServices.assignPutter(JSON.stringify(data))
            .subscribe(
                data => {
                    jQuery('#assign-csr').modal('hide');
                    this.updatePutterInPAList();
                    this.showLoadingOverlay = false;
                },
                err => {
                    jQuery('#assign-csr').modal('hide');
                    this.showMessage('danger', this._func.parseErrorMessageFromServer(err));
                    this.showLoadingOverlay = false;
                }
            );
    }

    updatePutterInPAList() {

        this.search(this.Pagination['current_page']);

        for(var i = 0, l = this.rowData.length; i < l; i++) {
            if(this.listSelectedGR.indexOf(this.rowData[i].odr_id) != -1) {
                this.rowData[i].handler = this.assignUser.first_name + ' ' + this.assignUser.last_name;
            }
        }
        this.refreshList();
    }

    refreshList() {
        this.refreshRowData = true;
        setTimeout(()=>{
            this.refreshRowData = false;
        }, 400);
    }

    private assignHandler() {

        this.getSelectedRow = 'assignHandler';
    }

    selectCSR(item) {
        for(var i = 0, l = this.dataCSRUser.length; i < l; i++) {
            this.dataCSRUser[i].selected = false;
        }
        item.selected = true;
        this.assignUser = item;
    }

    private PAUpdate() {

        this.getSelectedRow = 'edit';
    }

    private printPutBackPdf() {

        this.messages = false;
        if (!this.flagPerPrintPutback) {

            this.messages = this._func.Messages('danger', this._func.msg('CC008'));
            return false;
        }
        this.getSelectedRow = 'printPutBackPdf';
    }

    afterGetSelectedRow($event) {
        var listSelectedItem = $event.data;
        this.messages = false;
        
        this.listSelectedGR = [];
        for(let i = 0, l = listSelectedItem.length; i < l; i++) {
            this.listSelectedGR.push(listSelectedItem[i].return_id);
        }
        switch ($event.action) {
            case 'edit':
                if (listSelectedItem.length > 1) {
                    this.messages = this._func.Messages('danger', this._func.msg('PAS04'));
                }
                else {
                    if (listSelectedItem.length < 1) {
                        this.messages = this._func.Messages('danger', this._func.msg('PAS04'));
                    }
                    else {
                        if(this.action == 'pul' && listSelectedItem[0].return_sts != 'SG') {
                            // this.messages = this._func.Messages('danger', this._func.msg('PAS07'));
                            this._router.parent.navigateByUrl('/putback-process/putaway/putaway-update/' + listSelectedItem[0].return_id);
                        }
                        else {
                            this._router.parent.navigateByUrl('/putback-process/putaway/putaway-update/' + listSelectedItem[0].return_id);
                        }
                    }
                }
                break;
            case 'delete':
                break;

            case 'printPutBackPdf':
                if (listSelectedItem.length < 1) {
                    this.messages = this._func.Messages('danger', this._func.msg('PAS05'));
                }
                else {

                    if (listSelectedItem.length > 1) {
                        this.messages = this._func.Messages('danger', this._func.msg('PAS05'));
                        break;
                    }

                    try {

                        this.showLoadingOverlay = true;
                        var that = this;
                        var xhr = new XMLHttpRequest();

                        xhr.open("GET", that._API_Config.API_ORDERS_V2 + '/putback/' + listSelectedItem[0].return_id + "/print", true);
                        xhr.setRequestHeader("Authorization", 'Bearer ' +this._func.getToken());
                        xhr.responseType = 'blob';

                        xhr.onreadystatechange = function () {
                            if(xhr.readyState == 2) {
                                if(xhr.status == 200) {
                                    xhr.responseType = "blob";
                                } else {
                                    xhr.responseType = "text";
                                }
                            }

                            if(xhr.readyState === 4) {
                                if(xhr.status === 200) {
                                    var blob = new Blob([this.response], {type: 'application/pdf'});
                                    saveAs.saveAs(blob, listSelectedItem[0].return_num_name + ' Putaway List.pdf');
                                }else{
                                    let errMsg = '';
                                    if(xhr.response) {
                                        try {
                                            var res = JSON.parse(xhr.response);
                                            errMsg = res.errors && res.errors.message ? res.errors.message : xhr.statusText;
                                        }
                                        catch(err){errMsg = xhr.statusText}
                                    }
                                    else {
                                        errMsg = xhr.statusText;
                                    }
                                    if(!errMsg) {
                                        errMsg = that._func.msg('VR100');
                                    }
                                    that.showMessage('danger', errMsg);
                                }
                                that.showLoadingOverlay = false;
                            }
                        };

                        xhr.send();

                    } catch (err) {

                        this.showMessage('danger', this._func.msg('VR100'));
                        that.showLoadingOverlay = false;
                    }
                }
                break;

            case 'assignHandler':
                this.dataCSRUser = [];
                this.assignUser = null;
                if (listSelectedItem.length < 1) {
                    this.showMessage('danger', this._func.msg('PAS01'));
                }
                else {
                        var listCustomer = [];
                        listCustomer=this.mapCustomers(listSelectedItem);

                        if(listCustomer.length > 1){
                            this.showMessage('danger', this._func.msg('PAS06'));
                        }else{

                            var hasCanceledOrder = false;
                            var hasCo = false;

                            for(let i = 0; i < listSelectedItem.length; i++) {
                                if(listSelectedItem[i].odr_sts_code == 'CC') {
                                    hasCanceledOrder = true;
                                }

                                if (listSelectedItem[i].return_sts == 'CO') {

                                    hasCo = true;
                                    break;
                                }
                            }

                            if (hasCo) {
                                this.showMessage('danger', this._func.msg('PAS001'));
                                break;
                            }

                            if(hasCanceledOrder) {
                                this.showMessage('danger', this._func.msg('PAS02'));
                            }
                            else {

                                this.showLoadingOverlay = true;
                                this._paPbServices.getListCRSByCustomer(listCustomer.join(','),'/' + localStorage.getItem('whs_id'))
                                    .subscribe(
                                        data => {

                                            if(data.data && data.data.length) {
                                                jQuery('#assign-csr').modal('show');
                                                this.dataCSRUser = data.data;
                                            }
                                            else {
                                                this.showMessage('danger', this._func.msg('PAS02'));
                                            }
                                            this.showLoadingOverlay = false;
                                        },
                                        err => {

                                            this.showMessage('danger', this._func.parseErrorMessageFromServer(err));
                                            this.showLoadingOverlay = false;
                                        }
                                    );
                            }
                        }

                }
                break;
        }

        this.getSelectedRow = false;
    }

    private mapCustomers(listSelectedItem:Array<any>=[]){
        let listCustomer=[];
        for(let i = 0, l = listSelectedItem.length; i < l; i++) {
            let cusId = listSelectedItem[i].cus_id;
            if(listCustomer.indexOf(cusId) == -1) {
                listCustomer.push(cusId);
            }
        }
        return listCustomer;
    }

    private doSort(objSort) {
        this.objSort = objSort;
        this.getListReturnOrder(this.Pagination.current_page);
    }

    // Search
    private search(page = 1) {
        this.searchParams = '';
        let params_arr = jQuery("#form-filter").serializeArray();
        for (let i in params_arr) {
            if (params_arr[i].value == "") continue;
            this.searchParams += "&" + params_arr[i].name.trim() + "=" + encodeURIComponent(params_arr[i].value.trim());
        }
        this.getListReturnOrder(page);
    }

    // Reset form
    private reset() {

        jQuery("#form-filter input, #form-filter select").each(function (index) {

            jQuery(this).val("");
        });
        this.searchParams = '';
        this.getListReturnOrder(1);

    }

    // Show error when server die or else
    private parseError(err) {

        this.messages = {'status': 'danger', 'txt': this._func.parseErrorMessageFromServer(err)};
    }

    private expandTable = false;

    private viewListFullScreen() {
        this.expandTable = true;
        setTimeout(() => {
            this.expandTable = false;
        }, 500);
    }
}