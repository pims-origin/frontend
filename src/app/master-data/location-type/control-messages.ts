import { Component, Host } from '@angular/core';
import { NgFormModel } from '@angular/common';
import {ValidationService} from "./validation.service";

@Component({
    selector: 'control-messages',
    inputs: ['controlName: control'],
    template: `<div *ngIf="errorMessage !== null" class="error">{{errorMessage}}</div>`
})
export class ControlMessages {
    controlName: string;
    private params = [];
    constructor(@Host() private _formDir: NgFormModel) {

        this.params['loc_type_name'] = 'Location Type Name';
        this.params['loc_type_code'] = 'Location Type Code';

    }

    get errorMessage() {
        // Find the control in the Host (Parent) form
        let c = this._formDir.form.find(this.controlName);

        for (let propertyName in c.errors) {

            // If control has a error
            if (c.errors.hasOwnProperty(propertyName) && c.touched) {
                
                // Return the appropriate error message from the Validation Service
                return ValidationService.getValidatorErrorMessage(this.params[this.controlName] ,propertyName);
            }
        }

        return null;
    }
}