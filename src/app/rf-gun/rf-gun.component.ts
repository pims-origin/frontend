import {Component} from "@angular/core";
import {RouteConfig, RouterLink,RouteData, Router, ROUTER_DIRECTIVES} from '@angular/router-deprecated';
import { FORM_DIRECTIVES,FormBuilder,Location} from '@angular/common';
import {UserService, Functions} from '../common/core/load';
import { ValidationService } from '../common/core/validator';
import {WMSBreadcrumb,WMSPagination,PaginationControlsCmp,ColorPickerDirective,WMSMessages,ColorPickerService,PaginationService} from '../common/directives/directives';
import {GoBackComponent} from "../common/component/goBack.component";
import {BoxPopupService} from "../common/popup/box-popup.service";
import {CustomerServices} from "../system-setup/customer/customer-service";
import {RFGunServices} from "./rf-gun-service";
import { Title }     from '@angular/platform-browser';
/*
* Import page
* */
import {RFPUTAWAYComponent} from "./page/rf-putaway/rf-putaway.component";
import {RFGUNDASHBOARDComponent } from "./page/rf-dashboard/rf-dashboard";
import {RFSKUComponent} from "./page/rf-putaway/rf-sku-list/rf-sku-list";
import {RF_PutAwayUpdateComponent} from "./page/rf-putaway/rf-putaway-update/rf-putaway-update";
import {RF_WavePickListComponent} from "./page/rf-wavepick/rf-wavepick-list.component";
import {RF_WavePickSKUListComponent} from "./page/rf-wavepick/rf-wv-sku-list/rf-wv-sku-list";
import {RF_WavePickUpdateComponent} from "./page/rf-wavepick/rf-wv-update/rf-wave-update";
import {RF_RelocateComponent} from "./page/rf-relocate/rf-relocate";

@Component({
  selector: 'rf-gun',
  providers: [Title,CustomerServices,RFGunServices,PaginationService,ColorPickerService,ValidationService,FormBuilder, BoxPopupService,UserService],
  directives: [FORM_DIRECTIVES,ROUTER_DIRECTIVES,WMSPagination,PaginationControlsCmp,ColorPickerDirective,WMSMessages,GoBackComponent,WMSBreadcrumb],
  templateUrl: 'rf-gun.component.html',
})

@RouteConfig([
  { path: '/', component: RFGUNDASHBOARDComponent, name: 'RF GUN' ,data: {page_title:'RF GUN'}},
  { path: '/putaway', component: RFPUTAWAYComponent, name: 'RF GUN' , data: {page_title:'Put Away'}},
  { path: '/putaway/sku-list/:gr_id', component: RFSKUComponent, name: 'RF List' , data: {page_title:'Put Away'} },
  { path: '/putaway/rf-putaway-update/:id', component: RF_PutAwayUpdateComponent, name: 'RF List', data: {page_title:'Put Away'} },
  { path: '/wave-pick', component: RF_WavePickListComponent, name: 'RF List' ,data:{page_title:'Wave Pick'}},
  { path: '/wave-pick/rf-sku-list/:id', component: RF_WavePickSKUListComponent, name: 'RF List',data: {page_title:'Wave Pick'} },
  { path: '/wave-pick/rf-wavepick-update/:id', component: RF_WavePickUpdateComponent, name: 'RF List' ,data: {page_title:'Wave Pick'}},
  { path: '/relocate', component: RF_RelocateComponent, name: 'RF List' ,data: {page_title:'Relocate'}},
])

export class RFGUNComponent{

  private show_dashboard_nav=true;
  private allowAccess;
  private showLoadingOverlay;
  private messages;
  private rfGunPermission;
  private whs_id;
  private showMenu:boolean=false;
  private suggestPalletLoc;
  private routerParent='/rf-gun/';
  private page_title='RF GUN';

  constructor(private rfGunService:RFGunServices,
              private _router: Router,
              private _user:UserService,
              private titleService: Title,
              private _RTAction: RouteData,
              private location: Location,
              private _Func: Functions)
 {
    this.checkPermission();
   this.getPageTile();
 }

  backPage()
  {
    this.location.back();
  }

  public setTitle( newTitle: string) {
    this.titleService.setTitle(newTitle);
  }


  getPageTile()
  {
    if(this._router.currentInstruction)
    {
      let page_title=this._router.currentInstruction.component.routeData.data['page_title'];
      this.page_title=page_title;
      this.setTitle(page_title);
    }

    this._router.root.subscribe((router)=>{
      if(typeof router!=='undefined'){
        let page_title=this._router.currentInstruction.component.routeData.data['page_title'];
        this.page_title=page_title;
        this.setTitle(page_title);
      }
    });
  }

  _showMenu()
  {
    this.showMenu = !this.showMenu;
  }

  redirectTo(page)
  {
    this._router.parent.navigateByUrl(this.routerParent+page);
  }

  checkPermission(){

    this.showLoadingOverlay = true;
    this._user.GetPermissionUser().subscribe(
      data => {
        this.showLoadingOverlay = false;
        this.rfGunPermission = this._user.RequestPermission(data, 'viewRFGUN');
          if(this.rfGunPermission) {
            this.allowAccess=true;
            this.whs_id = localStorage.getItem('whs_id');
          }
          else{
            this.redirectDeny();
          }
      },
      err => {
        this.parseError(err);
        this.showLoadingOverlay = false;
      }
    );
  }

  redirectDeny(){
    this._router.parent.navigateByUrl('/deny');
  }

  goBackPage()
  {

  }

  // Show error when server die or else
  private parseError (err) {
    err = err.json();
    this.messages = this._Func.Messages('danger', err.errors.message);
  }

}
