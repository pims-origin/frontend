import {Component,OnInit} from "@angular/core";
import { Router, RouterLink, ROUTER_DIRECTIVES } from '@angular/router-deprecated';
import { FORM_DIRECTIVES,ControlGroup,FormBuilder, Control,Validators} from '@angular/common';
import {API_Config, UserService, Functions} from '../../../common/core/load';
import { Http } from '@angular/http';
import {BoxPopupService} from "../../../common/popup/box-popup.service";
import {CommodityServices} from "../commodity-service";
import { ValidationService } from '../../../common/core/validator';
import {WMSPagination,AgGridExtent,WMSMessages} from '../../../common/directives/directives';
import 'ag-grid-enterprise/main';
declare var jQuery: any;

@Component({
    selector: 'event-tracking',
    providers:[CommodityServices,ValidationService,BoxPopupService],
    directives: [ROUTER_DIRECTIVES,AgGridExtent,WMSMessages,WMSPagination],
    templateUrl: 'commodity-list.html'
})

export class CommodityList{

    private headerURL = this.apiService.API_User_Metas+'/cmd';
    private columnData = {
        cmd_id:{
            attr:'checkbox',
            id:true,
            title:'#',
            width:25,
            pin:true,
            ver_table:'2'
        },
        cmd_name: {
            title:'Commodity Name',
            url:'#/master-data/commodity/',
            field_get_id:'cmd_id',
            width:120,
            pin:true,
            sort:true
        },
        cmd_cls: {
            title:'Commodity Class',
            width:170,
            pin:true,
            sort:true
        },
        commodity_nmfc:{
            title:'Commodity NMFC',
            width:120,
            pin:true
        },
        cmd_des: {
            title:'Description',
            width:170
        }
    };

    private Pagination={};
    private perPage=20;
    private whs_id;
    private messages;
    private Loading={};
    private searching={};
    searchForm:ControlGroup;
    private isSearch:boolean=false;
    private CommodityList:Array<any>=[];
    private sortData={fieldname:'cmd_name',sort:'asc'};
    private showLoadingOverlay:boolean=false;
    private ListSelectedItem:Array<any>=[];
    private Customers:Array<any>=[];

    constructor(
        private cmdService:CommodityServices,
        private _Func: Functions,
        private _boxPopupService: BoxPopupService,
        private  apiService:API_Config,
        private _user:UserService,
        private fb: FormBuilder,
        private _http: Http,
        private _Valid: ValidationService,
        private _router: Router) {

        this.checkPermission();
        this.buildSearchForm();

    }

    // Check permission for user using this function page
    private createCommodity;
    private viewCommodity;
    private editCommodity;
    private allowAccess:boolean=false;
    private checkPermission() {
        this.showLoadingOverlay = true;
        this._user.GetPermissionUser().subscribe(
            data => {
                this.createCommodity = this._user.RequestPermission(data, 'createCommodity');
                this.viewCommodity = this._user.RequestPermission(data,'viewCommodity');
                this.editCommodity=this._user.RequestPermission(data,'editCommodity');
                /* Check orther permission if View allow */
                if(!this.viewCommodity) {
                    this.redirectDeny();
                }
                else {
                    this.allowAccess=true;

                    this.getCommodityList();
                }
            },
            err => {
                this.messages=this._Func.Messages('danger', this._Func.parseErrorMessageFromServer(err));
                this.showLoadingOverlay = false;
            }
        );
    }

    buildSearchForm()
    {
        this.searchForm =this.fb.group({
            cmd_name: [''],
            cmd_cls: [''],
            commodity_nmfc: ['']
        });
    }

    private searchParam;
    Search()
    {
        this.isSearch=true;
        let data=this.searchForm.value;
        // set status search is true
        // assign to SearchQuery
        // whs_id=&cus_code=&cus_city_name=&cus_name=&cus_billing_account=&cus_country_id=&cus_state_id=&cus_postal_code=
        this.searchParam="&cmd_name="+data['cmd_name']+"&cmd_cls="+encodeURIComponent(data['cmd_cls'])+"&commodity_nmfc="
            +encodeURIComponent(data['commodity_nmfc']);
        this.getCommodityList();
    }


    redirectDeny(){
        this._router.parent.navigateByUrl('/deny');
    }


    private onPageSizeChanged($event)
    {
        this.perPage = $event.target.value;
        if(this.Pagination){
            if((this.Pagination['current_page'] - 1) * this.perPage >= this.Pagination['total']) {
                this.Pagination['current_page'] = 1;
            }
            this.getCommodityList(this.Pagination['current_page']);
        }

    }


    /*
     * Router edit
     * */
    Edit() {
        if (this.ListSelectedItem.length > 1) {
            this.messages = this._Func.Messages('danger',this._Func.msg('WMS002'));
            this.scolltoTop();
        }
        else {
            if(this.ListSelectedItem.length)
            {
                this._router.navigateByUrl('/master-data/commodity/' + this.ListSelectedItem[0]['cmd_id'] + '/edit');
            }
            else{
                this.messages = this._Func.Messages('danger',this._Func.msg('WMS001'));
                this.scolltoTop();
            }
        }
    }


    private getCommodityList(page=null){

        let param="?sort["+this.sortData['fieldname']+"]="+this.sortData['sort']+"&page="+page+"&limit="+this.perPage;
        if(this.isSearch){
            param=param+this.searchParam;
        }
        this.showLoadingOverlay=true;

        this.cmdService.getCommoditylist(param).subscribe(
            data => {
                this.showLoadingOverlay=false;
                this.CommodityList = this._Func.formatData(data.data);
                //pagin function
                if(data['meta']){
                    this.Pagination=data['meta']['pagination'];
                    this.Pagination['numLinks']=3;
                    this.Pagination['tmpPageCount']=this._Func.Pagination(this.Pagination);
                }
            },
            err => {
                this.parseError(err);
                this.showLoadingOverlay=false;
            },
            () => {}
        );

    }
    scolltoTop(){
        jQuery('html, body').animate({
            scrollTop: 0
        }, 700);
    }
    /*
     * Reset Form Builder
     * */
    ResetSearch()
    {
        this.isSearch=false; // remove action search
        this._Func.ResetForm(this.searchForm);
        this.getCommodityList(this.Pagination['current_page']);
    }

    // Show error when server die or else
    private parseError (err) {
        err = err.json();
        this.messages = this._Func.Messages('danger', err.errors.message);
    }

}
