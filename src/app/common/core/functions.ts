import {Component, Injectable} from '@angular/core';
import {Http, Response, Headers, HTTP_BINDINGS } from '@angular/http';
import {ControlGroup,Control} from '@angular/common';
import { Router} from '@angular/router-deprecated';
import {Messages} from '../languages/en/messages';

declare var jQuery: any;
@Injectable()
export class Functions {
  public ListMgs=new Messages().ListMgs;
  constructor(private _router:Router){}
  /*================================
   Get Token
   =================================*/
  getToken() {
    return localStorage.getItem('jwt');
  }
  /*================================
   Get Warehouse ID
   =================================*/
  getWareHouseId() {
    return localStorage.getItem('whs_id');
  }
  /*=================================
   AuthHeader
   ==================================*/
  AuthHeader(){
    var authHeader = new Headers();
    authHeader.append('Authorization', 'Bearer ' + this.getToken());
    return authHeader;
  }
  AuthHeaderNoTK(){
    var authHeader = new Headers();
    authHeader.append('Content-Type', 'application/x-www-form-urlencoded');
    return authHeader;
  }
  AuthHeaderPost()
  {
    var authHeader = new Headers();
    authHeader.append('Content-Type', 'application/x-www-form-urlencoded');
    authHeader.append('Authorization', 'Bearer ' + this.getToken());
    return authHeader;
  }
  AuthHeaderPostJson() {
    var authHeader = new Headers();
    authHeader.append('Content-Type', 'application/json');
    authHeader.append('Authorization', 'Bearer ' + this.getToken());
    return authHeader;
  }
  Messages(status,text_messages)
  {
    let mes = {
      status: status,
      txt: text_messages
    }
    return mes;
  }
  /*
   user AllowIfPermission
   */
  AllowIfPermission(permissionAllow)
  {
    let CurrentUserPermission;
    if (permissionAllow in CurrentUserPermission)
    {
      return true;
    }
    else{
      return false;
    }
  }
  trim(str){
    let cvtoString=str+'';
    if(typeof cvtoString !== 'string') {
      throw new Error('only string parameter supported!');
    }
    return cvtoString.replace(/ /g,'');
  }
  /*
   Redirect function
   */
  RedirectTo(url)
  {
    this._router.parent.navigateByUrl(url);
  }
  /*
   let err=['a','b','c'];
   string ="This date {0} is not from {1} to {2}"
   */
  msg(vrId)
  {
    for (let item of this.ListMgs) {
      //console.log(item.vrId+'<>'+vrId);
      if (item.vrId == vrId)
      {
        //console.log('has found');
        return item.msgText;
      }
    }
    /*If not found*/
    return 'Msg not found';
  }
  // Set params for pagination
  /*
   * Param
   *
   *
   * **/
  Pagination(data) {
    let totalCount = data['total'];
    let ItemOnPage = data['count'];
    let pageCount = data['total_pages'];
    let currentPage = data['current_page'];
    let perPage = data['per_page'];
    let numLinks= data['numLinks'] ? data['numLinks'] : 3;
    let start=1;
    let end=1;
    let tmpPageCount=[];
    if (currentPage > numLinks) {
      start =currentPage - numLinks;
    } else {
      start = 1;
    }
    if (currentPage +numLinks <pageCount) {
      end = currentPage + numLinks;
    } else {
      end = pageCount;
    }
    //Create tmp for loop perPage in view
    for (var i = start; i <= end; i++) {
      tmpPageCount[i] = i;
    }
    tmpPageCount =tmpPageCount.filter(function(item) {
      return item !== undefined;
    });
    // console.log('__pagin fuction');
    return tmpPageCount;
  }
  LastParam()
  {
    var url = window.location.href;
    var array = url.split('/');
    return array[array.length-1];
  }
  FixEncodeURI (str) {
    let str_rep= str.replace('+','%2B');
    return encodeURI(str_rep);
  }
  /*
   * Function Reset Form
   * */
  ResetForm(FormName)
  {
    Object.keys(FormName.controls).map(
      key =>{
        (<Control>FormName.controls[key]).updateValue("");
      }
    );
  }
  /*
   * Reset Value State
   * */
  ResetState(FormName,ControlReset:string)
  {
    Object.keys(FormName.controls).map(
      key =>{
        if(<Control>FormName.controls[key]['controls'][ControlReset])
        {
          // console.log('reset form');
          (<Control>FormName.controls[key]['controls'][ControlReset]).updateValue('');
          //(<Control>FormName.controls[key]['controls'][ControlReset]).setErrors(null, true);
        }
      }
    );
  }
  /*
   * Function show error
   * */
  messageErrorForAddEdit(el,err)
  {
    // console.log(err);
    if(err.json().error.status==500)
    {
      return this.Messages('danger',err.json().error.message);
    }
    else{
      if(err.json().error.message)
      {
        return this.Messages('danger','Data is invalid ! Please fix it and try again');
      }
      else{
        return this.Messages('danger','Error! please contact administrator');
      }
    }
  }
  /*
   * MessageError
   * */
  MessageError(err)
  {
    if(err.json().error)
    {
      let  stt_code=err.json().errors.status;
      switch (stt_code)
      {
        case 200 :
          return this.Messages('danger',err.json().error.message);
        case 500 :
          return this.Messages('danger',err.json().error.message);
        default :
          return this.Messages('danger',err.json().error.message);
      }
    }
    else{
      return this.Messages('danger','Error ! please contact administrator');
    }
  }
  /*
   Do not use
   * Search and return litst data
   * */
  filterArray(items:any[], args:any):any[] {
    var isSearch = (data:any): boolean => {
      var isAll = false;
      if(typeof data === 'object' ){
        for (var z in data) {
          if(isAll = isSearch(data[z]) ){
            break;
          }
        }
      } else {
        if(typeof args === 'number'){
          isAll = data === args;
        } else {
          isAll = data.toString().match( new RegExp(args, 'i') );
        }
      }
      return isAll;
    };
    return items.filter(isSearch);
  }
  /*
   * Search item by key field
   * */
  filterbyfieldName(arr:any[], fieldname:string , value:any):any[]{
    var isSearch = (data:any): boolean => {
      var isAll = false;
      if(typeof data === 'object' && typeof data[fieldname] !== 'undefined'){
        isAll = isSearch(data[fieldname]);
      } else {
        if(typeof value === 'number'){
          isAll = data === value;
        } else {
          isAll = data.toString().match( new RegExp(value, 'i') );
        }
      }
      return isAll;
    };
    return arr.filter(isSearch);
  }
  // Error handle
  parseErrorMessageFromServer(err) {
    var errMessage = '';
    try {
      if(typeof err == 'object' && err._body) {
        err = JSON.parse(err._body);
      }
      else {
        err = err.json();
      }
      if(err.error) {
        if(typeof err.error == 'object') {
          for(let key in err.error) {
            let msgErr = typeof err.error[key] == 'string' ? err.error[key] : err.error[key][0];
            errMessage += '<p>'+ msgErr + '</p>'
          }
        }
        if(typeof err.error == 'string') {
          errMessage = err.error;
        }
      }
      else if(err.message || err.detail) {
        errMessage = err.message || err.detail;
      }
      else if(err.errors) {
        err = err.errors;
        errMessage = err.message || err.detail;
      }
    }
    catch(err) {
      // console.log('parse err', err);
    }
    if(!errMessage) {
      errMessage = this.msg('VR100');
    }
    return errMessage;
  }
  parseErrorMessageFromServerStandard(err){
    var errMessage = '';
    try {
      if(typeof err == 'object' && err._body) {
        err = JSON.parse(err._body);
      }
      else {
        err = err.json();
      }
      if(err.errors) {
        if(typeof err.errors == 'object') {
          if(err.errors['errors']){
            let errs=err.errors['errors'];
            Object.keys(errs).map(
              key =>{
                let msgErr = errs[key][0];
                errMessage += '<p>'+ msgErr + '</p>';
              }
            );
          }
          else{
            errMessage=err.errors.message;
          }
        }
      }
    }
    catch(err) {
      // console.log('parseErrorMessageFromServerStandard', err);
    }
    if(!errMessage) {
      errMessage = this.msg('VR100');
    }
    return errMessage;
  }
  formatData(data) {
    for(var i = 0, l = data.length; i < l; i++) {
      var item = data[i];
      for(let key in item) {
        if(typeof item[key] == 'string') {
          item[key] = item[key].replace(/</g,'&lt;');
        }
        if(typeof item[key] == 'object') {
          if(jQuery.isArray(item[key])) {
            for(var j = 0, k = item[key].length; j < k; j++) {
              var subItem = item[key][j];
              if(typeof subItem == 'string') {
                item[key][j] = item[key][j].replace(/</g,'&lt;');
              }
              if(typeof subItem == 'object') {
                for(let key2 in subItem) {
                  if(typeof subItem[key2] == 'string') {
                    subItem[key2] = subItem[key2].replace(/</g,'&lt;');
                  }
                }
              }
            }
          }
          else {
            for(let key2 in item[key]) {
              if(typeof item[key][key2] == 'string') {
                item[key][key2] = item[key][key2].replace(/</g,'&lt;');
              }
            }
          }
        }
      }
    }
    return data;
  }
  getIndexOf(arr:any[],value:any,indexfield:string): number {
    for(let i in arr) {
      if(value==arr[i][indexfield]){
        return parseInt(i);
      }
    }
    return -1;
  }
  lstGetItem(filename:string){
    let v=localStorage.getItem(filename);
    return v !== 'undefined' ? v : null;
  }
  checkDuplicatedValueRow(array:Array<any>,field){
    let dup=false;
    for(let i=0;i<array.length;i++) {
      array[i]['duplicated']=false;
    }
    for(let i=0;i<array.length;i++){
      let item=array[i][field];
      for(let j=i+1;j<array.length;j++){
        let itemj=array[j][field];
        if(i==j) {
          continue;
        }
        // console.log(i ,'->', item,'&&', j ,' > ',itemj);
        if((item&&itemj)&&item==itemj){
          array[i]['duplicated']=true;
          array[j]['duplicated']=true;
          dup=true;
        }
      }
    } // end for
    return {
      data:array,
      duplicated:dup
    }
  }
  genSearchCriteriaFromForm(searchForm: ControlGroup) {
    let keys = Object.keys(searchForm.controls);
    let sObj = {};
    if(Object.keys(sObj).length === 0 ) {
      // console.log('null');
    }
    keys.forEach(function (value) {
      sObj[value] = searchForm.controls[value].value;
    })
    // console.log('s', sObj);
    return sObj;
  }
  /*
   * Check All/Off
   * */
  setSelectedAll(array:Array<any>=[],value) {
    if(array.length){
      array.forEach((item) => {
        item['selected'] = value;
      });
    }
  }
  /*
   * Selected
   * */
  Selected(array,row,value) {
    let fullselect=false;
    array[row]['selected']=value;
    if (value==true) {
      if(this.checkSelectedAll(array)) {
        fullselect=true;
      }
    }
    else{
      fullselect=false;
    }
    return {
      full_selected:fullselect,
      array_data:array
    }
  }

  /*=============================================
  * Using for list model @tien.nguyen
  * =========================================*/
  public checkedItem(Data={},item={},$event:any) {

    if($event.target.checked){
      item['selected']=true;
    }else{
      item['selected']=false;
      Data['checkAll']=false;
    }
    if(this.checkSelectedAll(Data['data'])){
      Data['checkAll']=true;
    }else{
      Data['checkAll']=false;
    }

  }
    /*=============================================
  * Example :
  * Checkall :
  * <input [(ngModel)]="dataOrigin['checkAll']" (ngModelChange)="checkedAll(dataOrigin,$event)"  type="checkbox"/>
  * Checked Item
  * <input [(ngModel)]="item.selected" (ngModelChange)="checkedItem(cartonDataListMaster,item,$event)"  type="checkbox">
  * =========================================*/
  public checkedItemData(Data={},datalabel='data') {
    
    Data['checkAll']=false;
    if(this.checkSelectedAll(Data[datalabel])){
      Data['checkAll']=true;
    }else{
      Data['checkAll']=false;
    }
    
  }

  public __initPagination(DataArray={}){
    DataArray['numLinks']=3;
    DataArray['tmpPageCount']=this.Pagination(DataArray);
  }

  public onPageSizeChanged(Pagination={},$event)
  {
    if(Pagination){
      if((Pagination['current_page'] - 1) * Pagination['perPage'] >= Pagination['total']) {
        Pagination['current_page'] = 1;
      }
    }

  }

  /*=============================================
   * Using for list model @tien.nguyen
   * =========================================*/
  public checkedAll(Data={},$event:any) {
    if($event.target.checked){
      Data['checkAll']=true;
      this.setSelectedAll(Data['data'],true);
    }else{
      Data['checkAll']=false;
      this.setSelectedAll(Data['data'],false);
    }
  }

  /*=============================================
   * Using for list model @tien.nguyen
   * =========================================*/
  public checkedAllData(Data={},$event:any,label='data') {
    if($event){
      Data['checkAll']=true;
      this.setSelectedAll(Data[label],true);
    }else{
      Data['checkAll']=false;
      this.setSelectedAll(Data[label],false);
    }
  }

  checkSelectedAll(array:Array<any>){
    let checked=0;
    array.forEach((item)=>{
      if(item['selected']){
        checked++;
      }
    });
    if(checked==array.length){
      return true;
    }
    return false;
  }
  Selecteddemo(model){
    model=[];
  }
  deleteItemList(listArray:Array<any>){
    let arayTmp=listArray.slice();
    arayTmp.forEach((item)=>{
      // console.log('loop', item);
      if(item['selected']){
        let index=listArray.indexOf(item);
        listArray.splice(index, 1);
      }
    });
  }
  /*
   * scroll to TOP function
   * */
  scrollToTop() {
    jQuery('html,body').animate({ scrollTop: 0 }, 1000);
  }
  /*
   * Check two number
   * */
  checkSameValue(a,b){
    return a==b ? true : false;
  }

  public selectCustomerNameFromID(id = '', cus_nameArr = []){
    let result = '';
    for(let i = 0; i < cus_nameArr.length; i ++){
      if(id == cus_nameArr[i].cus_id){
        result = cus_nameArr[i].cus_name;
        break;
      }
    }
    return result;
  }
  goBack_OrderType(obj,data)
  {
    if(data['origin_odr_type']=='EC')
    {
      obj._router.parent.navigateByUrl('/outbound/online-order');
    }
    else{
      obj._router.parent.navigateByUrl('/outbound/order');
    }
  }

  RequestPermission(permission, permission_req) {
    if (permission && permission.length) {
      for (var per of permission) {
        //console.log(per.name + "<==>" + permission_req)
        if (per.name == permission_req) {
          //console.log('found permission');
          return true;
        }
      }
      return false;

    }
    else {
      return false;
    }
  }

  groupBy(arr, key) {
    var newArr = [],
        types = {},
        newItem, i, j, cur;
    for (i = 0, j = arr.length; i < j; i++) {
      cur = arr[i];
      if (!(cur[key] in types)) {
        types[cur[key]] = { group: cur[key], data: [] };
        newArr.push(types[cur[key]]);
      }
      types[cur[key]].data.push(cur);
    }
    return newArr;
  }

  public formatSWISNumber(value:number):string {
    return value ? value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',') : '0';
  }

  public parseQueryString(queryString) {
    var params = {}, queries, temp, i, l;
    // Split into key/value pairs
    queries = queryString.split("&");
    // Convert the array of strings into an object
    for ( i = 0, l = queries.length; i < l; i++ ) {
        temp = queries[i].split('=');
        params[temp[0]] = temp[1];
    }
    return params;
  }

  public getValueArray(array:Array<any>=[],filename:string,selectedonly=false){

    let array_value = [];
    for (let item of array){

      if(selectedonly){
        if(item['selected']){
          array_value.push(item[filename]);
        }
      }else{
        array_value.push(item[filename]);
      }

    }
    return array_value;

  }

  public createObjectData(array:Array<any>=[],key:any,value:any){

    let dataObject={};
    array.forEach((item)=>{
      dataObject[item[key]]=item[value];
    });

    return dataObject;

  }

  public cloneObject(source) {
    let target = {};

    for (let prop in source) {
      target[prop] = source[prop];
    }

    return target;
  }
  
  getToday() {
    let today = new Date();
    let dd = today.getDate();
    let mm = today.getMonth() + 1; // January is 0!
    let d = '', m = '';
    
    let yyyy = today.getFullYear();
    d = dd < 10 ? '0' + dd : dd.toString();
    m = mm < 10 ? '0' + mm : mm.toString();

    return m + '/' + d + '/' + yyyy;
  }
}
