import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {API_Config, Functions} from '../../common/core/load';

@Injectable()
export class SkuService {
	public AuthHeader = this._Func.AuthHeader();
	public AuthHeaderPost = this._Func.AuthHeaderPost();

	constructor(private _Func:Functions, private _API:API_Config, private http:Http) {
	}

	getListActivity(wh_id, params) {
		return this.http.get(`${this._API.API_URL_ROOT}/core/warehouse/v1/warehouses/${wh_id}/sku-report` + params, {headers: this.AuthHeader})
			.map((res:Response) => res.json());
	}

	getReportAPIUrl(wh_id, params:string = '') {
		return `${this._API.API_URL_ROOT}/core/warehouse/v1/warehouses/${wh_id}/sku-report${params}&export=1`;
	}

	// getCustomersByWH(params) {
	// 	return this.http.get(`${this._API.API_INVENTORY_NEW_REPORTS}/customers${params}&sort[cus_name]=asc`, {headers: this.AuthHeader})
	// 		.map(res => res.json());
	// }

	getCustomersByUser(params) {
		return this.http.get(`${this._API.API_Customer_User}${params}&sort[cus_name]=asc`, {headers: this.AuthHeader})
			.map(res => res.json());
	}
	
	public getSKUs(params) {
		return this.http.get(`${this._API.API_INVENTORY_NEW_REPORTS}/auto-sku${params}`, {headers: this.AuthHeader})
			.map(res => res.json());

	}
	

}
