/**
 * Created by admin on 10/4/2016.
 */
import {Component } from '@angular/core';
import {OrderListComponent} from "../order/order-list/order-list.component";

@Component({
  selector: 'online-order',
  providers: [],
  directives: [OrderListComponent],
  pipes:[],
  templateUrl: 'online-order.component.html',
})

export class onlineOrderComponent {
  constructor(){
    
  }
}
