import {Component} from '@angular/core';
import {RouteConfig, ROUTER_DIRECTIVES} from '@angular/router-deprecated';
import {PbOrderListComponent} from "./pb-order/pb-order-list.component";
import {PbReceiptListComponent} from "./pb-receipt/pb-receipt-list.component";
import {PutAwayPbComponent} from "./putaway-pb/putaway-pb.component";
import {PbReceiptDetailComponent} from "./pb-receipt/pb-receipt-detail.component";


@Component ({
    selector: 'putback-process',
    directives: [ROUTER_DIRECTIVES],
    template: `<router-outlet></router-outlet>`
})
    
@RouteConfig([
    { path: '/', component:PbOrderListComponent , name: 'Pb Order List',useAsDefault: true },
    { path: '/pb-receipt', component:PbReceiptListComponent , name: 'Pb Receipt List' },
    { path: '/putaway/...', component:PutAwayPbComponent , name: 'Putaway Pb Component' },
    { path: '/pb-receipt/:id', component:PbReceiptDetailComponent , name: 'Pb Receipt Detail' },
])
export class PutbackProcessComponent {

}