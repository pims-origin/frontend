import {Component, ViewChild} from '@angular/core';
import {ControlGroup, FormBuilder, Control, Validators} from '@angular/common';
import {Router} from '@angular/router-deprecated';
import {UserService} from "../../common/users/users.service";
import {MigrationService} from "../migration.service";
import {Functions, API_Config} from '../../common/core/load';
import {WMSBreadcrumb, WMSMessages} from '../../common/directives/directives';

@Component({
	selector: 'forecast-migration',
	providers: [MigrationService],
	directives: [WMSBreadcrumb, WMSMessages],
	templateUrl: 'forecast-migration.component.html'
})

export class ForecastMigrationComponent {

	@ViewChild('itemFileView') itemFileView: any;
	public showLoadingOverlay:boolean = false;
	public canAccess:boolean = false;
	public messages:any;
	public customers:Array<any>;
	public importForm:ControlGroup;
	public imported:boolean = false;
	public itemFile:any;
	public curYear = new Date().getFullYear();
	public curQuater = Math.ceil((new Date().getMonth()+1)/3);
	public period = 'month';

	constructor(private userService:UserService,
				private migrationService:MigrationService,
				private funcs:Functions,
				private api:API_Config,
				private router:Router,
				private fb:FormBuilder) {
		this.buildFormImport();
		this.checkPermission();
	}

	buildFormImport() {
		this.importForm = this.fb.group({
			cus_id: ['', Validators.required],
			// quarter : [this.curQuater],
			year : [this.curYear]
		});
	}

	private checkPermission() {
		this.showLoadingOverlay = true;
		this.userService.GetPermissionUser().subscribe(
			data => {
				this.showLoadingOverlay = false;
				this.canAccess = this.userService.RequestPermission(data, 'importOrder');
				if (this.canAccess) {
					this.getCustomers();
					this.initQuarter();
					this.initYear();
				} else {
					// redirect to deny page.
					this.router.parent.navigateByUrl('/deny');
				}
			},
			err => {
				this.messages = this.funcs.Messages('danger', this.funcs.parseErrorMessageFromServer(err));
				this.showLoadingOverlay = false;
			}
		);
	}

	private getCustomers() {
		this.showLoadingOverlay = true;
		this.migrationService.getCustomersByWH().subscribe(
			data => {
				this.showLoadingOverlay = false;
				this.customers = data['data'];
			},
			err => {
				this.showLoadingOverlay = false;
				this.messages = this.funcs.Messages('danger', this.funcs.parseErrorMessageFromServer(err));
			}
		)
	}

	public changeItemFile(event) {
		this.itemFile = <Array<File>> event.target.files[0];
	}

	public importFile() {
		const that = this;
		that.imported = true;
		that.messages = null;

		if (that.importForm.valid && this.itemFile) {
			let formData:any = new FormData();
			let xhr = new XMLHttpRequest();

			// process file
			const fileExtension = that.itemFile.name.substr(that.itemFile.name.length - 4);
			if (fileExtension !== "xlsx" && fileExtension !== ".csv") {
				that.messages = that.funcs.Messages('danger', that.funcs.msg('CF003'));
				return;
			}

			formData.append("file", that.itemFile, that.itemFile.name);
			// formData.append('quarter', that.importForm.value['quarter']);
			formData.append('period', that.period);
			formData.append('year', that.importForm.value['year']);
			formData.append('cus_id', that.importForm.value['cus_id']);

			that.showLoadingOverlay = true;
			xhr.open("POST", `${that.api.API_REPORTS_CORE}/forecast/import`, true);
			xhr.setRequestHeader("Authorization", 'Bearer ' + that.funcs.getToken());
			xhr.onreadystatechange = function () {
				if (xhr.readyState == 2) {
					if (xhr.status == 200) {
						xhr.responseType = "blob";
					} else {
						xhr.responseType = "text";
					}
				}

				if (xhr.readyState == 4) {
					if (xhr.status === 200) {
						that.itemFileView.nativeElement.value = '';
						that.messages = that.funcs.Messages('success', 'Import file successfully!');
						that.showLoadingOverlay = false;
					} else {
						let msg = '';

						if (xhr.response) {
							try {
								var res = JSON.parse(xhr.response);
								msg = res.errors && res.errors.message ? res.errors.message : res.data.message;
							}
							catch (err) {
								msg = xhr.statusText
							}
						}
						else {
							msg = xhr.statusText;
						}

						if (!msg) {
							msg = that.funcs.msg('VR100');
						}

						if (xhr.status === 201) {
							that.messages = that.funcs.Messages('success', msg);
							that.itemFileView.nativeElement.value = '';
						} else {
							that.messages = that.funcs.Messages('danger', msg);
						}
						that.showLoadingOverlay = false;
					}
				}
			};
			xhr.send(formData);
		}
	}

	public quarter = [];
	public initQuarter() {

		this.quarter = [];
		for (var i = 1; i <= 4; i++) {
			this.quarter.push({key: i, value: i});
		}
	}

	public year = [];
	public initYear() {

		this.year = [];
		const end = this.curYear;
		const minYear = 2010;
		for (var i = end; i >= minYear; i--) {
			this.year.push({key: i, value: i});
		}
	}

}
