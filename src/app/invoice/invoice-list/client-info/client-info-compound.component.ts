
import {Component, ViewChild, Injectable, Input} from '@angular/core';
import {API_Config} from '../../../common/core/API_Config';
import {BaseClass} from '../../../common/core/classes_us/base-class';
import {BaseService} from '../../../common/core/classes_us/base-service';
import {CountryService} from '../../../master-data/country/country-service';
import {StateService} from '../../../master-data/state/state-service';

import {Alert} from '../../../common/directives/popup-modal_us/alert/alert';
import {Confirm} from '../../../common/directives/popup-modal_us/confirm/confirm';

declare var jQuery: any;

@Component({
    selector: 'client-info-compound',
    directives: [
        Confirm,
        Alert
    ],
    providers: [
        BaseService,
        CountryService,
        StateService
    ],
    templateUrl: 'client-info-compound.component.html',
})

@Injectable()

export class ClientInfoCompoundComponent extends BaseClass
{
    @ViewChild(Confirm) confirm: Confirm;
    @ViewChild(Alert) alert: Alert;

    public showLoadingOverlay = false;

    private custID;

    private billTo = {
        cus_name: '',
        bill_to_add_line_1: '',
        bill_to_add_line_2: '',
        bill_to_city: '',
        bill_to_state_id: '',
        bill_to_zip: '',
        bill_to_country_id: ''
    };

    private shipTo = {
        ship_to_add_line_1: '',
        ship_to_add_line_2: '',
        ship_to_city: '',
        ship_to_state_id: '',
        ship_to_zip: '',
        ship_to_country_id: ''
    };

    protected infoDropDowns = {
        countries: {
            service: '_countryService',
            idField: 'sys_country_id',
            nameField: 'sys_country_name'
        },
        states: {
            service: '_stateService',
            idField: 'sys_state_id',
            nameField: 'sys_state_name'
        }
    };

    @Input('cus_id') set cus_id(value: number) {
        if (typeof value !== 'undefined') {
            this.custID = value;
        }
    }

    @Input('bill_to') set bill_to(value: any) {
        if (typeof value !== 'undefined') {
            this.billTo = value;
        }
    }

    @Input('ship_to') set ship_to(value: any) {
        if (typeof value !== 'undefined') {
            this.shipTo = value;
        }
    }

    constructor (
        public _api: API_Config,
        private _baseService: BaseService,
        private _countryService: CountryService,
        private _stateService: StateService
    )
    {
        super();

        this.getInfoDropdowns();
    }

    //**************************************************************************

    public updateShippingInfo()
    {
        let errors = [],
            infoInputs = {
                ship_to_add_line_1: 'Ship Address Line 1',
                ship_to_add_line_2: 'Ship Address Line 2',
                ship_to_city: 'Ship City',
                ship_to_state_id: 'Ship State',
                ship_to_country_id: 'Ship Country',
                ship_to_zip: 'Ship Zip'
            };

        let checkInfo = ['billTo', 'shipTo'],
            ignoreValues = [
                'ship_to_add_line_2'
            ];

        for (let count=0; count<checkInfo.length; count++) {

            let info = checkInfo[count];

            for (var key in this[info]) {
                if (! ~jQuery.inArray(key, ignoreValues) && ! this[info][key]
                 && infoInputs.hasOwnProperty(key)) {

                    errors.push(infoInputs[key] + ' is a mandatory value');
                }
            }
        }

        if (errors.length) {

            let message = errors.join("\n");

            this.alert.show(message);
        } else {

            let message = 'Customer Shipping information will be updated. '
                       + 'Proceede?';

            this.confirm.show(message, {});
        }
    }

    //**************************************************************************

    public onConfirm($event)
    {
        if (! $event.response) {
            return;
        }

        this.shipTo['update'] = ['ship'];

        let url = this._api.API_CLIENT_INFO + '/update/' + this.custID,
            params = JSON.stringify(this.shipTo);

        this.showLoadingOverlay = true;

        this._baseService.post(url, params)
            .subscribe(
                data => {
                    this.alert.show('Customer data was successfully updated');
                },
                err => {},
                () => {
                    this.showLoadingOverlay = false;
                }
        );
    }

    //**************************************************************************
}
