import {Injectable,Component, ElementRef, EventEmitter} from '@angular/core';
import { ControlGroup, ControlArray, Control} from '@angular/common';
import {CTRVTableFunctions} from './ctrlv-table.functions';
import {CTRLVServices} from './ctrlv-table.services';
@Injectable()
export class CTRLVTable {

    constructor(private service:CTRLVServices,private func:CTRVTableFunctions){}

    onKeyDown(controlArray:ControlArray,event,row,__callback) {
        if(event.code=='KeyV') {
            this.service.executeOnTempElement(
                (textArea: HTMLTextAreaElement)=> {
                    textArea.focus();
                },
                (element: HTMLTextAreaElement)=> {
                    var text = this.func.dataToArray(element.value);
                    var ngControl=event.target.attributes.getNamedItem('ngControl').value;
                    this.service.pasteData(controlArray,row,ngControl,text,__callback);
                }
            );
        }
    }


}
