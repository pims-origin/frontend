
export class BaseClass
{
    public whs_id: number;

    //**************************************************************************

    constructor()
    {
        this.whs_id = parseInt(localStorage.getItem('whs_id'));
    }

    //**************************************************************************

    public get(property: string)
    {
        return this[property];
    }

    //**************************************************************************

    public set(property: string, value: any)
    {
        return this[property] = value;
    }

    //**************************************************************************

    public cloneObject(source)
    {
        let target = {};

        for (let prop in source) {
            target[prop] = source[prop];
        }

        return target;
    }

    //**************************************************************************

    public getInfoDropdowns()
    {
        let infoDropDowns = {};

        if (this.hasOwnProperty('infoDropDowns')) {
            infoDropDowns = this['infoDropDowns'];
        } else {
            return;
        }

        let amount = Object.keys(this['infoDropDowns']).length,
            count = 0;

        this['showLoadingOverlay'] = true;

        for (let dropDown in infoDropDowns) {

            let dropDownInfo = infoDropDowns[dropDown];

            this[dropDown] = [];

            this[dropDownInfo.service].getDropdown()
                .subscribe(
                    data => {
                        for (var key in data) {
                            this[dropDown].push({
                                key: data[key][dropDownInfo.idField],
                                value: data[key][dropDownInfo.nameField]
                            });
                        }
                    },
                    err => {},
                    () => {
                        if (amount == ++count) {
                            this['showLoadingOverlay'] = false;
                        }
                    }
                );
        }
    }

    //**************************************************************************

}
