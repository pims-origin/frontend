import {Component} from '@angular/core';
import {Router, RouterLink, RouteConfig, ROUTER_DIRECTIVES} from '@angular/router-deprecated';
import {CruWarehouseComponent} from "./cru-warehouse/cru-warehouse.component";
import {WarehouseListComponent} from "./warehouse-list/warehouse-list.component";



@Component ({
    selector: 'warehouse-management',
    directives: [ROUTER_DIRECTIVES],
    template: `<router-outlet></router-outlet>`,
})

@RouteConfig([
    { path: '/new', component: CruWarehouseComponent , name: 'Create Warehouse', data:{action:'new'}},
    { path: '/:id', component: CruWarehouseComponent , name: 'View Warehouse', data:{action:'view'}},
    { path: '/:id/edit', component: CruWarehouseComponent , name: 'Edit Warehouse', data:{action:'edit'}},
    { path: '/', component: WarehouseListComponent , name: 'Warehouse List', useAsDefault: true }
])
export class WarehouseManagementComponent {


}
