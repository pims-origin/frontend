import {Component} from '@angular/core';
import {Router, RouterLink, RouteConfig, ROUTER_DIRECTIVES} from '@angular/router-deprecated';

import {CruXferComponent} from "./cru-xfer/cru-xfer.component";
import {XferINVComponent} from "./xfer-inv/xfer-inv.component";
import {XFERListComponent} from "./xfer-record-list/xfer-record-list.component";
import {XFERTicketListComponent} from "./xfer-ticket-list/xfer-ticket-list.component";

@Component ({
    selector: 'xfer',
    directives: [ROUTER_DIRECTIVES],
    template: `<router-outlet></router-outlet>`,
})

@RouteConfig([
    { path: '/', component:XFERListComponent  , name: 'Xfer List', useAsDefault: true },
    { path: '/new', component:CruXferComponent  , name: 'Create Xfer', data:{action:'new'}},
    { path: '/:id', component:CruXferComponent  , name: 'View Xfer', data:{action:'view'}},
    { path: '/:id/edit', component:CruXferComponent  , name: 'Edit Xfer', data:{action:'edit'}},
    { path: '/xfer-ticket-list', component:XFERTicketListComponent  , name: 'XTicketfer List' },
    { path: '/xfer-ticket/:id', component: XferINVComponent  , name: 'XTicket View',data:{action:'view'} },
    { path: '/xfer-ticket/:id/update-inv', component: XferINVComponent  , name: 'XTicket Update', data:{action:'update'} },
])
export class XferComponent {

}
