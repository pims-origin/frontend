import { Component, Host } from '@angular/core';
import { NgFormModel } from '@angular/common';
import {ValidationService} from "./validation.service";

@Component({
    selector: 'control-messages',
    inputs: ['controlName: control'],
    template: `<div *ngIf="errorMessage !== null">{{errorMessage}}</div>`
})
export class ControlMessages {
    controlName: string;
    private params = [];
    constructor(@Host() private _formDir: NgFormModel) {

        this.params['loc_code'] = 'Location Code';
        this.params['loc_type_id'] = 'Location Type';
        this.params['loc_width'] = 'Width';
        this.params['loc_height'] = 'Height';
        this.params['loc_length'] = 'Length';
        this.params['loc_available_capacity'] = 'Available Capacity';
        this.params['loc_weight_capacity'] = 'Weight Capacity';
        this.params['loc_max_weight'] = 'Max Weight';
        this.params['loc_alternative_name'] = 'Location Name';
        this.params['loc_min_count'] = 'Min Count';
        this.params['loc_max_count'] = 'Max Count';

    }

    get errorMessage() {
        // Find the control in the Host (Parent) form
        let c = this._formDir.form.find(this.controlName);

        for (let propertyName in c.errors) {

            // If control has a error
            if (c.errors.hasOwnProperty(propertyName) && c.touched) {
                
                // Return the appropriate error message from the Validation Service
                return ValidationService.getValidatorErrorMessage(this.params[this.controlName] ,propertyName);
            }
        }

        return null;
    }
}