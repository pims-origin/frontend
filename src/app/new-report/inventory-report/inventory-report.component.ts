import {Component, NgZone} from "@angular/core";
import {Router, ROUTER_DIRECTIVES} from '@angular/router-deprecated';
import {ControlGroup, FormBuilder, Control} from '@angular/common';
import {API_Config, UserService, Functions} from '../../common/core/load';
import {WMSPagination, AgGridExtent, WMSMessages, ReportFileExporter} from '../../common/directives/directives';
import 'ag-grid-enterprise/main';
import {InventoryService} from "./inventory-report-service";
import {BoxPopupService} from "../../common/popup/box-popup.service";

declare const $:any;

@Component({
	selector: 'performance-report',
	providers: [InventoryService, BoxPopupService],
	directives: [ROUTER_DIRECTIVES, AgGridExtent, WMSMessages, WMSPagination, ReportFileExporter],
	templateUrl: 'inventory-report.component.html',

})
export class InventoryReportComponent {

	private headerURL = `${this.apiService.API_User_Metas}/ivr`;
	private columnData = {
		cus_name: {
			title: 'Customer',
			pin: true
		},
		item_id: {
			title: 'Item ID',
			pin: true
		},
		sku: {
			title: 'SKU',
			id: true,
			ver_table: '8',
			pin: true
		},
		size: {
			title: 'Size',
			pin: true
		},
		color: {
			title: 'Color',
			pin: true
		},
		// lot: {
		// 	title: 'Lot',
		// 	pin: true
		// },
		upc: {
			title: 'UPC'
		},
		avail: {
			title: 'Available QTY'
		},
		// lock_qty: {
		// 	title: 'Locked Qty'
		// },
		// allocated_qty: {
		// 	title: 'Allocated QTY'
		// },
		picked_qty: {
			title: 'Picked QTY'
		},
		// dmg_qty: {
		// 	title: 'Damaged QTY'
		// },
		ttl: {
			title: 'Total QTY'
		},
		pack: {
			title: 'Pack'
		},
		description: {
			title: 'Description'
		},
		ctns: {
			title: 'Total CTNS'
		}
	};

	private Pagination;
	private perPage = 30;
	private whs_id;
	private messages;
	searchForm:ControlGroup;
	private listActivity:Array<any> = [];
	private showLoadingOverlay:boolean = false;
	private customers:Array<any> = [];
	private currentSKU:any = {};
	private isLoadingSKUs:boolean = false;
	private SKUs:Array<string> = [];

	// Check permission for user using this function page
	private canAccess:boolean = false;

	// Export csv file
	private exportAPIUrl:string = '';
	private fileName:string = 'Inventory-report.csv';

	constructor(private formBuilder:FormBuilder,
	            private eventService:InventoryService,
	            private _Func:Functions,
	            private _user:UserService,
	            private _router:Router,
	            private apiService:API_Config,
	            public lc:NgZone) {
		this.whs_id = localStorage.getItem('whs_id');
		this.buildSearchForm();
		this.checkPermission();
		this.initializeCurSKU();

	}

	buildSearchForm() {
		this.searchForm = this.formBuilder.group({
			cus_id: [''],
			sku: ['']
		});
	}

	initializeCurSKU() {
		this.currentSKU = {size: '', color: '', lot: ''};
	}

	private checkPermission() {
		this.showLoadingOverlay = true;
		this._user.GetPermissionUser().subscribe(
			data => {
				this.showLoadingOverlay = false;
				this.canAccess = this._user.RequestPermission(data, 'viewPerformanceActivity');
				if (this.canAccess) {
					this.getCustomers();
					this.getListActivity();
				}
				else {
					this._router.parent.navigateByUrl('/deny');
				}
			},
			err => {
				this.messages = this._Func.Messages('danger', this._Func.parseErrorMessageFromServer(err));
				this.showLoadingOverlay = false;
			}
		);
	}

	private onPageSizeChanged($event) {
		this.perPage = $event.target.value;
		if (this.Pagination) {
			if ((this.Pagination['current_page'] - 1) * this.perPage >= this.Pagination['total']) {
				this.Pagination['current_page'] = 1;
			}
			this.getListActivity(this.Pagination['current_page']);
		}

	}


	private getListActivity(page:number = 1) {
		// this.searchForm.value['lot'] = this.currentSKU['lot'];
		this.searchForm.value['page'] = page;
		this.searchForm.value['limit'] = this.perPage;
		this.showLoadingOverlay = true;
		this.exportAPIUrl = this.eventService.getReportAPIUrl(`?` + $.param(this.searchForm.value));

		this.eventService.getListActivity(`?` + $.param(this.searchForm.value)).subscribe(
			data => {
				this.showLoadingOverlay = false;
				this.listActivity = data.data;

				//paging function
				this.Pagination = data['meta']['pagination'];
				this.Pagination['numLinks'] = 3;
				this.Pagination['tmpPageCount'] = this._Func.Pagination(this.Pagination);
			},
			err => {
				this.parseError(err);
				this.showLoadingOverlay = false;
			},
			() => {}
		);

	}

	getCustomers() {
		// const params = `/${this.whs_id}?limit=100`;
		const params = `?limit=100`;
		this.eventService.getCustomersByUser(params).subscribe(
			data => {
				this.customers = data.data;
			},
			err => {
				this.parseError(err);
			},
			() => {}
		);
	}

	loadSKUs($event:any) {
		const cusId = this.searchForm.value['cus_id'];
		const skuKeySearch = this.searchForm.value['sku'].trim();
		const params = `/${this.whs_id}?cus_id=${cusId}&sku=${skuKeySearch}&limit=20`;
		const enCodeSearchQuery = this._Func.FixEncodeURI(params);

		this.initializeCurSKU();
		if (!skuKeySearch) {
			return;
		}
		this.isLoadingSKUs = true;
		this.eventService.getSKUs(enCodeSearchQuery).debounceTime(400).distinctUntilChanged().subscribe(
			data => {
				this.isLoadingSKUs = false;
				this.SKUs = data['data'];
			},
			err => {
				this.isLoadingSKUs = false;
				this.messages = this._Func.Messages('danger', err.json().errors.message);
			},
			() => {
			}
		);
	}

	selectSKU($event, sku) {
		this.currentSKU = sku;
		(<Control>this.searchForm.controls['sku']).updateValue(sku.sku);
	}

	resetSearchForm() {
		(<Control>this.searchForm.controls['cus_id']).updateValue('');
		(<Control>this.searchForm.controls['sku']).updateValue('');
	}

	reset() {
		this.resetSearchForm();
		this.initializeCurSKU();
		this.getListActivity();
	}

	search() {
		this.getListActivity();
	}

	private updateMessages(messages) {
		this.messages = messages;
	}

	private parseError(err) {
		err = err.json();
		this.messages = this._Func.Messages('danger', err.errors.message);
	}
}
