import {Component} from '@angular/core';
import {Router, RouterLink, RouteConfig, ROUTER_DIRECTIVES} from '@angular/router-deprecated';
import {ViewGoodsReceiptComponent} from "./view-goods-receipt/view-goods-receipt.component";
import {EditGoodsReceiptComponent} from "./edit-goods-receipt/edit-goods-receipt.component";
import {CreateGoodsReceiptComponent} from "./create-goods-receipt/create-goods-receipt.component";
import {GoodsReceiptListComponent} from "./goods-receipt-list/goods-receipt-list.component";
import {DamagedCartonComponent} from "./damaged-carton/damaged-carton.component";
import {PalletAssignmentComponent} from "./pallet-assignment/pallet-assignment.component";
import {AssignCrossDockComponent} from "./assign-cross-dock/assign-cross-dock";

@Component ({
    selector: 'goods-receipt',
    directives: [ROUTER_DIRECTIVES],
    template: `<router-outlet></router-outlet>`,
})

@RouteConfig([
    { path: '/', component:GoodsReceiptListComponent  , name: 'Goods Receipt List', useAsDefault: true , data : {titlePage:'Goods Receipt List'}},
    { path: '/:id', component:ViewGoodsReceiptComponent  , name: 'View Goods Receipt'},
    // { path: '/:id/edit', component:EditGoodsReceiptComponent  , name: 'Edit Goods Receipt'},
    { path: '/:id/:ctn_id/new', component:CreateGoodsReceiptComponent  , name: 'Create Goods Receipt'},
    { path: '/:id/damaged-carton', component: DamagedCartonComponent  , name: 'Damaged Cartons' , data : {titlePage:'Damaged Cartons'}},
    { path: '/:id/pallet-assignment', component: PalletAssignmentComponent  , name: 'Pallet Assignment'},
    { path: '/:id/assign-x-dock', component: AssignCrossDockComponent  , name: 'Assign Cross Dock'},

])
export class GoodsReceiptComponent {

}
