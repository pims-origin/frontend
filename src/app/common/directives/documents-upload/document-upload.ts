import {Component,Input, Injectable} from '@angular/core';
import {API_Config, Functions} from '../../../common/core/load';
import {DEFAULT_TEMPLATE, DEFAULT_STYLES} from './template';
@Component({
  selector: 'documents-upload',
  template: DEFAULT_TEMPLATE,
  styles:[DEFAULT_STYLES]
})
@Injectable()
export class DocumentUpload {

  private _doc_type;
  private _trans;
  private uploadDocumentData={};
  private _attValue1;
  private _attValue2;
  private _attTitle1;
  private _attTitle2;

  @Input('doc_type') set doc_type(value: string) {
    this._doc_type=value;
  }

  @Input('attValue1') set attValue1(value: string) {
    this._attValue1=value;
  }

  @Input('attValue2') set attValue2(value: string) {
    this._attValue2=value;
  }

  @Input('attTitle1') set attTitle1(value: string) {
    this._attTitle1=value;
  }

  @Input('attTitle2') set attTitle2(value: string) {
    this._attTitle2=value;
  }

  @Input('trans') set trans(value: string) {
    if(value){
      this.getUploadDoccument(this._doc_type,value,this._attTitle1,this._attValue1,this._attTitle2,this._attValue2);
    }
  }

  constructor(
    private _Func: Functions,
    private _API: API_Config){

  }

  getDocumentUploadAPIString()
  {
    return this._API.DOCUMENT_API_UPLOAD;
  }

  getUploadDoccument(doc_type,trans,attTitle1,attValue1,attTitle2,attValue2) {

    var that = this;
    this.uploadDocumentData['completed']=true;
    let apiString = that.getDocumentUploadAPIString()+'&docType='+doc_type+'&trans='+ trans +'&jwt=' + that._Func.getToken();
    apiString+='&attTitle1='+attTitle1+'&attValue1='+attValue1+'&attTitle2='+attTitle2+'&attValue2='+attValue2;
    that.uploadDocumentData['api_string']=apiString;
    try{
      let xhr = new XMLHttpRequest();
      xhr.open("GET", apiString , true);
      xhr.responseType = 'blob';
      xhr.onreadystatechange = function () {
        if(xhr.readyState == 2) {
          if(xhr.status == 200) {
            that.uploadDocumentData['completed']=true;
          } else {
            xhr.responseType = "text";
            that.uploadDocumentData['completed']=false;
            that.uploadDocumentData['messages']= xhr.statusText;
          }
        }
      };
      xhr.send();
    }catch (err){
      that.uploadDocumentData['messages']=that._Func.msg('VR100');
    }

  }


}
