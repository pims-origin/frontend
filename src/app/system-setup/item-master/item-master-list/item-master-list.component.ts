import {Component} from '@angular/core';
import {RouteParams,Router ,RouteData} from '@angular/router-deprecated';
import {ItemMasterServices} from '../item-master-service';
import { FORM_DIRECTIVES,ControlGroup,FormBuilder, Control, Validators} from '@angular/common';
import {Functions, API_Config} from '../../../common/core/load';
import { CountryService } from '../../../master-data/country/country-service';
import { Http } from '@angular/http';
import {WMSPagination, AdvanceTable,WMSBreadcrumb, ReportFileExporter} from '../../../common/directives/directives';
import {UserService} from "../../../common/users/users.service";
import { ValidationService } from '../../../common/core/validator';
import { WarningBoxPopup, BoxPopupService } from '../../../common/popup';
declare var jQuery: any;
declare var saveAs: any;
declare var swal: any;
@Component ({
  selector: 'item-master-list',
  directives:[FORM_DIRECTIVES, AdvanceTable,WMSBreadcrumb, ReportFileExporter],
  providers: [ItemMasterServices,CountryService,FormBuilder,UserService, BoxPopupService],
  templateUrl: 'item-master-list.component.html',
  styles: [' .autocomplete li { text-transform: none; } ']
})

export class ItemMasterListComponent {

  private tableID = 'item-master-list';
  private headerURL = this._API_Config.API_User_Metas + '/itm';
  private headerDef = [{id: 'ver_table', value: 6},
                    {id: 'ck', name: '', width: 25},
                    {id: 'status', name: 'Status', width: 100},
                    {id: 'item_id', name: 'Item ID', width: 70},
                    {id: 'customer', name: 'Customer', width: 150, unsortable: true},
                    {id: 'sku', name: 'SKU', width: 130},
                    {id: 'size', name: 'Size', width: 130},
                    {id: 'color', name: 'Color', width: 80},
                    {id: 'pack', name: 'Packsize', width: 100},
                    {id: 'uom_code', name: 'UOM Code', width: 100},
                    {id: 'length', name: 'Length', width: 100},
                    {id: 'width', name: 'Width', width: 100},
                    {id: 'height', name: 'Height', width: 100},
                    {id: 'cus_upc', name: 'UPC', width: 100},
                    {id: 'description', name: 'Description', width: 100}];
  private pinCols = 3;
  private rowData: any[];
  private getSelectedRow;
  private objSort = {
    sort_field: 'item_id',
    sort_type: 'desc'
  };

  arrStatus = [
    { code: 'RG', name: 'Recv-Waitapp' },
    { code: 'AC', name: 'Recv-Approved' }
  ];

  private ItemMasterList:Array<any>=[];
  private autoSearchData:Array<any>=[];
  private Customers=[];
  private ListPermission=[];
  private Pagination = [];
  public Loading=[];
  private errMessages=[];
  private totalCount = 0;
  private tmpPageCount;
  private pageCount=0;
  private ItemOnPage=0;
  private currentPage = 1;
  private perPage = 20;
  private numLinks = 3;
  private start = 1;
  private end = 1;
  private Country=[];
  private State:Array<any>=[];
  public ListSelectedItem:Array<any>=[];
  public selectedAll:boolean=false;
  private messages;
  private showLoadingOverlay = false;
  private showAutocompleteLoading = false;
  private isSearch:boolean=false;
  private showErrorPrintASN = false;
  SearchForm: ControlGroup;
  public PrintASNForm: ControlGroup;
  public currentSKUToPrint = '';

  // Export file variables
  private fileName:string = 'Item-Master.xlsx';
  private exportAPIUrl:string = '';

  constructor(
    private _http: Http,
    private _Func: Functions,
    private _API_Config: API_Config,
    private _CT:CountryService,
    private _user:UserService,
    private fb: FormBuilder,
    private _IMService: ItemMasterServices,
    private _boxPopupService:BoxPopupService,
    private _Valid: ValidationService,
    private _router:Router)
  {
    this.buildFormImport();
    this.checkPermission();
  }

  FormBuilder()
  {
    this.SearchForm =this.fb.group({
      item_id: [''],
      cus_id: [''],
      sku: [''],
      size: [''],
      color: [''],
        description: [''],
    });
  }

  private buildFormASN() {
    this.PrintASNForm =this.fb.group({
      num_of_print: [1, Validators.compose([Validators.required, this._Valid.isZero,this._Valid.inPutOnlyNumber])],
    });
  }

  private createItem;
  private viewItem;
  private editItem;
  private allowAccess:boolean=false;
  private checkPermission() {
    this.showLoadingOverlay = true;
    this._user.GetPermissionUser().subscribe(
      data => {
        this.createItem = this._user.RequestPermission(data, 'createItem');
        this.viewItem = this._user.RequestPermission(data,'viewItem');
        this.editItem=this._user.RequestPermission(data,'editItem');
        /* Check orther permission if View allow */
        if(!this.viewItem) {
          this._router.parent.navigateByUrl('/deny');
        }
        else {
          // pass
          this.allowAccess=true;
          this.FormBuilder();
          this.buildFormASN();
          this.GetItemMasterList();
          this.GetListCustomer();
        }
      },
      err => {
        this.messages=this._Func.Messages('danger', this._Func.parseErrorMessageFromServer(err));
        this.showLoadingOverlay = false;
      }
    );
  }

  private GetItemMasterList(page=null)
  {
    this.showLoadingOverlay = true;
    let param="?page="+page+"&limit="+this.perPage;    
    if(this.isSearch){
      param=param+this.searchParam;
      this.Loading['searching']=true;
    }
    if(this.objSort && this.objSort['sort_type'] != 'none') {
      param += '&sort['+ this.objSort['sort_field']+ ']='+ this.objSort['sort_type'];
    }

    this.exportAPIUrl = `${this._API_Config.API_ItemMaster_Exporter}${param}`;

    this._IMService.GetItemMasterList(param).subscribe(
      data => {
        this.ItemMasterList = data.data;
        if(this.ItemMasterList.length === 0&&this.SearchForm.value.description) {
          let warningPopup = new WarningBoxPopup();
          warningPopup.text = this._Func.msg('ITM001');
          let that = this;
          this._boxPopupService.showWarningPopup(warningPopup)
              .then(function (dm) {
                  if(dm) {
                    localStorage.removeItem('add-item-master');
                    localStorage.setItem('add-item-master', JSON.stringify(that.SearchForm.value));
                    that._router.parent.navigateByUrl('/system-setup/item-master/new');
                  }
                  else{
                  }
              });
        }
        this.Loading['searching']=false;
        //pagin function
        this.Pagination=data['meta']['pagination'];
        this.Pagination['numLinks']=3;
        this.Pagination['tmpPageCount']=this._Func.Pagination(this.Pagination);
        this.createRowData(data);
        this.showLoadingOverlay = false;
      },
      err => {
        this.Loading['searching']=false;
        this.showLoadingOverlay = false;
      },
       () => {}
    );
  }

  private createRowData(data) {
        var rowData: any[] = [];
        if (typeof data.data != 'undefined') {

          data = this._Func.formatData(data.data);

          for (var i = 0; i < data.length; i++) {
            rowData.push({
              item_id: '<a href="#/system-setup/item-master/' + data[i].item_id + '">'+ data[i].item_id+'</a>',
              status: this._IMService.getNameByCode(data[i].status, this.arrStatus),
              customer: data[i].customer_name,
              sku: data[i].sku,
              size: data[i].size,
              color: data[i].color,
              pack: data[i].pack,
              uom_code: data[i].uom_code,
              length: data[i].length,
              width: data[i].width,
              height: data[i].height,
              cus_upc:data[i].cus_upc,
              description: data[i].description,
              itm_id: data[i].item_id
            })
          }
          this.rowData = rowData;
        }

  }

  // get list custommer
  GetListCustomer()
  {
    // this.Customers=this._IMService.ListCustomers();
    var params ="?limit=10000";
    this._IMService.getListCustomersByUser(params)
      .subscribe(
          data => {
              this.Customers = data.data;
          },
          err => {},
          () => {}
      );
  }

  private onPageSizeChanged($event)
  {
    this.selectedAll=false;
    this.perPage = $event.target.value;
    if((this.Pagination['current_page'] - 1) * this.perPage >= this.Pagination['total']) {
        this.Pagination['current_page'] = 1;
    }
    this.GetItemMasterList(this.currentPage);

  }

  private filterList(num)
  {
    this.selectedAll=false;
    this.GetItemMasterList(num);
  }
  private getPage(num)
  {
    let arr=new Array(num);
    return arr;
  }

  /*
   * Router edit
   * */
  Edit() {
    this.getSelectedRow = 'edit';
    // console.log('your choose ', this.ListSelectedItem.length);
    // if (this.ListSelectedItem.length > 1) {
    //   this.messages = this._Func.Messages('danger','Please choose only one item to edit');
    // }
    // else {
    //   if(this.ListSelectedItem.length)
    //   {
    //     console.log(this.ListSelectedItem[0]);
    //     this._router.navigateByUrl('/system-setup/item-master/' + this.ListSelectedItem[0] + '/edit');
    //   }
    //   else{
    //     this.messages = this._Func.Messages('danger','Please choose one item to edit');
    //   }
    // }
  }

  private printSKU() {
    this.getSelectedRow = 'printSKU';
  }

  afterGetSelectedRow($event) {
    this.ListSelectedItem = $event.data;
    switch($event.action) {
      case 'edit':
        if (this.ListSelectedItem.length > 1) {
          this.messages = this._Func.Messages('danger','Please choose only one item to edit');
        }
        else {
          if(this.ListSelectedItem.length)
          {
            this._router.navigateByUrl('/system-setup/item-master/' + this.ListSelectedItem[0].itm_id + '/edit');
          }
          else{
            this.messages = this._Func.Messages('danger','Please choose one item to edit');
          }
        }
        break;
      case 'printSKU':
        if (this.ListSelectedItem.length > 1) {
          this.messages = this._Func.Messages('danger','Please choose only one item to print');
        }
        else {
          if(this.ListSelectedItem.length) {
            this.showErrorPrintASN = false;
            this.currentSKUToPrint = this.ListSelectedItem[0]['sku'];
            jQuery('#print-sku-upc-barcode').modal('show');
          }
          else{
            this.messages = this._Func.Messages('danger','Please choose one item to print');
          }
        }
        break;
    }
    this.getSelectedRow = false;
  }

  private doSort(objSort) {
    this.objSort = objSort;
    this.GetItemMasterList(this.Pagination['current_page']);
  }

  /*
   * Get Country
   * */
  GetCountry()
  {
    this.Loading['getCountry']=true;
    this._CT.Country().subscribe(
      data=>{
        this.Loading['getCountry']=false;
        this.Country=data.data;
      },
      err=>{
        this.Loading['getCountry']=true;
        this.errMessages.push('Could not load country.');
      }
    );
  }


  private Delete()
  {
    if(!this.ListSelectedItem.length)
    {
      // if no select any item to delete
      return;
    }
    if(confirm(this._Func.msg('PBAP002'))&&this.ListSelectedItem.length) {
      // remove item local
      this.RemoveItemLocal();

      this.ListSelectedItem.forEach((item)=> {
        // the fisrt , remove item in list data

        this._IMService.DeleteItemMaster(item).subscribe(
          data => {
            this.selectedAll = false;

            // reload data new
            if (this.ItemMasterList.length <10) {
              this.GetItemMasterList(this.Pagination['current_page']);
            }

          },
          err => {},
          () => {}
        );

      });

      // set empty selected
      this.ListSelectedItem = [];
    }

  }

  RemoveItemLocal()
  {
    let sumItemDelet=0;

    this.ListSelectedItem.forEach((item)=>{

      for(let el of this.ItemMasterList) {
        let els=parseInt(el);
        if (item == els['item_id']) {
          let index = this.ItemMasterList.indexOf(item);
          // remove item out of array
          this.ItemMasterList.splice(index, 1);
          sumItemDelet++;
        }
        else{
        }
      }

    });


  }

  private searchParam;
  Search():void
  {
    let data=this.SearchForm.value;
    this.messages = null;
    this.ListSelectedItem=[];
    this.isSearch=true;
    this.searchParam="&item_id="+encodeURIComponent(data['item_id'])+"&sku="+encodeURIComponent(data['sku'])+"&cus_id="
        +encodeURIComponent(data['cus_id']) +"&size="+encodeURIComponent(data['size'])+"&color="+encodeURIComponent(data['color'])+"&description="+encodeURIComponent(data['description']);
    this.GetItemMasterList();
  }

  ResetSearch()
  {
    this.isSearch=false;
    this._Func.ResetForm(this.SearchForm);
    this.GetItemMasterList(this.currentPage);
  }

  private expandTable = false;
  private viewListFullScreen() {
    this.expandTable = true;
    setTimeout(() => {
      this.expandTable = false;
    }, 500);
  }

  importForm: ControlGroup;
  private showError = false;
  private filesToUpload = [];

  private showMessage(msgStatus, msg) {
      this.messages = {
          'status': msgStatus,
          'txt': msg
      }
  }

  buildFormImport() {
    this.importForm =this.fb.group({
      customer: ['', Validators.required]
    });
  }

  fileChangeEvent(fileInput: any){
    this.filesToUpload = <Array<File>> fileInput.target.files;
  }

  importFile(data) {
    var that = this;
    this.showError = true;
    this.messages = null;
    if(this.importForm.valid) {
      var formData: any = new FormData();
      var xhr = new XMLHttpRequest();
      var files = this.filesToUpload;

      for(var i = 0; i < files.length; i++) {

          var fileExtension = files[i].name.substr(files[i].name.length - 5);

          if (fileExtension != ".xlsx") {

              this.messages = this._Func.Messages('danger', this._Func.msg('CF003'));
              return false;
          }

          formData.append("file", files[i], files[i].name);
      }
      formData.append("file_type", 'xlsx');

      formData.append('cus_id', data.customer);

      this.showLoadingOverlay = true;
      xhr.open("POST", this._API_Config.API_ItemMaster + '/import', true);
      xhr.setRequestHeader("Authorization", 'Bearer ' +this._Func.getToken());
      xhr.onreadystatechange = function () {
          if (xhr.readyState == 4) {
              if (xhr.status == 200) {
                  var data = JSON.parse(xhr.response).data;
                  that.showMessage('success', `${data.processed}/${data.total} items have been imported successfully`);
                  that.showLoadingOverlay = false;
                  jQuery("#importFile").val("");
                  that.GetItemMasterList();
              } else {
                  var error = xhr.response;
                  try {
                    error = JSON.parse(error);
                    var errMsg = error.errors?error.errors.message : '';
                    if(error.data && error.data.code == '422') {
                        that.showMessage('danger', error.data.message);
                    }
                    else {
                      if(errMsg) {
                          that.showMessage('danger', errMsg);
                      }
                    }
                  }
                  catch(err) {
                  }
                  that.showLoadingOverlay = false;
              }
          }
      };
      xhr.send(formData);
    }
  }

  private updateMessages(messages) {
    this.messages = messages;
  }

  public autoCompleteDesc(keyword) {
    this.autoSearchData = [];
    this.showAutocompleteLoading = true;
    if (keyword) {
      this._IMService.GetItemMasterList('?limit=20&description=' + keyword).debounceTime(400).distinctUntilChanged().subscribe(
        data => {
          this.autoSearchData = data.data;
        },
        err => {
          this.parseError(err);
        },
        () => {
        this.showAutocompleteLoading = false;
        }
      );
    }
  }

  public selectDesc(desc) {
    (<Control>this.SearchForm.controls['description']).updateValue(desc);
  }

  private parseError(err) {
      this.messages = {'status': 'danger', 'txt': this._Func.parseErrorMessageFromServer(err)};
  }

  public checkInputNumber(event) {
    this._Valid.isNumber(event);
  }

  public printASNForm() {
    var that = this;
    this.showErrorPrintASN = true;
    if(this.PrintASNForm.valid) {
      let api = that._API_Config.API_ITEM_MASTER + `/print-sku?sku=${that.currentSKUToPrint}&num_to_print=${(<Control>that.PrintASNForm.controls['num_of_print']).value}`;
      try{
        that.showLoadingOverlay = true;
        let xhr = new XMLHttpRequest();
        xhr.open("GET", api , true);
        xhr.setRequestHeader("Authorization", 'Bearer ' + that._Func.getToken());
        xhr.responseType = 'blob';

        xhr.onreadystatechange = function () {
          if(xhr.readyState == 2) {
            if(xhr.status == 200) {
              xhr.responseType = "blob";
            } else {
              xhr.responseType = "text";
            }
          }

          if(xhr.readyState === 4) {
            if(xhr.status === 200) {
              var fileName = `${that.currentSKUToPrint}-UPC-barcode.pdf`;
              var blob = new Blob([this.response], {type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'});
              saveAs.saveAs(blob, fileName + '.pdf');
            }else{
              let errMsg = '';
              if(xhr.response) {
                try {
                  var res = JSON.parse(xhr.response);
                  errMsg = res.errors && res.errors.message ? res.errors.message : xhr.statusText;
                }
                catch(err){errMsg = xhr.statusText}
              }
              else {
                errMsg = xhr.statusText;
              }
              if(!errMsg) {
                errMsg = that._Func.msg('VR100');
              }
              that.showMessage('danger', errMsg);
            }
            jQuery('#print-sku-upc-barcode').modal('hide');
            that.showLoadingOverlay = false;
          }
        }
        xhr.send();
      }catch (err){
        that.showMessage('danger', that._Func.msg('VR100'));
        that.showLoadingOverlay = false;
      }
    }
  }

}
