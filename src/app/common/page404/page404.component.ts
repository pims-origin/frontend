import {Component} from '@angular/core';

@Component({
    selector: 'page-not-found',
    templateUrl: 'page404.component.html',
})
export class PageNotFoundComponent {}
