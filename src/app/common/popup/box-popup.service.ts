import {JwtHelper} from "angular2-jwt/angular2-jwt";
import {Injectable} from '@angular/core';
import {Component} from '@angular/core';
import {BoxPopup} from './box-popup';
import {WarningBoxPopup} from './warning-box-popup';
import { Router, RouterLink, ROUTER_DIRECTIVES, RouteParams } from '@angular/router-deprecated';
declare var swal: any;

@Injectable()
export class BoxPopupService {
    warningBoxPopup:WarningBoxPopup;

    constructor(
        private _router: Router) {
    }

    generatePopup(options) {
        return swal(options);
    }

    showWarningPopup(warningBoxPopup: WarningBoxPopup = null) {
        if(warningBoxPopup == null)
            warningBoxPopup = new WarningBoxPopup();
        
        this.warningBoxPopup = warningBoxPopup;
        return this.generatePopup(this.warningBoxPopup);

    }

    showWarningRemoveItemPopup(warningBoxPopup: WarningBoxPopup = null) {
        if(warningBoxPopup == null)
            warningBoxPopup = new WarningBoxPopup({ text: 'Are you sure you want to remove this item?'});

        this.warningBoxPopup = warningBoxPopup;
        return this.generatePopup(this.warningBoxPopup);

    }
    
    showAuthenPopup(message) {
        return swal({
            title: message,
            type: 'warning',
            showCancelButton: false,
            showConfirmButton: true,
            // confirmButtonColor: '#3085d6',
            confirmButtonText: 'Login Again',
            confirmButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then((agree) => {
            this._router.parent.navigateByUrl('/login');
        });  
    }
}
