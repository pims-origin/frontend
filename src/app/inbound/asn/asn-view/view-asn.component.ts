import { Component } from '@angular/core';
import { ControlGroup, FormBuilder, Control, Validators } from '@angular/common';
import { Router, RouteParams } from '@angular/router-deprecated';
import 'ag-grid-enterprise/main';
import 'rxjs/add/operator/map';
import { API_Config, UserService, Functions, FormBuilderFunctions } from '../../../common/core/load';
import { ASN_Services } from "../asn-services";
import { CountItemPipe } from "../countItem.pipe";
import {
    WMSBreadcrumb, PaginatePipe, WMSMessages, WMSPagination, PaginationControlsCmp, DocumentUpload, DmsIn,
    PaginationService, ReportFileExporter
} from '../../../common/directives/directives';
import { GoBackComponent } from "../../../common/component/goBack.component";
import { PrintCartonBarcode } from "../../../common/directives/directives";
declare var jQuery: any;
declare var saveAs: any;

@Component({
    selector: 'view-asn',
    pipes: [CountItemPipe, PaginatePipe],
    templateUrl: 'view-asn.component.html',
    providers: [ASN_Services, PaginationService],
    directives: [
        GoBackComponent, WMSMessages, DocumentUpload, WMSPagination, PaginationControlsCmp, WMSBreadcrumb, ReportFileExporter,
        PrintCartonBarcode, DmsIn
    ],
})

export class ASNViewComponent {
    private id: any;
    private ctn_id: any;
    private dataListContainersByASN = [];
    private messages:any;
    private dataGoodReceipt = [];
    private dataAsn = [];
    private timeoutHideMessage;
    private hasCreateBtn:any;
    public createGROnWeb = false;
    public completeRG = false;
    public revertToRG = false;
    private flagBtnCreateGR = false;
    private dataEventTracking = [];
    private ckLoadDocument =  {'flag': false, 'message': ''};
    private token = '';
    private perPage=20;
    private Pagination;
    private currentPage = 1;
    private numLinks = 3;
    private showLoadingOverlay = false;
    private loggerUser = {};
    private containerList:Array<any>=[];

    viewASNForm: ControlGroup;
    submitForm = false;

    // Export csv file
    public exportAPIUrl:string = '';
    public fileName:string = 'Receiving-Slip.csv';
    public whsId = '';

    constructor (
        private _router: Router,
        private _Func: Functions,
        private fbdFunc: FormBuilderFunctions,
        private fb: FormBuilder,
        private _asnServices: ASN_Services,
        params: RouteParams,
        private _user: UserService,
        private _api_Config: API_Config
    ) {

        // Check permission
        this.checkPermission();
        this.id = params.get('id');
        this.ctn_id = params.get('ctn_id');
        this.getAsnByIdCtnId(this.ctn_id);
        this.getContainersByASN();
        this.chkExportCsv();
        this.token = this._Func.getToken();
        //by Nhan
        this.getLoggedUser();
        //end by Nhan
        this.whsId = localStorage.getItem('whs_id');
    }

    // Check Permission
    private checkPermission() {

        this._user.GetPermissionUser().subscribe(

            data => {

                this.hasCreateBtn = this._user.RequestPermission(data, 'createGoodsReceipt');
                this.createGROnWeb = this._user.RequestPermission(data, 'createGoodReceiptOnWeb');
                this.completeRG = this._user.RequestPermission(data, 'completeReceiving');
                this.revertToRG = this._user.RequestPermission(data, 'revertToReceiving');
                var flag= this._user.RequestPermission(data,'viewASN');

                if(!flag) {
                    this._router.parent.navigateByUrl('/deny');
                }
            },
            err => {

                this.showMessage('danger', this._Func.parseErrorMessageFromServer(err));

            }
        );
    }

    private showMessage(msgStatus, msg) {

        this.messages = {
            'status': msgStatus,
            'txt': msg
        };
        jQuery(window).scrollTop(0);
    }

    private requestPermission(permission, permission_req) {

        if (permission && permission.length) {

            for (var per of permission) {

                if (per.name == permission_req) {

                    return true;
                }
            }

        }

        return false;

    }

    /**
     * Get actual status item
     *
     * @param item
     */
    getActualStatusItem(item: any) {
        let actualStatus = '';

        if (Number(item['dtl_gr_dtl_act_ctn_ttl']) === 0) {
            actualStatus = 'New';
        } else {
            if (item['dtl_item_status_code'].toUpperCase() === 'RG') {
                actualStatus = item['dtl_item_status'];
            } else {
                actualStatus = this.dataAsn['asn_sts_name'];
            }
        }

        return actualStatus;
    }

    // Get Asn by ID
    private getAsnByIdCtnId (ctn_id) {

        this._asnServices.getAsnByIdCtnId(this.id, ctn_id)
            .subscribe(
                data => {

                    this.dataAsn = data.data;

                    this.FormBuilder(this.dataAsn);

                    this.ckCreatedGR(this.dataAsn['asn_details']);

                    this.chkExportCsv();
                },
                err => {

                    this.parseError(err);
                },
                () => {

                    this.getEventTrackingByAsn();
                    //this.ckDoccument();
                }
            );

    }

    // Get Asn by ID
    private getEventTrackingByAsn (page = 1) {

        var param = "?owner=" + this.dataAsn['asn_hdr_num'] + '&type=ib';

        param += "&page=" + page + "&limit=" + this.perPage;
        this.showLoadingOverlay=true;

        // reset Pagination
        this.Pagination={};

        try {

            this._asnServices.getEventTrackingByAsn(param)
                .subscribe(
                    data => {

                        this.dataEventTracking = data.data;

                        this.Pagination = data['meta']['pagination'];
                        this.Pagination['numLinks'] = 3;
                        this.Pagination['tmpPageCount'] = this._Func.Pagination(this.Pagination);
                        this.showLoadingOverlay=false;
                    },
                    err => {
                        this.showLoadingOverlay=false;
                        this.parseError(err);
                    },
                    () => {}
                );

        } catch (err) {

            this.messages = this._Func.Messages('danger', this._Func.msg('VR100'));
        }

    }

    // Check have create GR
    private ckCreatedGR($dataAsn) {

        this.flagBtnCreateGR = false;

        var tmp = '';
        for (var i = 0; i < $dataAsn.length; i++) {

            if ($dataAsn[i].asn_dtl_sts.toLowerCase() == 'receiving') {

                tmp = 're';
                break;
            }
            if ($dataAsn[i].asn_dtl_sts.toLowerCase() == 'new') {

                tmp = 'new';
            }

            if ($dataAsn[i].created_from.toLowerCase() == 'wap' || $dataAsn[i].created_from.toLowerCase() == 'gun') {

                tmp = 're';
                break;
            }
        }
        if (tmp == 're') {
            this.flagBtnCreateGR = false;
        } else if (tmp == 'new') {
            this.flagBtnCreateGR = true;
        }

    }

    // Check export Receiving Slip
    chkExportCsv() {
        if (this.dataAsn['asn_sts'] && this.dataAsn['asn_sts'].toLowerCase() === 're') {
            this.fileName = 'Receiving-Slip-' + this.dataAsn['asn_hdr_num'] +'.csv';
            this.exportAPIUrl = `${this._api_Config.API_REPORTS}/asn/receiving-slip/${this.whsId}/asn/${this.dataAsn['asn_hdr_id']}`;
        }
    }

    private createGoodsReceipt() {

        var ctn_id = jQuery("#t_ctnr_id").val();
        this._router.parent.navigateByUrl('/goods-receipt/' + this.id + '/' + ctn_id + '/new');
        //this.saveGoodReceipt();
    }

    // Change ctnr
    private changeCtnr ($event) {

        var ctn_id = $event.target.value;
        this.ctn_id=ctn_id;
        this.getAsnByIdCtnId(ctn_id);

    }

    private setFristTabActive(){
        jQuery(".nav-tabs li a:first").tab('show');
    }

    // Get containers
    private getContainersByASN () {

        this._asnServices.getContainersByASN(this.id)
            .subscribe(
                data => {

                    this.dataListContainersByASN = data.data;
                    this.renderPrintANS();
                    //export container when print by Nhan Truong
                    this.dataAsn['container'] = '';
                    for(let i = 0; i < this.dataListContainersByASN.length; i++)
                    {
                        this.dataAsn['container'] += this.dataListContainersByASN[i]['ctnr_num'] + ', ';

                    }
                    this.dataAsn['container'] = this.dataAsn['container'].substring(0, this.dataAsn['container'].length - 2);

                },
                err => {

                    this.parseError(err);
                },
                () => {}
            );

    }

    private saveGoodReceipt () {

        var params = {asn_hdr_id: this.id};
        params['ctnr_id'] = jQuery("select[name=ctnr_id]").val();

        if (params['ctnr_id'] == "" ) {

            return false;
        }

        this._asnServices.saveGoodReceipt(JSON.stringify(params))
            .subscribe(
                data => {

                    this.dataGoodReceipt = data.data;

                },
                err => {

                    this.parseError(err);
                },
                () => {
                    this._router.parent.navigateByUrl('/goods-receipt/' + this.dataGoodReceipt['gr_hdr_id'] + '/new');

                }
            );
    }

    // Go back
    private goBack() {

        history.back();
    }

    // Show error when server die or else
    private parseError (err) {

        this.messages = {'status' : 'danger', 'txt' : this._Func.parseErrorMessageFromServer(err)};
    }

    private ckDoccument() {

        var that = this;
        let api = 'https://pims.dev.khachvip.vn/api/v1/upload/prepare?sys=wms2.0&doc_type=ASN&trans=' + this.dataAsn['asn_hdr_num'] + '&jwt=' + that._Func.getToken();
        try{
            let xhr = new XMLHttpRequest();
            xhr.open("GET", api , true);
            //xhr.setRequestHeader("Authorization", 'Bearer ' + that._Func.getToken());
            xhr.responseType = 'blob';

            xhr.onreadystatechange = function () {

                if(xhr.readyState == 2) {

                    if(xhr.status == 200) {

                        that.ckLoadDocument =  {'flag': true, 'message': xhr.status + ''};
                        xhr.responseType = "text";
                    } else {
                        xhr.responseType = "text";
                        that.ckLoadDocument =  {'flag': false, 'message': xhr.status + ''};
                    }
                }
            };
            xhr.send();
        }catch (err){

            that.showMessage('danger', that._Func.msg('VR100'));
        }
    }

    // Event when user filter form
    private onPageSizeChanged ($event,el) {

        this.perPage = $event.target.value;

        if((this.Pagination['current_page'] - 1) * this.perPage >= this.Pagination['total']) {
            this.currentPage = 1;
        }
        else {
            this.currentPage = this.Pagination['current_page'];
        }

        this.getEventTrackingByAsn(this.currentPage);
    }

    //By Nhan Truong
    getDateTime(){
        var now   = new Date();
        var year  = now.getFullYear();
        var month = now.getMonth();
        var date  = now.getDate().toString();
        var day   = now.getDay();
        var dayNames = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
        var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        if(date.toString().length == 1) {
            var date = '0'+date;
        }
        var dateTime = dayNames[day] + ', ' + monthNames[month] +' '+date+', '+year;
        return dateTime;
    }
    //get Name
    getLoggedUser(){
        this.loggerUser = localStorage.getItem('logedUser');
        this.loggerUser = JSON.parse('' + this.loggerUser);
    }

    /*
     * render data for print
     * */
    renderPrintANS()
    {
        let i=0;
        for (let ctn in this.dataListContainersByASN){
            // get asn detail by ctn id
            this._asnServices.getAsnByIdCtnId(this.id, this.dataListContainersByASN[ctn]['ctnr_id'])
                .subscribe(
                    data => {
                        this.dataListContainersByASN[i]['asn_details'] = data.data['asn_details'];
                        i++;
                    },
                    err => {
                        this.parseError(err);
                    },
                    () => { }
                );

        }

    }

    private printReceiving() {

        try {

            var that = this;
            that.messages = false;

            var xhr = new XMLHttpRequest();
            xhr.open("GET", this._api_Config.API_Asns + "/" + this.id + '/containers/' + this.ctn_id + "/print", true);
            xhr.setRequestHeader("Authorization", 'Bearer ' +this._Func.getToken());
            xhr.responseType = 'blob';

            xhr.onreadystatechange = function () {
                if(xhr.readyState == 2) {
                    if(xhr.status == 200) {
                        xhr.responseType = "blob";
                    } else {
                        xhr.responseType = "text";
                    }
                }
                // If we get an HTTP status OK (200), save the file using fileSaver
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {

                        var blob = new Blob([this.response], {type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'});
                        saveAs.saveAs(blob, that.dataAsn['asn_hdr_num'] + '.pdf');

                    } else {
                        let errMsg = '';
                        if(xhr.response) {
                            try {
                                var res = JSON.parse(xhr.response);
                                errMsg = res.errors && res.errors.message ? res.errors.message : xhr.statusText;
                            }
                            catch(err){errMsg = xhr.statusText}
                        }
                        else {
                            errMsg = xhr.statusText;
                        }
                        if(!errMsg) {
                            errMsg = that._Func.msg('VR100');
                        }
                        that.messages = that._Func.Messages('danger', errMsg);
                    }
                }
            };

            xhr.send();
        } catch (err) {

            this.messages = this._Func.Messages('danger', this._Func.msg('VR100'));
        }
    }

    private _openBarCode:any;
    private dataAsnDetails = [];
    private openBarCode(){

        this.dataAsnDetails = this.removeCalcelItem(this.dataAsn['asn_details']);
        this._openBarCode=true;
        setTimeout(()=>{
            this._openBarCode=false;
        })
    }

    private removeCalcelItem(data:Array<any>=[]){

        let tmp= data.slice();

        data.forEach((item)=>{
            if(item['asn_dtl_sts_code'].toLowerCase() == 'cc'){
                let index=tmp.indexOf(item);
                if (index > -1) {
                    tmp.splice(index, 1);
                }
            }
        })

        return tmp;

    }

    FormBuilder(data={}) {
        let today = new Date();
        this.viewASNForm = this.fb.group({
            asn_hdr_act_dt: [data['asn_hdr_act_dt'] ? data['asn_hdr_act_dt'] : (today.getMonth() + 1) + '/' + (today.getDate()) + '/' + today.getFullYear()],
            asn_dtl_ept_dt: [data['asn_dtl_ept_dt'] ? data['asn_dtl_ept_dt'] : ''],
        });
        setTimeout(() => {
            this.fbdFunc.handelDropdownDatePicker(this.viewASNForm);
        }, 200);
    }

    completeReceiving() {
        if (this.viewASNForm.valid) {
            const that = this;
            this.viewASNForm.value['ctnr_id'] = this.ctn_id;
            const paramsPost = this.viewASNForm.value;
            this.showLoadingOverlay = true;
            this._asnServices.completeReceiving(this.id, JSON.stringify(paramsPost)).subscribe(
                data => {
                    that.showLoadingOverlay = false;
                    if (data.data && data.data.asn_hdr_id ) {
                        that.messages = that._Func.Messages('success', that._Func.msg('ASN003'));
                        that._redirectToList(that);
                    } else {
                        that.messages = that._Func.Messages('danger', data.message);
                    }
                },
                err => {
                    that.showLoadingOverlay = false;
                    that.parseError(err);
                },
                () => {
                }
            );
        } else {
            this.submitForm = true;
        }
    }

    undoComplete() {
        if (this.viewASNForm.valid) {
            const that = this;
            const paramsPost = this.viewASNForm.value;
            this.showLoadingOverlay = true;
            this._asnServices.undoComplete(this.id, JSON.stringify(paramsPost)).subscribe(
                data => {
                    that.showLoadingOverlay = false;
                    that.messages = that._Func.Messages('success', that._Func.msg('ASN004'));
                    that._redirectToList(that);
                },
                err => {
                    that.showLoadingOverlay = false;
                    that.parseError(err);
                },
                () => {
                }
            );
        } else {
            this.submitForm = true;
        }
    }

    _redirectToList(that, time = 600) {
        setTimeout(function () {
            that._router.parent.navigateByUrl('/inbound/asn');
        }, time);
    }

    updateMessages(messages) {
        this.messages = messages;
    }

}
