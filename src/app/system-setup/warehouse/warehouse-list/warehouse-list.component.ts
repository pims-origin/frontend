import {Component,Input} from '@angular/core';
import { WarehouseServices } from '../warehouse-service';
import { Router } from '@angular/router-deprecated';
import { FORM_DIRECTIVES,ControlGroup,FormBuilder, Control} from '@angular/common';
import {API_Config,UserService, Functions} from '../../../common/core/load';
import { WarehouseServicesMaster } from '../../../common/services/warehouse.service.master';
import { CountryService } from '../../../master-data/country/country-service';
import { Http } from '@angular/http';
import {WMSPagination,AgGridExtent,WMSBreadcrumb} from '../../../common/directives/directives';
@Component ({
  selector: 'warehouse-list',
  providers: [WarehouseServices,CountryService,WarehouseServicesMaster,FormBuilder],
  directives: [AgGridExtent,WMSPagination,FORM_DIRECTIVES, WMSBreadcrumb],
  templateUrl: 'warehouse-list.component.html',
})

export class WarehouseListComponent {

  private headerURL = this._API.API_User_Metas+'/whs';
  private WareHouseList:Array<any>=[];
  private ListPermission=[];
  private Pagination;
  public Loading=[];
  private errMessages=[];
  private totalCount = 0;
  private tmpPageCount;
  private pageCount=0;
  private ItemOnPage=0;
  private currentPage = 1;
  private perPage = 20;
  private numLinks = 3;
  private start = 1;
  private end = 1;
  private Country=[];
  private State:Array<any>=[];
  public ListSelectedItem:Array<any>=[];
  public selectedAll:boolean=false;
  private Mcountry;
  private Mstate;
  private StateProvinces=[];
  private Mstatus = [];
  private activeStatus;
  private messages;
  SearchWareHouseForm: ControlGroup;
  private IsSearch:boolean=false;
  private SearchQuery=null;
  private sortData={fieldname:'whs_code',sort:'asc'};
  private showLoadingOverlay:boolean=false;

  private columnData = {
    whs_id:{
      /*attr:'checkbox',
      id:true,
      title:'#',
      width:20,
      pin:true,
      ver_table:3*/
      attr:'checkbox',
      id:true,
      title:'#',
      width:25,
      pin:true,
      ver_table:'2'
    },
    whs_status_name:{
      title: 'Status',
      width:60,
      pin:true,
      sort:true
    },
    whs_code:{
      title:'Warehouse Code',
      width:80,
      url:'#/system-setup/warehouse/',
      field_get_id:'whs_id',
      pin:true,
      sort:true
    },
    whs_name: {
      title:'Warehouse Name',
      width:120,
      pin:true,
      sort:true
    },
    contact_name:{
      title: 'Contact Name',
      parent :'whs_contact',
      groupfield:['whs_con_fname','whs_con_lname'],
      width:140,
      pin:true
    },
    whs_con_email:{
      title: 'Contact Email',
      parent :'whs_contact',
      width:160,
      pin:true
    },
    whs_con_phone:{
      title: 'Phone',
      parent :'whs_contact',
      width:140,
      pin:true
    },
    whs_country_name: {
      title: 'Country',
      width:80
    },
    whs_city_name:{
      title: 'City',
      width:100,
      sort:true
    },
    whs_state_name:{
      title: 'State',
      width:100
    }
  };


  constructor(
  	private _http: Http,
    private _Func: Functions,
    private _API: API_Config,
    private _WHSM:WarehouseServicesMaster,
    private fb: FormBuilder,
    private _CT:CountryService,
    private _user: UserService,
    private _WHS: WarehouseServices,
    private _router:Router)
  {

    // builder form search

    /*
    * Get List WareHouse
    * */

    this.GetCountry();
    this.getWarehouseStatus();
    this.FormBuilder();
    this.checkPermission();

  }

  private allowAccess:boolean=false;
  private createWarehouse;
  private editWarehouse;
  private viewWarehouse;
  private checkPermission() {
    this.showLoadingOverlay = true;
    this._user.GetPermissionUser().subscribe(
      data => {
        this.createWarehouse = this._user.RequestPermission(data, 'createWarehouse');
        this.editWarehouse = this._user.RequestPermission(data,'editWarehouse');
        this.viewWarehouse=this._user.RequestPermission(data,'viewWarehouse');

        // pass
        this.showLoadingOverlay = false;

        if(!this.viewWarehouse){
          this.redirectDeny();
        }
        else{
          this.allowAccess=true;
          this.GetWareHouseList();
        }

        // end pass
      },
      err => {
        this.messages=this._Func.Messages('danger', this._Func.parseErrorMessageFromServer(err));
        this.showLoadingOverlay = false;
      }
    );
  }
  redirectDeny(){
    this._router.parent.navigateByUrl('/deny');
  }

  FormBuilder()
  {
    this.SearchWareHouseForm =this.fb.group({
      whs_code: [''],
      whs_name: [''],
      whs_country_id: [''],
      whs_state_id: [''],
      whs_city_name: [''],
      whs_status:['']
    });

  }

  /*
   * Load state of country when change
   * */
  ChangeCountry(item)
  {
    if(item)
    {
      this.GetState(item);
    }
    else{
      this.State=[];
      (<Control>this.SearchWareHouseForm.controls['whs_state_id']).updateValue('');
    }

  }

  getWarehouseStatus() {
    this._WHSM.Status().subscribe(
      data=>{
        this.Mstatus=data.data;
      },
      err=>{
        this.errMessages.push('Could not load country.');
      }
    );
  }

  /*
   * Get Country
   * */
  GetCountry()
  {
    let params='?limit=1000&sort[sys_country_name]=asc';
    this._WHS.Country(params).subscribe(
      data=>{
        this.Country=data.data;
      },
      err=>{
        this.errMessages.push('Could not load country.');
      },
      ()=> {}
    );
  }

  GetState(idCountry)
  {
    this.Loading['state']=true;
    let params='states?sort[sys_state_name]=asc&limit=500';
    this._WHS.State(idCountry,params).subscribe(
      data=>{
        this.Loading['state']=false;
        this.State=data.data;
      },
      err=>{
        this.Loading['state']=false;
        this.errMessages.push('Could not load State.');
      }
    );
  }

  private createDetail()
  {
    this._router.parent.navigateByUrl('/system-setup/warehouse/new');
  }
  private GetWareHouseList(page=null)
  {
    let param="?sort["+this.sortData['fieldname']+"]="+this.sortData['sort']+"&page="+page+"&limit="+this.perPage;

    if(this.IsSearch){
      param+=this.SearchQuery;
    }
    this.showLoadingOverlay=true;

		this._WHS.GetWareHouseList(param).subscribe(
			data => {
        this.selectedAll = false;
        this.showLoadingOverlay = false;
				this.WareHouseList = this._Func.formatData(data.data);
        //pagin function
        this.Pagination=data['meta']['pagination'];
        this.Pagination['tmpPageCount']=this._Func.Pagination(this.Pagination);

			},
			err => {
        this.showLoadingOverlay = false;
      },
			() => {}
		);
  }
  sortList($event)
  {
    this.sortData=$event;
    this.GetWareHouseList(this.Pagination['current_page']);
  }
  private onPageSizeChanged($event)
  {
    this.perPage = $event.target.value;
    this.GetWareHouseList(this.currentPage);
  }

  Search()
  {
    this.messages = null;
    let data=this.SearchWareHouseForm.value;
    this.ListSelectedItem=[];
    this.selectedAll=false;
    // set status search is true
    this.IsSearch=true;
    // assign to SearchQuery
    this.SearchQuery="&whs_code="+encodeURIComponent(data['whs_code'])+"&whs_name="+encodeURIComponent(data['whs_name'])+"&whs_country_id="
        +data['whs_country_id']+"&whs_city_name="+encodeURIComponent(data['whs_city_name'])+"&whs_state_id="+data['whs_state_id']+"&whs_status="+data['whs_status'];
    this.GetWareHouseList();
  }

  /*
  * Router edit
  * */
  Edit()
  {
    if(this.ListSelectedItem.length>1) {
      this.messages = this._Func.Messages('danger','Please choose only one item to edit');
    }
    else{
      if(this.ListSelectedItem.length)
      {
        this._router.parent.navigateByUrl('/system-setup/warehouse/'+this.ListSelectedItem[0]['whs_id']+'/edit');
      }
      else{
        this.messages = this._Func.Messages('danger','Please choose one item to edit .');
      }
    }
  }


  private Delete()
  {
    if(!this.ListSelectedItem.length)
    {
      // if no select any item to delete
      return;
    }
    if(confirm(this._Func.msg('PBAP002'))&&this.ListSelectedItem.length) {
      // remove item local
      this.RemoveItemLocal();
      this.ListSelectedItem.forEach((item)=> {
        // the fisrt , remove item in list data

        this._WHS.DeleteWareHouse(item['whs_id']).subscribe(
          data => {
            this.selectedAll = false;

            // reload data new
            if (this.WareHouseList.length <10) {
              this.GetWareHouseList(this.Pagination['current_page']);
            }

          },
          err => {},
          () => {}
        );

      });

      // set empty selected
      this.ListSelectedItem = [];
    }

  }

  RemoveItemLocal()
  {
    let sumItemDelet=0;

    this.ListSelectedItem.forEach((item)=>{

        for(let el of this.WareHouseList) {
          let els=parseInt(el);
          if (item == els['whs_id']) {
            let index = this.WareHouseList.indexOf(item);
            // remove item out of array
            this.WareHouseList.splice(index, 1);
            sumItemDelet++;
          }
          else{
          }
        }

    });

  }


  /*
  * Reset Form Builder
  * */
  ResetSearch()
  {
    this.IsSearch=false; // remove action search
    this._Func.ResetForm(this.SearchWareHouseForm);
    // redirect to page 1
    this.GetWareHouseList();
  }


}
