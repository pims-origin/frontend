import {Component, Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {API_Config, Functions} from '../../common/core/load';

@Injectable()
export class CustomerServices {

  public AuthHeader = this._Func.AuthHeader();
  public AuthHeaderPost = this._Func.AuthHeaderPost();
  public whs_id = localStorage.getItem('whs_id');

  constructor(private _Func: Functions, private _API: API_Config, private http: Http)
  {

  }

  /*
   * Add new Customer
   * */
  Add_New_Customer(data)
  {
    //console.log(data);
    return this.http.post(this._API.API_Customer, data, { headers: this._Func.AuthHeaderPostJson() })
      .map((res: Response) => res.json().data);
  }

  /*
   * Update ItemMaster
   * */
  Update_Customer(id,data)
  {
    //console.log('__update item master id > ' +id,data);
    return this.http.put(this._API.API_Customer+'/'+id, data, { headers: this._Func.AuthHeaderPostJson() })
      .map((res: Response) => res.json().data);
  }

  /*
   * Get detail customer
   * */
  Get_CustomerDetail(id)
  {
    return this.http.get(this._API.API_Customer+'/'+id, { headers: this.AuthHeader })
      .map((res: Response) => res.json().data);
  }
  /*
   * Delete function
   * */
  DeleteItemMaster(id)
  {
    return this.http.delete(this._API.API_Customer + "/" + id, { headers: this.AuthHeader });
  }

  /*
   *
   * Search function
   * */
  Search(param)
  {
    return this.http.get(this._API.API_Customer+param,{ headers: this.AuthHeader })
      .map((res: Response) => res.json());
  }

  /*
   * Get List custommers
   * */
  ListCustomers(param)
  {
    //console.log('__customer service >> ',this._API.API_Customer+param)
    return this.http.get(this._API.API_Customer_List_All+param,{ headers: this.AuthHeader })
      .map((res: Response) => res.json());
  }

  /*
   * Get list warehosue
   * */
  GetWareHouseList(param)
  {
    return this.http.get(this._API.API_Warehouse+param,{ headers: this.AuthHeader })
      .map((res: Response) => res.json());
  }

  /**
   * Get Warehouse list not in customer
   *
   * @param cusId
   */
  getWarehouseNotInCus(cusId: string) {
    console.log(`getWarehouseNotInCus URL: `, `${this._API.API_CUSTOMER_CORE}/customers/${cusId}/whs-not-in-cus`);
    return this.http.get(`${this._API.API_CUSTOMER_CORE}/customers/${cusId}/whs-not-in-cus`, { headers: this.AuthHeader })
      .map((res: Response) => res.json());
  }

  /**
   * Update warehouse to customer
   *
   * @param cusId
   * @param data
   */
  updateWarehouseToCustomer(cusId: string, data: any) {
    return this.http.post(`${this._API.API_CUSTOMER_CORE}/customers/${cusId}/whs-to-cus`, data, { headers: this._Func.AuthHeaderPostJson() })
      .map((res: Response) => res.json());
  }

  /*
   * Get list warehosue
   * */
  GetChargCodebySeach(param)
  {
    //console.log(this._API.API_CUSTOMER_MASTER+'/charge-codes'+param);
    return this.http.get(this._API.API_CUSTOMER_MASTER+'/charge-codes'+param,{ headers: this.AuthHeader })
      .map((res: Response) => res.json());
  }

  /*
  *GetUOMs
  * */
  getUOMs(param)
  {
    return this.http.get(this._API.API_CUSTOMER_MASTER+'/system-uoms'+param,{ headers: this.AuthHeader })
      .map((res: Response) => res.json());
  }

  /*
   *getChargeTypes
   * */
  getChargeTypes(param)
  {
    return this.http.get(this._API.API_CUSTOMER_MASTER+'/charge-types'+param,{ headers: this.AuthHeader })
      .map((res: Response) => res.json());
  }

  addNewChargCode(data)
  {
    return this.http.post(this._API.API_CUSTOMER_MASTER+'/charge-codes', data, { headers: this._Func.AuthHeaderPostJson() })
      .map((res: Response) => res.json().data);
  }


  getCountry(param) {
    return this.http.get(this._API.API_Country + param,{ headers: this.AuthHeader })
      .map((res: Response) => res.json().data);
  }

  /*
   * State
   * */
  State(idCountry,param)
  {
    return this.http.get(this._API.API_Country+'/'+idCountry+'/states'+param,{ headers: this.AuthHeader })
      .map((res: Response) => res.json().data);
  }

  /*getZoneType
  * */
  getZoneByCustomer(cus_id,param)
  {
    //console.log('__getZoneByCustomer',this._API.API_CUSTOMER_MASTER+'/customers/'+cus_id+'/zones'+param);
    return this.http.get(this._API.API_CUSTOMER_MASTER+'/customers/'+cus_id+'/zones'+param,{ headers: this.AuthHeader })
      .map((res: Response) => res.json());
  }

  /*getZone
   * */
  getZone(param)
  {
    return this.http.get(this._API.API_Warehouse+'/free-zones'+param,{ headers: this.AuthHeader })
      .map((res: Response) => res.json());
  }


  /*
   * Get warehouse list all
   * */
  getWareHouseList(param)
  {
    return this.http.get(this._API.API_Warehouse+param,{ headers: this.AuthHeader })
      .map((res: Response) => res.json());
  }

  /*
  * Get warehouse by customer
  * */
  getWareHouseByCustomer(cus_id,param)
  {

    return this.http.get(this._API.API_CUSTOMER_MASTER+'/customers/'+cus_id+'/warehouses'+param,{ headers: this.AuthHeader })
      .map((res: Response) => res.json());
  }

  /*
   * Get warehouse by customer
   * */
  getLocationByCustomer(cus_id,param)
  {
    //console.log('api load locations by customer ' , this._API.API_CUSTOMER_MASTER+'/customers/'+cus_id+'/locations'+param);
    return this.http.get(this._API.API_CUSTOMER_MASTER+'/customers/'+cus_id+'/locations'+param,{ headers: this.AuthHeader })
      .map((res: Response) => res.json());
  }

  getLocaltionList(cus_id,param)
  {

    return this.http.get(this._API.API_Warehouse+'/'+cus_id+'/locations'+param,{ headers: this.AuthHeader })
      .map((res: Response) => res.json());
  }
  
  /*
   * saveOrderFlow
   * */
  saveOrderFlow(cus_id, data)
  {
    //console.log('__update item master id > ' +id,data);
    return this.http.post(this._API.API_CUSTOMER_MASTER+'/customers/order-flow/' + cus_id, data, { headers: this._Func.AuthHeaderPostJson() })
        .map((res: Response) => res.json().data);
  }

  getListCRSByCustomer(cus_id, whs_id) {
    let _api = this._API.API_Authen + '/customer-csr/' + cus_id ;
    if(whs_id != ''){
      _api += '/' + whs_id;
    }
    return  this.http.get(_api, { headers: this.AuthHeader })
        .map(res => res.json());
  }

  addUserOfCustomer(cus_id, data) {
    return this.http.post(this._API.API_CUSTOMER_MASTER+'/customers/user-of-customer/' + cus_id, data, { headers: this._Func.AuthHeaderPostJson() })
        .map((res: Response) => res.json().data);
  }

  getListCsrUser(cus_id)
  {
    return this.http.get(this._API.API_CUSTOMER_MASTER+'/customers/user-meta/' + cus_id + '?qualifier=CSR&whs_id=' + this.whs_id,{ headers: this.AuthHeader })
        .map((res: Response) => res.json());
  }

  getListPKRByCustomer(cus_id, whs_id)
  {
    let _api = this._API.API_Authen + '/customer-picker/' + cus_id ;
    if(whs_id != ''){
      _api += '/' + whs_id;
    }
    return  this.http.get(_api, { headers: this.AuthHeader })
        .map(res => res.json());
  }

  getListPkrUser(cus_id)
  {
    return this.http.get(this._API.API_CUSTOMER_MASTER+'/customers/user-meta/' + cus_id + '?qualifier=PKR&whs_id=' + this.whs_id,{ headers: this.AuthHeader })
        .map((res: Response) => res.json());
  }

  getCurrency()
  {
    return this.http.get(this._API.API_COMMON+'/currency',{ headers: this.AuthHeader })
        .map((res: Response) => res.json());
  }

  /**
   * Save Inbound Config
   *
   * @param cus_id
   * @param data
   */
  saveInboundConfig(cus_id: string, data: string) {
    return this.http
      .post(this._API.API_CUSTOMER_MASTER + '/customers/' + cus_id + '/inbound-config', data, {
        headers: this._Func.AuthHeaderPostJson()
      })
      .map((res: Response) => res.json().data);
  }

  /**
   * Get Inbound Config data
   *
   * @param cus_id
   */
  getInboundConfig(cus_id: string) {
    return this.http
      .get(this._API.API_CUSTOMER_MASTER + '/customers/' + cus_id + '/inbound-config', { headers: this.AuthHeader })
      .map((res: Response) => res.json().data);
  }

  /**
   * Save configuration email
   *
   * @param customerId
   * @param data
   */
  saveConfigurationEmail(customerId: string, data: any) {
    return this.http.post(`${this._API.API_CUSTOMER_CORE}/${customerId}/customer-email`, data, {
      headers: this._Func.AuthHeaderPostJson()
    }).map(res => res.json());
  }

  /**
   * Get configuration email
   *
   * @param customerId
   */
  getConfigurationEmail(customerId: string){
    return this.http.get(`${this._API.API_CUSTOMER_CORE}/${customerId}/customer-email`, {
      headers: this.AuthHeader
    }).map(res => res.json());
  }

  getCarrierData(cus_id: string) {
    return this.http.get(`${this._API.API_CUSTOMER_CORE}/customers/carrier-service/${cus_id}?qualifier[]=UPS&whs_id=${this.whs_id}&qualifier[]=FDX`, { headers: this.AuthHeader })
      .map((res: Response) => res.json());
  }

  submitCarrier(cus_id, data) {
    return this.http.post(`${this._API.API_CUSTOMER_CORE}/customers/carrier-service/${cus_id}`, data, { headers: this._Func.AuthHeaderPostJson() })
      .map((res: Response) => res.json().data);
  }
}
