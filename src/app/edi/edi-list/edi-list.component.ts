/**
 * Created by admin on 12/28/2016.
 */
import {Component} from '@angular/core';
import {Router} from '@angular/router-deprecated';
import {API_Config, UserService, Functions} from '../../common/core/load';
import {FORM_DIRECTIVES, ControlGroup, FormBuilder, Control} from '@angular/common';
import {Http, Response} from '@angular/http';
import {WMSBreadcrumb,AgGridExtent} from '../../common/directives/directives';
import {BoxPopupService} from "../../common/popup/box-popup.service";
import {UserDepartmentService} from "../../master-data/user-department/user-department-service";
import {OrderBy} from "../../common/pipes/order-by.pipe";
import {WMSMessages} from "../../common/directives/messages/messages";
import {EditEDIDirective} from "./edit-edi"
import {EDIServices} from "./edi-service"

declare var jQuery:any;

@Component({
    selector: 'edi-list',
    providers: [UserDepartmentService, EDIServices,FormBuilder, BoxPopupService],
    pipes: [OrderBy],
    directives: [FORM_DIRECTIVES,AgGridExtent,EditEDIDirective, WMSBreadcrumb,WMSMessages],
    templateUrl: 'edi-list.component.html',
})

export class EDIListComponent {
    private ediList:Array<any> = [];
    private Pagination = [];
    private perPage = 20;
    public Loading = [];
    public ListSelectedItem:Array<any> = [];
    public selectedAll:boolean = false;
    private messages;
    private SearchForm:ControlGroup;
    public showLoadingOverlay = false;
    private configEDIPermission = false;
    private dataListCustomer = [];
    private warehouseList = [];
    private EDI_ID;
    private whs_id;
    private Action;
    private dataForRenderTable:Array<any>=[];
    private headerURL = this._API.API_User_Metas+'/edi';
    private columnData = {
        cus_id:{
            attr:'checkbox',
            id:true,
            title:'#',
            width:25,
            pin:true,
            ver_table:'21032017'
        },
        cus_name: {
            title:'Customer',
            width:120,
            pin:true
        },
        edi:{
            title:'EDI',
            width:120,
            pin:true
        }
    };
    private param;

    constructor(private _http:Http,
                private _Func:Functions,
                private _user:UserService,
                private fb:FormBuilder,
                private _API: API_Config,
                private edis:EDIServices,
                private _router:Router,
                private _boxPopupService:BoxPopupService) {
        this.BuilderSearchForm();
        this.checkPermission();
        this.whs_id=this._Func.lstGetItem('whs_id');
    }

    // Check permission for user using this function page
    private checkPermission() {
        this.showLoadingOverlay = true;
        this._user.GetPermissionUser().subscribe(
            data => {
                this.configEDIPermission = this._user.RequestPermission(data, 'configEDI');
                if (!this.configEDIPermission) {
                    this._router.parent.navigateByUrl('/deny');
                }
                else {
                    this.getEdiList();
                    this.getCustomers();
                }
            },
            err => {
                this.showMessage('danger', this._Func.parseErrorMessageFromServer(err));
                this.showLoadingOverlay = false;
            }
        );
    }

    // get list custommer

    private getCustomers () {
        let param="?whs_id="+this.whs_id+"&limit=9999";
        this._http.get(this._API.API_Customer_List_All+param,{ headers: this._Func.AuthHeader() })
            .map((res: Response) => res.json())
            .subscribe(
                data => {
                    this.dataListCustomer = data.data;
                },
                err => {

                },
                () => {}
            );

    }
    
    private showMessage(msgStatus, msg) {
        this.messages = {
            'status': msgStatus,
            'txt': msg
        }
        jQuery(window).scrollTop(0);
    }

    /*
     * Builder form
     * */
    private BuilderSearchForm() {
        this.SearchForm = this.fb.group({
            cus_id: [''],
            edi: ['']
        });
    }

    /*
     * Get getEdiList
     * */
    private getEdiList(page = null,search=false) {

        let param= '/'+this.whs_id + "?page=" + page + "&limit=" + this.perPage;

        if(search){
            let datasearch=this.SearchForm.value;
            param+='&cus_id='+datasearch['cus_id']+'&edi='+datasearch['edi'];
        }

        this.showLoadingOverlay = true;

        this.edis.get_EDI_List(param).subscribe(
            data => {
                this.ediList = data.data;
                this.selectedAll = false;
                //pagin function
                this.Pagination = data['meta']['pagination'];
                this.Pagination['numLinks'] = 3;
                this.Pagination['tmpPageCount'] = this._Func.Pagination(this.Pagination);
                this.showLoadingOverlay = false;

                setTimeout(()=>{
                    this.ViewEDI();
                },1000)

            },
            err => {},
            () => {}
        );
       
    }

    private editSelectedItem(){

        this.Action="edit";

        if (this.ListSelectedItem.length > 1) {
            this.messages = this._Func.Messages('danger', 'Please choose only one item to config EDI.');
        }
        else {
            if (this.ListSelectedItem.length) {
                this.EDI_ID=null;
                setTimeout(()=>{
                    this.EDI_ID=this.ListSelectedItem[0]['cus_id'];
                })
            }
            else {
                this.messages = this._Func.Messages('danger', 'Please choose one item to config EDI.');
            }

        }
    }

    private onPageSizeChanged($event) {
        this.perPage = $event.target.value;
        if ((this.Pagination['current_page'] - 1) * this.perPage >= this.Pagination['total']) {
            this.Pagination['current_page'] = 1;
        }
        this.getEdiList(this.Pagination['current_page']);
    }


    /*========================
     * Reset Form Builder
     * =====================*/
    private ResetSearch() {
        (<Control>this.SearchForm.controls["cus_id"]).updateValue("");
        (<Control>this.SearchForm.controls["edi"]).updateValue("");
        this.getEdiList(this.Pagination['current_page']);
    }


    /*==============================
    * Get id by using jquery
    * ============================*/
    private ViewEDI(){

        let that=this;
        jQuery("div.ag-row").click(function(){
                let id=jQuery(this).find('div[colid="cus_id"] span.ag-cell-value').text();
                that.Action="view";
                that.EDI_ID=null;
                setTimeout(()=>{
                    that.EDI_ID=id;
                });
        });
        
    }



}
