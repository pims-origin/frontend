import {Component, DynamicComponentLoader, ElementRef, Injector} from '@angular/core';
import { FORM_DIRECTIVES,ControlGroup,FormBuilder, Control ,Validators} from '@angular/common';
import { Http } from '@angular/http';
import { Router, RouterLink, ROUTER_DIRECTIVES, RouteParams } from '@angular/router-deprecated';
import 'ag-grid-enterprise/main';
import 'rxjs/add/operator/map';
import {API_Config} from "../../common/core/API_Config";
import {Functions} from "../../common/core/functions";
import {GoodsReceiptServices} from "../goods.receipt.service";
import {JwtHelper} from "angular2-jwt/angular2-jwt";
import {WMSMessages, WMSBreadcrumb} from '../../common/directives/directives';
import {UserService} from "../../common/users/users.service";
import {BoxPopupService} from "../../common/popup/box-popup.service";
declare var jQuery: any;

@Component ({
    selector: 'create-goods-receipt',
    templateUrl: 'create-goods-receipt.component.html',
    providers: [GoodsReceiptServices,BoxPopupService],
    directives: [WMSMessages, WMSBreadcrumb],
})

export class CreateGoodsReceiptComponent {

    private id: any;
    private ctn_id: any;
    private listNull = {};
    private listNull2 = {};
    private dataListContainer = {};
    private messages:any;
    private tmpItem = [null];
    private dataListMeasurements = [];
    private dataListContainersByASN = [];
    private dataListCustomer = [];
    private dataAsn = [];
    private loc_whs_id: any;
    private messagesItem = {};
    private dataGoodReceipt = [];
    private flagSubmitValid = false;
    private timeoutHideMessage;
    private ctnrCode = '';
    private gr_hdr_id = '';

    // Construct
    constructor (
        private _router:Router,
        private _goodsReceiptServices: GoodsReceiptServices,
        params: RouteParams,
        private _Func: Functions,
        public jwtHelper: JwtHelper,
        private _user: UserService,
        private _boxPopupService: BoxPopupService
    ) {

        // Check permission
        this.checkPermission();
        this.id = params.get("id");
        this.ctn_id = params.get("ctn_id");
        this.getGoodsReceiptByIdCtnID();

    }

    // Check Permission
    private checkPermission() {

        this._user.GetPermissionUser().subscribe(

            data => {

                var flag= this._user.RequestPermission(data,'createGoodsReceipt');

                if(!flag) {
                    this._router.parent.navigateByUrl('/deny');
                }
            },
            err => {

                this.showMessage('danger', this._Func.parseErrorMessageFromServer(err));

            }
        );
    }

    private showMessage(msgStatus, msg) {

        this.messages = {
            'status': msgStatus,
            'txt': msg
        }
        jQuery(window).scrollTop(0);
    }

    private requestPermission(permission, permission_req) {

        if (permission && permission.length) {

            for (var per of permission) {

                if (per.name == permission_req) {

                    return true;
                }
            }

        }

        return false;

    }

    // Get goods receipt by id
    private getGoodsReceiptByIdCtnID() {

        this._goodsReceiptServices.getGoodsReceiptByIdCtnID(this.id, this.ctn_id)
            .subscribe(
                data => {

                    this.dataGoodReceipt = data.data;
                    this.dataGoodReceipt['asn_details'] = this.removeCalcelItem(this.dataGoodReceipt['asn_details']);
                    this.ctnrCode = data.data['asn_details']['0']['ctnr_num'];
                },
                err => {

                    this.parseError(err);
                },
                () => {}
            );
    }

    // cancel gôt list
    private cancel () {

        this._router.parent.navigateByUrl('/goods-receipt');
    }

    // save
    private save ($status: string) {

        this.flagSubmitValid = false;
        this.messages = false;
        var tar_get = this;

        var submit = true;

        // Check logic for GR
        // Case only submit button
        this.listNull2 = {};

        // Ignore validate labor charge field
        /*if(jQuery("#labor_charge")[0].value == ''){
          tar_get.listNull2['labor_charge_required'] = true;
        }else{
          tar_get.listNull2['labor_charge_required'] = false;
        }*/

        if ($status == '0') {

            this.flagSubmitValid = true;
            jQuery('form').find('input[id^=gr_dtl_act_ctn_ttl_], input[id^=gr_dtl_dmg_ttl_]').each(function(i, field) {

                var patt = /^gr_dtl_act_ctn_ttl_/;
                if (patt.test(field.id)) {

                    tar_get.filterForm2(field, 'required_int_greater0');
                } else {

                    tar_get.filterForm2(field, 'int');
                }

                tar_get.checkLessAct(i);

            });

        } else {

            jQuery('form').find('input[id^=gr_dtl_act_ctn_ttl_], input[id^=gr_dtl_dmg_ttl_]').each(function(i, field) {

                var patt = /^gr_dtl_act_ctn_ttl_/;

                if (patt.test(field.id)) {

                    tar_get.filterForm2(field, 'int_greater0');

                } else {

                    tar_get.filterForm2(field, 'int');
                }

                tar_get.checkLessAct(i);

            });
        }

        this.calTotalEP();

        jQuery.each( this.listNull2, function( key, value ) {

            if (value) {

                submit = false;
                return false;
            }
        });

        if (!submit) {

            return false;
        }

        var params = jQuery("form").serializeArray();

        var paramsJson = this.buildParams(params);
         paramsJson['gr_received_pending'] = $status;

        this._goodsReceiptServices.addNewGoodsReceipt(JSON.stringify(paramsJson))
            .subscribe(
                data => {

                    tar_get.gr_hdr_id = data.data['gr_hdr_id'];
                    this.messages = this._Func.Messages('success', 'The Goods Receipt was added successfully!');
                },
                err => {

                    this.parseError(err);
                },
                () => {

                    if ($status == '0') {

                        var tmp_checked = jQuery('input[id^=gr_dtl_is_dmg]:checkbox:checked').length;

                        if (tmp_checked > 0) {  // goto damage

                            let parent = this;
                            setTimeout(function(){

                                parent._router.parent.navigateByUrl('/goods-receipt/' + parent.gr_hdr_id + '/damaged-carton');
                            }, 1100);

                        } else {  // goto assign pallet

                            let parent = this;
                            // var redirectURL = '/goods-receipt/' + parent.gr_hdr_id + '/pallet-assignment';
                            var redirectURL = '/goods-receipt/' + parent.gr_hdr_id + '/pallet-assignment';

                            for(let item of parent.dataGoodReceipt['asn_details']) {
                                if(item.dtl_crs_doc > 0) {
                                    redirectURL = '/goods-receipt/' + parent.gr_hdr_id + '/assign-x-dock';
                                }
                            }

                            setTimeout(function(){

                               parent._router.parent.navigateByUrl(redirectURL);
                            }, 1100);

                        }

                    } else {

                        let parent = this;
                        setTimeout(function(){

                            parent._router.parent.navigateByUrl('/goods-receipt');
                        }, 1100);
                    }
                }
            );
    }

    /*
     * Disable input text
     * */
    checkInputNumber(evt,value)
    {
        evt = evt || window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (!evt.ctrlKey && charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 46){
            evt.preventDefault();
        }
    }
    // build params
    private buildParams (params) {

        var object = {};

        object['ctnr_id'] = this.ctn_id;
        object['asn_hdr_id'] = this.id;
        object['gr_in_note'] = jQuery("#gr_in_note")[0].value;
        object['gr_ex_note'] = jQuery("#gr_ex_note")[0].value;
       // object['labor_charge'] = jQuery("#labor_charge")[0].value ? jQuery("#labor_charge")[0].value : null;

        var detail = {};
        var tmp = [];

        var target = this;
        jQuery('form').find("input[id^=gr_dtl_act_ctn_ttl_]").each(function(j, obj) {

            var index = obj.id.replace('gr_dtl_act_ctn_ttl_', '');

            detail['asn_dtl_id']  = target.dataGoodReceipt['asn_details'][index]['asn_dtl_id'];
            detail['gr_dtl_act_ctn_ttl']  = jQuery("#gr_dtl_act_ctn_ttl_" + index)[0].value;
            detail['gr_dtl_dmg_ttl']  = jQuery("#gr_dtl_dmg_ttl_" + index)[0].value;

            // if(jQuery("#gr_dtl_is_dmg_" + index).is(':checked'))
            //     detail['gr_dtl_is_dmg'] = '1';
            // else
            //     detail['gr_dtl_is_dmg'] = '0';

            tmp.push(detail);
            detail = {};

        });

        object['details'] = tmp;
        return object;
    }

    // Get asn by id
    private getAsnByID() {

        this._goodsReceiptServices.getAsnByID(this.id)
            .subscribe(
                data => {

                    this.dataAsn = data.data;
                },
                err => {

                    this.parseError(err);
                },
                () => {}
            );
    }

    // Show error when server die or else
    private parseError (err) {

        this.messages = {'status' : 'danger', 'txt' : this._Func.parseErrorMessageFromServer(err)};
    }

    private setDefaultValid (index, flag = true) {

        var i = index;
        this.listNull2['gr_dtl_act_ctn_ttl_' + i + "_required"] = flag;
        this.listNull2['gr_dtl_act_ctn_ttl_' + i + "_int"] = flag;
       // this.listNull2['gr_dtl_plt_ttl_' + i + "_required"] = flag;
       // this.listNull2['gr_dtl_plt_ttl_' + i + "_int"] = flag;

    }

    // Filter form only show choose one
    private filterForm2 ($target, flag = 'required') {

        try {

            switch (flag) {

                case 'required_int_greater0':

                    this.listNull2[$target.id + "_required"] = false;
                    this.listNull2[$target.id + "_int"] = false;
                    this.listNull2[$target.id + "_greater0"] = false;

                    if ($target.value == "") {

                         this.listNull2[$target.id + "_required"] = true;

                    } else if(!this.validateInt($target.value)){

                        this.listNull2[$target.id + "_int"] = true;

                    } else if ($target.value < 1) {

                        this.listNull2[$target.id + "_greater0"] = true;
                    }

                    break;

                case 'required_int':

                    this.listNull2[$target.id + "_required"] = false;
                    this.listNull2[$target.id + "_int"] = false;

                    if ($target.value == "") {

                        this.listNull2[$target.id + "_required"] = true;

                    } else if(!this.validateInt($target.value)){

                        this.listNull2[$target.id + "_int"] = true;

                    }
                    break;

                case 'int_greater0':

                    this.listNull2[$target.id + "_int"] = false;
                    this.listNull2[$target.id + "_greater0"] = false;

                    if(!this.validateInt($target.value) && $target.value != ''){

                        this.listNull2[$target.id + "_int"] = true;

                    } else if ($target.value < 1  && $target.value != '') {

                        this.listNull2[$target.id + "_greater0"] = true;
                    }

                    break;

                case 'int':
                    if (!this.validateInt($target.value) && flag == 'int') {

                        this.listNull2[$target.id + "_int"] = true;

                    } else {

                        this.listNull2[$target.id + "_int"] = false;
                    }
                    break;
                default:
                    break;

            }

            return false;

        } catch (err) {

            return false;
        }


    }

    private removeRequired($target) {

        if ($target.value != "") {

            this.listNull2[$target.id + "_required"] = false;

        }else{
          this.listNull2[$target.id + "_required"] = true;
        }

    }

    // Cal disc
    private calDisc ($event, index) {


        jQuery("#dtl_gr_dtl_disc_" + index).html('');
        if (this.validateNumber($event.target.value) && $event.target.value != '') {

            var act_ctn = parseInt($event.target.value);
            var ctn = parseInt(this.dataGoodReceipt['asn_details'][index]['dtl_ctn_ttl']);

            var tmpCal = act_ctn - ctn;

            if(isNaN(tmpCal)) {

                tmpCal = 0;
            }

            var html  = '<span style="color: #D91E18">' + tmpCal + '</span>';

            if (tmpCal == 0) {

                var html  = '<span>' + tmpCal + '</span>';
            }

            jQuery("#dtl_gr_dtl_disc_" + index).html(html);

        }

    }

    // Validate float
    private validateFloat(float ) {

        var re = /^([0-9,.]*)$/;
        return re.test(float);

    }

    // Validate Int
    private validateInt(number = '') {

        var re = /^([0-9]*)$/;
        var re2 = /^0([0-9]*)$/;
        if (re.test(number)) {

            if (number.length > 1 && re2.test(number)) {

                return false;

            }

            return true;
        }

        return false;
    }

    // Validate number
    private validateNumber(number) {

        var re = /^([0-9]*)$/;
        return re.test(number);
    }

    // value = .. return true
    private validateMultidot(float) {

        var rgx = /\.\./;
        return (rgx.test(float));
    }

    private changeColor($event, objectText = '') {

        if (objectText == 'input') {

            jQuery("#gr_dtl_is_dmg_" + index).prop("checked", true);
            var index = $event.target.id.replace("gr_dtl_is_dmg_", "");
            jQuery($event.target).hide();
            jQuery("#checked_" + index).show();
        }else if (objectText == 'check') {

            var index = $event.target.id.replace("checked_", "");
            jQuery($event.target).hide();
            jQuery("#gr_dtl_is_dmg_" + index).show();
            jQuery("#gr_dtl_is_dmg_" + index).prop("checked", false);
        }

    }

    private redirectToList(){
        let n = this;
        this._boxPopupService.showWarningPopup()
            .then(function (dm) {
                if(dm)
                    n._router.parent.navigateByUrl('/goods-receipt');
            })
    }

    private calTotalEP() {

        var target = this;
        var tmpTotal = 0;
        jQuery('form').find("input[id^=gr_dtl_act_ctn_ttl_]").each(function(j, obj) {

            if (obj.value != '') {

                tmpTotal += parseInt(obj.value);
                target.listNull2['gr_dtl_act_ctn_ttl_' + j +'_less9999'] = false;
            }
        });
        if (tmpTotal > 99999) {

            jQuery('form').find("input[id^=gr_dtl_act_ctn_ttl_]").each(function(j, obj) {

                target.listNull2['gr_dtl_act_ctn_ttl_' + j +'_less9999'] = true;
            });
        }
    }
    private checkLessAct(i) {

        this.listNull2['gr_dtl_dmg_ttl_' + i +'_lessAct'] = false;

        var act = jQuery("#gr_dtl_act_ctn_ttl_" + i).val() != '' ? parseInt(jQuery("#gr_dtl_act_ctn_ttl_" + i).val()): 0;
        var dmg_ttl = jQuery("#gr_dtl_dmg_ttl_" + i).val() != '' ? parseInt(jQuery("#gr_dtl_dmg_ttl_" + i).val()): 0;

        if (dmg_ttl > act) {

            this.listNull2['gr_dtl_dmg_ttl_' + i +'_lessAct'] = true;
        }

        if (this.dataGoodReceipt['asn_details'][i] != undefined) {

            this.dataGoodReceipt['asn_details'][i]['tmpActXdoc'] = act;
            this.listNull2['gr_dtl_dmg_ttl_' + i +'_lessActDock'] = false;
            this.dataGoodReceipt['asn_details'][i]['tmpActXdoc'] = act - this.dataGoodReceipt['asn_details'][i]['dtl_crs_doc'];
            if ((act - dmg_ttl) < this.dataGoodReceipt['asn_details'][i]['dtl_crs_doc'] && (act - dmg_ttl) >= 0 && jQuery("#gr_dtl_dmg_ttl_" + i).val() != '') {

                this.listNull2['gr_dtl_dmg_ttl_' + i +'_lessActDock'] = true;
            }

            this.listNull2['gr_dtl_dmg_ttl_' + i +'_lessActDock2'] = false;
            if ((act - this.dataGoodReceipt['asn_details'][i]['dtl_crs_doc'] < 0) && jQuery("#gr_dtl_act_ctn_ttl_" + i).val() != '') {

                this.listNull2['gr_dtl_dmg_ttl_' + i +'_lessActDock2'] = true;
            }
        }
    }

    private removeCalcelItem(data:Array<any>=[]){

        let tmp= data.slice();

        data.forEach((item)=>{
            if(item['asn_dtl_sts_code'].toLowerCase() == 'cc'){
                let index=tmp.indexOf(item);
                if (index > -1) {
                    tmp.splice(index, 1);
                }
            }
        })

        return tmp;

    }
}
