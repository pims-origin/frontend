import {Component,Input, Output,EventEmitter} from '@angular/core';
import { FORM_DIRECTIVES,ControlGroup,Control,ControlArray,FormBuilder,Validators} from '@angular/common';
import {Router, RouteData, RouteParams } from '@angular/router-deprecated';
import {UnsetDamageServices} from "./unset-damage-service";
import { ValidationService } from '../../common/core/validator';
import {UserService, Functions} from '../../common/core/load';
import {WMSPagination} from '../../common/directives/directives';
declare var jQuery: any;
@Component ({
    selector: 'unset-damage-directive',
    directives:[FORM_DIRECTIVES,WMSPagination],
    providers: [UnsetDamageServices,ValidationService],
    templateUrl: 'unset-damage-template.html'
})

export class UnsetDamageDirective {

    private whs_id;
    private UnsetDamageDeatail={data:[],checkAll:false};
    private gdId:any;
    private Pagination={};
    private DamagedCartonList:Array<any>=[];
    private DamagedCartonDetail={};
    private showLoadingOverlay:boolean=false;
    private perPage=20;


    @Output() print = new EventEmitter();
    @Output() messages = new EventEmitter();


    constructor(
        private fb: FormBuilder,
        private _Valid: ValidationService,
        private params: RouteParams,
        private _Func: Functions,
        private _service:UnsetDamageServices) {
        this.whs_id=this._Func.lstGetItem('whs_id');
        this.gdId=this.params.get('id');
        this.getData();
        this.getDamagedCarton();
    }

    private getData(page=''){

        let param='?limit='+this.perPage+'&page='+page;
        this.showLoadingOverlay=true;
        this._service.getGoodReceiptList(this.gdId,param).subscribe(
            data => {
                this.UnsetDamageDeatail['data']=data['data']['damagedCartons'];
                this.DamagedCartonDetail=data.data;
                this.showLoadingOverlay=false;
                if(data['meta']){
                    this.Pagination=data['meta']['pagination'];
                    this.Pagination['numLinks']=3;
                    this.Pagination['tmpPageCount']=this._Func.Pagination(this.Pagination);
                }
            },
            err => {
                this.parseError(err);
            }
        );

    }

    private changeDamagedNote(item={}){

        let data={};
        data['data']= { ...this.DamagedCartonDetail };
        data['data']['damagedCartons']=[];
        data['data']['damagedCartons'].push(item);
        this.showLoadingOverlay=true;

        setTimeout(()=>{
            this._service.updateDamageCarton(JSON.stringify(data)).subscribe(data=>{
                this.showLoadingOverlay=false;
                let msg=this._Func.Messages('success',this._Func.msg('DMCT002'));
                this.messages.emit(msg);
            },err=>{
                this.parseError(err);
            });
        })


    }

    private Unsetdamaged(){

        let data={};
        data['data']= { ...this.DamagedCartonDetail };
        data['data']['damagedCartons']=this.UnsetDamageDeatail['data'];
        this.setUnsetDamaged(data['data']['damagedCartons']);
        let countSelected = 0;
        for(let item of data['data']['damagedCartons']) {
            if(item['selected']) {
                countSelected ++;
            }
        }
        if(!countSelected) {
            let msg=this._Func.Messages('danger', 'Please select cartons to unset damage.');
            this.messages.emit(msg);
        }
        else {
            this.showLoadingOverlay=true;
            this._service.unsetDamaged(JSON.stringify(data)).subscribe(data=>{

                this.showLoadingOverlay=false;
                let msg=this._Func.Messages('success',this._Func.msg('DMCT001'));
                this.messages.emit(msg);
                this.getData();

            },err=>{

                this.parseError(err);

            });
        }
    }

    private setUnsetDamaged(data:Array<any>=[]){

        data.forEach((item)=>{
            if(item['selected']){
                item['unset']=true;
            }else{
                item['unset']=false;
            }
        });

    }

    private getDamagedCarton(){

        let param='?limit=999';
        this._service.getDamagedCartonList(param).subscribe(
            data => {
                this.DamagedCartonList=data;
            },
            err => {
                this.parseError(err);
            }
        );

    }

    private selectedItem($event:any,item={}){
        this._Func.checkedItem(this.UnsetDamageDeatail,item,$event);
    }

    private checkAll($event){
        this._Func.checkedAll(this.UnsetDamageDeatail,$event);
    }

    private checkInputNumber(event)
    {
        this._Valid.isNumber(event);
    }


    private onPageSizeChanged($event:any)
    {
        this.perPage = $event.target.value;
        if(this.Pagination){
            if((this.Pagination['current_page'] - 1) * this.perPage >= this.Pagination['total']) {
                this.Pagination['current_page'] = 1;
            }
            this.getData(this.Pagination['current_page']);
        }

    }

    private parseError(err){
        let msg={'status' : 'danger', 'txt' : this._Func.parseErrorMessageFromServer(err)};
        this.messages.emit(msg);
    }

    private _print(){

        this.print.emit({});

    }

    private setClickedRow(item) {
        item.selected = !item.selected;
    }

}
