import {Component, Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {Functions, API_Config} from '../../common/core/load';


@Injectable()

export class PbServices {

    private headerGet = this._func.AuthHeader();
    private headerPostJson = this._func.AuthHeaderPostJson();
    private urlAPIContainer = this._api.API_Container;
    private urlAPIItem = this._api.API_ItemMaster;
    private urlAPIMeasurements = this._api.API_Measurements;
    private urlAPICustomer = this._api.API_Customer;
    private urlAPIAsn = this._api.API_Asns;
    private urlAPIUoms = this._api.API_UOM;
    private urlAPIGoodsReceipt = this._api.API_Goods_Receipt;
    private urlAPIUsers = this._api.API_Users;
    private urlAPICustomerMaster = this._api.API_CUSTOMER_MASTER;
    private urlEventTracking = this._api.API_GOODS_RECEIPT_MASTER + '/eventracking';

    constructor(private _func:Functions,
                private _api:API_Config,
                private _http:Http) {

    }

    // Save ASN
    public saveAsn(params) {

        return this._http.post(this.urlAPIAsn, params, {headers: this.headerPostJson})
            .map(res => res.json())

    }

    getUOM(param) {
        return this._http.get(this._api.API_UOM + param, {headers: this.headerGet})
            .map((res:Response) => res.json());
    }

    // Edit ASN
    public editAsn($idAsn, params) {

        return this._http.put(this.urlAPIAsn + "/" + $idAsn, params, {headers: this.headerPostJson})
            .map(res => res.json())

    }

    // Edit ASN for new containers
    public editAsnNewCtns($idAsn, params, $idCtn = 0) {

        return this._http.put(this.urlAPIAsn + "/" + $idAsn + "/containers/" + $idCtn, params, {headers: this.headerPostJson})
            .map(res => res.json())

    }

    public saveGoodReceipt(params) {

        return this._http.post(this.urlAPIGoodsReceipt, params, {headers: this.headerPostJson})
            .map(res => res.json())
    }

    // Get customers
    public getCustomers($params = '') {

        return this._http.get(this.urlAPICustomer + $params, {headers: this.headerGet})
            .map(res => res.json());

    }

    // Get customers
    public getCustomersByUser($user_id, $params = '') {

        return this._http.get(this.urlAPIUsers + '/info/' + $user_id + $params, {headers: this.headerGet})
            .map(res => res.json());

    }

    // Get customers by WH
    public getCustomersByWH($params) {

        return this._http.get(this.urlAPICustomerMaster + '/customer-warehouses' + $params + '&sort[cus_name]=asc', {headers: this.headerGet})
            .map(res => res.json());

    }

    // Get customers by WH
    public getCustomersByUserWH($params) {

        return this._http.get(this.urlAPICustomerMaster + '/customers' + $params + '&sort[cus_name]=asc', {headers: this.headerGet})
            .map(res => res.json());

    }

    public getEventTrackingByAsn($params) {

        return this._http.get(this.urlEventTracking + $params, {headers: this.headerGet})
            .map(res => res.json());

    }

    // Get uoms
    public getUoms($params) {

        return this._http.get(this.urlAPIUoms + $params + '&sort[sys_uom_name]=asc&sys_uom_type=item', {headers: this.headerGet})
            .map(res => res.json());

    }

    // Get measurements
    public getMeasurements() {

        return this._http.get(this.urlAPIMeasurements, {headers: this.headerGet})
            .map(res => res.json());

    }

    // Get containers
    public getContainersByASN(asnId) {

        return this._http.get(this.urlAPIAsn + '/' + asnId + '/list-containers', {headers: this.headerGet})
            .map(res => res.json());

    }

    // Autocomplete for search
    public getItemBySKU(sku, cus_id = '') {

        return this._http.get(this.urlAPIItem + '?sku=' + sku + '&cus_id=' + cus_id, {headers: this.headerGet})
            .map(res => res.json().data);


    }

    // Autocomplete for search by name
    public getContainerByName($params) {

        return this._http.get(this.urlAPIContainer + $params, {headers: this.headerGet})
            .map(res => res.json().data);


    }

    // Get container
    public getContainer($params = '') {

        return this._http.get(this.urlAPIContainer + $params, {headers: this.headerGet})
            .map(res => res.json());

    }

    // Get list ReturnOrder
    public getListReturnOrder($params = '') {

        return this._http.get(this._api.API_ORDERS + '/return-orders/' + $params, {headers: this.headerGet})
            .map(res => res.json());

    }

    public getReturOrderById($id) {

        return this._http.get(this._api.API_ORDERS_V2 + '/putback/return-order-details/'  + $id , {headers: this.headerGet})
            .map(res => res.json());
    }

}
