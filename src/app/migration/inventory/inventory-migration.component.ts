import {Component, ViewChild} from '@angular/core';
import {ControlGroup, FormBuilder, Control, Validators} from '@angular/common';
import {Router} from '@angular/router-deprecated';
import {UserService} from "../../common/users/users.service";
import {MigrationService} from "../migration.service";
import {Functions, API_Config} from '../../common/core/load';
import {WMSBreadcrumb, WMSMessages} from '../../common/directives/directives';

declare var $:any;
declare var saveAs:any;
declare var swal:any;

@Component({
	selector: 'inventory-migration',
	providers: [MigrationService],
	directives: [WMSBreadcrumb, WMSMessages],
	templateUrl: 'inventory-migration.component.html',
})

export class InventoryMigrationComponent {

	@ViewChild('itemFileView') itemFileView: any;
	private showLoadingOverlay:boolean = false;
	private canAccess:boolean = false;
	private messages:any;
	private customers:Array<any>;
	private importForm:ControlGroup;
	private imported:boolean = false;
	private itemFile:any;

	constructor(private userService:UserService,
	            private migrationService:MigrationService,
	            private funcs:Functions,
	            private api:API_Config,
	            private router:Router,
	            private fb:FormBuilder) {
		this.buildFormImport();
		this.checkPermission();
	}

	buildFormImport() {
		this.importForm = this.fb.group({
			cus_id: ['', Validators.required]
		});
	}

	private checkPermission() {
		this.showLoadingOverlay = true;
		this.userService.GetPermissionUser().subscribe(
			data => {
				this.showLoadingOverlay = false;
				this.canAccess = this.userService.RequestPermission(data, 'importInventory');
				if (this.canAccess) {
					this.getCustomers();
				} else {
					// redirect to deny page.
					this.router.parent.navigateByUrl('/deny');
				}
			},
			err => {
				this.messages = this.funcs.Messages('danger', this.funcs.parseErrorMessageFromServer(err));
				this.showLoadingOverlay = false;
			}
		);
	}

	private getCustomers() {
		this.showLoadingOverlay = true;
		this.migrationService.getCustomersByWH().subscribe(
			data => {
				this.showLoadingOverlay = false;
				this.customers = data['data'];
			},
			err => {
				this.showLoadingOverlay = false;
				this.messages = this.funcs.Messages('danger', this.funcs.parseErrorMessageFromServer(err));
			}
		)
	}

	private changeInventoryFile(event) {
		this.itemFile = <Array<File>> event.target.files[0];
	}

	private importFile() {
		const that = this;
		that.imported = true;
		that.messages = null;

		if (that.importForm.valid && this.itemFile) {
			let formData:any = new FormData();
			let xhr = new XMLHttpRequest();

			// process file
			const fileExtension = that.itemFile.name.substr(that.itemFile.name.length - 4);
			if (fileExtension !== "xlsx" && fileExtension !== ".csv") {
				that.messages = that.funcs.Messages('danger', that.funcs.msg('CF003'));
				return;
			}

			formData.append("file", that.itemFile, that.itemFile.name);
			// formData.append('cus_id', that.importForm.value['cus_id']);

			that.showLoadingOverlay = true;
			xhr.open("POST", `${that.api.API_MIGRATION}/import/inventory/${that.importForm.value['cus_id']}`, true);
			xhr.setRequestHeader("Authorization", 'Bearer ' + that.funcs.getToken());
			xhr.onreadystatechange = function () {
				if (xhr.readyState == 2) {
					if (xhr.status == 200) {
						xhr.responseType = "blob";
					} else {
						xhr.responseType = "text";
					}
				}

				if (xhr.readyState == 4) {
					if (xhr.status === 200) {
						that.messages = that.funcs.Messages('danger', 'Import file has some errors.');
						var blob = new Blob([this.response], {type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'});
						saveAs.saveAs(blob, 'Inventory_Error.xlsx');
						that.showLoadingOverlay = false;
					} else {
						let msg = '';

						if (xhr.response) {
							try {
								var res = JSON.parse(xhr.response);
								msg = res.errors && res.errors.message ? res.errors.message : res.data.message;
							}
							catch (err) {
								msg = xhr.statusText
							}
						}
						else {
							msg = xhr.statusText;
						}

						if (!msg) {
							msg = that.funcs.msg('VR100');
						}

						if (xhr.status === 201) {
							that.messages = that.funcs.Messages('success', msg);
							that.itemFileView.nativeElement.value = '';
						} else {
							that.messages = that.funcs.Messages('danger', msg);
						}
						that.showLoadingOverlay = false;
					}
				}
			};
			xhr.send(formData);
		}
	}

}
