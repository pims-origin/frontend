
import {Component} from '@angular/core';
import {API_Config} from '../../common/core/API_Config';
import {BaseTable} from '../../common/core/classes_us/base-table';
import {TableFunctions} from '../../common/core/table-functions';
import {DateFunctions} from '../../common/core/functions/date-functions';
import {CustomersService} from '../../master-data/customers/customers-service';
import {CustomerStatusesService} from '../../master-data/customer-statuses/customer-statuses-service';
import {CountryService} from '../../master-data/country/country-service';
import {StateService} from '../../master-data/state/state-service';
import {BaseService} from '../../common/core/classes_us/base-service';
import {TableStructure} from '../../common/directives/directives';

@Component ({
    selector: 'customers',
    directives: [
        TableStructure
    ],
    templateUrl: '../../common/directives/table-structure/table.component.html',
    providers: [
        BaseService,
        TableFunctions,
        DateFunctions,
        CustomersService,
        CustomerStatusesService,
        CountryService,
        StateService
    ],
})

export class CustomersComponent extends BaseTable
{
    public tableID = 'customers';
    public url = this._api.API_CUSTOMERS_REPORTS;
    public headerUrlSuffix = 'rep';
    public title = 'Search Customers';
    public permissionPage = 'viewReport';

    public headerDef = [
        {
            id: 'ver_table',
            value: 1 + Math.random() * 10000
        },
        this.columnList.customer,
        this.columnList.customerStatus,
        this.columnList.customerCode,
        this.columnList.customerCuntry,
        this.columnList.customerState,
        this.columnList.customerCityName,
        this.columnList.customerPostalCode,
        this.columnList.customerBillingAccount,
        this.columnList.customerDescription,
        this.columnList.netTerms,
        this.columnList.customerContactFirstName,
        this.columnList.customerContactLastName,
        this.columnList.customerContactPosition,
        this.columnList.customerContactDepartment,
        this.columnList.customerContactEmail,
        this.columnList.customerContactPhone,
        this.columnList.customerContactExtention,
        this.columnList.customerContactMobile,
        this.columnList.warehouseCity,
    ];

    public searches = [
        this.searchList.customer,
        this.searchList.customerCode,
        this.searchList.customerStatus,
        this.searchList.country,
        this.searchList.state
    ];

    public searchFields = {
        Country: 'cus_country_id',
        State: 'cus_state_id'
    };

    /*
    ****************************************************************************
    */

    constructor (
        public _api: API_Config,
        public _tableFunc: TableFunctions,
        public _tableService: BaseService,
        public _customersService: CustomersService,
        public _customerStatusesService: CustomerStatusesService,
        public _countryService: CountryService,
        public _stateService: StateService
    )
    {
        super();

        this._tableFunc.permissionCall(this);
    }

    //**************************************************************************

}
