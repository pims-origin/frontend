import {Component, Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {Functions, API_Config} from '../../../common/core/load';

@Injectable()
export class PickingUpdateServices {

  public AuthHeader = this._Func.AuthHeader();
  public AuthHeaderPost = this._Func.AuthHeaderPost();

  constructor(
    private _Func: Functions,
    private _http: Http,
    private _api: API_Config) {
  }

  searchLocation(param) {
    return this._http.get(this._api.API_Wavepick + "/suggest-actual-locations" + param,{ headers: this.AuthHeader})
    	.map((res: Response) => res.json());
  }

  getWavepickDetail(whs_id,wv_id) {
    return this._http.get(this._api.API_Wavepick_2 + '/whs/'+whs_id+'/wave-pick-details/list?wv_id=' + wv_id,{ headers: this.AuthHeader })
      .map((res: Response) => res.json());
  }

  getSuggestLocation(param,id)
  {
    return this._http.get(this._api.API_Wavepick + '/'+param+'/' + id,{ headers: this.AuthHeader })
        .map((res: Response) => res.json());
  }

  /*
  *
  * */
  getMoreSuggestLocation(param)
  {
    return this._http.get(this._api.API_Wavepick + '/more-suggest-location' + param,{ headers: this.AuthHeader })
        .map((res: Response) => res.json());
  }


  public updateWavepickItems($id, params) {
      return this._http.put(this._api.API_Wavepick + "/update-wave-pick/" + $id + "/no-ecom" , params , {headers: this._Func.AuthHeaderPostJson()})
          .map(res => res.json())
  }

  public ckLocaton(params) {
    return this._http.post(this._api.API_Wavepick + "/order-picking/checkLocation" , params , {headers: this._Func.AuthHeaderPostJson()})
        .map(res => res.json())
  }

  public autoConsolidate(params) {
    return this._http.post(this._api.API_Wavepick + "/order-picking/autoConsolidate" , params , {headers: this._Func.AuthHeaderPostJson()})
        .map(res => res.json())
  }

  public checkActualLocations(data) {
    return this._http.post(this._api.API_Wavepick + "/check-actual-locations" , data , {headers: this._Func.AuthHeaderPostJson()})
        .map(res => res.json())
  }

  autoAssignCartonToOrder(whs_id, wv_id) {
    return this._http.get(`${this._api.API_ORDERS_V2}/whs/${whs_id}/order/wave/${wv_id}/auto-assign-cartons`, { headers: this.AuthHeader })
      .map((res: Response) => res.json());
  }
}
