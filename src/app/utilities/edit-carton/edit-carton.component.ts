import {Component} from '@angular/core';
import {EditCartonService} from "./edit-carton.service";

@Component ({
    selector: 'edit-carton',
    templateUrl: 'edit-carton.component.html',
    providers: [EditCartonService]
})

export class EditCartonComponent {

    public carton = '';
    public cartonList = [];
    public selectedCarton = null;
    public showLoading = false;
    constructor(private _editCarton: EditCartonService){
        // this.getCartonDetail('251039');
    }

    filter() {
        if ( this.carton !== '') {
            this.getCartonNumList(this.carton);
        } else {
            this.cartonList = [];
        }

    }

    select(item) {
        this.selectedCarton = item;
        this.carton = item.ctn_num;
        this.cartonList = [];

        if(this.selectedCarton) {
            this.getCartonDetail(this.selectedCarton.ctn_id);
        }
    }

    getCartonNumList(cartonNum) {
        this.showLoading = true;
        this._editCarton.getCartonNum(cartonNum)
            .subscribe(
                result => {
                    this.showLoading = false;
                    this.cartonList = result.data;
                },
                err => {
                    this.showLoading = false;
                }
            )
    }

    cartonDetail = {};
    showGetCartonLoading = false;
    getCartonDetail(cartonID) {
        this.showGetCartonLoading= true;
        this._editCarton.getCartonDetail(cartonID)
            .subscribe(
                result => {
                    this.showGetCartonLoading = false;
                    this.cartonDetail = result.data[0];
                },
                err => {
                    this.showGetCartonLoading = false;
                }
            )
    }
}