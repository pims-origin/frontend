import {Component,Input, Output,EventEmitter} from '@angular/core';
import {ControlGroup, FORM_DIRECTIVES, FormBuilder, Validators, Control, ControlArray} from "@angular/common";
import {WMSMessages} from "../../../../common/directives/messages/messages";
import {ValidationService} from "../../../../common/core/validator";
import { API_Config,Functions} from '../../../../common/core/load';
import {Services} from './courier.service';
import {PaginatePipe,PaginationControlsCmp,PaginationService} from '../../../../common/directives/directives';
import {GoBackComponent} from "../../../../common/component/goBack.component";
import {FormBuilderFunctions} from "../../../../common/core/formbuilder.functions";
import {BoxPopupService} from "../../../../common/popup/box-popup.service";
import {WarningBoxPopup} from "../../../../common/popup/warning-box-popup";
declare var $:any;

@Component ({
    selector: 'courier-directive',
    directives:[GoBackComponent,PaginationControlsCmp,WMSMessages],
    providers: [BoxPopupService,Services,FormBuilderFunctions,ValidationService, FormBuilder],
    pipes: [PaginatePipe],
    templateUrl: 'courier.html'
})

export class courierFormDirective {

    public Loading = [];
    private _cus_id:any;
    private whs_id = localStorage.getItem('whs_id');

    @Input() action: string;
    @Output() messages =  new EventEmitter();
    @Output() success =  new EventEmitter();
    @Output() showLoadingOverlay=  new EventEmitter();
    @Output() changed =  new EventEmitter();


    @Input('cus_id') set cus_id(value:any){
            this.getCourierByCusId(value);
            this._cus_id=value;
    }

    @Input('submitted') set submitted(value:any){
        if(value){
            this.Save();
        }
    }




    constructor(
        private fb:FormBuilder, private _Valid:ValidationService,
        private _Func: Functions,
        private services:Services,
        private _boxPopupService:BoxPopupService,
        private fbdFunc:FormBuilderFunctions,
        private _API: API_Config) {

        this.getCourierName();

        this.CourierItemArray.valueChanges.subscribe((data)=>{
            this.fbdFunc.checkDuplicatedKey(this.CourierItemArray,['courier_name']);
            this.changed.emit(this.CourierItemArray);
        })

    }


    CourierItem:ControlGroup[] = [];
    CourierItemArray:ControlArray = new ControlArray(this.CourierItem);

    private resetCourierItem(){

        while(this.CourierItemArray.controls.length){
            this.CourierItemArray.removeAt(0);
        }

    }

    private addCourierItem(data={}){

        this.CourierItemArray.push(
            new ControlGroup({
                'courier_name': new Control(data['courier_name'] ? data['courier_name'] : '',Validators.compose([Validators.required])),
                'acc_num' :  new Control(data['acc_num'] ? data['acc_num'] : '', Validators.compose([Validators.required, this._Valid.validateSpace])),
                'pay_type': new Control(data['pay_type'] ? data['pay_type'] : '', Validators.compose([Validators.required]))
            })
        );

    }

    private Save(){

            let data = {
                qualifier: 'ACN',
                value : this.CourierItemArray.value
            }

            if(this.CourierItemArray.valid){

                let n = this;
                this.showLoadingOverlay.emit(true);
                this.services.saveCourier(this._cus_id, JSON.stringify(data)).subscribe(
                    data => {
                       this.success.emit(true);
                    },
                    err => {
                        this.showLoadingOverlay.emit(false);
                        this.parseError(err);
                    },
                    () => {}
                );


            }

    }

    private courierName:Array<any>=[];
    private getCourierName(){

        let param = '/search?md_type=CR&md_code=&md_name=&md_sts=&page=1&limit=20';

        this.services.searchCourierName(param).subscribe(
            data => {
                this.courierName = data.data;
            },
            err => {
                this.parseError(err);
            },
            () => {}
        );

    }

    private getCourierByCusId(cus_id:any){
        let param = cus_id+'?qualifier=ACN&limit=20';
        this.services.getCourierByCusId(param).subscribe(
            data => {

                if(data['data'].length){

                    data['data'].forEach((item)=>{
                        this.addCourierItem(item);
                    })

                }
                else{
                    this.addCourierItem();
                }

            },
            err => {
                this.parseError(err);
            },
            () => {}
        );


    }

    deleteItem() {

        let n = this;
        if (this.fbdFunc.getItemchecked(this.CourierItemArray) > 0) {

            /*case 1: modify text*/
            let warningPopup = new WarningBoxPopup();
            warningPopup.text = this._Func.msg('PBAP002');
            this._boxPopupService.showWarningPopup(warningPopup)
                .then(function (dm) {
                    if (!dm) {
                        return;
                    } else {
                        n.fbdFunc.deleteItem(n.CourierItemArray).subscribe(deletedAll=>{
                            if(deletedAll) {
                                n.addCourierItem();
                            }
                        });
                    }
                });
        }
        else {

            this.messages.emit(this._Func.Messages('danger', this._Func.msg('VR113')));

        }
    }

    private isNumber(evt, int:boolean = false) {
        this._Valid.isNumber(evt, int);
    }


    private parseError(err){
        this.messages.emit({'status' : 'danger', 'txt' : this._Func.parseErrorMessageFromServer(err)});
    }
}
