import {Component} from "@angular/core";
import { Router, RouterLink, ROUTER_DIRECTIVES } from '@angular/router-deprecated';
import { FORM_DIRECTIVES,ControlGroup,FormBuilder, Control,Validators} from '@angular/common';
import {API_Config, UserService, Functions} from '../common/core/load';
import { Http } from '@angular/http';
import {ConsolidationServices} from './consolidation-service';
import { ValidationService } from '../common/core/validator';
import {WMSPagination,AgGridExtent,WMSMessages,WMSBreadcrumb} from '../common/directives/directives';
import {WarningBoxPopup} from "../common/popup/warning-box-popup";
import {BoxPopupService} from "../common/popup/box-popup.service";
declare var jQuery: any;
declare var jsPDF: any;
@Component({
  selector: 'consolidation',
  providers:[ConsolidationServices,ValidationService,BoxPopupService],
  directives: [ROUTER_DIRECTIVES,AgGridExtent,WMSPagination,WMSMessages,WMSBreadcrumb],
  templateUrl: 'consolidation.component.html',
})
export class ConsolidationComponent{
  private unCheckall:boolean=false;
  private listSelected:Array<any>=[];
  private headerURL = this.apiService.API_User_Metas+'/con';
  private dataForRenderTable=null;
  private columnData = {
    item_id_ck:{
      attr:'checkbox',
      title:'#',
      width:20,
      pin:true,
      ver_table:'9/14/23tsr'
    },
    ctn_num:{
      title:'Carton Number',
      width:155,
      pin:true
    },
    item_id: {
      title:'Item ID',
      width:70,
      pin:true
    },
    sku: {
      title:'SKU',
      width:120,
      pin:true
    },
    asn_dtl_lot:{
      title: 'LOT',
      width:80,
      pin:true
    },
    size : {
      title: 'Size',
      width:90,
      pin:true
    },
    color:{
      title: 'Color',
      width:90
    },
    asn_dtl_pack:{
      title: 'Pack Size',
      width:90
    },
    uom_name:{
      title: 'UOM',
      width:80
    },
    asn_dtl_width: {
      title: 'W-IN',
      width:70
    },
    asn_dtl_height:  {
      title: 'H-IN',
      width:70
    },
    asn_dtl_weight:  {
      title: 'WE-LB',
      width:70
    },
    ctn_id:{
      hidden:true
    }
  };
  private Pagination;
  private perPage=20;
  private whs_id;
  private messages;
  private Loading={};
  private searching={};
  private ListAutoCompleteData={};
  consolidationForm:ControlGroup;
  private Consolidateby='Pallet';
  private consolidationModel={};
  private listContainerNum:Array<any>=[];
  private sortData={fieldname:'item_id', sort:'asc'};
  private showLoadingOverlay:boolean=false;
  private errControlStorge={};

  constructor(
    private consService:ConsolidationServices,
    private _Func: Functions,
    private fb: FormBuilder,
    private  apiService:API_Config,
    private _user:UserService,
    private _http: Http,
    private _Valid: ValidationService,
    private _router: Router,
    private _boxPopupService: BoxPopupService) {
    this.checkPermission();
    this.consolidationForm=fb.group({
      Location:fb.group({
        from_loc_code: ['', Validators.compose([Validators.required, this._Valid.validateSpace])],
        from_loc_id: [null,Validators.required],
        to_loc_id: [null,Validators.required],
        to_loc_code: ['', Validators.compose([Validators.required, this._Valid.validateSpace])],
        ctn_id:[null],
        ctn_num:[null]
      }),
      Pallet:fb.group({
        from_plt_num: ['', Validators.compose([Validators.required, this._Valid.validateSpace])],
        from_plt_id: [null,Validators.required],
        to_plt_id: [null,Validators.required],
        to_plt_num: ['', Validators.compose([Validators.required, this._Valid.validateSpace])],
        ctn_id:[null],
        ctn_num:[null]
      })
    });
    /*
     * Get Warehouse id current
     * */
    if(localStorage.getItem('whs_id'))
    {
      this.whs_id=localStorage.getItem('whs_id');
    }

    // Build error storge
    this.buildErrorStorge('Location','from_loc_code');
    this.buildErrorStorge('Location','to_loc_code');

  }
  // Check permission for user using this function page
  private consolidate;
  private allowAccess:boolean=false;
  private checkPermission() {
    this.showLoadingOverlay = true;
    this._user.GetPermissionUser().subscribe(
      data => {
        this.showLoadingOverlay = false;
        this.consolidate = this._user.RequestPermission(data, 'consolidate');
        if(this.consolidate){
          this.allowAccess=true;
          // add for show "No rows to show"
          this.getListItemOfLocation(0,0);
        }
        else{
          this.redirectDeny();
        }
      },
      err => {
        this.messages=this._Func.Messages('danger', this._Func.parseErrorMessageFromServer(err));
        this.showLoadingOverlay = false;
      }
    );
  }
  redirectDeny(){
    this._router.parent.navigateByUrl('/deny');
  }
  private firstSearch:number=0;
  autoSearchComplete(loadingName,ListDataName,controlCode,controlId,key,keycompare)
  {
    // reset field

    this.setControlGroup(this.Consolidateby,controlId).updateValue('');
    this.exitsLocation[controlCode]=null;
    let param;
    let searchsubscribeParam;
    let keyw=this._Func.trim(key);
    if(this.Consolidateby=='Location')
    {
      param='?loc_code='+encodeURIComponent(keyw)+'&without=ECO&limit=20';
      searchsubscribeParam=this.consService.SearchLocation(this.whs_id,param);
    }else{
      param='?plt_num='+encodeURIComponent(keyw)+'&without=ECO&limit=20';
      searchsubscribeParam=this.consService.SearchPallet(param);
    }
    if(keyw)
    {
      let n=this;
      setTimeout(function(){
        // do Search
        n.searching[loadingName]=true;
        searchsubscribeParam.subscribe(
          data => {
            n.firstSearch++;
            // disable loading icon
            n.searching[loadingName]=false;
            n.ListAutoCompleteData[ListDataName] = data.data;
            /* ###  ListDataName:Array<any>,controlCode,controlId,key,keycompare */
            n.autoSelectMatchItem(data.data, controlCode,controlId,keyw,keycompare);
          },
          err =>{
            n.searching[loadingName]=false;
          },
          () => {}
        );
      }, 100);
    }
  }
  private exitsLocation={};
  selectedItem(controlCode,controlId,data,keycompare,regetList=true)
  {
    //console.log('keycompare' , keycompare);
    this.exitsLocation[controlCode]=true;
    if(keycompare=='loc_code'){
      (<Control>this.consolidationForm.controls['Location']['controls'][controlCode]).updateValue(data['loc_code']);
      (<Control>this.consolidationForm.controls['Location']['controls'][controlId]).updateValue(data['loc_id']);
      this.checkLoc_Status('Location',controlCode,data['loc_sts_code']);
    }else{
      (<Control>this.consolidationForm.controls['Pallet']['controls'][controlCode]).updateValue(data['plt_num']);
      (<Control>this.consolidationForm.controls['Pallet']['controls'][controlId]).updateValue(data['plt_id']);
    }
    if(controlCode=='from_loc_code'&&regetList){
      this.getListItemOfLocation(this.Pagination['current_page'],this.consolidationForm.value['Location']['from_loc_id']);
    }
    if(controlCode=='from_plt_num'){
      this.getListItemOfPallet(this.Pagination['current_page'],this.consolidationForm.value['Pallet']['from_plt_id']);
    }
  }
  checkExitsKey(controlGroup:string,control:string)
  {
    let value=this.getValueControl(controlGroup,control);
    //console.log('getValueControl ' ,controlGroup , control , value);
    if(this.exitsLocation[control]!==true&&value)
    {
      //console.log('set false for control ', control);
      this.exitsLocation[control]=false;
    }
  }
  // Function will find the item exist and auto select it
  autoSelectMatchItem(ListDataName:Array<any>,controlCode,controlId,key,keycompare,regetList=true){
    for(let item in ListDataName){
      //console.log('compare ', item , ListDataName[item], ' --- ', key ,keycompare , ListDataName[item][keycompare]==key);
      if(this._Func.trim(ListDataName[item][keycompare])==this._Func.trim(key))
      {
        this.selectedItem(controlCode,controlId,ListDataName[item],keycompare,regetList);
        return;
      }
    }
  }
  private getValueControl(ctrlGroup,control)
  {
    //console.log('value control ', this.consolidationForm.value[ctrlGroup]);
    return this.consolidationForm.value[ctrlGroup][control];
  }
  private resetErrorforEmptyControl(ctrlGroup,control)
  {
    let value=this.getValueControl(ctrlGroup,control);
    if(!value){
      //console.log('reset error for control' ,(<Control>this.consolidationForm.controls[ctrlGroup]));
      (<Control>this.consolidationForm.controls[ctrlGroup]['controls'][control]).setErrors(null);
    }
  }
  private onPageSizeChanged($event)
  {
    this.perPage = $event.target.value;
    if((this.Pagination['current_page'] - 1) * this.perPage >= this.Pagination['total']) {
      this.Pagination['current_page'] = 1;
    }
    let consoby=this.Consolidateby;
    if(consoby=='Location')
    {
      this.getListItemOfLocation(this.Pagination['current_page'],this.getValueControl(consoby,'from_loc_id'))
    }else{
      this.getListItemOfPallet(this.Pagination['current_page'],this.getValueControl(consoby,'from_plt_id'))
    }
  }
  private checkActiveConsolidation()
  {
    this.messages=false;
    let controlGroup=this.Consolidateby;
    if(controlGroup=='Pallet')
    {
      this.setControlGroup('Location','from_loc_code').setErrors(null);
      this.setControlGroup('Location','to_loc_code').setErrors(null);
      this.checkExitsKey(controlGroup,'from_plt_num');
      this.checkExitsKey(controlGroup,'to_plt_num');
      this.resetErrorforEmptyControl('Location','from_loc_code');
      this.resetErrorforEmptyControl('Location','to_loc_code');
    }
    else{
      this.setControlGroup('Pallet','from_plt_num').setErrors(null);
      this.setControlGroup('Pallet','to_plt_num').setErrors(null);
      this.checkExitsKey(controlGroup,'from_loc_code');
      this.checkExitsKey(controlGroup,'to_loc_code');
      this.resetErrorforEmptyControl('Pallet','from_plt_num');
      this.resetErrorforEmptyControl('Pallet','to_plt_num');

      // check loc stt
      this.geterrControlStorge('Location','from_loc_code');
      this.geterrControlStorge('Location','to_loc_code');
    }
  }

  geterrControlStorge(ControlGroup,control)
  {

    if(this.errControlStorge[ControlGroup][control])
    {
      let errStorge =  this.errControlStorge[ControlGroup][control];
      let that=this;
      Object.keys(errStorge).forEach(function(key) {
        let err={};
        err[key]=errStorge[key];
        that.setErrorsControl(ControlGroup,control,err);
      });
    }


  }

  checkErrorForm()
  {
    let controlGroup=this.Consolidateby;
    if(controlGroup=='Location')
    {
      if(!this.getValueControl(controlGroup,'from_loc_code')){
        this.setControlGroup(controlGroup,'from_loc_code').setErrors({required :true});
      }
      if(!this.getValueControl(controlGroup,'to_loc_code')){
        this.setControlGroup(controlGroup,'to_loc_code').setErrors({required :true});
      }
    }
    else{
      if(!this.getValueControl(controlGroup,'from_plt_num')){
        this.setControlGroup(controlGroup,'from_plt_num').setErrors({required :true});
      }
      if(!this.getValueControl(controlGroup,'to_plt_num')){
        this.setControlGroup(controlGroup,'to_plt_num').setErrors({required :true});
      }
    }
  }
  /*
   * reLocation
   * */
  private submitForm:boolean=false;
  private listitemLocation:Array<any>=[];
  private flagLoading2:boolean=false;
  getListItemOfLocation(page=null,idLocation)
  {
    this.showLoadingOverlay = true;
    let params="?sort["+this.sortData['fieldname']+"]="+this.sortData['sort']+"&page="+page+"&limit="+this.perPage;
    this.consService.getItemLocation(idLocation,params)
      .subscribe(
        data => {
          this.showLoadingOverlay= false;
          this.listitemLocation = data.data;
          this.dataForRenderTable=this._Func.formatData(data.data);
          //pagin function
          if(typeof  data['meta']!=='undefined')
          {
            this.Pagination=data['meta']['pagination'];
            this.Pagination['tmpPageCount']=this._Func.Pagination(this.Pagination);
          }
        },
        err => {
          this.Loading['getList']= false;
          this.parseError(err);
        },
        () => {
        }
      );
  }
  private listitemPallet :any[];
  private sortDataPallet={fieldname:'plt_code',sort:'asc'}
  getListItemOfPallet(page=null,idPallet)
  {
    this.showLoadingOverlay = true;
    //let params='?page='+page+'&sort[plt_code]=asc&limit='+this.perPage;
    let params="?sort["+this.sortDataPallet['fieldname']+"]="+this.sortDataPallet['sort']+"&page="+page+"&limit="+this.perPage;
    this.consService.getListItemOfPallet(idPallet,params)
      .subscribe(
        data => {
          this.showLoadingOverlay= false;
          this.listitemPallet = data.data;
          this.dataForRenderTable=this._Func.formatData(data.data);
          //pagin function
          if(typeof  data['meta']!=='undefined')
          {
            this.Pagination=data['meta']['pagination'];
            this.Pagination['tmpPageCount']=this._Func.Pagination(this.Pagination);
          }
        },
        err => {
          this.Loading['getList']= false;
          this.parseError(err);
        },
        () => {
        }
      );
  }
  sortList($event)
  {
    let consoby=this.Consolidateby;
    if(consoby=='Location')
    {
      if(this.getValueControl(consoby,'from_loc_id')){
        this.sortData=$event;
        this.getListItemOfLocation(this.Pagination['current_page'],this.getValueControl(consoby,'from_loc_id'));
      }
    }else{
      if(this.getValueControl(consoby,'from_plt_id')) {
        this.sortDataPallet = $event;
        this.getListItemOfPallet(this.Pagination['current_page'], this.getValueControl(consoby, 'from_plt_id'));
      }
    }
  }


  consoliDation()
  {
    this.checkActiveConsolidation();
    this.submitForm=true;
    this.checkErrorForm();
    let formValid;
    let paramConso;
    let data=this.consolidationForm.value;
    let msgInline=this.msgConsolidation();
    // process data
    data[this.Consolidateby]['ctn_id']=this.listCNTiD();
    data[this.Consolidateby]['ctn_num']=this.listCNTNum();
    //console.log(' Consolidateby ', data[this.Consolidateby]);
    let d=data[this.Consolidateby];
    let params=JSON.stringify(d);
    //console.log(params);
    if(this.Consolidateby=='Location'){
      formValid=this.consolidationForm.controls['Location'].valid;
      paramConso=this.consService.consolidationbyLocation(params);
      // check same  from_loc_code
      if(this.checkSameValue(this.consolidationForm.value['Location']['from_loc_code'],this.consolidationForm.value['Location']['to_loc_code'])&&formValid){
        this.messages = this._Func.Messages('danger',msgInline['location']);
        return;
      }
    }else{
      formValid=this.consolidationForm.controls['Pallet'].valid;
      paramConso=this.consService.consolidationbyPallet(params)
      // check same  from_loc_code
      if(this.checkSameValue(this.consolidationForm.value['Pallet']['from_plt_num'],this.consolidationForm.value['Pallet']['to_plt_num'])&&formValid){
        this.messages = this._Func.Messages('danger',msgInline['pallet']);
        return;
      }
    }
    if(formValid&&this.listSelected.length)
    {
      // reset control
      //this.removeItemAfterConsolidation();
      // push list selected to data

      //

      this.showLoadingOverlay= true;
      paramConso.subscribe(
        data => {
          this.submitForm=false;
          this.showLoadingOverlay= false;
          // reget list
          this.regetListItem();
          if(this.Consolidateby=='Location'){
            this.messages = this._Func.Messages('success',d['ctn_id'].length + (d['ctn_id'].length == 1 ? ' carton has' : ' cartons have') + ' been consolidated from '+ d['from_loc_code'] +' to '+ d['to_loc_code'] +'');
          }else{
            this.messages = this._Func.Messages('success',d['ctn_id'].length + (d['ctn_id'].length == 1 ? ' carton has' : ' cartons have') + ' been consolidated from '+ d['from_plt_num'] +' to '+ d['to_plt_num'] +'');
          }
        },
        err => {
          this.showLoadingOverlay= false;
          this.submitForm=false;
          if(err.json().errors.message){
          }
          let errcode=err.json().errors.code;
          let errsttcode=err.json().errors.status_code;
          let loc_id_inv;
          // get id to active
          if(this.Consolidateby=='Location'){
            loc_id_inv=d['to_loc_id'];
          }else{
            loc_id_inv=d['to_plt_id'];
          }
          
          // case : Not active || only inactive
          // if(errcode==1){
          //   let that = this;
          //   let n = this;
          //   let warningPopup = new WarningBoxPopup();
          //   warningPopup.text= this._Func.msg('RLC001');;
          //   this._boxPopupService.showWarningPopup(warningPopup)
          //     .then(function (yes) {
          //       if(yes){
          //         n.activeLocation(loc_id_inv);
          //       }
          //     });
          // }
          // else{
          //   this.messages = this._Func.Messages('danger',err.json().errors.message);
          // }
          // fixed bug WMS2-4218
          this.messages = this._Func.Messages('danger',err.json().errors.message);
        },
        () => {
        }
      );
    }else{
      //console.log('List selected ', this.listSelected);
      if(!this.listSelected.length)
      {
        this.messages = this._Func.Messages('danger', this._Func.msg('CONS001'));
      }
    }
  }

  private reCheckStatusLoc='';
  recheckLoc()
  {

    if(this.Consolidateby=='Location'&&this.consolidationForm.value['Location']['from_loc_code']&&this.consolidationForm.value['Location']['to_loc_code']) {
      this.messages = this._Func.Messages('warning','Check Current Location status....');
      let param = '?loc_code=' + encodeURIComponent(this.consolidationForm.value['Location']['from_loc_code']) + '&without=ECO&limit=1';
      this.consService.SearchLocation(this.whs_id, param).subscribe(
        data=> {
          this.autoSelectMatchItem(data.data, 'from_loc_code', 'from_loc_id', this.consolidationForm.value['Location']['from_loc_code'], 'loc_code',false);
          this.messages = this._Func.Messages('warning','Check New Location status....');
          let param = '?loc_code=' + encodeURIComponent(this.consolidationForm.value['Location']['to_loc_code']) + '&without=ECO&limit=1';
          this.consService.SearchLocation(this.whs_id, param).subscribe(
            data=> {
              this.messages  = null;
              this.autoSelectMatchItem(data.data, 'to_loc_code', 'to_loc_id', this.consolidationForm.value['Location']['to_loc_code'], 'loc_code',false);
              this.consoliDation();
            },
            err=> {
            },
            ()=> {
            }
          );

        },
        err=> {
        },
        ()=> {
        }
      );
    }
    else{
      this.consoliDation();
    }

  }

  /*
   *Active loc
   */
  activeLocation(loc_id){
    this.Loading['activeloc'] = true;
    this.consService.activeLoc(loc_id).subscribe(
      data => {
        this.consoliDation();
      },
      err => {
        let errMsg = err.json().message || err.json().errors.message;
        if(errMsg){
          this.messages = this._Func.Messages('danger',errMsg);
        }
        else{
          this.messages = this._Func.Messages('danger',this._Func.msg('VR111'));
        }
      },
      () => {}
    );
  }
  private  checkSameValue(a,b){
    return a==b ? true : false;
  }
  private removeItemAfterConsolidation(){
    let arrTemp=this.dataForRenderTable.slice();
    for(let i in this.listSelected)
    {
      let index=this._Func.getIndexOf(this.dataForRenderTable,this.listSelected[i]['ctn_id'],'ctn_id');
      arrTemp.splice( index , 1 );
    }
    this.dataForRenderTable=arrTemp;
  }
  private listCNTiD()
  {
    let idArray:Array<any>=[];
    //console.log('listSelected' , this.listSelected);
    this.listSelected.forEach((item)=>{
      idArray.push(item['ctn_id']);
    });
    return idArray;
  }
  private listCNTNum()
  {
    let containerNum:Array<any>=[];
    //console.log('listSelected' , this.listSelected);
    this.listSelected.forEach((item)=>{
      containerNum.push(item['ctn_num']);
    });
    return containerNum;
  }
  /*
   * When user change select button
   * Reget List item
   * */
  regetListItem()
  {
    // reset list selected
    this.listSelected=[];
    let consoby=this.Consolidateby;
    this.dataForRenderTable=[];
    this.Pagination={};
    if(consoby=='Location')
    {
      if(this.getValueControl(consoby,'from_loc_id')){
        //console.log('reget list item of location');
        this.getListItemOfLocation(this.Pagination['current_page'],this.getValueControl(consoby,'from_loc_id'))
      }
    }else{
      if(this.getValueControl(consoby,'from_plt_id')){
        //console.log('reget list item of pallet');
        this.getListItemOfPallet(this.Pagination['current_page'],this.getValueControl(consoby,'from_plt_id'))
      }
    }
  }
  // Show error when server die or else
  private parseError (err) {
    err = err.json();
    this.messages = this._Func.Messages('danger', err.errors.message);
  }
  private setControlGroup(controlGroup:string,control:string){
    return (<Control>this.consolidationForm.controls[controlGroup]['controls'][control]);
  }

  checkLoc_Status(controlGroup,control,loc_code)
  {

    if(loc_code=='LK')
    {
      this.setErrorsControl(controlGroup,control,{'loc_status_is_locked':true});
      // save to storge local data
      this.errControlStorge[controlGroup][control]={};
      this.errControlStorge[controlGroup][control]['loc_status_is_locked']=true;

      // break
      return;
    }
    if(loc_code=='IA')
    {
      // show popup change stt
      // break
      return;
    }
    if(loc_code=='CL')
    {
      this.setErrorsControl(controlGroup,control,{'loc_status_is_locked_cycle':true});

      // create new object value
      this.errControlStorge[controlGroup][control]={};
      this.errControlStorge[controlGroup][control]['loc_status_is_locked_cycle']=true;
      // break
      return;
    }

    // if not belong any condition above
    this.errControlStorge[controlGroup][control]={};

  }
  /*=====================================================
   setErrorsControl
   *=====================================================*/
  setErrorsControl(controlGroup,control,err){
    (<Control>this.consolidationForm.controls[controlGroup]['controls'][control]).setErrors(err);
  }

  buildErrorStorge(controlGroup,control)
  {
    // int object child
    if(typeof this.errControlStorge[controlGroup]=='undefined')
    {
      this.errControlStorge[controlGroup]={};
    }
    this.errControlStorge[controlGroup][control]=null;
  }

  private msgConsolidation(){
    let msg={
      location:"New location is the same as current location!",
      pallet:"New pallet is the same as current pallet!"
    }
    return msg;
  }
  private changeConsolidateby(event)
  {
  }
}
