import {Component} from '@angular/core';
import {Router, RouterLink, RouteConfig, ROUTER_DIRECTIVES} from '@angular/router-deprecated';
import {CruZoneComponent} from "./cru-zone/cru-zone.component";
import {ZoneListComponent} from "./zone-list/zone-list.component";




@Component ({
    selector: 'zone-management',
    directives: [ROUTER_DIRECTIVES],
    template: `<router-outlet></router-outlet>`,
})

@RouteConfig([
    { path: '/new', component: CruZoneComponent  , name: 'Add Zone' , data: {Action: 'New'}},
    { path: '/:id/edit', component: CruZoneComponent  , name: 'Edit Zone' , data: {Action: 'Edit'}},
    { path: '/:id', component: CruZoneComponent  , name: 'View Zone', data: {Action: 'View'}},
    { path: '/', component: ZoneListComponent  , name: 'Zone List', useAsDefault: true}

])
export class ZoneManagementComponent {


}
