export class PutawayAssignmentMessages{
  public messages = {
  	"remove_none": "Please choose one item to remove",
  	"remove_confirm": "Are you sure?",
  	"agree": "Yes, delete it!",
  	"cancel": "No, cancel!",
  	"edit_none": "Please choose one item to edit",
  	"edit_multiple": "Please choose only one item to edit",
  	"print_none": "Please choose one item to print",
  	"print_multiple": "Please choose only one item to print",
    "connection": "Sorry, there's a problem with the connection."
  }
}
