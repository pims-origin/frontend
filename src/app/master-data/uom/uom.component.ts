import {Component} from '@angular/core';
import {Router, RouterLink, RouteConfig, ROUTER_DIRECTIVES} from '@angular/router-deprecated';
import {UOMListComponent} from "./uom-list/uom-list.component";
import {CruUOMComponent} from "./cru-uom/cru-uom.component";

@Component ({
    selector: 'uom',
    directives: [ROUTER_DIRECTIVES],
    template: `<router-outlet></router-outlet>`,
})

@RouteConfig([
    { path: '/', component:UOMListComponent  , name: 'UOM List', useAsDefault: true},
    { path: '/new', component:CruUOMComponent  , name: 'Add UOM', data: {action: 'new'}},
    { path: ':id', component:CruUOMComponent  , name: 'View UOM',data: {action: 'view'}},
    { path: ':id/edit', component:CruUOMComponent  , name: 'Edit UOM', data: {action: 'edit'}},
])
export class UOMComponent {


}
