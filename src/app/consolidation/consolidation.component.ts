import { Component } from "@angular/core";
import { Router, RouterLink, ROUTER_DIRECTIVES } from '@angular/router-deprecated';
import { FORM_DIRECTIVES, ControlGroup, FormBuilder, Control, Validators } from '@angular/common';
import { API_Config, UserService, Functions } from '../common/core/load';
import { Http } from '@angular/http';
import { ConsolidationServices } from './consolidation-service';
import { ValidationService } from '../common/core/validator';
import { WMSMessages, WMSBreadcrumb } from '../common/directives/directives';

@Component({
  selector: 'consolidation',
  providers: [ConsolidationServices, ValidationService],
  directives: [ROUTER_DIRECTIVES, WMSMessages, WMSBreadcrumb],
  templateUrl: 'consolidation.component.html',
})

export class ConsolidationComponent {
  private messages;
  private Loading = {};
  private searching = {};
  private ListAutoCompleteData = {};
  consolidationForm: ControlGroup;
  private Consolidateby = 'Pallet';
  private consolidationModel = {};
  private showLoadingOverlay: boolean = false;
  private submitForm: boolean = false;

  constructor(
    private consService: ConsolidationServices,
    private _Func: Functions,
    private fb: FormBuilder,
    private apiService: API_Config,
    private _user: UserService,
    private _http: Http,
    private _Valid: ValidationService,
    private _router: Router) {
    this.checkPermission();
    this.consolidationForm = fb.group({
      lpn_from: ['', Validators.compose([Validators.required, this._Valid.validateSpace])],
      lpn_to: ['', Validators.compose([Validators.required, this._Valid.validateSpace])]
    });
  }
  // Check permission for user using this function page
  private consolidate;
  private allowAccess: boolean = false;
  private checkPermission() {
    this.showLoadingOverlay = true;
    this._user.GetPermissionUser().subscribe(
      data => {
        this.showLoadingOverlay = false;
        this.consolidate = this._user.RequestPermission(data, 'consolidate');
        if (this.consolidate) {
          this.allowAccess = true;
        }
        else {
          this.redirectDeny();
        }
      },
      err => {
        this.messages = this._Func.Messages('danger', this._Func.parseErrorMessageFromServer(err));
        this.showLoadingOverlay = false;
      }
    );
  }

  redirectDeny() {
    this._router.parent.navigateByUrl('/deny');
  }

  private getValueControl(control) {
    return this.consolidationForm.value[control];
  }
  
  private checkActiveConsolidation() {
    this.messages = false;
    this.checkExistPallet('lpn_from');
    this.checkExistPallet('lpn_to');
  }

  consoliDation() {
    this.checkActiveConsolidation();
    this.submitForm = true;
    let paramConso;
    let data = this.consolidationForm.value;
    let params = JSON.stringify(data);
    //console.log(params);
    // check same  from_loc_code
    if (this.checkSameValue(this.consolidationForm.value['lpn_from'], this.consolidationForm.value['lpn_to']) && this.consolidationForm.valid) {
      this.messages = this._Func.Messages('danger', 'To Loc/LPN is the same as From Loc/LPN!');
      return;
    }
    
    if (this.consolidationForm.valid) {
      paramConso = this.consService.callConsolidation(params)
      this.showLoadingOverlay = true;
      paramConso.subscribe(
        data => {
          this.submitForm = false;
          this.showLoadingOverlay = false;
          this.messages = this._Func.Messages('success', data['message']);
        },
        err => {
          this.showLoadingOverlay = false;
          this.submitForm = false;
          // fixed bug WMS2-4218
          this.messages = this._Func.Messages('danger', this._Func.parseErrorMessageFromServer(err));
        },
        () => {
        }
      );
    }
  }
  
  private checkSameValue(a, b) {
    return a == b ? true : false;
  }

  // Show error when server die or else
  private parseError(err) {
    err = err.json();
    this.messages = this._Func.Messages('danger', err.errors.message);
  }

  autocompleteLPNLoc(loadingName, ListDataName, controlCode, key, keycompare) {
    this.existPallet[controlCode] = null;
    let param;
    let searchsubscribeParam;
    let keyw = this._Func.trim(key);
    
    if (keyw) {
      keyw = keyw.toUpperCase();
      param = '?loc_lpn_carton=' + encodeURIComponent(keyw) + '&limit=20';
      searchsubscribeParam = this.consService.autocompleteLPNLoc(param);
      let n = this;
      setTimeout(function () {
        // do Search
        n.searching[loadingName] = true;
        searchsubscribeParam.subscribe(
          data => {
            // disable loading icon
            n.searching[loadingName] = false;
            n.ListAutoCompleteData[ListDataName] = data.data;
            /* ###  ListDataName:Array<any>,controlCode,key,keycompare */
            n.autoSelectMatchItem(data.data, controlCode, keyw, keycompare);
          },
          err => {
            n.searching[loadingName] = false;
          },
          () => { }
        );
      }, 100);
    }
  }

  private existPallet = {};
  selectedPallet(controlCode, data) {
    this.existPallet[controlCode] = true;
    const field = data['is_lpn'] === 1 ? 'lpn_carton' : 'loc_name';
    (<Control>this.consolidationForm.controls[controlCode]).updateValue(data[field]);
  }

  checkExistPallet(control: string) {
    let value = this.getValueControl(control);
    if (this.existPallet[control] !== true && value) {
      this.existPallet[control] = false;
    }
  }

  // Function will find the item exist and auto select it
  autoSelectMatchItem(ListDataName: Array<any>, controlCode, key, keycompare) {
    for (let item in ListDataName) {
      if (this._Func.trim(ListDataName[item][keycompare]) == this._Func.trim(key)) {
        this.selectedPallet(controlCode, ListDataName[item]);
        return;
      }
    }
  }
}
