import {Component, Input} from '@angular/core';
import {DashboardServices} from '../dashboard.service';
import { Router } from '@angular/router-deprecated';
import { DataShareService } from '../../common/services/data-share.service';
@Component({
    selector: 'alert-bar-dashboard',
    templateUrl: 'alert-bar-dashboard.html',
    providers: [DashboardServices]
})

export class AlertBarDashboard {
    private whsId:string = '';
    private alertBarInfo:any = {};
    private capacity:any = {};

    @Input('currentWHSId') set setCurrentWHSId(value:any) {
        this.whsId = value;
        if (value) {
            setTimeout(() => {
                this.getAlertBarInfo();
            }, 200);
        }
    }

    constructor(private dashboardServices: DashboardServices, private _router: Router, private dataShareService: DataShareService) {
    }

    private getAlertBarInfo() {
        this.dashboardServices.getAlertBarInfo().subscribe(
            data => {
                this.alertBarInfo = data['data'];
                this.capacity = this.alertBarInfo['capacity'];
            },
            err => {}
        )
    }

    private redirectPage(url, data) {
        this.dataShareService.data = data;
        this._router.navigateByUrl(url);
    }
}
