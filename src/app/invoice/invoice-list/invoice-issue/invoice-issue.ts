
import {API_Config} from '../../../common/core/load';
import {DateFunctions} from '../../../common/core/functions/date-functions';
import {BaseClass} from '../../../common/core/classes_us/base-class';
import {BaseService} from '../../../common/core/classes_us/base-service';

export class InvoiceIssue extends BaseClass
{
    protected _invoiceHeaderComponent;
    protected _invoiceBodyComponent;

    protected dateCaption = '';

    protected showLoadingOverlay = true;

    protected invoiceData = {
        inv_num: '',
        storRange: '',
        summary: {
            netOrder: '',
            discount: '',
            freight: '',
            salesTax: '',
            balanceDue: ''
        }
    };

    protected action = 'create';
    protected cancellable = false;

    constructor (
        protected _api: API_Config,
        protected _dateFunctions: DateFunctions,
        protected _baseService: BaseService
    )
    {
        super();
    }

    //**************************************************************************

    public showInvoiceIssue(invoiceData)
    {
        this.invoiceData = invoiceData;

        setTimeout(() => {

            this._invoiceHeaderComponent.gridUpdate({
                cus_name: invoiceData['cus_name'],
                cus_code: invoiceData['billTo']['cus_code'],
                inv_num: invoiceData['inv_num'],
                inv_dt: invoiceData['inv_dt'],
                terms: invoiceData['billTo']['net_terms']
            });

            this._invoiceBodyComponent.gridUpdate(invoiceData);

        }, 500);
    }

    //**************************************************************************

    public printInvoice()
    {
        let cus_id = this.invoiceData['cus_id'],
            inv_num = this.invoiceData['inv_num'],
            startDate = this.invoiceData['startDate'],
            endDate = this.invoiceData['endDate'];

        let url = this._api.API_Store_Invoice + '/' + inv_num + '/' + cus_id,
            params = JSON.stringify(this.invoiceData);

        this.showLoadingOverlay = true;

        this._baseService.post(url, params)
            .subscribe(
                data => {

                    if (data) {

                        let message = 'Error invoicing (possible duplicate billing)';

                        for (let cat in data) {

                            let title = cat == 'Receiving' ?
                                'Received Containers' : 'Order Numbers';

                            message += '<br>' + title + ': <strong>' +
                                data[cat].join(', ') + '</strong>';
                        }

                        this._invoiceHeaderComponent.alert.show(message);
                    }
                },
                err => {

                    let filename = 'Invoice ' + inv_num + '.pdf',
                        printUrl = this._api.API_Print_Invoice
                                + '?inv_num=' + inv_num + '&cus_id=' + cus_id
                                + '&startDate=' + startDate + '&endDate=' + endDate;

                    this.action = 'print';

                    this._baseService.print(printUrl, filename);

                    this.showLoadingOverlay = false;
                },
                () => {
                    this.showLoadingOverlay = false;
                }
        );
    }

    //**************************************************************************

    public goBack(action)
    {
        window.top.close();
    }

    //**************************************************************************

}


