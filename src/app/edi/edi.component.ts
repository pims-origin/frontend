import {Component} from '@angular/core';
import {Router, RouterLink, RouteConfig, ROUTER_DIRECTIVES} from '@angular/router-deprecated';
import {EDIListComponent} from "./edi-list/edi-list.component";
import {CruEDIComponent} from "./cru-edi/cru-edi.component";

@Component ({
    selector: 'edi',
    directives: [ROUTER_DIRECTIVES],
    template: `<router-outlet></router-outlet>`,
})

@RouteConfig([
    { path: '/', component:EDIListComponent  , name: 'EDI List', useAsDefault: true},
    { path: '/:id/', component:EDIListComponent  , name: 'View EDI' , data:{action:'view'}},
    { path: '/:id/edit', component:EDIListComponent  , name: 'Edit EDI' , data:{action:'edit'}},
    { path: '/new', component:CruEDIComponent  , name: 'New EDI' , data:{action:'new'}}

])
export class EDIComponent {

}
