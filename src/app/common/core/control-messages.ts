import { Component, Input } from '@angular/core';
import { ControlGroup, Control } from '@angular/common';
import { ValidationService } from './validator';

@Component({
  selector: 'control-messages',
  template: `<div *ngIf="errorMessage !== null">{{errorMessage}}</div>`
})
export class ControlMessages {
  private errorMessage: string;
  @Input() control;
  constructor() {

    this.errorMessages();

  }

   errorMessages() {

    return null;
  }
}

