import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {API_Config, Functions} from '../../common/core/load';

@Injectable()
export class DamagedCartonReportService {
	private authHeader:any;
	private authHeaderPost:any;
	private whsId:string;

	constructor(private func:Functions, private api:API_Config, private http:Http) {
		this.authHeader = func.AuthHeader();
		this.authHeaderPost = func.AuthHeaderPost();
		this.whsId = localStorage.getItem('whs_id');
	}

	getReports(params:string = '') {
		return this.http.get(`${this.api.API_DAMAGED_CARTON_REPORT}/search?whs_id=${this.whsId}` + params, {headers: this.authHeader})
			.map(res => res.json());
	}

	getReportAPIUrl(params:string = '') {
		return `${this.api.API_DAMAGED_CARTON_REPORT}/export-csv?whs_id=${this.whsId}${params}`;
	}

	getCustomers(params:string = '') {
		return this.http.get(`${this.api.API_CUSTOMER_DAMAGED}?whs_id=${this.whsId}` + params, {headers: this.authHeader})
			.map(res => res.json());
	}

	getCTNRNums(params:string = '') {
		return this.http.get(`${this.api.API_DAMAGED_CARTON_REPORT}/containers?whs_id=${this.whsId}` + params, {headers: this.authHeader})
			.map(res => res.json());
	}

	getGRNums(params:string = '') {
		return this.http.get(`${this.api.API_DAMAGED_CARTON_REPORT}/good-receipts?whs_id=${this.whsId}` + params, {headers: this.authHeader})
			.map(res => res.json());
	}

	getDamagedTypes(params:string = '') {
		return this.http.get(`${this.api.API_DAMAGED_CARTON_REPORT}/damage-type?${this.whsId}` + params, {headers: this.authHeader})
			.map(res => res.json());
	}

	getSKUs(params:string = '') {
		return this.http.get(`${this.api.API_DAMAGED_CARTON_REPORT}/sku?whs_id=${this.whsId}` + params, {headers: this.authHeader})
			.map(res => res.json());
	}
}
