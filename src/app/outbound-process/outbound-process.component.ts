import {Component} from '@angular/core';
import {RouteConfig, ROUTER_DIRECTIVES} from '@angular/router-deprecated';
import {OrderManagementComponent} from "./order/order.component";
import {CSRListComponent} from "./csr/csr-list/csr-list.component";
import {AllocateManagementComponent} from "./allocate/allocate.component";
import {WavePickListComponent} from "./wave-pick/wave-pick-list/wave-pick-list.component";
import {PickingManagementComponent} from "./picking/picking.component";
import {PackingManagementComponent} from "./packing/packing.component";
import {WorkOrder} from "./work-order/work-order.component"
import {ShippingManagementComponent} from "./shipping/shipping.component";
import {onlineOrderComponent} from "./onlineOrder/online-order.component";
import {BillOfLadingsManagementComponent} from "./bill-of-ladings/bill-of-lading.component";
import {PalletAssignmentManagementComponent} from "./pallet-assignment/pallet-assignment.component";
import {CancelOrderManagementComponent} from "./cancel-order/cancel-order.component";
import {WavepickAssignmentManagementComponent} from "./wave-pick-assignment/wavepick-assignment.component";
import {CancelOrderBySKUComponent} from "./cancel-order-by-sku/cancel-order-by-sku.component";

import {RevertOrderComponent} from "./revert-order/revert-order.component";
import {AssignMultiplePickersComponent} from "./picking/assign-multiple-pickers/assign-multiple-pickers.component";

@Component ({
    selector: 'outbound-process',
    directives: [ROUTER_DIRECTIVES],
    template: `<router-outlet></router-outlet>`
})

@RouteConfig([
    { path: 'order/...', component: OrderManagementComponent, name: 'Order Management', useAsDefault: true },
    { path: 'csr', component:CSRListComponent , name: 'CSR', data: {page: 'assignCSR'} },
    { path: 'allocate/...', component:AllocateManagementComponent , name: 'Allocate' },
    { path: 'wave-pick', component:WavePickListComponent , name: 'Wave Pick', data: {page: 'wavepick'} },
    { path: 'picking/...', component:PickingManagementComponent , name: 'Picking Management' },
    { path: 'packing/...', component:PackingManagementComponent , name: 'Packing Management' },
    { path: 'work-order/...', component:WorkOrder , name: 'Work Order' },
    { path: 'shipping/...', component:ShippingManagementComponent , name: 'Shipping Management' },
    { path: 'online-order', component:onlineOrderComponent , name: 'Online Order',data: {page: 'onlineOrder'} },
    { path: 'bill-of-ladings/...', component:BillOfLadingsManagementComponent , name: 'Bill Of Ladings'},
    { path: 'pallet-assignment/...', component:PalletAssignmentManagementComponent , name: 'Pallet Assignment Management'},
    { path: 'cancel-order/...', component:CancelOrderManagementComponent , name: 'Cancel Order Management'},
    { path: 'wave-pick-assignment/...', component:WavepickAssignmentManagementComponent , name: 'Wave Pick Assignment'},
    { path: 'cancel-order-by-sku/:id', component:CancelOrderBySKUComponent , name: 'Cancle Order by SKU'},
    { path: 'order-revert/:id', component:RevertOrderComponent , name: 'Order Revert'},
    { path: 'assign-pickers/:id', component:AssignMultiplePickersComponent , name: 'Assign multiple pickers',data: {action: 'view'}},
])
export class OutboundProcessComponent {

}
