import { Component } from '@angular/core';
import { CORE_DIRECTIVES } from '@angular/common';
import { FORM_DIRECTIVES,ControlGroup,FormBuilder, Control, Validators} from '@angular/common';
import { Http, Headers } from '@angular/http';
import { JwtHelper } from 'angular2-jwt/angular2-jwt';
import {API_Config} from "../../common/core/API_Config";
import { Router, RouterLink, ROUTER_DIRECTIVES, RouteParams } from '@angular/router-deprecated';
import {UserService} from "../../common/users/users.service";
import {Functions} from "../../common/core/functions";
import { OrderBy } from "../../common/pipes/order-by.pipe";
import {WMSMessages} from "../../common/directives/messages/messages";
import { ValidationService } from '../../common/core/validator';
declare var jQuery: any;
declare var html2canvas: any;

@Component({
    selector: 'header',
    pipes: [OrderBy],
    directives: [WMSMessages],
    providers: [ValidationService],
    templateUrl: 'header.component.html'
})
export class HeaderComponent {
    jwt: string = '';
    decodedJwt: string = '';
    messages:any;
    messageReportIssue: any;
    private authHeader = this._Func.AuthHeader();
    private warehouseList: any = [];
    // private whs_id:string = localStorage.getItem('whs_id');
    private whs_id;
    private warehouseName: any;
    private inputReportIssue;
    private fileName = '';
    private fileReportIssue;
    reportIssueForm: ControlGroup;
    private showLoadingReportIssue = false;
    private listNull = {};
    private fileConvertImage = '';
    private sendOtherEmail:boolean = false;

    constructor(
        public _router: Router,
        public _http: Http,
        public jwtHelper: JwtHelper,
        public _API: API_Config,
        private _user: UserService,
        private _Func: Functions,
        private fb: FormBuilder,
        private _params: RouteParams,
        private _Valid: ValidationService
    ) {
        if(localStorage.getItem('jwt')) {
            this.jwt = localStorage.getItem('jwt');
            if(jwtHelper.decodeToken(this.jwt))
            {
                this.decodedJwt = jwtHelper.decodeToken(this.jwt);
                let user = this.decodedJwt,
                    userId = user['jti'];
                this._http.get(this._API.API_User_Info + '/' + userId, { headers: this.authHeader })
                    .map((res) => res.json().data).subscribe(
                    data => {
                        //this.decodedJwt['name'] = data.first_name + ' ' + data.last_name;
                        this.warehouseList = data['user_warehouses'];
                        this.whs_id && this.showWarehouseName();
                        (<Control>this.reportIssueForm.controls['from_email']).updateValue(data.email);
                        (<Control>this.reportIssueForm.controls['phone']).updateValue(data.phone);
                        localStorage.setItem('logedUser', JSON.stringify(data));
                        this._user.User = data;
                    },
                    err => {
                        if(err.status==500)
                        {
                            this.messages = this._Func.Messages('danger', this._Func.msg('VR100'));
                        }
                        else{
                            this.messages = this._Func.Messages('danger', this._Func.msg('VR100'));
                        }
                    }
                );

                this._http.get(this._API.API_WarehouseId_Meta, {headers: this.authHeader})
                    .map((res) => res.json().data).subscribe(
                    data => {
                        localStorage.setItem('whs_id', data);
                        this.whs_id = data;
                        this.warehouseList.length && this.showWarehouseName();
                    },
                    err => {
                        this.warehouseList.length && this.showWarehouseName();
                    }
                );
            }
            else{
                this._router.parent.navigateByUrl('/login');
            }
        }
        else{
            this._router.parent.navigateByUrl('/login');
        }

        this.getEmailReport();
        this.buildReportIssueForm();

        // this.getWarehouse();
        // this.getWarehouseName();

    }

    ngAfterViewInit () {
        this.initReprtIssueForm();
    }

    private convertPageToImage() {

        var target = this;
        html2canvas(jQuery('body'), {
            onrendered: function(canvas) {

                var $html = '<img class="img-responsive" src="' + canvas.toDataURL("image/png") + '"/>';
                jQuery("#showImg").html($html);
            }
        });
    }

    getToken() {

        return localStorage.getItem('jwt');
    }
    RemoveToken() {

        this._http.get(this._API.API_Logout, {
            headers: this.authHeader })
            .subscribe(
                data => data,
                err => {},
                () => {}
            );
    }

    logout() {
        //this.router.navigate(['Login']);
        this.RemoveToken();
        localStorage.removeItem('jwt');
        localStorage.removeItem('access');
        localStorage.removeItem('whs_id');
        localStorage.removeItem('logedUser');
        localStorage.removeItem('inputting');
        localStorage.removeItem('local_cus_id');
        this._router.parent.navigateByUrl('/login');
    }

    private getWarehouse() {
        this.messages = {};

        this._http.get(this._API.API_Warehouse + '?limit=200', {
            headers: this.authHeader
        })
            .map(res => res.json().data)
            .subscribe(
                data => {
                    this.warehouseList = data;
                    this.warehouseData(data);
                },
                err => {
                    if(err.status==500)
                    {
                        this.messages = this._Func.Messages('danger', this._Func.msg('VR100'));
                    }
                    else{
                        this.messages = this._Func.Messages('danger', this._Func.msg('VR100'));
                    }
                },
                () => {}
            );
    }

    private warehouseData(data){
        // for(var d of data)
        // {
        // this.warehouseAuto.push(d.whs_name);
        //   }
    }
    // Get Warehouse Name

    private getWarehouseName() {
        this.messages = {};

        this._http.get(this._API.API_Warehouse + '/' + this.whs_id, {
            headers: this.authHeader
        })
            .map(res => res.json())
            .subscribe(
                data => {
                    this.warehouseName = data.whs_name;
                    //this.warehouseFilter(data);

                },
                err => {
                },
                () => {}
            );
    }

    private showWarehouseName(){
        var currentWarehouse;
        if(this.whs_id) {
            for(let i = 0; i < this.warehouseList.length; i++) {
                let warehouse = this.warehouseList[i];
                if(warehouse['whs_id'] == this.whs_id) {
                    currentWarehouse = warehouse;
                    break;
                }
            }
        }
        else {
            currentWarehouse = this.warehouseList[0];
        }
        if(currentWarehouse)
        {
            this.warehouseName = currentWarehouse['whs_name'];
        }
    }

    private SelectWareHouse(item)
    {
        this._http.put(this._API.API_WarehouseId_Meta, JSON.stringify({'value': item.whs_id}), { headers: this._Func.AuthHeaderPostJson() })
            .map((res) => res.json().data).subscribe(
            data => {
            },
            err => {
            }
        );
        localStorage.setItem('whs_id', item.whs_id);
        this.warehouseName = item.whs_name;
        this._router.navigateByUrl('/dashboard');
    }

    private initReprtIssueForm() {
        this.inputReportIssue = jQuery('#report-issue-file');
    }

    private uploadReportIssueFile() {
        jQuery('#report-issue-file').click();

    }

    // Filter form only show choose one
    private filterForm ($target, flag = 'required') {

        try {

            switch (flag) {
                case 'ivEmail':

                    this.listNull[$target.id + '_' + flag] = false;
                    if ($target.value != "" && !this._Valid.validateEmail($target.value)) {

                        this.listNull[$target.id + '_' + flag] = true;

                    }
                    break;
                default:
                    break;


            }

            return false;

        } catch (err) {

            return false;
        }


    }

    private reportIssueFileChange($target) {

        var fullPath = $target.value;
        if (fullPath) {
            var startIndex = (fullPath.indexOf('\\') >= 0 ? fullPath.lastIndexOf('\\') : fullPath.lastIndexOf('/'));
            var filename = fullPath.substring(startIndex);
            if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
                filename = filename.substring(1);
                this.fileName = filename;
            }
            this.fileReportIssue = <Array<File>> $target.files;
        }
        else {
            this.fileName = '';
            this.fileReportIssue = '';
        }
    }

    private buildReportIssueForm() {
        this.reportIssueForm =this.fb.group({
            to_email: 'support@logisoft.com',
            other_email: '',
            from_email: '',
            phone: '',
            des_issue: '',
            flagAttach: false,
        });
    }

    private sendToOtherEmail(event) {
        this.sendOtherEmail = event.target.checked;

        if (!this.sendOtherEmail) {
            (<Control>this.reportIssueForm.controls['other_email']).updateValue('');
            this.filterForm(jQuery("#otherEmail")[0], 'ivEmail');
        }
    }

    private sendReportIssue(data) {

        this.filterForm(jQuery("#fromEmail")[0], 'ivEmail');
        var that = this;

        var submit = true;
        jQuery.each( this.listNull, function( key, value ) {

            if (value) {

                submit = false;
                return false;
            }
        });

        if (!submit) {

            return false;
        }

        var formData: any = new FormData();
        formData.append('email_to', data.to_email);
        formData.append('other_email', data.other_email);
        formData.append('email_from', data.from_email);
        formData.append('phone', data.phone);
        formData.append('content', data.des_issue);

        if (data.flagAttach) {

            formData.append('img_attach_screen', jQuery("#showImg img").attr('src'));
        }

        if(this.fileReportIssue) {
            formData.append("file", this.fileReportIssue[0], this.fileReportIssue[0].name)
        }
        else {
            formData.append("file", '');
        }

        this.showLoadingReportIssue = true;
        var xhr = new XMLHttpRequest();
        xhr.open("POST", this._API.API_MASTER_URL + '/users/report-issue', true);
        xhr.setRequestHeader("Authorization", 'Bearer ' +this._Func.getToken());
        xhr.onreadystatechange = function () {
            if(xhr.readyState === 4) {
                if(xhr.status != 200) {
                    let errMsg = '';
                    if(xhr.response) {
                        try {
                            var res = JSON.parse(xhr.response);
                            if(res.errors) {
                                errMsg = res.errors.message || res.errors.detail || res.errors.title || res.errors.statusText;
                            }
                        }
                        catch(err){errMsg = xhr.statusText}
                    }
                    else {
                        errMsg = xhr.statusText;
                    }
                    if(!errMsg) {
                        errMsg = that._Func.msg('VR100');
                    }
                    that.messageReportIssue = that._Func.Messages('danger', errMsg);
                }
                else {

                    that.messageReportIssue = that._Func.Messages('success', that._Func.msg('SF001'));

                    setTimeout(function () {

                        jQuery('#reportIssuePopup').modal('hide');
                        that.messageReportIssue = false;
                        // Reset form
                        (<Control>that.reportIssueForm.controls['des_issue']).updateValue('');
                        (<Control>that.reportIssueForm.controls['flagAttach']).updateValue(false);
                        that.fileName = '';
                        that.fileReportIssue = '';
                        jQuery('#report-issue-file').val('');
                    }, 3000);

                }

                that.showLoadingReportIssue = false;
            }
        };
        xhr.send(formData);
    }

    private closeModel() {

        (<Control>this.reportIssueForm.controls['des_issue']).updateValue('');
        this.fileName = '';
        jQuery('#report-issue-file').val('');
        jQuery('#reportIssuePopup').modal('hide');
        this.messageReportIssue = false;
    }
    private getEmailReport() {

        let API = this._API.API_SYSTEM_CONFIGURATION + 'smtp';

        this._http.get(API, {headers: this.authHeader})
            .map((res) => res.json().data)
            .subscribe(
                data => {


                    if(data.length > 0){

                        for (var i = 0; i < data.length; i++) {
                            if (data[i]['setting_key'] == 'ER') {

                                (<Control>this.reportIssueForm.controls['to_email']).updateValue(data[i]['setting_value']);
                                break;
                            }
                        }
                    }
                },
                err => {

                },
                () => {

                }
            )
    }

}

