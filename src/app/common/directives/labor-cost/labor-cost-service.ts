import {Component, Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {API_Config,Functions} from '../../../common/core/load';

@Injectable()
export class LaborCostServices {

    public AuthHeader = this._Func.AuthHeader();
    public AuthHeaderPost = this._Func.AuthHeaderPost();

    constructor(private _Func: Functions,
                private _API: API_Config,
                private http: Http) {

    }

    updateLaborCost(param,data){
        return this.http.post(this._API.API_Labor+'/'+param, data, { headers: this._Func.AuthHeaderPostJson() })
            .map((res: Response) => res.json().data);
    }

    getLaborCost(param){
        return  this.http.get(this._API.API_Labor+'/'+param, {headers: this.AuthHeader})
            .map(res => res.json().data);
    }


}
