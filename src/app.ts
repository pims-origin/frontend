import {FORM_PROVIDERS, LocationStrategy, HashLocationStrategy} from '@angular/common';
import {bootstrap} from '@angular/platform-browser-dynamic';
import {provide, enableProdMode} from '@angular/core';
import {Http, HTTP_PROVIDERS} from '@angular/http';
import {ROUTER_PROVIDERS} from '@angular/router-deprecated';
import { AuthConfig, AuthHttp, JwtHelper } from 'angular2-jwt/angular2-jwt';
import {DND_PROVIDERS} from 'ng2-dnd/ng2-dnd';
import {MainApp} from "./app/main-app";
import {SF_PROVIDERS} from './app/common/core/load';
import {Messages} from './app/common/languages/en/messages';

// if (process.env.ENV === ( 'production' || 'development')) {
 enableProdMode();
// }

bootstrap(MainApp, [
  HTTP_PROVIDERS,
  ROUTER_PROVIDERS,
  DND_PROVIDERS,
  Messages,
  FORM_PROVIDERS,
  JwtHelper,
  SF_PROVIDERS,
  provide(LocationStrategy, {useClass: HashLocationStrategy}),
  provide(AuthHttp, {
    useFactory: (http) => {
      return new AuthHttp(new AuthConfig({
        tokenName: 'jwt',
        tokenGetter: (() => localStorage.getItem(this.tokenName)),
        noJwtError: true,
        noTokenScheme: true
      }), http);
    },
    deps: [Http]
  })

])
.catch(err => console.error(err));
