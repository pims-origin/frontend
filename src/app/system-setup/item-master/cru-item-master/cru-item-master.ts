import {Component, ViewChild} from '@angular/core';
import { 
  FORM_DIRECTIVES, ControlGroup, FormBuilder, Control, Validators, ControlArray 
} from '@angular/common';
import {RouteParams,Router ,RouteData} from '@angular/router-deprecated';
import {API_Config, UserService, Functions, FormBuilderFunctions} from '../../../common/core/load';
import { ValidationService } from '../../../common/core/validator';
import {ItemMasterServices} from '../item-master-service';
import { Http } from '@angular/http';
import { ControlMessages } from '../../../common/core/control-messages';
import {GoBackComponent} from "../../../common/component/goBack.component";
import {ErrorMessageForControlService} from "../../../common/services/error-message-for-control.service";
import {BoxPopup} from "../../../common/popup/box-popup";
import {WarningBoxPopup} from "../../../common/popup/warning-box-popup";
import {BoxPopupService} from "../../../common/popup/box-popup.service";
import {JwtHelper} from "angular2-jwt/angular2-jwt";
import {
  WMSBreadcrumb,
  InfiniteScrollAutocomplete,
  DmsIn
} from '../../../common/directives/directives';
import { exists } from 'fs';
import { Messages } from 'src/app/common/languages/en/messages';
declare const jQuery: any;
declare var saveAs:any;

@Component({
  selector: 'cru-item-master',
  providers: [ItemMasterServices,ValidationService,FormBuilder,ErrorMessageForControlService,UserService, BoxPopupService],
  directives: [FORM_DIRECTIVES,ControlMessages,GoBackComponent,WMSBreadcrumb,InfiniteScrollAutocomplete, DmsIn],
  templateUrl: 'cru-item-master.component.html',
})

export class CruItemMasterComponent {

  public Loading = [];
  private messages;
  private Action;
  private idDetail;
  private TitlePage='Item Master';
  private IsView:boolean=false;
  isEdit: boolean = false;
  private disableEdit:boolean = false;
  private ZoneTypeDetail=[];
  private Customers=[];
  private Detail;
  private uomList: Array<any> = [];
  private showError= false;
  private isSubmit = false;
  private ItemMasterForm: ControlGroup;
  private showLoadingOverlay:boolean=false;
  private innerItemsInfo: ControlGroup[] = [];
  private innerItemsArray: ControlArray = new ControlArray(this.innerItemsInfo);
  private perPage = 20;
  private selectedAll = false;
  private packingLevel = 5;
  private specHangling = {

  };
  packItems: any[] = [];
  disableTotalPackSize = false;
  isItemApproved: boolean = false;

  arrStatus = [
    { code: 'RG', name: 'Recv-Waitapp' },
    { code: 'AC', name: 'Recv-Approved' }
  ];
  private isAddFromPopup = false;
  private showAutocompleteLoading = false;

  constructor(
    private _http: Http,
    private _Func: Functions,
    private params: RouteParams,
    private _user:UserService,
    private _Valid: ValidationService,
    private _IMService: ItemMasterServices,
    private _ErrMsgControlService: ErrorMessageForControlService,
    private _boxPopupService: BoxPopupService,
    private fb: FormBuilder,
    private fbFunc: FormBuilderFunctions,
    _RTAction: RouteData,
    private api: API_Config,
    private _router: Router) {

    this.checkPermission();

    this.Action=_RTAction.get('Action');
    if(this.Action.toLowerCase() != 'new') {
      this.showPackingLevelTab = true;
    }
    else {
      for(let i = 1; i <= this.packingLevel; i++) {
        this.newLevelsItem(i);
      }
    }

    this.getUOMList();
    this.getDefaultUOM();
    this.getPackingLevelDropdown();
  }

  private createItem;
  private viewItem;
  private editItem;
  private allowAccess:boolean=false;

  itemImageURL: string = '';

  private checkPermission() {
    this.showLoadingOverlay = true;
    this._user.GetPermissionUser().subscribe(
      data => {

        this.showLoadingOverlay = false;

        this.createItem = this._user.RequestPermission(data, 'createItem');
        this.viewItem = this._user.RequestPermission(data,'viewItem');
        this.editItem = this._user.RequestPermission(data,'editItem');
        this.idDetail = this.params.get('id');

        switch(this.Action.toLowerCase()) {
          case 'view':
              if (!this.viewItem) {
                this.redirectDeny();
              } else {
                this.allowAccess = true;
                this.IsView = true;
                this.TitlePage = 'View Item Master';
                this._IMService.Get_ItemMasterDetail(this.idDetail).subscribe(
                  (data) => {
                    this.Detail = data;

                    this.getItemPacking(this.idDetail);
                    this.GetListCustomer();
                    this.FormBuilderEdit();
                    this.buildItemLevel();
                    // this.getPackageItem(this.idDetail);

                    this.itemImageURL = `${this.api.API_URL_ROOT}/core/item-master/v1/view/${data.itm_img}?token=${this._Func.getToken()}`;
                  },
                  (err) => {
                    this._router.parent.navigateByUrl('/system-setup/item-master');
                  },
                  () => {}
                );
              }
          break;
          case 'new':
            if (!this.createItem) {
              this.redirectDeny();
            } else {
              this.TitlePage = 'Add Item Master';
              this.allowAccess = true;
              this.GetListCustomer();
              this.FormBuilder();
            }
          break;
          case 'edit':
            if (!this.editItem) {
              this.redirectDeny();
            } else {
              this.TitlePage = 'Edit Item Master';
              this.allowAccess = true;
              this.isEdit = true;
              this._IMService.Get_UpdatedItemMaster(this.idDetail).subscribe(
                  (data) => {
                    this.Detail = data;

                    if(this.Detail){
                      this.isItemApproved = this.Detail['status'] && this.Detail['status'] === 'AC' ? true : false;
                    }
                    
                    this.disableEdit = data['inventory'] === 1;
                    this.getItemPacking(this.idDetail);
                    this.GetListCustomer();
                    this.FormBuilderEdit();
                    this.buildItemLevel();

                    this.itemImageURL = `${this.api.API_URL_ROOT}/core/item-master/v1/view/${data.itm_img}?token=${this._Func.getToken()}`;
                  },
                  (err) => {
                    this._router.parent.navigateByUrl('/system-setup/item-master');
                  },
                  () => {}
              );
            }
          break;
        }
      },
      err => {
        this.messages=this._Func.Messages('danger', this._Func.parseErrorMessageFromServer(err));
        this.showLoadingOverlay = false;
      }
    );
  }

  redirectDeny(){
    this._router.parent.navigateByUrl('/deny');
  }

  FormBuilder() {
    let data = JSON.parse(localStorage.getItem('add-item-master'));
    localStorage.removeItem('add-item-master');
    if (data) {
      this.isAddFromPopup = true;
    }
    this.ItemMasterForm = this.fb.group({
      sku: [null, Validators.compose([Validators.required,this._Valid.validateSpace])],
      cus_upc: ['', Validators.compose([this._Valid.checkUPCdigitCode, this._Valid.checkUPClength])],
      size: [data ? data['size'] :null],
      color: [data ? data['color'] :null],
      uom_id: [this.defaultUOM, Validators.required],
      pack: [null, Validators.compose([Validators.required, this._Valid.validateInt, this._Valid.isZero])],
      length: [null, Validators.compose([this._Valid.isDecimalNumber])],
      width: [null, Validators.compose([this._Valid.isDecimalNumber])],
      height: [null, Validators.compose([this._Valid.isDecimalNumber])],
      weight: [null],
      cus_id: [data ? data['cus_id'] :'', Validators.required],
      description: [data ? data['description'] :'', Validators.required],
      innerItems: this.innerItemsArray,
      inner_pack: ['', Validators.compose([this._Valid.validateInt])],
      spc_hdl_code: [''],
      status: ['']
    });

    this.specHangling = {
      RAC: 1,
      SHE: 1,
      BIN: 1
    };
    
    if(data&&data['cus_id']) {
      this.onChangeCustomer(data['cus_id']);
    }

    // watch form
    this.ItemMasterForm.valueChanges.subscribe(data => {
      if (this.Action.toLowerCase() === 'new') {
        this.checkDuplicatedKey();
        this.checkUOM();
      }
    });
  }

  FormBuilderEdit() {
    this.ItemMasterForm = this.fb.group({
      item_id: [this.Detail['item_id']],
      cus_upc: [this.Detail['cus_upc'], Validators.compose([this._Valid.checkUPCdigitCode, this._Valid.checkUPClength])],
      sku: [this.Detail['sku'], Validators.compose([ Validators.required, this._Valid.validateSpace])],
      size: [this.Detail['size']],
      color: [this.Detail['color']],
      uom_id: [this.Detail['uom_id'], Validators.required],
      pack: [this.Detail['pack'], Validators.compose([Validators.required, this._Valid.validateInt, this._Valid.isZero])],
      length: [this.Detail['length'], Validators.compose([this._Valid.isDecimalNumber])],
      width: [this.Detail['width'], Validators.compose([this._Valid.isDecimalNumber])],
      height: [this.Detail['height'], Validators.compose([this._Valid.isDecimalNumber])],
      weight: [this.Detail['weight']],
      cus_id: [this.Detail['cus_id'], Validators.required],
      description: [this.Detail['description'], Validators.required],
      innerItems: this.innerItemsArray,
      inner_pack: [this.Detail['inner_pack'], Validators.compose([this._Valid.validateInt])],
      spc_hdl_code: [this.Detail['spc_hdl_code'] ? this.Detail['spc_hdl_code'] : 'RAC'],
      status: [this._IMService.getNameByCode(this.Detail['status'], this.arrStatus)]
    });
    if (this.Detail['spc_hdl_code'] && this.Detail['spc_hdl_code'] !== 'NA') {
      const spc_hdl_code = this.Detail['spc_hdl_code'];
      for (const i in spc_hdl_code) {
        this.specHangling[i] = spc_hdl_code[i];
      }
    }

    // watch form
    this.ItemMasterForm.valueChanges.subscribe(data => {
      if (this.Action.toLowerCase() === 'edit') {
        this.checkDuplicatedKey();
        this.checkUOM();
      }
    });
  }


  // get list custommer
  GetListCustomer()
  {
    // this.Customers=this._IMService.ListCustomers();
    var params ="?limit=10000";
    this._IMService.getListCustomersByUser(params)
      .subscribe(
        data => {
          this.Customers = data.data;
        },
        err => {},
        () => {}
      );

  }

  imageFileRequiredError: boolean = false;
  imageFileExtensionError: boolean = false;
  /**
   * save item master
   *
   * @param data
   */
  save(data: Object, isApprove: boolean = false): void {
    let n = this;
    this.isSubmit = true;
    this._ErrMsgControlService._genErrMsgForControl(this.ItemMasterForm);

    if (!this.isEdit) {
      this.imageFileRequiredError = this.imageFile ? false : true;
    }

    if (this.ItemMasterForm.valid && !this.imageFileRequiredError && (this.specHangling['RAC'] || this.specHangling['SHE'] || this.specHangling['BIN'])) {
      if (data['sku']) {
        data['sku'] = data['sku'].trim();
      }
      // if (!data['color']) {
      //   data['color'] = 'NA';
      // }
      // if (!data['size']) {
      //   data['size'] = 'NA';
      // }
      if (!this.isEdit) {
        data['itm_img'] = this.imageFile ? this.imageFile['name'] : '';
      }
      const spc_hdl_code = [];
      for (const i in this.specHangling) {
        if (this.specHangling[i])
          spc_hdl_code.push(i);
      }
      data['spc_hdl_code'] = spc_hdl_code.join(',');
      delete data['status'];
      let data_json = JSON.stringify(data);
      if (!this.idDetail) {
        // add new
        this.AddNew(data_json);
      }
      else {
        // edit zone type
        this.Update(this.idDetail, data_json);

        if (isApprove === true) {
          this.approveItemMaster();
        }
      }
    }
    else {
      this.showError = true;
    }
  }

  uploadImage(itemId: string) {
    const that = this;
    that.imported = true;
    that.messages = null;

    if (this.imageFile) {
      let formData: any = new FormData();
      let xhr = new XMLHttpRequest();

      formData.append('itm_img', that.imageFile, that.imageFile.name);
      formData.append('sku', that.ItemMasterForm.value['sku']);

      that.showLoadingOverlay = true;
      xhr.open("POST", `${that.api.API_URL_ROOT}/core/item-master/v1/items/${itemId}/upload-img`, true);
      xhr.setRequestHeader("Authorization", 'Bearer ' + that._Func.getToken());
      xhr.onreadystatechange = function () {
        if (xhr.readyState == 2) {
          if (xhr.status == 200) {
            xhr.responseType = "blob";
          } else {
            xhr.responseType = "text";
          }
        }

        if (xhr.readyState == 4) {
          let msg = '';

          if (xhr.response) {
            try {
              var res = JSON.parse(xhr.response);
              msg = res.errors && res.errors.message ? res.errors.message : res.data.message;
            }
            catch (err) {
              msg = xhr.statusText
            }
          }
          else {
            msg = xhr.statusText;
          }

          if (!msg) {
            msg = that._Func.msg('VR100');
          }

          if (xhr.status === 201) {
            that.messages = that._Func.Messages('success', msg);
          } else {
            that.messages = that._Func.Messages('danger', msg);
          }

          that.showLoadingOverlay = false;
        }
      };

      xhr.send(formData);
    }
  }

  /*
   *
   * */
  AddNew(data)
  {
    let that = this;
    this.showLoadingOverlay=true;
    this._IMService.Add_New_ItemMaster(data).subscribe(
      (res) => {
        const msg = this._Func.msg('VR107');

        this.Detail = res;
        this.idDetail = this.Detail['item_id'];
        if (this.idDetail) {
          this.uploadImage(this.idDetail);
        }
        
        if (!this.ItemMasterForm.value['innerItems'].length) {
          this.showLoadingOverlay = false;
          this.messages = this._Func.Messages('success', msg);
          setTimeout(function(){
            that.GoToList();
          }, 600);
        } else {
          // add successful that mean Action is Edit
          // define info to save Inner Items
          let info = this.ItemMasterForm.value;
          info['item_id'] = this.idDetail;
          this.saveInnerItems(info, msg);
        }
      },
      (err) => {
        this.showLoadingOverlay= false;
        if(err.json().errors.message)
        {
          this.messages = this._Func.Messages('danger',err.json().errors.message);
        }
      },
      () => {}
    );
  }

  
  imported: boolean = false;
  @ViewChild('imageFileView') imageFileView: any;

  Update(id,dataJson)
  {
    console.log('dataJson', dataJson)
    let that = this;
    this.showLoadingOverlay=true;
    this._IMService.Update_ItemMaster(id,dataJson).subscribe(
      (data) => {
        // prepare Info to save Inner Items
        const msg = this._Func.msg('VR109');
        let info = this.ItemMasterForm.value;
        info['item_id'] = this.idDetail;
        this.saveInnerItems(info, msg);
      },
      (err) => {
        this.showLoadingOverlay=false;
        if(err.json().errors.status_code==422)
        {
          this.messages = this._Func.Messages('danger',this._Func.msg('VR108'));
        }
        else{
          this.messages = this._Func.Messages('danger',err.json().errors.message);
        }
      },
      () => {}
    );
  }

  GoToList()
  {
    this._router.navigateByUrl('system-setup/item-master');
  }

  cancel() {
    let n = this;
    this._boxPopupService.showWarningPopup()
      .then(function (dm) {
        if (dm)
          n.GoToList();
      })
  }

  /**
   * Disable input text
   */
  checkInputNumber(evt, int = false) {
    this._Valid.isNumber(evt, int);
  }

  private getUOMList(page = null) {
    this._IMService.getUOM()
      .subscribe(
          data => {
              this.uomList = data.data;
          },
          err => {},
          () => {}
      );
  }

  private defaultUOM = '';
  private getDefaultUOM() {
    var search_uom_name = 'PIECE';
    this._IMService.searchUOM_ID(search_uom_name)
      .subscribe(
          data => {
              for(let i = 0; i < data.data.length; i++) {
                let uom = data.data[i];
                if(uom.sys_uom_name == search_uom_name) {
                  this.defaultUOM = data.data[0].sys_uom_id;
                  this.ItemMasterForm && (<Control>this.ItemMasterForm.controls['uom_id']).updateValue(this.defaultUOM);
                }
              }
          },
          err => {}
      );
  }

  detectControlErr(control) {
    this._ErrMsgControlService._genErrMsgForControl(control);
  }

  private newItem(controlArray: ControlArray, item = {}) {
    var tmp = {
        'item_id': new Control(item['item_id'] ? item['item_id'] : '', Validators.required),
        'sku': new Control(item['sku']),
        'size': new Control(item['size']),
        'color': new Control(item['color']),
        'uom_id': new Control(item['uom_id']),
        'inner_pack': new Control(1),
        'pack': new Control(item['pack'] ? item['pack'] : '', Validators.required),
        'cus_id': new Control(item['cus_id'] ? item['cus_id'] : '')
    };

    controlArray.push(
        new ControlGroup(tmp)
    );
  }

  private resetInnerItemRow(controlArray: ControlArray, row) {
      controlArray.controls[row]['disabled'] = false;
      let fieldReset = ['item_id', 'uom_id', 'size', 'color', 'inner_pack', 'pack'];
      this.resetItemRow(controlArray, row, fieldReset);
  }

  private resetItemRow(controlArray: ControlArray, row: number, fields: string[]) {
    for (let key in fields) {
      this.fbFunc.setValueControlItem(controlArray, row, fields[key], '');
    }
  }

  private setItemRow(controlArray: ControlArray, row: number, fields: Object, values: any) {
    for (let key in fields) {
      this.fbFunc.setValueControlItem(controlArray, row, key, values[fields[key]]);
    }
  }

  private checkDuplicatedKey() {
    this.fbFunc.checkDuplicatedKey(this.innerItemsArray, ['sku','size','color']);
  }

  private _getLevelByUOMId(uom_id: any): number {
    let level = 0;
    const n = this.uomList.length;
    for (let i = 0; i < n; i++) {
      if (this.uomList[i].sys_uom_id === parseInt(uom_id, 10)) {
        level = this.uomList[i].level_id;
        break;
      }
    }
    return level;
  }

  private checkUOM() {
    const UOMId = this.ItemMasterForm.value['uom_id'];
    const UOMlevelId = this._getLevelByUOMId(UOMId);
    let maxUOM = 0;

    this.innerItemsArray['controls'].forEach((ctrl) => {
      const level_id = this._getLevelByUOMId(ctrl['controls']['uom_id'].value);
      if (level_id && level_id > maxUOM) {
        maxUOM = level_id;
      }
    });
    
    if (UOMlevelId <= maxUOM) {
      this.fbFunc.setErrorControl(this.ItemMasterForm, 'uom_id', {'invalidUOM': true});
      this.showError = true;
    } else {
      this.fbFunc.setErrorControl(this.ItemMasterForm, 'uom_id', null);
    }
  }

  private selectedItem(controlArray: ControlArray, row, data, click = false) {
      // check if item've been selected yet
      let existed = false;
      const n = controlArray['controls'].length;
      for (let i = 0; i < n; i++) {
        if (controlArray['controls'][i]['controls']['item_id'].value === data.item_id) {
          existed = true;
          break;
        }
      }
      
      // if item's not selected then added it to array inner items
      controlArray['controls'][row]['disabled'] = true;
      let fieldReset = {// control_id : data.value
        'item_id': 'item_id', 
        'sku': 'sku', 
        'uom_id': 'uom_id',
        'cus_id': 'cus_id',
        'size': 'size',
        'color': 'color',
        'inner_pack': 'pack'
      };
      this.setItemRow(controlArray, row, fieldReset, data);
  }
  
  private checkInnerItemExist(controlArray:ControlArray, row, str:string, arr:Array<any>) {

      let string=str;
      for(let i in arr) {
          if(string==this._Func.trim(arr[i]['sku'])) {
              this.selectedItem(controlArray,row,arr[i],false);
              return;
          }
      }
      // set error if not found
      if(str) {
          this.fbFunc.setErrorsControlItem(controlArray, row, 'sku', {'invaildValue':true})
      }

  }

  private deleteItem(controlArray:ControlArray) {

    let n = this;
    if (this.fbFunc.getItemchecked(controlArray) > 0) {

        /*case 1: modify text*/
        let warningPopup = new WarningBoxPopup();
        warningPopup.text = this._Func.msg('PBAP002');
        this._boxPopupService.showWarningPopup(warningPopup)
            .then(function (dm) {
                if (!dm) {
                    return;
                } else {
                  n.fbFunc.deleteItem(controlArray).subscribe(deletedAll => {
                      n.updateTotalPackSize();
                      if(deletedAll) {
                        // n.newItem(controlArray);
                      }
                  });
                }
            });
    }
    else {

        this.messages = this._Func.Messages('danger', this._Func.msg('VR113'));

    }
  }
  
  private searchSku = [];
  private autoSearchData = [];
  private autocompleteSKU(row, value, fieldname, page=1, reset=true) {
    this.innerItemsInfo[row]['showLoading'] = true;
    this.innerItemsInfo[row]['current_page'] = page;

    // reset all column in row
    this.resetInnerItemRow(this.innerItemsArray, row);

    // assign to SearchQuery
    let key = this._Func.trim(value);
    const cusId = this.ItemMasterForm.controls['cus_id'].value;
    let param = '?' + fieldname + '=' + key + '&cus_id=' + cusId + '&limit=20&page=' + page;
    let enCodeSearchQuery = this._Func.FixEncodeURI(param);

    if(this.innerItemsInfo[row]['subscribe']) {
      this.innerItemsInfo[row]['subscribe'].unsubscribe();
    }

    // do Search
    this.searchSku[row] = true;
    this.innerItemsInfo[row]['subscribe'] = this._IMService.searchItems(enCodeSearchQuery).debounceTime(400).distinctUntilChanged().subscribe(
      (data) => {
        // disable loading icon
        this.innerItemsInfo[row]['showLoading'] = false;
        this.innerItemsInfo[row]['paginData'] = data.meta;
        if(!this.innerItemsInfo[row]['autoSearchData'] || reset){
            this.innerItemsInfo[row]['autoSearchData'] = [];
        }
        const parentSKU = this.ItemMasterForm.controls['sku'].value;
        let items = data.data.filter(item => item.sku !== parentSKU);
        this.innerItemsInfo[row]['autoSearchData'].push(...items);
        this.checkInnerItemExist(this.innerItemsArray, row, key, items);
      },
      (err) => {
        this.searchSku[row] = false;
        this.messages = this._Func.Messages('danger','Not found');
      },
      () => {}
    );
  }

  private checkPackSize(val) {
    if (this.Action.toLowerCase() === 'new') {
      return;
    }
    
    let packSizeCtrl = this.ItemMasterForm.controls['pack'];
    let totalPack = 0;
    // reset error
    this.fbFunc.setErrorControl(this.ItemMasterForm, 'pack', null);
    if (this.innerItemsArray['controls'].length) {

      this.innerItemsArray['controls'].forEach(ctrl => {
        if (ctrl['controls']['pack'].value) {
          totalPack += parseInt(ctrl['controls']['pack'].value, 10);
        }
      });
      
      if (totalPack.toString() !== packSizeCtrl.value) {
        this.fbFunc.setErrorControl(this.ItemMasterForm, 'pack', {'invalidPacksize': true});
      }
    }
  }
  
  private updateTotalPackSize() {
    let packSizeCtrl = this.ItemMasterForm.controls['pack'];
    let totalPack = 0;


    if (this.innerItemsArray['controls'].length < 1) {
      this.disableTotalPackSize = false;
    }

    this.innerItemsArray['controls'].forEach(ctrl => {
      if (ctrl['controls']['pack'].value) {
        totalPack += parseInt(ctrl['controls']['pack'].value, 10);
      }
    });

    if (totalPack && totalPack !== packSizeCtrl.value) {
      this.disableTotalPackSize = true;
      (<Control>packSizeCtrl).updateValue(totalPack);
    }
  }

  private onChangeUOM(val) {
    // reset all UOMs of items
    this.innerItemsArray['controls'].forEach(ctrl => {
      if (ctrl['controls']['uom_id'].value && ctrl['controls']['uom_id'].value.length) {
        (<Control>ctrl['controls']['uom_id']).updateValue('');
      }
    });
    // prefill packing level tab
    this.prefillPackingLevelTab();
  }

  private onChangeCustomer(val) {
    this.innerItemsArray['controls'].forEach(ctrl => {
      (<Control>ctrl['controls']['cus_id']).updateValue(val);
    });
    if (this.isAddFromPopup) {
      this.skuGenerate(val);
    }
    if(this.ItemMasterForm.controls['sku'].value) {
      this.checkExisted(this.ItemMasterForm.controls['sku'], val);
    }
  }

  private removeErrorRequired (field, err = 'required') {

      delete this.ItemMasterForm.controls[field].errors[err];
      if(!Object.keys(this.ItemMasterForm.controls[field].errors).length) {

          this.ItemMasterForm.controls[field].setErrors(null);
      }
  }

  private getItemPacking(itemId) {
    this._IMService.getItemPacking(itemId).subscribe(
      (data) => {
        this.Detail['child_items'] = data.data;
        if (this.Detail['child_items'] && this.Detail['child_items'].length) {
          this.Detail['child_items'].forEach(item => {
            this.newItem(this.innerItemsArray, item);
          });
        }
      },
      (err) => {

      }
    )
  }

  private saveInnerItems(info: any, msg: string) {
    let that = this;
    let level = this._getLevelByUOMId(info.uom_id);
    let params = {
      'parent_item': {
        'item_id': info.item_id,
        'sku': info.sku,
        'size': info.size,
        'color': info.color,
        'uom_id': info.uom_id,
        'level_id': level,
        'pack': info.pack,
        'cus_id': info.cus_id,
        'description': info.description
      },
      'child_items': info.innerItems
    }

    this._IMService.saveItemPacking(JSON.stringify(params)).subscribe(
      (data) => {
        this.showLoadingOverlay=false;
        this.messages = this._Func.Messages('success', msg);
        setTimeout(function(){
          that.GoToList();
        }, 600);
      },
      (err) => {
        this.showLoadingOverlay=false;
        const errMsg = this._Func.parseErrorMessageFromServer(err);
        this.messages = this._Func.Messages('danger', errMsg);
      }
    )
  }

  // for Packing Level
  item_hier:ControlGroup[] = [];
  packingLevelsArray:ControlArray = new ControlArray(this.item_hier);
  private packingLevelDropdown = [];
  private showPackingLevelTab = false;
  private alwayRequiredLevels = [1];
  
  getPackingLevelDropdown() {
    this._IMService.getPackingLevelDropdown()
      .subscribe(
        data => {
          this.packingLevelDropdown = data;
          this.prefillPackingLevelTab();
        }
      );
  }
  
  itemDetail(index, item) {
    if(item) {
      this.packingLevelsArray.push(
          new ControlGroup({
              'level_id': new Control(item['level_id']),
              'uom_code': new Control(item['uom_code']),
              'pack': new Control(item['pack'], Validators.compose([this._Valid.validateInt,this._Valid.isZero]))
          })
      );
    }
    else {
      this.newLevelsItem(index);
    }
  }
  
  newLevelsItem(i) {
    if(-1 !== this.alwayRequiredLevels.indexOf(i)) {
      this.packingLevelsArray.push(
          new ControlGroup({
              'level_id': new Control(i),
              'uom_code': new Control('', Validators.required),
              'pack': new Control(i == 1 ? 1 : '', Validators.compose([Validators.required, this._Valid.validateInt,this._Valid.isZero]))
          })
      );
    }
    else {
      this.packingLevelsArray.push(
          new ControlGroup({
              'level_id': new Control(i),
              'uom_code': new Control(''),
              'pack': new Control(i == 1 ? 1 : '', Validators.compose([this._Valid.validateInt,this._Valid.isZero]))
          })
      );
    }
  }

  prefillPackingLevelTab() {
    if(!this.showPackingLevelTab) {
      this.showPackingLevelTab = true;
      // auto selection for level 1
      let arrControl = this.packingLevelsArray.controls[0]['controls'];
      let dropdown = this.packingLevelDropdown[1]['details'];
      (<Control>arrControl['uom_code']).updateValue(dropdown ? dropdown[0].pkg_code : '');
    }
  }
  
  validateGroup(index) {
    if(-1 == this.alwayRequiredLevels.indexOf(index+1)) {
      setTimeout(() => {
        let arrControl = this.packingLevelsArray.controls[index]['controls'];
        let hasValue = false;
        for (let key in arrControl) {
          if(key != 'level_id') {
            let control = arrControl[key],
                ctrlValue = jQuery.trim(control.value);
            if(ctrlValue) {
              hasValue = true;
            }
          }
        }
          
        for( let key in arrControl) {
          if(key != 'upc') {          
              let control = arrControl[key],
                  ctrlValue = jQuery.trim(control.value);
              if(hasValue) {
                if (!ctrlValue) (<Control>control).setErrors({'required': true});
              }
              else {
                (<Control>control).setErrors(null);
              }
          }
        }
      })
    }
  }
  
  buildItemLevel() {
    let itemLevels = this.Detail.item_hier;
    if (!itemLevels.length) {
      return;
    }
    for(let i = 1; i <= this.packingLevel; i++) {
      this.itemDetail(i, itemLevels[i].details);
    }
  }

  getPackageItem(itemId) {
    this._IMService.getPackageItem(itemId)
      .subscribe(
        data => {
          this.packItems = data.data;
        },
        err => {},
        () => {}
      );

  }

  public checkExisted(target, cus_id) {
    if (target.value) {
      this.ItemMasterForm.controls['sku'].setErrors(null)
      this._IMService.checkExistSKU(cus_id, target.value).subscribe(
        data => {
          if(data['data'] == 'true') {
            this.ItemMasterForm.controls['sku'].setErrors({'existed' : true});
          }
        }
      );
    }
  }

  imageFile: any;
  changeImageFile(event) {
    this.imageFile = <Array<File>>event.target.files[0];
    this.imageFileRequiredError = this.imageFile ? false : true;

    const fileExtension = this.imageFile.name.substr(this.imageFile.name.length - 4).toLowerCase();
    if (fileExtension !== "jpeg" && fileExtension !== ".jpg" && fileExtension !== ".png" && fileExtension !== ".gif" && fileExtension !== "tiff") {
      this.imageFileExtensionError = true;
    } else {
      this.imageFileExtensionError = false;
    }
  }

  selectedPictureUrl = '';
  selectedPictureName = 'http://sohanews.sohacdn.com/thumb_w/660/2018/11/21/ava-ngang-15427010531241796308130-crop-1542701055764399234855-15428095940722145590867-crop-1542809598418847799055.png';
  /**
   * Show picture modal
   *
   * @param pictureName Picture name
   * @param pictureUrl Picture Url
   */
  showPicture(pictureName: string = '', pictureUrl: string = '') {
    // this.selectedPictureUrl = pictureUrl;
    // this.selectedPictureName = pictureName;

    jQuery('#pictureModal').modal('show');
  }

  /**
   * Approve item master
   */
  approveItemMaster() {
    let that = this;
    this._IMService.approveItemMaster(this.idDetail).subscribe(res => {
      this.messages = this._Func.Messages('success', res.data.message);
      setTimeout(function () {
        that.GoToList();
      }, 600);
    });
  }

  skuGenerate(cus_id) {
    this._IMService.skuGenerate(cus_id).subscribe(data => {      
      (<Control>this.ItemMasterForm.controls['sku']).updateValue(data['data']['sku'] ? data['data']['sku'] : '');
    })
  }

  
  public selectDesc(desc) {
    (<Control>this.ItemMasterForm.controls['description']).updateValue(desc);
  }

  public autoSearchDesc = [];
  public autoCompleteDesc(keyword) {
    this.autoSearchDesc = [];
    if (keyword) {
      this.showAutocompleteLoading = true;
      this._IMService.GetItemMasterList('?limit=20&description=' + keyword).debounceTime(400).distinctUntilChanged().subscribe(
        data => {
          this.autoSearchDesc = data.data;
        },
        err => {
          this.messages = {'status': 'danger', 'txt': this._Func.parseErrorMessageFromServer(err)};
        },
        () => {
        this.showAutocompleteLoading = false;
        }
      );
    }
  }

}
