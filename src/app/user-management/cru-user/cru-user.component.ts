import {Component, OnInit} from '@angular/core';
import {FORM_DIRECTIVES, ControlGroup, FormBuilder, Control, Validators} from '@angular/common';
import {RouteParams, Router} from '@angular/router-deprecated';
import {UserService} from '../../common/core/load';
import {CRUUserService} from '../cru-user/cru-user-service';
import {CRUUserMessages} from '../cru-user/cru-user-messages';
import {Http} from '@angular/http';
import {Functions} from "../../common/core/functions";
import {JwtHelper} from 'angular2-jwt/angular2-jwt';
import {GoBackComponent} from "../../common/component/goBack.component";
import {ValidationService} from '../../common/core/validator';
import {BoxPopupService} from "../../common/popup/box-popup.service";
import {OrderBy} from "../../common/pipes/order-by.pipe";
import {PaginationControlsCmp, PaginationService, PaginatePipe} from '../../common/directives/directives';
import {WarningBoxPopup} from "../../common/popup/warning-box-popup";
import {WMSMessages} from "../../common/directives/messages/messages";
declare var jQuery:any;
declare var swal:any;
@Component({
  selector: 'cru-user',
  providers: [CRUUserService, CRUUserMessages, FormBuilder, ValidationService, BoxPopupService, PaginationService],
  directives: [FORM_DIRECTIVES, GoBackComponent, PaginationControlsCmp, WMSMessages],
  pipes: [OrderBy, PaginatePipe],
  templateUrl: 'cru-user.component.html',
})

export class CRUUserComponent implements OnInit {

  public loading = [];
  private messages;
  private timeoutHideMessage;
  private listMessage = {};
  private action;
  private actionText;
  private TitlePage = '';
  private cruUserID;
  private isView:boolean = false;
  private showError = false;
  private allRoles = [];
  private addedRoles = [];
  private allWarehouses = [];
  private addedWarehouses = [];
  private mapCustomerWarehouse = [];
  private allCustomerByWarehouse = [];
  private addedCustomers = [];
  private loadCustomerByWarehouse = false;
  private showLoadingOverlay = false;
  private logedUser;
  private logedUserRoles = [];
  private logedUserWarehouses = [];
  userForm:ControlGroup;

  private roleCodeFilter:string = '';
  private warehouseCodeFilter:string = '';
  private customerCodeFilter:string = '';
  private tempAllRoles = [];
  private tempAllWarehouses = [];
  private tempAllCustomers = [];
  private selectedAll = {};
  private rolePerPage = 20;
  private warehousePerPage = 20;
  private customerPerPage = 20;
  private departments = [];

  constructor(private _http:Http,
              private _user:UserService,
              private _func:Functions,
              private params:RouteParams,
              private _cruUserService:CRUUserService,
              private _cruUserMessages:CRUUserMessages,
              private _boxPopupService:BoxPopupService,
              public jwtHelper:JwtHelper,
              private _Valid:ValidationService,
              private fb:FormBuilder,
              private _router:Router) {
  }

  ngOnInit() {
    this.listMessage = this._cruUserMessages.messages;
    this.buildForm();

    this.cruUserID = this.params.get('id');
    if (this.cruUserID) {
      this.showLoadingOverlay = true;
      this.action = this.params.get('edit') == 'edit' ? 'edit' : 'view';
      if (this.action == 'view') {
        this.isView = true;
        this.TitlePage = 'View User';
      }
      else {
        this.actionText = "Update";
        this.TitlePage = 'Edit User';
      }
    }
    else {
      this.action = 'new';
      this.actionText = "Save";
      this.TitlePage = 'Add User';
    }
    this.getDepartments();
    this.checkPermission();
  }

  ngAfterViewInit() {
    this.initResetModal();
  }

  private checkPermission() {
    this.showLoadingOverlay = true;
    this._user.GetPermissionUser().subscribe(
      data => {
        if ((this.action == 'view' && !this._user.RequestPermission(data, 'viewUser')) || (this.action == 'edit' && !this._user.RequestPermission(data, 'editUser')) || (this.action == 'new' && !this._user.RequestPermission(data, 'createUser'))) {
          this._router.parent.navigateByUrl('/deny');
          this.showLoadingOverlay = false;
        }
        else {
          this.getAllWarehouse_customer_role();
        }
      },
      err => {
        this.showMessage('danger', this._func.parseErrorMessageFromServer(err));
        this.showLoadingOverlay = false;
      }
    );
  }

  private getDepartments() {
    this._cruUserService.getDepartmentInfo2().subscribe(
      data => {
        this.departments = data;
      },
      err => {

      }
    )
  }

  private getAllWarehouse_customer_role() {
    var jwt = localStorage.getItem('jwt'),
      logedUserId = this.jwtHelper.decodeToken(jwt)['jti'];
    this._cruUserService.getAllWarehouse_customer_role().subscribe(
      data => {
        this.logedUser = data;
        this.logedUser['user_id'] = logedUserId;
        this.initData();
      },
      err => {
        this.showMessage('danger', this._func.parseErrorMessageFromServer(err));
        this.showLoadingOverlay = false;
      }
    )
  }

  private initData() {
    // console.log('loged user info', this.logedUser);
    this.tempAllRoles = this.logedUser.user_roles.slice();
    this.allRoles = this.logedUser.user_roles.slice();

    this.tempAllWarehouses = this.logedUser.user_warehouses.slice()
    this.allWarehouses = this.logedUser.user_warehouses.slice();

    this.mapCustomerWarehouse = this.logedUser.user_customers;
    this.showLoadingOverlay = false;
    if (this.cruUserID) {
      /*if (this.action == 'edit' && this.cruUserID == this.logedUser.user_id) {
        this.action = 'view';
        this.isView = true;
        this.TitlePage = 'View User';
      }*/
      this.getCRUUserDetail();
    }
  }

  private getCRUUserDetail() {
    this.showLoadingOverlay = true;
    this._cruUserService.getUserDetail(this.cruUserID).subscribe(
      data => {
        // console.log('cru user detail', data);
        (<Control>this.userForm.controls['user_id']).updateValue(data['user_id']);
        (<Control>this.userForm.controls['emp_code']).updateValue(data['emp_code']);
        (<Control>this.userForm.controls['email']).updateValue(data['email']);
        (<Control>this.userForm.controls['username']).updateValue(data['username']);
        (<Control>this.userForm.controls['first_name']).updateValue(data['first_name']);
        (<Control>this.userForm.controls['last_name']).updateValue(data['last_name']);
        (<Control>this.userForm.controls['off_loc']).updateValue(data['off_loc']);
        (<Control>this.userForm.controls['usr_dpm_id']).updateValue(data['usr_dpm_id']);
        (<Control>this.userForm.controls['phone']).updateValue(data['phone']);
        (<Control>this.userForm.controls['phone_extend']).updateValue(data['phone_extend']);
        (<Control>this.userForm.controls['status']).updateValue(data['status']);
        (<Control>this.userForm.controls['mobile']).updateValue(data['mobile']);
        this.addedRoles = data['user_roles'];
        this.addedWarehouses = data['user_warehouses'];
        this.addedCustomers = data['user_customers'];

        this.checkRemoveAddedRoles();
        this.checkRemoveAddedWarehouse();
        this.showLoadingOverlay = false;
      },
      err => {
        // console.log('err', err.json());
        this.showLoadingOverlay = false;
        this.showMessage('danger', this._func.parseErrorMessageFromServer(err));
      }
    );
  }

  private checkRemoveAddedRoles() {
    let resData = this.allRoles;
    if (this.addedRoles.length) {
      let listItemRemove = [];
      for (let i = 0; i < resData.length; i++) {
        let itemCheck = resData[i];
        for (let j = 0; j < this.addedRoles.length; j++) {
          let itemAdded = this.addedRoles[j];
          if (itemCheck['name'] == itemAdded['name']) {
            listItemRemove.push(i);
          }
        }
      }
      if (listItemRemove.length) {
        for (let i = listItemRemove.length - 1; i >= 0; i--) {
          resData.splice(listItemRemove[i], 1);
        }
      }
    }
    this.allRoles = resData;
  }

  private checkRemoveAddedWarehouse() {
    let resData = this.allWarehouses;
    if (this.addedWarehouses.length) {
      let listItemRemove = [];
      for (let i = 0; i < resData.length; i++) {
        let itemCheck = resData[i];
        for (let j = 0; j < this.addedWarehouses.length; j++) {
          let itemAdded = this.addedWarehouses[j];
          if (itemCheck['whs_id'] == itemAdded['whs_id']) {
            listItemRemove.push(i);
          }
        }
      }
      if (listItemRemove.length) {
        for (let i = listItemRemove.length - 1; i >= 0; i--) {
          resData.splice(listItemRemove[i], 1);
        }
      }
    }
    this.allWarehouses = resData;
  }

  private checkIndex(arr, attr, value) {
    for (let i = 0; i < arr.length; i++) {
      if (arr[i][attr] == value) {
        return i;
      }
    }
    return -1;
  }

  private initResetModal() {
    let that = this;
    jQuery('.modal').on('hidden.bs.modal', function () {
      let modal = jQuery(this);
      if (modal.attr('id') == 'modal-login') {
        return;
      }
      modal.find(':checkbox').prop('checked', false);
      // modal.find('.input-filter').val('').trigger('input.search');
      modal.find('.input-filter').val('');
      that.allRoles = [];
      that.allWarehouses = [];
      that.allCustomerByWarehouse = [];

      if (modal.attr('id') == 'addCustomer') {
        modal.find('select').val('');
      }
    });
  }

  private initFilter(inputId, listFilterQR, listDataName) {
    var that = this,
      inputFilter = jQuery(inputId),
      listFilter = jQuery(listFilterQR);
    inputFilter.off('input.search').on('input.search', function () {
      let filterValue = jQuery.trim(inputFilter.val().toLowerCase());
      if (listFilter.length !== that[listDataName].length) {
        listFilter = jQuery(listFilterQR);
      }
      listFilter.each(function () {
        var elm = jQuery(this);
        if (elm.text().toLowerCase().indexOf(filterValue) != -1) {
          elm.closest('tr').show();
        }
        else {
          elm.closest('tr').hide();
        }
      })
    }).trigger('input.search');
  }

  private showPopupConfirm(message) {
    return swal({
      title: message,
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: this.listMessage['agree'],
      cancelButtonText: this.listMessage['cancel'],
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: false
    })
  }

  private getCustomerByWarehouse($event) {
    var that = this,
      warehouseID = jQuery($event.target).val();
    jQuery('#customer-code-filter').val('');
    if (warehouseID) {
      this.loadCustomerByWarehouse = true;
      var resData = [];
      for (let i = 0, l = this.mapCustomerWarehouse.length; i < l; i++) {
        let item = this.mapCustomerWarehouse[i];
        item['selected'] = false;
        if (item.whs_id == warehouseID) {
          resData.push(item);
        }
      }
      // check remove added customers by this warehouse
      if (this.addedCustomers.length) {
        let listItemRemove = [];
        for (let i = 0; i < resData.length; i++) {
          let itemCheck = resData[i];
          for (let j = 0; j < this.addedCustomers.length; j++) {
            let itemAdded = this.addedCustomers[j];
            if (itemCheck['whs_id'] == itemAdded['whs_id'] && itemCheck['cus_id'] == itemAdded['cus_id']) {
              listItemRemove.push(i);
            }
          }
        }
        if (listItemRemove.length) {
          for (let i = listItemRemove.length - 1; i >= 0; i--) {
            resData.splice(listItemRemove[i], 1);
          }
        }
      }
      // end check
      that.allCustomerByWarehouse = resData;
      that.tempAllCustomers = resData.slice();
      that.loadCustomerByWarehouse = false;
    }
    else {
      that.allCustomerByWarehouse = [];
      that.tempAllCustomers = [];
    }
  }

  private deleteCustomerInWarehouses(listWarehouseCodeRemove) {
    let that = this;
    for (let i = this.addedCustomers.length - 1; i >= 0; i--) {
      if (listWarehouseCodeRemove.indexOf(this.addedCustomers[i].whs_code) != -1) {
        this.addedCustomers.splice(i, 1);
      }
    }
  }

  showMessage(msgStatus, msg) {
    this.messages = {
      'status': msgStatus,
      'txt': msg
    }
    jQuery(window).scrollTop(0);
  }

  private logisoftEmailValidator(control:Control):{[key:string]:any} {
    // RFC 2822 compliant regex
    var val = jQuery.trim(control.value);
    if (val && val.match(/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)) {
      return null;
    } else {
      if (val != '') {
        return {'invalidEmailAddress': true};
      }
    }
  }

  buildForm() {
    this.userForm = this.fb.group({
      user_id: [null],
      username: [null],
      emp_code: [null, this._Valid.customRequired],
      email: [null, Validators.compose([this._Valid.customRequired, this.logisoftEmailValidator])],
      first_name: [null, this._Valid.customRequired],
      last_name: [null, this._Valid.customRequired],
      off_loc: [null, this._Valid.customRequired],
      usr_dpm_id: ['', Validators.required],
      phone: [null, this._Valid.customRequired],
      phone_extend: [null],
      status: ['', Validators.required],
      mobile: [''],
    });
  }

  submitForm(data:Object):void {
    this.showError = true;
    if (this.userForm.valid) {
      // trim data before submit
      for (let key in data) {
        let val = data[key];
        if (val && typeof val == 'string') {
          data[key] = jQuery.trim(val);
        }
      }
      let submitRole = [];
      for (let i = 0; i < this.addedRoles.length; i++) {
        submitRole.push(this.addedRoles[i].name);
      }
      data['user_roles'] = submitRole.join(',');

      let submitWarehouse = [];
      for (let i = 0; i < this.addedWarehouses.length; i++) {
        submitWarehouse.push({'whs_id': this.addedWarehouses[i]['whs_id']});
      }
      data['user_warehouses'] = submitWarehouse;

      let submitCustomer = [];
      for (let i = 0; i < this.addedCustomers.length; i++) {
        submitCustomer.push({
          'cus_id': this.addedCustomers[i]['cus_id'],
          'whs_id': this.addedCustomers[i]['whs_id']
        });
      }
      data['user_customers'] = submitCustomer;
      if (this.action == 'new') {
        data['setup_password_url'] = this._cruUserService.API_URL_SETUP_PASSWORD;
      }
      if (this.addedRoles.length && this.addedWarehouses.length) {
        let stringData = JSON.stringify(data);
        this.showLoadingOverlay = true;
        if (this.action == 'edit') {
          this.update(this.cruUserID, stringData);
        }
        else {
          this.addNew(stringData);
        }
      }
    }
  }

  update(userID, stringData) {
    let that = this;
    this._cruUserService.updateUser(this.cruUserID, stringData).subscribe(
      data => {
        this.showLoadingOverlay = false;
        this.showMessage('success', this.listMessage['update_success']);
        setTimeout(function () {
          that._router.parent.navigateByUrl('/users');
        }, 600);
      },
      err => {
        this.showLoadingOverlay = false;
        this.showMessage('danger', this._func.parseErrorMessageFromServer(err));
      }
    );
  }

  addNew(stringData) {
    let that = this;
    this._cruUserService.addUser(stringData).subscribe(
      data => {
        this.showLoadingOverlay = false;
        this.showMessage('success', this.listMessage['add_new_success']);
        setTimeout(function () {
          that._router.parent.navigateByUrl('/users');
        }, 600);
      },
      err => {
        this.showLoadingOverlay = false;
        this.showMessage('danger', this._func.parseErrorMessageFromServer(err));
      },
      () => {}
    );
  }

  cancel() {
    var that = this;
    this._boxPopupService.showWarningPopup()
      .then(function (ok) {
        if (ok)
          that._router.navigateByUrl('/users');

      });
  }

  private Selected($event, item, ListName:string, state, paginId) {

    let Array = eval(`this.` + ListName);
    this.selectedAll[ListName] = false;

    if ($event.target.checked) {
      item['selected'] = true;
      this.checkCheckAll(ListName, state, paginId);
    }
    else {
      item['selected'] = false;
    }
  }

  checkCheckAll(ListName:string, StatePagin, objName) {
    let Array = eval(`this.` + ListName);
    // console.log('StatePagin > ', StatePagin);
    let el = StatePagin[objName];
    let end = el['end'];
    let chk = 0;
    // console.log('check check all');

    if (el['size']) {

      if (el['size'] < el['end']) {
        end = el['size'];
      }

      for (let i = el['start']; i < end; i++) {
        // console.log('i ' , i , Array[i]['selected']);
        if (Array[i]['selected']) {
          chk++;
        }
      }
      if (chk == el['slice'].length) {
        this.selectedAll[ListName] = true;
      }
      else {
        this.selectedAll[ListName] = false;
      }

      // console.log('check all of list ' , ListName ,  this.selectedAll[ListName] );
    } // end if size
  }

  private CheckAll(event, ListName:string, StatePagin, objName) {

    let Array = eval(`this.` + ListName);

    if (!Array.length) {
      this.selectedAll[ListName] = false;
      return;
    }
    if (event) {
      // console.log('')
      this.selectedAll[ListName] = true;
    }
    else {
      this.selectedAll[ListName] = false;
    }

    //console.log('List checkall from ',StatePagin ,  StatePagin[objName]['start'] , ' to ' , StatePagin[objName]['end']);
    let end = StatePagin[objName]['end'];
    if (StatePagin[objName]['size'] < StatePagin[objName]['end']) {
      end = StatePagin[objName]['size'];
    }
    //console.log('end -> ' , end);
    for (let i = StatePagin[objName]['start']; i < end; i++) {
      //console.log(' i ' , i ,  Array[i]);
      Array[i]['selected'] = this.selectedAll[ListName];
    }
  }

  searchRoleCode(key) {
    this.allRoles = this._func.filterbyfieldName(this.tempAllRoles, 'code', key);
  }

  searchWarehouseCode(key) {
    this.allWarehouses = this._func.filterbyfieldName(this.tempAllWarehouses, 'whs_code', key);
  }

  searchCustomerCode(key) {
    this.allCustomerByWarehouse = this._func.filterbyfieldName(this.tempAllCustomers, 'cus_code', key);
  }

  private addRolesToUser() {
    let listItemRemove = [];

    this.tempAllRoles.forEach((item, idx)=> {
      if (item['selected']) {
        item['selected'] = false;
        this.addedRoles.push(item);
        listItemRemove.push(idx);
      }
    });
    if (listItemRemove.length) {
      for (let i = listItemRemove.length - 1; i >= 0; i--) {
        this.tempAllRoles.splice(listItemRemove[i], 1);
      }
    }
  }

  private addWarehousesToUser() {
    let listItemRemove = [];

    this.tempAllWarehouses.forEach((item, idx)=> {
      if (item['selected']) {
        item['selected'] = false;
        this.addedWarehouses.push(item);
        listItemRemove.push(idx);
      }
    });
    if (listItemRemove.length) {
      for (let i = listItemRemove.length - 1; i >= 0; i--) {
        this.tempAllWarehouses.splice(listItemRemove[i], 1);
      }
    }
  }

  private addCustomersToUser() {
    let listItemRemove = [];
    this.tempAllCustomers.forEach((item, idx)=> {
      if (item['selected']) {
        item['selected'] = false;
        this.addedCustomers.push(item);
        listItemRemove.push(idx);
      }
    });
    if (listItemRemove.length) {
      for (let i = listItemRemove.length - 1; i >= 0; i--) {
        this.tempAllCustomers.splice(listItemRemove[i], 1);
      }
    }
  }

  // reget list roles popup
  private getPopupRoles() {
    this.tempAllRoles = this.filterShowDataListPopup(this.logedUser.user_roles.slice(), this.addedRoles, 'code');
    for (var i = 0, l = this.tempAllRoles.length; i < l; i++) {
      this.tempAllRoles[i]['selected'] = false;
    }
    this.allRoles = this.tempAllRoles.slice();
  }

  private getPopupWarehouses() {
    this.tempAllWarehouses = this.filterShowDataListPopup(this.logedUser.user_warehouses.slice(), this.addedWarehouses, 'whs_code');
    for (var i = 0, l = this.tempAllWarehouses.length; i < l; i++) {
      this.tempAllWarehouses[i]['selected'] = false;
    }
    this.allWarehouses = this.tempAllWarehouses.slice();
  }

  private getPopupCustomers() {
    this.tempAllCustomers = [];
    this.allCustomerByWarehouse = [];
  }

  Delete(ListName:string, PaginId, IsControlGroupForm = false) {
    // destroy messages
    this.messages = false;
    // create array temp
    let Arr = eval(`this.` + ListName);
    let ArrTemp = Arr.slice();
    /*
     * Check any item has value check
     * */
    let f = 0;
    if (PaginId == 'warehouse') {
      var listWarehouseCodeRemove = [];
    }
    for (let i = 0; i < Arr.length; i++) {
      if (Arr[i]['selected'] == true) {
        f++;
        break;
      }
    }
    if (f == 0) {
      this.showMessage('danger', this.listMessage['remove_none']);
    }
    else {
      let n = this;
      let warningPopup = new WarningBoxPopup();
      warningPopup.text = this.listMessage['remove_' + PaginId + '_confirm'];
      this._boxPopupService.showWarningPopup(warningPopup)
        .then(function (yes) {
          if (!yes) {
            return;
          } else {
            n.selectedAll = {};
            let i = 0;
            ArrTemp.forEach((item)=> {
              // find item selected and remove it
              if (item['selected'] == true) {
                let el = Arr.indexOf(item);
                PaginId == 'warehouse' && listWarehouseCodeRemove.push(item['whs_code']);
                Arr.splice(el, 1);
              }
              i++;
            });
            PaginId == 'warehouse' && n.deleteCustomerInWarehouses(listWarehouseCodeRemove);
          }
        });
    }
  }

  filterShowDataListPopup(array_data, listFilter, field) {
    if (listFilter.length) {
      for (var i in listFilter) {
        for (let j = 0; j < array_data.length; j++) {
          if (listFilter[i][field] == array_data[j][field]) {
            let el = array_data.indexOf(array_data[j]);
            array_data.splice(el, 1);
          }
        }
      }
    }
    return array_data;
  }

  private onPageSizeChanged($event, page) {
    switch (page) {
      case "zone":
        this.rolePerPage = parseInt($event.target.value);
        break;
      case "warehouse":
        this.warehousePerPage = parseInt($event.target.value);
        break;
      case "customer":
        this.customerPerPage = parseInt($event.target.value);
        break;
    }
  }
}
