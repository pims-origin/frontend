import {Component } from '@angular/core';
import { FORM_DIRECTIVES,ControlGroup,FormBuilder, Control ,ControlArray,Validators} from '@angular/common';
import {RouteParams,Router ,RouteData} from '@angular/router-deprecated';
import {UserService, Functions} from '../../common/core/load';
import {CancalOrderBySKUServices} from './cancel-order-by-sku.service';
import { ValidationService } from '../../common/core/validator';
import {WMSBreadcrumb,PaginatePipe,WMSMessages, PaginationControlsCmp, PaginationService} from '../../common/directives/directives';
import { Http } from '@angular/http';
import {GoBackComponent} from "../../common/component/goBack.component";
import {BoxPopupService} from "../../common/popup/box-popup.service";
import {FormBuilderFunctions} from "../../common/core/formbuilder.functions";
@Component({
    selector: 'cancel-order-by-sku',
    providers: [CancalOrderBySKUServices,PaginationService,ValidationService,FormBuilder, BoxPopupService,UserService],
    directives: [FORM_DIRECTIVES,PaginationControlsCmp,WMSMessages,GoBackComponent,WMSBreadcrumb],
    pipes:[PaginatePipe],
    templateUrl: 'cancel-order-by-sku.component.html'
})

export class CancelOrderBySKUComponent {
    ODForm:ControlGroup;
    items:ControlGroup[] = [];
    itemsArray:ControlArray = new ControlArray(this.items);

    public Loading = [];
    private messages;
    private Action;
    private TitlePage='Cancle Order by SKU';
    private IsView:boolean=false;
    private perPage=20;
    private showLoadingOverlay:boolean=false;
    private orderData;
    private orderId;
    private allItem:Array<any>=[];
    private canceled:boolean = false;

    constructor(
        private _http: Http,
        private _Func: Functions,
        private params: RouteParams,
        private fb: FormBuilder,
        private _Valid: ValidationService,
        private _boxPopupService: BoxPopupService,
        private ALCService:CancalOrderBySKUServices,
        _RTAction: RouteData,
        private _user:UserService,
        private fbFunc:FormBuilderFunctions,
        private _router: Router) {


        this.Action=_RTAction.get('action');

        this.checkPermission();

    }

    // Check permission for user using this function page
    private allocateOrder;
    private allowAccess:boolean=false;
    checkPermission(){

        this.showLoadingOverlay = true;
        this._user.GetPermissionUser().subscribe(
            data => {
                this.showLoadingOverlay = false;
                this.allocateOrder = this._user.RequestPermission(data, 'allocateOrder');

                if(this.allocateOrder) {
                    this.allowAccess=true;
                    this.orderDetail();
                }
                else{
                    this.redirectDeny();
                }

            },
            err => {
                this.showMessages('danger',err);
                this.showLoadingOverlay = false;
            }
        );
    }


    redirectDeny(){
        this._router.parent.navigateByUrl('/deny');
    }

    private cancelOrder() {
        
        this.showLoadingOverlay = true;
        let that = this;
        this.ALCService.cancelOrder(this.orderId)
            .subscribe(
                data => {
                    this.showLoadingOverlay = false;
                    this.messages = {status: 'success', txt: data.data};
                    setTimeout( function(){
                        that._router.parent.navigateByUrl('/outbound/cancel-order');
                    }, 600);
                },
                err => {
                    this.showLoadingOverlay = false;
                    this.showMessages('danger', err);
                }
            )

    }

    private cancelOrderBySKU() {
        this.showLoadingOverlay = true;
        let that = this;
        this.ALCService.cancelOrderBySKU(JSON.stringify(this.ODForm.value))
            .subscribe(
                data => {
                    this.showLoadingOverlay = false;
                    this.messages = {status: 'success', txt: data.data};
                    setTimeout( function(){
                        that._router.parent.navigateByUrl('/outbound/order');
                    }, 600);
                },
                err => {
                    this.showLoadingOverlay = false;
                    this.showMessages('danger', err);
                }
            )
    }

    private Submit(){
        this.canceled = true;
        if(!this.ODForm.valid){
            return;
        }

        let arr_cancel_qty=this.ODForm.value['items'];
        let e=0;
        let sumcancel_qty:any;

        for(let i in arr_cancel_qty){
            if(arr_cancel_qty[i]['cancel_qty'] == this.allItem[i]['allocated_qty']){
                e++;
            }
            
            if(arr_cancel_qty[i]['cancel_qty']>0){
                sumcancel_qty=true;
            }
        }
        
        if(!sumcancel_qty){
            this.showMessages('danger','CANC001',false);
            return;
        }

        if(e==arr_cancel_qty.length){
            // call new api
            this.cancelOrder();

        } else{

            this.cancelOrderBySKU();
        }

    }

    formBuilder(data:Array<any>=[]) {

        this.ODForm = this.fb.group({
            items: this.itemsArray,
            cancel_reason: new Control('', Validators.compose([Validators.required]))
        });
        data.forEach(()=>{
            this.addNewItem();
        })
    }

    addNewItem() {

        this.itemsArray.push(
            new ControlGroup({
                'cancel_qty': new Control(0,
                    Validators.compose([Validators.required])),
                'odr_dtl_id': new Control()
            })
        );
    }

    checkInputNumber(uomCode, value, evt) {
        if (uomCode !== 'KG') {
            this._Valid.isNumber(evt,true);
        } else {
            this._Valid.validateFloatKeyPress(value, evt);
        }
    }

    orderDetail(){

        this.orderId=this.params.get('id');
        this.ALCService.getOrderDetail(this.orderId).subscribe(
            data => {
                this.orderData=data;
                this.orderData['totalSku']=this.orderData['items'] ? this.orderData['items'].length : 0;
                this.allItem = this.orderData.items ? this.orderData.items : [];
                this.formBuilder(this.allItem);
            },
            err => {
                this.showMessages('danger',err);
            },
            () => {}
        );

    }

    private showMessages (msgStatus ='danger' , err , api=true) {

        if(api){
            this.messages = {'status' : msgStatus ? msgStatus : 'danger', 'txt' : this._Func.parseErrorMessageFromServer(err)};
        }else{
            this.messages = {'status' : 'danger' , 'txt' : this._Func.msg(err)};
        }
        this._Func.scrollToTop();

    }

    redirectToList(){

        let n = this;
        this._boxPopupService.showWarningPopup()
            .then(function (dm) {
                if(dm)
                    window.history.back();
            })
    }

    changeCancelQty(i:any,allqty:any,$event:any){
        
        if(Number(allqty)<Number($event.target.value)){
            this.fbFunc.setErrorsControlItem(this.itemsArray,i,'cancel_qty',{'large_than_allqty':true})
        }

    }

}



