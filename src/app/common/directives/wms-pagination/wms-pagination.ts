import { Component, Input, Directive, Output, EventEmitter, ElementRef } from '@angular/core';
import { Functions,API_Config } from '../../../common/core/load';

@Component ({
  selector: 'wms-pagination',
  directives: [],
  providers: [],
  templateUrl: 'wms-pagination.html',
})

export class WMSPagination {
  Pagination;
  _perPage = 20;
  _shortText = false;


  @Input('paginData') set paginData(value: any) {
    this.Pagination = value;
  }

  @Input('perPage') set perPage(value: any) {
    this._perPage = value;
  }

  @Input('shortText') set shortText(value: any) {
    this._shortText = value;
  }

  @Output() pageChange = new EventEmitter();

  constructor() {}

  setCurrentPage(pageNum) {
    this.pageChange.emit(pageNum);
  }
}
