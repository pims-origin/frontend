import {Component} from '@angular/core';
import { FORM_DIRECTIVES,ControlGroup,FormBuilder, Control ,Validators} from '@angular/common';
import {API_Config, Functions, UserService} from '../../../common/core/load';
import {RouteParams,Router,RouteData } from '@angular/router-deprecated';
import {  ValidationService } from '../../../common/core/validator';
import {BoxPopupService} from "../../../common/popup/box-popup.service";
import { OrderBy } from "../../../common/pipes/order-by.pipe";
import {WMSBreadcrumb,WMSPagination,WMSMessages} from '../../../common/directives/directives';
import {GoBackComponent} from "../../../common/component/goBack.component";
import {PackingUpdateServices} from '../packing-update-service';
import {PaginationControlsCmp, PaginationService, PaginatePipe, DocumentUpload, DmsIn} from '../../../common/directives/directives';
declare var jQuery: any;
declare var saveAs: any;

@Component ({
    selector: 'packing-update',
    directives: [FORM_DIRECTIVES,GoBackComponent,WMSBreadcrumb,WMSMessages,WMSPagination, PaginationControlsCmp,DocumentUpload, DmsIn],
    pipes: [OrderBy, PaginatePipe],
    providers: [ValidationService,FormBuilder, BoxPopupService, PackingUpdateServices, PaginationService],
    templateUrl: 'packing-update.component.html',
})

export class PackingUpdateComponent {

    /** list permission need to check **/
    private hasUpdateOrderPackingPermission;
    /* end list permission */

    private messages;
    private showLoadingOverlay = false;

    private selectedAll = {};

    private order_id;
    private orderPackingData;
    private allItem = [];
    private itemPerPage = 20;
    private allCarton = [];
    private cartonPerPage = 20;
    private token = '';
    private isDisplayDocTab = false;
    private flagXDock = false;
    private listMsgs = {
        // AutoPack
        'AP1': 'Please choose items to auto pack.',
        'AP2': 'Auto Pack only accepts items which have the same Total QTY and Remaining QTY.',
        'AP3': 'Cartons have been packed successfully.'
    }

    constructor(
        private _func: Functions,
        private params: RouteParams,
        private _router:Router,
        private _Valid: ValidationService,
        private _user: UserService,
        private _boxPopupService: BoxPopupService,
        private _packingUpdateService: PackingUpdateServices,
        _RTAction: RouteData,
        private fb: FormBuilder,
        private _API: API_Config) {
        this.token = this._func.getToken();
        this.order_id = this.params.get('id');
        this.checkPermission();
    }

    private showMessage(msgStatus, msg) {
        this.messages = {
            'status': msgStatus,
            'txt': msg
        }
        jQuery(window).scrollTop(0);
    }

    private checkPermission() {
        this.showLoadingOverlay = true;
        this._user.GetPermissionUser().subscribe(
            data => {
                this.hasUpdateOrderPackingPermission = this._user.RequestPermission(data,'orderPacking');

                if(!this.hasUpdateOrderPackingPermission) {
                    this._router.parent.navigateByUrl('/deny');
                }
                else {
                    this.getOrderPackingDetail();
                }
            },
            err => {
                // console.log('check permission err', err);
                this.showMessage('danger', this._func.parseErrorMessageFromServer(err));
                this.showLoadingOverlay = false;
            }
        );
    }

    private getOrderPackingDetail() {
        this.showLoadingOverlay = true;
        this._packingUpdateService.getOrderPackingDetail(this.order_id)
            .subscribe(
                data => {
                    //console.log('get packing detail success', data);
                    this.orderPackingData = data.data;
                    this.allItem = this.orderPackingData.items;
                    for(var i = 0, l = this.allItem.length; i < l; i++) {
                        var item = this.allItem[i];
                        var successPercent = (item.alloc_qty - item.remain_qty)/item.alloc_qty * 100;
                        var remainPercent = 100 - successPercent;
                        this.allItem[i].successPercent = successPercent + '%';
                        this.allItem[i].remainPercent = remainPercent + '%';
                    }
                    this.allCarton = this.orderPackingData.cartons;
                    this.showLoadingOverlay = false;
                    this.isDisplayDocTab = true;
                },
                err => {
                    this.showMessage('danger', this._func.parseErrorMessageFromServer(err));
                    this.showLoadingOverlay = false;
                }
            );
    }

    private packCarton() {
        this.messages=false;
        let Arr = this.allItem;
        let ArrTemp = Arr.slice();
        let f = 0;
        var listItemChecked = [];
        for (let i = 0; i < Arr.length; i++) {
            if (Arr[i]['selected'] == true) {
                f++;
            }
        }
        if (f == 0) {
            this.showMessage('danger', this._func.msg('VR135'));
        }
        else{
            this.selectedAll={};
            ArrTemp.forEach((item)=> {
                if (item['selected'] == true) {
                    listItemChecked.push(item['odr_dtl_id']);
                }
            });
            this._router.parent.navigateByUrl('/outbound/packing/pack-carton/' + this.order_id + '::' + this.orderPackingData.cus_id + '/' + listItemChecked.join(','));
        }
    }

    private autoPackCarton() {
        this.flagXDock = (this.orderPackingData.odr_type.toUpperCase().trim()) == "CROSSDOCK";
        this.messages=false;
        let Arr = this.allItem;
        let ArrTemp = Arr.slice();
        let f = 0;
        var listItemChecked = [];
        const listItem = ArrTemp.filter(item => item['selected']);
      
        if (listItem.length == 0) {
            this.showMessage('danger', this.listMsgs.AP1);
        }
        else{
            if (!this.flagXDock) {
                var hasErrPacking = false;
                ArrTemp.forEach((item)=> {
                    if (item['selected'] == true) {
                        if(item.alloc_qty == item.remain_qty) {
                            listItemChecked.push({
                                'odr_dtl_id': item['odr_dtl_id'],
                                'itm_id': item['itm_id']
                            });
                        } else {
                            hasErrPacking = true;
                        }
                    }
                });
                if(hasErrPacking) {
                    this.showMessage('danger', this.listMsgs.AP2);
                } else {
                    this.showLoadingOverlay = true;
                    this._packingUpdateService.autoPack(this.order_id, JSON.stringify(listItemChecked)).subscribe(
                        data => {
                            this.showLoadingOverlay = false;
                            this.showMessage('success', this.listMsgs.AP3);
                            this.getOrderPackingDetail();
                        },
                        err => {
                            this.showLoadingOverlay = false;
                            this.showMessage('danger', this._func.parseErrorMessageFromServer(err));
                        }
                    )
                }
            } else {
                let data = [];
                listItem.forEach(item => {
                    data.push({
                        'odr_dtl_id': item['odr_dtl_id'],
                        'itm_id': item['itm_id']
                    });
                })
                this._packingUpdateService.autoPackXDock(this.order_id, JSON.stringify(data)).subscribe(
                    data => {
                        this.showLoadingOverlay = false;
                        this.showMessage('success', this.listMsgs.AP3);
                        this.getOrderPackingDetail();
                    },
                    err => {
                        this.showLoadingOverlay = false;
                        this.showMessage('danger', this._func.parseErrorMessageFromServer(err));
                    }
                )
            }
        }
    }

    private editCarton() {
        this.messages=false;
        let Arr = this.allCarton;
        let ArrTemp = Arr.slice();
        let f = 0;
        let selectedItem;
        for (let i = 0; i < Arr.length; i++) {
            if (Arr[i]['selected'] == true) {
                selectedItem = Arr[i];
                f++;
            }
        }
        if (f == 0) {
            this.showMessage('danger', this._func.msg('CF005'));
        }
        else
        if(f > 1) {
            this.showMessage('danger', this._func.msg('CF004'));
        }
        else {
            this._router.parent.navigateByUrl('/outbound/packing/pack-carton/' + this.order_id + '::' + this.orderPackingData.cus_id + '::' +  selectedItem['pack_hdr_id'] + '/edit');
        }
    }

    private Selected($event,item,ListName:string,state,paginId) {

        let Array=eval(`this.`+ListName);
        this.selectedAll[ListName]=false;

        if ($event.target.checked) {
            item['selected']=true;
            this.checkCheckAll(ListName,state,paginId);
        }
        else {
            item['selected']=false;
        }
    }

    private CheckAll(event,ListName:string,StatePagin,objName) {

        let Array=eval(`this.`+ListName);

        if(!Array.length)
        {
            this.selectedAll[ListName] = false;
            return;
        }
        if (event) {
            this.selectedAll[ListName] = true;
        }
        else {
            this.selectedAll[ListName]= false;
        }

        //console.log('List checkall from ',StatePagin ,  StatePagin[objName]['start'] , ' to ' , StatePagin[objName]['end']);
        let end=StatePagin[objName]['end'];
        if(StatePagin[objName]['size']<StatePagin[objName]['end'])
        {
            end=StatePagin[objName]['size'];
        }
        //console.log('end -> ' , end);
        for(let i=StatePagin[objName]['start'];i<end;i++)
        {
            //console.log(' i ' , i ,  Array[i]);
            Array[i]['selected']=this.selectedAll[ListName];
        }
    }

    checkCheckAll(ListName:string,StatePagin,objName) {
        let Array=eval(`this.`+ListName);
        // console.log('StatePagin > ', StatePagin);
        let el=StatePagin[objName];
        let end=el['end'];
        let chk=0;
        // console.log('check check all');

        if(el['size']) {

            if(el['size']<el['end'])
            {
                end=el['size'];
            }

            for(let i=el['start'];i<end;i++) {
                // console.log('i ' , i , Array[i]['selected']);
                if(Array[i]['selected'])
                {
                    chk++;
                }
            }
            if(chk==el['slice'].length) {
                this.selectedAll[ListName]=true;
            }
            else{
                this.selectedAll[ListName]=false;
            }

            // console.log('check all of list ' , ListName ,  this.selectedAll[ListName] );
        } // end if size
    }

    private onPageSizeChanged($event, page) {
        switch (page) {
            case "itemList":
                this.itemPerPage = parseInt($event.target.value);
                break;
            case "cartonList":
                this.cartonPerPage = parseInt($event.target.value);
                break;
        }
    }
    private printCarton(){
        let that = this;
        let Arr = that.allCarton;
        let ArrTemp = Arr.slice();
        let f = 0;
        let selectedItem = [];
        for (let i = 0; i < Arr.length; i++) {
            if (Arr[i]['selected'] == true) {
                selectedItem.push(Arr[i]);
                if (Arr[i]['has_rfid']) {
                    that.showMessage('danger', that._func.msg('PRCT01'));
                    f = 0;
                    break;

                }
                f++;
            }
        }

        if (f > 0) {
            let packHdrId = [];
            for(let i in selectedItem){
                packHdrId.push(selectedItem[i].pack_hdr_id);
            }
            let api = that._API.API_Pack  + '/packs/' + that.order_id + '/print?pack_hdr_id=' + packHdrId.join();
            try{
                let xhr = new XMLHttpRequest();
                xhr.open("GET", api , true);
                xhr.setRequestHeader("Authorization", 'Bearer ' +that._func.getToken());
                xhr.responseType = 'blob';

                xhr.onreadystatechange = function () {
                    if(xhr.readyState == 2) {
                        if(xhr.status == 200) {
                            xhr.responseType = "blob";
                        } else {
                            xhr.responseType = "text";
                        }
                    }

                    if(xhr.readyState === 4) {
                        if(xhr.status === 200) {
                            //update printed label value
                            for (let i = 0; i < Arr.length; i++) {
                                if (Arr[i]['selected'] == true) {
                                    Arr[i]['is_print'] = 1;
                                }
                            }
                            var fileName = 'Packed-Cartons-' + that.orderPackingData['odr_num'];

                            var blob = new Blob([this.response], {type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'});
                            saveAs.saveAs(blob, fileName + '.pdf');
                        }else{
                            let errMsg = '';
                            if(xhr.response) {
                                try {
                                    var res = JSON.parse(xhr.response);
                                    errMsg = res.errors && res.errors.message ? res.errors.message : xhr.statusText;
                                }
                                catch(err){errMsg = xhr.statusText}
                            }
                            else {
                                errMsg = xhr.statusText;
                            }
                            if(!errMsg) {
                                errMsg = that._func.msg('VR100');
                            }
                            that.showMessage('danger', errMsg);
                        }
                    }
                }
                xhr.send();
            }catch (err){
                that.showMessage('danger', that._func.msg('VR100'));
            }
        }else{
            that.showMessage('danger', that._func.msg('PRCT01'));
        }

    }

    goBack()
    {
        this._func.goBack_OrderType(this,this.orderPackingData);
    }
}
