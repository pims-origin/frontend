import {Component, Input} from '@angular/core';
import {DecimalPipe} from '@angular/common';

import {WMSPagination} from '../../common/directives/directives';
import {DashboardServices} from '../dashboard.service';
import {Functions} from '../../common/core/load';
import {PaginatePipe, PaginationControlsCmp} from '../../common/directives/directives';
import {WMSNumberFormatter} from '../../common/pipes/wms-number-formatter';

declare let Chart:any;

@Component({
	selector: 'cycle-count-dashboard',
	templateUrl: 'cycle-count-dashboard.html',
	directives: [WMSPagination, PaginationControlsCmp],
	providers: [DashboardServices],
	pipes: [PaginatePipe, DecimalPipe, WMSNumberFormatter]
})

export class CycleCountDashboard {
	private ccType:string = '';
	private cycleCountTypes: Array<any> = [];
	private whsId:string = '';

	@Input('currentWHSId') set setCurrentWHSId(value:any) {
        this.whsId = value;
        if (value) {
            setTimeout(() => {
                this.getCycleCount();
            }, 200);
        }
	}
	
	ngAfterViewInit()
	{
		this.getCycleCountType();
		this.getCycleCount();
	}

	constructor(private dashboardServices:DashboardServices, private funcs:Functions) {
	}

	private changeCycleCountType(event) {
		let type = '';
		if (event.target.value) {
			type = event.target.value;
		}
		this.getCycleCount(type);
	}

	private cycleCountList:Array<any> = [];
	private totalCC:any;

	private getCycleCount(type:string = '') {
		this.dashboardServices.getCycleCount(type).subscribe(
			data => {
				this.totalCC = data['meta'];
				this.cycleCountList = data.data;
				this.drawZoneCapaciesChart();
			},
			err => {
			}
		);
	}

	private CycleCountChart;

	drawZoneCapaciesChart() {
		const chartColors = ['#36c6d3', 'rgb(0, 128, 0)'];
		let dataLabels = [];
		let dataUsed = [];
		let dataUnused = [];
		for (let item of this.cycleCountList) {
			dataLabels.push(item.cycle_num);
			dataUsed.push(item.used_loc_total);
			dataUnused.push(parseInt(item.loc_total) - parseInt(item.used_loc_total));
		}
		let barChartData = {
			labels: dataLabels,
			datasets: [{
				label: 'Cycled Count Location',
				backgroundColor: chartColors[0],
				data: dataUsed
			}, {
				label: 'Uncycled Count Location',
				backgroundColor: chartColors[1],
				data: dataUnused
			}]

		};

		let ctx = document.getElementById('cycle-count-wrapper');
		if (ctx == null) {
			return true;
		}
		if (this.CycleCountChart) this.CycleCountChart.destroy();
		this.CycleCountChart = new Chart(ctx, {
			type: 'bar',
			data: barChartData,
			options: {
				title: {
					display: false,
					// text:""
				},
				tooltips: {
					callbacks: {
						title: (tooltipItem, data) => {
							let item = this.cycleCountList[tooltipItem[0].index];
							return `${item.cycle_num}: ${item.loc_total}`;
						},
						label: (tooltipItem, data) => {
							return data.datasets[tooltipItem.datasetIndex].label + ": " + this.funcs.formatSWISNumber(tooltipItem.yLabel);
						}
					},
					mode: 'label'
				},
				responsive: true,
				scales: {
					xAxes: [{
						stacked: true,
					}],
					yAxes: [{
						stacked: true,
						ticks: {
							callback: (value, index, values) => {
								if (value.toString().indexOf(".") !== -1) {
									return this.funcs.formatSWISNumber(value.toFixed(1));
								}
								return this.funcs.formatSWISNumber(value);
							}
						}

					}]
				}
			}
		});
	}
	private getCycleCountType () {
		this.dashboardServices.getCycleCountType()
		  .subscribe(
			data => {
			  this.cycleCountTypes = data.filter(x => x.cycle_type !== 'IMP');
			},
			err => {}
		  );
	  }
}
