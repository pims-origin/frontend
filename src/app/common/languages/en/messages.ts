export class Messages{
  public ListMgs=[
    { vrId: 'WMS001', valiRule: 'InputError', msgText: 'Please choose one item to edit' },
    { vrId: 'WMS002', valiRule: 'InputError', msgText: 'Please choose only one item to edit' },
    { vrId: 'WMSPCI001', valiRule: 'InputError', msgText: 'Please choose one item to print' },
    { vrId: 'WMSPCI002', valiRule: 'InputError', msgText: 'Please choose only one item to print' },
    { vrId: 'VR001', valiRule: 'Mandatory', msgText: 'The %s cannot be blank' },
    { vrId: 'VR002', valiRule: 'AbnormalCharacter', msgText: `Please don't insert any of following character: \ / * ? : < > |` },
    { vrId: 'VR003', valiRule: 'IsNull', msgText: 'Data is null' },
    { vrId: 'VR004', valiRule: 'IsNotNull', msgText: 'Data is not null' },
    { vrId: 'VR005', valiRule: 'IsTypeOf', msgText: 'The input must be %s type' },
    { vrId: 'VR006', valiRule: 'IsTypeOf', msgText: 'The input must not be %s type' },
    { vrId: 'VR007', valiRule: 'IsTrue', msgText: 'The %s is true' },
    { vrId: 'VR100', valiRule: 'Connection', msgText: "Sorry, there's a problem with the connection." },
    { vrId: 'VR101', valiRule: 'Connection', msgText: "Email not found." },
    { vrId: 'VR102', valiRule: 'Connection', msgText: "Sorry, there's a problem with the connection.<br> ! Please contact administrator" },
    { vrId: 'VR103', valiRule: 'PasswordInput', msgText: `Password at least 1 number<br>Password at least 1 lowercase character (a-z)<br>Password at least 1 uppercase character (A-Z)` },
    { vrId: 'VR104', valiRule: 'ResetPasswordDone', msgText: `Password was changed successfully! Please <a href="#/login">login</a> again.` },
    { vrId: 'VR105', valiRule: 'InvalidTokenUrl', msgText: `This URL is expired!<br>Please click <a href="#/forgot-password">here</a> to go back to the Forgot password page.` },
    { vrId: 'VR106', valiRule: 'InvalidTokenUrl', msgText: 'Please enter new password and confirm' },
    { vrId: 'VR107', valiRule: 'AddNewDone', msgText: 'Add new successfully' },
    { vrId: 'VR108', valiRule: 'InputError', msgText: 'Error ! Please fill out the form' },
    { vrId: 'VR109', valiRule: 'UpdateDone', msgText: 'Update data successfully' },
    { vrId: 'VR110', valiRule: 'InputError', msgText: 'Data input is invalid ! Please fill out the form !' },
    { vrId: 'VR111', valiRule: 'InputError', msgText: 'Data is invalid ! Please fix it and try again' },
    { vrId: 'BZ001', valiRule: 'InputError', msgText: 'Create Zone successfully. Now please add location(s) to zone.' },
    { vrId: 'BZ002', valiRule: 'InputError', msgText: 'No location was added to zone. Please confirm!' },
    { vrId: 'CF001', valiRule: 'Confirm', msgText: 'Please choose only one item to change status!' },
    { vrId: 'CF002', valiRule: 'Confirm', msgText: 'Please choose one item to change status!' },
    { vrId: 'CF003', valiRule: 'Confirm', msgText: 'Only upload file extend .xlsx !!!' },
    { vrId: 'CF004', valiRule: 'Confirm', msgText: 'Please choose only one item to edit!' },
    { vrId: 'CF005', valiRule: 'Confirm', msgText: 'Please choose one item to edit!' },
    { vrId: 'CF006', valiRule: 'Confirm', msgText: 'Please choose only one item to create put back receipt!' },
    { vrId: 'CF007', valiRule: 'Confirm', msgText: 'Please choose one item to create put back receipt !' },
    { vrId: 'CF008', valiRule: 'Confirm', msgText: 'Please choose one new ASN to edit!' },
    { vrId: 'CF009', valiRule: 'Confirm', msgText: 'Only upload file extend .csv!' },
    { vrId: 'CF010', valiRule: 'Confirm', msgText: 'Please choose at least 1 item to Print Barcode!' },

    { vrId: 'VR110', valiRule: 'Confirm', msgText: '{0} is greater than {1}' },
    { vrId: 'VR013', valiRule: 'Confirm', msgText: 'Max Count should be greater than Min Count' },
    { vrId: 'BZ003', valiRule: 'InputError', msgText: 'Zone code is duplicated. Please create another zone code.' },
    { vrId: 'BZ004', valiRule: 'InputError', msgText: 'Max Count should be greater than Min Count' },
    { vrId: 'VR112', valiRule: 'InputError', msgText: 'From date is not greater than to date' },
    { vrId: 'VR113', valiRule: 'InputError', msgText: 'Please select one item to remove!' },
    { vrId: 'VR114', valiRule: 'InputError', msgText: 'Do you want to remove location/locations?' },
    { vrId: 'BZ005', valiRule: 'InputError', msgText: 'Input file is invalid. Please check again.' },
    { vrId: 'VR115', valiRule: 'InputError', msgText: 'Are you sure you want to remove location/locations?' },
    // Customer msg
    { vrId: 'CU001', valiRule: 'AddMoreContact', msgText: 'You can only add maximum 5 contacts.' },
    { vrId: 'CU002', valiRule: 'AddMoreContact', msgText: 'You can only add maximum 2 addresses.' },

    { vrId: 'GR001', valiRule: 'AddMoreContact', msgText: 'Goods Receipt has discrepancy or damage. Please submit by RCVD-Pending!' },
    { vrId: 'ASN001', valiRule: 'AddMoreCtnr', msgText: 'Container is existed!' },
    { vrId: 'ASN002', valiRule: 'AddMoreItem', msgText: 'Please add at least 1 item' },

    { vrId: 'BZ006', valiRule: 'InputError', msgText: 'Zone has been updated successfully' },

    { vrId: 'CONS001', valiRule: 'InputError', msgText: 'Please choose one item to consolidation' },

    { vrId: 'CT001', valiRule: 'InputError', msgText: 'Please add at least 1 carton number !' },
    { vrId: 'VR116', valiRule: 'InputError', msgText: 'Sorry, there is problem with the connection. Please try to login later!' },
    { vrId: 'VR117', valiRule: 'InputError', msgText: 'Sorry, there is problem with our server. Please try to login later!' },

    { vrId: 'VR118', valiRule: 'InputError', msgText: 'Please choose one item to remove' },
    { vrId: 'VRLCT001', valiRule: 'InputError', msgText: 'Are you sure you want to remove the location/locations?' },

    { vrId: 'VR119', valiRule: 'InputError', msgText: 'The Reason field is required when Location Status is InActive' },
    { vrId: 'VR120', valiRule: 'InputError', msgText: 'Only Location have status Active can be change to Locked' },
    { vrId: 'LCT001', valiRule: 'InputError', msgText: 'Location Status has been changed successfully!' },


    // Relocation
    { vrId: 'RLC001', valiRule: 'Confirm', msgText: 'Locations are not active. Do you want to activate them ?' },

    // Assign CSR
    { vrId: 'VR121', valiRule: 'InputError', msgText: 'Please choose at least one Order to assign CSR' },
    // Allocate
    { vrId: 'VR122', valiRule: 'InputError', msgText: 'Please choose one Order to allocate' },
    { vrId: 'VR123', valiRule: 'InputError', msgText: 'Please choose only one Order to allocate' },
    { vrId: 'VR124', valiRule: 'InputError', msgText: 'Only New Orders can be allocated!' },
    { vrId: 'VR125', valiRule: 'InputError', msgText: 'Order has not been assigned CSR. Do you want to assign CSR to this Order?' },
    { vrId: 'VR126', valiRule: 'InputError', msgText: 'Only New Order can be edited!' },
    { vrId: 'ALC006', valiRule: 'InputError', msgText: 'Available quantity is less than requested quantity. Partial allocate is not supported for this order!' },

    // Create Wave Pick
    { vrId: 'VR127', valiRule: 'InputError', msgText: 'Please choose one Order to create Wave Pick'},
    { vrId: 'VR142', valiRule: 'InputError', msgText: 'Please choose only one Order to create Wave Pick'},
    { vrId: 'VR128', valiRule: 'InputError', msgText: 'Wave Pick can be created for Allocated or Partial Allocated Orders only!'},

    { vrId: 'VR129', valiRule: 'InputError', msgText: 'There is no CSR in charge of the selected Customers'},
    { vrId: 'VR130', valiRule: 'InputError', msgText: 'Order Pick can be executed for New Wave Pick or Picking only!'},
    { vrId: 'VR131', valiRule: 'InputError', msgText: 'Order Pick for Wave Pick {0} is executed successfully!'},

    { vrId: 'VR132', valiRule: 'InputError', msgText: 'Please choose one Order to pack' },
    { vrId: 'VR133', valiRule: 'InputError', msgText: 'Please choose only one Order to pack' },
    { vrId: 'VR134', valiRule: 'InputError', msgText: 'Only Picked or Packing Orders can be packed!' },

    { vrId: 'VR135', valiRule: 'InputError', msgText: 'Please choose items to pack!' },
    { vrId: 'VR136', valiRule: 'InputError', msgText: 'At least one item should have Pack Qty!' },
    { vrId: 'VR137', valiRule: 'InputError', msgText: 'Carton packed successfully!' },
    { vrId: 'VR138', valiRule: 'InputError', msgText: 'Order can not be allocated!' },
    { vrId: 'VR139', valiRule: 'InputError', msgText: 'At least one item should be allocated!' },
    { vrId: 'ALC009', valiRule: 'InputError', msgText: 'Order cannot be allocated due to not support partial!' },


    {vrId: 'WAV001', valiRule: 'InputError', msgText:'Picked QTY must be less than or equal to total QTY'},

    { vrId: 'VR140', valiRule: 'InputError', msgText: 'Please choose one Online Order to print Packing Slip!' },
    { vrId: 'VR141', valiRule: 'InputError', msgText: 'Please choose one item to print Wave Pick!' },
    /*work order */
    { vrId: 'M043', valiRule: 'WRReset', msgText: `The change may cause the input data.<br>Do you still want to process ?`},
    { vrId: 'BM044', valiRule: 'WRReset', msgText: 'Work Order should have at least 1 item!'},

    { vrId: 'BM050', valiRule: 'WRReset', msgText: 'Charged Qty of Workorder cannot be greater than that of Order!'},

    { vrId: 'CC001', valiRule: 'CCadd', msgText: 'Cycle count was added successfully!'},

    /*  */
    { vrId: 'XFER01', valiRule: 'InputError', msgText: 'Total Pieces cannot be less than Requested Pieces/XFER'},

    /*XFer*/
    {vrId: 'XF001', validRule: 'InputError', msgText: 'Please choose one item with status Pending to Create ticket.'},
    {vrId: 'BM056', validRule: 'InputError', msgText: 'Only XFER records with Pending status can be saved and printed wave pick ticket!'},
    {vrId: 'BM057', validRule: 'InputError', msgText: 'Only Wave Pick  with Picking status can be updated Inventories!'},
    {vrId: 'XF003', validRule: 'InputError', msgText: 'Please choose one item with status Picking to Update Inventory.'},
    {vrId: 'BOL001', valiRule: 'InputError', msgText: 'Only new shipment can be updated!'},
    {vrId: 'BOL003', valiRule: 'InputError', msgText: 'Please select orders.' },
    {vrId: 'BOL002', valiRule: 'InputError', msgText: 'Do you want to remove order/orders?' },
    {vrId: 'XF004', validRule: 'InputError', msgText: 'Please choose one XFER Ticket to Print PDF!'},
    {vrId: 'XF005', validRule: 'InputError', msgText: 'Please choose XFER Ticket with Pending or Complete status to print PDF!'},
    {vrId: 'XF006', validRule: 'InputError', msgText: 'Only Picking XFER can be updated Inventory.'},
    {vrId: 'XF007', validRule: 'InputError', msgText: 'Only XFER records with Pending status can be edited!'},
    {vrId: 'XF008', validRule: 'InputError', msgText: 'Please choose at least one item with status Pending to Create ticket.'},
    {vrId: 'XF009', valiRule: 'InputError', msgText: "Actual picked can't be equal to zero for all XFER Detail of XFER Number " },
    {vrId: 'XF010', validRule: 'InputError', msgText: 'Please choose one item with status Pending to edit.'},
    // Cycle count
    { vrId: 'CC001', valiRule: 'CCadd', msgText: 'Cycle count was added successfully!'},
    { vrId: 'CC002', valiRule: 'Confirm', msgText: 'Please choose only one item to adjust!' },
    { vrId: 'CC003', valiRule: 'CCAdjust', msgText: 'Please choose one item to adjust!' },
    { vrId: 'CC004', valiRule: 'Confirm', msgText: 'Please choose only one item to delete!' },
    { vrId: 'CC005', valiRule: 'CCDelete', msgText: 'Please choose one item to delete!' },
    { vrId: 'CC006', valiRule: 'Confirm', msgText: 'Please choose only one count item to update!' },
    { vrId: 'CC007', valiRule: 'CCUpdate', msgText: 'Please choose one count item to update!' },
    { vrId: 'CC008', valiRule: 'CCConfirm', msgText: 'You have\'t permission to action!' },
    { vrId: 'CC009', valiRule: 'CCUpdate', msgText: 'Please choose one item to update!' },
    { vrId: 'CC010', valiRule: 'CCadd', msgText: 'Cycle count reason was added successfully!'},
    { vrId: 'BS001', valiRule: 'RSadd', msgText: 'Block stock reason was added successfully!'},
    { vrId: 'BS002', valiRule: 'RSadd', msgText: 'Delivery Service was added successfully!'},
    { vrId: 'CC0010', valiRule: 'Confirm', msgText: 'Please choose one item to accept!' },
    { vrId: 'CC0011', valiRule: 'Confirm', msgText: 'Please choose one item to recount!' },
    { vrId: 'CANC001', valiRule: 'InputError', msgText: 'Please cancel 1 SKU at least!'},
    { vrId: 'CC012', valiRule: 'CCUpdate', msgText: 'Please choose at least one location to unset Rfid!'},
    { vrId: 'CC013', valiRule: 'CCUpdate', msgText: 'Unset RFIDs successfully!'},


    // WareHouse Layout
    { vrId: 'WHLO001', valiRule: 'InputError', msgText: 'No Results Found.'},

    { vrId: 'CP002', valiRule: 'Confirm', msgText: 'Please choose only one Goods Receipt to assign carton to pallet!' },
    { vrId: 'CP003', valiRule: 'CCAdjust', msgText: 'Please choose one Goods Receipt to assign carton to pallet!' },
    { vrId: 'CP004', valiRule: 'CCAdjust', msgText: 'Cartons have been assigned to pallets for this Goods Receipt!' },
    { vrId: 'GR002', valiRule: 'GR', msgText: 'Only Receiving Goods Receipt can be updated!' },

    { vrId: 'PAS01', valiRule: 'InputError', msgText: 'Please choose at least one Goods Receipt to assign Putter' },
    { vrId: 'PAS02', valiRule: 'InputError', msgText: 'There is no Putter in charge of the selected Customers'},
    { vrId: 'WPS01', valiRule: 'InputError', msgText: 'Please choose at least one Wave Pick to assign Picker' },

    { vrId: 'WPS02', valiRule: 'InputError', msgText: 'There is no Picker in charge of the selected Customers' },

    { vrId: 'OPU001', valiRule: 'InputError', msgText: 'Picking update successfully!' },
    { vrId: 'LOC001', valiRule: 'InputError', msgText: 'Status of Locked Location cannot be changed!' },
    { vrId: 'SF001', valiRule: 'SC', msgText: 'Thanks for contacting with us! We will get in touch with you shortly' },

    { vrId: 'NOT_FOUND', valiRule: 'SC', msgText: 'No Results Found.' },

    { vrId: 'WP001', valiRule: 'WP', msgText: 'Wave Pick has been updated. Picker cannot be assigned again!' },
    { vrId: 'WP002', valiRule: 'WP', msgText: 'Cartons has been assigned to order successfully!'},
    { vrId: 'WP003', valiRule: 'CCadd', msgText: 'Wave pick has been created successfully' },
    { vrId: 'WP004', valiRule: 'WP', msgText: 'Wave Pick {0} has been updated. Picker cannot be assigned again!' },

    { vrId: 'PAS001', valiRule: 'PA', msgText: 'Putaway has been updated. Handler cannot be assigned again!' },
    { vrId: 'PBAP001', valiRule: 'InputError', msgText: 'Items cannot be deleted all.'},
    { vrId: 'PBAP002', valiRule: 'Confirm', msgText: 'Are you sure you want to delete?'},
    { vrId: 'PBAP003', valiRule: 'Confirm', msgText: 'Assign pallet successfully!'},

    { vrId: 'PBAP004', valiRule: 'Confirm', msgText: 'Pallets have been assigned to this Put back Order already!'},

    { vrId: 'PAS03', valiRule: 'InputError', msgText: 'Only New Put Back Order can be assigned to pallets!' },
    { vrId: 'PAS04', valiRule: 'InputError', msgText: 'Please select one Put Back Order to assign pallets!'},
    { vrId: 'PAS05', valiRule: 'InputError', msgText: 'Please select one Put Back Order to print PDF!'},
    { vrId: 'PAS06', valiRule: 'InputError', msgText: 'Please select Putaway of the same customer to assign handler.'},
    { vrId: 'PAS07', valiRule: 'InputError', msgText: 'Only Putback with status Suggested can be updated.'},
    { vrId: 'PAS08', valiRule: 'InputError', msgText: 'This Goods Receipt is created from WAP/GUN' },

    { vrId: 'WO001', valiRule: 'InputError', msgText: 'Work Order has been finalized.'},
    { vrId: 'WO002', valiRule: 'InputError', msgText: 'Please select Work Order with status New to edit.'},
    { vrId: 'GR0013', valiRule: 'InputError', msgText: 'Putaway cannot be printed for Goods Receipt scanned from WAP!'},
    { vrId: 'GR0014', valiRule: 'InputError', msgText: 'Please select only one Goods Receipt to print putaway list!'},
    { vrId: 'GR0015', valiRule: 'InputError', msgText: 'Please select one Goods Receipt to print putaway list!'},

    { vrId: 'GRA001', valiRule: 'InputError', msgText: 'Please select only one Goods Receipt to approve!'},
    { vrId: 'GRA002', valiRule: 'InputError', msgText: 'Please select at least one Goods Receipt to approve!'},
    { vrId: 'GRA003', valiRule: 'InputError', msgText: 'Only Receiving Goods Receipt can be approve!'},

    { vrId: 'DMCT001', valiRule: 'Confirm', msgText: 'Unset damaged cartons successfully!'},
    { vrId: 'DMCT002', valiRule: 'Confirm', msgText: 'Update carton successfully!'},
    { vrId: 'CC0012', valiRule: 'Confirm', msgText: 'Please select one cycle count to change assignee!' },
    { vrId: 'CC0013', valiRule: 'Confirm', msgText: 'Change assignee successfuly.' },
    { vrId: 'CC0014', valiRule: 'Confirm', msgText: 'Cycle count has been completed. Assignee cannot be changed' },

    { vrId: 'SD001', valiRule: 'Confirm', msgText: 'Shipped Date can be updated for order having Shipped status only!'},
    { vrId: 'CLGR001', valiRule: 'Confirm', msgText: 'Complete Goods Receipt successfully!'},
    { vrId: 'CLGR002', valiRule: 'Confirm', msgText: 'This Goods Receipt was created from WAP. No need to assign carton to pallet.'},
    { vrId: 'CLGR003', valiRule: 'Confirm', msgText: 'Complete Goods Receipts successfully.'},
    { vrId: 'CCN001', valiRule: 'Confirm', msgText: 'Only cycle count notification with new status can be edited.'},
    { vrId: 'CCN002', valiRule: 'Confirm', msgText: 'Please choose one item to add cycle count.'},
    { vrId: 'PBR001', valiRule: 'Confirm', msgText: 'Put Back Receipt was created successfully.'},
    { vrId: 'CCN003', valiRule: 'Confirm', msgText: 'Cycle count can be created for cycle count notification with new status.'},
    { vrId: 'WMS003', valiRule: 'InputError', msgText: 'Please choose one item to cancel Cycle Count Notification' },
    { vrId: 'CCN004', valiRule: 'InputError', msgText: 'Are you sure you want to cancel Cycle Count Notification?' },
    { vrId: 'PA0003', valiRule: 'InputError', msgText: 'Assign Handler successfully.' },
    { vrId: 'BOL004', valiRule: 'InputError', msgText: 'Please choose only one BOL to print BOL LPNs!' },
    { vrId: 'BOL005', valiRule: 'InputError', msgText: 'Please select New Bill of Ladings to print LPNs.' },
    { vrId: 'VRRVOD01', valiRule: 'InputError', msgText: 'Please choose at least one Order to revert order!' },
    { vrId: 'VRRVOD02', valiRule: 'InputError', msgText: 'Please choose only one Order to revert order!' },
    { vrId: 'VRRVOD03', valiRule: 'InputError', msgText: 'Only Order with Allocated and Picking status can be reverted.' },
    { vrId: 'ODR001', valiRule: 'Confirm', msgText: 'Order has been reverted to New successfully.'},
    { vrId: 'ODIT001', valiRule: 'Confirm', msgText: "Input data will be reset if you make this change. Are you sure you want to change this?"},
    // Inbound messages
    { vrId: 'INB001', valiRule: 'Confirm', msgText: 'Only New ASN can be canceled.'},
    { vrId: 'INB002', valiRule: 'Confirm', msgText: 'Cancel ASN successfully.'},



    {vrId: 'VERSION_CHANGE', valiRule: 'Confirm', msgText : "A new version has just been deployed. Do you want to re-load the page to get up-to-date?"},
    {vrId: 'IDLE_TIME_WARNING', valiRule: 'Confirm', msgText : "<b>Your session is about to expire!</b>\n\nYou will be logged out in 60 seconds.\nDo you want to stay signed in?"},
    {vrId: 'IDLE_TIME_EXPIRED', valiRule: 'Confirm', msgText : "Your session has been expired. Please log in again!"},

    { vrId: 'TFPLT001', valiRule: 'Confirm', msgText: 'Pallet has been transfered successfully.'},
    { vrId: 'ASNM043', valiRule: 'WRReset', msgText: 'You have unsaved changes. Are you sure you want to leave?'},
    {vrId: 'WP02321', valiRule: 'Confirm', msgText : "Only New Wave Pick can be assign Pickers."},
    {vrId: 'WP02322', valiRule: 'Confirm', msgText : "Picker has been assigned sucessfully."},
    { vrId: 'WP02323', valiRule: 'InputError', msgText: 'Please choose at least one item to assign Picker' },

    { vrId: 'ROLE001', valiRule: 'Confirm', msgText: 'Created Role successfully.'},
    { vrId: 'ROLE002', valiRule: 'Confirm', msgText: 'Updated Role successfully.'},
    { vrId: 'ODR002', valiRule: 'InputError', msgText: 'Only Order with Picking and Partial Picking status can be completed.' },
    { vrId: 'ODR003', valiRule: 'Confirm', msgText: 'Order has been updated successfully.'},
    { vrId: 'WMS004', valiRule: 'InputError', msgText: 'This Goods Receipt is received from Barcode Gun. Please put away by the Barcode Gun and complete it' },
    { vrId: 'WMS005', valiRule: 'InputError', msgText: 'BOL has been finalized. Order cannot be cancelled.' },
    { vrId: 'WMS006', valiRule: 'InputError', msgText: 'X-Dock Order cannot be cancelled.' },
    { vrId: 'ODRPCI001', valiRule: 'InputError', msgText: 'Only Shipped Order can be printed.' },
    { vrId: 'ODRPCI002', valiRule: 'InputError', msgText: 'BOL has not been created for this Order yet.' },

    { vrId: 'OPS001', valiRule: 'InputError', msgText: 'Order Picking Slip can be printed for Order with status from Picked to Shipped.' },
    { vrId: 'PLT001', valiRule: 'InputError', msgText: 'Volume updated successfully.' },
    { vrId: 'ODR004', valiRule: 'Confirm', msgText: 'Schedule To Ship Date can be updated for order reaches Picked status only!'},
    { vrId: 'ODR005', valiRule: 'Confirm', msgText: 'Only CSRs of Order can update Schedule to Ship Date!'},
    { vrId: 'ODR006', valiRule: 'Confirm', msgText: 'Only CSRs of Order can update Routed Date!'},
    { vrId: 'ODR007', valiRule: 'Confirm', msgText: 'Routed Date can be updated for order having Scheduled To Ship status only!'},
    { vrId: 'PLT001', valiRule: 'InputError', msgText: 'Volume updated successfully.' },
    { vrId: 'PLT002', valiRule: 'InputError', msgText: 'Outbound Pallet deleted successfully.' },
    { vrId: 'PRCT01', valiRule: 'InputError', msgText: 'Please select none-RFID carton(s) to print the label.' },
    { vrId: 'PRCT02', valiRule: 'InputError', msgText: 'Please only one carton to print the shipping label.' },
    { vrId: 'CSRC01', valiRule: 'InputError', msgText: 'Updated Comments successfully!' },
    { vrId: 'ASN003', valiRule: 'Confirm', msgText: 'Complete Receiving successfully!'},
    { vrId: 'ASN004', valiRule: 'Confirm', msgText: 'Undo Complete successfully!'},
    { vrId: 'ODR008', valiRule: 'Confirm', msgText: 'Unpack Cartons successfully!' },
    { vrId: 'ODR009', valiRule: 'Confirm', msgText: 'Are you sure you want to Unpack Cartons?'},
    { vrId: 'PRI001', valiRule: 'InputError', msgText: 'Please choose one item to print' },
    { vrId: 'PRI002', valiRule: 'InputError', msgText: 'Please choose only one item to print' },
    { vrId: 'ODR010', valiRule: 'InputError', msgText: 'Merge orders successfully!' },
    { vrId: 'ODR011', valiRule: 'InputError', msgText: 'Please choose at least two items to merge orders.' },
    { vrId: 'ODR012', valiRule: 'Confirm', msgText: 'Tracking Number has been updated successfully!'},
    { vrId: 'XD0001', valiRule: 'InputError', msgText: 'Please choose only one Order to X-Dock' },
    { vrId: 'XD0002', valiRule: 'InputError', msgText: 'Please choose one Order to X-Dock' },
    { vrId: 'XD0003', valiRule: 'InputError', msgText: 'Only X-Dock Orders with status is X-Dock Processing can be complete!' },
    { vrId: 'XD0004', valiRule: 'Confirm', msgText: 'X-Dock has been updated successfully.'},
    { vrId: 'CSRC02', valiRule: 'InputError', msgText: 'Updated Picking successfully!' },

    { vrId: 'LC001', valiRule: 'InputError', msgText: 'Floor Location can not be edited!'},

    { vrId: 'LC002', valiRule: 'InputError', msgText: 'Floor Location can not be changed status!'},

    { vrId: 'OD001', valiRule: 'InputError', msgText: 'Please choose at least one Order to assign driver' },
    { vrId: 'OD002', valiRule: 'InputError', msgText: 'Only shipped can be assgin driver!' },
    { vrId: 'OD003', valiRule: 'Confirm', msgText: 'Assign Driver successfully.' },
    { vrId: 'WPA001', valiRule: 'InputError', msgText: 'Please assign Picker!' },
    { vrId: 'ITM001', valiRule: 'InputError', msgText: 'No items found. Would you like to create a new item?' },
  ];

}
