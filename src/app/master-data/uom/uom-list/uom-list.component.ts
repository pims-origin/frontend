import {Component} from '@angular/core';
import { Router } from '@angular/router-deprecated';
import {API_Config, UserService, Functions} from '../../../common/core/load';
import {UOMServices} from "../uom-service";
import { FORM_DIRECTIVES,ControlGroup,FormBuilder, Control} from '@angular/common';
import { Http } from '@angular/http';
import {WMSBreadcrumb} from '../../../common/directives/directives';
declare var jQuery: any;
@Component({
    selector: 'uom-list',
    providers: [UOMServices, FormBuilder],
    directives: [FORM_DIRECTIVES,WMSBreadcrumb],
    templateUrl: 'uom-list.component.html',
})

export class UOMListComponent {
    private uomList: Array<any> = [];
    private selectedAll = false;
    public listSelectedItem = [];
    private Pagination;
    private totalCount = 0;
    private tmpPageCount;
    private pageCount=0;
    private ItemOnPage=0;
    private currentPage = 1;
    private perPage = 20;
    private numLinks = 3;
    private messages;
    searchForm: ControlGroup;
    public loading = [];
    public showLoadingOverlay = false;
    private hasEditPermission = false;
    private hasCreatePermission = false;
    private viewUser = false;
    private searchCriteria = {};
    constructor(private _http:Http,
                private _Func:Functions,
                private _user:UserService,
                private fb:FormBuilder,
                private _uomService: UOMServices,
                private _router:Router) {

        this.builderSearchForm();
        this.searchCriteria = this._Func.genSearchCriteriaFromForm(this.searchForm);
        this.checkPermission();

    }
    // Check permission for user using this function page
    private checkPermission() {
        this.showLoadingOverlay = true;
        this._user.GetPermissionUser().subscribe(
            data => {
                this.hasEditPermission = this._user.RequestPermission(data, 'editUOM');
                this.hasCreatePermission = this._user.RequestPermission(data, 'createUOM');
                this.viewUser = this._user.RequestPermission(data,'viewUOM');
                /* Check orther permission if View allow */
                if(!this.viewUser) {
                    this._router.parent.navigateByUrl('/deny');
                }
                else {
                    this.getUOMList();
                }
            },
            err => {
                this.showMessage('danger', this._Func.parseErrorMessageFromServer(err));
                this.showLoadingOverlay = false;
            }
        );
    }

    private showMessage(msgStatus, msg) {
        this.messages = {
            'status': msgStatus,
            'txt': msg
        }
        jQuery(window).scrollTop(0);
    }

    private getUOMList(page =null) {
        let param="?page="+page+"&limit="+this.perPage;
        if(Object.keys(this.searchCriteria).length !== 0) // check if searchCriteria Object not empty  ( {} )
        {
            param += '&sys_uom_code=' + this.searchCriteria['sys_uom_code'] + '&sys_uom_name='
                + this.searchCriteria['sys_uom_name'];
        }
        if(this.sortQuery != '') {
            param += '&'+this.sortQuery;
        }
        this.loading['getUOM']=true;
        this.showLoadingOverlay = true;
        this._uomService.getUOM(param).subscribe(
            data =>{
                this.selectedAll=false;
                this.uomList=data.data;
                //paging funtion
                this.Pagination=data['meta']['pagination'];
                this.Pagination['numLinks']=3;
                this.Pagination['tmpPageCount']=this._Func.Pagination(this.Pagination);

                this.loading['getUOM']=false;
                this.showLoadingOverlay = false;
            },
            err => {},
            () => {}
        );
    }

    private checkSelectItem(item) {
        let id = item['sys_uom_id'];
        item.selected = !item.selected;
        this.selectedAll = false;
        if (item.selected) {
            this.listSelectedItem.push(id);
            this.setItemListSelected(id, true);

            if(this.listSelectedItem.length==this.uomList.length)
            {
                this.selectedAll=true;
            }
        }
        else {
            let el = this.listSelectedItem.indexOf(id);
            // remove item out of array
            this.listSelectedItem.splice(el, 1);
            this.setItemListSelected(id, false);
        }
    }

    private setItemListSelected(id,value) {
        this.uomList.forEach((item) => {
            if(item['sys_uom_id'] == id) {
                item['selected'] = value;
            }
        });
    }

    private checkAll(event) {
        this.selectedAll = !this.selectedAll;
        // set empty array  ListSelectedItem
        this.listSelectedItem=[];
        // Loop
        this.uomList.forEach((item) => {
            item['selected'] = this.selectedAll;
            if(this.selectedAll) {
                // if check all = true , pushing to ListSelectedItem
                this.listSelectedItem.push(item['sys_uom_id']);
            }
        })
    }

    private editSelectedItem() {
        if (this.listSelectedItem.length > 1) {
            this.messages = this._Func.Messages('danger',this._Func.msg('CF004'));
        }
        else {
            if(this.listSelectedItem.length) {
                this._router.navigateByUrl('/master-data/uom/' + this.listSelectedItem[0] + '/edit');
            }
            else {
                this.messages = this._Func.Messages('danger',this._Func.msg('CF005'));
            }
        }
    }

    private onPageSizeChanged($event) {
        this.perPage = $event.target.value;
        if((this.Pagination['current_page'] - 1) * this.perPage >= this.Pagination['total']) {
            this.currentPage = 1;
        }
        else {
            this.currentPage = this.Pagination['current_page'];
        }
        this.getUOMList(this.currentPage);
    }

    private filterList(num) {
        this.currentPage = num;
        this.getUOMList(num);
    }

    private getPage(num) {
        let arr=new Array(num);
        return arr;
    }

    private builderSearchForm() {
        this.searchForm =this.fb.group({
            'sys_uom_code': [''],
            'sys_uom_name': [''],
            'sys_uom_type': ['']
        });
    }

    search(data) {
        this.messages = null;
        this.loading['searching']=true;
        this.showLoadingOverlay = true;

        let param="?page="+1+"&limit="+this.perPage;
        this.searchCriteria = this._Func.genSearchCriteriaFromForm(this.searchForm);
        let encodeSearchQuery = param+"&sys_uom_code="+encodeURIComponent(this.searchCriteria['sys_uom_code'])+
                            "&sys_uom_name="+encodeURIComponent(this.searchCriteria['sys_uom_name']) +
                            "&sys_uom_type="+encodeURIComponent(this.searchCriteria['sys_uom_type']);
        if(this.sortQuery != '') {
            param += '&'+this.sortQuery;
        }
        this._uomService.search(encodeSearchQuery).subscribe(
            data => {

                // disable loading icon
                this.loading['searching']=false;
                this.showLoadingOverlay = false;
                this.selectedAll = false;

                this.uomList = data.data;
                //pagin function
                this.Pagination=data['meta']['pagination'];
                this.Pagination['numLinks']=3;
                this.Pagination['tmpPageCount']=this._Func.Pagination(this.Pagination);
            },
            err =>{
                this.messages = this._Func.Messages('danger','Not found');
            },
            () => {}
        );
    }

    resetSearch(data) {
        // if(data.sys_uom_code || data.sys_uom_name) {
            (<Control>this.searchForm.controls["sys_uom_code"]).updateValue("");
            (<Control>this.searchForm.controls["sys_uom_name"]).updateValue("");
            (<Control>this.searchForm.controls["sys_uom_type"]).updateValue("");
            this.searchCriteria = this._Func.genSearchCriteriaFromForm(this.searchForm);
            this.getUOMList(this.Pagination['current_page']);
        // }
    }
    private FieldsToSortFlag = {
        sys_uom_code: false,
        sys_uom_name: false,
    };
    private countClick = {
        sys_uom_code: 3,
        sys_uom_name: 3,
    }
    private sortQuery = 'sort[sys_uom_code]=asc';
    sort(field) {
        if(this.countClick[field] == 3) {
            this.countClick[field] = 0;
        }

        this.countClick[field] ++;
        if(this.countClick[field] == 3) {
            this.sortQuery = 'sort[sys_uom_code]=asc';
        }
        else {
            this.FieldsToSortFlag[field] = !this.FieldsToSortFlag[field];

            this.sortQuery = 'sort['+field+']';
            if(this.FieldsToSortFlag[field]) {
                this.sortQuery += '=asc';
            } else {
                this.sortQuery += '=desc';
            }
        }

        if(field) {
            for (let sortField in this.FieldsToSortFlag ) {
                if(sortField != field) {
                    this.FieldsToSortFlag[sortField] = false;
                    this.countClick[sortField] = 3;
                }
            }
        }
        this.getUOMList(this.currentPage);
    }
}
