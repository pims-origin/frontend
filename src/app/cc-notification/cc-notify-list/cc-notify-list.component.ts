import {Component} from '@angular/core';
import { Router, RouterLink, ROUTER_DIRECTIVES, RouteParams } from '@angular/router-deprecated';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import {Functions} from "../../common/core/functions";
import {JwtHelper} from "angular2-jwt/angular2-jwt";
import {API_Config} from '../../common/core/load';
import {WMSPagination, AdvanceTable, WMSBreadcrumb} from '../../common/directives/directives';
import { OrderBy } from "../../common/pipes/order-by.pipe";
import {WMSMessages} from "../../common/directives/messages/messages";
import {UserService} from "../../common/users/users.service";
import {CCNotifyServices} from "../cc-notify-service";
import {BoxPopupService} from "../../common/popup/box-popup.service";
import {WarningBoxPopup} from "../../common/popup/warning-box-popup";
import { DataShareService } from '../../common/services/data-share.service';
declare var jQuery: any;
declare var jsPDF: any;
declare var saveAs: any;

@Component({
  selector: 'cc-notify-list',
  directives: [ROUTER_DIRECTIVES, AdvanceTable, WMSMessages,WMSBreadcrumb, WMSPagination],
  templateUrl: 'cc-notify-list.component.html',
  pipes: [OrderBy],
  providers: [CCNotifyServices, BoxPopupService]

})
export class CCNotifyListComponent{

  private tableID = 'gr-list';
  private headerURL = this._API_Config.API_User_Metas + '/ccn';
  private headerDef = [{id: 'ver_table', value: 2},
    {id: 'ck', name: '', width: 30},
    {id: 'loc_code', name: 'Location', width: 70},
    {id: 'item_id', name: 'Item ID', width: 70},
    {id: 'sku', name: 'SKU', width: 70},
    {id: 'size', name: 'Size', width: 70,  unsortable: true},
    {id: 'color', name: 'Color', width: 70,  unsortable: true},
    {id: 'lot', name: 'Lot', width: 70,  unsortable: true},
    {id: 'pack', name: 'Packzie', width: 70,  unsortable: true},
    {id: 'remain_qty', name: 'Remain QTY', width: 70,  unsortable: true},
    {id: 'reason', name: 'Reason', width: 70,  unsortable: true},
    {id: 'created_at', name: 'Created Date', width: 150,  unsortable: true},
    {id: 'cc_ntf_sts_name', name: 'Status', width: 120, unsortable: true},
    ];
  private pinCols = 5;
  private rowData: any[];
  private getSelectedRow;
  private objSort;
  private searchParams;
  private whs_id: any;
  private Pagination;
  private totalCount = 0;
  private tmpPageCount;
  private pageCount=0;
  private ItemOnPage=0;
  private currentPage = 1;
  private perPage = 20;
  private numLinks = 3;
  private dataListGoodReceipt = [];
  private dataListStatus = [];
  private messages;
  private listMessage = {};
  private showLoadingOverlay = false;
  private hasPermissionView = false;
  private dataCSRUser = [];
  private assignUser;
  private listSelectedGR = [];
  private refreshRowData = false;
  private listSelectedItem = [];
  private listDataAutocomplete = {};
  private listNull = {};
  private hasPermissionCreate = false;
  private queryParams;

  // Construct
  constructor (
      private _CCNServices: CCNotifyServices,
      private _func: Functions,
      private _boxPopupService:BoxPopupService,
      private _http: Http,
      public jwtHelper: JwtHelper,
      private _API_Config: API_Config,
      private _user: UserService,
      private _router:Router,
      private dataShareService: DataShareService
  ) {
    this.listMessage = [];
    this._getQueryParams();
    this.whs_id = localStorage.getItem('whs_id');
    this.getStatusCCN();
    this.checkPermission();
    this.getCycleCountUsers();
  }

  ngOnDestroy() {
    this._resetQueryParams();
  }

  private _resetQueryParams() {
    this.queryParams = null;
    this.dataShareService.data = '';
  }

_getQueryParams() {
    // get params from Dashboard
    const queryString = this.dataShareService.data;

    this.queryParams = this._func.parseQueryString(queryString);
    if(queryString.length) {
        if(this.searchParams) {
            this.searchParams += "&" + queryString;
        } else {
            this.searchParams = "&" + queryString;
        }
    }
}

  private hasPermissionCreateCC = false;
  private checkPermission() {
    this.showLoadingOverlay = true;
    this._user.GetPermissionUser().subscribe(
        data => {
          this.hasPermissionView = this._user.RequestPermission(data,'viewCycleCountNotification');
          this.hasPermissionCreate = this._user.RequestPermission(data,'createCycleCountNotification');
          this.hasPermissionCreateCC = this._user.RequestPermission(data,'createCycleCount');

          if(!this.hasPermissionView) {
            this._router.parent.navigateByUrl('/deny');
          }
          else {
            this.getListCCN(1, '&cc_ntf_sts=NW');
          }
        },
        err => {
          this.showMessage('danger', this._func.parseErrorMessageFromServer(err));
          this.showLoadingOverlay = false;
        }
    );
  }

  private showMessage(msgStatus, msg) {
    this.messages = {
      'status': msgStatus,
      'txt': msg
    }
    jQuery(window).scrollTop(0);
  }

  // Format data for ag-grid
  private createRowData(data) {
    var rowData: any[] = [];
    // Check data
    if (typeof data.data != 'undefined') {
      data = this._func.formatData(data.data);
      for (var i = 0; i < data.length; i++) {
        rowData.push({
          loc_code: data[i].loc_code,
          item_id: data[i].item_id,
          sku: data[i].sku,
          size: data[i].size,
          color: data[i].color,
          lot: data[i].lot,
          pack: data[i].pack,
          remain_qty: data[i].remain_qty,
          created_at: data[i].created_at,
          reason: data[i].reason,
          cc_ntf_sts: data[i].cc_ntf_sts,
          cc_ntf_sts_name: data[i].cc_ntf_sts_name,
          ccn_id: data[i].ccn_id,
          loc_id: data[i].loc_id,
          cus_id: data[i].cus_id,
          uom_code: data[i].uom_code,
          uom_name: data[i].uom_name,
          des: data[i].des,
        })
      }
    }

    this.rowData = rowData;
  }

  private getListCCN(page = null, $params = '') {

    this.messages = false;
    this.showLoadingOverlay = true;
    if(!page) page = 1;
    var params = "?page="+page+"&limit="+this.perPage + $params;
    if(this.objSort && this.objSort['sort_type'] != 'none') {
      params += '&sort['+ this.objSort['sort_field']+ ']='+ this.objSort['sort_type'];
    }
    else {
      params += '&sort[created_at]=desc';
    }
    if(this.searchParams) {
      params += this.searchParams;
    }
    this._CCNServices.getListCCN(this.whs_id, params)
        .subscribe(
            data => {

              this.dataListGoodReceipt = data.data;
              this.initPagination(data);
              this.createRowData(data);
              this.showLoadingOverlay = false;
            },
            err => {

              this.parseError(err);
              this.showLoadingOverlay = false;
            },
            () => {

            }
        );
  }

  // Get customers
  private getStatusCCN () {

    var $params ='?limit=10000';

    this._CCNServices.getStatusCCN(this.whs_id)
        .subscribe(
            data => {

              this.dataListStatus = data.data;
            },
            err => {

              this.parseError(err);
            },
            () => {}
        );

  }

  // Get customers
  private autoSearchComplete ($nameField, $e, item: any = {}) {

    this.listDataAutocomplete['loading-' + $nameField] = true;

    if ($nameField == 'location-edit') {


      item['flagDisSku'] = false;
      this.listNull['loc_code_not_existed'] = false;
      let $params = '/' + this.whs_id + "/complete-add-ccn?limit=10&keyword=" + this._func.trim($e.target.value);

      this._CCNServices.getLocationCCNL($params)
          .subscribe(
              data => {

                this.listDataAutocomplete[$nameField] = data.data;
                this.listDataAutocomplete['loading-' + $nameField] = false;

                if ($e.type == 'focusout' && $nameField == 'location-edit') {

                  this.ckExistLocation($e);
                }

              },
              err => {

                this.listDataAutocomplete['loading-' + $nameField] = false;
                this.parseError(err);
              },
              () => {
              }
          );
    } else {

      var $params ='/complete?com-loc=1&limit=10&loc-code=' + $e.target.value;
      this._CCNServices.autoSearchComplete(this.whs_id, $params)
          .subscribe(
              data => {

                this.listDataAutocomplete[$nameField] = data.data;
                this.listDataAutocomplete['loading-' + $nameField] = false;
              },
              err => {

                this.listDataAutocomplete['loading-' + $nameField] = false;
                this.parseError(err);
              },
              () => {
              }
          );
    }

  }

  private autoCompleteSku($nameField, $e) {


    if ($e.type == 'keyup') {

      jQuery("#size").val('');
      jQuery("#color").val('');
      jQuery("#lot").val('');
    }
    this.listDataAutocomplete['loading-' + $nameField] = false;

    // do Search
    var param = `?sku=` +this._func.trim($e.target.value)+ `&limit=20`;

    this._CCNServices.getSKUs(this.whs_id, param)
        .subscribe(
            data => {

              this.listDataAutocomplete[$nameField] = data.data;
              this.listDataAutocomplete['loading-' + $nameField] = false;
            },
            err => {

              this.listDataAutocomplete['loading-' + $nameField] = false;
              this.parseError(err);
            },
            () => {
            }
        );
  }

  private ckExistLocation($e) {

    setTimeout(() => {

      if (this.listSelectedItem[0]['loc_code'].trim() == $e.target.value.trim()) {
        return true;
      }
      var flag = false;
      for( var i = 0 ; i < this.listDataAutocomplete['location-edit'].length; i++) {

        if (this.listDataAutocomplete['location-edit'][i]['loc_code'].trim() == $e.target.value.trim()) {

          flag = true;
          break;
        }
      }

      this.listNull['loc_code_not_existed'] = false;

      if (!flag) {
        this.listNull['loc_code_not_existed'] = true;
      }
    },200);

  }
  private selectedItem($nameField, $item, itemParent:any = ''){

    if (itemParent != '') {

      this.listNull['loc_code_not_existed'] = false;
      itemParent['loc_code'] = $item.loc_code;
      itemParent['flagDisSku'] = true;
    } else if ($nameField == 'sku_s') {

      jQuery("#size").val($item.size);
      jQuery("#color").val($item.color);
      jQuery("#lot").val($item.lot);
      jQuery("#" +  $nameField).val($item.sku);

    } else {

      this.listNull['loc_code_not_existed'] = false;
      jQuery("#" +  $nameField).val($item.loc_code);
    }
  }

  private assignDate($target) {

    if ($target.value != "") {

      this.listNull[$target.id + "_required"] = false;
    }

    var target = this;
    jQuery("body").off("click.myExpect").on("click.myExpect", function(e) {

      if(e.target.id !== "cycle_due_date")
      {
        var $target = jQuery("#cycle_due_date");

        target.listNull["cycle_due_date_required"] = false;
        if ($target.val() == "") {

          target.listNull["cycle_due_date_required"] = true;

        }
      }

    });
  }

  private messagesCompleteUpdate: any;
  private updateCCNotify() {

    this.messagesCompleteUpdate = false;
    this.messages = false;
    var target = this;

    jQuery('#loc_code, #reason, #sku').each(function (i, object) {

      target.filterForm(object);
    });

    var submit = true;
    jQuery.each( this.listNull, function( key, value ) {

      if (value) {

        submit = false;
        return false;
      }
    });

    if (!submit) return false;

    var $params = {};
    $params['loc_id'] = this.listSelectedItem[0]['loc_id'];
    $params['loc_code'] = jQuery("#loc_code").val();
    $params['cc_ntf_sts'] = this.listSelectedItem[0]['cc_ntf_sts'];
    $params['reason'] = jQuery("#reason").val();
    $params['remain_qty'] = this.listSelectedItem[0]['remain_qty'];
    $params['sku'] = this.listSelectedItem[0]['sku'];
    $params['size'] = this.listSelectedItem[0]['size'];
    $params['color'] = this.listSelectedItem[0]['color'];
    $params['lot'] = this.listSelectedItem[0]['lot'];
    $params['pack'] = this.listSelectedItem[0]['pack'];
    $params['item_id'] = this.listSelectedItem[0]['item_id'];
    $params['cus_id'] = this.listSelectedItem[0]['cus_id'];
    $params['uom_code'] = this.listSelectedItem[0]['uom_code'];
    $params['uom_name'] = this.listSelectedItem[0]['uom_name'];
    $params['des'] = this.listSelectedItem[0]['des'];

    $params  = JSON.stringify($params);

    this.showLoadingOverlay = true;
    this._CCNServices.updateCCNotify(this.whs_id, this.listSelectedItem[0]['ccn_id'], $params)
        .subscribe(
            data => {

              jQuery("#popup").modal('hide');
              this.getListCCN(this.currentPage);
              this.messages = this._func.Messages('success', this._func.msg('VR109'));
              this.showLoadingOverlay = false;
            },
            err => {
              this.showLoadingOverlay = false;
              this.messagesCompleteUpdate = {'status' : 'danger', 'txt' : this._func.parseErrorMessageFromServer(err)};
            },
            () => {

            }
        );
  }

  private addCC() {

    this.messagesCompleteUpdate = false;
    this.messages = false;
    var target = this;

    jQuery('#cycle_assign_to, #cycle_due_date, #cycle_name').each(function (i, object) {

      target.filterForm(object);
    });

    var submit = true;
    jQuery.each( this.listNull, function( key, value ) {

      if (value) {

        submit = false;
        return false;
      }
    });

    if (!submit) return false;

    var $params = {};
    $params['whs_id'] = this.whs_id;
    $params['cycle_name'] = jQuery("#cycle_name").val();
    $params['cycle_due_date'] = jQuery("#cycle_due_date").val();
    $params['cycle_assign_to'] = jQuery("#cycle_assign_to").val();
    $params['cycle_type'] = 'CCN';
    $params['cycle_des'] = '';
    $params['cycle_has_color_size'] = true;
    $params['cycle_count_by'] = jQuery("input[name=cycle_count_by_OUM]:checked").val();
    $params['cycle_method'] = 'paper';

    $params['cycle_detail'] = '';
    $params['ccn_ids'] = '';
    for (var i = 0; i < this.listSelectedItem.length; i++) {
      if ((this.listSelectedItem.length - 1) == i) {
        $params['cycle_detail'] += this.listSelectedItem[i]['loc_code'];
        $params['ccn_ids'] += this.listSelectedItem[i]['ccn_id'];
        continue;
      }
      $params['ccn_ids'] += this.listSelectedItem[i]['ccn_id']+ ",";
      $params['cycle_detail'] += this.listSelectedItem[i]['loc_code'] + ",";
    }

    $params  = JSON.stringify($params);
    this.showLoadingOverlay = true;
    this._CCNServices.addCC(this.whs_id, $params)
        .subscribe(
            data => {

              jQuery("#popupAddCC").modal('hide');
              this.messagesCompleteUpdate = this._func.Messages('success', this._func.msg('CC001'));
              this.showLoadingOverlay = false;
              this.getListCCN(this.currentPage);
            },
            err => {
              this.showLoadingOverlay = false;
              this.messagesCompleteUpdate = {'status' : 'danger', 'txt' : this._func.parseErrorMessageFromServer(err)};
            },
            () => {
              this.ccAddCC();
            }
        );


  }

  private cycleCountUserList = [];
  private getCycleCountUsers() {

    this._CCNServices.getCycleCountUsers()
        .subscribe(
            data => {

              this.cycleCountUserList = data.data;
            },
            err => {
              this.parseError(err);
            },
            () => {
            }
        );
  }

  private filterList(pageNumber) {
    this.getListCCN(pageNumber);
  }

  private getPage(pageNumber) {
    let arr = new Array(pageNumber);
    return arr;
  }

  // Set params for pagination
  private initPagination(data){
    var meta = data.meta;
    this.Pagination=meta['pagination'];
    this.Pagination['numLinks']=3;
    this.Pagination['tmpPageCount']=this._func.Pagination(this.Pagination);

  }

  // Event when user filter form
  private onPageSizeChanged ($event) {
    this.perPage = $event.target.value;
    if((this.Pagination['current_page'] - 1) * this.perPage >= this.Pagination['total']) {
      this.currentPage = 1;
    }
    else {
      this.currentPage = this.Pagination['current_page'];
    }
    this.getListCCN(this.currentPage);
  }

  // Search
  private search (page = 1) {

    this.searchParams = '';
    let params_arr = jQuery("#form-filter").serializeArray() ;
    for (let i in params_arr) {
      if (params_arr[i].value == "") continue;
      this.searchParams +="&" +  params_arr[i].name.trim() + "=" + encodeURIComponent(params_arr[i].value.trim());
    }
    this.getListCCN(page);

  }

  private add() {

    this._router.parent.navigateByUrl('/cycle-count-notify/add-location');
  }

  // Reset form
  private reset() {

    jQuery("#form-filter input, #form-filter select").each(function( index ) {

      jQuery(this).val("");
    });

    jQuery("select[name=cc_ntf_sts]").val('NW');
    this.searchParams = '';
    this.getListCCN(1);

  }

  // Show error when server die or else
  private parseError (err) {
    this.showMessage('danger', this._func.parseErrorMessageFromServer(err));
  }

  private edit() {
    this.listSelectedItem  = [];
    this.getSelectedRow = 'edit';
  }

  private createCC() {
    this.listSelectedItem  = [];
    this.getSelectedRow = 'createCC';
  }

  private ccCCN() {
    this.listSelectedItem  = [];
    this.getSelectedRow = 'ccCCN';
  }


  afterGetSelectedRow($event) {

    this.listSelectedItem = $event.data;
    this.messages = false;
    this.messagesCompleteUpdate = false;
    this.listNull = {};
    switch ($event.action) {
      case 'edit':

        if (this.listSelectedItem.length < 1) {
          this.showMessage('danger', this._func.msg('WMS001'));
          break;
        }

        if (this.listSelectedItem.length > 1) {
          this.showMessage('danger', this._func.msg('WMS001'));
          break;
        }


        if (this.listSelectedItem[0]['cc_ntf_sts_name'].toLowerCase() != 'new') {
          this.showMessage('danger', this._func.msg('CCN001'));
          break;
        }
        this.getListReason();
        jQuery("#popup").modal('show');
        break;
      case 'createCC':
        if (this.listSelectedItem.length < 1) {
          this.showMessage('danger', this._func.msg('CCN002'));
          break;
        }

          var flag = true;
          for (var i = 0; i < this.listSelectedItem.length; i++) {
            if (this.listSelectedItem[i]['cc_ntf_sts'] != 'NW') {

              flag = false;
              break;
            }
          }
          if (!flag) {
            this.showMessage('danger', this._func.msg('CCN003'));
            break;
          }
        jQuery("#popupAddCC").modal('show');
        break;
      case 'ccCCN':

        if (this.listSelectedItem.length < 1) {
          this.showMessage('danger', this._func.msg('WMS003'));
          break;
        }

        if (this.listSelectedItem.length > 1) {
          this.showMessage('danger', this._func.msg('WMS003'));
          break;
        }


        if (this.listSelectedItem[0]['cc_ntf_sts_name'].toLowerCase() != 'new') {
          this.showMessage('danger', this._func.msg('CCN001'));
          break;
        }

        let n = this;
        let warningPopup = new WarningBoxPopup();
        warningPopup.text = this._func.msg('CCN004');
        this._boxPopupService.showWarningPopup(warningPopup)
            .then(function (dm) {
              if (!dm) {
                return;
              } else {
                n.canCelCCAPI(n.listSelectedItem);
              }
            });

        break;

    }

    this.getSelectedRow = false;
  }

  private canCelCCAPI(listSelectedItem) {

    var $params = {};
    $params['loc_id'] = this.listSelectedItem[0]['loc_id'];
    $params['loc_code'] = this.listSelectedItem[0]['loc_code'];
    $params['cc_ntf_sts'] = 'CE';
    $params['reason'] = this.listSelectedItem[0]['reason'];
    $params['remain_qty'] = this.listSelectedItem[0]['remain_qty'];
    $params['sku'] = this.listSelectedItem[0]['sku'];
    $params['size'] = this.listSelectedItem[0]['size'];
    $params['color'] = this.listSelectedItem[0]['color'];
    $params['lot'] = this.listSelectedItem[0]['lot'];
    $params['pack'] = this.listSelectedItem[0]['pack'];
    $params['item_id'] = this.listSelectedItem[0]['item_id'];
    $params['cus_id'] = this.listSelectedItem[0]['cus_id'];
    $params['uom_code'] = this.listSelectedItem[0]['uom_code'];
    $params['uom_name'] = this.listSelectedItem[0]['uom_name'];
    $params['des'] = this.listSelectedItem[0]['des'];

    $params  = JSON.stringify($params);

    this.showLoadingOverlay = true;
    this._CCNServices.updateCCNotify(this.whs_id, this.listSelectedItem[0]['ccn_id'], $params)
        .subscribe(
            data => {

              this.getListCCN(this.currentPage);
              this.messages = this._func.Messages('success', this._func.msg('VR109'));
              this.showLoadingOverlay = false;
            },
            err => {
              this.showLoadingOverlay = false;
              this.messages = {'status' : 'danger', 'txt' : this._func.parseErrorMessageFromServer(err)};
            },
            () => {

            }
        );
  }

  private doSort(objSort) {
    this.objSort = objSort;
    this.getListCCN(this.Pagination.current_page);
  }

  refreshList() {
    this.refreshRowData = true;
    setTimeout(()=>{
      this.refreshRowData = false;
    }, 400);
  }


  private expandTable = false;
  private viewListFullScreen() {
    this.expandTable = true;
    setTimeout(() => {
      this.expandTable = false;
    }, 500);
  }


  // Filter form only show choose one
  public filterForm($target, flag = 'required', item : any = '') {

    try {

      switch (flag) {

        case 'required':

          this.listNull[$target.id + '_' + flag] = false;
          if ($target.value == "") {

            this.listNull[$target.id + '_' + flag] = true;

          }
          break;
        case 'required_greater0_lesspacksize':

          this.listNull['remain_qty_required'] = false;
          this.listNull['remain_qty_greater0'] = false;
          this.listNull['remain_qty_less_equal'] = false;
          if ($target.value == "") {

            this.listNull['remain_qty_required'] = true;

          } else if ($target.value < 1) {

            this.listNull['remain_qty_greater0'] = true;
          } else if (parseInt($target.value) > parseInt(item['pack'])) {

            this.listNull['remain_qty_less_equal'] = true;
          }


          break;
      }
    } catch (e) {

    }
  }

  private ccAddCC() {

    jQuery("#popupAddCC").modal('hide');
    jQuery('#cycle_assign_to, #cycle_due_date, #cycle_name').each(function (i, object) {

      object.value = '';
    });
    jQuery("#carton").prop("checked", true);
  }


  private autoCompleteSkuForEdit(row, $e, item) {


    this.flagExistSku = false;
    this.messages = false;
    item['showLoadingSku'] = true;

    // do Search
    var param = `?sku=` +this._func.trim($e.target.value)+ `&limit=20`;
    this._CCNServices.getSKUs(this.whs_id, param)
        .subscribe(
            data => {

              item['showLoadingSku'] = false;
              item['autoSku'] = data.data;
            },
            err => {

              item['showLoadingSku'] = false;
              this.parseError(err);
            },
            () => {
            }
        );

    this.listNull['remain_qty_required'] = false;
    this.listNull['remain_qty_greater0'] = false;
    this.listNull['remain_qty_less_equal'] = false;
    this.setDataFieldBySku(row, {}, item);
  }

  private setDataFieldBySku(row, dataItem = {}, $itemParent,  flagSetSku = false) {

    $itemParent['size'] = dataItem['size'];
    $itemParent['color'] = dataItem['color'];
    $itemParent['lot'] = dataItem['lot'];
    $itemParent['pack'] = dataItem['ctn_pack_size'];
    $itemParent['item_id'] = dataItem['item_id'];
    $itemParent['cus_id'] = dataItem['cus_id'];
    $itemParent['uom_code'] = dataItem['uom_code'];
    $itemParent['uom_name'] = dataItem['uom_name'];
    $itemParent['des'] = dataItem['des'];
    $itemParent['remain_qty'] = dataItem['remain'];

    if (flagSetSku) {

      $itemParent['sku'] = dataItem['sku'];
      jQuery("#sku").val(dataItem['sku']);
    }
  }

  private selectedItemSku($index, $item, $itemParent:any = ''){

    this.flagExistSku = true;
    this.listNull['sku_' + $index + '_not_existed'] =  false;
    this.listNull['sku_required'] =  false;
    this.listNull['remain_qty_required'] =  false;
    this.listNull['remain_qty_greater0'] =  false;
    this.listNull['remain_qty_less_equal'] =  false;
    this.setDataFieldBySku($index,$item, $itemParent, true);
  }

  private dataListReason = [];
  private getListReason() {

    this.messages = null;
    this.showLoadingOverlay = true;
    this._CCNServices.getListReason().debounceTime(400).distinctUntilChanged().subscribe(
        data => {

          this.dataListReason = data.data;
          this.showLoadingOverlay = false;
        },
        err => {
          this.messages = {'status': 'danger', 'txt': this._func.parseErrorMessageFromServer(err)};
          this.showLoadingOverlay = false;
        }
    )
  }

  private flagExistSku = true;
  private eventCkExistSku(row, fieldName = 'sku_') {

    setTimeout(() => {
      this.listNull[fieldName + row + '_not_existed'] = false;

      if (!this.flagExistSku) {
        this.listNull[fieldName + row + '_not_existed'] = true;
      }
    },200);

  }

  checkInputNumber(evt, flagInt = false)
  {
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)){
      if(flagInt) {
        evt.preventDefault();
      }
      else {
        if(charCode != 46) {
          evt.preventDefault();
        }
      }
    }
  }

}
