import {Component,OnInit} from "@angular/core";
import { Router, RouterLink, ROUTER_DIRECTIVES } from '@angular/router-deprecated';
import { FORM_DIRECTIVES,FormBuilder} from '@angular/common';
import {UserService, Functions} from '../../../common/core/load';
import {WMSBreadcrumb,WMSPagination,PaginationControlsCmp,ColorPickerDirective,WMSMessages,ColorPickerService,PaginationService} from '../../../common/directives/directives';
import {CustomerServices} from "../../../system-setup/customer/customer-service";
import {RFGUNPUTAWAYServices} from "./rf-putaway.service";
import {RFGunServices} from "../../rf-gun-service";
import {GoBackComponent} from "../../../common/component/goBack.component";
@Component({
  selector: 'rf-gun',
  providers: [CustomerServices,RFGUNPUTAWAYServices,PaginationService,ColorPickerService,UserService],
  directives: [FORM_DIRECTIVES,WMSPagination,PaginationControlsCmp,ColorPickerDirective,WMSMessages,GoBackComponent,WMSBreadcrumb],
  templateUrl: 'rf-putaway.component.html',
})
export class RFPUTAWAYComponent {

  private show_dashboard_nav=true;
  private page='GR-list-page';
  private GRList:Array<any>=[];
  private allowAccess;
  private perPage=10;
  private showLoadingOverlay;
  private messages;
  private Pagination;
  private rfGunPermission;
  private whs_id;
  private grDetail;
  private skuDetail;
  private LocPalletModel={};
  private SKUList:Array<any>=[];
  private showMenu:boolean=false;
  private suggestPalletLoc;

  constructor(private rfGunService:RFGunServices,
              private _router: Router,
              private _user:UserService,
              private _Func: Functions)
  {
    this.checkPermission();
  }

  anchorHref(elementHref)
  {
    this.page=elementHref;
    this.show_dashboard_nav=false;

    if(this.page=='GR-list-page')
    {
      this.getGRList();
    }

  }

  _showMenu()
  {
    this.showMenu = !this.showMenu;
  }

  checkPermission(){

    this.showLoadingOverlay = true;
    this._user.GetPermissionUser().subscribe(
      data => {
        this.showLoadingOverlay = false;
        this.rfGunPermission = this._user.RequestPermission(data, 'viewRFGUN');
        if(this.rfGunPermission) {
          this.allowAccess=true;
          this.whs_id = localStorage.getItem('whs_id');
          this.getGRList();
        }
        else{
          this.redirectDeny();
        }
      },
      err => {
        this.parseError(err);
        this.showLoadingOverlay = false;
      }
    );
  }

  redirectDeny(){
    this._router.parent.navigateByUrl('/deny');
  }

  goBackPage()
  {
    this.messages=false;
    let page=this.page;
    switch (page)
    {
      case 'GR-detail-page' :  this.page='GR-list-page'; break;
      case 'updatePutAway' :  this.page='GR-detail-page'; break;
      case 'GR-list-page' :  this.show_dashboard_nav=true; break;
    }

  }

  getGRList(page='')
  {
    this.showLoadingOverlay=true;
    this.rfGunService.RF_GUN_API_GET(this.whs_id,'')
      .subscribe(
        data => {
          this.showLoadingOverlay=false;
          this.GRList=data.data;
        },
        err => {
          this.parseError(err);
          this.showLoadingOverlay = false;
        },
        () => {
        }
      );
  }

  getSKUList(page='')
  {
    this.showLoadingOverlay=true;
    let params='/'+this.grDetail['gr_hdr_id'];
    this.rfGunService.RF_GUN_API_GET(this.whs_id,params)
      .subscribe(
        data => {
          this.SKUList=data.data;
          this.showLoadingOverlay=false;
        },
        err => {
          // this.flagLoading = false;
          // this.flagLoading2 = false;
          this.parseError(err);
          this.showLoadingOverlay = false;
        },
        () => {

          // this.flagLoading = false;
          // this.flagLoading2 = false;
        }
      );

  }

  rediect_updatePutAway(item)
  {
    this.messages=false;
    this.page='updatePutAway';
    this.skuDetail=item;
    this.suggestPalletLoc=null;
    this.getLocaPallet();
  }

  viewDetailGR(item)
  {
    this._router.parent.navigateByUrl('/rf-gun/putaway/sku-list/'+item['gr_hdr_id']);
  }

  /*
   * updateLocation
   * */

  updateLocation(data):void
  {
    let data_r={};
    data_r['codes']=(<any>Object).assign({},data.split("\n"));
    data_r=this.removeEmptyKey(data_r);
    this.showLoadingOverlay=true;
    this.rfGunService.RF_GUN_API_UPDATE(this.whs_id,JSON.stringify(data_r))
      .subscribe(
        data => {
          this.messages=this._Func.Messages('success',data['message']);
          this.showLoadingOverlay=false;
          this.suggestPalletLoc=null;
          // reget
          this.getLocaPallet();
        },
        err => {
          this.showLoadingOverlay=false;
          this.parseError(err);
        },
        () => {
        }
      );

  }

  removeEmptyKey(object)
  {
    let data_tmp=JSON.parse(JSON.stringify(object));
    Object.keys(object['codes']).map(
      key =>{
        if(object['codes'][key]==''){
          delete data_tmp['codes'][key];
        }
      }
    );
    return data_tmp
  }

  getLocaPallet()
  {
    this.showLoadingOverlay=true;
    let params="/suggest/"+this.skuDetail['gr_dtl_id'];
    this.rfGunService.RF_GUN_API_GET(this.whs_id,params)
      .subscribe(
        data => {
          this.showLoadingOverlay=false;
          this.LocPalletModel['dtl']=data['dtl'].join("\n");
          this.LocPalletModel['hdr']=data['hdr'];
          this.set_skuDetail(data['hdr'][0]);
        },
        err => {
          this.showLoadingOverlay=false;
          this.parseError(err);
        },
        () => {
        }
      );

  }

  set_skuDetail(data)
  {

    this.skuDetail['gr_hdr_num']=data['gr_hdr_num'];
    this.skuDetail['sku']=data['sku'];
    if(data['size'])
    {
      this.skuDetail['sku']+='-'+data['size'];
    }
    if(data['color'])
    {
      this.skuDetail['sku']+='-'+data['color'];
    }

    this.skuDetail['plt']=data['total']+'/'+data['actual'];

  }

  showSuggetPalletLoc()
  {
    this.suggestPalletLoc=this.LocPalletModel['dtl'];
  }

  // Show error when server die or else
  private parseError (err) {
    err = err.json();
    this.messages = this._Func.Messages('danger', err.errors.message);
  }

}
