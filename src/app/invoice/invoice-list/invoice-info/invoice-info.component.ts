
import {Component} from '@angular/core';

@Component({
    selector: 'invoice-info',
    templateUrl: 'invoice-info.component.html',
})

export class InvoiceInfo
{
    private invoiceInfo: any [];
    private invoiceType: string;
    public isVisible: boolean;

    /*
    ****************************************************************************
    */

    public showInvoiceInfo(invoiceType='', invoiceInfo: any)
    {
        this.invoiceType = invoiceType;
        this.invoiceInfo = invoiceInfo;
        this.isVisible = true;
    }

    /*
    ****************************************************************************
    */

    public hideInvoiceInfo()
    {
        this.isVisible = false;
    }

    /*
    ****************************************************************************
    */

}
