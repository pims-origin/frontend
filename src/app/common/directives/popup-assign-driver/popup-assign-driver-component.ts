import {Input, Output, ComponentMetadata, EventEmitter, Injectable, Component } from '@angular/core';
import {
    WMSPagination, PaginationControlsCmp, PaginationService, PaginatePipe
} from '../../../common/directives/directives';
import {Functions} from '../../../common/core/load';
import{PopupAssignDriverService} from './popup-assign-driver-service';
import { count } from 'rxjs/operator/count';

declare const jQuery: any;

@Injectable()
@Component({
    templateUrl: 'popup-assign-driver.html',
    styleUrls: ['./style.css'],
    selector: 'popup-assgin-driver',
    directives: [WMSPagination, PaginationControlsCmp],
    pipes: [PaginatePipe],
    providers:[PopupAssignDriverService],
})

export class PopupAssignDriver {
    @Input('listSelectedItem') set listSelectedItem(value) {
        if (value.length > 0) {
            this.listItemOrder = value;
            this.infoSearchDriver = {};
            this.assginDriver();
        }
    }

    @Output() messages = new EventEmitter();

    private listItemOrder = [];
    public infoSearchDriver = {};
    private listDataUser = [];
    private listDataUserSearch = [];
    private showLoadingOverlay = false;
    selectedRow = -1;
    private driver_id: Number;

    constructor(private _func: Functions,
            private popupAssignService: PopupAssignDriverService
        ) {}

        private assginDriver() {
            this.popupAssignService.listUserRoleDriver().subscribe(
            data => {
                if (data.data && data.data.length) {
                    this.showLoadingOverlay = true;
                    this.listDataUser =  data.data;
                    this.listDataUserSearch = data.data;
                    jQuery('#assign-driver').modal('show');
                }
            },
            err => {
                jQuery('#assign-driver').modal('hide');
                this.showMessage('danger', this._func.parseErrorMessageFromServer(err));
            },
            () => {this.showLoadingOverlay = false;}
        )
    }

    private setClickedRow(index, item) {
        this.selectedRow = index;
        this.driver_id = item.user_id;
    }

    private filterDriverUser() {
        let data = this.listDataUserSearch;
        if (this.infoSearchDriver['first_name']) {
            data = this.listDataUserSearch.filter(item => 
                item.first_name.toLowerCase().includes(this.infoSearchDriver['first_name'].toLowerCase()));
        }
        if (this.infoSearchDriver['last_name']) {
            data = data.filter(item =>
                item.last_name.toLowerCase().includes(this.infoSearchDriver['last_name'].toLowerCase()));
        }
        if (this.infoSearchDriver['email']) {
            data = data.filter(item =>
                item.email.toLowerCase().includes(this.infoSearchDriver['email'].toLowerCase()));
        }
        return this.listDataUser =  data;
    }

    private getArrayOdrId() {
        let arrayOdr_id = [];
        this.listItemOrder.forEach(item => {
            arrayOdr_id.push(item.odr_id);
        })
        return arrayOdr_id;
    }

    private saveAssigmDriver() {
        const objectDriver = {
            driver_id: this.driver_id,
            odr_ids: this.getArrayOdrId()
        };
        this.popupAssignService.saveDriverUser(JSON.stringify(objectDriver)).subscribe(
            data => {
                this.showLoadingOverlay = true;
                this.showMessage('success', this._func.msg('OD003'));
            },
            err => {
                let error = JSON.parse(err._body);
                this.showMessage('danger', error.errors['message']);
            },
            () => {
                this.showLoadingOverlay = false;
            }
        )

    }

    private resetFilterDriverUser() {
        this.infoSearchDriver['first_name'] = '';
        this.infoSearchDriver['last_name'] = '';
        this.infoSearchDriver['email'] = '';
        this.listDataUser = this.listDataUserSearch;
    }

    private showMessage(msgStatus, msg) {
        this.messages.emit({
            'status': msgStatus,
            'txt': msg
        })
        jQuery(window).scrollTop(0);
    }
}
