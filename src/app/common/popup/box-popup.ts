import {JwtHelper} from "angular2-jwt/angular2-jwt";
declare var swal: any;
export class BoxPopup<T> {
    type:string;
    text:string;
    showCancelButton:boolean;
    confirmButtonText:string;
    cancelButtonText:string;

    constructor(options: {
        type?: string,
        text?:string,
        showCancelButton?:boolean,
        confirmButtonText?:string,
        cancelButtonText?:string
    } = {}) {
        this.type = options.type;
        this.text = options.text || '';
        this.showCancelButton = !!options.showCancelButton;
        this.confirmButtonText = options.confirmButtonText || '';
        this.cancelButtonText = options.cancelButtonText || '';
    }
}
