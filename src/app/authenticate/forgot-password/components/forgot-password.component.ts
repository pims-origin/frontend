import {Component} from "@angular/core";
import { Router, RouterLink, ROUTER_DIRECTIVES } from '@angular/router-deprecated';
import {Http, Response, JSONP_PROVIDERS, Jsonp} from '@angular/http';
import { CORE_DIRECTIVES, FORM_DIRECTIVES,ControlGroup, FormBuilder, Validators } from '@angular/common';
import {API_Config, Functions} from '../../../common/core/load';
import { ValidationService } from '../../../common/core/validator';
@Component({
    selector: 'forgot-password',
    directives: [CORE_DIRECTIVES, FORM_DIRECTIVES],
    templateUrl: '../views/forgot-password.component.html',
    providers:[ValidationService]
})
export class ForgotPasswordComponent{

    public ForgotPassWordForm:ControlGroup;
    public AuthHeader = this._Func.AuthHeaderNoTK();
    public messages;
    public loading;
    private submitForm:boolean=false;
    constructor(
        public router: Router,
        private _Func: Functions,
        private _API: API_Config,
        private _Valid: ValidationService,
        fb: FormBuilder,
        private http: Http)
    {
        this.ForgotPassWordForm =fb.group({
            emailForm: [null, Validators.compose([Validators.required, this._Valid.emailValidator])],
        });
    }

    notice(){
        this.router.parent.navigateByUrl('/forgot-password-notice');
    }

    SendRequest(data: any): void {
        this.submitForm=true;
        if(this.ForgotPassWordForm.valid){
            this.loading=true;
            let param = "email=" + data['emailForm'] + "&reset_password_url="+this._API.API_URL_RESET_PASS;
            this.http.post(this._API.API_MASTER_URL + "/forgot-password", param, { headers: this.AuthHeader})
                .subscribe(
                    data => {
                        this.notice();
                    },
                    (err: Response)=> {
                        this.loading=false;
                        if(err.status==400)
                        {
                            // error email
                            this.messages = this._Func.Messages('danger', this._Func.msg('VR101'));
                        }
                        else{
                            if (err.status == 200 || err.status==504)
                            {
                                this.messages = this._Func.Messages('danger', this._Func.msg('VR100'));
                            }
                            else{
                                this.messages = this._Func.Messages('danger', this._Func.msg('VR102'));
                            }
                        }
                    }
                );
        }

    }
}
