import {Component,Input, Output, EventEmitter} from '@angular/core';
import { FORM_DIRECTIVES,ControlGroup,FormBuilder, Control,Validators} from '@angular/common';
import {WMSBreadcrumb,WMSMessages,WMSPagination} from '../../common/directives/directives';
import {Router, RouteData, RouteParams } from '@angular/router-deprecated';
import {BoxPopupService} from "../../common/popup/box-popup.service";
import {GoBackComponent} from "../../common/component/goBack.component";
import {WarningBoxPopup} from "../../common/popup/warning-box-popup";
import { ValidationService } from '../../common/core/validator';
import {UserService, Functions} from '../../common/core/load';
import {API_Config} from "../../common/core/API_Config";
import {Services} from "./services";
declare var saveAs: any;
declare var jQuery: any;
@Component ({
    selector: 'delivery-service-form',
    directives:[WMSBreadcrumb,WMSPagination,GoBackComponent,FORM_DIRECTIVES,WMSMessages],
    providers: [BoxPopupService,ValidationService,Services,UserService],
    templateUrl: 'delivery-form.html'
})

export class DeliveryServiceForm {

    Form: ControlGroup;
    private submitForm=false;
    private TEMPLATE='default';

    @Output() messages = new EventEmitter();
    @Output() data = new EventEmitter();
    @Output() showLoadingOverlay = new EventEmitter();

    @Input('template') set template(value:string){
        this.TEMPLATE=value;
    }

    @Input('submitFormCustom') set submitFormCustom(value:string){

       if(value){
           this.Save();
       }

    }

    constructor(private _router: Router,
                private _boxPopupService:BoxPopupService,
                _RTAction: RouteData,
                private ser:Services,
                private _user:UserService,
                private fb: FormBuilder,
                private _Valid: ValidationService,
                private _Func: Functions,
                private _api_Config: API_Config,
                private params: RouteParams) {

        this.checkPermission();

    }

    private canCreate;
    private showForm=true;
    private checkPermission(){
        this.showLoadingOverlay.emit(true);
        this._user.GetPermissionUser().subscribe(
            data => {
                
                this.canCreate = this._user.RequestPermission(data, 'createDeliveryService');
                if(!this.canCreate){
                    //this._router.parent.navigateByUrl('/deny');
                }
                this.showLoadingOverlay.emit(false);
                this.formBuilder();

            },
            err => {
                this.parseError(err);
                this.showLoadingOverlay.emit(false);
            }
        );
    }

    private formBuilder(data={}){

        this.Form =this.fb.group({
            id: [data['id']],
            ds_key: [data['ds_key'],Validators.compose([Validators.required, this._Valid.validateSpace])],
            ds_value: [data['ds_value'],Validators.compose([Validators.required, this._Valid.validateSpace])]
        });

    }


    private Save(){

        this.submitForm=true;
        if(this.Form.valid){
            this.showLoadingOverlay.emit(true);
            this.ser.createDeliveryService(JSON.stringify(this.Form.value))
                .subscribe(
                    data => {
                        this.messages.emit(this._Func.Messages('success', this._Func.msg('BS002')));
                        this.data.emit(true);
                        this.showForm=false;
                        setTimeout(()=>{
                            this.showForm=true;
                            this.formBuilder();
                            this.submitForm=false;
                            this.showLoadingOverlay.emit(false);
                        })
                    },
                    err => {
                        this.showLoadingOverlay.emit(false);
                        this.parseError(err);
                    },
                    () => {}
                );

        }

    }

    // Show error when server die or else
    private parseError(err) {
        let mes = {'status': 'danger', 'txt': this._Func.parseErrorMessageFromServer(err)};
        this.messages.emit(mes);
    }



}