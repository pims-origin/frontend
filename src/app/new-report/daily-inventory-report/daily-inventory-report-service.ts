import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {API_Config, Functions} from '../../common/core/load';

@Injectable()
export class DailyInventoryService {
    public AuthHeader = this._Func.AuthHeader();
    public AuthHeaderPost = this._Func.AuthHeaderPost();

    constructor(private _Func:Functions, private _API:API_Config, private http:Http) {
    }

    getCustomersByWH(params) {
        return this.http.get(`${this._API.API_INVENTORY_NEW_REPORTS}/customers${params}&sort[cus_name]=asc`, {headers: this.AuthHeader})
            .map(res => res.json());
    }

}