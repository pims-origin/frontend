import {Component, DynamicComponentLoader, ElementRef, Injector } from '@angular/core';
import { Router, RouterLink, ROUTER_DIRECTIVES, RouteParams } from '@angular/router-deprecated';
import 'ag-grid-enterprise/main';
import 'rxjs/add/operator/map';
import {Functions} from "../../common/core/functions";
import {GoodsReceiptServices} from "../goods.receipt.service";
import {JwtHelper} from "angular2-jwt/angular2-jwt";
import {API_Config} from "../../common/core/API_Config";
import {WMSMessages, WMSBreadcrumb} from '../../common/directives/directives';
import { Title }     from '@angular/platform-browser';
declare var jQuery: any;
declare var saveAs: any;

@Component({
    selector: 'damaged-carton',
    directives: [ROUTER_DIRECTIVES, WMSMessages, WMSBreadcrumb],
    templateUrl: 'damaged-carton.component.html',
    providers: [GoodsReceiptServices,Title],
})
export class DamagedCartonComponent{

    private gr_id: any;
    private dataDamageCarton = [];
    private dataGrDetail = [];
    private messages:any;
    private tmpItem = [[]];
    private dataDamageType = [];
    private dataCartonNext = [];
    private ctn_id_next = 0;
    private listNull = {};
    private urlAPIDamageCarton  = this._api.API_Damage_Carton;
    private flagAddMoreItem = true;
    private timeoutHideMessage;
    private tmpDelete = [];
    private flagCallNextCtn = [];
    private showLoadingOverlay = false;
    // Construct
    constructor (
        private _router:Router,
        private _goodsReceiptServices: GoodsReceiptServices,
        params: RouteParams,
        private titleService: Title,
        private _Func: Functions,
        private _api: API_Config,
        public jwtHelper: JwtHelper
    ) {

        this.gr_id = params.get("id");
        this.getDamageCarton();
        this.getDamageType();

    }

    // Save
    private save (flag = '') {

        try {
            var tar_get = this;
            this.messages = false;

            var params = {};
            params['damage_cartons'] = [];
            var params_print = [];

            jQuery.each(jQuery("select[id^=dmg_id_]"), function( key, object ) {

                // Check form
                tar_get.filterForm(object);

                if (object.value != "") {

                    var tmp_print = {};
                    var tmp_value = {};
                    var tmp = object.id.split("_");

                    tmp_value['ctn_id'] = tar_get.tmpItem[tmp[2]][tmp[3]]['ctn_id'];
                    tmp_value['dmg_id'] = object.value.split("_")[0];
                    tmp_value['dmg_note'] = jQuery("#dmg_note_" + tmp[2] + "_" + tmp[3]).val();
                    params['damage_cartons'].push(tmp_value);

                    tmp_print['ctn_num'] = tar_get.tmpItem[tmp[2]][tmp[3]]['ctn_num'];
                    tmp_print['dmg_type'] = object.value.split("_")[1];
                    tmp_print['dmg_note'] =  tmp_value['dmg_note'];
                    params_print.push(tmp_print);
                }

            });

            // Check form
            var submit = true;
            jQuery.each( this.listNull, function( key, value ) {

                if (value) {

                    submit = false;
                    return false;
                }
            });

            if (!submit) {

                return false;
            }

            if (params['damage_cartons'].length < 1) {

                this.messages = this._Func.Messages('danger', this._Func.msg('CT001'));
                return false;
            }

            this.showLoadingOverlay = true;
            this._goodsReceiptServices.saveDamageCarton(JSON.stringify(params))
                .subscribe(
                    data => {
                        this.showLoadingOverlay = false;
                        if (flag != 'spr') {

                            this.messages = this._Func.Messages('success', this._Func.msg('VR107'));
                        }

                    },
                    err => {
                        this.showLoadingOverlay = false;
                        this.parseError(err);
                    },
                    () => {

                        //Print data
                        if (flag == 'spr') {

                            tar_get.printDamageCarton(params_print)

                        } else {

                            setTimeout(function () {

                                tar_get._router.parent.navigateByUrl('/goods-receipt/' + tar_get.gr_id + '/pallet-assignment');
                            }, 1100);
                        }

                    }
                );

        } catch (err) {

            this.messages = this._Func.Messages('danger', this._Func.msg('VR100'));
        }

    }

    // Filter form only show choose one
    private filterForm ($target, flag = 'required') {

        try {

            switch (flag) {
                case 'required':

                    if ($target.value == "" && flag == 'required') {

                        this.listNull[$target.id + "_required"] = true;

                    } else {

                        this.listNull[$target.id + "_required"] = false;
                    }
                    break;
                default:
                    break;

            }

            return false;

        } catch (err) {

            return false;
        }


    }

    // Private damage carton
    private printDamageCarton($params) {

        this.messages = false;

        try {

            var tar_get = this;
            var xhrl = new XMLHttpRequest();

            xhrl.open("POST", this.urlAPIDamageCarton + "/print", $params);
            xhrl.setRequestHeader("Authorization", 'Bearer ' +this._Func.getToken());
            xhrl.responseType = 'blob';

            xhrl.onreadystatechange = function () {

                // If we get an HTTP status OK (200), save the file using fileSaver
                if (xhrl.readyState === 4) {

                    if (xhrl.status === 200) {

                        tar_get.messages = tar_get._Func.Messages('success', tar_get._Func.msg('VR107'));
                        var blob = new Blob([this.response], {type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'});
                        saveAs.saveAs(blob, tar_get.dataDamageCarton[0]['gr_hdr_num'] + '_Damaged CTN.pdf');

                        setTimeout(function () {

                             tar_get._router.parent.navigateByUrl('/goods-receipt/' + tar_get.gr_id + '/pallet-assignment');
                        }, 1100);

                    } else {

                        tar_get.messages = tar_get._Func.Messages('danger', xhrl.statusText);

                    }

                }
            };

            xhrl.send(JSON.stringify($params));

        } catch (err) {

            this.messages = this._Func.Messages('danger', this._Func.msg('VR100'));
        }


    }

    // Get damage type
    private getDamageType () {

        this.messages = false;

        try {

            var $params = "?limit=10000&sort[dmg_name]=asc";
            this._goodsReceiptServices.getDamageType($params)
                .subscribe(
                    data => {

                        this.dataDamageType = data.data;
                    },
                    err => {

                        this.parseError(err);
                    },
                    () => {
                    }
                );

        } catch (err) {

            this.messages = this._Func.Messages('danger', this._Func.msg('VR100'));
        }
    }

    // Get ctn
    private getCartonNext (index, flag = '') {

        this.messages = false;
        var dtl_gr_dtl_id = this.dataGrDetail[index]['dtl_gr_dtl_id'];

        var ctn_id_next = jQuery("#ctn_next_" + index).val();

        if (ctn_id_next == undefined || ctn_id_next == '') {

            ctn_id_next = 0;
        }

        if (!this.flagCallNextCtn[index]) {

            this.assignCtnDeleted(index);
            this.flagAddMoreItem = true;
            return false;
        }

        try {

            var target = this;
            var $params = "/nextcarton/" + ctn_id_next + "/gr-dtl-id/" + dtl_gr_dtl_id;

            this._goodsReceiptServices.getCartonNext($params)
                .subscribe(
                    data => {

                        this.dataCartonNext = data.data;
                        target.tmpItem[index].push(target.dataCartonNext);
                        jQuery("#ctn_next_" + index).val(target.dataCartonNext['ctn_id']);

                    },
                    err => {

                        this.assignCtnDeleted(index, err);
                        target.flagAddMoreItem = true;
                    },
                    () => {

                        target.flagAddMoreItem = true;

                        if (flag == 'first') {

                            var tmp = ++ index ;

                            if (tmp < target.dataGrDetail.length) {

                                target.getCartonNext (tmp, 'first');
                            }
                        }

                    }
                );

        } catch (err) {

            this.messages = this._Func.Messages('danger', this._Func.msg('VR100'));
        }

    }

    // Get carton when call API don't have data
    private assignCtnDeleted(index, err = '') {

        this.flagCallNextCtn[index] = false;

        if (this.tmpDelete[index].length < 1 ) {

            if (err == '') {

                this.messages = {'status' : 'danger', 'txt' : 'The carton is not existed!'};
            } else {

                this.parseError(err);
            }

            return false;
        }


        var tmpIndex = this.tmpDelete[index].length - 1;
        this.tmpItem[index].push(this.tmpDelete[index][tmpIndex]);

        this.tmpDelete[index].splice(tmpIndex, 1);


    }

    // get damage carton
    private getDamageCarton () {

        try {

            var $params = "/" + this.gr_id;
            this._goodsReceiptServices.getDamageCarton($params)
                .subscribe(
                    data => {

                        this.dataDamageCarton = data.data;
                        this.dataGrDetail = this.dataDamageCarton;
                        this.addItemFirst(this.dataGrDetail.length);

                    },
                    err => {

                        this.parseError(err);
                    },
                    () => {
                    }
                );

        } catch (err) {

            this.messages = this._Func.Messages('danger', this._Func.msg('VR100'));
        }

    }

    // event check all
    private ckAll($event) {

        if (jQuery($event.target).prop('checked')) {

            jQuery($event.target).parents("table").find("tbody :checkbox").prop("checked", true);
        } else {

            jQuery($event.target).parents("table").find("tbody :checkbox").prop("checked", false);
        }

    }

    // Event check item
    private eventCheck($event) {

        let table = jQuery($event.target).parents("table");
        let checkboxAll = table.find('thead :checkbox');
        table.find('tbody :checkbox:not(:checked)').length ? checkboxAll.prop('checked', false) : checkboxAll.prop('checked', true);

    }

    // delete item
    private deleteItem ($event) {

        this.messages = false;
        let target = this;
        let ojCkbox =  jQuery("#wrap-pallet-assign").find(":checkbox[id^=item_ck]:checked");

        if (ojCkbox.length < 1 ) {

            this.messages = this._Func.Messages('danger', this._Func.msg('VR118'));
            return false;
        }

        target.listNull = {};

        for (var i = ojCkbox.length - 1; i >= 0; --i) {

            var tmp = ojCkbox[i].id.split("_");
            this.tmpDelete[tmp[2]].push(target.tmpItem[tmp[2]][tmp[3]]);
            target.tmpItem[tmp[2]].splice(tmp[3], 1);

            if (target.tmpItem[tmp[2]].length == 0) {

                target.getCartonNext(tmp[2]);
            }

        }

        jQuery("#wrap-pallet-assign").find(":checkbox").prop('checked', false);

    }

    private addItemFirst($length) {

        for( var i = 0; i < $length; i++) {

            this.tmpItem[i] = [];
            this.tmpDelete[i] = [];
            this.flagCallNextCtn[i] = true;
        }

        this.getCartonNext (0, 'first');

    }

    // Add more item
    private eAddMoreItem($event) {

        this.flagAddMoreItem = false;
        var $parentIndex = $event.target.id.replace("event_add_", "");

        this.getCartonNext($parentIndex);

    }

    // Show error when server die or else
    private parseError (err) {

        this.messages = {'status' : 'danger', 'txt' : this._Func.parseErrorMessageFromServer(err)};
    }

}

