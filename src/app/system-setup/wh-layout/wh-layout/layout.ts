import { Component,Directive,Output, ElementRef, EventEmitter , HostListener, Input } from '@angular/core';
import { WarehouseLayoutServices } from '../wh-layout-service';
import { Functions } from "../../../common/core/functions";
import { TOOLTIP_DIRECTIVES } from "ng2-bootstrap/ng2-bootstrap";

declare const jQuery: any;

@Component ({
    selector: '[warehouse-layout-directive]',
    directives:[TOOLTIP_DIRECTIVES],
    providers: [WarehouseLayoutServices],
    templateUrl: 'layout.html'
})

export class WarehouseLayoutDirective {
    private warehouseData: any[] = [];
    private aisleID;
    private locationDetail;
    private max_lpn;
    private locationName;
    private showLoadingOverlay: any;

    @Input('data') set data(value) {
        if (value) {
            value.forEach(aisleData => {
                if (aisleData.aisle) {
                    aisleData.aisle.forEach(level => {
                        if (level) {
                            const bins = Object.keys(level).map(key => level[key])
                            // console.log(bins);
                            level.data = bins;
                        }
                    })
                }
            });
        }
        this.warehouseData = value;
    }

    @Output() messages=  new EventEmitter();

    constructor(
        private _func: Functions,
        private _warehouseLayoutServices: WarehouseLayoutServices){}

    private showDetail(locID, locName) {
        this.locationName = locName;
        this.showLoadingOverlay = true;
        this._warehouseLayoutServices.getLocationDetail(locID).subscribe(
            data => {
                this.locationDetail = data.data.items;
                this.max_lpn = data.data.max_lpn || 0;
                jQuery('#location-detail').modal('show');
                this.showLoadingOverlay = false;
            },
            err => {
                this.showMessage('danger', this._func.parseErrorMessageFromServer(err));
                this.showLoadingOverlay = false;
            }
        );
    }

    private showMessage(msgStatus, msg) {
        this.messages.emit({
            'status': msgStatus,
            'txt': msg
        });
        jQuery(window).scrollTop(0);
    }

    private getPositionTooltip(firstBlock, row, data, index, length) {
        let pos = 'top';
        // case first block
        if (firstBlock && row <= 1) {
            pos = 'bottom';   
        }
        // case long tooltip
        if ((data && data['code']) || length === 1) {
            if (index <= 1) {
                pos = 'right';
            } else if (index >= 8 && index >= length - 3) {
                pos = 'left';
            }
        }
        return pos;
    }

}