import {Component,Input} from '@angular/core';
import { Router } from '@angular/router-deprecated';
import { FORM_DIRECTIVES,ControlGroup,FormBuilder, Control} from '@angular/common';
import {API_Config,UserService, Functions} from '../../../common/core/load';
import { Http } from '@angular/http';
import {WMSBreadcrumb, AdvanceTable, WMSPagination, WMSMultiSelectComponent, ReportFileExporter, WMSMessages} from '../../../common/directives/directives';
import {PickingListServices} from '../picking-list/picking-list-service';
declare var jQuery: any;
declare var saveAs: any;
@Component ({
  selector: 'picking-list',
  providers: [FormBuilder, PickingListServices],
  directives: [FORM_DIRECTIVES, WMSBreadcrumb, AdvanceTable, WMSMessages, WMSPagination, WMSMultiSelectComponent, ReportFileExporter],
  templateUrl: 'picking-list.component.html',
})

export class PickingListComponent {

  /** list permission need to check **/
  private hasUpdateOrderPickingPermission;
  private hasPrintWavePickPermission;
  private viewWavePick;
  /* end list permission */

  private messages;
  private showLoadingOverlay = false;

  private tableID = 'picking-list';
  private headerURL = this._API.API_User_Metas + '/pic';
  private headerDef = [{id: 'ver_table', value: 18},
                    {id: 'ck', name: '', width: 25},
                    {id: 'wv_sts', name: 'Status', width: 70},
                    {id: 'wv_num', name: 'Wave Pick Number', width: 150},
                    {id: 'odr_num', name: 'Order Number', width: 150},
                    {id: 'cus_odr_num', name: 'Customer Order Number', width: 150},
                    {id: 'cus_po', name: 'PO', width: 100},
                    {id: 'num_sku', name: '# of SKUs', width: 40,'unsortable': true},
                    {id: 'num_odr', name: '# of Orders', width: 40,'unsortable': true},
                     {id: 'picker_name', name: 'Picker', width: 90,'unsortable': true},
                    {id: 'user', name: 'User', width: 130, 'unsortable': true},
                    {id: 'created_at', name: 'Created Date', width: 90}];
  private pinCols = 2;
  private rowData: any[] = [];
  private getSelectedRow;
  private objSort;
  private searchParams;
  private refreshRowData = false;

  private Pagination;
  private totalCount = 0;
  private tmpPageCount;
  private pageCount=0;
  private ItemOnPage=0;
  private currentPage = 1;
  private perPage = 20;
  private numLinks = 3;

  private wavepickStatus = [];
  private whsID = localStorage.getItem('whs_id');
  public refreshMultiSelect = false;
  private fileName = 'Wavepicks.csv';
  private exportAPIUrl: string = '';

  constructor(
  	private _http: Http,
    private _func: Functions,
    private _API: API_Config,
    private fb: FormBuilder,
    private _user: UserService,
    private _pickingListService: PickingListServices,
    private _router:Router){

    this.checkPermission();
  }

  private showMessage(msgStatus, msg) {
      this.messages = {
          'status': msgStatus,
          'txt': msg
      }
      jQuery(window).scrollTop(0);
  }

  private checkPermission() {
    this.showLoadingOverlay = true;
    this._user.GetPermissionUser().subscribe(
      data => {
        this.hasUpdateOrderPickingPermission = this._user.RequestPermission(data,'updateOrderPicking');
        this.viewWavePick = this._user.RequestPermission(data,'viewWavePick');
        this.hasPrintWavePickPermission = this._user.RequestPermission(data,'printWavePick');

        if(!this.viewWavePick && !this.hasUpdateOrderPickingPermission) {
            this._router.parent.navigateByUrl('/deny');
        }
        else {
          this.getWavepickStatusList();
        }
      },
      err => {
        // console.log('check permission err', err);
        this.showMessage('danger', this._func.parseErrorMessageFromServer(err));
        this.showLoadingOverlay = false;
      }
    );
  }

  private getWavepickStatusList () {
    this._pickingListService.getWavepickStatusList()
      .subscribe(
        data => {
          this.wavepickStatus = data.data;
          let preSelectedStatus = false;
          for (let i = 0; i < this.wavepickStatus.length; i++) {
            const status = this.wavepickStatus[i];
            if (status.checked) {
              preSelectedStatus = true;
              break;
            }
          }
          if (preSelectedStatus) {
            setTimeout(() => {
              this.search();
            }, 500);
          } else {
            this.getPickingList(1);
          }
        }
      );
  }

  private getPickingList(page = null){
    this.showLoadingOverlay = true;
    if(!page) page = 1;
    let params="?page="+page+"&limit="+this.perPage+ '&type=wv_update';
    if(this.objSort && this.objSort['sort_type'] != 'none') {
      params += '&sort['+ this.objSort['sort_field']+ ']='+ this.objSort['sort_type'];
    }
    else {
      params += '&sort[created_at]=desc&picker_id=99'
    }

    if(this.searchParams) {
      params += this.searchParams;
    }

    this.exportAPIUrl = this._pickingListService.getExportAPIUrl(params + '&export=1');

    this._pickingListService.getPickingList(params)
      .subscribe(
        data => {
          this.createRowData(data);
          this.showLoadingOverlay = false;
        },
        err => {
          this.showMessage('danger', this._func.parseErrorMessageFromServer(err));
          this.showLoadingOverlay = false;
        }
      );
  }

  updateMessages(messages) {
    this.messages = messages;
  }

  private createRowData(data) {
      var rowData: any[] = [];
      if (typeof data.data != 'undefined') {
        this.initPagination(data);

        data = this._func.formatData(data.data);
        for (var i = 0; i < data.length; i++) {
          rowData.push(({
            wv_sts: data[i].wv_sts,
            wv_num: '<a href="#/outbound/picking/' + data[i].wv_id + '">'+ data[i].wv_num +'</a>',
            wv_num_print: data[i].wv_num,
            num_sku: data[i].num_sku,
            num_odr: data[i].num_odr,
            odr_num: data[i].odr_num,
            cus_odr_num: data[i].cus_odr_num,
            cus_po: data[i].cus_po,
            picker_name:data[i].picker_name,
            picker_id:data[i].picker_id,
            user: data[i].user,
            created_at: data[i].created_at,
            wp_id: data[i].wv_id
          }));
        }
      }
      else {
        this.rowData = [];
        this.Pagination = null;
      }

      this.rowData = rowData;
  }

  private initPagination(data){
    let meta = data['meta'];
    if(meta['pagination']) {
      this.Pagination=meta['pagination'];
    }
    else {
      let perPage = meta['perPage'],
          curPage = meta['currentPage'],
          total = meta['totalCount'];
      let count = perPage * curPage > total ? perPage * curPage - total : perPage;
      this.Pagination = {
        "total": total,
        "count": count, // TODO
        "per_page": perPage,
        "current_page": curPage,
        "total_pages": meta['pageCount'],
        "links": {
          "next": data['link']['next']
        }
      }
    }
    this.Pagination['numLinks']=3;
    this.Pagination['tmpPageCount']=this._func.Pagination(this.Pagination);
  }

  private filterList(pageNumber) {
      this.getPickingList(pageNumber);
  }

  private getPage(pageNumber) {
      let arr=new Array(pageNumber);
      return arr;
  }

  private onPageSizeChanged($event) {
      this.perPage = $event.target.value;
      if((this.Pagination['current_page'] - 1) * this.perPage >= this.Pagination['total']) {
          this.currentPage = 1;
      }
      else {
          this.currentPage = this.Pagination['current_page'];
      }
      this.getPickingList(this.currentPage);
  }

  private search() {
    this.searchParams = '';
    let params_arr = jQuery("#form-filter").serializeArray() ;
    for (let i in params_arr) {
        if (params_arr[i].value == "") continue;
        this.searchParams +="&" +  encodeURIComponent(params_arr[i].name.trim()) + "=" + encodeURIComponent(params_arr[i].value.trim());
    }
      this.getPickingList(1);
  }

  private doSort(objSort) {
    this.objSort = objSort;
    this.getPickingList(this.Pagination.current_page);
  }

  private reset() {
      jQuery("#form-filter input[type=text], #form-filter select").each(function( index ) {
          jQuery(this).val("");
      });
      this.refreshMultiSelect = true;
      this.searchParams = '';
      this.getPickingList(1);
  }

  updatePicking() {
    this.getSelectedRow = 'edit';
  }
  runInsertOrderCarton() {
    this.getSelectedRow = 'run-insert';
  }
  
  afterGetSelectedRow($event) {
    var listSelectedItem = $event.data;
    this.messages = null;
    switch ($event.action) {
      case 'edit':
        if (listSelectedItem.length > 1) {
          this.showMessage('danger', 'Please choose only one item to pick!');
        }
        else {
          if (listSelectedItem.length < 1) {
            this.showMessage('danger', 'Please choose one item to pick!');
          }
          else {
            if(listSelectedItem[0].wv_sts != 'New'&& listSelectedItem[0].wv_sts != 'Picking') {
              this.showMessage('danger', this._func.msg('VR130'));
            } else if (!listSelectedItem[0].picker_id) {
              this.showMessage('danger', this._func.msg('WPA001'));
            } else {
              if (listSelectedItem[0].wv_sts == 'New') {
                this._router.parent.navigateByUrl('/outbound/picking/' + listSelectedItem[0].wp_id + '/edit');
              } else if (listSelectedItem[0].wv_sts == 'Picking') {
                this._router.parent.navigateByUrl('/outbound/picking/' + listSelectedItem[0].wp_id + '/complete');
              }
            }
          }
        }
        break;
      case 'run-insert':
        if (listSelectedItem.length > 1) {
          this.showMessage('danger', 'Please choose only one Wave Pick to Run Insert Order Carton Queue.');
        }
        else {
          if (listSelectedItem.length < 1) {
            this.showMessage('danger', 'Please choose one Wave Pick to Run Insert Order Carton Queue.');
          }
          else {
            if(listSelectedItem[0].wv_sts != 'Picking') {
              this.showMessage('danger', 'Run Insert Order Carton Queue is for Wave Pick with status Picking only.');
            }
            else {
              this.showLoadingOverlay = true;
              this._pickingListService.runInsertOrderCartonQueue(this.whsID, listSelectedItem[0].wp_id)
                .subscribe(
                  data => {
                    this.showMessage('success', 'Run Insert Order Carton Queue successfully.');
                    this.showLoadingOverlay = false;
                  },
                  err => {
                    this.showMessage('danger', this._func.parseErrorMessageFromServer(err));
                    this.showLoadingOverlay = false;
                  }
                );
            }
          }
        }
        break;
    }
    this.getSelectedRow = false;
  }

  private expandTable = false;
  private viewListFullScreen() {
    this.expandTable = true;
    setTimeout(() => {
      this.expandTable = false;
    }, 500);
  }
}
