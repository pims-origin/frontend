export class UserListMessages{
  public messages = {
  	"remove_none": "Please choose one item to remove",
  	"remove_confirm": "Are you sure?",
  	"agree": "Yes, delete it!",
  	"cancel": "No, cancel!",
  	"edit_none": "Please choose one user to edit",
    "resetpwd_none": "Please choose one user to change password",
    "resetpwd_multiple": "Please choose only one user to change password",
  	"edit_multiple": "Please choose only one user to edit",
    "connection": "Sorry, there's a problem with the connection."
  }
}
