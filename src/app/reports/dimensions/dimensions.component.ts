
import {Component} from '@angular/core';
import {API_Config} from '../../common/core/API_Config';
import {BaseTable} from '../../common/core/classes_us/base-table';
import {Functions} from '../../common/core/functions';
import {TableFunctions} from '../../common/core/table-functions';
import {DateFunctions} from '../../common/core/functions/date-functions';
import {BaseService} from '../../common/core/classes_us/base-service';
import {CustomersService} from '../../master-data/customers/customers-service';
import {TableStructure} from '../../common/directives/directives';

@Component ({
    selector: 'dimensions',
    directives: [
        TableStructure
    ],
    templateUrl: '../../common/directives/table-structure/table.component.html',
    providers: [
        BaseService,
        CustomersService,
        TableFunctions,
        DateFunctions
    ]
})

export class DimensionsComponent extends BaseTable
{
    public tableID = 'dimensions';
    public url = this._api.API_DIMENSIONS_REPORTS;
    public headerUrlSuffix = 'rep';
    public title = 'Dimensions';
    public permissionPage = 'viewReport';

    public headerDef = [
        {
            id: 'ver_table',
            value: 1 + Math.random() * 10000
        },
        this.columnList.warehouse,
        this.columnList.customer,
        this.columnList.upc,
        this.columnList.sku,
        this.columnList.color,
        this.columnList.size,
        this.columnList.createdAt,
        this.columnList.ctn_cuft,
        this.columnList.ctn_qty
    ];

    public unsortableColumns = [
        'customer',
        'upc',
        'sku',
        'color',
        'size',
        'createdAt'
    ];

    public searches = [
        this.searchList.customer,
        this.searchList.sku,
        this.searchList.upc_dim
    ];

    /*
    ****************************************************************************
    */

    constructor (
        public _api: API_Config,
        public _func: Functions,
        public _tableFunc: TableFunctions,
        public _tableService: BaseService,
        public _customersService: CustomersService
    )
    {
        super();

        this._tableFunc.permissionCall(this);
    }

    //**************************************************************************

}
