import {Component} from '@angular/core';
import {Router, RouterLink, RouteConfig, ROUTER_DIRECTIVES} from '@angular/router-deprecated';
import {PickingListComponent} from "./picking-list/picking-list.component";
import {PickingUpdateComponent} from "./picking-update/picking-update.component";
import { PickingCompleteComponent } from './picking-complete/picking-complete.component';

@Component ({
    selector: 'picking-management',
    directives: [ROUTER_DIRECTIVES],
    template: `<router-outlet></router-outlet>`,
})

@RouteConfig([
    { path: '/', component: PickingListComponent , name: 'Picking List', useAsDefault: true },
    { path: '/:id', component: PickingUpdateComponent , name: 'Picking Inventory Update', data:{action:'view'}},
    { path: '/:id/edit', component: PickingUpdateComponent , name: 'Picking Inventory Update', data:{action:'edit'}},
    { path: '/:id/complete', component: PickingCompleteComponent , name: 'Picking Inventory Update', data:{action:'complete'}}
])
export class PickingManagementComponent {

}
