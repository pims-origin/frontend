/**
 * Created by admin on 6/9/2016.
 */
import {Injectable} from '@angular/core';
declare var jQuery: any;

@Injectable()
export class StylesheetService{
  private enableStyle: boolean = true;// this property is using for shutting down StylesheetService
  constructor(){

  }

  addLoading(){
    if(this.enableStyle){
      // jQuery('body').css('opacity',0.3);
      // jQuery('body').append('<div id="loadingSpinner" class="spinner"></div>');
      jQuery('body').append('<div id="loadingSpinner" class="loading-overlay" ><img src="/assets/images/spinner.gif" alt=""></div>');

    }
  }

  removeLoadding(){
    if(this.enableStyle){
      // jQuery('body').removeAttr('style');
      jQuery('#loadingSpinner').remove();
    }
  }
}
