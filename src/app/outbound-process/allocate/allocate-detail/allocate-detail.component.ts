import {Component } from '@angular/core';
import { FORM_DIRECTIVES,ControlGroup,FormBuilder, Control ,ControlArray,Validators} from '@angular/common';
import {RouteParams,Router ,RouteData} from '@angular/router-deprecated';
import {UserService, Functions} from '../../../common/core/load';
import {AllocateDetailServices} from './allocate-detail-service';
import { ValidationService } from '../../../common/core/validator';
import {WMSBreadcrumb,PaginatePipe,WMSMessages, PaginationControlsCmp, PaginationService} from '../../../common/directives/directives';
import { Http } from '@angular/http';
import {GoBackComponent} from "../../../common/component/goBack.component";
import {BoxPopupService} from "../../../common/popup/box-popup.service";
import {WarningBoxPopup} from "../../../common/popup/warning-box-popup";

@Component({
  selector: 'cru-order',
  providers: [AllocateDetailServices,PaginationService,ValidationService,FormBuilder, BoxPopupService,UserService],
  directives: [FORM_DIRECTIVES,PaginationControlsCmp,WMSMessages,GoBackComponent,WMSBreadcrumb],
  pipes:[PaginatePipe],
  templateUrl: 'allocate-detail.component.html',
})

export class AllocateDetailComponent {

  public Loading = [];
  private messages;
  private Action;
  private TitlePage='Allocate Detail';
  private IsView:boolean=false;
  private perPage=20;
  ODForm: ControlGroup;
  private showLoadingOverlay:boolean=false;
  private Country=[];
  private whs_id;
  private orderType=[];
  private orderData;
  private orderId;
  private orderCannotAllocate = false;
  private allItem;

  constructor(
    private _http: Http,
    private _Func: Functions,
    private params: RouteParams,
    private fb: FormBuilder,
    private _Valid: ValidationService,
    private _boxPopupService: BoxPopupService,
    private ALCService:AllocateDetailServices,
    _RTAction: RouteData,
    private _user:UserService,
    private _router: Router) {


    this.Action=_RTAction.get('action');

    this.checkPermission();

  }

  // Check permission for user using this function page
  private allocateOrder;
  private allowAccess:boolean=false;
  checkPermission(){

    this.showLoadingOverlay = true;
    this._user.GetPermissionUser().subscribe(
      data => {
        this.showLoadingOverlay = false;
        this.allocateOrder = this._user.RequestPermission(data, 'allocateOrder');

          if(this.allocateOrder) {
            this.allowAccess=true;
            this.TitlePage = 'Allocate Order';
            this.orderDetail();
          }
          else{
            this.redirectDeny();
          }

      },
      err => {
        this.parseError(err);
        this.showLoadingOverlay = false;
      }
    );
  }

  redirectDeny(){
    this._router.parent.navigateByUrl('/deny');
  }

  allCateOrder(){

    let n=this;
    var hasFormError = false;
    var hasAtleastOneAllocatedQty = false;
    this.messages = null;
    for(var i = 0, l = this.allItem.length; i < l; i++) {
      if(this.allItem[i].allocated_qty) {
        hasAtleastOneAllocatedQty = true;
      }
      if(this.allItem[i].hasErrGreaterThanRemain) {
        hasFormError = true;
      }
    }
    if(!hasAtleastOneAllocatedQty) {
      hasFormError = true;
      this.messages = this._Func.Messages('danger', this._Func.msg('VR139'));
    }
    if(!hasFormError) {

      this.showLoadingOverlay = true;
      this.ALCService.allCateOrder(this.orderId,this.allItem,this.orderData['odr_type_key']).subscribe(
        data => {
          this.showLoadingOverlay = false;
          //Reset Form
          this.messages = this._Func.Messages('success', this._Func.msg('VR107'));
          setTimeout(function(){

            if(n.orderData['origin_odr_type']=='EC'){
              n._router.parent.navigateByUrl('/outbound/online-order');
            }
            else{
              n._router.parent.navigateByUrl('/outbound/order');
            }


          }, 600);
        },
        err => {
          this.showLoadingOverlay = false;
          if(err.json().errors.message)
          {
            this.messages = this._Func.Messages('danger',err.json().errors.message);
          }
        },
        () => {}
      );
    }
  }

  orderDetail(){

    this.orderId=this.params.get('id');
    this.ALCService.getOrderDetail(this.orderId).subscribe(
      data => {
        this.orderData=data;
        this.orderData['totalSku']=this.orderData['items'] ? this.orderData['items'].length : 0;
        this.allItem = this.orderData.items;

        // check checkInvalidAvailPieceQty
        this.checkPartical(this.orderData.items);

      },
      err => {
        this.parseError(err);
      },
      () => {}
    );

  }

  /*
  * Check Phare 1 :  If any row has avail qty = 0 or null
  * */
  private checkPhare={};
  checkPartical(dataList:Array<any>) {

    // Check Phare 1

    let checkPhare = this.checkPhare;
    let sumQty = 0;

    /*
     * Use only one loop for all handle
     * */

      for (let i=0;i<dataList.length;i++) {

        sumQty = sumQty + dataList[i]['avail'];

        if(!this.orderData['partial']){

          // Check Phare 1 :  If any row has avail qty = 0 or null
          if (dataList[i]['avail'] == 0 || dataList[i]['avail'] == null){
            checkPhare['order_canot_be_allocated'] = true;
            this.orderCannotAllocate=true;
            this.messages = this._Func.Messages('danger', this._Func.msg('ALC009'));
            return;
          }
          /*
           *  If any row aval qty < requested qty
           * */
          if (dataList[i]['avail'] < dataList[i]['piece_qty']) {
            this.orderCannotAllocate=true;
            checkPhare['avail_qty_smaller_than_requested_qty'] = true;
          }



          // Neu tat cac cac dong avail qty >= requested qty => hop le. Hien thi allocate qty = requested qty.

        }

        if(this.orderData['partial'])
        {
          if (dataList[i]['avail'] > 0){
            checkPhare['avail_qty_greater_than_zero'] = true;
            // set  allo qty = min (requested qty, avail qty);
            this.setAllocateQtyMin(dataList);
            return;
          }
          if (i == (dataList.length-1) && sumQty == 0) {
            checkPhare['sum_qty_is_zero'] = true;
            this.orderCannotAllocate=true;
            this.messages = this._Func.Messages('danger', this._Func.msg('VR138'));
            return;
          }

        }


    }

    // Only check if done loop
    if(!this.orderData['partial']){

      if(checkPhare['avail_qty_smaller_than_requested_qty']){
        this.messages = this._Func.Messages('danger', this._Func.msg('ALC006'));
        return;
      }
      else{
        checkPhare['avail_qty_all_row_greater_request_qty']=true;
        this.setAlloCateQtyEqualToReqQty(dataList);
      }

    }



  }

  setAllocateQtyMin(list:Array<any>)
  {
    list.forEach((item)=>{

      item['allocated_qty']= item['piece_qty'] < item['avail'] ? item['piece_qty'] : item['avail']

    });
  }

  setAlloCateQtyEqualToReqQty(list:Array<any>)
  {
      list.forEach((item)=>{
          item['allocated_qty']=item['piece_qty'];
      });
  }


  // Show error when server die or else
  private parseError (err) {
    this.messages = {'status' : 'danger', 'txt' : this._Func.parseErrorMessageFromServer(err)};
  }

  redirectToList(){
    let n = this;
    /*case 1: modify text*/
    // let warningPopup = new WarningBoxPopup();
    // warningPopup.text = "Are you sure to cancel";
    // this._boxPopupService.showWarningPopup(warningPopup)

    /*case 2: default*/
    this._boxPopupService.showWarningPopup()
      .then(function (dm) {
        if(dm)
          window.history.back();
          // n._router.parent.navigateByUrl('/outbound/allocate');
      })
  }

  private checkInputNumber (evt) {
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)){
      evt.preventDefault();
    }
  }

  private formatValue(evt, item) {
    var value = evt.target.value;
    if(!value) {
      value = null;
    }
    else {
      if(isNaN(value)) {
        value = null;
      }
      else {
        value = parseInt(value);
      }
      if(value <= 0) {
        value = null;
      };
      evt.target.value = value;
    }
    item.allocated_qty = value;
    this.checkGreater(item);
  }

  private checkGreater(item) {
    if(item.allocated_qty && (item.allocated_qty > item.piece_qty || item.allocated_qty > item.avail)) {
      item.hasErrGreaterThanRemain = true;
    }
    else {
      item.hasErrGreaterThanRemain = false;
    }
  }
}




