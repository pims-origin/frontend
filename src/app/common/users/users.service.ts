import {Component, Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import { JwtHelper } from 'angular2-jwt/angular2-jwt';
import {Functions} from '../../common/core/functions';
import { Router} from '@angular/router-deprecated';
import {API_Config} from '../../common/core/API_Config';
declare var jQuery: any;
@Injectable()
export class UserService {

  public AuthHeader = this._Func.AuthHeader();
  public AuthHeaderPost = this._Func.AuthHeaderPost();
  User:any;
  public permissionList=[];

  constructor(private _Func: Functions, private _router: Router, private _API: API_Config, private http: Http, public jwtHelper: JwtHelper)
  {
  }
  /*
    Get PerMissionUser
  */
  GetPermissionUser()
  {
    let ListPermissionUser=null;

    /*let n=this;

    jQuery.ajax({
      url: this._API.API_Permission,
      method: 'GET',
      beforeSend: function(xhr) { xhr.setRequestHeader('Authorization', 'Bearer ' + n._Func.getToken()) },
      async: false,
      success: function(data) {
          ListPermissionUser=data.data;
           console.log('get permission done');
      },
      error: function(error) {
            ListPermissionUser = false;
            console.log(error);
      }
    });*/

    return this.http.get(this._API.API_USER_Permission, { headers: this._Func.AuthHeader() })
    //return this.http.get(this._API.API_GroupMenu)
    .map((res: Response) => res.json().data);

   // return ListPermissionUser;

  }

  /*====================
  RequestPermission
  =====================*/
  // RequestPermission(permission) {

  //   let flag = false;
  //   let datapermission=this.GetPermissionUser();

  //   console.log(datapermission);

  //   if (datapermission && datapermission.length)
  //   {
  //     for (var per of datapermission) {
  //       console.log(per.name +"=="+permission)
  //       if (per.name == permission)
  //       {
  //         console.log('found permission');
  //         return true;
  //       }
  //     }
  //     /*
  //       If not found permission
  //     */
  //     //this._Func.RedirectTo('/deny');
  //      return false;

  //  }
  RequestPermission(permission, permission_req) {

    if (permission && permission.length) {
      for (var per of permission) {
        //console.log(per.name + "<==>" + permission_req)
        if (per.name == permission_req) {
          //console.log('found permission');
          return true;
        }
      }
      return false;

    }
    else {
      return false;
    }
  }

  checkAuthentication() {
    return this.http.get(this._API.API_User_Token, { headers: this._Func.AuthHeader() })
    .map((res: Response) => res.json().data);
  }

  hasAuthenticationError(err) {
    var hasAuthenError = false;
    if(typeof err == 'object' && err._body) {
      var errJSON = JSON.parse(err._body);
      if(errJSON.status == 401 && (errJSON.code == 402 || errJSON.code == 403 || errJSON.code == 0)) {
        hasAuthenError = true;
      }
    }
    return hasAuthenError;
  }
}
