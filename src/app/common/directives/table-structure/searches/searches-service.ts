
import {Injectable} from '@angular/core';
import {TableFunctions} from '../../../core/table-functions';
import {DateFunctions} from '../../../core/functions/date-functions';
declare var jQuery: any;

@Injectable()

export class SearchesService {

    private format = 'yyyy-mm-dd';

    constructor(
        private _tableFunc: TableFunctions,
        private _dateFunctions: DateFunctions
    )
    {

    }

    //**************************************************************************

    private getTableData(tableComponent)
    {
        this._tableFunc.callGetTableData({
            parentComponent: tableComponent
        });
    }

    //**************************************************************************

    public reset(tableComponent)
    {
        let searches = tableComponent.get('searches');

        for (let count=0; count<searches.length; count++) {
            if (tableComponent.get('searches')[count].hasOwnProperty('autocomplete')) {
                this.autocomplete({
                    tableComponent: tableComponent,
                    autocomplete_field: tableComponent.get('searches')[count].name,
                    autocomplete_value: ''
                });
            }
        }

        jQuery('#form-filter input, #form-filter select').map(function() {
            jQuery(this).val('');
        });

        let searchParams = tableComponent.hasOwnProperty('controlSearch') ?
                tableComponent.get('controlSearch') : '';

        tableComponent.set('searchParams', searchParams);

        this.getTableData(tableComponent);
    }

    //**************************************************************************

    public getSearchParams(data)
    {
        let tableComponent = data.tableComponent;

        let searchParams = tableComponent.hasOwnProperty('controlSearch') ?
                tableComponent.get('controlSearch') : '';

        tableComponent.set('searchParams', searchParams);

        let params_arr = jQuery('#form-filter').serializeArray(),
            searches = tableComponent.get('searches'),
            setValue = tableComponent.get('searchParams');

        for (let count in params_arr) {
            if (! params_arr[count].value) {
                continue;
            }

            var field = params_arr[count].name.trim(),
                param = params_arr[count].value.trim();

            for (let count=0; count<searches.length; count++) {

                let search = searches[count];

                if (search.name != field) {
                    continue;
                }

                field = search.hasOwnProperty('field') ? search.field : field;
                param = search.hasOwnProperty('datePicker') && search.datePicker ?
                    this._dateFunctions.getDateString(new Date(param), this.format) :
                    param;
            }

            setValue += '&' + field + '=' + encodeURIComponent(param);
        }

        return setValue;
    }

    //**************************************************************************

    public search(data)
    {
        let tableComponent = data.tableComponent,
            searchParams = this.getSearchParams(data);

        tableComponent.set('searchParams', searchParams);

        this.getTableData(tableComponent);
    }

    /*
    ****************************************************************************
    */

    public autocomplete(data)
    {
        let tableComponent = data.tableComponent;

        let field = data.autocomplete_field,
            value = data.autocomplete_value;

        if (value) {
            let searchParams = this.getSearchParams({
                tableComponent: tableComponent
            });

            searchParams += '&' + field + '=' + value + '&autocomplete=' + field;

            tableComponent.set('searchParams', searchParams);

            this._tableFunc.callGetTableData({
                parentComponent: tableComponent,
                autocomplete: field
            });
        } else {
            this._tableFunc.setAutcomplete(tableComponent, field, []);
        }
    }

    /*
    ****************************************************************************
    */

}