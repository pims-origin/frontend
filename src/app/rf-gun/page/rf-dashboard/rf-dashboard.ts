import {Component,OnInit} from "@angular/core";
import {RouteConfig, RouterLink, Router, ROUTER_DIRECTIVES} from '@angular/router-deprecated';
@Component({
  selector: 'rf-dashboard-zone',
  providers: [],
  directives: [],
  templateUrl: 'rf-dashboard.html',
})
export class RFGUNDASHBOARDComponent {
  private routerParent='/rf-gun/';
  constructor( private _router: Router)
  {
  }
  redirectTo(page)
  {
    this._router.parent.navigateByUrl(this.routerParent+page);
  }
}
