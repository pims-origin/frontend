import {Component} from '@angular/core';
import {Router, RouterLink, RouteConfig, ROUTER_DIRECTIVES} from '@angular/router-deprecated';
import {OrderListComponent} from "../order/order-list/order-list.component";
import {AllocateDetailComponent} from "./allocate-detail/allocate-detail.component";
import {AllocateListComponent} from "./allocate-list/allocate-list.component";

@Component ({
    selector: 'allocate-management',
    directives: [ROUTER_DIRECTIVES],
    template: `<router-outlet></router-outlet>`,
})

@RouteConfig([
    { path: '/', component: AllocateListComponent , name: 'Allocate List', data:{page:'allocate'}, useAsDefault: true },
    { path: '/:id', component: AllocateDetailComponent , name: 'Allocate Detail', data:{action:'edit'}}
])
export class AllocateManagementComponent {

}
