import {Component} from "@angular/core";
import { Router, RouterLink, ROUTER_DIRECTIVES } from '@angular/router-deprecated';
import { CORE_DIRECTIVES, FORM_DIRECTIVES,ControlGroup,FormBuilder, Control ,Validators} from '@angular/common';
import {UserProfileServices} from "../user-profile.service";
import {StylesheetService} from "../../common/services/stylesheet.service";
import {BoxPopup} from "../../common/popup/box-popup";
import {WarningBoxPopup} from "../../common/popup/warning-box-popup";
import {BoxPopupService} from "../../common/popup/box-popup.service";
import {ValidationService } from '../../common/core/validator';
import {Functions} from "../../common/core/functions";
import {WMSMessages} from "../../common/directives/messages/messages";
declare var jQuery: any;

@Component({
  selector: 'edit-profile',
  directives: [CORE_DIRECTIVES, FORM_DIRECTIVES, WMSMessages],
  providers: [UserProfileServices,StylesheetService,BoxPopupService, ValidationService],
  templateUrl: 'edit-profile.html',

})
export class EditProfileComponent{
  public SuccessMessage: string = '';
  private showError = false;
  private messages;
  private timeoutHideMessage;
  userForm: ControlGroup;

  constructor(
      private _func: Functions,
      private _profileService: UserProfileServices,
      private _styleService: StylesheetService,
      private _boxPopupService: BoxPopupService,
      private _Valid: ValidationService,
      private fb: FormBuilder,
      private _router: Router
  ){
    this.userForm =this.fb.group({
        first_name: [null, this._Valid.customRequired],
        last_name: [null, this._Valid.customRequired],
        email: [null, Validators.compose([this._Valid.customRequired, this.logisoftEmailValidator])],
        phone: [null, this._Valid.customRequired],
        phone_extend: [null],
        mobile: [''],
        username: [''],
        usr_dpm_id: ['']
    });
    this._styleService.addLoading();
    this.SuccessMessage = '';
    _profileService.getProfileDetailByUserId().subscribe(
      (data:any) => {
        data = data.json;
        (<Control>this.userForm.controls['first_name']).updateValue(data['first_name']);
        (<Control>this.userForm.controls['last_name']).updateValue(data['last_name']);
        (<Control>this.userForm.controls['email']).updateValue(data['email']);
        (<Control>this.userForm.controls['phone']).updateValue(data['phone']);
        (<Control>this.userForm.controls['phone_extend']).updateValue(data['phone_extend']);
        (<Control>this.userForm.controls['mobile']).updateValue(data['mobile']);
        (<Control>this.userForm.controls['username']).updateValue(data['username']);
        (<Control>this.userForm.controls['usr_dpm_id']).updateValue(data['usr_dpm_id']);
        this._styleService.removeLoadding();
      },
      err => {
        this._styleService.removeLoadding();
      },
      () => {
      }
    )
  }

  private logisoftEmailValidator(control:Control): {[key: string]: any} {
      // RFC 2822 compliant regex
      var val = jQuery.trim(control.value);
      if (val && val.match(/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)) {
          return null;
      } else {
          if(val!='')
          {
            return { 'invalidEmailAddress': true };
          }
      }
  }

  showMessage(msgStatus, msg) {
      this.messages = {
          'status': msgStatus,
          'txt': msg
      }
      jQuery(window).scrollTop(0);
  }

  submitForm(data :Object):void {
    this.showError = true;
    if(this.userForm.valid) {
        // trim data before submit
        for(let key in data) {
          let val = data[key];
          if(val && typeof val == 'string') {
            data[key] = jQuery.trim(val);
          }
        }
        data['status'] = 'AC';
        var fullName = data['first_name'] + ' ' + data['last_name'];
        let stringData = JSON.stringify(data);

        this._styleService.addLoading();
        let objectData = new Map();
        objectData.set("editProfile","editProfile");

        this._profileService.updateProfileByUserId(stringData,objectData).subscribe(
          res => {
            if(res.data.success){
              this.showMessage('success', 'Update successfully.');
              jQuery('.user-fullname').text(fullName);
              setTimeout(() =>{
                this._router.parent.navigateByUrl('/profile');
              }, 600);
            }
            this._styleService.removeLoadding();
          },
          err => {
            console.error('err', err);
            this.showMessage('danger', this._func.parseErrorMessageFromServer(err));
            this._styleService.removeLoadding();
          }
        );
    }
  }


  cancel()
  {
    let n = this;
    this._boxPopupService.showWarningPopup()
        .then(function (dm) {
          if(dm)
            n._router.navigateByUrl('profile');

        });
  }
}
