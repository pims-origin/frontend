import {Component} from "@angular/core";
import {Router, ROUTER_DIRECTIVES} from '@angular/router-deprecated';
import {ControlGroup, FormBuilder, Control, Validators} from '@angular/common';
import {API_Config, UserService, Functions, FormBuilderFunctions} from '../../common/core/load';
import {BoxPopupService} from "../../common/popup/box-popup.service";
import {ForecastReportService} from './forecast-report-service';
import {WMSMessages, FileExporter} from '../../common/directives/directives';
import 'ag-grid-enterprise/main';

@Component({
	selector: 'forecast-report',
	providers: [ForecastReportService, BoxPopupService],
	directives: [ROUTER_DIRECTIVES, WMSMessages, FileExporter],
	templateUrl: 'forecast-report.component.html'

})
export class ForecastReportComponent {
	public whs_id:string;
	public showError = false;
	public messages:any;
	public searchForm:ControlGroup;
	public showLoadingOverlay:boolean = false;
	public loading:boolean = false;
	public dataReports:Array<any> = [];
	public customers:Array<any> = [];
	public months = [];
	public years = [];
	public quarters = [];
	curYear = new Date().getFullYear();
	curMonth = new Date().getMonth()+1;
	exportAPIUrl: string = '';
	fileName: string = `forecast-report.csv`;
	headerExport = ['Indicator', 'INBOUND - Forecast', 'INBOUND - Actual', 'INBOUND - Variance', 'INBOUND - Percent',
		'OUTBOUND - Forecast', 'OUTBOUND - Actual', 'OUTBOUND - Variance', 'OUTBOUND - Percent', 'STORAGE - Forecast',
		'STORAGE - Actual', 'STORAGE - Variance', 'STORAGE - Percent', '1 Month Ago', '2 Month Ago', '3 Month Ago'];
	dataExport: any[] = [];
	availableDate: any = null;
	period = 'month';
	lastPeriod = 'month';

	// Check permission for user using this function page
	private canAccess:boolean = false;

	constructor(
		private formBuilder: FormBuilder,
		private fbFunc: FormBuilderFunctions,
		private _service: ForecastReportService,
		private _Func: Functions,
		private _user: UserService,
		private _apiConfig: API_Config,
		private _router:Router
	) {
		this.buildSearchForm();
		this.whs_id = localStorage.getItem('whs_id');
		this.checkPermission();
	}

	private _buildYears() {
		this.years = [];
		this.months = [];
		if (!this.availableDate || this.availableDate.length == 0) {
			// const minYear = 2010;
			// for (let i = this.curYear; i >= minYear; i--) {
			// 	this.years.push(i);
			// }
		} else {
			let existedCurYear = false;
			this.availableDate.forEach(item => {
				this.years.push(item.year);
				if (this.curYear === item.year) {
					existedCurYear = true;
				}
			});
			if (!existedCurYear) {
				this.years.push(this.curYear);
			}
		}
	}

	private _buildMonth(year = null) {
		this.months = [];
		if (!this.availableDate && !year) {
			for (let i = 1; i <= 12; i++) {
				this.months.push(i);
			}
			this._buildQuarter();
		} else {
			const n = this.availableDate.length;
			for (let i = 0; i < n; i++) {
				if (this.availableDate[i].year === parseInt(year, 10)) {
					this.months = this.availableDate[i].months;
				}
			}
			if (year === this.curYear) {
				let existedCurMonth = false;
				this.months.forEach(item => {
					if (this.curMonth === item) {
						existedCurMonth = true;
					}
				});
				if (!existedCurMonth) {
					this.months.push(this.curMonth);
				}
			}
			this._buildQuarter(this.months);
		}
	}

	buildSearchForm() {
		this.searchForm = this.formBuilder.group({
			cus_id: ['', Validators.required],
			month: [this.curMonth],
			quarter: [''],
			year: [this.curYear, Validators.required],
			period: [this.period, Validators.required],		
		});
	}

	public checkPermission() {
		this.showLoadingOverlay = true;
		this._user.GetPermissionUser().subscribe(
			data => {
				this.showLoadingOverlay = false;
				this.canAccess = this._user.RequestPermission(data, 'viewReport');
				if (this.canAccess) {
					this.getCustomers();
				} else {
					// redirect to deny page.
					this._router.parent.navigateByUrl('/deny');
				}
			},
			err => {
				this.messages = this._Func.Messages('danger', this._Func.parseErrorMessageFromServer(err));
				this.showLoadingOverlay = false;
			}
		);
	}

	public getReports() {
		this._validForm(this.searchForm);
		if (this.searchForm.valid) {
			const data = this.searchForm.value;
			const params = '?cus_id=' + data['cus_id'] + '&year=' + data['year']
				+ '&period=' + this.period + '&period_value=' + data[this.period];
			
			this.showLoadingOverlay = true;
	
			this._service.getForecastReports(params).subscribe(
				data => {
					this.showLoadingOverlay = false;
					this.dataReports = data.data;
					this.lastPeriod = this.period;
					if (data.data['length']) {
						this.createRowData(data);
						this.messages = '';
					}
				},
				err => {
					this.messages = this._Func.Messages('danger', err.json().errors.message);
					this.showLoadingOverlay = false;
				},
				() => {}
			);
		} else {
            this.showError = true;
        }
	}

	public getCustomers() {
		const params:string = `?whs_id=${this.whs_id}&limit=1000&sort[cus_name]=asc`;
		this._service.getCustomersByWH(params).subscribe(
			data => {
				this.customers = data['data'];
			},
			err => {
				this.messages = this._Func.Messages('danger', err.json().errors.message);
			},
			() => {
			}
		);
	}

	private createRowData(data) {

        // Check data
        if (typeof data.data != 'undefined') {
			

			data = this._Func.formatData(data.data);
			this.dataExport = [];
			for (var i = 0; i < data.length; i++) {
	
	            this.dataExport.push([
					data[i].fc_ind_name,
					data[i].fc_ib,
					data[i].fc_ib_act,
					data[i].fc_ib_var,
					data[i].fc_ib_percent,
	                data[i].fc_ob,
	                data[i].fc_ob_act,
	                data[i].fc_ob_var,
					data[i].fc_ob_percent,
					data[i].fc_st,
					data[i].fc_st_act,
					data[i].fc_st_var,
					data[i].fc_st_percent,
					data[i].fc_st_1_period_ago,
					data[i].fc_st_2_period_ago,
					data[i].fc_st_3_period_ago
				]);
			}
		}
	}

	changeCustomer($event) {
		const cusId = $event.target.value;
		if (!cusId) {
			return;
		}
		this.loading = true;
		this._service.getData(cusId).subscribe(
			data => {
				this.loading = false;
				this.availableDate = data;
				this._buildYears();
				this._buildMonth(this.curYear);
				if (this.availableDate && this.availableDate.length) {
					// set default value
					setTimeout(() => {
						this._selectDefaultValue();
						// search
						this.getReports();
					}, 500);
				} else {
					// reset data
					this._resetByFields(['month', 'quarter', 'year', 'period']);
				}
			},
			err => {
				this.loading = false;
				this.messages = this._Func.Messages('danger', err.json().errors.message);
			},
			() => {
			}
		);
	}

	changeYear($event) {
		const year = $event.target.value;
		this._buildMonth(year);
		this._resetByFields(['month', 'quarter']);
		if (parseInt(year, 10) === this.curYear) {
			this._selectDefaultValue();
		}
	}

	reset() {
		this.resetSearchForm();
		this.dataReports = [];
		this.showError = false;
	}

	search() {
		this.getReports();
	}

	resetSearchForm() {
		this._resetByFields(['cus_id', 'month', 'quarter', 'year', 'period']);
	}

	_resetByFields(fields: string[]) {
		fields.forEach(field => {
			this._setField(field, '');
		});
	}

	_setField(field: string, val: any) {
		this.fbFunc.setValueControl(this.searchForm, field, val);
	}

	_validForm(form: ControlGroup) {
		// reset month, quarter
		this.fbFunc.setErrorControl(form, 'month', null);
		this.fbFunc.setErrorControl(form, 'quarter', null);
		// check period
		const err = !form.value[this.period]? {'required': true} : null;
		this.fbFunc.setErrorControl(form, this.period, err);
	}

	changePeriod(val: string) {
		this.period = val;
		(<Control>this.searchForm.controls['period']).updateValue(this.period);
		// this._resetByFields(['month', 'quarter']);
		if (this.curYear === parseInt(this.searchForm.value['year']), 10) {
			this.period === 'month' ? this._setField('month', this.curMonth)
				: this._setField('quarter', this._getQuarterByMonth(this.curMonth));
		}
	}

	private _buildQuarter(months = null) {
		this.quarters = [];
		if (!this.availableDate && !months) {
			for (let i = 1; i <= 4; i++) {
				this.quarters.push(i);
			}
		} else {
			const n = months.length;
			for (let i = 0; i < n; i++) {
				const quarter = this._getQuarterByMonth(months[i]);
				if (this.quarters.indexOf(quarter) === -1) {
					this.quarters.push(quarter);
				}
			}
			this.quarters.sort((a, b) => a - b);
		}
	}

	private _getQuarterByMonth(month: Number): Number {
		let quarter = 1;
		switch(month) {
			case 1:
			case 2:
			case 3:
				quarter =  1;
				break;
			case 4:
			case 5:
			case 6:
				quarter =  2;
				break;
			case 7:
			case 8:
			case 9:
				quarter =  3;
				break;
			case 10:
			case 11:
			case 12:
				quarter =  4;
				break;
		}
		return quarter;
	}

	getHeaderExport() {
		const unit = this.period === 'month' ? 'Month' : 'Quarter';
		return ['Indicator', 'INBOUND - Forecast', 'INBOUND - Actual', 'INBOUND - Variance', 'INBOUND - Percent',
		'OUTBOUND - Forecast', 'OUTBOUND - Actual', 'OUTBOUND - Variance', 'OUTBOUND - Percent', 'STORAGE - Forecast',
		'STORAGE - Actual', 'STORAGE - Variance', 'STORAGE - Percent', '1 ' + unit + ' Ago', '2 ' + unit + 's Ago', '3 ' + unit + 's Ago'];
	}

	_selectDefaultValue() {
		this._setField('year', this.curYear);
		this._setField('period', this.period);
		if (this.period === 'month') {
			this._setField('month', this.curMonth);
		} else {
			this._setField('quarter', this._getQuarterByMonth(this.curMonth));
		}
	}

	changePeriodValue($event: any, period: string) {
		const val = $event.target.value;
		if (val !== '') {
			this._setField(period, val);
			this.search();
		}
	}
}
