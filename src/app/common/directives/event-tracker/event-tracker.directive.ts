import { Directive,HostListener,Input } from '@angular/core';
import { JwtHelper } from 'angular2-jwt/angular2-jwt';

@Directive({
    selector: '[eventTracker]'
})

export class EventTrackerDirective {
    private label: string = '';
    @Input('eventTracker') option:any;

    @HostListener('click', ['$event']) onClick($event){
        let that = this;
        if (!(<any>window).hasOwnProperty('ga')) 
            return; 

        (<any>window).ga('send', 'event', this.option.category, this.option.action, this.label, {
            hitCallback: function() {
                console.log('Tracking is successful ', that.option.category + ' - ' + that.option.action + ' - ' + that.label);
            }

        });

    }
    constructor(private jwtHelper: JwtHelper) {
        const token = localStorage.getItem('jwt');
        if (!token) {
            return;
        }
        const userInfo = this.jwtHelper.decodeToken(token);
        this.label = userInfo ? userInfo.jti + ' : ' + userInfo.name : '';
    }

}
