import {Component,OnInit} from "@angular/core";
import { Router, RouterLink, ROUTER_DIRECTIVES } from '@angular/router-deprecated';
import { FORM_DIRECTIVES,ControlGroup,FormBuilder, Control,Validators} from '@angular/common';
import {API_Config, UserService, Functions} from '../../common/core/load';
import {BoxPopupService} from "../../common/popup/box-popup.service";
import {OrderReportService} from './order-report-service';
import {WMSPagination,AgGridExtent,WMSMessages} from '../../common/directives/directives';
import 'ag-grid-enterprise/main';

@Component({
  selector: 'performance-report',
  providers:[OrderReportService,BoxPopupService],
  directives: [ROUTER_DIRECTIVES,AgGridExtent,WMSMessages,WMSPagination],
  templateUrl: 'order-report.component.html',

})
export class OrderReportComponent{

  private headerURL = this.apiService.API_User_Metas+'/orr';
  private columnData = {
    odr_num: {
      title:'Order Num',
      pin:true,
      sort:true,
      id:true,
      ver_table:'23s9993'
    },
    cus_name: {
      title:'Customer',
      pin:true,
      sort:true
    },
    sku_ttl: {
      title:'SKU',
      pin:true,
      sort:true
    },
    req_qty:{
      title:'Requested Qty',
      pin:true,
      sort:true
    },
    req_cmpl_dt: {
      title:'Requested Date',
      pin:true,
      sort:true
    },
    status:{
      title:'Status',
      pin:true,
      sort:true
    },
    created_at: {
      title:'Created At',
      pin:true,
      sort:true
    },
    created_by: {
      title:'Created By',
      pin:true,
      sort:true
    },
  };

  private Pagination;
  private perPage=20;
  private whs_id;
  private messages: any;
  private Loading={};
  private searching={};
  searchForm:ControlGroup;
  private isSearch:boolean=false;
  private listActivity:Array<any>=[];
  private sortData={fieldname:'created_at',sort:'desc'};
  private showLoadingOverlay:boolean=false;
  private viewTime=1;
  private userId='';
  private status='';
  private userList:Array<any>=[];
  private statusList:Array<any>=[];
  private loadDataStatus={};

  constructor(
    private eventService:OrderReportService,
    private _Func: Functions,
    private _user:UserService,
    private _router: Router,
    private apiService: API_Config) {
    this.whs_id = localStorage.getItem('whs_id');
    this.checkPermission();

  }

  // Check permission for user using this function page
  private viewReports;
  private allowAccess:boolean=false;
  private checkPermission() {
    this.showLoadingOverlay = true;
    this._user.GetPermissionUser().subscribe(
      data => {
        this.showLoadingOverlay = false;
        this.viewReports = this._user.RequestPermission(data, 'viewPerformanceActivity');
        if(this.viewReports){
          this.allowAccess=true;
          // add for show "No rows to show"
          //called after the constructor and called  after the first ngOnChanges()
          // let userData=JSON.parse(localStorage.getItem('logedUser'));
          // this.userId = userData['user_id'];
          this.getUserList();
          this.getStatusList();
          this.getListActivity();
        }
        else{
          this.redirectDeny();
        }
      },
      err => {
        this.messages=this._Func.Messages('danger', this._Func.parseErrorMessageFromServer(err));
        this.showLoadingOverlay = false;
      }
    );
  }

  redirectDeny(){
    this._router.parent.navigateByUrl('/deny');
  }


  private onPageSizeChanged($event)
  {
    this.perPage = $event.target.value;
    if(this.Pagination){
      if((this.Pagination['current_page'] - 1) * this.perPage >= this.Pagination['total']) {
        this.Pagination['current_page'] = 1;
      }
      this.getListActivity(this.Pagination['current_page']);
    }

  }


  private getListActivity(page=1){

    let param="?page="+page+"&limit="+this.perPage;
    if (this.userId) param = param+'&cus_id='+this.userId;
    if (this.status) param = param+'&odr_sts='+this.status;
    if (this.viewTime) param = param+'&day='+this.viewTime;

    this.showLoadingOverlay=true;
    this.eventService.getListActivity(this.whs_id, param).subscribe(
      data => {
        this.showLoadingOverlay=false;
        this.listActivity = data.data;
        this.loadDataStatus['list_activity']=true;
        //pagin function
        this.Pagination=data['meta']['pagination'];
        this.Pagination['numLinks']=3;
        this.Pagination['tmpPageCount']=this._Func.Pagination(this.Pagination);
      },
      err => {
        this.parseError(err);
        this.showLoadingOverlay=false;
      },
      () => {}
    );

  }

  getUserList()
  {
    let params='?whs_id=' + this.whs_id + "&limit=10000";
    this.eventService.getCustomersByWH(params).subscribe(
      data => {
        this.userList = data.data;
      },
      err => {
        this.parseError(err);
      },
      () => {}
    );

  }

  getStatusList()
  {
    this.eventService.getStatusList().subscribe(
      data => {
        this.statusList = data.data;
      },
      err => {
        this.parseError(err);
      },
      () => {}
    );

  }

  /**
   * changeTimeView
   * **/
  changeTimeView(time)
  {
    this.viewTime=time;
    this.getListActivity();
  }

  /*
  * changeCustomer
  * */
  changeCustomer(cusId)
  {
    this.userId=cusId;
    this.getListActivity();
  }

  // Show error when server die or else
  private parseError (err) {
    err = err.json();
    this.messages = this._Func.Messages('danger', err.errors.message);
  }

}
