/**
 * Created by admin on 6/10/2016.
 */
import {Component, Injectable} from '@angular/core';
import {Http, Response, JSONP_PROVIDERS} from '@angular/http';
import {API_Config, Functions} from '../common/core/load';

@Component({

})

@Injectable()
export class SystemConfigurationServices {

  private _authHeader = this._Func.AuthHeader();
  private _authHeaderPost = this._Func.AuthHeaderPost();
  private _authHeaderPostJson = this._Func.AuthHeaderPostJson();
  private _apiSystemConfig = this._API.API_SYSTEM_CONFIGURATION;
  constructor(private _Func:Functions, private _API:API_Config, private http:Http){

  }

  getDatabyTabName(tabName){
    let API = this._apiSystemConfig;
    if(tabName === "general"){
      API += 'general';
    }

    if(tabName === "email"){
      API += 'smtp';
    }

    return this.http.get(API, {headers: this._authHeader})
      .map((res:Response) => res.json().data);
  }

  saveDatabyTabName(tabName,dataToUpdate){
    let API = this._apiSystemConfig;
    if(tabName === "general"){
      API += 'general';
      return this.http.post(API, dataToUpdate, {headers: this._authHeaderPostJson});
    }

    if(tabName === "email"){
      API += 'smtp';
      return this.http.post(API, dataToUpdate, {headers: this._authHeaderPost});
    }


  }
}
