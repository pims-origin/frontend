import {Component, ViewChild} from '@angular/core';
import {Router} from '@angular/router-deprecated';
import {UserService} from "../../common/users/users.service";
import {Functions, API_Config} from '../../common/core/load';
import {WMSBreadcrumb, WMSMessages} from '../../common/directives/directives';

declare var $:any;
declare var saveAs:any;

@Component({
	selector: 'customer-migration',
	directives: [WMSBreadcrumb, WMSMessages],
	templateUrl: 'customer-migration.component.html',
})

export class CustomerMigrationComponent {

	@ViewChild('customerFileView') cusFile: any;
	private showLoadingOverlay:boolean = false;
	private canAccess:boolean = false;
	private messages:any;
	private customerFile:any;

	constructor(private userService:UserService,
	            private funcs:Functions,
	            private api:API_Config,
	            private router:Router) {
		this.checkPermission();
	}

	private checkPermission() {
		this.showLoadingOverlay = true;
		this.userService.GetPermissionUser().subscribe(
			data => {
				this.showLoadingOverlay = false;
				this.canAccess = this.userService.RequestPermission(data, 'importCustomer');
				if (this.canAccess) {
				} else {
					// redirect to deny page.
					this.router.parent.navigateByUrl('/deny');
				}
			},
			err => {
				this.messages = this.funcs.Messages('danger', this.funcs.parseErrorMessageFromServer(err));
				this.showLoadingOverlay = false;
			}
		);
	}

	private changeCustomerFile(event) {
		this.customerFile = <Array<File>> event.target.files[0];
	}

	private importFile() {
		const that = this;

		if(!this.customerFile) {
			return;
		}
		that.messages = null;
		let formData:any = new FormData();
		let xhr = new XMLHttpRequest();

		// process file
		const fileExtension = that.customerFile.name.substr(that.customerFile.name.length - 4);
		if (fileExtension !== "xlsx" && fileExtension !== ".csv") {
			that.messages = that.funcs.Messages('danger', that.funcs.msg('CF003'));
			return;
		}
		formData.append("file", that.customerFile, that.customerFile.name);

		that.showLoadingOverlay = true;
		xhr.open("POST", `${that.api.API_MIGRATION}/customers/import`, true);
		xhr.setRequestHeader("Authorization", 'Bearer ' + that.funcs.getToken());
		xhr.onreadystatechange = function() {
			if (xhr.readyState == 2) {
				if (xhr.status == 200) {
					xhr.responseType = "blob";
				} else {
					xhr.responseType = "text";
				}
			}

			if (xhr.readyState == 4) {
				if (xhr.status === 200) {
					that.messages = that.funcs.Messages('danger', 'Import file has some errors.');
					var blob = new Blob([this.response], {type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'});
					saveAs.saveAs(blob, 'Customer_Error.xlsx');
					that.showLoadingOverlay = false;

				} else {
					let msg = '';

					if(xhr.response) {
						try {
							var res = JSON.parse(xhr.response);
							msg = res.errors && res.errors.message ? res.errors.message : res.data.message;
						}
						catch(err){
							msg = xhr.statusText
						}
					}
					else {
						msg = xhr.statusText;
					}

					if(!msg) {
						msg = that.funcs.msg('VR100');
					}

					if (xhr.status === 201) {
						that.messages = that.funcs.Messages('success', msg);
						that.cusFile.nativeElement.value = '';
					} else {
						that.messages = that.funcs.Messages('danger', msg);
					}
					that.showLoadingOverlay = false;
				}
			}
		};
		xhr.send(formData);
	}
}
