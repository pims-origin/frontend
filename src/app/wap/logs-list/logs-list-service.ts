import {Component, Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {Functions, API_Config} from '../../common/core/load';

@Injectable()
export class LogsListService {

  public AuthHeader = this._func.AuthHeader();
  public AuthHeaderPost = this._func.AuthHeaderPost();

  constructor(private _func: Functions, private _http: Http, private _api: API_Config) {}

  getList(params, warehouseId) {
  	return this._http.get(`${this._api.API_COMMON}/logs/logslist/${warehouseId}` + params, {headers: this._func.AuthHeader()})
        .map(res => res.json())	
  }

  getLogType(warehouseId) {
  	return this._http.get(`${this._api.API_COMMON}/logs/type-dropdown/${warehouseId}`, {headers: this._func.AuthHeader()})
        .map(res => res.json());
  }

  getEventCode(warehouseId, param = '') {
    return this._http.get(`${this._api.API_COMMON}/logs/evt-code-dropdown/${warehouseId}${param}` , {headers: this._func.AuthHeader()})
        .map(res => res.json());
  }
}
