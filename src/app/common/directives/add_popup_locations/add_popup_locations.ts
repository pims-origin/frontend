import {Component,Input,Output,EventEmitter,Injectable} from '@angular/core';
import {WMSPagination} from '../../../common/directives/directives';
import {BoxPopupService} from "../../../common/popup/box-popup.service";
import {Functions} from '../../../common/core/load';
import {Services} from './services';
import {SearchPipe}  from './search.pipe';
declare var jQuery: any;
@Injectable()
@Component({
    templateUrl:'template.html',
    styleUrls:['./style.css'],
    selector: 'add-locations-popup',
    directives: [WMSPagination],
    pipes: [SearchPipe],
    providers: [Services,BoxPopupService]
})
export class AddPopupLocations
{

    @Input('data_used') data_used:Array<any>=[];
    @Input('style') style ='default';
    @Input('isdisabled') disabled:boolean = false;
    @Output() selectedLoc  = new EventEmitter();
    private Pagination;
    private perPage=20;
    private locationCodeFilter='';
    private messages:any;
    private locationList={};
    constructor(private _Func:Functions,
                private service:Services) {
        this.getLocation();
    }

    private loading:any;
    private getLocation(page=1){

        this.loading = true;
        var params="?page="+page+"&limit=20&status=AC&loc_code="+this.locationCodeFilter;
        let whs_id = localStorage.getItem('whs_id');
        this.service.getLocationByWH(whs_id,params).subscribe(
            data => {

                this.loading = false;
                this.buildList(data);
                if(typeof  data['meta']!=='undefined'){
                    this.Pagination=data['meta']['pagination'];
                    this.Pagination['tmpPageCount']=this._Func.Pagination(this.Pagination);
                }

            },
            err => {
                this.parseError(err);
                this.loading = false;
            },
            () => {}
        );

    }

    private selectedAllPage:any;
    private checkedItem(item,$event){
        this._Func.checkedItem(this.locationList,item,$event);
    }

    private checkedAll($event){
        this._Func.checkedAll(this.locationList,$event);
    }

    private buildList(data){
        this.locationList =  data;
    }

    private add(){

        let _selectedLoc:Array<any>=[];
        let tmp:Array<any> = JSON.parse(JSON.stringify(this.locationList['data']));
        tmp.forEach((item)=>{
            if(item['selected']){
                item['selected'] = false;
                _selectedLoc.push(item);
            }
        });

        if(_selectedLoc.length){
            this.selectedLoc.emit(_selectedLoc);
        }

    }

    // Show error when server die or else
    private parseError(err) {
        this.messages = {'status': 'danger', 'txt': this._Func.parseErrorMessageFromServer(err)};
    }

    private setClickedRow(item) {
        item.selected = !item.selected;
    }

}
