
import {Component} from '@angular/core';
import {API_Config} from '../../common/core/API_Config';
import {BaseTable} from '../../common/core/classes_us/base-table';
import {
    AdvanceTable,
    Searches,
    TableBlock
} from '../../common/directives/directives';
import {DateButtonsComponent} from './date-buttons/date-buttons.component';
import {TableFunctions} from '../../common/core/table-functions';
import {DateFunctions} from '../../common/core/functions/date-functions';
import {BaseService} from '../../common/core/classes_us/base-service';
import {SearchesService} from '../../common/directives/table-structure/searches/searches-service';
import {CustomersService} from '../../master-data/customers/customers-service';

declare var jQuery: any;

@Component ({
    selector: 'open-orders',
    directives: [
        AdvanceTable,
        Searches,
        TableBlock,
        DateButtonsComponent
    ],
    templateUrl: 'open-orders.component.html',
    providers: [
        TableFunctions,
        DateFunctions,
        BaseService,
        SearchesService,
        CustomersService
    ]
})

export class OpenOrdersComponent extends BaseTable
{
    public tableID = 'open-orders';
    public url = this._api.API_OPEN_ORDER_REPORTS;
    public headerUrlSuffix = 'rep';
    public title = 'Search Open Orders';
    public permissionPage = 'viewReport';

    public headerDef = [
        {
            id: 'ver_table',
            value: 1 + Math.random() * 10000
        },
        {
            id: 'whs_name',
            name: 'WHS',
            width: 120,
            unsortable: true,
            cellClassRules: this.setRowColor()
        },
        {
            id: 'cus_name',
            name: 'CLNT NM',
            width: 150,
            unsortable: true,
            cellClassRules: this.setRowColor()
        },
        {
            id: 'odr_num',
            name: 'ORD',
            width: 160,
            unsortable: true,
            cellClassRules: this.setRowColor()
        },
        {
            id: 'cus_odr_num',
            name: 'CUST ORD',
            width: 160,
            unsortable: true,
            cellClassRules: this.setRowColor()
        },
        {
            id: 'upd_sts',
            name: 'STS',
            width: 100,
            unsortable: true,
            cellRenderer: this.updateStatusCellRenderer,
            cellEditor: 'richSelect',
            cellEditorParams: {
                cellRenderer: this.updateStatusEditCellRenderer,
                values: ['Open', 'Closed']
            },
            editable: true,
            cellClassRules: this.setRowColor()
        },
        {
            id: 'upd_user',
            name: 'UPD DATE',
            width: 100,
            unsortable: true,
            cellClassRules: this.setRowColor()
        },
        {
            id: 'upd_date',
            name: 'UPD CSR',
            width: 100,
            unsortable: true,
            cellClassRules: this.setRowColor()
        },
        {
            id: 'odr_type',
            name: 'ORD Type',
            width: 100,
            unsortable: true,
            cellClassRules: this.setRowColor()
        },
        {
            id: 'user_name',
            name: 'CSR Name',
            width: 100,
            unsortable: true,
            cellClassRules: this.setRowColor()
        },
        {
            id: 'created_at',
            name: 'Create DT',
            width: 100,
            unsortable: true,
            cellClassRules: this.setRowColor()
        },
        {
            id: 'sku_count',
            name: 'SKU',
            width: 80,
            unsortable: true,
            cellClassRules: this.setRowColor()
        },
        {
            id: 'plt_count',
            name: 'PLT',
            width: 80,
            unsortable: true,
            cellClassRules: this.setRowColor()
        },
        {
            id: 'ctn_ttl',
            name: 'CTN',
            width: 80,
            unsortable: true,
            cellClassRules: this.setRowColor()
        },
        {
            id: 'pcs_ttl',
            name: 'PCS',
            width: 80,
            unsortable: true,
            cellClassRules: this.setRowColor()
        },
        {
            id: 'cancel_by_dt',
            name: 'Cancel DT',
            width: 100,
            unsortable: true,
            cellClassRules: this.setRowColor()
        },
        {
            id: 'ord_id',
            name: '',
            width: 0,
            hide: true
        }
    ];

    public objSort = {
        sort_field: 'cancel_by_dt',
        sort_type: 'desc'
    };

    public searches = [
        this.searchList.customer,
        this.searchList.orderNumber,
        this.searchList.customerOrderNumber,
        this.searchList.createdAt
    ];

    /*
    ****************************************************************************
    */

    constructor (
        private _api: API_Config,
        private _tableFunc: TableFunctions,
        private _dateFunctions: DateFunctions,
        public _tableService: BaseService,
        private _searchesService: SearchesService,
        private _customersService: CustomersService
    )
    {
        super();

        this._tableFunc.permissionCall(this);
    }

    //**************************************************************************

    public onCellValueChanged($event)
    {
        let newValue = $event.newValue,
            rowNo = $event.node.id;

        let loggedUser = localStorage.getItem('logedUser');

        let parsed = JSON.parse(loggedUser);

        let orderID = this.rowData[rowNo].ord_id,
            userID = parsed.user_id,
            params = JSON.stringify({
                value: newValue
            });

        let url = this._api.API_OPEN_ORDER_REPORTS + '/update/' + orderID
                + '/' + userID;

        this.showLoadingOverlay = true;

        this._tableService.post(url, params)
            .subscribe(
                data => {
                    this.updateStatusCallback(parsed, rowNo);
                },
                err => {
                    this.messages = this._tableFunc.parseError(err);
                },
                () => {
                    this.showLoadingOverlay = false;
                }
        );
    }

    /*
    ****************************************************************************
    */

    private updateStatusCellRenderer(params)
    {
        return params.data.upd_sts;
    }

    /*
    ****************************************************************************
    */

    private updateStatusEditCellRenderer(params)
    {
        return params.value;
    }

    /*
    ****************************************************************************
    */

    private updateStatusCallback(parsed, rowNo)
    {
        let today = new Date();

        let name = parsed.first_name + ' ' + parsed.last_name,
            date = this._dateFunctions.getDateString(today);

        jQuery('#open-orders [colid="upd_user"]').eq(rowNo + 1).html(name);
        jQuery('#open-orders [colid="upd_date"]').eq(rowNo + 1).html(date);
    }

    /*
    ****************************************************************************
    */

    private setRowColor()
    {
        let returnValue = {};

        for (let count=0; count<7; count++) {

            let date = this.getRowColor(count);

            returnValue['day' + count] =
                'data.cancel_by_dt == "' + date + '" && x != "Click to edit"';

            returnValue['day' + count + '-click-to-edit'] =
                'data.cancel_by_dt == "' + date + '" && x == "Click to edit"';
        }

        return returnValue;
    }

    /*
    ****************************************************************************
    */

    private getRowColor(dayNo)
    {
        let time = new Date().getTime();

        let reportDate = new Date(time + dayNo * 24 * 60 * 60 * 1000);

        return this._dateFunctions.getDateString(reportDate);
    }

    /*
    ****************************************************************************
    */

}
