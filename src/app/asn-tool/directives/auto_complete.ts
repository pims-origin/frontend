/*
* @tien.nguyen
* */
import {Component} from '@angular/core';
import { ControlGroup, ControlArray, Control} from '@angular/common';
import {FormBuilderFunctions} from '../../common/core/formbuilder.functions';
import {Functions} from '../../common/core/load';
declare var jQuery: any;
@Component ({
    providers: []
})
export class AutoCompleteProvider {
    constructor(private fbdFun:FormBuilderFunctions,private _globalFun:Functions) {
    }


    /*==================================
     * Check Key exist
     *================================= */
    public checkKeyExist(controlArray:ControlArray, row, str:string, arr:Array<any>, fieldname) {
        let string = str;
        let exist;
        for (let i in arr) {
            let item = arr[i];
            if (string == this._globalFun.trim(item[fieldname])) {
                this.selectedItem(controlArray,row, item);
                return;
            }
        }
        this.fbdFun.setErrorsControlItem(controlArray, row, fieldname, {invaildValue: true});
    }


    /*========================================
     * Auto select item if match
     * ======================================*/
    public selectedItem(controlArray:ControlArray, row, dataItem) {
        let keys = [
            {control: 'dtl_itm_id', key_value: dataItem['item_id']},
            {control: 'asn_dtl_id', key_value: null},
            {control: 'dtl_sku', key_value: dataItem['sku']},
            {control: 'dtl_size', key_value: dataItem['size']},
            {control: 'dtl_color', key_value: dataItem['color']},
            {control: 'dtl_pack', key_value: dataItem['pack']},
            {control: 'dtl_ctn_ttl', key_value: dataItem['uom']},
            {control: 'dtl_length', key_value: dataItem['length']},
            {control: 'dtl_width', key_value: dataItem['width']},
            {control: 'dtl_height', key_value: dataItem['height']},
            {control: 'dtl_weight', key_value: dataItem['weight']},
            {control: 'dtl_po_date', key_value: dataItem['created_at']['date']},
        ];

        for (let i in keys){
            this.fbdFun.setValueControlItem(controlArray, row, keys[i]['control'], keys[i]['key_value']);
        }

    }
    // /*=====================================
    //  * Select item recheck sku
    //  * ==================================*/
    // relectRecheckSku(controlArray:ControlArray,row,dataItem){
    //
    //     // alway update
    //     this.fbdFun.setValueControlItem(controlArray,row,'dtl_itm_id',dataItem['item_id']);
    //     this.fbdFun.setValueControlItem(controlArray,row,'dtl_sku',dataItem['sku']);
    //     this.fbdFun.setValueControlItem(controlArray,row,'dtl_size',dataItem['size']);
    //     this.fbdFun.setValueControlItem(controlArray,row,'dtl_color',dataItem['color']);
    //
    //     if(!this.fbdFun.getValueControlItem(controlArray,row,'dtl_pack')){
    //         this.fbdFun.setValueControlItem(controlArray,row,'dtl_pack',dataItem['pack']);
    //     }
    //     if(!this.fbdFun.getValueControlItem(controlArray,row,'dtl_ctn_ttl')){
    //         this.fbdFun.setValueControlItem(controlArray,row,'dtl_ctn_ttl',dataItem['uom']);
    //     }
    //     if(!this.fbdFun.getValueControlItem(controlArray,row,'dtl_length')){
    //         this.fbdFun.setValueControlItem(controlArray,row,'dtl_length',dataItem['length']);
    //     }
    //     if(!this.fbdFun.getValueControlItem(controlArray,row,'dtl_width')){
    //         this.fbdFun.setValueControlItem(controlArray,row,'dtl_width',dataItem['width']);
    //     }
    //     if(!this.fbdFun.getValueControlItem(controlArray,row,'dtl_height')){
    //         this.fbdFun.setValueControlItem(controlArray,row,'dtl_height',dataItem['height']);
    //     }
    //     if(!this.fbdFun.getValueControlItem(controlArray,row,'dtl_weight')){
    //         this.fbdFun.setValueControlItem(controlArray,row,'dtl_weight',dataItem['weight']);
    //     }
    //
    // }

}


