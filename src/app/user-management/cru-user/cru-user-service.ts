import {Component, Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {Functions, API_Config} from '../../common/core/load';

@Injectable()
export class CRUUserService {

  public AuthHeader = this._Func.AuthHeader();
  public AuthHeaderPost = this._Func.AuthHeaderPost();
  public API_URL_SETUP_PASSWORD = this._api.API_URL_SETUP_PASSWORD;

  constructor(
    private _Func: Functions,
    private http: Http,
    private _api: API_Config) {
  }

  addUser(stringData) {
    return this.http.post(this._api.API_MASTER_URL + '/users', stringData, { headers: this._Func.AuthHeaderPostJson() })
      .map((res: Response) => res.json().data);
  }

  getLogedUserRoles() {
  	return this.http.get(this._api.API_MASTER_URL + '/users/roles',{ headers: this.AuthHeader })
      .map((res: Response) => res.json());
  }

  getAllWarehouse() {
    return this.http.get(this._api.API_WAREHOUSE_URL + '/warehouses?limit=1000',{ headers: this.AuthHeader })
      .map((res: Response) => res.json());
  }

  getCustomerByWarehouse(warehouseID) {
    return this.http.get(this._api.API_Customers + '?whs_id='+ warehouseID +'&limit=10000',{ headers: this.AuthHeader })
      .map((res: Response) => res.json());
  }

  getUserDetail(userId) {
    return this.http.get(this._api.API_User_Info+'/' +userId, { headers: this.AuthHeader })
      .map((res: Response) => res.json().data);
  }

  updateUser(userId, stringData) {
    return this.http.put(this._api.API_MASTER_URL+'/users/'+userId, stringData, { headers: this._Func.AuthHeaderPostJson() })
      .map((res: Response) => res.json().data);
  }

  getAllWarehouse_customer_role() {
    return this.http.get(this._api.API_User_Info+'/', { headers: this.AuthHeader })
      .map((res: Response) => res.json().data);
  }

  getDepartmentInfo(){
    return this.http.get(this._api.API_Authen +'/department', { headers: this.AuthHeader })
      .map((res: Response) => res.json().data);
  }

  getDepartmentInfo2(){
    return this.http.get(this._api.API_USER_DEPARTMENT , { headers: this.AuthHeader })
        .map((res: Response) => res.json().data);
  }
}
