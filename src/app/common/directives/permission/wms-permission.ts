import {Component,Input,Directive, Output, EventEmitter, ElementRef} from '@angular/core';
import {Functions,API_Config} from '../../../common/core/load';
import {Http, Response} from '@angular/http';
import { Router, RouterLink, ROUTER_DIRECTIVES } from '@angular/router-deprecated';
@Component ({
  selector: 'wms-permission',
  directives:[],
  providers: [],
  template: '',
})
export class WMSPermission {

  @Output() userPermission = new EventEmitter();


  constructor(private _Func: Functions, private _router: Router, private _API: API_Config, private http: Http)
  {
    this.getPermission();
  }

  private getPermission() {
    this.GetPermissionUser().subscribe(
      data => {
        let datax=data;
        this.userPermission.emit(datax);
      },
      err => {
      },
      () => {
      }
    );
  }

  GetPermissionUser()
  {
    let ListPermissionUser=null;
    return this.http.get(this._API.API_USER_Permission, { headers: this._Func.AuthHeader() })
      .map((res: Response) => res.json().data);

  }

}
