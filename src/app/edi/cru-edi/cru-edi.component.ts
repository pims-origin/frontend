import {Component} from '@angular/core';
import { FORM_DIRECTIVES,ControlGroup,FormBuilder, Control,Validators} from '@angular/common';
import {WMSBreadcrumb,DocumentUpload,WMSMessages} from '../../common/directives/directives';
import {Router, RouteData, RouteParams } from '@angular/router-deprecated';
import {BoxPopupService} from "../../common/popup/box-popup.service";
import {EDIServices} from "./edi-services";
import {GoBackComponent} from "../../common/component/goBack.component";
import {WarningBoxPopup} from "../../common/popup/warning-box-popup";
import { ValidationService } from '../../common/core/validator';
import {UserService, Functions} from '../../common/core/load';
declare var jQuery: any;
@Component ({
    selector: 'work-order-list',
    directives:[WMSBreadcrumb,GoBackComponent,DocumentUpload,FORM_DIRECTIVES,WMSMessages],
    providers: [BoxPopupService,EDIServices,ValidationService,UserService],
    templateUrl: 'cru-edi.component.html'
})

export class CruEDIComponent {

    EDIForm: ControlGroup;
    private action = 'new';
    private title = 'Create';
    private messages;
    private showLoadingOverlay=false;
    private TitlePage="New EDI";
    private IsView=false;
    private Action;
    private skusArray:Array<any>=[];
    private chargecodesArray:Array<any>=[];
    private Customers:Array<any>=[];
    private whs_id;
    private submitForm=false;
    private selectedAllChargeCode=false;
    private selectedAllItems=false;
    private cus_id;
    private loading={};
    private workOrderDetail;
    private EDI_ID;
    private wareHouseList:Array<any>=[];
    private EDILIST:Array<any>=[];

    constructor(private _router: Router,
                private _boxPopupService:BoxPopupService,
                _RTAction: RouteData,
                private _user:UserService,
                private fb: FormBuilder,
                private _Valid: ValidationService,
                private _Func: Functions,
                private _EDIService:EDIServices,
                private params: RouteParams) {

        this.Action=_RTAction.get('action');
        this.whs_id=this._Func.lstGetItem('whs_id');
        this.checkPermission();

    }

    // Check permission for user using this function page
    private newEDIT;
    private viewWOrder;
    private editWOrder;
    private allowAccess:boolean=false;
    checkPermission(){

        this.showLoadingOverlay = true;
        this._user.GetPermissionUser().subscribe(
            data => {

                this.showLoadingOverlay = false;
                this.newEDIT = this._user.RequestPermission(data, 'viewWorkOrder');
                this.viewWOrder = this._user.RequestPermission(data,'viewWorkOrder');
                this.editWOrder = this._user.RequestPermission(data,'editWorkOrder');

                this.get_wareHouseList();

                /*
                 * Action router View detail
                 * */
                if(this.Action=='view')
                {
                    if(this.viewWOrder) {
                        this.IsView = true;
                        this.TitlePage = 'View EDI';
                        this.allowAccess=true;
                    }
                    else{
                        this.redirectDeny();
                    }
                }

                // Action new
                if(this.Action=='new')
                {
                    if(this.newEDIT) {
                        this.allowAccess=true;
                        this.TitlePage = 'New EDI';
                        this.formBuilder();
                        this.getEDIListDefault();
                    }
                    else{
                        this.redirectDeny();
                    }
                }

                // load detail / init data form
                if(this.Action=='edit'||this.Action=='view')
                {
                    this.getEDIDetail();
                }

                /*
                 * Action router Edit
                 * */
                if(this.Action=='edit')
                {
                    if(this.editWOrder) {
                        this.allowAccess=true;
                        this.TitlePage = 'Edit EDI';
                    }
                    else{
                        this.redirectDeny();
                    }
                }

            },
            err => {
                this.parseError(err);
                this.showLoadingOverlay = false;
            }
        );
    }

    redirectDeny(){
        this._router.parent.navigateByUrl('/deny');
    }


    /*======================
    * wareHouseList
    *====================== */
    get_wareHouseList(){

        this._EDIService.getWHSList().subscribe(
            data => {
                this.wareHouseList=data.data;
            },
            err => {
                this.parseError(err);
            },
            () => {

            }
        );
        
    }

    /*======================
     * getCustomersByWH
     *====================== */
    private getCustomersByWH (whId) {

        let params='?whs_id=' + whId + "&limit=10000";
        this.showLoadingOverlay = true;
        this._EDIService.getCustomersByWH(params)
            .subscribe(
                data => {
                    this.showLoadingOverlay = false;
                    this.Customers = data.data;
                },
                err => {
                    this.showLoadingOverlay = false;
                    this.parseError(err);
                },
                () => {
                }
            );

    }

    /*=========================
    * changeWHS
    * =======================*/
    changeWHS($event){

        let curentValue=this.EDIForm.value['whs_id'];
        if(curentValue){
            let that = this;
            let warningPopup = new WarningBoxPopup();
            warningPopup.text = this._Func.msg('M043');
            this._boxPopupService.showWarningPopup(warningPopup)
                .then(function (dm) {
                    if(dm) {
                        that.getCustomersByWH($event.target.value);
                    }
                    else{
                        that.updateValueControl('whs_id',curentValue);
                    }
                });
        }else{
            if($event.target.value){
                this.Customers=[];
                this.getCustomersByWH($event.target.value);
            }
        }

    }

    /*============
    * changeCustomer
    ============*/
    changeCustomer($event){



    }

    formBuilder(data={}){

        this.EDIForm =this.fb.group({
            cus_id: [data['cus_id'] ? data['cus_id'] : '',Validators.required],
            whs_id:[data['whs_id'] ? data['whs_id'] : '',Validators.compose([Validators.required])],
            details:['']
        });
         // auto get cus list if that is view page
        if(data['whs_id']){
            this.getCustomersByWH(data['whs_id']);
        }

    }


    getEDIDetail(){

        this.EDI_ID=this.params.get('id');
        /*this._EDIService.getEDI_Detail(this.EDI_ID).subscribe(
            data => {
                this.formBuilder(data);
                this.EDILIST=data['details'];
            },
            err => {
                this.parseError(err);
            },
            () => console.log('Get detail work order Complete')
        );*/

        let dataPre={
            cus_id:16,
            whs_id:2
        };
        this.formBuilder(dataPre);
        this.getEDIListDefault();

    }

    getEDIListDefault(){
       this.EDILIST=this._EDIService.EDITList();
    }


    Save(){

        this.submitForm=true;

        // empty messages
        this.messages=false;

        // scroll to top
        jQuery('html, body').animate({
            scrollTop: 0
        }, 700);


        if(this.EDIForm.valid){

            let data=this.EDIForm.value;
            data['details']=this.EDILIST;
            let data_json = JSON.stringify(data);

            if (this.Action == 'new') {
                this._newEDIT(data_json);
            }
            else {
                this.Update(data_json);
            }

        }

    }

    _newEDIT(data){
        let n=this;
        this.showLoadingOverlay=true;
        this._EDIService.updateEDIT(data).subscribe(
            data => {
                this.showLoadingOverlay = false;
                //Reset Form
                this.messages = this._Func.Messages('success', this._Func.msg('VR107'));
                setTimeout(function(){
                    n._router.parent.navigateByUrl('/edi');
                }, 600);
            },
            err => {
                this.showLoadingOverlay= false;
                this.parseError(err);
            },
            () => {}
        );
    }

    Update(data){

        //
        let n=this;
        this.showLoadingOverlay=true;
        this._EDIService.updateEDIT(data).subscribe(
            data => {
                this.showLoadingOverlay=false;
                this.messages = this._Func.Messages('success', this._Func.msg('VR109'));
                setTimeout(function(){
                    n._router.parent.navigateByUrl('/edi');
                }, 600);
            },
            err => {
                this.showLoadingOverlay=false;
                this.parseError(err);
            },
            () => {
                
            }
        );

    }
    
    /*=======================
    * checkEDITValue 
    *===================== */
    checkEDITValue($event,item){
        if($event.target.checked){
            item['value']=1;
        }else{
            item['value']=0;
        }
    }

    /*
     * SetValue Row
     * */
    updateValueControl(control,value)
    {
        (<Control>this.EDIForm['controls'][control]).updateValue(value);
    }


    // Show error when server die or else
    private parseError (err) {
        this.messages = {'status' : 'danger', 'txt' : this._Func.parseErrorMessageFromServer(err)};
    }

    redirectToList(){
        let n = this;
        /*case 2: default*/
        this._boxPopupService.showWarningPopup()
            .then(function (dm) {
                if(dm)
                    n._router.parent.navigateByUrl('/edi');
            })
    }

}
