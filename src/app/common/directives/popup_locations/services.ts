import {Component, Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {API_Config, Functions} from '../../../common/core/load';

@Injectable()
export class Services {

    public AuthHeader = this._Func.AuthHeader();
    public AuthHeaderPost = this._Func.AuthHeaderPostJson();

    constructor(private _Func: Functions, private _API: API_Config, private http: Http) {}

    getLocationByWH(whsId, param) {
      return this.http.get(this._API.API_Warehouse + "/" + whsId + "/locations" + param, {headers: this.AuthHeader})
        .map((res:Response) => res.json());
    }
    
    getAutocompleteLocation(params:string = '') {
      return this.http.get(`${this._API.API_REPORTS_CORE}/auto-complete` + params, {headers: this.AuthHeader})
        .map(res => res.json());
    }
}
