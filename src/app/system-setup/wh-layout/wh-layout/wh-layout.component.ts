import { Component,ElementRef, OnInit } from "@angular/core";
import { Http, Headers } from "@angular/http";
import { Router, RouterLink, ROUTER_DIRECTIVES } from '@angular/router-deprecated';
import { ControlGroup, FormBuilder, Control, CORE_DIRECTIVES, FORM_DIRECTIVES } from '@angular/common';
import { API_Config } from "../../../common/core/API_Config";
import { UserService } from "../../../common/users/users.service";
import { Functions } from "../../../common/core/functions";
import { WMSBreadcrumb, WMSMessages, WMSPagination } from '../../../common/directives/directives';
import { WarehouseLayoutDirective } from './layout';
import { WarehouseLayoutServices } from '../wh-layout-service';
import { TOOLTIP_DIRECTIVES } from "ng2-bootstrap/ng2-bootstrap";

declare var jQuery: any;

@Component({
    selector: 'wh-layout',
    directives: [
      TOOLTIP_DIRECTIVES,WarehouseLayoutDirective, CORE_DIRECTIVES, FORM_DIRECTIVES, WMSMessages,
      WMSBreadcrumb, WMSPagination
    ],
    providers : [WarehouseLayoutServices],
    templateUrl: 'wh-layout.component.html',
    styleUrls:['./wh-layout.component.scss']
})
export class WarehouseLayout{

  private messages;
  private showLoadingOverlay = false;
  private hasWarehouseGraphicPermission = false;
  private warehouseGraphics= [];
  private listCustomer = [];
  private listCustomerZone = [];
  private listZone = [];
  private locationTypes = [];
  private closeListCustomer = false;
  private Loading = {};
  private activeCustomer = 0;

  // Search information
  private searchForm: ControlGroup;
  private isLoadingSKUs: Boolean = false;
  private skus: Array<string> = [];
  private isLoadingLocations: Boolean = false;
  private locations: any[] = [];
  private code: string = '';
  private type: string = '';
  // pagination
  perPage = 20;
  pagination:any;

  constructor (
    private elementRef: ElementRef,
    private _http: Http,
    private _router: Router,
    private _user: UserService,
    private _func: Functions,
    private _API_Config: API_Config,
    private _warehouseLayoutServices: WarehouseLayoutServices,
    private formBuilder:FormBuilder,
  ) {
    this.buildSearchForm();
  	this.checkPermission();
    this.initToggleZone();
  }

  private buildSearchForm() {
    this.searchForm = this.formBuilder.group({
      loc_code: [''],
      loc_type_code: [''],
      sku: [''],
      lot: [''],
      spc_hdl_code: ['']
    });
  }

  private search() {
    this.getWarehouseLayout(1);
  }

  private reset() {
      this.resetSearchForm();
      this.resetRigthFilter();
      this.getWarehouseLayout(1);
  }

  private resetRigthFilter() {
    this.code = '';
    this.type = '';
  }

  private resetSearchForm() {
    (<Control>this.searchForm.controls['loc_code']).updateValue('');
    (<Control>this.searchForm.controls['loc_type_code']).updateValue('');
    (<Control>this.searchForm.controls['sku']).updateValue('');
    (<Control>this.searchForm.controls['lot']).updateValue('');
    (<Control>this.searchForm.controls['spc_hdl_code']).updateValue('');
  }

  private loadLocations($event: any = {}) {
    const locKeySearch = this.searchForm.value['loc_code'].trim();
    const params = `?loc_code[]=${locKeySearch}&sort[loc_code]=asc`;
    const enCodeSearchQuery = this._func.FixEncodeURI(params);

    if (!locKeySearch) {
        return;
    }


    this.isLoadingLocations = true;
    this._warehouseLayoutServices.getLocations(enCodeSearchQuery).subscribe(
        data => {
            this.locations = data['data'];
            this.isLoadingLocations = false;
        },
        err => {
            this.showMessage('danger', this._func.parseErrorMessageFromServer(err));
            this.isLoadingLocations = false;
        }
    )
  }

  private selectLocation($event: any = {}, locCode: string = '') {
      (<Control>this.searchForm.controls['loc_code']).updateValue(locCode);
  }

  public loadSKUs($event: any) {
    const whsId = localStorage.getItem('whs_id');
    const skuKeySearch = this.searchForm.value['sku'].trim();
    const params = `/${whsId}?sku=${skuKeySearch}&limit=20`;
    const enCodeSearchQuery = this._func.FixEncodeURI(params);
    // reset LOT
    (<Control>this.searchForm.controls['lot']).updateValue('');

    if (!skuKeySearch) {
        return;
    }
    this.isLoadingSKUs = true;
    this._warehouseLayoutServices.getSKUs(enCodeSearchQuery).debounceTime(400).distinctUntilChanged().subscribe(
        data => {
            this.isLoadingSKUs = false;
            this.skus = data['data'];
        },
        err => {
            this.isLoadingSKUs = false;
            this.messages = this._func.Messages('danger', err.json().errors.message);
        },
        () => {
        }
    );
  }

  public selectSKU($event, sku) {
    (<Control>this.searchForm.controls['sku']).updateValue(sku.sku);
    (<Control>this.searchForm.controls['lot']).updateValue(sku.lot);
  }

  private showMessage(msgStatus, msg) {
      this.messages = {
          'status': msgStatus,
          'txt': msg
      };
      jQuery(window).scrollTop(0);
  }

  private checkPermission() {
    this.showLoadingOverlay = true;
    this._user.GetPermissionUser().subscribe(
      data => {

        this.showLoadingOverlay = false;
        this.hasWarehouseGraphicPermission = this._user.RequestPermission(data,'viewWarehouseLayout');

        if(!this.hasWarehouseGraphicPermission) {
            this._router.parent.navigateByUrl('/deny');
        }
        else {
          this.getCustomers();
          this.getZones();
          this.getLocationTypes();
          this.getWarehouseLayout();
        }

      },
      err => {
        this.showMessage('danger', this._func.parseErrorMessageFromServer(err));
        this.showLoadingOverlay = false;
      }
    );
  }

  private wh_layout:Array<any>=[];
  private getWarehouseLayout($page = 1) {
    this.searchForm.value['code'] = this.code;
    this.searchForm.value['type'] = this.type;
    this.searchForm.value['page'] = $page;
    this.searchForm.value['limit'] = this.perPage;
    this.showLoadingOverlay = true;
    this._warehouseLayoutServices.getWarehouseLayout(`?` + jQuery.param(this.searchForm.value)).subscribe(
      data => {
        this.showLoadingOverlay = false;
        // if(renderCustomer) {
        //   this.customerWH(data.data['customer']);
        //   this.listCustomerZone = data.data['zone'];
        //   this.addOtherZoneForZone(data.data['spcZone']);
        // }
        this.wh_layout=data.data['layout'];
        // if(!this.wh_layout.length) {
        //   this.messages=this._func.Messages('danger',this._func.msg('WHLO001'));
        // }
        // paging
        this.pagination = data['data']['meta']['pagination'];
        this.pagination['numLinks'] = 2;
        this.pagination['tmpPageCount'] = this._func.Pagination(this.pagination);
      },
      err => {
        this.showMessage('danger', this._func.parseErrorMessageFromServer(err));
        this.showLoadingOverlay = false;
      },
      ()=>{
      }
    );

  }

  customerWH(customer:Array<any>){
    this.listCustomer = [];
    for(let key in customer) {
      this.listCustomer.push(customer[key]);
    }
  }


  private filterBy = 'customer';
  private showFilterBy = 'customer';
  filter(cusID, page = 1) {
    this.filterBy = 'customer';
    this.wh_layout = [];
    this.warehouseGraphics = [];
    this.showLoadingOverlay = true;
    this.messages = null;
    this.code = cusID;
    this.type = '';
    const param = '&limit=' + this.perPage + '&page=' + page;

    this.resetSearchForm();

    if(cusID != '') {
      this._warehouseLayoutServices.filterWarehouse(cusID, param).subscribe(
        data => {
          this.showLoadingOverlay = false;
          this.wh_layout=data.data['layout'];
          // paging
          this.pagination = data['data']['meta']['pagination'];
          this.pagination['numLinks'] = 2;
          this.pagination['tmpPageCount'] = this._func.Pagination(this.pagination);
        },
        err => {
          this.showMessage('danger', this._func.parseErrorMessageFromServer(err));
          this.showLoadingOverlay = false;
        }
      );
    }
    else {
      this.getWarehouseLayout(1);
    }
  }

  filterZone(zoneID: string = '', page = 1) {
    this.filterBy = 'zone';
    this.activeCustomer = -1;
    this.wh_layout = [];
    this.warehouseGraphics = [];
    this.showLoadingOverlay = true;
    this.messages = null;
    this.code = zoneID;
    this.type = 'zone';
    const param = '&limit=' + this.perPage + '&page=' + page;

    this.resetSearchForm();

    this._warehouseLayoutServices.filterZone(zoneID, param).subscribe(
      data => {
        this.showLoadingOverlay = false;
        this.wh_layout=data.data['layout'];
        // paging
        this.pagination = data['data']['meta']['pagination'];
        this.pagination['numLinks'] = 2;
        this.pagination['tmpPageCount'] = this._func.Pagination(this.pagination);
      },
      err => {
        this.showMessage('danger', this._func.parseErrorMessageFromServer(err));
        this.showLoadingOverlay = false;
      }
    );
  }

  initToggleZone() {
    jQuery('body').off('click.toggleZone', '.zone-content .customer-name').on('click.toggleZone', '.zone-content .customer-name', function() {
      jQuery(this).closest('li').toggleClass('open');
    });

    jQuery('body').on('click.activeZone', '.zone-content ul li, .zone-content .all', function() {
      jQuery('.zone-content li.active').removeClass('active');
      jQuery(this).addClass('active');
    });
  }

  private aisleID;
  private locationDetail;
  private locationName;
  private showDetail(locID, locName) {
    this.locationName = locName;
    this.showLoadingOverlay = true;
    this._warehouseLayoutServices.getLocationDetail(locID).subscribe(
      data => {
        this.locationDetail = data.data.items;
        jQuery('#location-detail').modal('show');
        this.showLoadingOverlay = false;
      },
      err => {
        this.showMessage('danger', this._func.parseErrorMessageFromServer(err));
        this.showLoadingOverlay = false;
      }
    );
  }

  private addOtherZoneForZone($otherZone = []) {

    var item = {};
    item['code'] =  "Other Zone";
    item['name'] =  "OtherZone";
    item['zones'] =  [];
    for (var i  = 0; i < $otherZone.length; i++) {


        item['zones'].push({
            zone_id: $otherZone[i]['id'],
            zone_code: $otherZone[i]['code'],
            zone_name: $otherZone[i]['name'],
            zone_color: $otherZone[i]['color']
        });
    }

    if ($otherZone.length > 0) {

        this.listCustomerZone.push(item);
    }
  }

  private getCustomers() {
    this._warehouseLayoutServices.getCustomers().subscribe(
      data => {
        this.customerWH(data.data['customer']);
      },
      err => {
        this.showMessage('danger', this._func.parseErrorMessageFromServer(err));
      }
    );
  }

  private getZones() {
    this._warehouseLayoutServices.getZones().subscribe(
      data => {
        this.listCustomerZone = data.data['zone'];
        this.addOtherZoneForZone(data.data['spcZone']);
      },
      err => {
        this.showMessage('danger', this._func.parseErrorMessageFromServer(err));
      }
    );
  }

  private getLocationTypes() {
    this._warehouseLayoutServices.getLocationTypes().subscribe(
      data => {
        this.locationTypes = data['data'];
      },
      err => {
        this.showMessage('danger', this._func.parseErrorMessageFromServer(err));
      }
    );
  }
}
