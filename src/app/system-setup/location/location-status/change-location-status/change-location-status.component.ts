import { Component, Input, OnChanges, SimpleChange, Output, EventEmitter} from '@angular/core';
import { FORM_DIRECTIVES,ControlGroup,FormBuilder, Control ,Validators} from '@angular/common';
import { Http } from '@angular/http';
import { Router, RouterLink, ROUTER_DIRECTIVES } from '@angular/router-deprecated';
import 'ag-grid-enterprise/main';
import 'rxjs/add/operator/map';
import {API_Config} from "../../../../common/core/API_Config";
import {UserService} from "../../../../common/users/users.service";
import {Functions} from "../../../../common/core/functions";
import {JwtHelper} from "angular2-jwt/angular2-jwt";
import {DateFormat} from "../date.pipe";
import {WMSMessages} from "../../../../common/directives/messages/messages";
declare var jQuery: any;

@Component ({
  selector: 'change-location-status',
  providers: [FormBuilder],
  directives: [WMSMessages],
  pipes: [DateFormat],
  templateUrl: 'change-location-status.component.html',
})

export class ChangeLocationStatusComponent {

  private headerGet: any;
  private headerPost: any;
  private headerPostJson: any;
  private urlAPILocation: any;
  private urlAPILocationType: any;
  private urlAPIZoneType: any;
  private urlWarehouse: any;
  private loc_whs_id: any;
  @Input('dataItemLoca') dataItemLoca: any;
  @Input('messagesChangeStatus') messagesChangeStatus: any;
  private listNull = {};
  private flagSubmit = false;
  private messages = {};
  private userCurrent = "";
  @Output() saveChangeStatus = new EventEmitter();
  private status = {
    'AC': 'Active',
    'IA': 'InActive',
    'LK': 'Locked',
    'RS': 'Reserved',

  };
  private fromDate;
  private dataReasonList = [];

  constructor (
    private _http: Http,
    private _router:Router,
    private _user: UserService,
    private _Func: Functions,
    private _API_Config: API_Config,
    private fb: FormBuilder,
    public jwtHelper: JwtHelper
  ) {
    this.fromDate = new Date();
    this.loc_whs_id = localStorage.getItem('whs_id');
    this.headerGet = this._Func.AuthHeader();
    this.headerPost = this._Func.AuthHeaderPost();
    this.headerPostJson = this._Func.AuthHeaderPostJson();
    this.urlWarehouse = this._API_Config.API_Warehouse;
    this.urlAPILocation = this._API_Config.API_Warehouse + "/" + this.loc_whs_id + "/locations";
    this.urlAPILocationType = this._API_Config.API_Location_type;
    this.urlAPIZoneType = this._API_Config.API_Zone_Type;
    var token = this._Func.getToken();
    if(token) {
      this.userCurrent = jwtHelper.decodeToken(token).name;
    }

    // get reason list
    this.getReasons();
  }

  ngAfterViewInit() {
    var modalChangeStatus = jQuery('#changeLocationStatus'),
      inputToDate = modalChangeStatus.find('[name="loc_sts_dtl_to_date"]');
    jQuery('#changeLocationStatus').on('hidden.bs.modal', () => {
      this.dataItemLoca = {};
      this.messagesChangeStatus = null;
      inputToDate.val('');
    });
  }

  private formatNumber(number) {
    return number < 10 ? '0' + number : number;
  }
  // Filter form only show choose one
  private filterForm2 ($target) {

    var arr_object = [
      {'input': 'loc_sts_dtl_sts_code', 'typeValidate': 'required'},
      {'input': 'loc_sts_dtl_from_date', 'typeValidate': 'required'},
      // {'input': 'loc_sts_dtl_reason', 'typeValidate': 'required'},
    ];

    try {

      for (var i = 0; i < arr_object.length; i++) {

        if (arr_object[i].input == $target.name) {

          switch (arr_object[i].typeValidate) {

            case 'required':

              if ($target.value.trim() == "" && arr_object[i].typeValidate == 'required') {

                this.listNull[$target.name + "_" + arr_object[i].typeValidate] = true;

              } else {

                this.listNull[$target.name + "_" + arr_object[i].typeValidate] = false;
              }
              break;
            default:
              break;

          }

        }

      }

      return false;

    } catch (err) {

      return false;
    }

  }

  // Filter form
  private filterFormStatus ($target) {

    var arr_object = [
      {'input': 'loc_sts_dtl_sts_code'},
      {'input': 'loc_sts_dtl_from_date'},
      // {'input': 'loc_sts_dtl_reason'},
    ];

    try {

      for (var i = 0; i < arr_object.length; i++) {

        if (arr_object[i].input == $target.name) {

          if ($target.value == "") {

            this.listNull[arr_object[i].input] = true;
            return false;

          } else {

            this.listNull[arr_object[i].input] = false;

          }
        }


      }

      return true;

    } catch (err) {

      return false;
    }

  }

  // Add data to status table
  private saveForm () {
    this.messages = null;
    this.messagesChangeStatus = null;

    var tar_get = this;
    var form = jQuery('form#change-status-form'),
      fromDate = form.find('[name="loc_sts_dtl_from_date"]').val(),
      toDate = form.find('[name="loc_sts_dtl_to_date"]').val(),
      status = form.find('[name="loc_sts_dtl_sts_code"]').val(),
      reason = jQuery.trim(form.find('[name="loc_sts_reason_id"]').val());
    jQuery('form').find('input, textarea, select').each(function(i, field) {

      tar_get.filterForm2(field);

    });

    var submit = true;
    jQuery.each( this.listNull, function( key, value ) {

      if (value) {

        submit = false;
        return false;
      }
    });
    if(fromDate && toDate && new Date(fromDate).getTime() > new Date(toDate).getTime()) {
      this.messagesChangeStatus = this._Func.Messages('danger', this._Func.msg('VR112'));
      form.find('.modal-body').scrollTop(0);
      submit = false;
    }
    if(status == 'IA' && !reason) {
      this.messagesChangeStatus = this._Func.Messages('danger', this._Func.msg('VR119'));
      form.find('.modal-body').scrollTop(0);
      submit = false;
    }
    if(status == 'LK' && this.dataItemLoca.loc_sts_code != 'AC' && this.dataItemLoca.loc_sts_code != 'LK') {
      this.messagesChangeStatus = this._Func.Messages('danger', this._Func.msg('VR120'));
      form.find('.modal-body').scrollTop(0);
      submit = false;
    }

    if (!submit) {

      return false;
    }
    var params = jQuery("form").serializeArray();
    params.push({'name': 'loc_sts_dtl_from_date', 'value': fromDate});
    this.saveChangeStatus.emit(params);
  }

  // Reset form
  private reset () {

    jQuery("input[name=loc_sts_dtl_from_date], input[name=loc_sts_dtl_to_date], input[name=loc_sts_reason_id], input[name=loc_sts_dtl_remark]").each(function( index ) {

      jQuery(this).val("");
    });
  }

  private checkInputComplete (obj)
  {

    this.listNull = [];

    var flag = false;
    for (let item in obj) {



      if ((obj[item] == null || obj[item] == '') && !this.nameNotRequire(item))
      {
        flag = true;
        this.listNull[item] = true;

      }
    }

    if (flag) {

      return false;
    }

    return true;

  }

  private nameNotRequire (item) {

    if (
      item == 'loc_sts_dtl_remark' ||
      item == 'loc_sts_dtl_to_date'

    ) {
      return true;

    }
    return false;


  }
  private parseError (err) {

    this.messages = this._Func.Messages('danger', this._Func.msg('VR100'));

  }

  private getReasons() {
    var target = this;
    this._http.get(this._API_Config.API_REASONS, {headers: this.headerGet})
      .map(res => res.json()).subscribe(
      data => {
        target.dataReasonList = data.data;
      },
      err => {
        this.parseError(err);
      }
    );
  }

}
