
import {Component} from '@angular/core';
import {API_Config} from '../../common/core/API_Config';
import {BaseTable} from '../../common/core/classes_us/base-table';
import {TableFunctions} from '../../common/core/table-functions';
import {DateFunctions} from '../../common/core/functions/date-functions';
import {BaseService} from '../../common/core/classes_us/base-service';
import {CustomersService} from '../../master-data/customers/customers-service';
import {TableStructure} from '../../common/directives/directives';

@Component ({
    selector: 'receiving-container',
    directives: [
        TableStructure
    ],
    templateUrl: '../../common/directives/table-structure/table.component.html',
    providers: [
        BaseService,
        TableFunctions,
        DateFunctions,
        CustomersService
    ]
})

export class RcvCtnrComponent extends BaseTable
{
    public tableID = 'receiving-ctnr';
    public url = this._api.API_RECEIVING_CONTAINER;
    public headerUrlSuffix = 'rep';
    public title = 'Receiving Container';
    public permissionPage = 'viewReport';
    
    public headerDef = [
        {
            id: 'ver_table',
            value: 1 + Math.random() * 10000
        },
        this.columnList.warehouse,
        this.columnList.customer,
        this.columnList.container,
        this.columnList.grEptDt,
        this.columnList.grFullName,
        this.columnList.grttlcarton,
        this.columnList.grttlpieces,
        this.columnList.grttlsku,
        this.columnList.grCreatedAt,
        this.columnList.dtDiff,
        this.columnList.grttlplt,
        this.columnList.ttlflrplt,
        this.columnList.billingDt
    ];
    
    
    public unsortableColumns = [
        'warehouse',
        'customer',
        'grEptDt',
        'grFullName',
        'grttlcarton',
        'grttlpieces',
        'grttlsku',
        'grCreatedAt',
        'dtDiff',
        'grttlplt',
        'ttlflrplt',
        'billingDt',
    ];
    
    public searches = [
        this.searchList.customer,
        this.searchList.container,
        this.searchList.createdGrStart,
        this.searchList.createdGrFinish,
    ];
    
    /*
    ****************************************************************************
    */

    constructor (
        public _api: API_Config,
        public _tableFunc: TableFunctions,
        public _tableService: BaseService,
        public _customersService: CustomersService
    )
    {
        super();

        this._tableFunc.permissionCall(this);
    }

    //**************************************************************************
}