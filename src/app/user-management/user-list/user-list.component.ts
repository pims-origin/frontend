import { Component, OnInit } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Router, RouterLink, ROUTER_DIRECTIVES } from '@angular/router-deprecated';
import 'rxjs/add/operator/map';
import {API_Config} from "../../common/core/API_Config";
import {AuthorizationService} from "../../common/services/authorization.service";
import {UserService} from "../../common/users/users.service";
import {UserListMessages } from '../user-list/user-list-messages';
import {Functions} from "../../common/core/functions";
import {BoxPopupService} from "../../common/popup/box-popup.service";
import {breadCrumbs} from "../../common/component/breadCrumbs.component";
import {breadCrumbRoutes} from "../../common/core/breadCrumb.routes";
import {WMSPagination, AdvanceTable, WMSBreadcrumb, WMSMessages} from '../../common/directives/directives';
import {ValidationService} from "../../common/core/validator";
declare var jQuery: any;
declare var jsPDF: any;
declare var swal: any;
declare var saveAs: any;

@Component({
    selector: 'user-list',
    directives: [WMSBreadcrumb, AdvanceTable, WMSMessages, WMSPagination],
    providers : [breadCrumbRoutes, UserListMessages, BoxPopupService],
    templateUrl: 'user-list.component.html',
    styles: [`
    .toolbar button {margin: 2px; padding: 0px;}
    .pagination li {cursor: pointer;}
    `],
})
export class UserListComponent{
    private hasEditUserPermission = false;
    private hasCreateUserPermission = false;
    private hasResetPasswordPermission = false;
    private tableID = 'user-list';
    private headerURL = this._API_Config.API_User_Metas + '/utc';
    private headerDef = [{id: 'ver_table', value: 5},
                      {id: 'ck', name: '', width: 25},
                      {id: 'status', name: 'Status', width: 70},
                      {id: 'emp_code', name: 'Employee ID', width: 100},
                      {id: 'username', name: 'Username', width: 160},
                      {id: 'first_name', name: 'First Name', width: 160},
                      {id: 'last_name', name: 'Last Name', width: 160},
                      {id: 'email', name: 'Email', width: 200},
                      {id: 'created_at', name: 'Created', width: 100},
                      {id: 'updated_at', name: 'Updated', width: 100}];
    private pinCols = 3;
    private rowData: any[];
    private getSelectedRow;
    private objSort;
    private searchParams;

    private dataExport: any[];
    private columnDefs: any[] = [];
    private rowCount: string;
    private url_server = this._API_Config.API_Users;

    private Pagination;
    private totalCount = 0;
    private tmpPageCount;
    private pageCount=0;
    private ItemOnPage=0;
    private currentPage = 1;
    private perPage = 20;
    private numLinks = 3;

    private messages;
    private listMessage = {};
    private showLoadingOverlay = false;

    private viewUser = false;
    private s_status = { 'AC': 'Active', 'LO': 'Locked', 'IA': 'Inactive'};
    private timeoutUpdateHeader;

    private headerExport = ['First Name', 'Last Name', 'Email', 'Created', 'Updated', 'Status'];
    private header:any;
    private breadCrumbRoute: Array<any> =[];
    private userListData;
    private objResetPassword = {};
    private newPass: string = '';
    private confirmNewPass: string = '';
    private resetPasswordMsgs = {};
    constructor(private _http: Http, private _router:Router, private _user: UserService,private _boxPopupService: BoxPopupService,
                private _func: Functions, private _API_Config: API_Config,
                private _userListMessage: UserListMessages,private _breadCrumb:breadCrumbRoutes,private _valid:ValidationService) {
        this.resetPasswordMsgs['err'] = [];
        this.resetPasswordMsgs['success'] = '';
        this.header = this._func.AuthHeader();
        this.listMessage = this._userListMessage.messages;
        this.breadCrumbRoute = this._breadCrumb.getUserListBreadCrumb();
        this.checkPermission();

    }

    // Check permission for user using this function page
    private checkPermission() {
        this.showLoadingOverlay = true;
        this._user.GetPermissionUser().subscribe(
            data => {
                this.hasEditUserPermission = this._user.RequestPermission(data, 'editUser');
                this.hasCreateUserPermission = this._user.RequestPermission(data, 'createUser');
                this.hasResetPasswordPermission = this._user.RequestPermission(data, 'resetUserPassword');
                this.viewUser = this._user.RequestPermission(data,'viewUser');
                /* Check orther permission if View allow */
                if(!this.viewUser) {
                    this._router.parent.navigateByUrl('/deny');
                }
                else {
                  this.getUserList();
                    this.getRoleList();
                }
            },
            err => {
                this.showMessage('danger', this._func.parseErrorMessageFromServer(err));
                this.showLoadingOverlay = false;
            }
        );
    }

    private showMessage(msgStatus, msg) {
        this.messages = {
            'status': msgStatus,
            'txt': msg
        }
        jQuery(window).scrollTop(0);
    }

    private filterList(pageNumber) {
        this.getUserList(pageNumber);
    }

    private getPage(pageNumber) {
        let arr=new Array(pageNumber);
        return arr;
    }

    private initPagination(data){
      let meta = data['meta'];
      if(meta['pagination']) {
        this.Pagination=meta['pagination'];
      }
      else {
        let perPage = meta['perPage'],
            curPage = meta['currentPage'],
            total = meta['totalCount'];
        let count = perPage * curPage > total ? perPage * curPage - total : perPage;
        this.Pagination = {
          "total": total,
          "count": count, // TODO
          "per_page": perPage,
          "current_page": curPage,
          "total_pages": meta['pageCount'],
          "links": {
            "next": data['link']['next']
          }
        }
      }
      this.Pagination['numLinks']=3;
      this.Pagination['tmpPageCount']=this._func.Pagination(this.Pagination);
    }

    private convertInttoDate(date, format = "-"){
        var date:any = new Date(date),
            month = date.getMonth() + 1,
            day = date.getDate(),
            year = date.getFullYear();
        if(month < 10) month = '0' + month;
        if(day < 10) day = '0' + day;
        return month + format + day + format + year;
    }

    private getUserList(page = null){
        this.showLoadingOverlay = true;
        if(!page) page = 1;
        let params="?page="+page+"&limit="+this.perPage;
        if(this.objSort && this.objSort['sort_type'] != 'none') {
          params += '&sort['+ this.objSort['sort_field']+ ']='+ this.objSort['sort_type'];
        }
        else {
          params += '&sort[first_name]=asc';
        }
        if(this.searchParams) {
          params += this.searchParams;
        }

        this._http.get(this.url_server + params, {headers: this.header})
              .map(res => res.json())
              .subscribe(
                data => {
                  this.createRowData(data);
                  // this.removeCheckAll(jQuery('#user-list'));
                  this.showLoadingOverlay = false;
                },
                err => {
                  this.showMessage('danger', this._func.parseErrorMessageFromServer(err));
                  this.showLoadingOverlay = false;
                }
              );
    }
    private onPageSizeChanged($event) {
        this.perPage = $event.target.value;
        if((this.Pagination['current_page'] - 1) * this.perPage >= this.Pagination['total']) {
            this.currentPage = 1;
        }
        else {
            this.currentPage = this.Pagination['current_page'];
        }
        this.getUserList(this.currentPage);
    }

    private search() {
      this.searchParams = '';
      let params_arr = jQuery("#form-filter").serializeArray() ;
      for (let i in params_arr) {
          if (params_arr[i].value == "") continue;
          this.searchParams +="&" +  params_arr[i].name.trim() + "=" + encodeURIComponent(params_arr[i].value.trim());
      }
        this.getUserList(1);
    }
    // Format data for ag-grid
    private createRowData(data) {
        var rowData: any[] = [];
        // Check data
        if (typeof data.data != 'undefined') {
          this.userListData = data.data;
          this.initPagination(data);

          data = this._func.formatData(data.data);
          this.dataExport = [];
          for (var i = 0; i < data.length; i++) {

            this.dataExport.push([
                  data[i].first_name,
                  data[i].first_name,
                  data[i].email,
                  this.convertInttoDate(parseInt(data[i].created_at) * 1000, "/"),
                  this.convertInttoDate(parseInt(data[i].updated_at) * 1000, "/"),
                  this.s_status[data[i].status]
            ]);
            var employeeId = data[i].emp_code ? data[i].emp_code : '';
            rowData.push(({
                emp_code: '<a href="#/users/' + data[i].user_id + '">'+ employeeId +'</a>',
                username: data[i].username,
                first_name: data[i].first_name,
                last_name: data[i].last_name,
                email: data[i].email,
                created_at: this.convertInttoDate(parseInt(data[i].created_at) * 1000, "/"),
                updated_at: this.convertInttoDate(parseInt(data[i].updated_at) * 1000, "/"),
                status: this.s_status[data[i].status],
                user_id: data[i].user_id
            }));
          }
        }
        else {
          this.userListData = [];
          this.rowData = [];
          this.Pagination = null;
        }

        this.rowData = rowData;
    }

    afterGetSelectedRow($event) {
      var listSelectedItem = $event.data;
      switch ($event.action) {
        case 'edit':
          if (listSelectedItem.length > 1) {
            this.showMessage('danger', this.listMessage['edit_multiple']);
          }
          else {
            if (listSelectedItem.length < 1) {
              this.showMessage('danger', this.listMessage['edit_none']);
            }
            else {
              this._router.parent.navigateByUrl('/users/' + listSelectedItem[0].user_id + "/edit");
            }
          }
          break;

        case 'delete':
          if (listSelectedItem.length < 1) {
            this.showMessage('danger', this.listMessage['remove_none']);
          }
          else {
            var listItemDelete = [];
            for(var i = 0, l = listSelectedItem.length; i < l; i++) {
              listItemDelete.push(listSelectedItem[i].user_id)
            }
            swal({
                title: this.listMessage['remove_confirm'],
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: this.listMessage['agree'],
                cancelButtonText: this.listMessage['cancel'],
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false
            })
            .then((agree) => {
              if (agree) {
                this.showLoadingOverlay = true;
                this._http.delete(this._API_Config.API_Users, {body: 'user_ids='+listItemDelete.join(','), headers: this._func.AuthHeaderPost()}).map(res => res.json())
                  .subscribe(
                      data => {
                        this.getUserList(this.currentPage);
                      },
                      err => {
                        this.showMessage('danger', this._func.parseErrorMessageFromServer(err));
                        this.showLoadingOverlay = false;
                      }
                  );
              }
            });
          }
          break;
        case 'resetPassword' :
          if (listSelectedItem.length > 1) {
            this.showMessage('danger', this.listMessage['resetpwd_multiple']);
          }
          else {
            if (listSelectedItem.length < 1) {
              this.showMessage('danger', this.listMessage['resetpwd_none']);
            }
            else {
              this.objResetPassword = listSelectedItem[0];
              jQuery('.resetPasswordModal').modal('show');
            }
          }
          break;
      }

      this.getSelectedRow = false;
    }

    public deleteUser() {
      this.getSelectedRow = 'delete';
    }

    private edit() {
      this.getSelectedRow = 'edit';
    }

    private onCellClicked($event) {
      // console.log('onCellClicked: ', $event);
      // let field = $event.colDef.field;
      // if(field != 'ck') {
      //   this._router.parent.navigateByUrl('/users/' + $event.data.user_id);
      // }
    }

    private doSort(objSort) {
      this.objSort = objSort;
      this.getUserList(this.Pagination.current_page);
    }

    // Export data to CSV format
    private exportCsv(){
        let api = this._API_Config.API_Authen  + '/users/export/csv?'+this.searchParams,
            fileName = 'user_list',
            fileType = '.csv';
        this.exportFile(api, fileName, fileType);
    }

    // Export data to PDF format
    private exportPdf(){
        let api = this._API_Config.API_Authen  + '/users/export/pdf?'+this.searchParams,
            fileName = 'user_list',
            fileType = '.pdf';
        this.exportFile(api, fileName, fileType);
    }

    private reset() {
        jQuery("#form-filter input, #form-filter select").each(function( index ) {
            jQuery(this).val("");
        });
        this.searchParams = '';
        this.getUserList(1);
    }

    private exportFile(api, fileName, fileType) {
      var that = this;
        try{
          that.showLoadingOverlay = true;
          let xhr = new XMLHttpRequest();
          xhr.open("GET", api , true);
          xhr.setRequestHeader("Authorization", 'Bearer ' + that._func.getToken());
          xhr.responseType = 'blob';

          xhr.onreadystatechange = function () {
            if(xhr.readyState == 2) {
              if(xhr.status == 200) {
                xhr.responseType = "blob";
              } else {
                xhr.responseType = "text";
              }
            }

            if(xhr.readyState === 4) {
              if(xhr.status === 200) {
                var blob = new Blob([this.response], {type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'});
                saveAs.saveAs(blob, fileName + fileType);
              }else{
                let errMsg = '';
                if(xhr.response) {
                  try {
                    var res = JSON.parse(xhr.response);
                    errMsg = res.errors && res.errors.message ? res.errors.message : xhr.statusText;
                  }
                  catch(err){errMsg = xhr.statusText}
                }
                else {
                  errMsg = xhr.statusText;
                }
                if(!errMsg) {
                  errMsg = that._func.msg('VR100');
                }
                that.showMessage('danger', errMsg);
              }
              that.showLoadingOverlay = false;
            }
          }
          xhr.send();
        }catch (err){
          that.showMessage('danger', that._func.msg('VR100'));
          that.showLoadingOverlay = false;
        }
    }

    private expandTable = false;
    private viewListFullScreen() {
      this.expandTable = true;
      setTimeout(() => {
        this.expandTable = false;
      }, 500);
    }

  private resetPassword(){
    this.getSelectedRow = 'resetPassword';
  }

  private saveResetPassword(){
    if(this.checkPasswordPolicy()){
      this.showLoadingOverlay = true;
      this.resetPasswordMsgs['success'] = '';
      let creds = {
        'password': this.newPass,
        'password_confirmation': this.confirmNewPass
      }
      this._http.post(this._API_Config.API_Users + '/reset-password/' + this.objResetPassword['user_id'] ,
        JSON.stringify(creds), {headers: this._func.AuthHeaderPostJson()})
        .map(res => res.json())
        .subscribe(
          data => {
            this.resetPasswordMsgs['success'] = 'User password has been reset successfully.';
            this.newPass = '';
            this.confirmNewPass = '';
            this.showLoadingOverlay = false;
          },
          err => {
            this.resetPasswordMsgs['err'] = [];
            let error = err.json();
            if(error.message != undefined){
              this.resetPasswordMsgs['err'].push(error.message);
            }
            if(error.error.new_password != undefined){
              for(let i = 0; i< error.error.new_password.length; i++){
                this.resetPasswordMsgs['err'].push(error.error.new_password[i]);
              }
            }

            this.showLoadingOverlay = false;
          }
        );
    }
  }

  private closePopup(){
    this.newPass = '';
    this.confirmNewPass = '';
    this.resetPasswordMsgs['err'] = [];
    this.resetPasswordMsgs['success'] = '';
  }
  private checkPasswordPolicy(){
    this.resetPasswordMsgs['err'] = this._valid.validatePasswordByPolicy(this.newPass,this.confirmNewPass);
    if(this.resetPasswordMsgs['err'].length > 0){
      return false;
    }
    return true;
  }

  private removeMsgs(msg){
    let index = this.resetPasswordMsgs['err'].indexOf(msg);
    this.resetPasswordMsgs['err'].splice(index,1);
  }

    private dataRoleList = [];
    private getRoleList () {

        this._http.get(this._API_Config.API_MASTER_URL + '/roles?limit=10000&sort[code]=asc', {headers: this.header})
            .map(res => res.json())
            .subscribe(
                data => {

                    this.dataRoleList = data['data'];
                },
                err => {
                }
            );
    }
}

