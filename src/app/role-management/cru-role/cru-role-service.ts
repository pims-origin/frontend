import {Component, Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {Functions, API_Config} from '../../common/core/load';

@Injectable()
export class CRURoleService {

  public AuthHeader = this._Func.AuthHeader();
  public AuthHeaderPost = this._Func.AuthHeaderPost();
  public API_MASTER_URL = this._api.API_MASTER_URL;

  constructor(private _Func: Functions, private http: Http, private _api: API_Config) {}

  addRole(stringData) {
    return this.http.post(this.API_MASTER_URL + '/roles', stringData, { headers: this._Func.AuthHeaderPostJson() })
      .map((res: Response) => res.json().data);
  }

  getRoleDetail(roleName) {
    return this.http.get(this.API_MASTER_URL+'/roles/'+roleName, { headers: this.AuthHeader })
      .map((res: Response) => res.json().data);
  }

  updateRole(roleId, stringData) {
    return this.http.put(this.API_MASTER_URL+'/roles/'+roleId, stringData, { headers: this._Func.AuthHeaderPostJson() })
      .map((res: Response) => res.json().data);
  }

  getAllPermissionByLogedUser() {
    return this.http.get(this.API_MASTER_URL + '/permissions/groups', {headers: this.AuthHeader})
            .map(res => res.json())
  }
}
