import {Component, OnInit} from '@angular/core';
import {RouteParams, Router} from '@angular/router-deprecated';
import {UserService} from "../../../common/users/users.service";
import {Http} from '@angular/http';
import {JwtHelper} from 'angular2-jwt/angular2-jwt';
import {Functions} from "../../../common/core/functions";
import {BoxPopupService} from "../../../common/popup/box-popup.service";
import {WMSBreadcrumb} from '../../../common/directives/directives';
import {WMSMessages} from "../../../common/directives/messages/messages";
import {PaginationControlsCmp} from "../../../common/directives/pagination/pagination-controls-cmp";
import {PaginationService} from "../../../common/directives/pagination/pagination-service";
import {PaginatePipe} from "../../../common/directives/pagination/paginate-pipe";
import {PbUpdateMessages} from "./pb-update-messages";
import {PaPbServices} from "../pa-pb.service";
declare var jQuery:any;
declare var swal:any;

@Component({
    selector: 'put-away-update',
    directives: [WMSBreadcrumb, WMSMessages, PaginationControlsCmp],
    providers: [BoxPopupService, PaPbServices, PbUpdateMessages, PaginationService],
    pipes: [PaginatePipe],
    templateUrl: 'pb-update.component.html',

})
export class PbUpdateComponent {

    private messages;
    private listMessage = {};
    private timeoutHideMessage;
    private showLoadingOverlay = false;
    private dataReturnOrderDetail = [];
    private id;
    private whs_id;
    private cus_id;
    private isLoadingGRItem;
    private hasPermissionEdit;
    private isHideButton;
    private listMsgs = {
        'ER1': 'Location is not existed or unavailable.',
        'ER2': 'Location is in cycle count process',
        'ER3': 'Location is {{loc_status}}',
        'ER4': 'Location already has pallet'
    };

    constructor(
                private _user:UserService,
                private _func:Functions,
                private _paPbServices:PaPbServices,
                private _messageService:PbUpdateMessages,
                private params:RouteParams,
                private _boxPopupService:BoxPopupService,
                public jwtHelper:JwtHelper,
                private _router:Router) {
    }

    ngOnInit() {
        this.listMessage = this._messageService.messages;
        this.id = this.params.get('id');
        if (localStorage.getItem('whs_id')) {
            this.whs_id = localStorage.getItem('whs_id');
        }
        // this.getGoodReceiptDetail();
        // this.getGoodReceiptItems();
        this.checkPermission();
        jQuery(window).on('scroll.reposition resize.reposition', function () {
            var ul = jQuery('.autocomplete:visible');
            if (ul.length) {
                var input = ul.prev('input');
                ul.css({
                    width: input.outerWidth(),
                    top: input.offset().top - jQuery(window).scrollTop() + input.outerHeight(),
                    left: input.offset().left
                })
            }
        });
    }

    private checkPermission() {
        this.showLoadingOverlay = true;
        this._user.GetPermissionUser().subscribe(
            data => {
                this.hasPermissionEdit = this._user.RequestPermission(data, 'updatePutback');
                if (!this.hasPermissionEdit) {
                    this._router.parent.navigateByUrl('/deny');
                }
                else {

                    this.getPutBackById();
                }

            },
            err => {

                this.showLoadingOverlay = false;
                this.showMessage('danger', this._func.parseErrorMessageFromServer(err));

            }
        );
    }

    // Get return order by ID
    private getPutBackById() {

        var that = this;
        this._paPbServices.getPutBackById(this.id)
            .subscribe(
                data => {

                    this.showLoadingOverlay = false;

                    for (var i = 0, l = data.data['details'].length; i < l; i++) {
                        data.data['details'][i].old_loc_id = data.data['details'][i].actual_loc_id ? data.data['details'][i].actual_loc_id : 0;
                        data.data['details'][i].checked = data.data['details'][i]['suggested_loc_id'] && (!data.data['details'][i].actual_loc_id || data.data['details'][i].actual_loc_id == data.data['details'][i]['suggested_loc_id']) ? true : false;
                        data.data['details'][i].disable_checkbox = data.data['details'][i].actual_loc_code ? true : false;
                        if (data.data['details'][i].actual_loc_code == null) {
                            data.data['details'][i].actual_loc_code = '';
                        }

                        if (!data.data['details'][i].disable_checkbox) {
                            this.isHideButton = true;
                        }

                        data.data['details'][i].disable_checkbox = data.data['details'][i].suggested_loc_code == "" ? true : false;

                    }

                    this.dataReturnOrderDetail = data.data;
                    this.cus_id = data.data['cus_id'];
                },
                err => {

                    this.parseError(err);
                    this.showLoadingOverlay = false;
                },
                () => {
                    
                }
            );

    }

    private showMessage(msgStatus, msg) {
        // if(this.timeoutHideMessage) clearTimeout(this.timeoutHideMessage);
        this.messages = {
            'status': msgStatus,
            'txt': msg
        };
        // this.timeoutHideMessage = setTimeout(() => {
        //     this.messages = null;
        // }, 4000);
        jQuery(window).scrollTop(0);
    }


    private getGoodReceiptItems() {
        this.isLoadingGRItem = true;
        this.showLoadingOverlay = true;
        this._paPbServices.getGoodReceiptItems(this.id)
            .subscribe(
                data => {
                    for (var i = 0, l = data.data.length; i < l; i++) {
                        data.data[i].old_loc_id = data.data[i].actual_loc_id ? data.data[i].actual_loc_id : 0;
                        data.data[i].checked = data.data[i].suggested_loc_id && (!data.data[i].actual_loc_id || data.data[i].actual_loc_id == data.data[i].suggested_loc_id) ? true : false;
                        data.data[i].disable_checkbox = data.data[i].actual_loc_code ? true : false;
                        if (data.data[i].actual_loc_code == null) {
                            data.data[i].actual_loc_code = '';
                        }
                        if (!data.data[i].disable_checkbox) {
                            this.isHideButton = true;
                        }


                    }
                    this.dataReturnOrderDetail['details'] = data.data;
                    this.cus_id = data.data['cus_id'];
                    this.isLoadingGRItem = false;
                },
                err => {

                    if (err.json().errors.message != undefined) {
                        this.showMessage('danger', err.json().errors.message);
                    }
                    this.showLoadingOverlay = false;
                },
                () => {
                    this.showLoadingOverlay = false;
                    this.isLoadingGRItem = false;
                }
            );
    }

    // V1
    /*private submitForm(params) {

        var tmpFlagRequired = false;
        let l = this.dataReturnOrderDetail['details'].length;

        var submitData = [],
            hasError = false;

        this.showLoadingOverlay = true;
        for (let i = 0; i < l; i++) {
            let item = this.dataReturnOrderDetail['details'][i],
                locationID = !item.checked ? item.actual_loc_id : item.suggested_loc_id;

            // @hoangtran 29/11/2016 check out actual location before saving
            let locationValue = '';
            if (!item.checked && item.actual_loc_code != '') {
                locationValue = item.actual_loc_code;
            } else {
                locationValue = item.suggested_loc_code;
            }

            if (locationID) {
                submitData.push({
                    pb_id: item.pb_id,
                    return_dtl_id: item.return_dtl_id,
                    loc_id: locationID,
                    loc_code: locationValue
                })
            }
            else {
                if (!item.checked) {
                    item.hasErrRequired = true;
                }
            }
            
            let param = '?loc_code=' + encodeURIComponent(locationValue) + '&limit=20';
            //item.searching = true;
            if (item.subscribe) {
                item.subscribe.unsubscribe();
            }
            item.subscribe = this._paPbServices.searchLocation(this.whs_id, param)
                .subscribe(
                    data => {
                        item.listAutocompleteLocation = data.data;
                        this.checkExistLocation(item, locationValue);
                        if (item.hasErrRequired || item.notExist || item.searching || item.hasErrGreater || item.duplicated) {
                            hasError = true;
                        }
                        //API calling
                        if (i == l - 1) {
                            if (!hasError) {
                                params = params ? this.id + params : this.id;

                                var tmp = {};
                                tmp['items']= submitData;

                                this._paPbServices.updatePutBackItems(params, JSON.stringify(tmp))
                                    .subscribe(
                                        data => {
                                            this.showMessage('success', this.listMessage['update_success']);
                                            setTimeout(() => {
                                                this._router.parent.navigateByUrl('/putback-process/putaway/putaway-update');
                                            }, 600);
                                            this.showLoadingOverlay = false;
                                        },
                                        err => {
                                            if (typeof err == 'object' && err._body) {
                                                let parseErr = JSON.parse(err._body);
                                                if (parseErr.errors && parseErr.errors.status_code == 400 && (parseErr.errors.code == 4 || parseErr.errors.code == 5)) {

                                                    this.showMessage('danger', 'Some location already have pallets.');
                                                }
                                                else {
                                                    this.showMessage('danger', this._func.parseErrorMessageFromServer(err));
                                                }
                                            }
                                            else {
                                                this.showMessage('danger', this._func.parseErrorMessageFromServer(err));
                                            }
                                            this.showLoadingOverlay = false;
                                        },
                                        () => {
                                            this.showLoadingOverlay = false;
                                        }
                                    );
                            } else {
                                this.showLoadingOverlay = false;
                            }
                        }
                    },
                    err => {
                        this.showLoadingOverlay = false;
                    },
                    () => {

                    }
                )// end
        }
    }*/

    // V2
    private submitForm() {
      var submitData = [],
          listLocation = [],
          dataCheck = [],
          hasErr = false;
      this.messages = null;
      for(let i = 0, l = this.dataReturnOrderDetail['details'].length; i < l; i++) {
        let item = this.dataReturnOrderDetail['details'][i],
          locationID = !item.checked ? item.actual_loc_id : item.suggested_loc_id,
          locationCode = !item.checked ? item.actual_loc_code : item.suggested_loc_code;

        if(locationID) {
          submitData.push({
              pb_id: item.pb_id,
              return_dtl_id: item.return_dtl_id,
              loc_id: locationID,
              loc_code: locationCode,
              spc_hdl_code: item['spc_hdl_code'],
              plt_type: item['plt_type'],
              cus_id: item['cus_id']
          });
          listLocation.push(locationID);
          dataCheck.push({
            loc_id: locationID,
            item_id: item.item_id
          });
        }
        else {
          if(!item.checked && !item.actual_loc_code.trim()) {
            item.hasErrRequired = true;
            hasErr = true;
          }
        }

          if (item.duplicated) {
              hasErr = true;
          }
      }
      try {
        if(!hasErr && listLocation.length == this.dataReturnOrderDetail['details'].length) {
          let tmp = {};
          tmp['items']= submitData;

          this.showLoadingOverlay = true;
          this._paPbServices.updatePutBackItems(this.id, JSON.stringify(tmp))
              .subscribe(
                  data => {
                      this.showMessage('success', this.listMessage['update_success']);
                      setTimeout(() => {
                          this._router.parent.navigateByUrl('/putback-process/putaway/putaway-update');
                      }, 600);
                      this.showLoadingOverlay = false;
                  },
                  err => {
                      this.showMessage('danger', this._func.parseErrorMessageFromServer(err));
                      this.showLoadingOverlay = false;
                  }
              );
        }
      }
      catch(err) {
      }
    }

    private showLocationsError(listLocation, errDetails) {
      for(let i = 0, l = listLocation.length; i < l; i++) {
        let err = errDetails[listLocation[i]];
        if(err) {
          this.dataReturnOrderDetail['details'][i].notExist = true;
          this.dataReturnOrderDetail['details'][i].notExistMsg = err;
        }
      }
    }

    private cancel() {
        var that = this;
        this._boxPopupService.showWarningPopup()
            .then(function (ok) {
                if (ok)
                    that._router.navigateByUrl('/putback-process/putaway/putaway-update');
            });
    }

    private toggleActualLocation(item) {
        item.checked = !item.checked;
        item.actual_loc_id = null;
        item.actual_loc_code = '';
        if (item.checked) {
            // item.actual_loc_id = item.suggested_loc_id;
            // item.actual_loc_code = item.suggested_loc_code;
            item.notExist = false;
            item.hasErrRequired = false;
            clearTimeout(item.timeoutHideDropdown);
            item.showDropdown = false;
            jQuery('.autocomplete:visible').css('display', 'none');
            item.listAutocompleteLocation = [];
            if (item.subscribe) {
                item.subscribe.unsubscribe();
            }
            item.searching = false;
        }
        this.checkDuplicated();
    }

    private searchLocation(item, value) {
        item.searching = true;
        const params = `/cus-id/${this.cus_id}/pallet-type/${item['plt_type']}/spc-hdl-code/${item['spc_hdl_code']}?loc_code=${encodeURIComponent(value)}`;
        if(item.subscribe) {
          item.subscribe.unsubscribe();
        }

        item.subscribe = this._paPbServices.searchLocation(params)
          .subscribe(
            data => {
              item.listAutocompleteLocation = data.data;
              if(item.listAutocompleteLocation && item.listAutocompleteLocation.length) {
                for(var i = 0, l = item.listAutocompleteLocation.length; i < l; i++) {
                  var loc = item.listAutocompleteLocation[i];
                  if(loc.loc_code == value) {
                    item.actual_loc_id = loc.loc_id;
                  }
                }
              }
              this.checkDuplicated();
            },
            err => {},
            () => {item.searching = false;}
          )
    }

    private checkExistLocation(item, value) {
        let notExist = true,
        errMsg = this.listMsgs.ER1;
        if(item.listAutocompleteLocation && item.listAutocompleteLocation.length) {
          for(let i = 0, l = item.listAutocompleteLocation.length; i < l; i++) {
            let loc = item.listAutocompleteLocation[i];
            if(loc.loc_code == value) {
              item.act_loc_id = loc.loc_id;
              notExist = false;
              // if(loc.loc_sts_code != 'AC') {
              //   notExist = true;
              //   if(loc.loc_sts_code == 'CL') {
              //     errMsg = this.listMsgs.ER2;
              //   }
              //   else {
              //     errMsg = this.listMsgs.ER3.replace('{{loc_status}}', loc.loc_sts_code_name);
              //   }
              // }
              // else 
              //   if(loc.has_pallet == 1) {
              //     notExist = true;
              //     errMsg = this.listMsgs.ER4;
              //   }
              break;
            }
          }
        }
        item.notExist = notExist;
        item.notExistMsg = errMsg;
    }

    private checkDuplicated() {
        var duplicates = {},
            listLocationsID = [],
            arrDuplicatedPos = [];

        for (var i = 0; i < this.dataReturnOrderDetail['details'].length; i++) {
            var item = this.dataReturnOrderDetail['details'][i],
                locationID = item.checked ? item.suggested_loc_id : item.actual_loc_id;
            item.duplicated = false;
            if (locationID) {
                listLocationsID.push(locationID);
            }
            else {
                listLocationsID.push('_null');
            }
        }
        for (var i = 0; i < listLocationsID.length; i++) {
            if (duplicates.hasOwnProperty(listLocationsID[i])) {
                duplicates[listLocationsID[i]].push(i);
            } else if (listLocationsID.lastIndexOf(listLocationsID[i]) !== i) {
                duplicates[listLocationsID[i]] = [i];
            }
        }

        for (let key in duplicates) {
            if (key != '_null') {
                arrDuplicatedPos = arrDuplicatedPos.concat(duplicates[key]);
            }
        }

        if (arrDuplicatedPos.length) {
            for (let i = 0, l = arrDuplicatedPos.length; i < l; i++) {
                this.dataReturnOrderDetail['details'][arrDuplicatedPos[i]].duplicated = true;
            }
        }
    }

    private showDropdown(item, $event) {
        var value = $event.target.value;
        item.showDropdown = true;
        this.searchLocation(item, value);
        var input = jQuery($event.target);
        var ul = input.next('ul');
        ul.css({
            display: 'block',
            position: 'fixed',
            width: input.outerWidth(),
            top: input.offset().top - jQuery(window).scrollTop() + input.outerHeight(),
            left: input.offset().left
        })
    }

    private hideDropDown(item, $event) {
        item.timeoutHideDropdown = setTimeout(() => {
          var value = jQuery.trim($event.target.value);
          item.showDropdown = false;
          var input = jQuery($event.target);
          var ul = input.next('ul');
          ul.css({ display: 'none' })
          if(!item.checked) {
            if(!value) {
              item.hasErrRequired = true;
            }
            else {
              if(!item.searching) {
                this.checkExistLocation(item, value);
                this.checkDuplicated();
              }
              else {
                item.searching = true;
                if(item.subscribe) {
                  item.subscribe.unsubscribe();
                }
                  const params = `/cus-id/${this.cus_id}/pallet-type/${item['plt_type']}/spc-hdl-code/${item['spc_hdl_code']}?loc_code=${encodeURIComponent(value)}`;
                item.subscribe = this._paPbServices.searchLocation(params)
                  .subscribe(
                    data => {
                      item.listAutocompleteLocation = data.data;
                      this.checkExistLocation(item, value);
                      this.checkDuplicated();
                    },
                    err => {},
                    () => {item.searching = false;}
                  )
              }
            }
          }
        }, 200)
    }

    private autoSearchLocation(item, value) {
        item.hasErrRequired = jQuery.trim(value) ? false : true;
        item.notExist = false;
        item.notExistMsg = this.listMsgs.ER1;
        item.actual_loc_id = null;
        if (!item.hasErrRequired) {
            this.searchLocation(item, value);
        }
    }

    private selectItem(item, locID, locCode, $event) {
        clearTimeout(item.timeoutHideDropdown);
        item.hasErrRequired = false;
        item.showDropdown = false;
        item.actual_loc_code = locCode;
        item.actual_loc_id = locID;
        jQuery($event.target).closest('ul').css('display', 'none');
        if (item.assigned_loc_code == locCode) {
            item.checked = true;
            //item.actual_loc_code = '';
        } else {
            this.checkExistLocation(item, locCode);
            this.checkDuplicated();
        }
    }

    // Show error when server die or else
    private parseError(err) {

        this.messages = {'status': 'danger', 'txt': this._func.parseErrorMessageFromServer(err)};
    }
}
