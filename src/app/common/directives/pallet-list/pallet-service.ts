import {Component, Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {API_Config,Functions} from '../../../common/core/load';

@Injectable()
export class PalletServices {

    public AuthHeader = this._Func.AuthHeader();
    public AuthHeaderPost = this._Func.AuthHeaderPost();
    public AuthHeaderPostJson = this._Func.AuthHeaderPostJson();

    constructor(private _Func: Functions,
                private _API: API_Config,
                private http: Http) {

    }

    public getPalletList(wh_id, id, params){
        return this.http.get(this._API.API_ORDERS_CORE + '/' + wh_id +'/out-pallet/get-by-order/' + id + params,{ headers: this.AuthHeader })
            .map((res: Response) => res.json());
    }

    public updateData(wh_id, pltId, data){
        return this.http.put(this._API.API_ORDERS_CORE + '/' + wh_id +'/out-pallet/' + pltId, data, { headers: this.AuthHeaderPostJson })
            .map((res: Response) => res.json());
    }

    public deleteLPN(wh_id, pltId){
        return this.http.delete(this._API.API_ORDERS_CORE + '/' + wh_id +'/out-pallet/' + pltId, { headers: this.AuthHeader })
            .map((res: Response) => res.json());
    }
    
}
