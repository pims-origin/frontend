
import {BaseClass} from './base-class';

export class ModalForm extends BaseClass
{
    public parentComponent = <any> {};
    public messages: any;
    public isVisible: boolean = false;
    public showLoadingOverlay: boolean = false;

    //**************************************************************************

    constructor()
    {
        super();
    }

    //**************************************************************************

    public hideInfo()
    {
        this.isVisible = false;
    }

    //**************************************************************************

}
