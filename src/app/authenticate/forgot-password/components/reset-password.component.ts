import {Component} from "@angular/core";
import {Router, RouterLink, RouteParams, ROUTER_DIRECTIVES} from '@angular/router-deprecated';
import {CORE_DIRECTIVES, FORM_DIRECTIVES, ControlGroup, FormBuilder, Validators} from '@angular/common';
import {Http, Response, JSONP_PROVIDERS, Jsonp} from '@angular/http';
import {API_Config, Functions} from '../../../common/core/load';
import {ValidationService, matchingPasswords} from '../../../common/core/validator';
@Component({
    selector: 'reset-password',
    directives: [CORE_DIRECTIVES, FORM_DIRECTIVES],
    providers: [ValidationService],
    templateUrl: '../views/reset-password.component.html',
    // styleUrls:['assets/css/page/forgot-password.css']

})
export class ResetPasswordComponent {

    public AuthHeader = this._Func.AuthHeaderNoTK();
    public messages;
    public loading:boolean = false;
    private tokenInvalidExpired = false;
    private password = '';
    private password_confirmation = '';
    private resetPasswordMsgs = {}

    constructor(public router:Router,
                private _Func:Functions,
                private _API:API_Config,
                private _Valid:ValidationService,
                private params:RouteParams,
                private http:Http,
                fb:FormBuilder) {
        this.resetPasswordMsgs['err'] = [];
        this.CheckValidExpiredToken().subscribe(
            data=> {
            },
            err => {
                this.tokenInvalidExpired = true;
                this.messages = this._Func.Messages('danger', this._Func.parseErrorMessageFromServer(err));
            }
        );
    }

    GoToLoginPage() {
        this.router.parent.navigateByUrl('/login');
    }

    ResetPass():void {
        if (this.checkPasswordPolicy()) {
            this.loading = true;
            let param = "password=" + encodeURIComponent(this.password) + "&password_confirmation=" +
                encodeURIComponent(this.password_confirmation) + "&token=" + this.params.get('token');
            this.ResetPassService(param).subscribe(
                data=> {
                    this.loading = true;
                    this.messages = this._Func.Messages('success', this._Func.msg('VR104'));
                },
                err => {
                    this.loading = false;
                    this.messages = this._Func.Messages('danger', this._Func.parseErrorMessageFromServer(err));
                }
            );
        }
    }

    ResetPassService(param) {
        return this.http.post(this._API.API_RESET_PASS, param,
            {headers: this.AuthHeader}).map((res:Response) => res.json().data);
    }

    /*
     * Redirect to
     * */

    CheckValidExpiredToken() {
        let token = this.params.get('token');
        return this.http.get(this._API.API_CHECK_TOKEN + '/' + token, {headers: this.AuthHeader})
            .map((res:Response) => res.json().data);

    }

    private checkPasswordPolicy(){
        this.messages=false;
        this.resetPasswordMsgs['err'] = this._Valid.validatePasswordByPolicy(this.password,this.password_confirmation);
        if(this.resetPasswordMsgs['err'].length > 0){
            return false;
        }
        return true;
    }

    private removeMsgs(msg){
        let index = this.resetPasswordMsgs['err'].indexOf(msg);
        this.resetPasswordMsgs['err'].splice(index,1);
    }

}
