
import {Component, Input, Injectable} from '@angular/core';
import {
    WMSMessages,
    Searches,
    TableBlock
} from '../../directives';

@Component ({
    selector: 'table-structure',
    directives:[
        WMSMessages,
        Searches,
        TableBlock
    ],
    providers: [],
    templateUrl: 'table-structure.html',
})

@Injectable()

export class TableStructure
{
    @Input() title;
    @Input() tableID;
    @Input() searches: any [];
    @Input() headerURL = '';
    @Input() headerDef = [];
    @Input() pinCols = 0;
    @Input() rowData: any [];
    @Input() Pagination;
    @Input() perPage = 20;
    @Input() parentComponent;
    @Input() dataExport: any [];
    @Input() downloadName: string = 'exportTableData';
    @Input() messages;

    public objSort;

    constructor()
    {

    }

    /*
    ****************************************************************************
    */

}
