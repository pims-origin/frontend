import {Component, Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {Functions, API_Config} from '../../../common/core/load';

@Injectable()
export class PickingCompleteServices {

  public AuthHeader = this._Func.AuthHeader();
  public AuthHeaderPost = this._Func.AuthHeaderPost();

  constructor(
    private _Func: Functions,
    private _http: Http,
    private _api: API_Config) {
  }

  searchLocation(param) {
    return this._http.get(this._api.API_Wavepick + "/suggest-actual-locations" + param,{ headers: this.AuthHeader})
    	.map((res: Response) => res.json());
  }

  getWavepickDetail(whs_id,wv_id) {
    return this._http.get(this._api.API_Wavepick_2 + '/whs/'+whs_id+'/wave-pick-details/list?wv_id=' + wv_id,{ headers: this.AuthHeader })
      .map((res: Response) => res.json());
  }

  getWarepickOrders(wv_id) {
    return this._http.get(`${this._api.API_URL_ROOT}/core/wave-pick/v1/wvHdr/${wv_id}/orders`, { headers: this.AuthHeader })
      .map((res: Response) => res.json());
  }

  getWarepickSKU(wv_id, odr_id, sku) {
    return this._http.get(`${this._api.API_URL_ROOT}/core/wave-pick/v1/wvHdr/${wv_id}/items?odr_id=${odr_id}&sku=${sku}`, { headers: this.AuthHeader })
      .map((res: Response) => res.json());
  }

  updateWarePick(wv_id, data) {
    return this._http.put(`${this._api.API_URL_ROOT}/core/wave-pick/v1/wvHdr/${wv_id}/continue-picking`, data ,{ headers: this._Func.AuthHeaderPostJson() })
      .map((res: Response) => res.json());
  }
}
