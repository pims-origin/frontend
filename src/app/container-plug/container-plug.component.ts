import {Component, OnInit} from "@angular/core";
import {Http, Headers} from "@angular/http";
import { Router, RouteParams, RouterLink, ROUTER_DIRECTIVES } from '@angular/router-deprecated';
import {CORE_DIRECTIVES, FORM_DIRECTIVES,ControlGroup,FormBuilder,Control,ControlArray,Validators} from '@angular/common';
import {API_Config} from "../common/core/API_Config";
import {UserService} from "../common/users/users.service";
import {Functions} from "../common/core/functions";
import {WMSMessages} from "../common/directives/messages/messages";
import {WMSBreadcrumb} from '../common/directives/directives';
import {TOOLTIP_DIRECTIVES} from "ng2-bootstrap/ng2-bootstrap";
import {ContainerPlugServices} from '../container-plug/container-plug-service';
import {ValidationService } from '../common/core/validator';
declare var jQuery:any;

@Component({
  selector: 'container-plug',
  directives: [TOOLTIP_DIRECTIVES, CORE_DIRECTIVES, FORM_DIRECTIVES, WMSMessages, WMSBreadcrumb],
  providers : [FormBuilder,ValidationService,ContainerPlugServices],
  templateUrl: 'container-plug.component.html',
})
export class ContainerPlugComponent{

  private messages;
  private showLoadingOverlay = false;
  private bayList = [];
  private showError = false;
  form: ControlGroup;
  private popupBayDetail;
  private datetimePickerStart;
  private datetimePickerEnd;
  private inputStart;
  private inputEnd;
  private listCustomer = [];
  private listContainer = [];
  private whs_id;
  private containerId;
  private bayId;

  constructor(
    private _func: Functions,
    private _API: API_Config,
    private _containerPlugServices: ContainerPlugServices,
    private _Valid: ValidationService,
    private fb: FormBuilder,
    private http: Http
  ){
    this.whs_id = localStorage.getItem('whs_id');
    this.getCustomersByWH();
    this.getListContainer();
    this.getBayList();
    this.buildForm();
  }

  ngAfterViewInit () {
    var that = this;
    this.datetimePickerStart = jQuery('#datetimepicker1');
    this.datetimePickerEnd = jQuery('#datetimepicker2');
    this.inputStart = this.datetimePickerStart.find('input');
    this.inputEnd = this.datetimePickerEnd.find('input');

    this.datetimePickerStart.datetimepicker({format: 'MM/DD/YYYY HH:mm', collapse: false, sideBySide: true, allowInputToggle: true, showClose: true})
      .on('dp.change', function() {
        var startVal = that.inputStart.val();
        (<Control>that.form.controls['start']).updateValue(startVal);
        that.calculateDuration();
      })

    this.datetimePickerEnd.datetimepicker({format: 'MM/DD/YYYY HH:mm', collapse: false, sideBySide: true, allowInputToggle: true, showClose: true})
      .on('dp.change', function() {
        var endVal = that.inputEnd.val();
        (<Control>that.form.controls['end']).updateValue(endVal);
        that.calculateDuration();
      })

    this.popupBayDetail = jQuery('#bay-detail');
  }

  private getCustomersByWH () {
    let params='?whs_id=' + this.whs_id + "&limit=10000";
    this._containerPlugServices.getCustomersByWH(params)
      .subscribe(
        data => {
          this.listCustomer = data.data;
        },
        err => {
        }
      );
  }

  private getListContainer() {
    this._containerPlugServices.getListContainer()
      .subscribe(
        data => {
          this.listContainer = data.data;
        },
        err => {
        }
      );
  }

  private showMessage(msgStatus, msg) {
      this.messages = {
          'status': msgStatus,
          'txt': msg
      }
      jQuery(window).scrollTop(0);
  }

  private calculateDuration() {
    var min = '',
        startVal, endVal, startTime, endTime, duration;
    startVal = this.inputStart.val();
    endVal = this.inputEnd.val();
    if(this.bayAction == 'update') {
      if(!endVal) {
        (<Control>this.form.controls['end']).setErrors({'required': true});
      }
    }
    else {
      (<Control>this.form.controls['end']).setErrors(null);
    }
    if(startVal && endVal) {
      (<Control>this.form.controls['end']).setErrors(null);
      startTime = new Date(startVal).getTime();
      endTime = new Date(endVal).getTime();
      duration = endTime - startTime;
      if(duration >= 0) {
        min = (duration/1000/60).toString();
      }
      else {
        (<Control>this.form.controls['end']).setErrors({'invalid': true});
      }
    }
    (<Control>this.form.controls['duration']).updateValue(min);
  }

  private getBayList() {
    this.showLoadingOverlay = true;
    this._containerPlugServices.getBayList().subscribe(
      data => {
        for(let key in data.data) {
          this.bayList.push(data.data[key]);
        }
        this.showLoadingOverlay = false;
      },
      err => {
        this.showMessage('danger', this._func.parseErrorMessageFromServer(err));
        this.showLoadingOverlay = false;
      }
    );
  }

  private popupTitle;
  private bayAction;
  showBayDetail(bay) {

    this.containerId = '';
    this.bayId = bay.bay_id;
    if(bay.bay_sts == 'A') { // Available
      this.popupTitle = 'New container plug';
      this.bayAction = 'new';
    }
    else {
      this.popupTitle = 'Update container plug';
      this.bayAction = 'update';
    }

    // reset form
    this.showError = false;
    (<Control>this.form.controls['ctnr_id']).updateValue('');
    (<Control>this.form.controls['cus_id']).updateValue('');

    this.datetimePickerStart.data("DateTimePicker").date(null);
    this.datetimePickerEnd.data("DateTimePicker").date(null);
    (<Control>this.form.controls['start']).updateValue('');
    (<Control>this.form.controls['end']).updateValue('');

    // show bay detail
    (<Control>this.form.controls['cc_sts']).updateValue('O');
    if(bay.container) {
      this.containerId = bay.container.cc_id;
      (<Control>this.form.controls['ctnr_id']).updateValue((bay.container.ctnr_id || '').toString());
      (<Control>this.form.controls['cus_id']).updateValue((bay.container.cus_id || '').toString());
      if(bay.container.start) {
        (<Control>this.form.controls['start']).updateValue(bay.container.start);
        this.datetimePickerStart.data("DateTimePicker").date(bay.container.start);
      }
      if(bay.container.end) {
        (<Control>this.form.controls['end']).updateValue(bay.container.end);
        this.datetimePickerEnd.data("DateTimePicker").date(bay.container.end);
      }
      (<Control>this.form.controls['temp']).updateValue((bay.container.temp || '').toString());
      this.calculateDuration();
    }
    this.popupBayDetail.modal('show');
  }

  private buildForm() {
      this.form = this.fb.group({
          ctnr_id: ['', this._Valid.customRequired],
          cus_id: ['', this._Valid.customRequired],
          start: ['', this._Valid.customRequired],
          end: [''],
          duration: [''],
          temp: [''],
          cc_sts: ['O'],
      });
  }

  onSubmit(form) {
    this.showError = true;
    if (this.form.valid) {
      if(this.containerId) {
        this.update(this.containerId, JSON.stringify(this.form.value));
      }
      else {
        this.addNew(this.bayId, JSON.stringify(this.form.value));
      }
    }
  }

  addNew(bayId, stringData) {
        let that = this;
        this._containerPlugServices.addNew(bayId, stringData).subscribe(
            data => {
                this.showLoadingOverlay = false;
                window.location.reload();
            },
            err => {
                this.showLoadingOverlay = false;
                this.showMessage('danger', this._func.parseErrorMessageFromServer(err));
            }
        );
    }

    update(containerID, stringData) {
        let that = this;
        this._containerPlugServices.update(containerID, stringData).subscribe(
            data => {
                this.showLoadingOverlay = false;
                window.location.reload();
            },
            err => {
                this.showLoadingOverlay = false;
                this.showMessage('danger', this._func.parseErrorMessageFromServer(err));
            }
        );
    }
}
