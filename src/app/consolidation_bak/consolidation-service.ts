import {Component, Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Http, Response} from '@angular/http';
import {API_Config, Functions} from '../common/core/load';

@Injectable()
export class ConsolidationServices {

  public AuthHeader = this._Func.AuthHeader();
  public AuthHeaderPost = this._Func.AuthHeaderPost();
  private apiService = this._API.API_GOODS_RECEIPT_MASTER;

  constructor(private _Func: Functions, private _API: API_Config, private http: Http)
  {
  }

  /*
   *
   * Search function
   * */
  SearchLocation(whsId,param)
  {
    return this.http.get(this._API.API_Warehouse + "/" + whsId + "/locations" + param,{ headers: this.AuthHeader })
      .map((res: Response) => res.json());
  }

  getItemLocation(idLocations,params)
  {
    return this.http.get(this.apiService+"/consolidation_loc/"+idLocations+params,{ headers: this.AuthHeader })
      .map((res: Response) => res.json());
  }

  getListItemOfPallet(palletId,params)
  {
    return this.http.get(this.apiService+"/consolidation_plt/"+palletId+params,{ headers: this.AuthHeader })
      .map((res: Response) => res.json());
  }

  consolidationbyLocation(params)
  {
    return this.http.put(this.apiService+'/consolidation_loc',params,{ headers: this._Func.AuthHeaderPostJson() })
      .map((res: Response) => res.json().cartons);
  }

  consolidationbyPallet(params)
  {
    return this.http.put(this.apiService+'/consolidation_plt',params,{ headers: this._Func.AuthHeaderPostJson() })
      .map((res: Response) => res.json().cartons);
  }

  SearchPallet(params)
  {
    return this.http.get(this.apiService+"/pallets"+params,{ headers: this.AuthHeader })
      .map((res: Response) => res.json());
  }

  activeLoc(loc_id){
    // # /v1/locations/{locationId}/changeLocationStatus
    return this.http.put(this._API.API_GOODS_RECEIPT_MASTER+'/locations/'+loc_id+'/changeLocationStatus','',{ headers: this._Func.AuthHeaderPostJson() })
      .map((res: Response) => res.json().data);
  }


}
