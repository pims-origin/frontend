import {Injectable} from '@angular/core';
import { FORM_DIRECTIVES,ControlGroup,FormBuilder, Control ,Validators} from '@angular/common';

@Injectable()
export class ErrorMessageForControlService {

    /*
     generate messages array for controls
     input:
     form (controlGroup)
     msgArr
     output: will add for controls
     */
    _genErrMsgForMultiControls(form:ControlGroup) {
        let n = this;
        Object.keys(form.controls).forEach(function (controlName) {
            let control = form.controls[controlName];
            n._genErrMsgForControl(control);
        });
    }

    _genErrMsgForControl(control) {
        let controlErrors = control.errors; // is object errors: {required: true, invalidSpace: true}
        let n = this;
        if(controlErrors) { //there are errors
            controlErrors['error_messages'] = []; // {required: true, invalidSpace: true}
            Object.keys(this.errorMsg).forEach(function (msgName) {
                if(controlErrors[msgName]) {
                    controlErrors['error_messages'].push(n.errorMsg[msgName]);
                }

            })
        }
        return true;
    }


    public errorMsg =
    {
        'required': 'is required.',
        'invalidSpace': 'has invalid space.',
        'invalidEmailAddress': 'must be email address.', // add so on
        'invalidInt': 'must be integer.',
        'invalidWarehouseCode': 'is invalid.',
        'invalidDecimalNumber': 'is invalid. Maximum is 999.99',
        'isZero' : 'must be greater than 0',
    };
}
    
