import {Component, OnInit} from "@angular/core";
import {Http, Headers} from "@angular/http";
import { Router, RouteParams, RouterLink, ROUTER_DIRECTIVES } from '@angular/router-deprecated';
import { CORE_DIRECTIVES, FORM_DIRECTIVES } from '@angular/common';
import {API_Config} from "../../../common/core/API_Config";
import {UserService} from "../../../common/users/users.service";
import {Functions} from "../../../common/core/functions";
import {WMSMessages} from "../../../common/directives/messages/messages";
import {WMSBreadcrumb} from '../../../common/directives/directives';
import {WarehouseLayoutServices} from '../wh-layout-service';
import {TOOLTIP_DIRECTIVES} from "ng2-bootstrap/ng2-bootstrap";

declare var jQuery: any;

@Component({
    selector: 'aisle-detail',
    directives: [TOOLTIP_DIRECTIVES, CORE_DIRECTIVES, FORM_DIRECTIVES, WMSMessages, WMSBreadcrumb],
    providers : [WarehouseLayoutServices],
    templateUrl: 'aisle-detail.component.html',

})
export class AisleDetail{

    private messages;
	  private showLoadingOverlay = false;
	  private hasWarehouseGraphicPermission = false;
	  private aisleData;
	  private listCustomer = [];
	  private aisleID;
	  private closeListCustomer = false;

    constructor (private _http: Http, private _router:Router, private _user: UserService, private _func: Functions, private _API_Config: API_Config, private params: RouteParams, private _warehouseLayoutServices: WarehouseLayoutServices) {
	  	this.aisleID = this.params.get('id');
	  	this.checkPermission();
	  }

	  private showMessage(msgStatus, msg) {
	      this.messages = {
	          'status': msgStatus,
	          'txt': msg
	      }
	      jQuery(window).scrollTop(0);
	  }

	  private checkPermission() {
	    this.showLoadingOverlay = true;
	    this._user.GetPermissionUser().subscribe(
	      data => {
	        this.hasWarehouseGraphicPermission = this._user.RequestPermission(data,'viewWarehouseLayout');

	        if(!this.hasWarehouseGraphicPermission) {
	            this._router.parent.navigateByUrl('/deny');
	        }
	        else {
	          this.getAisleDetail();
	        }
	        this.showLoadingOverlay = false;
	      },
	      err => {
	        this.showMessage('danger', this._func.parseErrorMessageFromServer(err));
	        this.showLoadingOverlay = false;
	      }
	    );
	  }

	  private getAisleDetail() {
	    this.showLoadingOverlay = true;
	    this._warehouseLayoutServices.getAisleDetail(this.aisleID).subscribe(
	      data => {
	        // console.log('success', data);
	        try {
		        var tempData = data.data['layout'][this.aisleID],
		        		tempArr = [],
		        		dataCustomer = data.data['customer'];
		        for(let key in dataCustomer) {
		          this.listCustomer.push(dataCustomer[key]);
		        }
		        for(let level in tempData) {
	            tempArr.push(tempData[level]);
	          }
	          this.aisleData = tempArr;
		        this.showLoadingOverlay = false;
	        }
	        catch(err) {};
	      },
	      err => {
	        this.showMessage('danger', this._func.parseErrorMessageFromServer(err));
	        this.showLoadingOverlay = false;
	      }
	    );
	  }

	  private locationDetail;
	  private locationName;
	  private showDetail(locID, locName) {
	  	this.locationName = locName;
	  	this.showLoadingOverlay = true;
	    this._warehouseLayoutServices.getLocationDetail(locID).subscribe(
	      data => {
	        this.locationDetail = data.data.items;
	        jQuery('#location-detail').modal('show');
	        this.showLoadingOverlay = false;
	      },
	      err => {
	        this.showMessage('danger', this._func.parseErrorMessageFromServer(err));
	        this.showLoadingOverlay = false;
	      }
	    );
	  }
}
