import {Component} from '@angular/core';
import {Router, RouterLink, RouteConfig, ROUTER_DIRECTIVES} from '@angular/router-deprecated';
import {PutAwayListComponent} from "./putaway-list/putaway-list.component";
import {InventoryListComponent} from "./inventory-list/inventory-list.component";
import {InventoryUpdateComponent} from "./inventory-update/inventory-update.component";




@Component ({
  selector: 'putaway',
  directives: [ROUTER_DIRECTIVES],
  template: `<router-outlet></router-outlet>`,
})

@RouteConfig([
  { path: '/', component:PutAwayListComponent  , name: 'PutAway List', useAsDefault: true},
  { path: '/inventory-update-list', component:InventoryListComponent  , name: 'Inventory Update List'},
  { path: '/inventory-update/:id', component:InventoryUpdateComponent  , name: 'Inventory Update', data: {Action: 'Update'}},

])
export class PutAwayComponent {

}
