import {Component, Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {API_Config, Functions} from '../../common/core/load';

@Injectable()
export class EDIServices {

    public AuthHeader = this._Func.AuthHeader();
    public AuthHeaderPost = this._Func.AuthHeaderPost();

    constructor(private _Func: Functions, private _API: API_Config, private http: Http) {

    }

    getWHSList(){
        return  this.http.get(this._API.API_Warehouse, {headers: this.AuthHeader})
            .map(res => res.json());
    }

    getCustomersByWH($params) {
        return  this.http.get(this._API.API_Customer_List_All + $params + '&sort[cus_name]=asc' , {headers: this.AuthHeader})
            .map(res => res.json());
    }

    newEDIT(data){
        return this.http.post(this._API.API_EDI, data, { headers: this._Func.AuthHeaderPostJson() })
            .map((res: Response) => res.json().data);
    }

    updateEDIT(data){
        return this.http.put(this._API.API_EDI, data, { headers: this._Func.AuthHeaderPostJson() })
            .map((res: Response) => res.json().data);
    }


    getEDI_Detail(ediId){
        return  this.http.get(this._API.API_EDI+'/'+ediId, {headers: this.AuthHeader})
            .map(res => res.json().data);
    }

    EDITList(){

        let data=[

            {
                "edi_key":888,
                "description":"Item master",
                "value":0,
            },
            {
                "edi_key":856,
                "description":"ASN",
                "value":0,
            }
            ,
            {
                "edi_key":861,
                "description":"GR report",
                "value":0,
            }
            ,
            {
                "edi_key":940,
                "description":"Shipping Order",
                "value":1,
            }
            ,
            {
                "edi_key":945,
                "description":"Order Notice",
                "value":0,
            }
            ,
            {
                "edi_key":858,
                "description":"Bill of Lading",
                "value":0,
            }
            ,
            {
                "edi_key":846,
                "description":"Inventory report",
                "value":0,
            }
            ,
            {
                "edi_key":810,
                "description":"Invoice",
                "value":0,
            }
            ,
            {
                "edi_key":997,
                "description":"Acknowledgment",
                "value":0,
            }


        ];

        return data;

    }



}
