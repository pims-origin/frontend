import {Component} from '@angular/core';
import {Router, RouterLink, RouteConfig, ROUTER_DIRECTIVES} from '@angular/router-deprecated';
import {WMSBreadcrumb} from "../common/directives/wms-breadcrumb/wms-breadcrumb";
import {CCNotifyListComponent} from "./cc-notify-list/cc-notify-list.component";
import {CruCCNotifyComponent} from "./cru-cc-notify-location/cru-cc-notify.component";


@Component ({
    selector: 'cycle-count-notify-management',
    directives: [
      ROUTER_DIRECTIVES,
      WMSBreadcrumb
    ],
    template: `<router-outlet></router-outlet>`,
})

@RouteConfig([
    {
    path: '/',
    component: CCNotifyListComponent,
    name: 'Cycle Count Notify List',
    data:{
      action:'list'
    },
    useAsDefault: true
    },
    {
        path: '/add-location',
        component: CruCCNotifyComponent,
        name: 'Add Cycle Count Notify Location',
        data:{
            action:'add'
        }
    }
])

export class CycleCountNotifyManagementComponent {

}
