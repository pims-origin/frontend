import {Directive, ElementRef, HostListener, SkipSelf, Host, Optional, OnInit} from '@angular/core';
declare var $:any;
@Directive({
    selector: '[autocomplete-directive]',
    host:{
        '(keydown)': 'inputChanged($event)'
    }
})
export class AutocompleteDirective {

    constructor(private el:ElementRef) {}

    @HostListener('focus', ['$event'])
    inputChanged(event) {

        if (!$(this.el.nativeElement).siblings(".dropdown-menu:visible").size()) {
            $(this.el.nativeElement).closest('.dropdown-menu').show();
            $(this.el.nativeElement).parent('.dropdown').addClass('open');
        }

    }

    @HostListener('focusout', ['$event'])
    inputfocusChanged(event) {
        // $(this.el.nativeElement).closest('.dropdown-menu').hide(); $(this.el.nativeElement).parent('.dropdown').removeClass('open');
    }
}
