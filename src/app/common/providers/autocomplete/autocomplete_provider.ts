/*============= Autocomplete Provider ========================
 *Author : @tien.nguyen
 *Status : in progress
 * ====================================*/
import {Component,Injectable} from '@angular/core';
import { ControlGroup, ControlArray, Control} from '@angular/common';
import {FormBuilderFunctions} from '../../../common/core/formbuilder.functions';
import {Functions} from '../../../common/core/load';
declare var jQuery: any;
@Injectable()
export class AutoCompleteProvider {
    constructor(private fbdFun:FormBuilderFunctions,private _globalFun:Functions) {
    }
    /*==================================
     * Check Key exist
     *================================= */
    public checkKeyExist(controlArray:ControlArray, row, str:string,
                         searchdata_array:Array<any>,
                         fieldname,
                         keys:Array<any>=[],
                         _optinalFunction) {
        let string = str;
        for (let i in searchdata_array) {
            let item = searchdata_array[i];
            if (string == this._globalFun.trim(item[fieldname])) {
                this.selectedItem(controlArray,row, item,keys,_optinalFunction);
                return;
            }
        }
        this.fbdFun.setErrorsControlItem(controlArray, row, fieldname, {invaildValue: true});
    }


    /*========================================
     * Auto select item if match
     * ======================================*/
    public selectedItem(controlArray:ControlArray, row, item, keys:Array<any>=[],_optinalFunction) {

        for (let i in keys){
            this.fbdFun.setValueControlItem(controlArray, row, keys[i]['control'],
                keys[i]['key_value'] ? keys[i]['key_value'] : keys[i]['control']);
        }

        if(_optinalFunction){
            _optinalFunction();
        }

    }

}


