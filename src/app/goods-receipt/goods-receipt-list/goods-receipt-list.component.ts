import {Component, DynamicComponentLoader, ElementRef, Injector} from '@angular/core';
import { FORM_DIRECTIVES,ControlGroup,FormBuilder, Control ,Validators} from '@angular/common';
import { Http } from '@angular/http';
import { Router, RouterLink, ROUTER_DIRECTIVES, RouteParams } from '@angular/router-deprecated';
import 'rxjs/add/operator/map';
import {API_Config} from "../../common/core/API_Config";
import {Functions} from "../../common/core/functions";
import {GoodsReceiptServices} from "../goods.receipt.service";
import {JwtHelper} from "angular2-jwt/angular2-jwt";
import {WMSPagination, AdvanceTable, WMSMessages, WMSBreadcrumb, ReportFileExporter, WMSMultiSelectComponent} from '../../common/directives/directives';
import { OrderBy } from "../../common/pipes/order-by.pipe";
import {UserService} from "../../common/users/users.service";
import { DataShareService } from '../../common/services/data-share.service';
declare var jQuery: any;
declare var jsPDF: any;
declare var saveAs: any;

@Component ({
    selector: 'goods-receipt-list',
    directives: [AdvanceTable, WMSMessages, WMSBreadcrumb, ReportFileExporter, WMSPagination, WMSMultiSelectComponent],
    templateUrl: 'goods-receipt-list.component.html',
    pipes: [OrderBy],
    providers: [GoodsReceiptServices],
})

export class GoodsReceiptListComponent {

    private urlAPIGR = '';
    private tableID = 'gr-list';
    private headerURL = this._API_Config.API_User_Metas + '/gr';
    private headerDef = [{id: 'ver_table', value: 9},
                      {id: 'ck', name: '', width: 30},
                      {id: 'icon', name: '', width: 40, unsortable: true},
                      {id: 'gr_sts', name: 'Status', width: 70},
                      {id: 'gr_hdr_num', name: 'Goods Receipt #', width: 150},
                      {id: 'asn_number', name: 'ASN Number', width: 130, unsortable: true},
                      {id: 'item_ttl', name: '# of SKUs', width: 90, unsortable: true},
                      {id: 'gr_act_ctn_ttl', name: '# of Cartons', width: 90, unsortable: true},
                      {id: 'customer', name: 'Customer', width: 130, unsortable: true},
                      {id: 'container', name: 'Carrier', width: 80, unsortable: true},
                      {id: 'ref_code', name: 'Ref Code', width: 90, unsortable: true},
                      {id: 'user', name: 'User', width: 130, unsortable: true},
                      {id: 'created_at', name: 'Created Date', width: 130, unsortable: true},
                      {id: 'gr_hdr_act_dt', name: 'Putaway Date', width: 130, unsortable: true},
                      {id: 'gr_hdr_ept_dt', name: 'Expected Date', width: 130, unsortable: true},
                      {id : 'asn_hdr_act_dt', name: 'Goods Receipt Date', width: 180, unsortable: true}
                    ];
    private pinCols = 5;
    private rowData: any[];
    private getSelectedRow;
    private objSort;
    private searchParams;

    private headerExport = ['Good Receipt Number', 'Status', 'Container', 'ASN Number' ,'# of SKUs', '# of CTNs', 'Customer', 'REF Code', 'Number of Expected Cartons', 'Number of Actual Cartons', 'X-Dock', 'Damaged'];
    private dataExport: any[];
    private fileName:string = 'Goods-receipt.csv';
    private exportAPIUrl: string = '';

    private whs_id: any;
    private Pagination;
    private totalCount = 0;
    private tmpPageCount;
    private pageCount=0;
    private ItemOnPage=0;
    private currentPage = 1;
    private perPage = 20;
    private numLinks = 3;
    private messages:any;
    // private flagLoading = false;
    // private flagLoading2 = false;
    private dataListGoodReceipt = [];
    private dataListCustomer = [];
    private dataListGRStatus = [];
    private showLoadingOverlay;
    private timeoutHideMessage;
    private hasEditBtn:any;
    private hasViewPut = false;
    private hasSavePut = false;
    private queryParams;
    public refreshMultiSelect = false;

    // Construct
    constructor (
        private _goodsReceiptServices: GoodsReceiptServices,
        private _func: Functions,
        private _API_Config: API_Config,
        private _http: Http,
        public jwtHelper: JwtHelper,
        private _router:Router,
        private _user: UserService,
        private dataShareService: DataShareService,
    ) {
        this.urlAPIGR = this._API_Config.API_Goods_Receipt;
        this._getQueryParams();
        // Check permission
        this.checkPermission();
        this.whs_id = localStorage.getItem('whs_id');
        this.getCustomersByUserWH();
        this.getASNTypes();
    }

    ngOnDestroy() {
        this._resetQueryParams();
    }

    private _resetQueryParams() {
        this.queryParams = null;
        this.dataShareService.data = '';
    }

    private _getQueryParams() {
        // get params from Dashboard
        const queryString = this.dataShareService.data;

        this.queryParams = this._func.parseQueryString(queryString);
        if(queryString.length) {
            if(this.searchParams) {
                this.searchParams += "&" + queryString;
            } else {
                this.searchParams = "&" + queryString;
            }
        }
    }

    // Check Permission
    private checkPermission() {

        this._user.GetPermissionUser().subscribe(

            data => {
                this.hasViewPut = this._user.RequestPermission(data, 'viewPutaway');
                this.hasSavePut = this._user.RequestPermission(data, 'saveAndPrintPutaway');
                this.hasEditBtn = this._user.RequestPermission(data, 'editGoodsReceipt');
                var flag = this._user.RequestPermission(data,'viewGoodsReceipt');

                if(!flag) {
                    this._router.parent.navigateByUrl('/deny');
                } else {
                    this.getGoodsReceiptStatus();
                }
            },
            err => {

                this.showMessage('danger', this._func.parseErrorMessageFromServer(err));

            }
        );
    }

    private showMessage(msgStatus, msg) {

        this.messages = {
            'status': msgStatus,
            'txt': msg
        };
        jQuery(window).scrollTop(0);
    }

    private requestPermission(permission, permission_req) {

        if (permission && permission.length) {

            for (var per of permission) {

                if (per.name == permission_req) {

                    return true;
                }
            }

        }

        return false;

    }

    // Format data for ag-grid
    private createRowData(data) {
        var rowData: any[] = [];

        // Check data
        if (typeof data.data != 'undefined') {

          data = this._func.formatData(data.data);
          this.dataExport = [];
          for (var i = 0; i < data.length; i++) {

            this.dataExport.push([
                  data[i].gr_hdr_num,
                  data[i].gr_sts_name,
                  data[i].ctnr_num,
                  data[i].asn_hdr_num,
                data[i].item_ttl,
                data[i].gr_ctn_ttl,
                  data[i].cus_code,
                  data[i].asn_hdr_ref,
                  data[i].gr_ctn_ttl,
                  data[i].gr_act_ctn_ttl,
                  data[i].gr_crs_doc,
                  data[i].gr_dtl_is_dmg == '0' ? 'No' : 'Yes',
                  data[i].user_name,
                  data[i].gr_hdr_ept_dt,
                  data[i].asn_hdr_act_dt
            ]);

            let iconHTML = "";
            if(data[i].gr_dtl_is_dmg == '1'){
              iconHTML += "<div class='img-style'><img src='assets/images/damage.png' alt='Damaged' class='img-responsive' title='Damaged'/></div>"
            }
            if(data[i].gr_dtl_disc == '1'){
              iconHTML += "<div class='img-style'><img src='assets/images/desc.png' alt='Discrepancy' class='img-responsive' title='Discrepancy'/></div>"
            }
            rowData.push({
              gr_hdr_num: '<a href = "#/goods-receipt/' + data[i].gr_hdr_id + '">' + data[i].gr_hdr_num + '</a>',
              icon: iconHTML,
              gr_sts: data[i].gr_sts_name,
              container: data[i].ctnr_num,
              asn_number: data[i].asn_hdr_num,
                item_ttl: data[i].item_ttl,
                ctn_ttl: data[i].gr_ctn_ttl,
              customer: data[i].cus_code,
              ref_code: data[i].asn_hdr_ref,
              user: data[i].user_name,
              num_of_ctns: data[i].gr_ctn_ttl,
                gr_act_ctn_ttl: data[i].gr_act_ctn_ttl,
              x_dock: data[i].gr_crs_doc,
              damaged: data[i].gr_dtl_is_dmg == '0' ? 'No' : 'Yes',
              gr_hdr_id: data[i].gr_hdr_id,
                gr_hdr_num_text: data[i].gr_hdr_num,
                created_from: data[i].created_from,
                created_at: data[i].created_at,
                gr_hdr_act_dt: data[i].gr_hdr_act_dt,
                gr_hdr_ept_dt: data[i].gr_hdr_ept_dt,
                asn_hdr_act_dt : data[i].asn_hdr_act_dt
            })
          }
        }

        this.rowData = rowData;
    }

    // Export data to PDF format
    /*private exportPdf(){
        var doc = new jsPDF('p', 'pt');
        doc.autoTable(this.headerExport, this.dataExport, {
              theme: 'grid'
        });
        doc.save('good_receipt_list.pdf');
    }*/

    /*private exportCsv(){
        var content = this.dataExport;
        var finalVal = this.headerExport.join(',') + '\n';

        for (var i = 0; i < content.length; i++) {
            var value = content[i];

            for (var j = 0; j < value.length; j++) {
                var innerValue =  value[j]===null?'':value[j].toString();
                var result = innerValue.replace(/"/g, '""');
                if (result.search(/("|,|\n)/g) >= 0)
                    result = '"' + result + '"';
                if (j > 0)
                    finalVal += ',';
                finalVal += result;
            }

            finalVal += '\n';
        }
        var blob = new Blob([finalVal], { type: 'text/csv;charset=utf-8;' });

        if (navigator.msSaveBlob) { // IE 10+
            navigator.msSaveBlob(blob, finalVal);
        } else {
            var downloadLink = jQuery('<a style="visibility:hidden;" href="data:text/csv;charset=utf-8,' + encodeURIComponent(finalVal) +'" download="dataGoodReceipt.csv"></a>').appendTo('body');
            downloadLink[0].click();
            downloadLink.remove();
        }
    }*/

    private getGoodsReceiptStatus() {
      this._goodsReceiptServices.getGoodsReceiptStatus()
            .subscribe(
                data => {
                    this.dataListGRStatus = data.data;
                    let preSelectStatus = false;
                    for (let i = 0; i < this.dataListGRStatus.length; i++) {
                        const status = this.dataListGRStatus[i];
                        status['key'] = status['gr_sts_code'];
                        status['value'] = status['gr_sts_name'];
                        status['checked'] = status['gr_sts_checked'];
                        if (status.checked) {
                            preSelectStatus = true;
                        }
                    }
                    if (preSelectStatus) {
                        setTimeout(() => {
                            this.search();
                        }, 500);
                    } else {
                        this.getListGoodsReceipt(1);
                    }
                }
            );
    }

    // Get ASN
    private getListGoodsReceipt(page = null) {
        this.showLoadingOverlay = true;
        if(!page) page = 1;
        var params="?page="+page+"&limit="+this.perPage;
        params += "&whs_id=" + this.whs_id;
        if(this.objSort && this.objSort['sort_type'] != 'none') {
          params += '&sort['+ this.objSort['sort_field']+ ']='+ this.objSort['sort_type'];
        }
        else {
          params += '&sort[created_at]=desc';
        }
        if(this.searchParams) {
          params += this.searchParams;
        }
        // update exportAPIUrl
        this.exportAPIUrl = this._API_Config.API_Goods_Receipt + params + '&export=1';

        this._goodsReceiptServices.getListGoodsReceipt(params)
            .subscribe(
                data => {

                    // jQuery("#ck-first").prop('checked', false);
                    this.dataListGoodReceipt = data.data;
                    this.initPagination(data);
                    this.createRowData(data);
                    this.showLoadingOverlay = false;
                    // this.removeCheckAll(jQuery('#gr-list'));
                },
                err => {
                    // this.flagLoading = false;
                    // this.flagLoading2 = false;
                    this.parseError(err);
                    this.showLoadingOverlay = false;
                },
                () => {

                    // this.flagLoading = false;
                    // this.flagLoading2 = false;
                }
            );
    }

    // Get customers
    private getCustomersByUserWH() {
        var $params = '?limit=10000';
        this._goodsReceiptServices.getCustomersByWH($params)
            .subscribe(
                data => {
                    this.dataListCustomer = data.data;
                },
                err => {
                    this.parseError(err);
                },
                () => { }
            );
    }

    arrASNType = [];
    getASNTypes() {
        this._goodsReceiptServices.getASNTypeList().subscribe(
            res => {
                this.arrASNType = res.data;
            },
            err => {
                this.parseError(err);
            });
    }

    private filterList(pageNumber) {
        this.getListGoodsReceipt(pageNumber);
    }

    private getPage(pageNumber) {
        let arr=new Array(pageNumber);
        return arr;
    }

    // Set params for pagination
    private initPagination(data){
        var meta = data.meta;
        this.Pagination=meta['pagination'];
        this.Pagination['numLinks']=3;
        this.Pagination['tmpPageCount']=this._func.Pagination(this.Pagination);

    }

    // Event when user filter form
    private onPageSizeChanged ($event) {
        this.perPage = $event.target.value;
        if((this.Pagination['current_page'] - 1) * this.perPage >= this.Pagination['total']) {
            this.currentPage = 1;
        }
        else {
            this.currentPage = this.Pagination['current_page'];
        }
        this.getListGoodsReceipt(this.currentPage);
    }

    // Edit
    // Navigation to edit page
    private edit() {

        this.messages = false;
        this.getSelectedRow = 'edit';
    }

    afterGetSelectedRow($event) {
      var listSelectedItem = $event.data;
      switch ($event.action) {
        case 'edit':
          if (listSelectedItem.length > 1) {
            this.messages = this._func.Messages('danger', this._func.msg('CF004'));
          }
          else {
            if (listSelectedItem.length < 1) {
              this.messages = this._func.Messages('danger', this._func.msg('CF005'));
            }
            else {

                var status = '';
                for (var i = 0; i < this.dataListGoodReceipt.length; i++) {

                    if (listSelectedItem[0].gr_hdr_id  == this.dataListGoodReceipt[i]['gr_hdr_id']) {

                        status = this.dataListGoodReceipt[i]['gr_sts_name'].toLowerCase();
                        break;
                    }
                }

                if (status == 'receiving') {

                    this._router.parent.navigateByUrl('/goods-receipt/' + listSelectedItem[0].gr_hdr_id + '/edit');

                } else {

                    this.messages = this._func.Messages('danger', this._func.msg('GR002'));

                }

            }
          }
          break;
          case 'assignCartonToPallet':

              if (listSelectedItem.length > 1) {
                  this.messages = this._func.Messages('danger', this._func.msg('CP002'));
              }
              else {
                  if (listSelectedItem.length < 1) {
                      this.messages = this._func.Messages('danger', this._func.msg('CP003'));
                  }
                  else {

                      if (listSelectedItem[0]['created_from'].toUpperCase()   == 'GUN' || listSelectedItem[0]['created_from'].toUpperCase()   == 'WAP') {

                          this.messages = this._func.Messages('danger', this._func.msg('WMS004'));
                          break;
                      }

                      var is_pending = 0;
                      var status = '';
                      var flagCreateFromWAP = false;
                      for (var i = 0; i < this.dataListGoodReceipt.length; i++) {

                          if ((listSelectedItem[0].gr_hdr_id  == this.dataListGoodReceipt[i]['gr_hdr_id']) && (this.dataListGoodReceipt[i]['created_from'] == 'WAP')) {

                              flagCreateFromWAP = true;
                          }
                          if (listSelectedItem[0].gr_hdr_id  == this.dataListGoodReceipt[i]['gr_hdr_id']) {

                              is_pending = this.dataListGoodReceipt[i]['is_pending'];
                              status = this.dataListGoodReceipt[i]['gr_sts_name'].toLowerCase();
                              break;
                          }
                      }

                      if (status == 'receiving') {

                          if (flagCreateFromWAP) {

                              this.messages = this._func.Messages('danger', this._func.msg('CLGR002'));
                          } else {

                              this._router.parent.navigateByUrl('/goods-receipt/' + listSelectedItem[0].gr_hdr_id + '/assign-x-dock');
                          }

                      } else if (status == 'received') {

                          this.messages = this._func.Messages('danger', this._func.msg('CP004'));
                      }

                  }
              }
              break;
          case 'actionPrint':

              if (listSelectedItem.length > 1) {
                  this.messages = this._func.Messages('danger', this._func.msg('GR0014'));
              }
              else {
                  if (listSelectedItem.length < 1) {
                      this.messages = this._func.Messages('danger', this._func.msg('GR0015'));
                  } else {

                      this.printPutAway(listSelectedItem);
                      /*
                      if (listSelectedItem[0]['created_from'].toLowerCase() ==  'wap'){

                          this.messages = this._func.Messages('danger', this._func.msg('GR0013'));
                      }else  {

                          this.printPutAway(listSelectedItem);
                      }
                      */

                  }


              }
              break;

          case 'actionApprove':
              if (listSelectedItem.length < 1) {
                  this.messages = this._func.Messages('danger', this._func.msg('GRA002'));
              } else {
                  if (listSelectedItem.length > 1) {
                      this.messages = this._func.Messages('danger', this._func.msg('GRA001'));
                  } else {
                      let invalidRG = listSelectedItem.some((item) => item.gr_sts != 'Receiving');
                      if (invalidRG) {
                          this.messages = this._func.Messages('danger', this._func.msg('GRA003'));
                      } else {
                          let listSelectedId = listSelectedItem.map(item => item.gr_hdr_id);
                          const dataObj = {
                              gr_ids: listSelectedId
                          };

                          this._goodsReceiptServices.approveGoodsReceipt(JSON.stringify(dataObj)).subscribe(res => {
                              if (res.data.message) {
                                  this.messages = this._func.Messages('success', res.data.message);
                              }
                          })
                      }
                  }
              }

              break;
        case 'delete':
          break;
      }

      this.getSelectedRow = false;
    }

    private assignCartonToPallet() {

        this.getSelectedRow = 'assignCartonToPallet';
    }

    private doSort(objSort) {
      this.objSort = objSort;
      this.getListGoodsReceipt(this.Pagination.current_page);
    }

    // Search
    private search () {
        this._resetQueryParams();
        this.searchParams = '';
        let params_arr = jQuery("#form-filter").serializeArray() ;
        for (let i in params_arr) {
            if (params_arr[i].value == "") continue;
            this.searchParams +="&" +  encodeURIComponent(params_arr[i].name.trim()) + "=" + encodeURIComponent(params_arr[i].value.trim());
        }
        this.getListGoodsReceipt(1);

    }

    // Reset form
    private reset() {
        this._resetQueryParams();
        jQuery("#form-filter input[type=text], #form-filter select").each(function( index ) {
            jQuery(this).val("");
        });
        this.refreshMultiSelect = true;
        this.searchParams = '';
        this.getListGoodsReceipt(1);

    }

    // Show error when server die or else
    private parseError (err) {

        this.messages = {'status' : 'danger', 'txt' : this._func.parseErrorMessageFromServer(err)};
    }

    private expandTable = false;
    private viewListFullScreen() {
      this.expandTable = true;
      setTimeout(() => {
        this.expandTable = false;
      }, 500);
    }


    private actionPrint() {

        this.getSelectedRow = 'actionPrint';
    }

    actionApprove() {
        this.getSelectedRow = 'actionApprove';
    }

    private printPutAway(listSelectedItem) {

        try {

            var that = this;
            that.messages = false;

            var xhr = new XMLHttpRequest();
            xhr.open("GET", this.urlAPIGR + "/" + listSelectedItem[0].gr_hdr_id + "/print", true);
            xhr.setRequestHeader("Authorization", 'Bearer ' +this._func.getToken());
            xhr.responseType = 'blob';

            xhr.onreadystatechange = function () {
                if(xhr.readyState == 2) {
                    if(xhr.status == 200) {
                        xhr.responseType = "blob";
                    } else {
                        xhr.responseType = "text";
                    }
                }
                // If we get an HTTP status OK (200), save the file using fileSaver
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {

                        var blob = new Blob([this.response], {type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'});
                        saveAs.saveAs(blob, listSelectedItem[0]['gr_hdr_num_text'] + ' Putaway List.pdf');

                    } else {
                        let errMsg = '';
                        if(xhr.response) {
                            try {
                                var res = JSON.parse(xhr.response);
                                errMsg = res.errors && res.errors.message ? res.errors.message : xhr.statusText;
                            }
                            catch(err){errMsg = xhr.statusText}
                        }
                        else {
                            errMsg = xhr.statusText;
                        }
                        if(!errMsg) {
                            errMsg = that._func.msg('VR100');
                        }
                        that.messages = that._func.Messages('danger', errMsg);
                    }
                }
            };

            xhr.send();
        } catch (err) {

            this.messages = this._func.Messages('danger', this._func.msg('VR100'));
        }
    }

    private updateMessages(messages) {
        this.messages = messages;
    }
}
