import {Component,Input, Output,EventEmitter} from '@angular/core';
import {ControlGroup, FORM_DIRECTIVES, FormBuilder, Validators, Control, ControlArray} from "@angular/common";
import {WMSMessages} from "../../../common/directives/messages/messages";
import {ValidationService} from "../../../common/core/validator";
import {thirdPartyServices} from "./third-party-service";
import { API_Config,Functions} from '../../../common/core/load';
import {PaginatePipe,PaginationControlsCmp,PaginationService} from '../../../common/directives/directives';
import {GoBackComponent} from "../../../common/component/goBack.component";
import { GoogleAPI } from '../../../common/services/google-api'
declare var $:any;
declare var jQuery:any;

@Component ({
    selector: 'third-party-form-directive',
    directives:[GoBackComponent,PaginationControlsCmp,WMSMessages],
    providers: [ValidationService, FormBuilder, thirdPartyServices, GoogleAPI],
    pipes: [PaginatePipe],
    templateUrl: 'template.html'
})

export class ThirdPartyFormDirective {

    private cruThirdPartyTitle;
    private thirdPartyAction;
    public Loading = [];
    private whs_id = localStorage.getItem('whs_id');
    private autocomplete;

    @Input() IsView: string;
    @Input() cusId: string;
    @Output() messages =  new EventEmitter();
    @Output() success =  new EventEmitter();
    @Output() showLoadingOverlay=  new EventEmitter();

    @Input('openForm') set openForm(value:any){

        if(value){

            if(this.ThirdPartyForm){
                this.resetForm();
            }else{
                this.buildThirdPartyForm();
            }

            this.getListType();
            this.GetCountry();
        }

    }

    @Input('action') set action(action:any){

        this.thirdPartyAction = action;
        switch (action) {
            case "add":
                this.cruThirdPartyTitle = 'Add Third Party';
                break;
            case "view":
                this.cruThirdPartyTitle = 'View Third Party';
                break;
            case "edit":
                this.cruThirdPartyTitle = 'Edit Third Party';
                break;
        }

    }

    constructor(
        private fb:FormBuilder, private _Valid:ValidationService,
        private _Func: Functions,
        private _API: API_Config,
        private googleAPI: GoogleAPI,
        private _ODFSerivce:thirdPartyServices) {}


    ThirdPartyForm: ControlGroup;
    private showErrorThirdParty = false;
    private cruThirdPartyMessage;
    private listType = [];
    private Country = [];
    private StateProvinces = [];
    private showForm = true;

    getListType() {
        this.listType = [
            {code: 'CUS', name: 'End Customer'},
            {code: 'SHP', name: 'Shipper'}
        ];
    }

    GetCountry() {
        let param='?sort[sys_country_name]=asc&limit=500';
        this._ODFSerivce.getCountry(param).subscribe(
            data=>{
                this.Country=data;
            }
        );
    }

    GetState(idCountry, idState: string = '') {
        let param="?sort[sys_state_name]=asc&limit=500";
        this._ODFSerivce.State(idCountry,param).subscribe(
            data=>{
                this.StateProvinces = data;
            },
            err => {},
            () => {
                if (idState) {
                    this.setState(idState);
                }
            }
        );
    }

    ChangeCountry(idCountry) {
        if(idCountry) {
            this.GetState(idCountry);
        }
        else{
            this.StateProvinces= [];
        }
        (<Control>this.ThirdPartyForm.controls['state']).updateValue('');
    }

    resetForm(item={}) {

        this.showForm = false;
        setTimeout(() => {
            this.showForm = true;
            this.buildThirdPartyForm(item);
            this.showErrorThirdParty = false;
            this.cruThirdPartyMessage = false;
        });

        setTimeout(()=>{
            $('#thirdPartyForm').modal('show');
        },300);

    }

    buildThirdPartyForm(item = {}) {

            this.ThirdPartyForm =this.fb.group({
                type: ['CUS',Validators.compose([Validators.required])],
                name: [item['name'],Validators.compose([Validators.required, this._Valid.validateSpace, this._Valid.invalidNameStr])],
                phone: [item['phone']],
                mobile: [item['mobile']],
                add_1: [item['add_1'],Validators.compose([Validators.required, this._Valid.validateSpace])],
                add_2: [item['add_2']],
                country: [item['country'] ? item['country'] : '',Validators.compose([Validators.required])],
                state: [item['state'] ? item['state'] : '',Validators.compose([Validators.required])],
                city: [item['city'],Validators.compose([Validators.required, this._Valid.validateSpace])],
                zip: [item['zip'],Validators.compose([Validators.required, this._Valid.validateSpace])],
                ct_first_name: [item['ct_first_name'],this._Valid.invalidNameStr],
                ct_last_name: [item['ct_last_name'],this._Valid.invalidNameStr],
                des: [item['des']],
            });

            $('#thirdPartyForm').modal('show');
            setTimeout(() => {
                this.addAutoComplete();
            }, 300);
    }

    SaveThirdParty(){

        this.showErrorThirdParty=true;
        this.cruThirdPartyMessage = false;

        if(this.ThirdPartyForm.valid){
            let data=this.ThirdPartyForm.value;
            for(let x in data) {
                data[x] = data[x] ? data[x].trim() : data[x];
            }
            let data_json = JSON.stringify(data);
            if(this.thirdPartyAction == 'edit') {
                this.updateThirdParty(data_json);
            }
            else {
                this.createThirdParty(data_json);
            }
        }
    }


    createThirdParty(data){
        this.showLoadingOverlay.emit(true);
        this._ODFSerivce.createThirdParty(this.cusId, data).subscribe(
            data => {
                this.showLoadingOverlay.emit(false);
                this.messages.emit(this._Func.Messages('success', this._Func.msg('VR107')));
                this.success.emit(true);
                $('#thirdPartyForm').modal('hide');
            },
            err => {
                this.cruThirdPartyMessage = this._Func.Messages('danger', this._Func.parseErrorMessageFromServer(err));
                this.showLoadingOverlay.emit(false);
            }
        );
    }

    updateThirdParty(data={}) {
        this.showLoadingOverlay.emit(true);
        this._ODFSerivce.updateThirdParty(this.cusId,data['tp_id'], data).subscribe(
            data => {
                this.showLoadingOverlay.emit(false);
                this.messages.emit(this._Func.Messages('success', this._Func.msg('VR109')));
                this.success.emit(true);
                $('#add-thirdParty').modal('hide');
            },
            err => {
                this.cruThirdPartyMessage = this._Func.Messages('danger', this._Func.parseErrorMessageFromServer(err));
                this.showLoadingOverlay.emit(false);
            }
        );
    }

    private isNumber(evt, int:boolean = false) {
        this._Valid.isNumber(evt, int);
    }


    private parseError(err){
        this.messages.emit({'status' : 'danger', 'txt' : this._Func.parseErrorMessageFromServer(err)});
    }

    private addAutoComplete() {
        const that = this;
        this.googleAPI.initAutoComplete('autocomplete').then(() => {
            that.autocomplete = that.googleAPI.autocomplete;
            that.autocomplete.addListener('place_changed', function () {
                const place = that.autocomplete.getPlace();
                let addr = '';
                let state = '';
                let zipCode = '';
                let city = '';
                let country = '';
                // var length = place.address_components.length;
                console.log('place', place);
                if (place.address_components) {
                    for (let i = 0; i < place.address_components.length; i++) {
                        const addressType = place.address_components[i].types[0];
                        if (addressType === 'street_number') { // number address
                            addr += place.address_components[i].short_name;
                        }
                        if (addressType === 'route') { // street
                            addr += ' ' + place.address_components[i].short_name;
                        }
                        if (addressType === 'locality') { // city
                            city = place.address_components[i].short_name;
                        }
                        if (addressType === 'administrative_area_level_1') { // state
                            state = place.address_components[i].short_name;
                        }
                        if (addressType === 'postal_code') { // zip code
                            zipCode = place.address_components[i].short_name;
                        }
                        if (addressType === 'country') { // country
                            country = place.address_components[i].short_name;
                        }
                    }

                    // let location = place.geometry.location;
                    const paramObj = {
                        add_1: addr,
                        city: city,
                        zip: zipCode,
                        country: country,
                        state: state
                    };
                    that._autoCompleteUpdateForm(that.ThirdPartyForm, paramObj);
                }

            });
            that._turnOffAutofill();
        });
    }

    private _turnOffAutofill() {
        setTimeout(() => {
            jQuery('.address').attr('autocomplete', 'new-password');
        }, 500);
    }

    private _autoCompleteUpdateForm(form, paramObj) {
        for (const key in paramObj) {
            if (paramObj.hasOwnProperty(key)) {
                this.setField(key, paramObj[key]);
            }
        }
        this.GetState(paramObj.country, paramObj.state);
    }

    getZipcode(evt) {
        this.trimFields(['city', 'zip']);
        const data = this.ThirdPartyForm.value;
        if (data['country'] === 'US') {
            const zip = evt.target.value;
            this.googleAPI.getAddress(zip).then(address => {
                console.log(address);
                if (address['city'].toLowerCase() !== data['city'].toLowerCase() || address['state'].toLowerCase() !== data['state'].toLowerCase()) {
                    (<Control>this.ThirdPartyForm.controls['zip']).setErrors({invaildValue: true});
                }
            });
        }
    }

    public setState(val: string) {
        this.setField('state', val);
    }

    public setField(fieldname: string, value: any) {
        (<Control>this.ThirdPartyForm.controls[fieldname]).updateValue(value);
    }

    resetZipcode() {
        this.setField('zip', '');
    }

    trimFields(fields: string[]) {
        fields.forEach(field => {
            const val = this.ThirdPartyForm.value[field];
            this.setField(field, val? val.trim() : val);
        })
    }
}
