import {Component, Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {API_Config, Functions} from '../../common/core/load';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class DimensionServices {

    public AuthHeader = this._Func.AuthHeader();
    public AuthHeaderPost = this._Func.AuthHeaderPost();

    constructor(private _Func: Functions,
                private _API: API_Config,
                private http: Http) {

    }

    createDimesion(data){
        return this.http.post(this._API.API_Dimension, data, { headers: this._Func.AuthHeaderPostJson() })
            .map((res: Response) => res.json().data);    }

    updateDimension(data,pack_ref_id){
        return this.http.put(this._API.API_Dimension+'/'+pack_ref_id, data, { headers: this._Func.AuthHeaderPostJson() })
            .map((res: Response) => res.json().data);
    }

    getDimesnionDetail(pack_ref_id){
        return  this.http.get(this._API.API_Dimension+'/'+ pack_ref_id, {headers: this.AuthHeader})
            .map(res => res.json().data);
    }

    getDimensionlist($param){
        return  this.http.get(this._API.API_Dimension + '/packedcartondimensionlist'+$param, {headers: this.AuthHeader})
            .map(res => res.json());
        // let data = [
        //     {pack_ref_id:1,length:10,width:10,height:20,dimension:'test',pack_type:'Corrugated'},
        //     {pack_ref_id:2,length:20,width:30,height:10,dimension:'test2',pack_type:'Carton'},
        //     {pack_ref_id:3,length:50,width:100,height:10,dimension:'test3',pack_type:'Folding'},
        //     {pack_ref_id:4,length:50,width:100,height:10,dimension:'test4',pack_type:'Pallet'}
        // ];
        // return data;

    }

    getDimensionlistURL(): string{
        return  this._API.API_Dimension + '/packedcartondimensionlist';
    }

    getPackType($param){
        return  this.http.get(this._API.API_Packtype + '/dropdown'+$param, {headers: this.AuthHeader})
            .map(res => res.json());
    }

}
