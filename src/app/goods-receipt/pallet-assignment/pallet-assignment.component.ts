import {Component, DynamicComponentLoader, ElementRef, Injector} from '@angular/core';
import { Router, RouterLink, ROUTER_DIRECTIVES, RouteParams } from '@angular/router-deprecated';
import 'ag-grid-enterprise/main';
import 'rxjs/add/operator/map';
import {Functions} from "../../common/core/functions";
import {GoodsReceiptServices} from "../goods.receipt.service";
import {JwtHelper} from "angular2-jwt/angular2-jwt";
import {API_Config} from "../../common/core/API_Config";
import {WMSMessages, WMSBreadcrumb} from '../../common/directives/directives';
import {UserService} from "../../common/users/users.service";
import {WarningBoxPopup} from "../../common/popup/warning-box-popup";
import {BoxPopupService} from "../../common/popup/box-popup.service";
declare var jQuery: any;
declare var saveAs: any;
declare var swal: any;
@Component({
    selector: 'pallet-assignment',
    directives: [ROUTER_DIRECTIVES, WMSMessages, WMSBreadcrumb],
    templateUrl: 'pallet-assignment.component.html',
    providers: [GoodsReceiptServices],

})
export class PalletAssignmentComponent{

    private gr_id: any;
    private dataGoodReceipt = [];
    private dataGrDetail = [];
    private messages:any;
    private tmpItem = [[]];
    private urlAPIGR: any;
    private hasSavePrintBtn = false;
    private fields = [
        'pallet',
        'carton_per_pallet'
    ];
    private listNull = {};
    private timeoutHideMessage;
    private showLoadingOverlay = false;
    // Construct
    constructor (
        private _router:Router,
        private _goodsReceiptServices: GoodsReceiptServices,
        params: RouteParams,
        private _Func: Functions,
        public jwtHelper: JwtHelper,
        private _API_Config: API_Config,
        private _boxPopupService:BoxPopupService,
        private _user: UserService
    ) {
        // Check permission
        this.checkPermission();
        this.urlAPIGR = this._API_Config.API_Goods_Receipt;
        this.gr_id = params.get("id");
        this.getGoodsReceiptById();
    }

    // Check Permission
    private checkPermission() {

        this._user.GetPermissionUser().subscribe(

            data => {

                this.hasSavePrintBtn = this._user.RequestPermission(data, 'saveAndPrintPutaway');
            },
            err => {

                this.showMessage('danger', this._Func.parseErrorMessageFromServer(err));

            }
        );
    }

    private showMessage(msgStatus, msg) {

        this.messages = {
            'status': msgStatus,
            'txt': msg
        };
        jQuery(window).scrollTop(0);
    }

    private requestPermission(permission, permission_req) {

        if (permission && permission.length) {

            for (var per of permission) {

                if (per.name == permission_req) {

                    return true;
                }
            }

        }

        return false;

    }

    // Get goods receipt by id
    private getGoodsReceiptById() {

        try {

            this._goodsReceiptServices.getGoodsReceiptById(this.gr_id)
                .subscribe(
                    data => {

                        this.dataGoodReceipt = data.data;

                        if ( this.dataGoodReceipt['gr_sts_code'].toUpperCase() == 'RE') {

                            this._router.parent.navigateByUrl('/goods-receipt/');
                        }

                        if (data.data['is_pending'] == 1 || data.data['is_pending']) {

                            this._router.parent.navigateByUrl('/goods-receipt/' + data.data['gr_hdr_id'] + '/damaged-carton');
                        }
                        this.dataGrDetail =  this.remove_act_ctn_except_xdoc_is_zero(this.dataGoodReceipt['gr_details']);
                        this.dataGrDetail =  this.removeCalcelItem(this.dataGrDetail);
                        this.addItem(this.dataGrDetail.length);
                    },
                    err => {

                        this.parseError(err);
                    },
                    () => {
                    }
                );
        } catch (err) {

            this.messages = this._Func.Messages('danger', this._Func.msg('VR100'));
        }

    }

    // Save data
    private flagDisable = false;
    private save (flag = '') {

        this.flagDisable = false;
        var tar_get = this;
        this.messages = false;

        jQuery.each(jQuery(".pallet-assign-item input[type=text]"), function( key, object ) {

            tar_get.filterForm(object, 'required_int_greater0');
        });

        var submit = true;
        jQuery.each( this.listNull, function( key, value ) {

            if (value) {

                submit = false;
                return false;
            }
        });

        if (!submit) {

            return false;
        }

        if (!this.ckTotalAssigned()) {

            this.messages = this._Func.Messages('danger', 'Assigned Cartons must be equal Total');
            return false;
        }

        var paramsJson =  this.buildParams();

        try {
            this.showLoadingOverlay = true;
            this._goodsReceiptServices.savePalletAssignment(this.gr_id, JSON.stringify(paramsJson))
                .subscribe(
                    data => {
                      this.showLoadingOverlay = false;
                        if (flag != 'spr') {

                            this.messages = this._Func.Messages('success', this._Func.msg('VR107'));
                        }

                    },
                    err => {
                        this.showLoadingOverlay = false;
                        this.parseError(err);
                    },
                    () => {

                        // Save and print PutAway
                        if (flag == 'spr') {

                            tar_get.callPrint();

                        } else {

                            this.flagDisable = true;
                            setTimeout(function () {

                                tar_get._router.parent.navigateByUrl('/goods-receipt');
                            }, 1100);
                        }
                    }
                );

        } catch (err) {

            this.messages = this._Func.Messages('danger', this._Func.msg('VR100'));
        }

    }

    // event check all
    private ckAll($event) {

        if (jQuery($event.target).prop('checked')) {

            jQuery($event.target).parents("table").find("tbody :checkbox").prop("checked", true);
        } else {

            jQuery($event.target).parents("table").find("tbody :checkbox").prop("checked", false);
        }

    }

    private eventCheck($event) {

        let table = jQuery($event.target).parents("table");
        let checkboxAll = table.find('thead :checkbox');
        table.find('tbody :checkbox:not(:checked)').length ? checkboxAll.prop('checked', false) : checkboxAll.prop('checked', true);

    }

    private callPrint() {

        let that = this;
        let warningPopup = new WarningBoxPopup();
        warningPopup.text = 'Do you want to print the putaway list?';
        this._boxPopupService.showWarningPopup(warningPopup)
            .then(function (dm) {
                if(!dm){
                    that._router.parent.navigateByUrl('/goods-receipt');
                    return;
                }else {

                    that.printPutAway();

                }
            });

    }


    private printPutAway() {

        try {

            var tar_get = this;
            tar_get.flagDisable = false;
            var xhrl = new XMLHttpRequest();

            xhrl.open("GET", this.urlAPIGR + "/" + this.gr_id + "/print", true);
            xhrl.setRequestHeader("Authorization", 'Bearer ' +this._Func.getToken());
            xhrl.responseType = 'blob';

            xhrl.onreadystatechange = function () {
                if(xhrl.readyState == 2) {
                    if(xhrl.status == 200) {
                        xhrl.responseType = "blob";
                    } else {
                        xhrl.responseType = "text";
                    }
                }

                if(xhrl.readyState === 4) {
                    if(xhrl.status === 200 || xhrl.status === 201) {

                        tar_get.messages = tar_get._Func.Messages('success', tar_get._Func.msg('VR107'));

                        var blob = new Blob([this.response], {type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'});
                        saveAs.saveAs(blob, tar_get.dataGoodReceipt['gr_hdr_num'] + '_License_Plate.pdf');

                        setTimeout(function () {

                            tar_get._router.parent.navigateByUrl('/goods-receipt');
                        }, 1100);
                    }else{

                        tar_get.flagDisable = true;
                        let errMsg = '';
                        if(xhrl.response) {
                            try {
                                var res = JSON.parse(xhrl.response);
                                errMsg = res.errors && res.errors.message ? res.errors.message : xhrl.statusText;
                            }
                            catch(err){errMsg = xhrl.statusText}
                        }
                        else {
                            errMsg = xhrl.statusText;
                        }
                        if(!errMsg) {
                            errMsg = tar_get._Func.msg('VR100');
                        }
                        tar_get.showMessage('danger', errMsg);

                        setTimeout( () =>{
                            tar_get._router.parent.navigateByUrl('/goods-receipt');
                        }, 3000);

                    }
                    tar_get.showLoadingOverlay = false;
                }
            };
            xhrl.send();
        } catch (err) {

            this.messages = this._Func.Messages('danger', this._Func.msg('VR100'));
            this.showLoadingOverlay = false;
        }

    }

    // delete item
    private deleteItem ($event) {

        let target = this;
        let ojCkbox =  jQuery("#wrap-pallet-assign").find(":checkbox[id^=item_ck]:checked");
        target.listNull = {};

        if (ojCkbox.length < 1 ) {

            this.messages = this._Func.Messages('danger', this._Func.msg('VR118'));
            return false;
        }

        for (var i = ojCkbox.length - 1; i >= 0; --i) {

            var tmp = ojCkbox[i].id.split("_");
            var total_assignedCarton  = jQuery("span#total_Assigned_" + tmp[2]).html().split("/");
            var pallet = parseInt(jQuery("input[id=pallet_" + tmp[2] + "_" + tmp[3] + "]").val());
            var carton_per_pallet = parseInt(jQuery("input[id=carton_per_pallet_" + tmp[2] + "_" + tmp[3] + "]").val());

            total_assignedCarton[1] = parseInt(total_assignedCarton[1]);
            if (carton_per_pallet != 0 && !isNaN(carton_per_pallet) && !isNaN(pallet) && pallet != 0) {

                total_assignedCarton[1] -= pallet * carton_per_pallet;
            }

            target.tmpItem[tmp[2]].splice(tmp[3], 1);
            if (target.tmpItem[tmp[2]].length == 0) {

                target.addMoreItem(tmp[2]);
            }

            jQuery("span#total_Assigned_" + tmp[2]).html(total_assignedCarton[0] + "/" + total_assignedCarton[1]);

        }

        jQuery("#wrap-pallet-assign").find(":checkbox").prop('checked', false);

    }

    // Check value total
    private ckTotalAssigned() {

        var flag = false;

        // Check header
        for (var i = 0; i < this.dataGrDetail.length; i++ ) {

            var tmp_total_assigned  = jQuery("span#total_Assigned_" + i).html().split("/");

            if (parseInt(tmp_total_assigned[0]) != parseInt(tmp_total_assigned[1])) {

                flag = true;
                break;
            }

        }

        // Check each row item
        for (var i = 0; i < this.dataGrDetail.length; i++ ) {

            var total_assigned = 0;
            var tmp_total_assigned  = jQuery("span#total_Assigned_" + i).html().split("/");

            // Get each row item
            jQuery.each(jQuery("input[id^=pallet_" + i + "_]"), function( key, value ) {

                var pallet = parseInt(jQuery("input[id=pallet_" + i + "_" + key + "]").val());
                var carton_per_pallet = parseInt(jQuery("input[id=carton_per_pallet_" + i + "_" + key + "]").val());

                // Check input
                if (carton_per_pallet != 0 && !isNaN(carton_per_pallet) && !isNaN(pallet) && pallet != 0) {

                    total_assigned += pallet * carton_per_pallet;
                }

            });

            if (total_assigned != parseInt(tmp_total_assigned[0])) {

                flag = true;
                break;
            }

        }

        if (flag) {

            return false;
        }

        return true;
    }

    // Assign params
    private buildParams ($params = "") {

        var arr_item = [];

        for (var i = 0; i < this.dataGrDetail.length; i++ ) {

            var item = {};
            item['item_id'] = this.dataGrDetail[i]['dtl_item_id'];
            item['asn_dtl_id'] = this.dataGrDetail[i]['dtl_asn_dtl_id'];
            item['sku'] = this.dataGrDetail[i]['dtl_sku'];
            item['pallets']  = [];

            // Get each row item
            jQuery.each(jQuery("input[id^=pallet_" + i + "_]"), function( key, value ) {

                var pallet = parseInt(jQuery("input[id=pallet_" + i + "_" + key + "]").val());
                var carton_per_pallet = parseInt(jQuery("input[id=carton_per_pallet_" + i + "_" + key + "]").val());
                var tmp_assign = {};

                // Check input
                if (carton_per_pallet != 0 && !isNaN(carton_per_pallet) && !isNaN(pallet) && pallet != 0) {

                    tmp_assign['pallet'] = pallet;
                    tmp_assign['carton_per_pallet'] = carton_per_pallet;
                }

                if (tmp_assign.hasOwnProperty('pallet')) {

                    item['pallets'].push (tmp_assign);
                }

            });

            if (item['pallets'].length > 0) {

                arr_item.push(item);
            }

        }

        return arr_item;
    }
    private autoAssign($event, index) {

        var parentIndex = jQuery($event.target).closest('table').attr('id').replace("t_wrap_item_", "");

        var tmp_total_assigned  = jQuery("span#total_Assigned_" + parentIndex).html().split("/");


        var total_assigned = 0;
        jQuery.each(jQuery("input[id^=pallet_" + parentIndex + "_]"), function( key, value ) {

            if (key != index) {

                var pallet = parseInt(jQuery("input[id=pallet_" + parentIndex + "_" + key + "]").val());
                var carton_per_pallet = parseInt(jQuery("input[id=carton_per_pallet_" + parentIndex + "_" + key + "]").val());

                if (carton_per_pallet != 0 && !isNaN(carton_per_pallet) && !isNaN(pallet) && pallet != 0) {

                    total_assigned += pallet * carton_per_pallet;


                }
            }

        });

        var tmp_total =  parseInt(tmp_total_assigned[0]) - total_assigned ;

        var number = Math.floor(tmp_total/parseInt($event.target.value));
        if (tmp_total > 0 && $event.target.value != '' && $event.target.value != '0' && number != 0 && this.validateInt($event.target.value) ) {

            jQuery("#pallet_" + parentIndex + "_" + index).val(number);

        } else {

            jQuery("#pallet_" + parentIndex + "_" + index).val('');
        }

    }

    private calTotalAssigned($event) {

        var parentIndex = jQuery($event.target).closest('table').attr('id').replace("t_wrap_item_", "");

        /*
        var index = $event.target.id.replace("pallet_" + parentIndex + "_", "");

        /*
        if (/^car/.test($event.target.id)) {

            var index = $event.target.id.replace("carton_per_pallet_" + parentIndex + "_", "");
        }

        var pallet = parseInt(jQuery("#pallet_" + parentIndex + "_" + index).val());
        var carton_per_pallet = parseInt(jQuery("#carton_per_pallet_" + parentIndex + "_" + index).val());

        */
        this.assignedTotal($event, parentIndex);

    }

    private assignedTotal($event, parentIndex) {

        var tmp_total_assigned  = jQuery("span#total_Assigned_" + parentIndex).html().split("/");

        var total_assigned = 0;
        var target = this;
        jQuery.each(jQuery("input[id^=pallet_" + parentIndex + "_]"), function( key, value ) {

            target.listNull["pallet_" + parentIndex + "_" + key + "_total_assigned"] = false;
            target.listNull["carton_per_pallet_" + parentIndex + "_" + key + "_total_assigned"] = false;

            var pallet = parseInt(jQuery("input[id=pallet_" + parentIndex + "_" + key + "]").val());
            var carton_per_pallet = parseInt(jQuery("input[id=carton_per_pallet_" + parentIndex + "_" + key + "]").val());

            if (carton_per_pallet != 0 && !isNaN(carton_per_pallet) && !isNaN(pallet) && pallet != 0) {

                total_assigned += pallet * carton_per_pallet;


            }

        });

        if (total_assigned <= tmp_total_assigned[0] ) {

            jQuery("span#total_Assigned_" + parentIndex).html(tmp_total_assigned[0] +"/" + total_assigned);
        } else {

            jQuery("span#total_Assigned_" + parentIndex).html(tmp_total_assigned[0] +"/0");
        }


        if (total_assigned == parseInt(tmp_total_assigned[0])) {



        } else {

            // jQuery("span#total_Assigned_" + parentIndex).html(tmp_total_assigned[0] +"/0");
            this.listNull[$event.target.id + "_total_assigned"] = true;

        }

    }

    private addItem($length) {

        for( var i = 0; i < $length; i++) {

            this.tmpItem[i] = [];
            this.addMoreItem(i);
        }

    }

    // Add more item in table
    private addMoreItem ($parentIndex) {

        var index = this.tmpItem[$parentIndex].length;
        this.tmpItem[$parentIndex].push(index);
        this.resetItem($parentIndex, index);

    }

    private eAddMoreItem($event) {

        jQuery($event.target).parents("table").find(":checkbox").prop('checked', false);

        var $parentIndex = $event.target.id.replace("event_add_", "");
        var tmp_total_assigned  = (jQuery("span#total_Assigned_" + $parentIndex).html().split("/"));

        if (parseInt(tmp_total_assigned[1]) < parseInt(tmp_total_assigned[0])) {

            this.addMoreItem($parentIndex);
        }

        return false;
    }

    private resetItem($parentIndex, index) {

        var itemID = [
            'pallet_' + $parentIndex + "_" + index,
            'carton_per_pallet_' + $parentIndex + "_" + index
        ];

        for (var i = 0; i < itemID.length; i++) {

            jQuery("#" + itemID[i]).val("");
        }

    }

    private checkInputNumber (evt, flagNumber = true) {

        var charCode = (evt.which) ? evt.which : evt.keyCode;

        if (charCode > 31 && (charCode < 48 || charCode > 57)){

            if(flagNumber) {

                evt.preventDefault();
            }
            else {

                if(charCode != 46) {

                    evt.preventDefault();
                }
            }
        }
    }

    /*======================================
    * remove_act_ctn_except_xdoc_is_zero
    *====================================== */
    private remove_act_ctn_except_xdoc_is_zero(data:Array<any>=[]){

       let tmp=data.slice();

       data.forEach((item)=>{
           if(item['act_ctn_except_xdoc']==0){
               let index=tmp.indexOf(item);
               if (index > -1) {
                   tmp.splice(index, 1);
               }
           }
       })

        return tmp;

    }

    /*======================================
     * remove_act_ctn_except_xdoc_is_zero
     *====================================== */
    private removeCalcelItem(data:Array<any>=[]){

        let tmp= data.slice();

        data.forEach((item)=>{
            if(item['dtl_gr_dtl_sts'].toLowerCase() == 'cancelled'){
                let index=tmp.indexOf(item);
                if (index > -1) {
                    tmp.splice(index, 1);
                }
            }
        })

        return tmp;

    }

    // Filter form only show choose one
    private filterForm ($target, flag = 'required') {

        try {

            switch (flag) {

                case 'required_int_greater0':

                    this.listNull[$target.id + "_required"] = false;
                    this.listNull[$target.id + "_greater0"] = false;
                    this.listNull[$target.id + "_int"] = false;

                    if ($target.value == "" && flag == 'required_int_greater0') {

                        this.listNull[$target.id + "_required"] = true;

                    } else if(!this.validateInt($target.value)) {

                        this.listNull[$target.id + "_int"] = true;

                    } else if(parseInt($target.value) < 1) {

                        this.listNull[$target.id + "_greater0"] = true;

                    }

                    break;
                default:
                    break;

            }

            return false;

        } catch (err) {

            return false;
        }

    }

    // Validate Int
    private validateInt(number = '') {

        var re = /^([0-9]*)$/;
        var re2 = /^0([0-9]*)$/;
        if (re.test(number)) {

            if (number.length > 1 && re2.test(number)) {

                return false;

            }

            return true;
        }

        return false;
    }

    // Show error when server die or else
    private parseError (err) {

        this.messages = {'status' : 'danger', 'txt' : this._Func.parseErrorMessageFromServer(err)};
    }

  private printCartonLabel(){
    swal({
      text: 'This will be developed in the future.',
      type: 'warning',
      showCancelButton: false,
      showConfirmButton: true,
      confirmButtonText: 'OK',
      confirmButtonClass: 'btn btn-primary',
      width: '280'
    })
  }

}

