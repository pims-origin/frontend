import {Input, Output, EventEmitter, Component, ElementRef} from "@angular/core";
import 'rxjs/add/operator/filter';
import 'rxjs/Rx';
/**
 * Created by admin on 6/16/2016.
 */
@Component({
  selector: 'auto-complete',
  host        : {
    '(document:click)': 'handleClick($event)',
  },
  template: `
    <input type="text" class="form-control" [(ngModel)]= "query" (keyup)="filterData()" name="autocomplete" placeholder="auto-complete" (click)="filterData()">
      <div *ngIf="filterList.length > 0">
        <ul class="autocomplete">
          <li *ngFor="let item of filterList">
            <a (click)="select(item)">{{item}}</a>
          </li>
        </ul>
      </div>`
})
export class AutoCompleteComponent {
  @Input() data;
  @Output() model = new EventEmitter<string>(); // TypeScript supports initializing fields

  public query: string = '';
  public filterList: Array<any> = [];
  public elementRef;

  constructor(private _myElement: ElementRef){

  }

  private filterData() {
    if (this.query !== ''){
      this.filterList = this.data.filter(function(el){
        return el.toLowerCase().indexOf(this.query.toLowerCase()) > -1;
      }.bind(this));
    }else{
      this.filterList = [];
    }
  }

  private select(item){
    this.query = item;
    this.model.emit(this.query);
    this.filterList = [];
  }

  private handleClick(event){
    var clickedComponent = event.target;
    var inside = false;
    do {
      if (clickedComponent === this._myElement.nativeElement) {
        inside = true;
      }
      clickedComponent = clickedComponent.parentNode;
    } while (clickedComponent);
    if(!inside){
      this.filterList = [];
    }
  }

}
