Follow the following steps and you're good to go!

1: Install GIT and nodejs

https://git-scm.com/downloads

https://nodejs.org/

2: Clone repo

3: Install packages

```
npm install
```

```
bower install
```

4: Start server (includes auto refreshing) and gulp watcher
```
npm start
```

Open browser to [`http://localhost:3001`](http://localhost:3001)

You need to instal CORS - a Chrome extension to run the project on local.

--Good Luck--

