import {Component, Input} from '@angular/core';

import {WMSPagination} from '../../common/directives/directives';
import {DashboardServices} from '../dashboard.service';
import {Functions} from '../../common/core/load';

declare var Chart:any;

@Component({
	selector: 'histogram-capacity-chart',
	templateUrl: 'histogram-capacity-chart.html',
	directives: [WMSPagination],
	providers: [DashboardServices]
})

export class HistogramCapacityChart {
	private VIEW_TYPES:Array<string> = ['Day', 'Week', 'Month', 'Year'];
	private VIEW_BY:Array<string> = ['Min', 'Average', 'Max'];
	// https://www.w3schools.com/colors/colors_picker.asp?colorhex=005400
	private CHART_COLORS = ['#36c6d3', 'rgb(0, 128, 0)'];
	private perPage:number = 12;
	private pagination:any;
	private curViewBy:string = '';
	private curViewType:string = '';
	private hisCapacities:any;
	private cLabels:Array<string> = [];
	private usedData:Array<any> = [];
	private availableData:Array<any> = [];
	private lineChart:any;
	private barChart:any;
	private whsId:string = '';

	@Input('currentWHSId') set dataExport(value:any) {
		this.whsId = value;
		this.curViewBy = this.VIEW_BY[0];
		this.curViewType = this.VIEW_TYPES[0];
		if (value) {
			this.getHisCapacities();
		}
	}

	constructor(private dashboardServices:DashboardServices, private funcs:Functions) {
	}

	ngAfterViewInit()
	{
		// this.getHisCapacities();
	}

	public drawLineChart() {
		const chartData = {
			labels: this.cLabels,
			datasets: [
				{
					label: 'Used Locations',
					data: this.usedData,
					backgroundColor: this.CHART_COLORS[0],
					borderColor: this.CHART_COLORS[0],
					pointRadius: 3,
					fill: false
				}
			]
		};

		const ctx = document.getElementById('his-cap-line-chart');
		if (ctx == null) {
			return true;
		}
		const that = this;
		if (this.lineChart) this.lineChart.destroy();
		this.lineChart = new Chart(ctx, {
			type: 'line',
			data: chartData,
			options: {
				responsive: true,
				tooltips: {
					/*mode: 'label',
					intersect: true,*/
					callbacks: {
						title: function(tooltipItems, data) {
							const index = tooltipItems[0].index;
							return [
								that.hisCapacities[index]['cur_his'],
								`Total: ${that.funcs.formatSWISNumber(that.hisCapacities[index]['loc_ttl'])}`,
								`Available Locations: ${that.funcs.formatSWISNumber(that.hisCapacities[index]['loc_avai_ttl'])}`
							];
						},
						label: (tooltipItem, data) => {
							return data.datasets[tooltipItem.datasetIndex].label + ": " + this.funcs.formatSWISNumber(tooltipItem.yLabel);
						}
					}
				},
				scales: {
					yAxes: [{
						scaleLabel: {
							display: true,
							labelString: "Capacity"
						},
						ticks: {
							beginAtZero: true,
							callback: function(value, index, values) {
								if (value.toString().indexOf(".") !== -1) {
									return that.funcs.formatSWISNumber(value.toFixed(1));
								}
								return that.funcs.formatSWISNumber(value);
							}
						}
					}]
				}
			}
		});
	}

	public drawBarChart() {
		const chartData = {
			labels: this.cLabels,
			datasets: [
				{
					label: 'Used Locations',
					data: this.usedData,
					backgroundColor: this.CHART_COLORS[0]
				}
			]
		};

		const ctx = document.getElementById('his-cap-bar-chart');
		if (ctx == null) {
			return true;
		}
		const that = this;
		if (this.barChart) this.barChart.destroy();
		this.barChart = new Chart(ctx, {
			type: 'bar',
			data: chartData,
			maintainAspectRatio: false,
			options: {
				responsive: true,
				tooltips: {
					/*mode: 'label',
					intersect: true,*/
					callbacks: {
						title: function(tooltipItems, data) {
							const index = tooltipItems[0].index;
							return [
								that.hisCapacities[index]['cur_his'],
								`Total: ${that.funcs.formatSWISNumber(that.hisCapacities[index]['loc_ttl'])}`,
								`Available Locations: ${that.funcs.formatSWISNumber(that.hisCapacities[index]['loc_avai_ttl'])}`
							];
						},
						label: (tooltipItem, data) => {
							return data.datasets[tooltipItem.datasetIndex].label + ": " + this.funcs.formatSWISNumber(tooltipItem.yLabel);
						}
					}
				},
				scales: {
					xAxes: [{
						barPercentage: 1,
						categoryPercentage: 0.6
					}],
					yAxes: [{
						scaleLabel: {
							display: true,
							labelString: "Capacity"
						},
						ticks: {
							beginAtZero: true,
							callback: function(value, index, values) {
								if (value.toString().indexOf(".") !== -1) {
									return that.funcs.formatSWISNumber(value.toFixed(1));
								}
								return that.funcs.formatSWISNumber(value);
							}
						}
					}]
				}
			}
		});
	}

	private getHisCapacities(pageNum:number = 1) {
		let params = `?type=${this.curViewType.toUpperCase()}&view-by=${this.curViewBy.toUpperCase()}&page=${pageNum}&limit=${this.perPage}`;

		if (!this.pagination) {
			params += '&page_last=1';
		}

		this.dashboardServices.getHisCapacities(this.whsId, params).subscribe(
			data => {
				this.hisCapacities = data.data;

				this.prepareDataForChart();
				this.drawLineChart();
				this.drawBarChart();

				// paging
				this.pagination = data['meta']['pagination'];
				this.pagination['numLinks'] = 2;
				this.pagination['tmpPageCount'] = this.funcs.Pagination(this.pagination);
			},
			err => {
			},
			() => {}
		)

	}

	private prepareDataForChart() {
		this.usedData = this.hisCapacities.map(capacity => {
			return capacity.loc_used_ttl;
		});
		this.availableData = this.hisCapacities.map(capacity => {
			return capacity.loc_avai_ttl;
		});

		switch (this.curViewType.toUpperCase()) {
			case 'DAY':
				this.cLabels = this.hisCapacities.map(capacity => {
					return capacity.cur_day;
				});
				break;
			case 'WEEK':
				this.cLabels = this.hisCapacities.map(capacity => {
					return capacity.cur_day;
				});
				break;
			case 'MONTH':
				this.cLabels = this.hisCapacities.map(capacity => {
					return capacity.cur_his;
				});
				break;
			case 'YEAR':
				this.cLabels = this.hisCapacities.map(capacity => {
					return capacity.cur_his;
				});
				break;
		}
	}

	private changeViewType($event) {
		if (this.curViewType.toUpperCase() === 'DAY') {
			this.curViewBy = 'Min';
		}
		this.pagination = null;
		this.getHisCapacities();
	}

	private changeViewBy($event) {
		this.pagination = null;
		this.getHisCapacities();
	}
}
