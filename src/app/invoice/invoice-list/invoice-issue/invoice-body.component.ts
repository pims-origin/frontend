
import {Component} from '@angular/core';
import {API_Config} from '../../../common/core/API_Config';
import {BaseTable} from '../../../common/core/classes_us/base-table';
import {TableStructure} from '../../../common/directives/directives';

@Component({
    selector: 'invoice-body',
    directives: [
        TableStructure
    ],
    templateUrl: '../../../common/directives/table-structure/table.component.html',
})

export class InvoiceBodyComponent extends BaseTable
{
    public tableID = 'invoiceBody';
    public headerURL = this._API_Config.API_User_Metas + '/rep';

    public headerDef = [
        {
            id: 'ver_table',
            value: 1 + Math.random() * 10000
        },
        {
            id: 'chg_cd',
            name: 'ITEM',
            width: 180,
            unsortable: true
        },
        {
            id: 'chg_cd_desc',
            name: 'DESC',
            width: 280,
            unsortable: true
        },
        {
            id: 'info',
            name: 'INFO',
            width: 260,
            unsortable: true
        },
        {
            id: 'quantity',
            name: 'QTY',
            width: 100,
            unsortable: true
        },
        {
            id: 'chg_cd_uom',
            name: 'UOM',
            width: 200,
            unsortable: true
        },
        {
            id: 'chg_cd_price',
            name: 'PRICE',
            width: 100,
            unsortable: true
        },
        {
            id: 'ccTotal',
            name: 'AMT',
            width: 110,
            unsortable: true
        }
    ];

    private skipPageSize = true;
    private skipExport = true;
    private skipPagination = true;

    private invoiceItems: any;

    constructor (
        private _API_Config: API_Config
    )
    {
        super();

        this.tableComponent = this;
    }

    //**************************************************************************

    public gridUpdate(invoiceData)
    {
        this.rowData = [];
        this.invoiceItems = invoiceData;

        let invoInfo = invoiceData['invoInfo'];

        let tableData = invoInfo['details'][invoiceData.cus_id],
            currencyCode = invoiceData['currencyCode'],
            info = invoInfo['invItemsIDs'],
            orderNumber = invoiceData['orderNumber'],
            containerName = invoiceData['containerName'],
            referenceCode = invoiceData['referenceCode'],
            storRange = invoiceData['storRange'];

        for (var key in tableData) {

            let data = tableData[key],
                infoKey = '',
                caption = '',
                infoItem = '',
                infoValue = '';

            switch (data.chg_cd_type) {
                case 'RECEIVING':

                    infoKey = 'Receiving';
                    infoValue = containerName + ' / ' + referenceCode;

                    break;

                case 'ORD_PROC':

                    caption = 'Order # ';
                    infoKey = 'Order Processing';
                    infoValue = orderNumber;

                    break;
                case 'STORAGE':

                    infoItem = storRange;
                    break;
                default:
                    break;
            }

            if (infoKey != 'Storage' && info.hasOwnProperty(infoKey)) {

                let params = info[infoKey];

                if (params.length == 1) {
                    infoItem = caption + infoValue;
                } else {

                    let link = '/#/invoice/view-details/';

                    link += infoKey == 'Receiving' ? 'receiving' :
                        'order-processing';

                    link += '/' + JSON.stringify(params);

                    infoItem = `
                        <a href="` + link + `"target="_blank">View details</a>
                    `;
                }
            }

            this.rowData.push({
                chg_cd: data.chg_cd_name,
                chg_cd_desc: data.chg_cd_desc,
                info: infoItem,
                quantity: data.quantity,
                chg_cd_uom: data.sys_uom_name,
                chg_cd_price: currencyCode + ' ' + parseFloat(data.rate).toFixed(2),
                ccTotal: parseFloat(data.ccTotal).toFixed(2)
            });
        }
    }

    //**************************************************************************

}


