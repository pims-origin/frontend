import {Component, Input, Injectable} from '@angular/core';
import {PageSizeService} from './page-size-service';

@Component ({
    selector: 'page-size',
    directives:[],
    providers: [
        PageSizeService
    ],
    templateUrl: 'page-size.html',
})

@Injectable()

export class PageSize
{
    @Input() parentComponent: any;

//    --- actions example ---
//
//    private actions = [
//        {
//            id: 'add',
//            title: 'Add',
//            icon: 'icon-plus',
//            action: 'add'
//        },
//        {
//            id: 'action',
//            title: 'Action',
//            icon: 'icon-settings',
//            action: [
//                {
//                    id: 'edit',
//                    title: 'Edit',
//                    icon: 'icon-note',
//                    action: 'edit'
//                },
//                {
//                    id: 'remove',
//                    title: 'Remove',
//                    icon: 'icon-note',
//                    action: 'remove'
//                }
//            ]
//        },
//    ];

    constructor(
        private _pageSizeService: PageSizeService
    )
    {

    }

    //**************************************************************************

    public onPageSizeChanged(value: number)
    {
        this.parentComponent.set('perPage', value);

        let currentPage = this._pageSizeService.pageResize({
            tableComponent: this.parentComponent
        });

        this.parentComponent.set('currentPage', currentPage);
    }

    //**************************************************************************

    public isString(value: string)
    {
        return typeof value === 'string';
    }

    //**************************************************************************

}
