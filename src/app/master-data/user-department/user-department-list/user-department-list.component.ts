/**
 * Created by admin on 12/28/2016.
 */
import {Component} from '@angular/core';
import {Router} from '@angular/router-deprecated';
import {API_Config, UserService, Functions} from '../../../common/core/load';
import {FORM_DIRECTIVES, ControlGroup, FormBuilder, Control} from '@angular/common';
import {Http} from '@angular/http';
import {WMSBreadcrumb} from '../../../common/directives/directives';
import {UserDepartmentService} from "../user-department-service";
import {BoxPopupService} from "../../../common/popup/box-popup.service";

declare var jQuery:any;

@Component({
    selector: 'user-department-list',
    providers: [UserDepartmentService, FormBuilder, BoxPopupService],
    directives: [FORM_DIRECTIVES, WMSBreadcrumb],
    templateUrl: 'user-department-list.component.html',
})

export class UserDepartmentListComponent {
    private userDepartmentList:Array<any> = [];
    private Pagination = [];
    public Loading = [];
    public ListSelectedItem:Array<any> = [];
    public selectedAll:boolean = false;
    private messages;
    private currentPage;
    SearchForm:ControlGroup;
    public showLoadingOverlay = false;
    private hasEditPermission = false;
    private hasCreatePermission = false;
    private viewUser = false;
    private searchCriteria = {};

    constructor(private _http:Http,
                private _Func:Functions,
                private _user:UserService,
                private fb:FormBuilder,
                private _UDS:UserDepartmentService,
                private _router:Router,
                private _boxPopupService:BoxPopupService) {
        this.BuilderSearchForm();
        this.searchCriteria = this._Func.genSearchCriteriaFromForm(this.SearchForm);
        this.checkPermission();
    }

    // Check permission for user using this function page
    private checkPermission() {
        this.showLoadingOverlay = true;
        this._user.GetPermissionUser().subscribe(
            data => {
                this.hasEditPermission = this._user.RequestPermission(data, 'editDepartment');
                this.hasCreatePermission = this._user.RequestPermission(data, 'createDepartment');
                this.viewUser = this._user.RequestPermission(data, 'viewDepartment');
                /* Check orther permission if View allow */
                if (!this.viewUser) {
                    this._router.parent.navigateByUrl('/deny');
                }
                else {
                    this.GetUserDepartmentList();
                }
            },
            err => {
                this.showMessage('danger', this._Func.parseErrorMessageFromServer(err));
                this.showLoadingOverlay = false;
            }
        );
    }

    private showMessage(msgStatus, msg) {
        this.messages = {
            'status': msgStatus,
            'txt': msg
        }
        jQuery(window).scrollTop(0);
    }

    /*
     * Builder form
     * */
    BuilderSearchForm() {
        this.SearchForm = this.fb.group({
            usr_dpm_code: [''],
            usr_dpm_name: ['']
        });
    }

    /*
     * Get GetZoneTypeList
     * */
    private perPage = 20;

    private GetUserDepartmentList(page = null) {
        let param = "?page=" + page + "&limit=" + this.perPage;
        if (Object.keys(this.searchCriteria).length !== 0) // check if searchCriteria Object not empty  ( {} )
        {
            param += '&usr_dpm_code=' + this.searchCriteria['usr_dpm_code'] + '&usr_dpm_name='
                + this.searchCriteria['usr_dpm_name'];
        }
        this.Loading['getList'] = true;
        this.showLoadingOverlay = true;
        if (this.sortQuery != '') {
            param += '&' + this.sortQuery;
        }

        this._UDS.GetUserDepartmentList(param).subscribe(
            data => {

                this.userDepartmentList = data.data;
                this.selectedAll = false;
                this.ListSelectedItem = [];
                //pagin function
                this.Pagination = data['meta']['pagination'];
                this.Pagination['numLinks'] = 3;
                this.Pagination['tmpPageCount'] = this._Func.Pagination(this.Pagination);

                this.Loading['getList'] = false;
                this.showLoadingOverlay = false;
            },
            err => {},
            () => {}
        );
    }

    private filterList(num) {
        this.GetUserDepartmentList(num);
        // this.
        //remove list selected and checkall
        this.selectedAll = false;
        this.ListSelectedItem = [];
        this.currentPage = num;
    }

    private onPageSizeChanged($event) {
        this.perPage = $event.target.value;
        if ((this.Pagination['current_page'] - 1) * this.perPage >= this.Pagination['total']) {
            this.Pagination['current_page'] = 1;
        }
        this.GetUserDepartmentList(this.Pagination['current_page']);
    }


    /*
     * Check All/Off
     * */
    private CheckAll(event) {
        if (event) {
            this.selectedAll = true;
        }
        else {
            this.selectedAll = false;
        }
        // set empty array  ListSelectedItem
        this.ListSelectedItem = [];
        // Loop
        this.userDepartmentList.forEach((item) => {
            item['selected'] = this.selectedAll;
            if (this.selectedAll) {
                // if check all = true , pushing to ListSelectedItem
                this.ListSelectedItem.push(item['usr_dpm_id']);
            }
        });
    }

    /*
     * Selected
     * */
    private Selected(item) {
        let id = item['usr_dpm_id'];
        item.selected = !item.selected;
        this.selectedAll = false;
        if (item.selected) {
            this.ListSelectedItem.push(id);
            this.SetItemListSelected(id, true);
            if (this.ListSelectedItem.length == this.userDepartmentList.length) {
                this.selectedAll = true;
            }
        }
        else {
            let el = this.ListSelectedItem.indexOf(id);
            // remove item out of array
            this.ListSelectedItem.splice(el, 1);
            this.SetItemListSelected(id, false);
        }
    }

    /*
     * Set select item
     * */
    SetItemListSelected(id, value) {
        this.userDepartmentList.forEach((item) => {
            if (item['usr_dpm_id'] == id) {
                item['selected'] = value;
            }
        });
    }

    /*
     * Router edit
     * */
    Edit() {
        if (this.ListSelectedItem.length > 1) {
            this.messages = this._Func.Messages('danger', 'Please choose only one item to edit');
        }
        else {
            if (this.ListSelectedItem.length) {
                this._router.navigateByUrl('/master-data/user-department/' + this.ListSelectedItem[0] + '/edit');
            }
            else {
                this.messages = this._Func.Messages('danger', 'Please choose one item to edit .');
            }

        }
    }

    /*
     * Delete
     * */
    /*private Delete() {
        let that = this;
        if (this.ListSelectedItem.length > 1) {
            this.messages = this._Func.Messages('danger', 'Please choose only one item to delete');
        }
        else {
            if (this.ListSelectedItem.length > 0) {
                let num_of_staff = 0;
                that.userDepartmentList.forEach((item) => {
                    if (item['usr_dpm_id'] == that.ListSelectedItem[0]) {
                        num_of_staff = item['num_of_staff'];
                    }
                });

                if (num_of_staff == 0) {// delete in case of no staff
                    this._boxPopupService.showWarningRemoveItemPopup()
                        .then(function (dm) {
                            if (dm) {
                                that.RemoveItemLocal();
                                that._UDS.DeleteUserDepartment(that.ListSelectedItem[0]).subscribe(
                                    data => {
                                        that.selectedAll = false;
                                        if (that.userDepartmentList.length == 0) {
                                            that.GetUserDepartmentList(this.Pagination['current_page']);
                                        } else {
                                            that.ListSelectedItem = [];
                                        }
                                        that.messages = that._Func.Messages('success', 'The department was removed successfully.');
                                    },
                                    err => console.log(err)
                                );
                            }
                        });
                } else {
                    that.messages = that._Func.Messages('danger', 'The department cannot be removed as it have staff.');
                }

            }
            else {
                this.messages = this._Func.Messages('danger', 'Please choose one item to delete .');
            }

        }
    }*/

    /*RemoveItemLocal() {
        let sumItemDelet = 0;
        for (let el of this.userDepartmentList) {

            if (this.ListSelectedItem[0] == el['usr_dpm_id']) {
                let index = this.userDepartmentList.indexOf(el);
                // remove item out of array
                this.userDepartmentList.splice(index, 1);
                sumItemDelet++;

                break;
            }
            else {
                // console.log(item +' not equal '+ el['usr_dpm_id']);
            }
        }
    }*/

    Search(data:Object):void {
        this.messages = null;
        this.ListSelectedItem = [];
        this.selectedAll = false;
        let param = "?page=" + 1 + "&limit=" + this.perPage;
        this.searchCriteria = this._Func.genSearchCriteriaFromForm(this.SearchForm);
        let enCodeSearchQuery = param + "&usr_dpm_code=" + encodeURIComponent(this.searchCriteria['usr_dpm_code'])
            + "&usr_dpm_name=" + encodeURIComponent(this.searchCriteria['usr_dpm_name']);
        if (this.sortQuery != '') {
            enCodeSearchQuery += '&' + this.sortQuery;
        }

        // let enCodeSearchQuery=this._Func.FixEncodeURI(new_query);
        //let new_query="?whs_code="+data['whs_code'];
        //whs_name[]=name1&whs_short_name[]=short_name&whs_status[]=ok

        this.Loading['searching'] = true;
        this.showLoadingOverlay = true;
        this._UDS.Search(enCodeSearchQuery).subscribe(
            data => {
                this.Loading['searching'] = false;
                this.showLoadingOverlay = false;

                this.userDepartmentList = data.data;
                //pagin function
                this.Pagination = data['meta']['pagination'];
                this.Pagination['numLinks'] = 3;
                this.Pagination['tmpPageCount'] = this._Func.Pagination(this.Pagination);

            },
            err => {
                this.messages = this._Func.Messages('danger', 'Not found');
            },
            () => {}
        );

    }

    /*
     * Reset Form Builder
     * */
    ResetSearch() {
        (<Control>this.SearchForm.controls["usr_dpm_code"]).updateValue("");
        (<Control>this.SearchForm.controls["usr_dpm_name"]).updateValue("");
        this.searchCriteria = this._Func.genSearchCriteriaFromForm(this.SearchForm);
        this.GetUserDepartmentList(this.Pagination['current_page']);
    }

    private FieldsToSortFlag = {
        usr_dpm_id: false,
        usr_dpm_code: false,
        usr_dpm_name: false,
    };
    private countClick = {
        usr_dpm_id: 3,
        usr_dpm_code: 3,
        usr_dpm_name: 3,
    }
    private sortQuery = 'sort[usr_dpm_code]=asc';

    sort(field) {
        if (this.countClick[field] == 3) {
            this.countClick[field] = 0;
        }

        this.countClick[field]++;
        if (this.countClick[field] == 3) {
            this.sortQuery = 'sort[usr_dpm_code]=asc';
        }
        else {
            this.FieldsToSortFlag[field] = !this.FieldsToSortFlag[field];

            this.sortQuery = 'sort[' + field + ']';
            if (this.FieldsToSortFlag[field]) {
                this.sortQuery += '=asc';
            } else {
                this.sortQuery += '=desc';
            }
        }

        if (field) {
            for (let sortField in this.FieldsToSortFlag) {
                if (sortField != field) {
                    this.FieldsToSortFlag[sortField] = false;
                    this.countClick[sortField] = 3;
                }
            }
        }
        this.GetUserDepartmentList(this.Pagination['current_page']);
    }

}