import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { API_Config, Functions } from '../../common/core/load';

@Injectable()
export class LaborTrackingReportService {
  authHeader: any;
  authHeaderPost: any;
  whsId: string;

  constructor(private func: Functions, private api: API_Config, private http: Http) {
    this.authHeaderPost = func.AuthHeaderPost();
    this.whsId = localStorage.getItem('whs_id');
  }

  getReports(params: string = '') {
    return this.http
      .get(`${this.api.API_REPORTS_CORE}/labor-tracking/reports/${this.whsId}` + params, { headers: this.func.AuthHeader() })
      .map((res) => res.json());
  }

  getReportAPIUrl(params: string = '') {
    return `${this.api.API_REPORTS_CORE}/labor-tracking/reports/${this.whsId}${params}&export=1`;
  }

  getCustomers(params: string = '') {
    return this.http
      .get(`${this.api.API_REPORTS_CORE}/pallet/customers/${this.whsId}` + params, { headers: this.func.AuthHeader() })
      .map((res) => res.json());
  }
}
