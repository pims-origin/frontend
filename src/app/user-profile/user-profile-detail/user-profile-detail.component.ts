import {Component} from "@angular/core";
import { Router, RouterLink, ROUTER_DIRECTIVES } from '@angular/router-deprecated';
import { CORE_DIRECTIVES, FORM_DIRECTIVES } from '@angular/common';
import {UserProfileServices} from "../user-profile.service";
import {StylesheetService} from "../../common/services/stylesheet.service";

declare var jQuery: any;

@Component({
  selector: 'user-profile-detail',
  directives: [CORE_DIRECTIVES, FORM_DIRECTIVES],
  providers: [UserProfileServices,StylesheetService],
  templateUrl: 'user-profile-detail.html',
})
export class UserProfileDetailComponent{
  public profileData:any = {};
  constructor(private _profileService: UserProfileServices, private _styleService: StylesheetService){
    this._styleService.addLoading();
    _profileService.getProfileDetailByUserId().subscribe(
      (data:any) => {
        this.profileData = data.json;
        this._styleService.removeLoadding();
      },
      err => {
        console.error(err.status);
        this._styleService.removeLoadding();
      },
      () => {}
    );
  }
}
