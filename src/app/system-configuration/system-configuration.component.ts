import {Component, Pipe, PipeTransform} from "@angular/core";
import { Router, RouterLink,RouteConfig, ROUTER_DIRECTIVES } from '@angular/router-deprecated';
import { CORE_DIRECTIVES, FORM_DIRECTIVES } from '@angular/common';
import {GeneralComponent} from "./general/general.component";
import {EmailComponent} from "./email/email.component";
import {API_Config, UserService, Functions} from '../common/core/load';
import { Http } from '@angular/http';
declare var jQuery: any;

@Component({
  selector: 'system-configuration',
  directives: [CORE_DIRECTIVES, FORM_DIRECTIVES, ROUTER_DIRECTIVES],
  templateUrl: 'system-configuration.component.html'
})

@RouteConfig([
    { path: '/', component: GeneralComponent, name: 'General', useAsDefault: true },
    { path: '/email', component:EmailComponent, name: 'Email' }
])

export class SystemConfigurationComponent{

    private showLoadingOverlay = false;
    private configSystemPermission = false;
    private messages;
    constructor(
        private _http: Http,
        private _func: Functions,
        private _user: UserService,
        private _router: Router) {

        this.checkPermission();
    }
    // Check permission for user using this function page
    private checkPermission() {
        this.showLoadingOverlay = true;
        this._user.GetPermissionUser().subscribe(
            data => {
                this.configSystemPermission = this._user.RequestPermission(data,'configSystem');
                /* Check orther permission if View allow */
                if(!this.configSystemPermission) {
                    this._router.parent.navigateByUrl('/deny');
                }
                this.showLoadingOverlay = false;
            },
            err => {
                this.showMessage('danger', this._func.parseErrorMessageFromServer(err));
                this.showLoadingOverlay = false;
            }
        );
    }

    private showMessage(msgStatus, msg) {
        this.messages = {
            'status': msgStatus,
            'txt': msg
        }
        jQuery(window).scrollTop(0);
    }

}
