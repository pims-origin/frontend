import { Component } from '@angular/core';
import { FORM_DIRECTIVES, ControlGroup, FormBuilder, Control, Validators } from '@angular/common';
import { WMSBreadcrumb, DocumentUpload, WMSPagination, WMSMessages } from '../../../common/directives/directives';
import { Router, RouteData, RouteParams } from '@angular/router-deprecated';
import { BoxPopupService } from "../../../common/popup/box-popup.service";
import { LocationServices } from "./location-service";
import { GoBackComponent } from "../../../common/component/goBack.component";
import { ValidationService } from '../../../common/core/validator';
import { UserService, Functions } from '../../../common/core/load';
import { OrderBy } from "../../../common/pipes/order-by.pipe";
import { ReFillDirective } from "../refill/refill";
import { TOOLTIP_DIRECTIVES } from "ng2-bootstrap/ng2-bootstrap";
declare var jQuery: any;
@Component ({
    selector: 'cru-location',
    pipes: [OrderBy],
    directives:[WMSBreadcrumb,ReFillDirective,GoBackComponent,WMSPagination,DocumentUpload,FORM_DIRECTIVES,WMSMessages,TOOLTIP_DIRECTIVES],
    providers: [BoxPopupService,LocationServices,ValidationService,UserService],
    templateUrl: 'cru-location.html'
})
export class CRULocation {

    LocationForm: ControlGroup;
    private action = 'new';
    private title = 'Create';
    private messages;
    private Loading={};
    private showLoadingOverlay=false;
    private TitlePage="Location";
    private IsView=false;
    private Action;
    private whs_id;
    private submitForm=false;
    private loc_id;
    private Inventory:Array<any>=[];
    private dataLocatype:Array<any>=[];
    private Pagination;
    private perPage=20;
    maxLPN;

    constructor(private _router: Router,
                private _boxPopupService:BoxPopupService,
                _RTAction: RouteData,
                private _user:UserService,
                private fb: FormBuilder,
                private _Valid: ValidationService,
                private _Func: Functions,
                private locService:LocationServices,
                private params: RouteParams) {

        this.Action=_RTAction.get('action');
        this.whs_id=this._Func.lstGetItem('whs_id');
        this.checkPermission();
    }

    // Check permission for user using this function page
    private createLocation;
    private viewLocation;
    private editLocation;
    private allowAccess:boolean=false;

    private checkPermission(){

        this.showLoadingOverlay = true;
        this._user.GetPermissionUser().subscribe(
            data => {
                this.showLoadingOverlay = false;
                this.createLocation= this._user.RequestPermission(data, 'createLocation');
                this.viewLocation = this._user.RequestPermission(data,'viewLocation');
                this.editLocation = this._user.RequestPermission(data,'editLocation');
                this.getLocaType();
                this.loc_id=this.params.get('id');

                /*
                 * Action router View detail
                 * */
                if(this.Action=='view')
                {
                    if(this.viewLocation) {
                        this.IsView = true;
                        this.TitlePage = 'View Location';
                        this.allowAccess=true;
                        this.getInventory();
                    }
                    else{
                        this.redirectDeny();
                    }
                }

                // Action new
                if(this.Action=='new')
                {
                    if(this.createLocation) {
                        this.allowAccess=true;
                        this.TitlePage = 'Add Location';
                        this.formBuilder();
                    }
                    else{
                        this.redirectDeny();
                    }
                }
                // load detail / init data form
                if(this.Action=='edit'||this.Action=='view')
                {
                    this.getLocationDetail(this.loc_id);
                }

                /*
                 * Action router Edit
                 * */
                if(this.Action=='edit')
                {
                    if(this.editLocation) {
                        this.allowAccess=true;
                        this.TitlePage = 'Edit Location';
                    }
                    else{
                        this.redirectDeny();
                    }
                }

            },
            err => {
                this.parseErrorStandard(err);
                this.showLoadingOverlay = false;
            }
        );
    }

    /**
     * Redirect to deny page
     */
    private redirectDeny(){
        this._router.parent.navigateByUrl('/deny');
    }

    /**
     * Form builder
     */
    private formBuilder(data = {}) {
        this.LocationForm = this.fb.group({
            loc_id: [data['loc_id']],
            cus_name: [data['cus_name']],
            loc_code: [data['loc_code'], Validators.compose([Validators.required, this._Valid.validateSpace])],
            loc_alternative_name: [data['loc_code'], Validators.compose([Validators.required, this._Valid.validateSpace])],
            loc_type_id: [data['loc_type_id'] ? data['loc_type_id'] : '', Validators.compose([Validators.required])],
            loc_type_name: [data['loc_type_name']],
            loc_zone_name: [data['loc_zone_name']],
            rfid: [data['rfid'], Validators.compose([Validators.pattern('DDDDDDDD[0-9]{16}'), this.isInValidateRFID])],
            loc_width: [data['loc_width'], Validators.compose([Validators.required, this._Valid.isDecimalNumber, this._Valid.isZero])],
            loc_height: [data['loc_height'], Validators.compose([Validators.required, this._Valid.isDecimalNumber, this._Valid.isZero])],
            loc_length: [data['loc_length'], Validators.compose([Validators.required, this._Valid.isDecimalNumber, this._Valid.isZero])],
            loc_available_capacity: [data['loc_available_capacity']],
            loc_weight_capacity: [data['loc_weight_capacity'] ? data['loc_weight_capacity'] : '', Validators.compose([this._Valid.isDecimalNumber, this._Valid.isZero])],
            loc_sts_code: [data['loc_sts_code'] ? data['loc_sts_code'] : 'AC'],
            loc_desc: [data['loc_desc']],
            aisle: [data['aisle'], Validators.compose([Validators.required])],
            level: [data['level'], Validators.compose([Validators.required, this._Valid.validateInt, this._Valid.isZero])],
            row: [data['row'], Validators.compose([Validators.required])],
            bin: [data['bin'], Validators.compose([Validators.required, this._Valid.validateInt, this._Valid.isZero])],
            area: [data['area'] ? data['area'] : '0'],
            spc_hdl_code: [data['spc_hdl_code'] ? data['spc_hdl_code'] : 'RAC']
        });
    }

    /**
     * Get Loc Detail by Loc ID
     */
    private getLocationDetail(locId){
        this.locService.getLocationDetail(this.whs_id,locId).subscribe(
            data => {
                this.formBuilder(data);
                this.maxLPN = data['max_lpn'] ? data['max_lpn'] : '';
            },
            err => {
                this.parseErrorStandard(err);
            },
            () => {}
        );
    }

    /**
     * Get lodation type
     */
    private getLocaType () {
        this.locService.getLocationType().subscribe(
            data => {
                this.dataLocatype=data;
            },
            err => {
                this.parseErrorStandard(err);
            },
            () => {}
        );
    }

    /*
     * When change or input loc_code
     * */
    private inputLocationCode($event)
    {
        this.setValueControl('loc_alternative_name',$event.target.value);
    }

    /**
     * Save CRU function
     */
    private Save() {
        // empty messages
        this.messages = false;
        // scroll to top
        this._Func.scrollToTop();
        if (this.LocationForm.valid) {
            let data = JSON.stringify(this.LocationForm.value);
            if (this.Action == 'new') {
                // add new
                this.newLocation(data);
            }
            else {
                // Update
                const info = this._Func.cloneObject(this.LocationForm.value);
                // delete info['loc_available_capacity'];
                this.Update(JSON.stringify(info));
            }
        }
    }

    private _SaveRefill;
    SaveRefill(){
        this.submitForm=true;
        if(this.Action=='new'||this.LocationForm.value['loc_type_name']!=='Ecommerce'){
            this.Save();
        }else{
            this._SaveRefill=true;
            setTimeout(()=>{
                this._SaveRefill=false;
            });
        }

    }

    /**
     * Check Min Count Max Count
     */
    private validMinMaxCount()
    {
        if(this.LocationForm.value['loc_min_count'] && this.LocationForm.value['loc_min_count']) {
            return parseInt(this.LocationForm.value['loc_min_count']) >= parseInt(this.LocationForm.value['loc_max_count']);
        }
        return false;
    }

    /**
     * newLocation function
     */
    private newLocation(data){
        this.showLoadingOverlay=true;
        this.locService.createLocation(this.whs_id,data).subscribe(
            data => {
                this.showLoadingOverlay=false;
                this.messages = this._Func.Messages('success', this._Func.msg('VR109'));
                setTimeout(()=>{
                    this._router.parent.navigateByUrl('/system-setup/location');
                }, 600);
            },
            err => {
                this.showLoadingOverlay=false;
                this.parseErrorStandard(err);
            },
            () => {}
        );

    }

    /**
     * Update data
     */
    private Update(data){

        this.showLoadingOverlay=true;
        this.locService.updateLocation(this.whs_id,this.loc_id,data).subscribe(
            data => {
                this.showLoadingOverlay=false;
                this.messages = this._Func.Messages('success', this._Func.msg('VR109'));
                setTimeout(()=>{
                    this._router.parent.navigateByUrl('/system-setup/location');
                }, 600);
            },
            err => {
                this.showLoadingOverlay=false;
                this.parseErrorStandard(err);
            },
            () => {}
        );

    }

    // get IV
    private getInventory (page='') {
        this.Loading['inv']=true;
        let param = '/locations'+'/'+this.loc_id+'?limit='+this.perPage+'&page='+page;
        this.locService.getInventory(param).subscribe(
            data => {
                this.Inventory=data.data;
                this.Loading['inv']=false;
                //pagin function
                this.Pagination=data['meta']['pagination'];
                this.Pagination['numLinks']=3;
                this.Pagination['tmpPageCount']=this._Func.Pagination(this.Pagination);
            },
            err => {
                this.Loading['inv']=false;
                this.parseErrorStandard(err);
            },
            () => {}
        );
    }


    private checkInputNumber(event,int=false)
    {
        this._Valid.isNumber(event,int);
    }

    private cancel()
    {
        let n = this;
        this._boxPopupService.showWarningPopup()
            .then(function (dm) {
                if(dm)
                    n._router.navigateByUrl('/system-setup/location');
            });
    }

    /**
     * Set value control
     * @param control
     * @param value
     */
    private setValueControl(control,value)
    {
        (<Control>this.LocationForm.controls[control]).updateValue(value);
    }

    // Show error when server die or else
    private parseErrorStandard (err) {
        this.messages = {'status' : 'danger', 'txt' : this._Func.parseErrorMessageFromServerStandard(err)};
    }

    /**
     * redirect to list
     */
    private redirectToList(){
        let n = this;
        /*case 2: default*/
        this._boxPopupService.showWarningPopup()
            .then(function (dm) {
                if(dm)
                    n._router.parent.navigateByUrl('/system-setup/location');
            })
    }

    private isInValidateRFID(control:Control):{[key:string]:any} {
        const rfid = control.value;
        const whsId = localStorage.getItem('whs_id');

        if (rfid && rfid.length >= 12 && parseInt(rfid.trim().substring(8, 12)) !== parseInt(whsId)) {
            return {invalidRFID: true};
        }
        return null;
    }

    private setValueInvalid(event, int = true)
    {
        this._Valid.isNumber(event, int);
        if (parseInt(event.target.value) > 2 || event.target.value == '') {
            (<Control>this.LocationForm.controls['area']).updateValue(0);
        } else {
            (<Control>this.LocationForm.controls['area']).updateValue(parseInt(event.target.value));
        }
    }
	
}
